//
//  TblUser+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 20/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblUser (CoreDataProperties)

+ (NSFetchRequest<TblUser *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *about_us;
@property (nullable, nonatomic, copy) NSString *age;
@property (nullable, nonatomic, copy) NSNumber *audio;
@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *createdAt;
@property (nullable, nonatomic, copy) NSString *date_of_birth;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *fb_id;
@property (nullable, nonatomic, copy) NSString *first_name;
@property (nullable, nonatomic, copy) NSString *gender;
@property (nullable, nonatomic, copy) NSString *height;
@property (nullable, nonatomic, copy) NSNumber *is_default_image;
@property (nullable, nonatomic, copy) NSNumber *is_premium;
@property (nullable, nonatomic, copy) NSNumber *is_verified;
@property (nullable, nonatomic, copy) NSNumber *isVoiceChatEnable;
@property (nullable, nonatomic, copy) NSString *last_name;
@property (nullable, nonatomic, copy) NSNumber *muteVoiceChat;
@property (nullable, nonatomic, copy) NSNumber *notification;
@property (nullable, nonatomic, copy) NSString *notification_count;
@property (nullable, nonatomic, copy) NSNumber *notification_sound;
@property (nullable, nonatomic, copy) NSNumber *notification_vibrate;
@property (nullable, nonatomic, copy) NSString *picture;
@property (nullable, nonatomic, copy) NSString *picture_path;
@property (nullable, nonatomic, copy) NSString *profile_visibility;
@property (nullable, nonatomic, copy) NSNumber *profileStatus;
@property (nullable, nonatomic, copy) NSString *receipt_id;
@property (nullable, nonatomic, copy) NSString *reward_point;
@property (nullable, nonatomic, copy) NSString *run_performance;
@property (nullable, nonatomic, copy) NSString *run_rank;
@property (nullable, nonatomic, copy) NSString *run_total_BPM;
@property (nullable, nonatomic, copy) NSString *run_total_calories;
@property (nullable, nonatomic, copy) NSString *run_total_miles;
@property (nullable, nonatomic, copy) NSString *run_total_runs;
@property (nullable, nonatomic, copy) NSNumber *session_notice_checkbox;
@property (nullable, nonatomic, copy) NSNumber *status;
@property (nullable, nonatomic, copy) NSString *strava_id;
@property (nullable, nonatomic, copy) NSNumber *stravaConnection;
@property (nullable, nonatomic, copy) NSString *subscription;
@property (nullable, nonatomic, copy) NSString *subscription_exp_date;
@property (nullable, nonatomic, copy) NSNumber *subscription_plan;
@property (nullable, nonatomic, copy) NSNumber *subscription_trial;
@property (nullable, nonatomic, copy) NSString *token;
@property (nullable, nonatomic, copy) NSString *units;
@property (nullable, nonatomic, copy) NSString *updatedAt;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, copy) NSString *user_token;
@property (nullable, nonatomic, copy) NSNumber *verify;
@property (nullable, nonatomic, copy) NSString *weight;
@property (nullable, nonatomic, copy) NSString *link;
@property (nullable, nonatomic, retain) NSSet<TblChats *> *chats;

@end

@interface TblUser (CoreDataGeneratedAccessors)

- (void)addChatsObject:(TblChats *)value;
- (void)removeChatsObject:(TblChats *)value;
- (void)addChats:(NSSet<TblChats *> *)values;
- (void)removeChats:(NSSet<TblChats *> *)values;

@end

NS_ASSUME_NONNULL_END
