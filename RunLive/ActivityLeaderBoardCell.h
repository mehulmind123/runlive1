//
//  ActivityLeaderBoardCell.h
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityLeaderBoardCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UIView *viewUser;
@property(weak,nonatomic) IBOutlet UIImageView *imgUser,*imgVerifiedUser;
@property(weak,nonatomic) IBOutlet UIImageView *imgRankTag;
@property(weak,nonatomic) IBOutlet UILabel *lblRank;
@property(weak,nonatomic) IBOutlet UILabel *lblUserName;
@property(weak,nonatomic) IBOutlet UILabel *lblRunData;

@end
