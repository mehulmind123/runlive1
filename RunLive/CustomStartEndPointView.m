//
//  CustomStartEndPointView.m
//  RunLive
//
//  Created by mac-0005 on 13/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "CustomStartEndPointView.h"

@implementation CustomStartEndPointView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;
}

@end
