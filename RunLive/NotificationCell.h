//
//  NotificationCell.h
//  RunLive
//
//  Created by mac-0006 on 21/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : SWTableViewCell

@property IBOutlet TTTAttributedLabel *lblContent;

@property IBOutlet UILabel *lblTime;
@property IBOutlet UIImageView *imgDP,*imgSwipeIcon;
@property IBOutlet UIView *vwBorder;

@property(weak,nonatomic) IBOutlet UIButton *btnUser;

@end
