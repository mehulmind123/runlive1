//
//  OtherUserProfileViewController.m
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "OtherUserProfileViewController.h"
#import "SyncedActivityCell.h"
#import "StartActivityCell.h"
#import "ActivityAddPhotoCell.h"
#import "CommentsViewController.h"
#import "ActivityDetailViewController.h"
#import "SessionDetailsViewController.h"


@interface OtherUserProfileViewController ()
{
    NSIndexPath *indexPathForVideoCell;
    ActivityAddPhotoCell *cellActivityVideo;
    NSMutableArray *arrUserBasicDetails,*arrUserActivity;
    BOOL shouldShowLoader;
    
    NSURLSessionDataTask *taskRuningActivityFeed;
    BOOL isApiStarted;
    int iOffset;
    
    UIRefreshControl *refreshControl;
}
@end

@implementation OtherUserProfileViewController

#pragma mark - Life cycle
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [tblOtherUser registerNib:[UINib nibWithNibName:@"ProfileHeaderCell" bundle:nil] forCellReuseIdentifier:@"ProfileHeaderCell"];
    [tblOtherUser registerNib:[UINib nibWithNibName:@"ActivityAddPhotoCell" bundle:nil] forCellReuseIdentifier:@"ActivityAddPhotoCell"];
    [tblOtherUser registerNib:[UINib nibWithNibName:@"StartActivityCell" bundle:nil] forCellReuseIdentifier:@"StartActivityCell"];
    [tblOtherUser registerNib:[UINib nibWithNibName:@"SyncedActivityCell" bundle:nil] forCellReuseIdentifier:@"SyncedActivityCell"];
    tblOtherUser.estimatedRowHeight = 621;
    tblOtherUser.rowHeight = UITableViewAutomaticDimension;
    
    arrUserBasicDetails = [NSMutableArray new];
    arrUserActivity = [NSMutableArray new];
    
    viewPopup.hidden = YES;
    viewContent.layer.cornerRadius = 3;
    viewContent.layer.masksToBounds = YES;
    
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblOtherUser addSubview:refreshControl];
    
    [self getUserBasicDetails];
    
    appDelegate.configureRefreshOtherProfileActivityList = ^(ActivityData activityData)
    {
        if (activityData == CallActivityApi)
        {
            iOffset = 0;
            [self getActivityDataFromServer];
        }
        else if (activityData == RefreshActivityList)
        {
            [self fetchAllActivityFromLocal];
        }
        else if (activityData == DeleteActivity)
            [self fetchAllActivityFromLocal];
    };
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [appDelegate hideTabBar];
    [self fetchAllActivityFromLocal];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
// To Stop video when screen will disappear ........
    [self stopAllreadyPlayedVideo];
    
    if (taskRuningActivityFeed && taskRuningActivityFeed.state == NSURLSessionTaskStateRunning)
        [taskRuningActivityFeed cancel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Pull Refresh
#pragma mark -

-(void)handleRefresh
{
    if (isApiStarted)
        return;
    
    iOffset = 0;
    [self getActivityDataFromServer];
}

- (void)loadMoreData
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self getActivityDataFromServer];
}

#pragma mark - API Related Function
#pragma mark -

-(void)getUserBasicDetails
{
    if (!self.strUserName)
        return;

    [[APIRequest request] otherUserProfile:self.strUserName showLoader:YES completed:^(id responseObject, NSError *error)
    {
        [arrUserBasicDetails removeAllObjects];
        [tblOtherUser reloadData];
        
        if (responseObject && !error)
        {
            viewNoData.hidden = YES;
            tblOtherUser.hidden = NO;
            NSLog(@"%@",responseObject);
            [arrUserBasicDetails removeAllObjects];
            
            if ([[responseObject valueForKey:CJsonData] objectForKey:@"user_name"])
                [arrUserBasicDetails addObject:[responseObject valueForKey:CJsonData]];
            
            if (arrUserBasicDetails.count > 0)
            {
            }
            else
            {
                btnBack.hidden = NO;
                viewNoData.hidden = NO;
                tblOtherUser.hidden = YES;
            }
            
            CGPoint contentOffset = tblOtherUser.contentOffset;
            [tblOtherUser reloadData];
            [tblOtherUser layoutIfNeeded]; // Force layout so things are updated before resetting the contentOffset.
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [tblOtherUser setContentOffset:contentOffset animated:NO];
            });
            
            iOffset = 0;
            [self getActivityDataFromServer];

        }
        else
        {
            btnBack.hidden = NO;
            viewNoData.hidden = NO;
            tblOtherUser.hidden = YES;
        }
    }];
}

-(void)getActivityDataFromServer
{
    if (!self.strUserName)
        return;
    
    isApiStarted = YES;
    
    taskRuningActivityFeed = [[APIRequest request] GetActivityFeedList:[NSNumber numberWithInt:iOffset] Type:@"2" UserName:self.strUserName completed:^(id responseObject, NSError *error)
                              {
                                  isApiStarted = NO;
                                  [refreshControl endRefreshing];
                                  if (responseObject && !error)
                                  {
                                      if (iOffset == 0)
                                      {
                                          [arrUserActivity removeAllObjects];
                                          [tblOtherUser reloadData];
                                      }
                                      
                                      iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
                                      NSArray *arrTemp = [responseObject valueForKey:CJsonData];
                                      
                                      if (arrTemp.count > 0)
                                          [self fetchAllActivityFromLocal];
                                      
                                  }
                              }];
}

-(void)fetchAllActivityFromLocal
{
    [arrUserActivity removeAllObjects];
    [arrUserActivity addObjectsFromArray:[TblActivityDates fetch:[NSPredicate predicateWithFormat:@"user_name == %@",self.strUserName] orderBy:@"timestamp" ascending:NO]];
    [tblOtherUser reloadData];
    [self playVideoAutomatically];
}


-(NSArray *)getActivityArray:(TblActivityDates *)objActivitySection
{
    NSMutableArray *arrActTemp = [NSMutableArray new];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(user_name == %@)", self.strUserName];
    
    if (objActivitySection.objActivityPhoto.allObjects.count > 0 && [objActivitySection.activity_filter_type isEqual:@0])
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityPhoto.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityVideo.allObjects.count > 0  && [objActivitySection.activity_filter_type isEqual:@0])
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityVideo.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivitySyncSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivitySyncSession.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityRunSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityRunSession.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityCompleteSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityCompleteSession.allObjects filteredArrayUsingPredicate:predicate]];

    
    NSArray *arrDataActivity = [arrActTemp sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
    
    return arrDataActivity;
}

-(void)stopAllreadyPlayedVideo
{
    if (cellActivityVideo)  // Pause Allready played video.....
    {
        if ([cellActivityVideo isKindOfClass:[ActivityAddPhotoCell class]])
        {
            // NSLog(@"View hidden = = %d",cellActivityVideo.viewVideo.hidden);
            if (!cellActivityVideo.viewVideo.hidden && cellActivityVideo && cellActivityVideo.tag == 100)
            {
                NSLog(@"stop Video here ==========>>>>>  ");
                [cellActivityVideo StopVideoPlaying];
                cellActivityVideo = nil;
            }
        }
    }
}

-(void)playVideoAutomatically
{
        NSArray *arrVisibleCell = [tblOtherUser visibleCells];
        NSMutableArray *arrVideoCells = [NSMutableArray new];
        for (int c = 0; arrVisibleCell.count > c; c++)
        {
            UITableViewCell *cell = arrVisibleCell[c];
            if ([cell isKindOfClass:[ActivityAddPhotoCell class]])
            {
                // Get Video cell here....
                ActivityAddPhotoCell *cellVideo = (ActivityAddPhotoCell *)cell;
                if (!cellVideo.viewVideo.hidden)
                    [arrVideoCells addObject:cellVideo];
            }
        }
        
        for(ActivityAddPhotoCell *cell in arrVideoCells)
        {
            CGRect intersect = CGRectIntersection(tblOtherUser.frame, [tblOtherUser convertRect:[tblOtherUser rectForRowAtIndexPath:[tblOtherUser indexPathForCell:cell]] toView:tblOtherUser.superview]);
            
            //play here..
            if (![cell isEqual:cellActivityVideo] && !cell.viewVideo.hidden && intersect.size.height > CViewHeight(cell)*0.6) // only if 60% of the cell is visible
            {
                [self stopAllreadyPlayedVideo];
                cellActivityVideo = cell;
                cellActivityVideo.tag = 100;
                [cellActivityVideo setUpForVideoPlayer];
            }
        }
}

#pragma mark - UITableView Delegate and Datasource
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrUserActivity.count +1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return arrUserBasicDetails.count;
    else
    {
        TblActivityDates *objActivitySection = arrUserActivity[section-1];
        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        
        return arrActivity.count;

    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    if (indexPath.section == 0)
//        return 621;
//    
//    return UITableViewAutomaticDimension;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section == 0)
//        return 621;
//    
//    return UITableViewAutomaticDimension;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return nil;
    
    TblActivityDates *objActivitySection = arrUserActivity[section -1];
    
    ProfileHeaderView *viewHeader;
    viewHeader = [ProfileHeaderView viewFromXib];
    viewHeader.lblMonth.text = [NSString stringWithFormat:@"%@ -",objActivitySection.show_date];
    viewHeader.lblRunCount.text = objActivitySection.total_month_run.intValue == 1 ? [NSString stringWithFormat:@"%@ RUN",objActivitySection.total_month_run] : [NSString stringWithFormat:@"%@ RUNS",objActivitySection.total_month_run];
    viewHeader.lblMileCount.text = [[appDelegate getDistanceInWithSelectedFormate:objActivitySection.total_month_distance] uppercaseString];
    
    viewHeader.viewHeaderTopLine.hidden = !(section == 1);
    
    if ([objActivitySection.activity_filter_type isEqual:@0])
    {
        viewHeader.viewLineAll.hidden = NO;
        viewHeader.viewLineRuns.hidden = YES;
        viewHeader.btnRuns.selected = NO;
        viewHeader.btnAll.selected = YES;
    }
    else
    {
        viewHeader.viewLineAll.hidden = YES;
        viewHeader.viewLineRuns.hidden = NO;
        viewHeader.btnAll.selected = NO;
        viewHeader.btnRuns.selected = YES;
    }
    
    [viewHeader.btnAll touchUpInsideClicked:^{
        viewHeader.viewLineAll.hidden = NO;
        viewHeader.viewLineRuns.hidden = YES;
        viewHeader.btnRuns.selected = NO;
        viewHeader.btnAll.selected = YES;
        
        objActivitySection.activity_filter_type = @0;
        [[Store sharedInstance].mainManagedObjectContext save];
        [self stopAllreadyPlayedVideo];
        [tblOtherUser reloadData];
        
        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        if(arrActivity.count > 0)
            [tblOtherUser scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }];
    
    [viewHeader.btnRuns touchUpInsideClicked:^{
        viewHeader.viewLineAll.hidden = YES;
        viewHeader.viewLineRuns.hidden = NO;
        viewHeader.btnAll.selected = NO;
        viewHeader.btnRuns.selected = YES;
        
        [self stopAllreadyPlayedVideo];
        
        objActivitySection.activity_filter_type = @1;
        [[Store sharedInstance].mainManagedObjectContext save];
        [tblOtherUser reloadData];

        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        if(arrActivity.count > 0)
            [tblOtherUser scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }];
    
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 0;
    
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        NSString *strIdentifier = @"ProfileHeaderCell";
        ProfileHeaderCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:strIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrUserBasicDetails objectAtIndex:indexPath.row];
        cell.lblWebsiteLink.text = [dicData stringValueForJSON:@"website_url"];
        cell.lblWebsiteLink.text = appDelegate.loginUser.link;
        if(![cell.lblWebsiteLink.text isBlankValidationPassed])
            [cell.lblTotalEarnedPoints setConstraintConstant:-20 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        else
            [cell.lblTotalEarnedPoints setConstraintConstant:18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
        NSString *strName = [NSString stringWithFormat:@"%@ %@",[dicData stringValueForJSON:@"first_name"],[dicData stringValueForJSON:@"last_name"]];
        cell.lblUserBio.text = [NSString stringWithFormat:@"%@ %@",strName, [dicData stringValueForJSON:@"about_us"]];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblUserBio.attributedText];
        [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
        [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblUserBio.font.pointSize) range: NSMakeRange(0,strName.length)];
        cell.lblUserBio.attributedText  = text;

        
        [cell.btnWebsiteLink touchUpInsideClicked:^{
            if([cell.lblWebsiteLink.text isBlankValidationPassed])
            {
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:cell.lblWebsiteLink.text]])
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:cell.lblWebsiteLink.text] options:@{} completionHandler:nil];
                else
                {
                    [self customAlertViewWithOneButton:CMessageSorry Message:@"Link Incorrect." ButtonText:@"Ok" Animation:YES completed:nil];
                }
            }
        }];
        
        self.strUserId = [dicData stringValueForJSON:@"_id"];
        
        NSMutableArray *arrRunData = [NSMutableArray new];
        
        NSMutableDictionary *dicRun = [NSMutableDictionary new];
        [dicRun addObject:@"RUNS" forKey:@"title"];
        [dicRun addObject:[[dicData valueForKey:@"run"] stringValueForJSON:@"total_runs"] forKey:@"count"];
        [arrRunData addObject:dicRun];

        NSMutableDictionary *dicDis = [NSMutableDictionary new];
        
        if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
        {
            [dicDis addObject:@"DISTANCE (KM)" forKey:@"title"];
            [dicDis addObject:[NSString stringWithFormat:@"%.2f",[[dicData valueForKey:@"run"] stringValueForJSON:@"total_distance"].floatValue/1000] forKey:@"count"];
        }
        else
        {
            [dicDis addObject:@"DISTANCE (MILES)" forKey:@"title"];
            [dicDis addObject:[NSString stringWithFormat:@"%.2f",[[dicData valueForKey:@"run"] stringValueForJSON:@"total_distance"].floatValue * 0.000621371] forKey:@"count"];
        }

        [arrRunData addObject:dicDis];

        NSMutableDictionary *dicCal = [NSMutableDictionary new];
        [dicCal addObject:[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"EST KCAL. BURNED" : @"EST CAL. BURNED" forKey:@"title"];

        [dicCal addObject:[NSString stringWithFormat:@"%d",[[dicData valueForKey:@"run"] stringValueForJSON:@"total_calories"].intValue] forKey:@"count"];
        [arrRunData addObject:dicCal];
        
        NSMutableDictionary *dicBPM = [NSMutableDictionary new];
        [dicBPM addObject:@"AVG BPM" forKey:@"title"];
        [dicBPM addObject:[NSString stringWithFormat:@"%d",[[dicData valueForKey:@"run"] stringValueForJSON:@"average_bpm"].intValue] forKey:@"count"];

        [arrRunData addObject:dicBPM];

        NSInteger rowCount =  arrRunData.count/3;
        if (arrRunData.count%3 > 0)
            rowCount += 1;
        
        cell.arrList = arrRunData;
        cell.pageControl.numberOfPages = rowCount;
        cell.pageControl.currentPage = 0;
        [cell.collview reloadData];
        
        if ([[dicData numberForJson:@"picture_updated"] isEqual:@1])
            [cell.imgUser setImageWithURL:[NSURL URLWithString:[dicData stringValueForJSON:@"picture"]] placeholderImage:CPlaceholderUserImage options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        else
            cell.imgUser.image = CPlaceholderUserImage;

        cell.lblFirstName.text = [dicData stringValueForJSON:@"first_name"].uppercaseString;
        cell.lblLastName.text = [dicData stringValueForJSON:@"last_name"].uppercaseString;
        cell.lblUserName.text = [NSString stringWithFormat:@"@%@",[dicData stringValueForJSON:@"user_name"]];

        cell.imgVerified.hidden = [[dicData numberForJson:@"is_verified"] isEqual:@0];
        
        cell.lblTotalEarnedPoints.text = [NSString stringWithFormat:@"%d",[dicData stringValueForJSON:@"reward_point"].intValue];
        
         cell.btnNotification.hidden = cell.viewNotificationBadge.hidden = YES;
        
        if ([[dicData numberForJson:@"follow"] isEqual:@1])
        {
            cell.viewFollow.hidden = NO;
            cell.btnFollow.hidden = YES;
            [btnStartStopFollowing setTitle:@"Unfollow" forState:UIControlStateNormal];
        }
        else
        {
            cell.viewFollow.hidden = YES;
            cell.btnFollow.hidden = NO;
            [btnStartStopFollowing setTitle:@"Follow" forState:UIControlStateNormal];
        }
        
        if([[dicData numberForJson:@"block"] isEqual:@1])
            [btnBlockUnblock setTitle:@"Unblock" forState:UIControlStateNormal];
        else
            [btnBlockUnblock setTitle:@"Block" forState:UIControlStateNormal];
        
        [btnStartStopFollowing touchUpInsideClicked:^{
            
            if (!self.strUserId)
                return;
            
            if ([btnStartStopFollowing.titleLabel.text isEqualToString:@"Follow"])
            {
                if([btnBlockUnblock.titleLabel.text isEqualToString:@"Unblock"])
                {
                    [self customAlertViewWithOneButton:@"" Message:@"You can not follow this user. First unblock him." ButtonText:@"OK" Animation:YES completed:nil];
                }
                else
                {
                    [[APIRequest request] followUser:self.strUserId completed:^(id responseObject, NSError *error)
                     {
                         [btnStartStopFollowing setTitle:@"Unfollow" forState:UIControlStateNormal];
                         cell.btnFollow.hidden = viewPopup.hidden = YES;
                         cell.viewFollow.hidden = NO;
                     }];
                }
            }
            else
            {
                [[APIRequest request] unfollowUser:self.strUserId completed:^(id responseObject, NSError *error)
                 {
                     [btnStartStopFollowing setTitle:@"Follow" forState:UIControlStateNormal];
                     cell.btnFollow.hidden = NO;
                     cell.viewFollow.hidden = viewPopup.hidden = YES;
                 }];
            }
            
            return;
        }];
        
        [cell.btnFollow touchUpInsideClicked:^{
            
            if (self.strUserId)
            {
                [[APIRequest request] followUser:self.strUserId completed:^(id responseObject, NSError *error)
                 {
                     cell.viewFollow.hidden = NO;
                     cell.btnFollow.hidden = YES;
                     //btnStartStopFollowing.selected = YES;
                     [btnStartStopFollowing setTitle:@"Unfollow" forState:UIControlStateNormal];
                 }];
            }
        }];
        
        [cell.btnBack touchUpInsideClicked:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [cell.btnPopup touchUpInsideClicked:^{
            viewPopup.hidden = NO;
        }];
        
        [cell.btnUserZoom touchUpInsideClicked:^{
            
            shouldShowLoader = NO;
            
            if (![[dicData numberForJson:@"picture_updated"] isEqual:@1])
                return ;
            
            ProfileHeaderCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
            imageInfo.placeholderImage = cell.imgUser.image;
            imageInfo.image = cell.imgUser.image;
            imageInfo.referenceRect = cell.imgUser.frame;
            imageInfo.referenceView = cell.imgUser.superview;
            imageInfo.referenceContentMode = cell.imgUser.contentMode;
            imageInfo.referenceCornerRadius = cell.imgUser.layer.cornerRadius;
            
            JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo mode:JTSImageViewControllerMode_Image backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
            [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
        }];
        
        return cell;
    }
    else
    {
        TblActivityDates *objActivitySection = arrUserActivity[indexPath.section -1];
        NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
        NSManagedObject *objCoreData = (NSManagedObject *)arrDataActivity[indexPath.row];
        
        if ([objCoreData isKindOfClass:[TblActivityPhoto class]])
        {
            // Photo Activity
            TblActivityPhoto *objActPhoto = (TblActivityPhoto *)objCoreData;
            ActivityAddPhotoCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configurePhotoActivityCell:cell Activity:objActPhoto];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivityVideo class]])
        {
            // Video Activity
            TblActivityVideo *objActVideo = (TblActivityVideo *)objCoreData;
            ActivityAddPhotoCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureVideoActivityCell:cell Activity:objActVideo];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivitySyncSession class]])
        {
            // Sync Activity
            TblActivitySyncSession *objActSync = (TblActivitySyncSession *)objCoreData;
            SyncedActivityCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:NSStringFromClass([SyncedActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureSyncedActivityCell:cell Activity:objActSync];
            return cell;
            
        }
        else if ([objCoreData isKindOfClass:[TblActivityRunSession class]])
        {
            // Ongoing Run Activity
            TblActivityRunSession *objActRun = (TblActivityRunSession *)objCoreData;
            StartActivityCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureStartSessionActivityCell:cell Activity:objActRun];
            
            return cell;
            
        }
        else if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
        {
            // Complete Run Activity
            TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
            StartActivityCell *cell = [tblOtherUser dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [self configureCompleteSessionActivityCell:cell Activity:objActCom];
            return cell;
        }

    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
        return;
    
    TblActivityDates *objActivitySection = arrUserActivity[indexPath.section -1];
    NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
    NSManagedObject *objCoreData = (NSManagedObject *)arrDataActivity[indexPath.row];

    if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
    {
        TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
        ActivityDetailViewController *objAct = [[ActivityDetailViewController alloc] init];
        objAct.strActivityId = objActCom.activity_id;
        objAct.strResultSessionId = objActCom.session_id;
        [self.navigationController pushViewController:objAct animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // Stop video here when cell invisible from table view...
    if ([cell isEqual:cellActivityVideo])
        [self stopAllreadyPlayedVideo];
}

#pragma mark - Configure ActivityAddPhotoCell
#pragma mark -

- (void)configurePhotoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityPhoto *)objActivityPhoto
{
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = YES;
    cell.imgAddedPhoto.hidden = NO;
    cell.btnDelete.hidden = YES;
    
    [cell setImageHeightWidth:objActivityPhoto.img_width Height:objActivityPhoto.img_hieght PhotoActivity:YES];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityPhoto.activity_post_time];
    
    [cell.lblPhotoCaption hideByHeight:![objActivityPhoto.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityPhoto.caption;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityPhoto.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgAddedPhoto setImageWithURL:[NSURL URLWithString:objActivityPhoto.act_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityPhoto.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivityPhoto.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityPhoto.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    
    NSString *strName = objActivityPhoto.user_name;
    NSString *strSession = objActivityPhoto.session_name;
    NSString *strActText = objActivityPhoto.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityPhoto.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
    }
    
    cell.imgAddedPhoto.userInteractionEnabled = YES;
    [cell.doubleTapeGesture addTarget:self action:@selector(imgDoubleTapGesture:)];
    cell.doubleTapeGesture.numberOfTapsRequired = 2;
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityPhoto.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityPhoto.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityPhoto.is_like = @YES;
        }
        
        objActivityPhoto.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
        [appDelegate likeActivity:objActivityPhoto.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityPhoto.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityPhoto.activity_id ActType:objActivityPhoto.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

-(void)imgDoubleTapGesture:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:tblOtherUser];
    NSIndexPath *tappedIndexPath = [tblOtherUser indexPathForRowAtPoint:point];
    
    ActivityAddPhotoCell *cell = [tblOtherUser cellForRowAtIndexPath:tappedIndexPath];
    
    cell.imgHeartLikeZoom.hidden = NO;
    cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 8.0, 8.0);} completion:^(BOOL finished){ }];
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);} completion:^(BOOL finished) {cell.imgHeartLikeZoom.hidden = YES;}];
    
    if (!cell.btnLike.selected)
        [cell.btnLike sendActionsForControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Configure ActivityAddVideoCell
#pragma mark -
- (void)configureVideoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityVideo *)objActivityVideo
{
    cell.objVideoActivity = objActivityVideo;
    [cell setImageHeightWidth:objActivityVideo.video_width Height:objActivityVideo.video_height PhotoActivity:NO];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityVideo.activity_post_time];
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = NO;
    cell.imgAddedPhoto.hidden = YES;
    cell.btnDelete.hidden = YES;
    
    [cell.lblPhotoCaption hideByHeight:![objActivityVideo.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityVideo.caption;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityVideo.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgVThumb setImageWithURL:[NSURL URLWithString:objActivityVideo.thumb_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.btnLike.selected = objActivityVideo.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityVideo.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityVideo.total_comments.integerValue < 3];
    
    NSString *strName = objActivityVideo.user_name;
    NSString *strSession = objActivityVideo.session_name;
    NSString *strActText = objActivityVideo.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityVideo.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
       [appDelegate openSharingOption:nil VideoURL:objActivityVideo.video_url Video:YES TabBar:NO];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityVideo.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityVideo.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityVideo.is_like = @YES;
        }
        
        objActivityVideo.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
        [appDelegate likeActivity:objActivityVideo.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityVideo.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityVideo.activity_id  ActType:objActivityVideo.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnVideo touchUpInsideClicked:^{
        
        if (cell.btnVideo.selected)
        {
            // Already Played
            [cell StopVideoPlaying];
            cellActivityVideo = nil;
        }
        else
        {
            if (cellActivityVideo)
                [self stopAllreadyPlayedVideo];

            cellActivityVideo = cell;
            cellActivityVideo.tag = 100;
            [cell setUpForVideoPlayer];
        }
    }];
    
    cell.btnMuteUnmute.selected = objActivityVideo.video_mute.boolValue;
    [cell.btnMuteUnmute touchUpInsideClicked:^{
        cell.btnMuteUnmute.selected = !cell.btnMuteUnmute.selected;
        cell.playerView.player.muted = cell.btnMuteUnmute.selected;
        objActivityVideo.video_mute = cell.btnMuteUnmute.selected ? @YES : @NO;
        [[Store sharedInstance].mainManagedObjectContext save];
    }];
    
    
}


#pragma mark - Configure Start Session Activity
#pragma mark -

- (void)configureStartSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityRunSession *)objActivityRun
{
    [cell drawMapPath:objActivityRun.runner_path.mutableCopy];
    
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityRun.activity_post_time];
    //    cell.mapView = nil ;
    
    cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityRun.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityRun.total_comments.integerValue < 3];
    
    cell.lblSessionComTime.text = objActivityRun.session_complete_time;
    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityRun.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityRun.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";
    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityRun.position,objActivityRun.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityRun.total_runner];
    cell.lblCity.text = objActivityRun.city;
    cell.lblSubLocality.text = objActivityRun.sublocality;
    
    cell.btnLike.selected = objActivityRun.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityRun.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityRun.user_name;
    NSString *strSession = objActivityRun.session_name;
    NSString *strActText = objActivityRun.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivityRun.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text =cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityRun.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityRun.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityRun.is_like = @YES;
        }
        
        objActivityRun.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
        [appDelegate likeActivity:objActivityRun.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityRun.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityRun.activity_id  ActType:objActivityRun.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

#pragma mark - Configure Complete Session Activity
#pragma mark -

- (void)configureCompleteSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityCompleteSession *)objActivityCom
{
    //cell.mapView = nil ;
    [cell drawMapPath:objActivityCom.runner_path.mutableCopy];
    
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityCom.activity_post_time];
    
    if (objActivityCom.position.integerValue < 4)
    {
        cell.imgPosition.hidden = cell.viewPosition.hidden = NO;
        cell.lblWinPositon.text = objActivityCom.position;
    }
    else
        cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityCom.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityCom.total_comments.integerValue < 3];
    
    cell.lblSessionComTime.text = objActivityCom.session_complete_time;
    
    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityCom.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityCom.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";

    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityCom.position,objActivityCom.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityCom.total_runner];
    cell.lblCity.text = objActivityCom.city;
    cell.lblSubLocality.text = objActivityCom.sublocality;
    
    cell.btnLike.selected = objActivityCom.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityCom.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    NSString *strName = objActivityCom.user_name;
    NSString *strSession = objActivityCom.session_name;
    NSString *strActText = objActivityCom.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityCom.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityCom.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityCom.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityCom.is_like = @YES;
        }
        
        objActivityCom.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
        [appDelegate likeActivity:objActivityCom.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityCom.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityCom.activity_id  ActType:objActivityCom.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

#pragma mark - Configure SyncedActivityCell
#pragma mark -

- (void)configureSyncedActivityCell:(SyncedActivityCell *)cell Activity:(TblActivitySyncSession *)objActivitySync
{
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivitySync.activity_post_time];
    
    cell.objSessionTime = objActivitySync;
    [cell showSyncUser:objActivitySync.sync_user.mutableCopy];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivitySync.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivitySync.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivitySync.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivitySync.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    cell.lblSessionName.text = objActivitySync.session_name;
    
    NSString *strName = objActivitySync.user_name ? objActivitySync.user_name : @"";
    NSString *strSession = objActivitySync.session_name ? objActivitySync.session_name : @"";
    NSString *strActText = objActivitySync.activity_text ? objActivitySync.activity_text : @"";
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivitySync.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivitySync.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivitySync.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivitySync.is_like = @YES;
        }
        
        objActivitySync.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
        [appDelegate likeActivity:objActivitySync.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivitySync.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivitySync.activity_id  ActType:objActivitySync.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell startTimer];
    cell.configureSesionSyncTimeOver = ^(BOOL isOver)
    {
        if (objActivitySync)
        {
            [TblActivitySyncSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivitySync.activity_id]];
            [self fetchAllActivityFromLocal];
        }
    };
}

#pragma mark - NSMutableAttributedString
#pragma mark -

-(NSMutableAttributedString *)getAttributeStirng:(NSString *)strStartText andStartRange:(NSInteger)startRange Lable:(UILabel *)lbl strAttString:(NSMutableAttributedString *)text
{
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(startRange, strStartText.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(lbl.font.pointSize) range: NSMakeRange(0,strStartText.length)];
    return text;
}


#pragma mark - Scroll View Delegate
#pragma mark -

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt
{
    [self playVideoAutomatically];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self playVideoAutomatically];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if ([aScrollView isEqual:tblOtherUser] && Is_iPhone_X)
        cnTblTopSpace.constant = tblOtherUser.contentOffset.y > 588 ? 30 : 0;
}



#pragma mark - Helper Function
#pragma mark -

-(void)moveOnCommentScreen:(NSString *)strActID ActType:(NSString *)strActType
{
    CommentsViewController *objCo = [[CommentsViewController alloc] init];
    objCo.strActivityId = strActID;
    objCo.strActivityType = strActType;
    [self.navigationController pushViewController:objCo animated:YES];
}


#pragma mark - Action Event
#pragma mark -

- (IBAction)btnPopupClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
                composeViewController.mailComposeDelegate = self;
                
                [composeViewController setSubject:[NSString stringWithFormat:@"@%@",self.strUserName]];
                [composeViewController setMessageBody:@"Please tell us why you are reporting this user and we will look into your concerns:" isHTML:NO];
                [composeViewController setToRecipients:@[@"reportuser@runlive.fit"]];
                [composeViewController setCcRecipients:@[@"mike@runlive.fit"]];
                
                [self presentViewController:composeViewController animated:YES completion:nil];
            }
            else
            {
                [self customAlertViewWithOneButton:@"" Message:MILocalizedString(@"MFMailComposeViewControllerCanNotSendMail", @"Can not send Mail") ButtonText:@"OK" Animation:YES completed:nil];
            }

        }
            break;
        case 1:
        {
            if (!self.strUserId)
                return;
            
            if ([btnBlockUnblock.titleLabel.text isEqualToString:@"Unblock"])
            {
                [[APIRequest request] unblockUser:self.strUserId completed:^(id responseObject, NSError *error)
                 {
                     NSLog(@"%@",responseObject);
                    // btnBlockUnblock.selected = NO;
                     
                     [btnBlockUnblock setTitle:@"Block" forState:UIControlStateNormal];
                 }];
            }
            else
            {
                NSString *strMessage = [NSString stringWithFormat:@"Are you sure want to block %@?",self.strUserName];
                
                [self customAlertViewWithTwoButton:@"" Message:strMessage ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
                    if (index == 0)
                    {
                        [[APIRequest request] addBlockUser:self.strUserId completed:^(id responseObject, NSError *error)
                         {
                             NSLog(@"%@",responseObject);
                             //btnBlockUnblock.selected = YES;
                             [btnBlockUnblock setTitle:@"Unblock" forState:UIControlStateNormal];
                             [self getUserBasicDetails];
                         }];
                    }
                }];
            }
        }
            break;
        case 3:
        {
            viewPopup.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}

@end
