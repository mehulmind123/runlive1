//
//  UIButton+EventHandler.m
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIButton+EventHandler.h"

#import "Master.h"
#import "NSObject+NewProperty.h"


static NSString *const BUTTONCALLBACKHANDLER = @"buttonHandler";


@implementation UIButton (EventHandler)

#pragma mark - Touch Up Inside Handler


-(void)touchUpInsideClicked:(TouchUpInsideHandler)clicked
{
    [self setObject:clicked forKey:BUTTONCALLBACKHANDLER];
    [self addTarget:self action:@selector(touchUpInsideClickedEventFired:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)touchUpInsideClickedEventFired:(UIButton *)sender
{
    TouchUpInsideHandler obj = [self objectForKey:BUTTONCALLBACKHANDLER];
    
    if (obj)
    {
        obj();
    }
}


@end
