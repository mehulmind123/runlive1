//
//  UIApplication+Extension.m
//  Master
//
//  Created by mac-0001 on 01/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import "UIApplication+Extension.h"

#import "Master.h"

#import "NSObject+NewProperty.h"

#import "PushNotificationManager.h"


#import <EventKit/EventKit.h>


@interface UIViewController ()

+(void)sendEmailToDeveloper:(NSString *)subject body:(NSString *)body completion:(MIBooleanResultBlock)completion;

@end


@interface NSObject ()

+ (instancetype)sharedInstance;
- (void)setDisableTracing:(BOOL)tracing;

@end



@implementation UIApplication (Extension)

#pragma mark - Crash Management

+(NSString *)crashDirectory
{
    return [CCachesDirectory stringByAppendingPathComponent:@"MIApplication/Crash"];
}

+(NSString *)applicationInformation
{
    
    NSString *string = @"";
    
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Application Name == %@",CApplicationName];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Bundle Identidier == %@",CBundleIdentifier];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Build Version == %@",CVersionNumber];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Build Number == %@",CBuildNumber];

    return string;
}

+(NSString *)timeZoneInfomration
{
    NSString *string = @"";

    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Timezone == %@\n%@\n%@\n%ld\n%f",[[NSTimeZone defaultTimeZone] name],[[NSTimeZone defaultTimeZone] description],[[NSTimeZone defaultTimeZone] abbreviation],(long)[[NSTimeZone defaultTimeZone] secondsFromGMT],[[NSTimeZone defaultTimeZone] daylightSavingTimeOffset]];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"Locale == %@",[NSLocale currentLocale]];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"24-Hour Clock == %@",[UIApplication uses24HourTimeCycle]?@"YES":@"NO"];
    string = [string stringByAppendingString:@"\n"];
    string = [string stringByAppendingFormat:@"CurrentTime == %@ --- %f",[NSDate date],[[NSDate date] timeIntervalSince1970]];

    return string;
}

+(NSString *)userDefaultsInfomration
{
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] dictionaryRepresentation]];
}




+(void)setDeveloperEmail:(NSString *)email
{
    [UIApplication setKeychain:email forKey:@"dEmail"];
}

+(NSString *)developerEmail
{
    return [UIApplication keychainForKey:@"dEmail"];
}

#pragma mark - Device Clock Information

+ (BOOL)uses24HourTimeCycle
{
    BOOL uses24HourTimeCycle = NO;
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSScanner *symbolScanner = [NSScanner scannerWithString:formatStringForHours];
    
    NSString *singleQuoteCharacterString = @"'";
    
    // look for single quote characters, ignore those and the enclosed strings
    while ( ![symbolScanner isAtEnd] ) {
        
        NSString *scannedString = @"";
        [symbolScanner scanUpToString:singleQuoteCharacterString intoString:&scannedString];
        
        // if 'H' or 'k' is found the locale uses 24 hour time cycle, and we can stop scanning
        if ( [scannedString rangeOfString:@"H"].location != NSNotFound ||
            [scannedString rangeOfString:@"k"].location != NSNotFound ) {
            
            uses24HourTimeCycle = YES;
            
            break;
        }
        
        // skip the single quote
        [symbolScanner scanString:singleQuoteCharacterString intoString:NULL];
        // skip everything up to and including the next single quote
        [symbolScanner scanUpToString:singleQuoteCharacterString intoString:NULL];
        [symbolScanner scanString:singleQuoteCharacterString intoString:NULL];
    }
    
    return uses24HourTimeCycle;
}

#pragma mark - General

+(UIImage *)screenshot
{
    return [UIViewController screenshot];
}

+(UIViewController *)topMostController
{
    UIViewController *topController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    while ([topController presentedViewController])	topController = [topController presentedViewController];
    
    return topController;
}

+(BOOL)isNewApplciaitonVersionInstalled
{
    return [[UIApplication sharedApplication] booleanForKey:@"newVersion"];
}

+(BOOL)isNewInstallation
{
    return [[UIApplication sharedApplication] booleanForKey:@"newInstallation"];
}

+(NSString*)applicationName
{
    return CApplicationName;
}

+(void)setApplicationThemeWithTintColor:(UIColor*)tintColor barTintColor:(UIColor*)barTintColor navigationBarFont:(UIFont*)font
{
    [[UINavigationBar appearance] setBarTintColor:barTintColor];
    [[UINavigationBar appearance] setTintColor:tintColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:tintColor}];
    
    [[UIToolbar appearance] setBarTintColor:barTintColor];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UIToolbar class]]] setTintColor:tintColor];
    
    [[UISwitch appearance] setOnTintColor:barTintColor];
    
    [[UISearchBar appearance] setBarTintColor:barTintColor];
    [[UISearchBar appearance] setTintColor:tintColor];
    
    [[UISegmentedControl appearance] setTintColor:barTintColor];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:tintColor} forState:UIControlStateNormal];
    
    [[UIActivityIndicatorView appearance] setColor:barTintColor];
    
    [[UISlider appearance] setMinimumTrackTintColor:barTintColor];
    [[UISlider appearance] setMaximumTrackTintColor:tintColor];
    [[UISlider appearance] setThumbTintColor:barTintColor];
    
    [[UIButton appearance] setTitleColor:tintColor forState:UIControlStateNormal];
    
    [[UIPageControl appearance] setPageIndicatorTintColor:barTintColor];
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor:tintColor];
    [[UIPageControl appearance] setBackgroundColor:[UIColor clearColor]];
    
    [[UIProgressView appearance] setProgressTintColor:tintColor];
    [[UIProgressView appearance] setTrackTintColor:barTintColor];
    
    [[UITabBar appearance] setTintColor:tintColor];
    [[UITabBar appearance] setBarTintColor:barTintColor];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:tintColor} forState:UIControlStateNormal];
    
    [[UITableView appearance] setSeparatorColor:barTintColor];
    [[UITableView appearance] setTintColor:barTintColor];
}


#pragma mark - AccessTokens

+(void)setKeychainObject:(id)value forKey:(NSString *)key
{
    if (value)
        [KeychainWrapper createKeychainObject:value forIdentifier:key];
}

+(id)keychainObjectForKey:(NSString *)key
{
    return [KeychainWrapper keychainDataFromMatchingIdentifier:key];
}


+(void)setKeychain:(NSString *)value forKey:(NSString *)key
{
    if (value)
        [KeychainWrapper createKeychainValue:value forIdentifier:key];
}

+(void)removeKeychainForKey:(NSString *)key
{
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:key];
}

+(NSString *)keychainForKey:(NSString *)key
{
   return [KeychainWrapper keychainStringFromMatchingIdentifier:key];
}





+(NSString *)uniqueIdentifier
{
    return [UIApplication keychainForKey:@"deviceUniqueIdentifier"];
}



+(NSString *)appID
{
    return [UIApplication keychainForKey:@"appID"];
}


+(NSString *)deviceToken
{
    return [UIApplication keychainForKey:@"deviceToken"];
}

+(void)setDeviceToken:(NSString *)deviceToken
{
    if (![[UIApplication deviceToken] isEqualToString:deviceToken])
        [UIApplication setKeychain:@"YES" forKey:@"isNewDeviceToken"];
    
    [UIApplication setKeychain:deviceToken forKey:@"deviceToken"];
}

+(void)removeDeviceToken
{
    [UIApplication removeKeychainForKey:@"deviceToken"];
}



+(NSString *)accessToken
{
    return [UIApplication keychainForKey:@"userAccessToken"];
}

+(void)setAccessToken:(NSString *)accessToken
{
    [UIApplication setKeychain:accessToken forKey:@"userAccessToken"];
}

+(void)removeAccessToken
{
    [UIApplication removeKeychainForKey:@"userAccessToken"];
}



+(NSString *)userId
{
    return [UIApplication keychainForKey:@"userID"];
}

+(void)setUserId:(id)userId
{
    [UIApplication setKeychain:userId forKey:@"userID"];
}

+(void)removeUserId
{
    [UIApplication removeKeychainForKey:@"userID"];
    
    if (NSClassFromString(@"MIAFNetworking"))
    {
        [[NSClassFromString(@"MIAFNetworking") performSelectorFromStringWithReturnObject:@"sharedInstance"] performSelectorFromString:@"cancelAllRequests"];
    }
    
    if (NSClassFromString(@"SocialNetworks"))
    {
        [[NSClassFromString(@"SocialNetworks") performSelectorFromStringWithReturnObject:@"sharedInstance"] performSelectorFromString:@"logoutFromAllSocialNetworks"];
    }
}



#pragma mark - Applicaiton Lifecycle Methods


+ (void)applicationDidReceiveMemoryWarning:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}

+ (void)applicationDidBecomeActive:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}

+ (void)applicationWillResignActive:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}

+ (void)applicationDidEnterBackground:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}

+ (void)applicationWillEnterForeground:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}

+ (void)applicationWillTerminate:(Block)block
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillTerminateNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        block();
    }];
}


//#pragma mark - EventKit
+(void)loadContactsWithDetails_completed:(void (^)(NSArray *contactArry , NSError *error))completion;
{
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    
    
    NSArray *keyToFetch = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey , CNContactEmailAddressesKey ];
    
    
//    NSArray *keyToFetch = @[CNContactNamePrefixKey, CNContactGivenNameKey , CNContactMiddleNameKey, CNContactFamilyNameKey, CNContactPreviousFamilyNameKey , CNContactNameSuffixKey, CNContactNicknameKey  , CNContactOrganizationNameKey   , CNContactDepartmentNameKey , CNContactJobTitleKey  , CNContactPhoneticGivenNameKey  , CNContactPhoneticMiddleNameKey , CNContactPhoneticFamilyNameKey , CNContactPhoneticOrganizationNameKey, CNContactBirthdayKey  , CNContactNonGregorianBirthdayKey    , CNContactNoteKey  , CNContactImageDataKey , CNContactThumbnailImageDataKey , CNContactImageDataAvailableKey , CNContactTypeKey  , CNContactPhoneNumbersKey   , CNContactEmailAddressesKey , CNContactPostalAddressesKey, CNContactDatesKey , CNContactUrlAddressesKey   , CNContactRelationsKey , CNContactSocialProfilesKey , CNContactInstantMessageAddressesKey ];
    
    CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keyToFetch]; //Contacts fetch request parrams object allocation
    fetchRequest.sortOrder = CNContactSortOrderGivenName;
    NSMutableArray *aryContcts = @[].mutableCopy;
    [contactStore enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        [aryContcts addObject:contact]; //add objects of all contacts list in array
        
    }];
    if(completion)
    {
        completion(aryContcts,nil);
    }
    
//    CNContactStore *store = [[CNContactStore alloc] init];
    
//    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
//        if (granted == YES)
//        {
//            //keys with fetching properties
//            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey , CNContactEmailAddressesKey];
//
//
//            NSString *containerId = store.defaultContainerIdentifier;
//            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
//
//            NSError *error;
//
//            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
//
//            if (error)
//            {
//                if(completion)
//                {
//                    completion(@[],error);
//                }
//
//            }
//            else
//            {
//                if(completion)
//                {
//                    completion(cnContacts,nil);
//                }
//            }
//        }
//        else
//        {
//            if(completion)
//            {
//                completion(@[],nil);
//            }
//
//        }
//    }];
}


@end
