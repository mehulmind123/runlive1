//
//  MIImageCropperViewController.m
//  Engage
//
//  Created by mac-0007 on 05/12/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MIImageCropperViewController.h"

#define SCALE_FRAME_Y 100.0f
#define BOUNDCE_DURATION 0.3f
#define LimitRatio      3.0f

@interface MIImageCropperViewController ()
{
    IBOutlet UIImageView *imgVSnap;
    
    IBOutlet UIImageView *imgVPhoto;
    
    IBOutlet UIView *viewOverlay;
    
    IBOutlet UIView *viewCropper;
    
    IBOutlet UIButton *btnCancel;
    
    IBOutlet UIButton *btnDone;
    
    BOOL isImageSizeValidation;
    
    UIImage *snapShot;
}

@property (nonatomic, strong) UIImage *originalImage;

@property (nonatomic, assign) CGRect minFrame;
@property (nonatomic, assign) CGRect maxFrame;
@property (nonatomic, assign) CGRect currentFrame;


@end

@implementation MIImageCropperViewController

- (id)initWithImage:(UIImage *)originalImage withDelegate:(id<MIImageCropperDelegate>)delegate ImageSizeValidation:(BOOL)isSizeValidation withViewController:(UIViewController *)viewController
{
    self = [super initWithNibName:@"MIImageCropperViewController" bundle:nil];
    
    if (self)
    {
        self.delegate = delegate;
        self.originalImage = [self fixOrientation:originalImage];
        
        isImageSizeValidation = isSizeValidation;
        snapShot = [self screenshotWithViewController:viewController];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Methods

- (void)initialize
{
   
    [imgVSnap setImage:snapShot];
    imgVPhoto.image = self.originalImage;
    
    viewCropper.layer.borderColor = [UIColor whiteColor].CGColor;
    viewCropper.layer.borderWidth = 2.0f;
    viewCropper.layer.masksToBounds = YES;
    
    //......Calculate dynamic frame of viewCropper.....
    CGFloat cWidth = (CScreenWidth * 359) / 375;
    CGFloat cHeight = (cWidth * 422) / 359;
    CGFloat cX = (CScreenWidth - cWidth) / 2;
//    CGFloat cY = ((CScreenHeight - cHeight) / 2) - 48;
    CGFloat cY = ((CScreenHeight - cHeight) / 2);
    
    //......Scale to fit the screen.....
    CGFloat oriWidth = cWidth;
    CGFloat oriHeight = (self.originalImage.size.height) * (cWidth / self.originalImage.size.width);
    CGFloat oriX = cX + (cWidth - oriWidth) / 2;
    CGFloat oriY = cY + (cHeight - oriHeight) / 2;
    
    self.minFrame = CGRectMake(oriX, oriY, oriWidth, oriHeight);
    self.maxFrame = CGRectMake(0, 0, LimitRatio * self.minFrame.size.width, LimitRatio * self.minFrame.size.height);
    
    self.currentFrame = self.minFrame;
    imgVPhoto.frame = self.minFrame;
    
    
    //.....AddPinchGesture.....
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [self.view addGestureRecognizer:pinchGestureRecognizer];
    
    //.....AddPanGesture.....
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    [self.view addGestureRecognizer:panGestureRecognizer];
    
    
    
    //.....ClearCroppingView Area.....
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathAddRect(path, nil, CGRectMake(0, 0, cX, CScreenHeight));
    CGPathAddRect(path, nil, CGRectMake(cX + cWidth, 0, CScreenWidth - cX - cWidth, CScreenHeight));
    CGPathAddRect(path, nil, CGRectMake(0, 0, CScreenWidth, cY));
    CGPathAddRect(path, nil, CGRectMake(0, cY + cHeight, CScreenWidth, CScreenHeight - cY + cHeight));
    
    maskLayer.path = path;
    viewOverlay.layer.mask = maskLayer;
    CGPathRelease(path);
}

- (CGRect)handleScaleOverflow:(CGRect)newFrame
{
    //.....Bounce To Original Frame.....
    CGPoint oriCenter = CGPointMake(newFrame.origin.x + newFrame.size.width/2, newFrame.origin.y + newFrame.size.height/2);
    
    if (newFrame.size.width < self.minFrame.size.width)
        newFrame = self.minFrame;
    
    if (newFrame.size.width > self.maxFrame.size.width)
        newFrame = self.maxFrame;
    
    newFrame.origin.x = oriCenter.x - newFrame.size.width/2;
    newFrame.origin.y = oriCenter.y - newFrame.size.height/2;
    return newFrame;
}

- (CGRect)handleBorderOverflow:(CGRect)newFrame
{
    //.....Horizontally.....
    if (newFrame.origin.x > viewCropper.frame.origin.x)
        newFrame.origin.x = viewCropper.frame.origin.x;
    if (CGRectGetMaxX(newFrame) < viewCropper.frame.origin.x + viewCropper.frame.size.width)
        newFrame.origin.x = viewCropper.frame.origin.x + viewCropper.frame.size.width - newFrame.size.width;
    
    
    //.....Vertically.....
    if (newFrame.origin.y > viewCropper.frame.origin.y)
        newFrame.origin.y = viewCropper.frame.origin.y;
    if (CGRectGetMaxY(newFrame) < viewCropper.frame.origin.y + viewCropper.frame.size.height)
        newFrame.origin.y = viewCropper.frame.origin.y + viewCropper.frame.size.height - newFrame.size.height;
    
    
    
    //.....Adapt Horizontally Rectangle.....
    if (imgVPhoto.frame.size.width > imgVPhoto.frame.size.height &&
        newFrame.size.height <= viewCropper.frame.size.height)
    {
        newFrame.origin.y = viewCropper.frame.origin.y + (viewCropper.frame.size.height - newFrame.size.height) / 2;
    }
    return newFrame;
}

-(UIImage *)cropImage
{
    CGRect squareFrame = viewCropper.frame;
    CGFloat scaleRatio = self.currentFrame.size.width / self.originalImage.size.width;
    
    CGFloat x = (squareFrame.origin.x - self.currentFrame.origin.x) / scaleRatio;
    CGFloat y = (squareFrame.origin.y - self.currentFrame.origin.y) / scaleRatio;
    CGFloat w = squareFrame.size.width / scaleRatio;
    CGFloat h = squareFrame.size.height / scaleRatio;
    
    /**
     * For Crop image according to ratio
     
     if (self.currentFrame.size.width < viewCropper.frame.size.width)
     {
     CGFloat newW = self.originalImage.size.width;
     CGFloat newH = newW * (viewCropper.frame.size.height / viewCropper.frame.size.width);
     x = 0;
     y = y + (h - newH) / 2;
     w = newW; h = newH;
     }
     
     if (self.currentFrame.size.height < viewCropper.frame.size.height)
     {
     CGFloat newH = self.originalImage.size.height;
     CGFloat newW = newH * (viewCropper.frame.size.width / viewCropper.frame.size.height);
     x = x + (w - newW) / 2;
     y = 0;
     w = newW; h = newH;
     }
     
     */
    
    CGRect myImageRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = self.originalImage.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    
    CGSize size;
    size.width = myImageRect.size.width;
    size.height = myImageRect.size.height;
    
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    CGImageRelease(subImageRef);
    UIGraphicsEndImageContext();
    return smallImage;
}


-(UIImage *)screenshotWithViewController:(UIViewController *)viewController
{
    CGRect  rect = CGRectMake(0, 0, CScreenWidth, CScreenHeight);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [viewController.view.window.layer renderInContext:context];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


#pragma mark -
#pragma mark - Action Events

- (IBAction)btnCancelClicked:(UIButton *)sender
{
    if (self.delegate)
        [self.delegate finish:nil didCancel:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnDoneClicked:(UIButton *)sender
{
    UIImage *imgTemp = [self cropImage];
    CGFloat scaleRatio = self.currentFrame.size.width / self.originalImage.size.width;
    
    if ((imgTemp.size.width*scaleRatio +10 < CViewWidth(viewCropper) && isImageSizeValidation) || (imgTemp.size.height*scaleRatio +10 < CViewHeight(viewCropper) && isImageSizeValidation))
    {
        [self customAlertViewWithOneButton:@"" Message:@"To continue, please make sure your picture fills up the whole square 😊" ButtonText:@"OK" Animation:YES completed:nil];
    }
    else
    {
        if (self.delegate)
            [self.delegate finish:[self cropImage] didCancel:NO];
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}



#pragma mark -
#pragma mark - UIGestureRecognizer

- (void)pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    UIView *view = imgVPhoto;
    
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan ||
        pinchGestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
    else if (pinchGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGRect newFrame = imgVPhoto.frame;
        newFrame = [self handleScaleOverflow:newFrame];
        newFrame = [self handleBorderOverflow:newFrame];
        
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            imgVPhoto.frame = newFrame;
            self.currentFrame = newFrame;
        }];
    }
}

- (void)panView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UIView *view = imgVPhoto;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan ||
        panGestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        //.....Calculate Accelerator.....
        CGFloat absCenterX = viewCropper.frame.origin.x + viewCropper.frame.size.width / 2;
        CGFloat absCenterY = viewCropper.frame.origin.y + viewCropper.frame.size.height / 2;
        CGFloat scaleRatio = imgVPhoto.frame.size.width / viewCropper.frame.size.width;
        CGFloat acceleratorX = 1 - ABS(absCenterX - view.center.x) / (scaleRatio * absCenterX);
        CGFloat acceleratorY = 1 - ABS(absCenterY - view.center.y) / (scaleRatio * absCenterY);
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [view setCenter:(CGPoint){view.center.x + translation.x * acceleratorX, view.center.y + translation.y * acceleratorY}];
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }
    else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        //.....Bounce To Original Frame.....
        CGRect newFrame = imgVPhoto.frame;
        newFrame = [self handleBorderOverflow:newFrame];
        
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            imgVPhoto.frame = newFrame;
            self.currentFrame = newFrame;
        }];
    }
}



#pragma mark -
#pragma mark - Fix Orientation

- (UIImage *)fixOrientation:(UIImage *)srcImg
{
    if (srcImg.imageOrientation == UIImageOrientationUp) return srcImg;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, srcImg.size.width, srcImg.size.height,
                                             CGImageGetBitsPerComponent(srcImg.CGImage), 0,
                                             CGImageGetColorSpace(srcImg.CGImage),
                                             CGImageGetBitmapInfo(srcImg.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (srcImg.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.height,srcImg.size.width), srcImg.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.width,srcImg.size.height), srcImg.CGImage);
            break;
    }
    
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}
@end
