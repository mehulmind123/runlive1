//
//  ActivityUserCell.m
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityUserCell.h"
#import "FacebookCell.h"
#import "InviteSearchCell.h"

@implementation ActivityUserCell
{
    NSMutableArray *arrUser;
    NSMutableArray *arrSearchUser;
    
    NSURLSessionDataTask *taskRuning;
    BOOL isApiStarted;
    int iOffset;
    BOOL dragging;
    CGFloat lastScrollOffset;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [tblSearch registerNib:[UINib nibWithNibName:@"InviteSearchCell" bundle:nil] forCellReuseIdentifier:@"InviteSearchCell"];
    [clFreinds registerNib:[UINib nibWithNibName:@"FacebookCell" bundle:nil] forCellWithReuseIdentifier:@"FacebookCell"];
    
    viewSearchBar.layer.cornerRadius = 3;
 
    [txtSearch setValue:CRGB(146, 150, 163) forKeyPath:@"_placeholderLabel.textColor"];
    [txtSearch addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    arrUser = [NSMutableArray new];
    arrSearchUser = [NSMutableArray new];
    
    
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:@"None of your Facebook friends are on RunLive, Tap here to share us on Facebook!"];
    [attrString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:(NSRange){46,8}];
    lblNoFacebookFrnd.attributedText = attrString;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self btnTapBarCLk:btnPhone];
    });
}

#pragma mark - TextField Delegate Methods
-(void)textfieldDidChange:(UITextField *)txt
{
    viewFBNoDataFound.hidden = viewPhoneNoDataFound.hidden = lblSearchNoData.hidden = YES;
    
    if (taskRuning && taskRuning.state == NSURLSessionTaskStateRunning)
        [taskRuning cancel];

    iOffset = 0;
    
    if (txtSearch.text.length == 0)
    {
        tblSearch.hidden = YES;
        [txtSearch resignFirstResponder];
    }
    else
    {
        tblSearch.hidden = NO;
        [self searchUserByKeywaord];
    }
}

#pragma mark - General Method
- (void)LoadMoreDataFromServer
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self searchUserByKeywaord];
}

-(void)searchUserByKeywaord
{
    isApiStarted = YES;
    
    if (iOffset == 0)
    {
        [arrSearchUser removeAllObjects];
        [tblSearch reloadData];
    }
    
    
    taskRuning = [[APIRequest request] serachUser:[NSNumber numberWithInt:iOffset] KeyWord:txtSearch.text completed:^(id responseObject, NSError *error)
                  {
                      isApiStarted = NO;
                      if (responseObject && !error)
                      {
                          if (iOffset == 0)
                              [arrSearchUser removeAllObjects];
                          
                          iOffset = [responseObject stringValueForJSON:@"offset"].intValue;
                          
                          [arrSearchUser addObjectsFromArray:[responseObject valueForKey:CJsonData]];
                          [tblSearch reloadData];
                          
                          lblSearchNoData.hidden = arrSearchUser.count > 0;
                      }
                  }];
}

-(void)syncContactWithServer:(BOOL)isFacebook
{
    [[APIRequest request] syncContactWithServer:arrUser FromFacebook:isFacebook completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSLog(@"Check Contanct response here =========== >%@",responseObject);
             
             [arrUser removeAllObjects];
             [clFreinds reloadData];
             
             if (isFacebook)
             {
                 NSArray *arrFaceTemp = [[responseObject valueForKey:CJsonData] valueForKey:@"social_contact"];
                 for (int i = 0; arrFaceTemp.count > i; i++)
                 {
                     NSDictionary *dicData = arrFaceTemp[i];
                     if (![[dicData stringValueForJSON:@"_id"] isEqualToString:@""])
                         [arrUser addObject:dicData];
                 }
                 viewFBNoDataFound.hidden = arrUser.count > 0;
             }
             else
             {
                 [arrUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"phone_contact"]];
                 viewPhoneNoDataFound.hidden = arrUser.count > 0;
             }
             
             [clFreinds reloadData];
         }
         else
         {
             NSLog(@"Contanct failed response here =========== >");
         }
     }];
    
}

-(void)fetchLocalContact
{
    /*
     "email": "abc@gmail.com",
     "_id": "",
     "user_name": "",
     "fb_id": "",
     "first_name": "",
     "last_name": "",
     "picture": "",
     "country": "",
     "number": "123456789"
     */
    
    [arrUser removeAllObjects];
    [clFreinds reloadData];
    
    
    btnFacebook.userInteractionEnabled = btnFacebook.userInteractionEnabled = NO;
    [UIApplication loadContactsWithDetails_completed:^(NSArray* objects , NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            btnFacebook.userInteractionEnabled = btnFacebook.userInteractionEnabled = YES;
            
            [arrUser removeAllObjects];
            
            for (int i = 0; objects.count > i; i++)
            {
                NSMutableDictionary *dicDataForAPi = [[NSMutableDictionary alloc] init];
                CNContact *contactPerson = [objects objectAtIndex:i];
                
                if (contactPerson.givenName)
                    [dicDataForAPi setObject:contactPerson.givenName forKey:@"first_name"];
                else
                    [dicDataForAPi setObject:@"" forKey:@"first_name"];
                
                if (contactPerson.familyName)
                    [dicDataForAPi setObject:contactPerson.familyName forKey:@"last_name"];
                else
                    [dicDataForAPi setObject:@"" forKey:@"last_name"];
                
                
                NSArray *arrEmail = contactPerson.emailAddresses;
                NSArray *arrPhones = contactPerson.phoneNumbers;
                
                if (arrEmail.count > 0)
                {
                    CNLabeledValue *email = [arrEmail objectAtIndex:0];
                    
                    NSString *strEmail = email.value;
                    if (![strEmail isEqualToString:appDelegate.loginUser.email])
                        [dicDataForAPi setObject:strEmail forKey:@"email"];
                    
                }
                else
                    [dicDataForAPi setObject:@"" forKey:@"email"];
                
                if (arrPhones.count > 0)
                {
                    CNLabeledValue * phoneNumber = [arrPhones objectAtIndex:0];
                    
                    NSString * number = ((CNPhoneNumber *)phoneNumber.value).stringValue;
                    
                    NSString * strippedNumber = [number stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [number length])];
                    [dicDataForAPi setObject:strippedNumber forKey:@"number"];
                }
                else
                    [dicDataForAPi setObject:@"" forKey:@"number"];
                
                [dicDataForAPi setObject:@"" forKey:@"_id"];
                [dicDataForAPi setObject:@"" forKey:@"user_name"];
                [dicDataForAPi setObject:@"" forKey:@"country"];
                [dicDataForAPi setObject:@"" forKey:@"fb_id"];
                
                [arrUser addObject:dicDataForAPi];
            }
            
            [self syncContactWithServer:NO];
            
        });
    }];
    
}

-(void)fetchFriendFromFacebook
{
    [arrUser removeAllObjects];
    [clFreinds reloadData];
    
    btnPhone.userInteractionEnabled = btnFacebook.userInteractionEnabled = NO;
    
    [[SocialNetworks sharedInstance] fetchFriendsForSocialNetwork:SocialNetworkFacebook success:^(NSDictionary *result)
     {
         btnPhone.userInteractionEnabled = btnFacebook.userInteractionEnabled = YES;
         
         if (result)
         {
             NSLog(@"Friend list from facebook =========>  %@",result);
             [arrUser removeAllObjects];
             
             NSArray *arrTemp = [result valueForKey:CJsonData];
             
             for (int i = 0; arrTemp.count > i; i++)
             {
                 NSDictionary *dicData = arrTemp[i];
                 NSMutableDictionary *dicTemp = [NSMutableDictionary new];
                 [dicTemp addObject:@"" forKey:@"email"];
                 [dicTemp addObject:@"" forKey:@"_id"];
                 [dicTemp addObject:@"" forKey:@"user_name"];
                 [dicTemp addObject:[dicData stringValueForJSON:@"id"] forKey:@"fb_id"];
                 [dicTemp addObject:@"" forKey:@"first_name"];
                 [dicTemp addObject:@"" forKey:@"last_name"];
                 [dicTemp addObject:@"" forKey:@"picture"];
                 [dicTemp addObject:@"" forKey:@"country"];
                 [dicTemp addObject:@"" forKey:@"number"];
                 [arrUser addObject:dicTemp];
             }
             
             [self syncContactWithServer:YES];
         }
     } failure:^(NSError *error)
     {
         btnPhone.userInteractionEnabled = btnFacebook.userInteractionEnabled = YES;
     }];
}


#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSearchUser.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"InviteSearchCell";
    InviteSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
        cell = [[InviteSearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.viewSelect.hidden = YES;

    NSDictionary *dicData = [arrSearchUser objectAtIndex:indexPath.item];
    
    cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
    
    [cell.imgFreind setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    txtSearch.text = nil;
    tblSearch.hidden = YES;

    NSDictionary *dicData = [arrSearchUser objectAtIndex:indexPath.item];
    
    if (self.configureInviteUserDidSelect)
        self.configureInviteUserDidSelect(dicData,YES);

}

#pragma mark - ScrollView delegate methods
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)ltargetContentOffset
{
    dragging = NO;
    
    if (ltargetContentOffset->y>=scrollView.contentSize.height-240-scrollView.frame.size.height && ltargetContentOffset->y<scrollView.contentOffset.y)
        [self LoadMoreDataFromServer];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.isDragging && sender.contentOffset.y>=sender.contentSize.height-240-sender.frame.size.height && lastScrollOffset<sender.contentOffset.y && !dragging)
    {
        dragging = YES;
        [self LoadMoreDataFromServer];
    }
    
    lastScrollOffset = sender.contentOffset.y;
}


#pragma mark - Collection View Delegate
#pragma mark -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrUser.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FacebookCell";
    FacebookCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (btnPhone.selected)
    {
        NSDictionary *dicData = [arrUser objectAtIndex:indexPath.item];
        
        if (![[dicData stringValueForJSON:@"first_name"] isEqualToString:@""])
            cell.lblName.text = [NSString stringWithFormat:@"%@",[dicData stringValueForJSON:@"first_name"]];
        else
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
        
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];

        if (![[dicData stringValueForJSON:@"picture"] isEqualToString:@""] && [dicData objectForKey:@"picture"])
            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        else
            cell.imgUser.image = [UIImage imageNamed:@"default_avatar"];
        
        if ([dicData objectForKey:@"celebrity"])
            cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        else
            cell.imgVerifiedUser.hidden = YES;
        
    }
    else
    {
        // Load facebook Data here....
        NSDictionary *dicData = [arrUser objectAtIndex:indexPath.item];
        
        if (![[dicData stringValueForJSON:@"first_name"] isEqualToString:@""])
            cell.lblName.text = [NSString stringWithFormat:@"%@",[dicData stringValueForJSON:@"first_name"]];
        else
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
        
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        
        if (![[dicData stringValueForJSON:@"picture"] isEqualToString:@""])
            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        else
            cell.imgUser.image = [UIImage imageNamed:@"default_avatar"];
        
        if ([dicData objectForKey:@"celebrity"])
            cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        else
            cell.imgVerifiedUser.hidden = YES;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth/4 , 120);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *dicData = [arrUser objectAtIndex:indexPath.item];
    
    if (self.configureInviteUserDidSelect)
        self.configureInviteUserDidSelect(dicData,btnFacebook.selected);
    
}

#pragma mark - Action Event

-(IBAction)btnTapBarCLk:(UIButton *)sender
{
    viewFBNoDataFound.hidden = viewPhoneNoDataFound.hidden = lblSearchNoData.hidden = YES;
    
    if (sender.tag == 0)
    {
        if (btnFacebook.selected)
            return;
        
        btnFacebook.selected = !btnFacebook.selected;
        viewPhone.hidden = YES;
        
         btnPhone.selected = NO;
        viewFacebbok.hidden = NO;
        
        [self fetchFriendFromFacebook];
    }
    else
    {
        if (btnPhone.selected)
            return;
        
        btnPhone.selected = !btnPhone.selected;
        viewPhone.hidden = NO;
        
        btnFacebook.selected = NO;
        viewFacebbok.hidden = YES;
        
        [self fetchLocalContact];
    }
    
}

@end
