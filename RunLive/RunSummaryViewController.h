//
//  RunSummaryViewController.h
//  RunLive
//
//  Created by mac-0006 on 17/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface RunSummaryViewController : SuperViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, HPGrowingTextViewDelegate,AVSpeechSynthesizerDelegate>
{
    
    IBOutlet MGLMapView *mapView;
    
    IBOutlet UILabel *lblRunSessionName;
    IBOutlet UILabel *lblPosition;
    IBOutlet UILabel *lblPoint;
    IBOutlet UILabel *lblPointstxt;
    IBOutlet UILabel *lblWinnerTeamName,*lblWinner;
    IBOutlet UILabel *lblTime,*lblTimeLeft,*lblTotalComment;
    
    IBOutlet UILabel *lblScoreboard,*lblSoloScoreboard;
    IBOutlet UIActivityIndicatorView *activityIndicatorResultPending;
    IBOutlet UIButton *btnRun,*btnSend,*btnShare;
    IBOutlet UIButton *btnComments;
    
    IBOutlet UIView *viewRun;
    IBOutlet UIView *viewLineRun;
    IBOutlet UIView *viewLineComment;
    IBOutlet UIView *viewComment,*viewResultPending;
    
    IBOutlet UIScrollView *scrollRunView;
    IBOutlet HPGrowingTextView *txtComment;
    
    IBOutlet NSLayoutConstraint *txtCommentHeight,*tblpeopleHeight,*scrollIndY,*scrollIndheight;
    
    IBOutlet UICollectionView *collActivity;
    IBOutlet UITableView *tblScore;
    IBOutlet UITableView *tblComments,*tblPeopleList;
    IBOutlet NSLayoutConstraint *cnTblHeight;
    IBOutlet UIView *viewScrollIndicator,*ViewScrollSide;
    IBOutlet UIView *viewTeamTitle;
    

    IBOutlet UICollectionView *clRunnerPositionl;
}

@property(atomic,assign) BOOL isFromNotification,isNotificationComment,isRunnerTimerOut;
@property(strong,nonatomic) NSString *strResultSessionId;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnShareClicked:(id)sender;
- (IBAction)btnTabbarClicked:(UIButton *)sender;
- (IBAction)btnSendClicked:(id)sender;

@end
