//
//  SessionTypeEvenUser.h
//  RunLive
//
//  Created by mac-00012 on 11/19/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionTypeEvenUser : UIView

@property(weak,nonatomic) IBOutlet UIButton *btnGoBack,*btnProceed;

@end
