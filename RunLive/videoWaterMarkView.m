//
//  videoWaterMarkView.m
//  RunLive
//
//  Created by mac-0005 on 17/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "videoWaterMarkView.h"

@implementation videoWaterMarkView

+ (id)initVideoWaterMarkView:(CGSize)size
{
    videoWaterMarkView *objVideoWaterMark = [[[NSBundle mainBundle]loadNibNamed:@"videoWaterMarkView" owner:nil options:nil] lastObject];
    
    CViewSetWidth(objVideoWaterMark, size.width);
    
    float height = (size.width *17.34)/100;
    
    if (height < 65)
        height = 65;
    
    CViewSetHeight(objVideoWaterMark, height);
    CViewSetX(objVideoWaterMark, 0);
    CViewSetY(objVideoWaterMark, size.height - height);
    
    return objVideoWaterMark;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}


#pragma mark - Comfiguration
#pragma mark -
-(void)setDataForWaterMark:(NSDictionary *)dicData
{
    lblVideoTotalDistance.text = [dicData stringValueForJSON:@"distance"];
    lblVideoMovingTime.text = [dicData stringValueForJSON:@"time"];
    lblVideoPace.text = [dicData stringValueForJSON:@"pace"];
    lblVideoRank.text = [dicData stringValueForJSON:@"rank"];
}

@end
