//
//  QuitRunSession.m
//  RunLive
//
//  Created by mac-00012 on 11/18/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "QuitRunSession.h"

@implementation QuitRunSession

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.btnYes.layer.cornerRadius = self.btnNo.layer.cornerRadius = 3;
    self.center = CScreenCenter;
}


@end
