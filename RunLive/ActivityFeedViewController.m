//
//  ActivityFeedViewController.m
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityFeedViewController.h"
#import "ActivityMainCell.h"
#import "ActivityUserCell.h"
#import "ActivityMessageCell.h"
#import "ActivityDetailViewController.h"
#import "SettingViewController.h"
#import "CommentsViewController.h"
#import "MSGSelectFriendsViewController.h"
#import "ChatViewController.h"
#import "MyProfileViewController.h"
#import "SessionDetailsViewController.h"

@interface ActivityFeedViewController ()

@end

@implementation ActivityFeedViewController
{
    NSIndexPath *selectedIndexPath;
    
    BOOL isViewControllerVisible;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    
    selectedIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    
    [clActivityMain registerNib:[UINib nibWithNibName:@"ActivityMainCell" bundle:nil] forCellWithReuseIdentifier:@"ActivityMainCell"];
    [clActivityMain registerNib:[UINib nibWithNibName:@"ActivityMessageCell" bundle:nil] forCellWithReuseIdentifier:@"ActivityMessageCell"];
    [clActivityMain registerNib:[UINib nibWithNibName:@"ActivityUserCell" bundle:nil] forCellWithReuseIdentifier:@"ActivityUserCell"];

    
    
#pragma mark - User Name CLK
    appDelegate.configureBtnUserNameSelect = ^(NSString *strUserName,BOOL isLoginUser)
    {
        [appDelegate moveOnProfileScreen:strUserName UserId:@"" andNavigation:self.navigationController isLoginUser:isLoginUser];
    };

#pragma mark - Refresh chat Screen from Push notifcation
    appDelegate.configureRefreshChatListScreen = ^(BOOL isRefresh)
    {
        dispatch_async(GCDMainThread, ^{
            selectedIndexPath = [NSIndexPath indexPathForItem:2 inSection:0];
            [clActivityMain scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        });

    };
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [appDelegate showTabBar];
    cnCLActivityBottomSpace.constant = CScreenHeight - CViewY(appDelegate.objCustomTabBar);
    
    [self.navigationController setNavigationBarHidden:YES];
    [self hideShowStatusBar:NO];
    
    isViewControllerVisible = YES;
    
    if (appDelegate.isShowActivityScreen)
    {
        // To show Activity View for the first time....
//        appDelegate.isShowActivityScreen = NO;
//        [clActivityMain setContentOffset:CGPointMake(CScreenWidth, 0) animated:NO];
//        [clActivityMain reloadData];
    }
    else
    {
        // Show selected view here......
        [clActivityMain scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        [clActivityMain reloadData];
        clActivityMain.hidden = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /*
    if (appDelegate.isShowActivityScreen)
    {
        appDelegate.isShowActivityScreen = NO;
        [clActivityMain setContentOffset:CGPointMake(CScreenWidth, 0) animated:NO];
        [clActivityMain reloadData];
    }
     */
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (appDelegate.isShowActivityScreen)
    {
        appDelegate.isShowActivityScreen = NO;
        [clActivityMain setContentOffset:CGPointMake(CScreenWidth, 0) animated:NO];
        [clActivityMain reloadData];
    }
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        [clActivityMain setContentOffset:CGPointMake(CScreenWidth, 0) animated:NO];
//    });
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    isViewControllerVisible = NO;
    
    [self stopVideoInActivityFeed];
}

#pragma mark - Helper function

-(void)stopVideoInActivityFeed
{
    if (self.configureStopPlayVideo)
        self.configureStopPlayVideo(YES);

//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Collection View Delegate
#pragma mark -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.item)
    {
#pragma mark - Activity User Cell
        case 0:
        {
            static NSString *identifier = @"ActivityUserCell";
            ActivityUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            
            [cell.btnUserBack touchUpInsideClicked:^{
                selectedIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
                [clActivityMain scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            }];
            
            cell.configureInviteUserDidSelect = ^(NSDictionary *dicData,BOOL isFacebook)
            {
                if (![[dicData stringValueForJSON:@"_id"] isEqualToString:@""])
                {
                    [appDelegate moveOnProfileScreen:[dicData stringValueForJSON:@"user_name"] UserId:[dicData stringValueForJSON:@"_id"] andNavigation:self.navigationController isLoginUser:NO];
                }
                else
                {
                    if (isFacebook)
                        return ;
                    
                    if ([[dicData valueForKey:@"email"] isEqualToString:@""] && [[dicData valueForKey:@"number"] isEqualToString:@""])
                    {
                        // If only name OR photo is there: Open SMS composer with no number populated.
                        [self openMessageComposer:nil recepients:nil body:[NSString stringWithFormat:@"Hey %@ , I want you to checkout this new app called RunLive. You can race against people and win points that you can use to get free stuff!!\n%@",[dicData stringValueForJSON:@"first_name"],CAppStoreUrl]];
                    }
                    else if (![[dicData valueForKey:@"email"] isEqualToString:@""] && [[dicData valueForKey:@"number"] isEqualToString:@""])
                    {
                        // If only email is there: Open Email composer with email populated in TO field.
                        [self openMailComposer:nil recepients:@[[dicData valueForKey:@"email"]] body:nil isHTML:NO];
                    }
                    else if ([[dicData valueForKey:@"email"] isEqualToString:@""] && ![[dicData valueForKey:@"number"] isEqualToString:@""])
                    {
                        //If only phone number is there: Open SMS composer with number populated.
                        [self openMessageComposer:nil recepients:@[[dicData valueForKey:@"number"]] body:[NSString stringWithFormat:@"Hey %@ , I want you to checkout this new app called RunLive. You can race against people and win points that you can use to get free stuff!!\n%@",[dicData stringValueForJSON:@"first_name"],CAppStoreUrl]];
                    }
                    else if (![[dicData valueForKey:@"email"] isEqualToString:@""] && ![[dicData valueForKey:@"number"] isEqualToString:@""])
                    {
                        // If phone and email both is there: Open action sheet with options to invite through SMS OR through Email.
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Sharing option:" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                        
                        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }]];
                        
                        [alertController addAction:[UIAlertAction actionWithTitle:@"Invite through SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            [self openMessageComposer:nil recepients:@[[dicData valueForKey:@"number"]] body:[NSString stringWithFormat:@"Hey %@ , I want you to checkout this new app called RunLive. You can race against people and win points that you can use to get free stuff!!\n%@",[dicData stringValueForJSON:@"first_name"],CAppStoreUrl]];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }]];
                        
                        [alertController addAction:[UIAlertAction actionWithTitle:@"Invite through Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            [self openMailComposer:nil recepients:@[[dicData valueForKey:@"email"]] body:nil isHTML:NO];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }]];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
            };
            
            [cell.btnFacebookShare touchUpInsideClicked:^{
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                [mySLComposerSheet setInitialText:CAppStoreUrl];
                [mySLComposerSheet addImage:[UIImage imageNamed:@"socail_share"]];
                //        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://stackoverflow.com/questions/12503287/tutorial-for-slcomposeviewcontroller-sharing"]];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
                 {
                     switch (result)
                     {
                         case SLComposeViewControllerResultCancelled:
                             NSLog(@"Post Canceled");
                             break;
                         case SLComposeViewControllerResultDone:
                             NSLog(@"Post Sucessful");
                             break;
                             
                         default:
                             break;
                     }
                 }];
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
                //}

            }];
            
            return cell;
        }
            break;
#pragma mark - Activity Main Cell
        case 1:
        {
            static NSString *identifier = @"ActivityMainCell";
            ActivityMainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

            [cell.btnMyProfile touchUpInsideClicked:^{
                MyProfileViewController *objPro = [[MyProfileViewController alloc] init];
                objPro.isBackButton = YES;
                [self.navigationController pushViewController:objPro animated:YES];
            }];
            
            [cell.btnUser touchUpInsideClicked:^{
                [self stopVideoInActivityFeed];
                selectedIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                [clActivityMain scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            }];
            
            [cell.btnMessage touchUpInsideClicked:^{
                [self stopVideoInActivityFeed];
                selectedIndexPath = [NSIndexPath indexPathForItem:2 inSection:0];
                [clActivityMain scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            }];
            

// To Stop Play video here.........
        self.configureStopPlayVideo = ^(BOOL isStop)
            {
                if (isStop && cell.cellActivityVideo)
                {
                        NSLog(@"stop Video here ==========>>>>>  ");
                        if ([cell.cellActivityVideo isKindOfClass:[ActivityAddPhotoCell class]])
                        {
                            [cell.cellActivityVideo StopVideoPlaying];
                            cell.cellActivityVideo = nil;
                        }
                    }
            };
            
#pragma mark - Activity Main Did Select
            cell.configureActivityDidSelect = ^(NSDictionary *dicUser, BOOL isLeaderBoard,NSManagedObject *objCoreData)
            {
                if (isLeaderBoard)
                {
                    [appDelegate moveOnProfileScreen:[dicUser stringValueForJSON:@"user_name"] UserId:[dicUser stringValueForJSON:@"_id"] andNavigation:self.navigationController isLoginUser:NO];
                }
                else
                {
                    if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
                    {
                        TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
                        ActivityDetailViewController *objAct = [[ActivityDetailViewController alloc] init];
                        objAct.strActivityId = objActCom.activity_id;
                        objAct.strResultSessionId = objActCom.session_id;
                        [self.navigationController pushViewController:objAct animated:YES];
                    }
                }
            };

#pragma mark - Activity Add Photo Comment Comments
            cell.configureBtnCommentCLK = ^(NSString *strActivityId, NSString *strActType)
            {
                if (!strActivityId)
                    return ;
                
                CommentsViewController *objCom = [[CommentsViewController alloc] init];
                objCom.strActivityId = strActivityId;
                objCom.strActivityType = strActType;
                [self.navigationController pushViewController:objCom animated:YES];
            };
            return cell;
        }
            break;
#pragma mark - Activity Message Cell
        case 2:
        {
            static NSString *identifier = @"ActivityMessageCell";
            ActivityMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            
            NSLog(@"Called chat list screen api here ============ >>> ");
            
            [cell getChatUserList:self FirstTime:YES];
            
            [cell.btnMessageBack touchUpInsideClicked:^{
                selectedIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
                [clActivityMain scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            }];

#pragma mark - Activity Message Did Select
            cell.configureActivityMessageDidSelect = ^(TblChats *chat)
            {
                ChatViewController *objChat = [[ChatViewController alloc] init];
                objChat.isFromMSGScreen = YES;
                objChat.strChannelId = chat.channelid;
                objChat.chatObject = chat;
                [self.navigationController pushViewController:objChat animated:YES];
            };

            cell.configureBtnNewMSGCLK = ^(NSString *btnCLK)
            {
                MSGSelectFriendsViewController *objMSG = [[MSGSelectFriendsViewController alloc] init];
                [self.navigationController pushViewController:objMSG animated:YES];
            };
            
            // Refersh data when ever new message will come...........
                appDelegate.configureGotMessage = ^(NSDictionary *dicData)
                {
                    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
                    {
                        if (appDelegate.loginUser && isViewControllerVisible)
                            [cell getChatUserList:self FirstTime:NO];
                    }
                };
            
            return cell;
        }
            break;
        default:
            break;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth , CGRectGetHeight(clActivityMain.frame));
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
    selectedIndexPath = [NSIndexPath indexPathForItem:page inSection:0];
    [appDelegate hideKeyboard];
    [self stopVideoInActivityFeed];
}


@end
