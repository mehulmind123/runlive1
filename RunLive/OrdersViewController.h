//
//  OrdersViewController.h
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersViewController : SuperViewController
{
    IBOutlet UITableView *tblOrder;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}
@end
