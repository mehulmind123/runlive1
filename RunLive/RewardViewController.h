//
//  RewardViewController.h
//  CollectionViewDemo
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardViewController : SuperViewController
{
    IBOutlet UICollectionView *clProduct;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UILabel *lblUserName,*lblTotalPoints;
    IBOutlet UIActivityIndicatorView *activityIndicatorView;
    IBOutlet NSLayoutConstraint *cnPageControllerBottomSpace;
    
    
}
@end
