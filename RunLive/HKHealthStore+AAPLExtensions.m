/*
    Copyright (C) 2014 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    
                Contains shared helper methods on HKHealthStore that are specific to Fit's use cases.
            
*/

#import "HKHealthStore+AAPLExtensions.h"

@implementation HKHealthStore (AAPLExtensions)
//
//- (void)aapl_mostRecentQuantitySampleOfType:(HKQuantityType *)quantityType predicate:(NSPredicate *)predicate completion:(void (^)(HKQuantity *, NSError *))completion
//{
//    NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:NO];
//    
//    // Since we are interested in retrieving the user's latest sample, we sort the samples in descending order, and set the limit to 1. We are not filtering the data, and so the predicate is set to nil.
//    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:quantityType predicate:nil limit:1 sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error)
//    {
//        if (!results)
//        {
//            if (completion)
//                completion(nil, error);
//            
//            return;
//        }
//        
//        if (completion)
//        {
//            // If quantity isn't in the database, return nil in the completion block.
//            if (results.count > 0)
//            {
//                HKQuantitySample *quantitySample = results.firstObject;
//                HKQuantity *quantity = quantitySample.quantity;
//                completion(quantity, error);
//            }
//        }
//    }];
//    
//    [self executeQuery:query];
//}


#pragma mark - Read/Write Data Functions
// Returns the types of data that Fit wishes to write to HealthKit.
- (NSSet *)dataTypesToWrite
{
    HKQuantityType *activeEnergyBurnType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    HKQuantityType *heartRateType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    HKQuantityType *walkingRunningType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    return [NSSet setWithObjects:activeEnergyBurnType,walkingRunningType,heartRateType,[HKObjectType workoutType], nil];
}

// Returns the types of data that Fit wishes to read from HealthKit.
- (NSSet *)dataTypesToRead
{
    HKQuantityType *activeEnergyBurnType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    HKQuantityType *heartRateType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    HKQuantityType *walkingRunningType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    return [NSSet setWithObjects:activeEnergyBurnType,walkingRunningType,heartRateType,[HKObjectType workoutType], nil];
}


#pragma mark - Weight Related Functions;

// Store weight here...
//-(void)storeWeightInHealthKit:(float)weight completion:(void (^)(BOOL success, NSError *error))completion;
//{
//    // Some weight in gram
//    double weightInGram = weight;
//
//    // Create an instance of HKQuantityType and
//    // HKQuantity to specify the data type and value
//    // you want to update
//    NSDate          *now = [NSDate date];
//    HKQuantityType  *hkQuantityType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
//    HKQuantity      *hkQuantity = [HKQuantity quantityWithUnit:[HKUnit gramUnit] doubleValue:weightInGram];
//
//    // Create the concrete sample
//    HKQuantitySample *weightSample = [HKQuantitySample quantitySampleWithType:hkQuantityType quantity:hkQuantity startDate:now endDate:now];
//
//    // Update the weight in the health store
//    [self saveObject:weightSample withCompletion:^(BOOL success, NSError *error)
//     {
//         if (completion)
//             completion(success, error);
//
//     }];
//
//}

//// Fetch weight here...
//- (void)fetchUserWeightFromHealthKit:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;
//{
//    // Fetch the user's default weight unit in pounds.
//    NSMassFormatter *massFormatter = [[NSMassFormatter alloc] init];
//    massFormatter.unitStyle = NSFormattingUnitStyleLong;
//
//   // NSMassFormatterUnit weightFormatterUnit = NSMassFormatterUnitPound;
//   // NSString *weightUnitString = [massFormatter unitStringFromValue:10 unit:weightFormatterUnit];
//  //  NSString *localizedWeightUnitDescriptionFormat = NSLocalizedString(@"Weight (%@)", nil);
//
//    //self.weightUnitLabel.text = [NSString stringWithFormat:localizedWeightUnitDescriptionFormat, weightUnitString];
//
//    // Query to get the user's latest weight, if it exists.
//    HKQuantityType *weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
//
//    [self aapl_mostRecentQuantitySampleOfType:weightType predicate:nil completion:^(HKQuantity *mostRecentQuantity, NSError *error)
//     {
//         if (completion)
//             completion(mostRecentQuantity, error);
//     }];
//}


#pragma mark - Height Related Functions;

// Store height here...
//-(void)storeHeightInHealthKit:(float)height completion:(void (^)(BOOL success, NSError *error))completion;
//{
//    // Some Height in CM
//    double heightInCM = height;
//
//    // Create an instance of HKQuantityType and
//    // HKQuantity to specify the data type and value
//    // you want to update
//    NSDate          *now = [NSDate date];
//    HKQuantityType  *hkQuantityType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeight];
//    HKQuantity      *hkQuantity = [HKQuantity quantityWithUnit:[HKUnit inchUnit] doubleValue:heightInCM];
//
//    // Create the concrete sample
//    HKQuantitySample *heigthSample = [HKQuantitySample quantitySampleWithType:hkQuantityType quantity:hkQuantity startDate:now endDate:now];
//
//    // Update the Height in the health store
//    [self saveObject:heigthSample withCompletion:^(BOOL success, NSError *error)
//     {
//         if (completion)
//             completion(success, error);
//     }];
//
//}

//// Fetch weight here...
//- (void)fetchUserHeightFromHealthKit:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;
//{
//    // Fetch user's default height unit in inches.
//    NSLengthFormatter *lengthFormatter = [[NSLengthFormatter alloc] init];
//    lengthFormatter.unitStyle = NSFormattingUnitStyleLong;
//
//   // NSLengthFormatterUnit heightFormatterUnit = NSLengthFormatterUnitInch;
//  //  NSString *heightUnitString = [lengthFormatter unitStringFromValue:10 unit:heightFormatterUnit];
// //   NSString *localizedHeightUnitDescriptionFormat = NSLocalizedString(@"Height (%@)", nil);
//
//    //self.heightUnitLabel.text = [NSString stringWithFormat:localizedHeightUnitDescriptionFormat, heightUnitString];
//
//    HKQuantityType *heightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeight];
//
//
//    // Query to get the user's latest height, if it exists.
//    [self aapl_mostRecentQuantitySampleOfType:heightType predicate:nil completion:^(HKQuantity *mostRecentQuantity, NSError *error)
//     {
//         if (completion)
//             completion(mostRecentQuantity, error);
//     }];
//
//}



#pragma mark - Energy Related Functions

// Store energy here...
//-(void)storeEnergyInHealthKit:(float)energy completion:(void (^)(BOOL success, NSError *error))completion;
//{
//    double energyCalories = energy;
//
//    // Create an instance of HKQuantityType and
//    // HKQuantity to specify the data type and value
//    // you want to update
//    NSDate          *now = [NSDate date];
//    HKQuantityType  *hkQuantityType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
//    HKQuantity      *hkQuantity = [HKQuantity quantityWithUnit:[HKUnit calorieUnit] doubleValue:energyCalories];
//
//    // Create the concrete sample
//    HKQuantitySample *energySample = [HKQuantitySample quantitySampleWithType:hkQuantityType quantity:hkQuantity startDate:now endDate:now];
//
//    // Update the Energy in the health store
//    [self saveObject:energySample withCompletion:^(BOOL success, NSError *error)
//     {
//         if (completion)
//             completion(success, error);
//     }];
//}

// fetch energy here...
//- (void)fetchUserEnergyFromHealthKit:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;
//{
//    // Fetch user's default energy unit in Calories.
//    NSEnergyFormatter *energyFormatter = [[NSEnergyFormatter alloc] init];
//    energyFormatter.unitStyle = NSFormattingUnitStyleLong;
//    NSEnergyFormatterUnit energyFormatterUnit = NSEnergyFormatterUnitCalorie;
//    //NSString *energyUnitString = [energyFormatter unitStringFromValue:10 unit:energyFormatterUnit];
//  //  NSString *localizedenergyUnitDescriptionFormat = NSLocalizedString(@"Energy (%@)", energyUnitString);
//
//    HKQuantityType *energyType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
//
//    // Query to get the user's latest energy, if it exists.
//    [self aapl_mostRecentQuantitySampleOfType:energyType predicate:nil completion:^(HKQuantity *mostRecentQuantity, NSError *error)
//     {
//         if (completion)
//             completion(mostRecentQuantity, error);
//     }];
//
//}

/*
// Split energy from Total Enrgy
-(void)splitTotalEnergy
{
    
    HKQuantity *totalEnergy = [HKQuantity quantityWithUnit:[HKUnit calorieUnit] doubleValue:42.0];
    
    
    [self splitTotalEnergy:totalEnergy startDate:[NSDate date] endDate:[NSDate date] resultsHandler:^(HKQuantity * _Nullable restingEnergy, HKQuantity * _Nullable activeEnergy, NSError * _Nullable error)
    {
        //
        if (!activeEnergy)
        {
            NSLog(@"Either an error occured fetching the user's energy information or none has been stored yet. In your app, try to handle this gracefully.");
            
            dispatch_async(GCDMainThread, ^{
                //          self.heightValueLabel.text = NSLocalizedString(@"Not available", nil);
            });
        }
        else
        {
            // Determine the height in the required unit.
            HKUnit *heightUnit = [HKUnit calorieUnit];
            double actEnergy = [activeEnergy doubleValueForUnit:heightUnit];
            double restEnergy = [restingEnergy doubleValueForUnit:heightUnit];

            
            // Update the user interface.
            dispatch_async(GCDMainThread, ^{
                //        self.heightValueLabel.text = [NSNumberFormatter localizedStringFromNumber:@(usersHeight) numberStyle:NSNumberFormatterNoStyle];
            });
        }

    }];
    
}


#pragma mark - WorkOut Functions

// Store workout here....

-(void)storeWorkoutInHealthKit:(NSDictionary *)dicWork completion:(void (^)(BOOL success, NSError *error))completion;
{
    // In a real world app, you would pass in a model object representing your workout data, and you would pull the relevant data here and pass it to the HealthKit workout method.
    
    // For the sake of simplicity of this example, we will just set arbitrary data.
    
    CGFloat distanceInMeters = [dicWork floatForKey:@"distance"];
    NSDate *startDate = [NSDate date];
    NSDate *endDate = [startDate dateByAddingTimeInterval:(distanceInMeters/1000)*7.5];
    NSTimeInterval duration = [endDate timeIntervalSinceDate:startDate];
    
    HKQuantity *distanceQuantity = [HKQuantity quantityWithUnit:[HKUnit meterUnit] doubleValue:(double)distanceInMeters];
    HKQuantity *enrgyQuantity = [HKQuantity quantityWithUnit:[HKUnit jouleUnit] doubleValue:150.0];
    
//    HKWorkout *workout = [HKWorkout workoutWithActivityType:HKWorkoutActivityTypeRunning startDate:startDate endDate:endDate duration:duration totalEnergyBurned:nil totalDistance:distanceQuantity metadata:nil];

    HKWorkout *workout = [HKWorkout workoutWithActivityType:HKWorkoutActivityTypeRunning startDate:startDate endDate:endDate duration:duration totalEnergyBurned:nil totalDistance:distanceQuantity metadata:nil];

    [self saveObject:workout withCompletion:^(BOOL success, NSError *error)
    {
        NSLog(@"Saving workout to healthStore - success: %@", success ? @"YES" : @"NO");
        
        if (completion) {
            completion(success,error);
        }
        
        if (error != nil)
        {
            NSLog(@"error: %@", error);
        }
    }];
}


-(void)fetchWorkoutFromHealthKit:(void (^)(HKWorkout *workout, NSError *error))completion;
{
    
    // 1. Predicate to read only running workouts
    NSPredicate *predicate = [HKQuery predicateForWorkoutsWithWorkoutActivityType:HKWorkoutActivityTypeRunning];
    
    // 2. Order the workouts by date
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:HKSampleSortIdentifierStartDate ascending:false];
    
    // 3. Create the query
    HKSampleQuery *sampleQuery = [[HKSampleQuery alloc] initWithSampleType:[HKWorkoutType workoutType] predicate:predicate limit:HKObjectQueryNoLimit sortDescriptors:@[sortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error)
    {
        if(!error && results)
        {
            NSLog(@"Retrieved the following workouts");
            for(HKQuantitySample *samples in results)
            {
                // your code here
                HKWorkout *workout = (HKWorkout *)samples;
                NSLog(@"%@",workout);
                
                if (completion)
                    completion(workout,nil);
            }
        }
        else
        {
            NSLog(@"Error retrieving workouts %@",error);
            if (completion)
                completion(nil,error);
        }
    }];
    
    // Execute the query
    [self executeQuery:sampleQuery];
}



-(void)energycheck
{
    HKWorkoutConfiguration *configuration = [[HKWorkoutConfiguration alloc] init];
    configuration.activityType = HKWorkoutActivityTypeRunning;
    configuration.locationType = HKWorkoutSessionLocationTypeOutdoor;
    
    
//    do {
//        HKWorkoutSession
//        
//        let session = try HKWorkoutSession(configuration: configuration)
//        
//        session.delegate = self
//        healthStore.start(session)
//    }
//    catch let error as NSError {
//        // Perform proper error handling here...
//        fatalError("*** Unable to create the workout session: \(error.localizedDescription) ***")
//    }
}

*/

-(void)fetchBPMValueFromHealthKit:(HKWorkout *)dicWork1 completion:(void (^)(HKQuantity *quantity, NSError *error))completion;
{
    HKSampleType *tHeartRate = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    
    NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:YES];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:tHeartRate predicate:nil limit:HKObjectQueryNoLimit sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error)
                            {
                                if (!results)
                                {
                                    if (completion)
                                        completion(nil, error);
                                    
                                    return;
                                }
                                
                                if (completion)
                                {
                                    // If quantity isn't in the database, return nil in the completion block.
                                    if (results.count > 0)
                                    {
                                        HKQuantitySample *quantitySample = results.lastObject;
                                        
                                        NSDateFormatter *dtFormater = [[NSDateFormatter alloc] init];
                                        dtFormater.dateFormat = @"yyyy-MM-dd";
                                        NSString *strCurrentDate = [dtFormater stringFromDate:[NSDate date]];
                                        NSString *strHealthDate = [dtFormater stringFromDate:quantitySample.endDate];
                                        
                                        if ([strCurrentDate isEqualToString:strHealthDate])
                                        {
                                            HKQuantity *quantity = quantitySample.quantity;
                                            completion(quantity, error);
                                        }
                                    }
                                }
                            }];
    
    [self executeQuery:query];

}

/*
-(void)updateHeartbeat:(NSDate *)startDate{
    
    //first, create a predicate and set the endDate and option to nil/none
    NSPredicate *Predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:nil options:HKQueryOptionNone];
    
    //Then we create a sample type which is HKQuantityTypeIdentifierHeartRate
    HKSampleType *object = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    
    //ok, now, create a HKAnchoredObjectQuery with all the mess that we just created.
    HKAnchoredObjectQuery *heartQuery = [[HKAnchoredObjectQuery alloc] initWithType:object predicate:Predicate anchor:0 limit:0 resultsHandler:^(HKAnchoredObjectQuery *query, NSArray<HKSample *> *sampleObjects, NSArray<HKDeletedObject *> *deletedObjects, HKQueryAnchor *newAnchor, NSError *error) {
        
        if (!error && sampleObjects.count > 0) {
            HKQuantitySample *sample = (HKQuantitySample *)[sampleObjects objectAtIndex:0];
            HKQuantity *quantity = sample.quantity;
            NSLog(@"%f", [quantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]]);
        }else{
            NSLog(@"query %@", error);
        }
        
    }];
    
    //wait, it's not over yet, this is the update handler
    [heartQuery setUpdateHandler:^(HKAnchoredObjectQuery *query, NSArray<HKSample *> *SampleArray, NSArray<HKDeletedObject *> *deletedObjects, HKQueryAnchor *Anchor, NSError *error) {
        
        if (!error && SampleArray.count > 0) {
            HKQuantitySample *sample = (HKQuantitySample *)[SampleArray objectAtIndex:0];
            HKQuantity *quantity = sample.quantity;
            NSLog(@"%f", [quantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]]);
        }else{
            NSLog(@"query %@", error);
        }
    }];
    
    //now excute query and wait for the result showing up in the log. Yeah!
    [self executeQuery:heartQuery];
}
*/

/*
// Total walking/Running distance
-(void)fetchTotalDistanceFromHealthKit:(HKWorkout *)dicWork1 completion:(void (^)(HKQuantity *quantity, NSError *error))completion;
{
    HKSampleType *tHeartRate = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:tHeartRate predicate:nil limit:1 sortDescriptors:nil resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error)
                            {
                                if (!results)
                                {
                                    if (completion)
                                        completion(nil, error);
                                    
                                    return;
                                }
                                
                                if (completion)
                                {
                                    // If quantity isn't in the database, return nil in the completion block.
                                    if (results.count > 0)
                                    {
                                        HKQuantitySample *quantitySample = results.firstObject;
                                        HKQuantity *quantity = quantitySample.quantity;
                                        completion(quantity, error);
                                        
//                                        double beats = [quantity doubleValueForUnit:[[HKUnit meterUnit] unitDividedByUnit:[HKUnit minuteUnit]]];
                                        double distance = [quantity doubleValueForUnit:[HKUnit meterUnit]]/1000;
                                        NSLog(@"got: %@", [NSString stringWithFormat:@"%.f",distance]) ;

                                    }
                                }
                            }];
    
    [self executeQuery:query];
    
}
*/
@end
