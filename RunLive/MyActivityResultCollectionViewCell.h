//
//  MyActivityResultCollectionViewCell.h
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyActivityResultCollectionViewCell : UICollectionViewCell
@property IBOutlet UIImageView *imgTop;
@property IBOutlet UILabel *lblContent,*lblSubContent,*lblUnit;

@end
