//
//  SessionListViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/8/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionListViewController.h"
#import "SessionListCell.h"
#import "SessionListHeader.h"
#import "SessionDetailsViewController.h"
#import "CreateSessionViewController.h"
#import "PremiumView.h"
#import "InAppPurchaseViewController.h"


@interface SessionListViewController ()

@end

@implementation SessionListViewController
{
    NSMutableArray *arrSection;
    BOOL shouldShowLoader;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    arrSection = [NSMutableArray new];
    [tblSessions registerNib:[UINib nibWithNibName:@"SessionListCell" bundle:nil] forCellReuseIdentifier:@"SessionListCell"];
 
    shouldShowLoader = YES;
    
    [UIApplication applicationWillEnterForeground:^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[SessionListViewController class]])
            [tblSessions reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate.isUserOpenSessionListScreen = YES;
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:YES];
    
    btnCreateSession.hidden = NO;
    btnBack.hidden = YES;
    [tblSessions setContentInset:UIEdgeInsetsMake(0, 0, 80, 0)];
    [appDelegate showTabBar];
    cnTableViewBottomSpace.constant = CScreenHeight - CViewY(appDelegate.objCustomTabBar);
    
    NSLog(@"%f",cnTableViewBottomSpace.constant);
    
    
    if (!appDelegate.isStartSessionPopUp)
    {
        [arrSection removeAllObjects];
        [tblSessions reloadData];
        [self getSessionListFromServer];
    }
    else
    {
        [tblSessions reloadData];
    }
    
    appDelegate.isStartSessionPopUp = NO;
    [self refreshDataFromPushNotification];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    appDelegate.objSessionGoingToStart = nil;
}

#pragma mark - Handel Push Notification

-(void)refreshDataFromPushNotification
{
    appDelegate.configureRefreshSessionData = ^(RefreshSessionWithUpdate refreshSessionWithUpdate)
    {
        if (refreshSessionWithUpdate == DeleteSession || refreshSessionWithUpdate == SyncSession || refreshSessionWithUpdate == JoinSession || refreshSessionWithUpdate == UnjoinSession)
        {
            if ([[appDelegate getTopMostViewController] isKindOfClass:[SessionListViewController class]])
            {
                [arrSection removeAllObjects];
                [tblSessions reloadData];
                [self getSessionListFromServer];
            }
        }
    };
}

#pragma mark - API Related Function

-(void)getSessionListFromServer
{
    if (shouldShowLoader)
    {
        shouldShowLoader = NO;
        activityIndicator.hidden = NO;
        [activityIndicator startAnimating];
    }
    
    viewNoDataFound.hidden = YES;
    
    [[APIRequest request] GETSessionList:^(id responseObject, NSError *error)
     {
         activityIndicator.hidden = YES;
         [activityIndicator stopAnimating];

         if (responseObject && !error)
         {
             [arrSection removeAllObjects];
             [tblSessions reloadData];
             
             [arrSection addObjectsFromArray:[TblSessionListDate fetch:nil orderBy:@"timestamp" ascending:YES]];
             [tblSessions reloadData];
             viewNoDataFound.hidden = arrSection.count > 0;
         }
     }];
}

#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TblSessionListDate *objSessionDate = arrSection[section];
    return [objSessionDate.sessionList allObjects].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SessionListCell";
    SessionListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
        cell = [[SessionListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TblSessionListDate *objSessionDate = [arrSection objectAtIndex:indexPath.section];
    TblSessionList *objSession = [[objSessionDate.sessionList.allObjects sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"session_end_time" ascending:YES]]] objectAtIndex:indexPath.row];

    if (indexPath.section == 0 && indexPath.row == 0)
    {
        // Store session object here..
        appDelegate.objSessionGoingToStart = nil;
        appDelegate.objSessionGoingToStart = objSession;
    }
    
    
    cell.lblSessionName.text = objSession.session_name;
    
    NSArray *arrJoinedUser = objSession.joinedUser.allObjects; // Get Joiend user here....
    NSArray *arrSyncedUser = objSession.syncedUser.allObjects; // Get Synced user here....

    if (arrJoinedUser.count > 0 || arrSyncedUser.count > 0)
    {
        // Show user view here...
        cell.viewSyncedContainer.hidden = NO;
        
        BOOL isSynced;
        NSArray *arrSessionUser;
        if (arrSyncedUser.count > 0)
        {
            // Show Synced user
            isSynced = YES;
            arrSessionUser = arrSyncedUser;
            cell.btnSyncedUser.backgroundColor = CRGB(115, 212, 247);
            [cell.btnSyncedUser setTitle:[NSString stringWithFormat:@"%d SYNCED",(int)arrSyncedUser.count] forState:UIControlStateNormal];
        }
        else
        {
            // Show Joined user
            isSynced = NO;
            arrSessionUser = arrJoinedUser;
            cell.btnSyncedUser.backgroundColor = CRGB(213, 218, 226);
            [cell.btnSyncedUser setTitle:[NSString stringWithFormat:@"+ %d JOINED",(int)arrJoinedUser.count] forState:UIControlStateNormal];
        }
        
        for (int i = 0; arrSessionUser.count > i; i++)
        {
            if (isSynced)
            {
                TblSessionSyncedUser *objSynced = arrSessionUser[i];
                switch (i)
                {
                    case 0:
                    {
                        cell.viewSynced1.hidden =NO;
                        [cell.imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:objSynced.synced_image_url] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    }
                        break;
                    case 1:
                    {
//                        cell.viewSynced2.hidden = NO;
                        [cell.imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:objSynced.synced_image_url] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                TblJoinedUser *objJoined = arrSessionUser[i];
                switch (i)
                {
                    case 0:
                    {
                        cell.viewSynced1.hidden =NO;
                        [cell.imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:objJoined.joine_image_url] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    }
                        break;
                    case 1:
                    {
//                        cell.viewSynced2.hidden = NO;
                        [cell.imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:objJoined.joine_image_url] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    }
                        break;
                    default:
                        break;
                }
            }
            
            if (i == 1)
                break;
        }
        
        [cell.viewSynced2 hideByWidth:arrSessionUser.count < 2];
    }
    else
    {
        // Hide user view here...
        cell.viewSyncedContainer.hidden = YES;
    }
    
    cell.objSessionTime = objSession;
    [cell startTimer];
    

    if (objSession.isRunning.boolValue)
        cell.lblTime.text = @"IN PROGRESS";
    
    
    cell.configureSesionTimeOver = ^(TblSessionList *objSessionStart)
    {
        if (objSessionStart.isRunning.boolValue)
            return ;
        
        // Remove session from list if time is over........
        if (objSessionStart)
        {
            [appDelegate showCountDownScreenForManualSession:objSessionStart.session_id completed:nil];
            
            if (arrSection.count > 0)
            {
                [TblSessionList deleteObjects:[NSPredicate predicateWithFormat:@"self == %@",objSessionStart]];
                [[Store sharedInstance].mainManagedObjectContext save];
                [arrSection removeAllObjects];
                [arrSection addObjectsFromArray:[TblSessionListDate fetch:nil orderBy:@"timestamp" ascending:YES]];
                [tblSessions reloadData];
                
             //   [arrSection removeObjectAtIndex:indexPath.row];
                //[tblSessions reloadData];
            }
        }
    };
    
    cell.rightUtilityButtons = nil;
    if (!cell.rightUtilityButtons)
    {
        if (!objSession.isRunning.boolValue)
        {
            if ([objSession.user_id isEqualToString:appDelegate.loginUser.user_id])
            {
                [cell setRightUtilityButtons:@[[UIButton buttonWithColor:CRGB(115, 213, 248) icon:[UIImage imageNamed:@"ic_session_edit"]],[UIButton buttonWithColor:CRGB(229, 86, 97) icon:[UIImage imageNamed:@"ic_notification_cancel"]]]WithButtonWidth:80];
            }
            else
                [cell setRightUtilityButtons:@[[UIButton buttonWithColor:CRGB(229, 86, 97) title:@"Unjoin"]]WithButtonWidth:80];
        }
        
    }

#pragma mark - Delete/Edit Session
    __weak typeof(cell) weakCell = cell;
    
    [cell setLeftRightUtilityButtonsClicked:^(SWCellState state, NSUInteger index)
    {
        if (index == 1)
        {
#pragma mark - Delete Session
            
            [self customAlertViewWithTwoButton:@"" Message:@"Are you sure you want to delete this session?" ButtonFirstText:@"Cancel" ButtonSecondText:@"OK" Animation:YES completed:^(int index) {
                if (index == 1)
                {
                    // Delete session
                    [[APIRequest request] DeleteSession:objSession.session_id completed:^(id responseObject, NSError *error)
                     {
                         [weakCell stopTimer];
                         
                         NSArray *arrActivity = [TblActivitySyncSession fetch:[NSPredicate predicateWithFormat:@"session_id == %@",objSession.session_id] sortedBy:nil];
                         
                         for (int i = 0; arrActivity.count > i; i++)
                         {
                             TblActivitySyncSession *objSyncActivity = arrActivity[i];
                             [TblActivitySyncSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objSyncActivity.activity_id]];
                             [[Store sharedInstance].mainManagedObjectContext save];
                         }
                         
                         [appDelegate updateActivityListData:DeleteActivity MainWis:NO];
                         
                         [self getSessionListFromServer];
                     }];
                }
            }];
        }
        else
        {
#pragma mark - Edit Session
            if ([objSession.user_id isEqualToString:appDelegate.loginUser.user_id])
            {
//                if (arrJoinedUser.count < 2)
                if (arrJoinedUser.count+arrSyncedUser.count < 2 && arrSyncedUser.count < 1)
                {
                    CreateSessionViewController *objCreat = [[CreateSessionViewController alloc] init];
                    objCreat.isEditSession = YES;
                    objCreat.objSessionEdit = objSession;
                    [self.navigationController pushViewController:objCreat animated:YES];
                }
            }
            else
            {
                [self customAlertViewWithTwoButton:@"" Message:@"Are you sure you want to unjoin this session?" ButtonFirstText:@"Cancel" ButtonSecondText:@"OK" Animation:YES completed:^(int index) {
                    if (index == 1)
                    {
                        // Unjoin session
                        [[APIRequest request] DeclineSession:objSession.session_id completed:^(id responseObject, NSError *error)
                         {
                             // Remove notification from list
                             [self getSessionListFromServer];
                         }];
                    }
                }];
            }
        }
    }];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SessionListHeader *viewHeader = [SessionListHeader viewFromXib];
    
    TblSessionListDate *objSessionDate = arrSection[section];
    viewHeader.lblDay.text = objSessionDate.date_text;
    
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SessionListCell *cell = (SessionListCell *)[tblSessions cellForRowAtIndexPath:indexPath];
    
    TblSessionListDate *objSessionDate = [arrSection objectAtIndex:indexPath.section];
    TblSessionList *objSession = [[objSessionDate.sessionList.allObjects sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"session_end_time" ascending:YES]]] objectAtIndex:indexPath.row];
    
    if (!objSession.isRunning.boolValue)
    {
        [cell stopTimer];
        SessionDetailsViewController *objSes = [[SessionDetailsViewController alloc] init];
        objSes.strSessionId = objSession.session_id;
        [self.navigationController pushViewController:objSes animated:YES];
    }
}


-(BOOL)showWarningAlert
{
    if (!appDelegate.objSessionGoingToStart)
        return NO;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    double sessionEndTimeStamp = appDelegate.objSessionGoingToStart.session_end_time.doubleValue;
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;
    
    if (difTimeStamp > 0 && difTimeStamp < 60)
        return YES;
    else
        return NO;
}

-(void)moveOnCreateSessionScreen
{
    CreateSessionViewController *objCreateSession = [[CreateSessionViewController alloc] initWithNibName:@"CreateSessionViewController" bundle:nil];
    [self.navigationController pushViewController:objCreateSession animated:YES];
    
    /*
    NSDate *dtCreated = [self convertDateFromString:appDelegate.loginUser.subscription_exp_date isGMT:YES formate:CDateFormater];
    NSTimeInterval currentDateTimeStamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval SesisonExpireDateTimeStamp = [dtCreated timeIntervalSince1970];
    
    if (currentDateTimeStamp > SesisonExpireDateTimeStamp && !appDelegate.loginUser.is_premium.boolValue)
    {
        InAppPurchaseViewController *objInApp = [[InAppPurchaseViewController alloc] init];
        [self presentViewController:objInApp animated:YES completion:nil];
    }
    else
        [self.navigationController pushViewController:objCreateSession animated:YES];
    */
}

#pragma mark - Action Event

-(IBAction)btnCreateSessionCLK:(id)sender
{
    if ([self showWarningAlert])
    {
        appDelegate.isStartSessionPopUp = YES;
        
        CustomAlertView *objAlert = [CustomAlertView viewFromXib];
        //[viewController presentViewOnPopUpViewController:objAlert shouldClickOutside:NO dismissed:nil];
        
        [appDelegate.objTabBarController presentViewOnPopUpViewController:objAlert shouldClickOutside:NO dismissed:nil];
        [objAlert initWithTitle:@"WARNING!" message:CMessageSessionStartWarning firstButton:@"OK" secondButton:@"LEAVE ANYWAY"];
        
        [objAlert.btnFirst touchUpInsideClicked:^{
            // OK BTN CLK
            [appDelegate.objTabBarController dismissOverlayViewControllerAnimated:YES completion:nil];
        }];
        
        [objAlert.btnSecond touchUpInsideClicked:^{
            // Move ahead
            
            appDelegate.objSessionGoingToStart = nil;
            [self moveOnCreateSessionScreen];
            [appDelegate.objTabBarController dismissOverlayViewControllerAnimated:YES completion:nil];
        }];
    }
    else
    {
        [self moveOnCreateSessionScreen];
    }
}

@end
