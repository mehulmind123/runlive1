//
//  SessionListCell.h
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^sesionTimeOver)(TblSessionList *objSessionStart);

@interface SessionListCell : SWTableViewCell

@property(weak,nonatomic) IBOutlet UIView *viewContainer,*viewSubContainer,*viewSynced1,*viewSynced2,*viewBlueLine,*viewSyncedContainer;
@property(weak,nonatomic) IBOutlet UIImageView *imgCircleTag,*imgSynced1,*imgSynced2;
@property(weak,nonatomic) IBOutlet UIButton *btnSyncedUser;
@property(weak,nonatomic) IBOutlet UILabel *lblTime;
@property(weak,nonatomic) IBOutlet UILabel *lblSessionName;;
@property(strong,nonatomic) sesionTimeOver configureSesionTimeOver;

@property (strong, nonatomic) NSTimer *timer;
-(void)startTimer;
-(void)stopTimer;


@property(weak,nonatomic) TblSessionList *objSessionTime;

@end
