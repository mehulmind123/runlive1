//
//  RunScoreBoardBlueTeamCell.h
//  RunLive
//
//  Created by mac-0006 on 24/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunScoreBoardBlueTeamCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblPosition;
@property (strong, nonatomic) IBOutlet UIView *viewCircle;
@property (strong, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (strong, nonatomic) IBOutlet UIImageView *imgTriangle;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblCal;
@property (strong, nonatomic) IBOutlet UILabel *lblSpeed;
@property (strong, nonatomic) IBOutlet UIView *viewbluecircle;
@property (strong, nonatomic) IBOutlet UIView *viewHorizontalLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgVerifiedUser;

@end
