//
//  RunScoreBoardCell.m
//  RunLive
//
//  Created by mac-0006 on 17/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "RunScoreBoardCell.h"

@implementation RunScoreBoardCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.viewCircle.layer.cornerRadius =  self.viewCircle.frame.size.width/2;
    self.imgUserPic.layer.cornerRadius = self.imgUserPic.frame.size.width/2;
    self.viewCircle.layer.masksToBounds = YES;
    self.imgUserPic.layer.masksToBounds = YES;
    
    self.viewCircle.layer.borderWidth = 1;
    self.viewCircle.layer.borderColor = CRGB(235, 241, 245).CGColor;

}

@end
