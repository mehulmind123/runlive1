//
//  GallexyAlertView.m
//  RunLive
//
//  Created by mac-0005 on 14/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "GallexyAlertView.h"

@implementation GallexyAlertView

+(id)initGallexyAlertView
{
    GallexyAlertView *objPopUp = [[[NSBundle mainBundle]loadNibNamed:@"GallexyAlertView" owner:nil options:nil] lastObject];
    CViewSetWidth(objPopUp, CScreenWidth);
    CViewSetHeight(objPopUp, CScreenHeight);
    return objPopUp;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.viewContainer.layer.cornerRadius = self.btnFirst.layer.cornerRadius = self.btnSecond.layer.cornerRadius = self.btnThird.layer.cornerRadius = 3;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.center = CScreenCenter;
}

#pragma mark - Configuartion

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonFirstText:(NSString *)strBtnFirst ButtonSecondText:(NSString *)strBtnSecond ButtonThirdText:(NSString *)strBtnThird
{
    lblTitle.text = strTitle;
    lblMessage.text = strMessage;
    [self.btnFirst setTitle:strBtnFirst forState:UIControlStateNormal];
    [self.btnSecond setTitle:strBtnSecond forState:UIControlStateNormal];
    [self.btnThird setTitle:strBtnThird forState:UIControlStateNormal];
}

@end
