//
//  WarningPopUp.h
//  RunLive
//
//  Created by mac-00012 on 5/10/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarningPopUp : UIView

@property(strong,nonatomic) IBOutlet UIButton *btnOk;
@property(strong,nonatomic) IBOutlet UILabel *lblMessage,*lblTitle;

-(void)initWithTitle:(NSString *)title message:(NSString *)message;

@end
