//
//  videoWaterMarkView.h
//  RunLive
//
//  Created by mac-0005 on 17/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface videoWaterMarkView : UIView
{
    IBOutlet UILabel
    *lblVideoTotalDistance,
    *lblVideoMovingTime,
    *lblVideoPace,
    *lblVideoRank;
}

+ (id)initVideoWaterMarkView:(CGSize)size;

-(void)setDataForWaterMark:(NSDictionary *)dicData;

@end
