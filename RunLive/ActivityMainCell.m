//
//  ActivityMainCell.m
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityMainCell.h"
#import "ActivityLeaderBoardCell.h"
#import "ActivityAddPhotoCell.h"
#import "StartActivityCell.h"
#import "SyncedActivityCell.h"
#import "ActivityFeedViewController.h"


@implementation ActivityMainCell
{
    StartActivityCell *cellStartActivity;
    NSIndexPath *indexPathForVideoCell;
    
    NSURLSessionDataTask *taskRuningLeaderBoard;
    NSURLSessionDataTask *taskRuningActivityFeed;
    BOOL isApiStarted;
    int iOffset,iOffsetActivity;
    
    UIRefreshControl *refreshControl;
    
    CGFloat lastScrollOffset;
    BOOL dragging;
    
    NSMutableArray *arrActivityData;
}

@synthesize cellActivityVideo,tblActivity,arrData;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
    arrData = [NSMutableArray new];
    arrActivityData = [NSMutableArray new];
    
    [tblActivity registerNib:[UINib nibWithNibName:@"ActivityLeaderBoardCell" bundle:nil] forCellReuseIdentifier:@"ActivityLeaderBoardCell"];
    [tblActivity registerNib:[UINib nibWithNibName:@"ActivityAddPhotoCell" bundle:nil] forCellReuseIdentifier:@"ActivityAddPhotoCell"];
    [tblActivity registerNib:[UINib nibWithNibName:@"StartActivityCell" bundle:nil] forCellReuseIdentifier:@"StartActivityCell"];
    [tblActivity registerNib:[UINib nibWithNibName:@"SyncedActivityCell" bundle:nil] forCellReuseIdentifier:@"SyncedActivityCell"];
    
    tblActivity.estimatedRowHeight = 80;
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblActivity addSubview:refreshControl];
    
    appDelegate.configureRefreshMainActivityList = ^(ActivityData activityData)
    {
        if (btnActive.selected && ![[appDelegate getTopMostViewController] isKindOfClass:[ActivityFeedViewController class]])
        {
            if (activityData == CallActivityApi)
            {
                iOffsetActivity = 0;
                [self getActivityDataFromServer:NO];
            }
            else if (activityData == RefreshActivityList)
                [tblActivity reloadData];
            else if (activityData == DeleteActivity)
                [self fetchAllActivityFromLocal];
        }
        else if (activityData == DeleteActivity)
            [self fetchAllActivityFromLocal];
    };
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
    
    //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self btnTapBarCLk:btnActive];
    //});
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    imgUser.layer.cornerRadius = CGRectGetHeight(imgUser.frame)/2;
    viewUserImg.layer.cornerRadius = CGRectGetHeight(viewUserImg.frame)/2;
    viewUserImg.layer.borderWidth = 2;
    viewUserImg.layer.borderColor = CRGB(72, 223, 254).CGColor;
}

#pragma mark - Pull Refresh

-(void)showBottomAcitvityIndicator:(BOOL)isShow
{
    if (isShow)
    {
        viewLoadMoreData.hidden = NO;
        [BottomActivityIndicator startAnimating];
        [tblActivity setConstraintConstant:CViewHeight(viewLoadMoreData) toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
    }
    else
    {
        viewLoadMoreData.hidden = YES;
        [BottomActivityIndicator stopAnimating];
        [UIView animateWithDuration:0.8 animations:^{
            [tblActivity setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        }];
    }
}

-(void)handleRefresh
{
    if (btnActive.selected)
    {
        if (isApiStarted)
            return;
        
        iOffsetActivity = 0;
        [self getActivityDataFromServer:NO];
    }
    else
    {
        iOffset = 0;
        [self getLeaderBoardFromServer];
    }
    
}


- (void)LoadMoreDataFromServer
{
    if (isApiStarted)
        return;
    
    if (btnActive.selected)
    {
        if (iOffsetActivity == 0)
            return;
        
        [self getActivityDataFromServer:NO];
    }
    else
    {
        if (iOffset == 0)
            return;
        
        [self showBottomAcitvityIndicator:YES];
        [self getLeaderBoardFromServer];
    }
    
}

-(void)getLeaderBoardFromServer
{
    isApiStarted = YES;
    
    if (iOffset == 0)
    {
        [arrData removeAllObjects];
        [tblActivity reloadData];
        self.btnMyProfile.userInteractionEnabled = NO;
    }
    
    taskRuningLeaderBoard = [[APIRequest request] GetLeaderboardList:[NSNumber numberWithInt:iOffset] completed:^(id responseObject, NSError *error)
                             {
                                 [self showBottomAcitvityIndicator:NO];
                                 
                                 isApiStarted = NO;
                                 [refreshControl endRefreshing];
                                 if (responseObject && !error)
                                 {
                                     if (iOffset == 0)
                                         [arrData removeAllObjects];
                                     
                                     NSDictionary *dicData = [responseObject valueForKey:CJsonData];
                                     iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
                                     
                                     [arrData addObjectsFromArray:[dicData valueForKey:@"leaderBoard"]];
                                     [tblActivity reloadData];
                                     
                                     // Set Login User Data here.....
                                     
                                     NSDictionary *dicUser = [dicData valueForKey:@"loginUser"];
                                     
                                     if ([dicUser objectForKey:@"run"])
                                     {
                                         NSDictionary *dicUserRun = [dicUser valueForKey:@"run"];
                                         
                                         imgVerifiedUser.hidden = [[dicUser numberForJson:@"celebrity"] isEqual:@0];
                                         
                                         NSString *strFirstname = [[dicUser stringValueForJSON:@"first_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                         NSString *strLastname = [[dicUser stringValueForJSON:@"last_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                         NSString *strCountry = [[dicUser stringValueForJSON:@"country"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                         
                                         
                                         if (!strFirstname || [strFirstname isEqualToString:@""])
                                             _lblLoginUserName.text = [NSString stringWithFormat:@"%@ - %@", [dicUser stringValueForJSON:@"user_name"],strCountry];
                                         else
                                             _lblLoginUserName.text = [NSString stringWithFormat:@"%@ %@ - %@", strFirstname,strLastname,strCountry];
                                         
                                         [imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

                                         _lblLoginUserRank.text = [dicUserRun stringValueForJSON:@"rank"];
                                         _lblLoginUserCal.text = [NSString stringWithFormat:@"%.2f cal",[dicUserRun stringValueForJSON:@"total_calories"].floatValue];
                                         _lblLoginUserMiles.text = [appDelegate getDistanceInWithSelectedFormate:[dicUserRun stringValueForJSON:@"total_distance"]];
                                         
                                         if ([dicUserRun stringValueForJSON:@"total_runs"].integerValue == 1)
                                             _lblLoginUserRuns.text = [NSString stringWithFormat:@"%@ run",[dicUserRun stringValueForJSON:@"total_runs"]];
                                         else
                                             _lblLoginUserRuns.text = [NSString stringWithFormat:@"%@ runs",[dicUserRun stringValueForJSON:@"total_runs"]];

                                         
                                         _imgPerformanceTag.image = [appDelegate GetImageWithUserPerformance:[dicUserRun stringValueForJSON:@"performance"]];
                                         
                                         self.btnMyProfile.userInteractionEnabled = YES;
                                         
                                         viewSubUser.hidden = YES;
                                     }
                                 }
                             }];
}

-(void)getActivityDataFromServer:(BOOL)shouldShowLoader
{
    if (shouldShowLoader)
    {
        activityIndicator.hidden = NO;
        [activityIndicator startAnimating];
    }
    
    isApiStarted = YES;
    
    taskRuningActivityFeed = [[APIRequest request] GetActivityFeedList:[NSNumber numberWithInt:iOffsetActivity] Type:@"1" UserName:nil completed:^(id responseObject, NSError *error)
    {
        isApiStarted = NO;
        [refreshControl endRefreshing];
        
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];

        if (responseObject && !error)
        {
            if (iOffsetActivity == 0)
            {
                [arrActivityData removeAllObjects];
                [tblActivity reloadData];
            }
            
            iOffsetActivity = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;

            NSArray *arrTemp = [responseObject valueForKey:CJsonData];
            
            if (arrTemp.count > 0)
                [self fetchAllActivityFromLocal];
        }
    }];
}

-(void)fetchAllActivityFromLocal
{
    [arrActivityData removeAllObjects];
    
    NSArray *arrTemp = [TblActivityDates fetch:nil orderBy:@"timestamp" ascending:NO];
    for (int i = 0; arrTemp.count > i; i++)
    {
        TblActivityDates *objActivitySection = arrTemp[i];
        NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
        [arrActivityData addObjectsFromArray:arrDataActivity];
    }
    
    NSArray *arrDataActivity = [arrActivityData sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
    [arrActivityData removeAllObjects];
    [arrActivityData addObjectsFromArray:arrDataActivity];
    
    dispatch_async(GCDMainThread, ^{
        [tblActivity reloadData];
    });
    
    
    
//    [tblActivity scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];

    [self playVideoAutomatically];
    
}


#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (btnLeaderBoard.selected)
        return 1;
    
//    return arrActivityData.count;
    
    return 1;
}

-(NSArray *)getActivityArray:(TblActivityDates *)objActivitySection
{
    NSMutableArray *arrActTemp = [NSMutableArray new];
    
    if (objActivitySection.objActivityPhoto.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:objActivitySection.objActivityPhoto.allObjects];
    
    if (objActivitySection.objActivityVideo.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:objActivitySection.objActivityVideo.allObjects];
    
    if (objActivitySection.objActivitySyncSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:objActivitySection.objActivitySyncSession.allObjects];
    
    if (objActivitySection.objActivityRunSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:objActivitySection.objActivityRunSession.allObjects];
    
    if (objActivitySection.objActivityCompleteSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:objActivitySection.objActivityCompleteSession.allObjects];
    
    NSArray *arrDataActivity = [arrActTemp sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
    
    return arrDataActivity;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (btnLeaderBoard.selected)
        return arrData.count;
    
//    TblActivityDates *objActivitySection = arrActivityData[section];
//    NSArray *arrActivity = [self getActivityArray:objActivitySection];
    
    return arrActivityData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Load Leader Board Cell Here...
    if (btnLeaderBoard.selected)
    {
        NSString *identifier = @"ActivityLeaderBoardCell";
        ActivityLeaderBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil)
            cell = [[ActivityLeaderBoardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrData objectAtIndex:indexPath.row];
        NSDictionary *dicRun = [dicData valueForKey:@"run"];
        
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        NSString *strFirstname = [[dicData stringValueForJSON:@"first_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *strLastname = [[dicData stringValueForJSON:@"last_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *strCountry = [[dicData stringValueForJSON:@"country"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (!strFirstname || [strFirstname isEqualToString:@""])
            cell.lblUserName.text = [NSString stringWithFormat:@"%@ - %@", [dicData stringValueForJSON:@"user_name"],strCountry];
        else
            cell.lblUserName.text = [NSString stringWithFormat:@"%@ %@ - %@", strFirstname,strLastname,strCountry];

        cell.lblRank.text = [dicRun stringValueForJSON:@"rank"];
        
        NSString *strDis = [appDelegate getDistanceInWithSelectedFormate:[dicRun stringValueForJSON:@"total_distance"]];
        
        if ([dicRun stringValueForJSON:@"total_runs"].integerValue == 1)
            cell.lblRunData.text = [NSString stringWithFormat:@"%@/ %.2f cal/ %@ run",strDis,[dicRun stringValueForJSON:@"total_calories"].floatValue,[dicRun stringValueForJSON:@"total_runs"]];
        else
            cell.lblRunData.text = [NSString stringWithFormat:@"%@/ %.2f cal/ %@ runs",strDis,[dicRun stringValueForJSON:@"total_calories"].floatValue,[dicRun stringValueForJSON:@"total_runs"]];

        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        cell.imgRankTag.image = [appDelegate GetImageWithUserPerformance:[dicRun stringValueForJSON:@"performance"]];
        return cell;
    }
    // Load Activity Cell's Here...
    else
    {
//        TblActivityDates *objActivitySection = arrActivityData[indexPath.section];
//        NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
        
        NSManagedObject *objCoreData = (NSManagedObject *)arrActivityData[indexPath.row];
        
        if ([objCoreData isKindOfClass:[TblActivityPhoto class]])
        {
            // Photo Activity
            TblActivityPhoto *objActPhoto = (TblActivityPhoto *)objCoreData;
            ActivityAddPhotoCell *cell = [tblActivity dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configurePhotoActivityCell:cell Activity:objActPhoto];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivityVideo class]])
        {
            // Video Activity
            TblActivityVideo *objActVideo = (TblActivityVideo *)objCoreData;
            ActivityAddPhotoCell *cell = [tblActivity dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureVideoActivityCell:cell Activity:objActVideo];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivitySyncSession class]])
        {
            // Sync Activity
            TblActivitySyncSession *objActSync = (TblActivitySyncSession *)objCoreData;
            SyncedActivityCell *cell = [tblActivity dequeueReusableCellWithIdentifier:NSStringFromClass([SyncedActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureSyncedActivityCell:cell Activity:objActSync];
            return cell;

        }
        else if ([objCoreData isKindOfClass:[TblActivityRunSession class]])
        {
            // Ongoing Run Activity
            TblActivityRunSession *objActRun = (TblActivityRunSession *)objCoreData;
            StartActivityCell *cell = [tblActivity dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureStartSessionActivityCell:cell Activity:objActRun];
            
            return cell;

        }
        else if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
        {
            // Complete Run Activity
            TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
            StartActivityCell *cell = [tblActivity dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [self configureCompleteSessionActivityCell:cell Activity:objActCom];
            return cell;
        }
    }
    
    return nil;

}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnLeaderBoard.selected)
        return 80;
    
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnLeaderBoard.selected)
        return 80;
    
    return UITableViewAutomaticDimension;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (btnLeaderBoard.selected)
    {
        // Leader Board selected
        NSDictionary *dicData = [arrData objectAtIndex:indexPath.row];
        if (self.configureActivityDidSelect)
            self.configureActivityDidSelect(dicData,YES,nil);
    }
    else
    {
//        TblActivityDates *objActivitySection = arrActivityData[indexPath.section];
//        NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
        NSManagedObject *objCoreData = (NSManagedObject *)arrActivityData[indexPath.row];

        if (self.configureActivityDidSelect)
            self.configureActivityDidSelect(nil,NO,objCoreData);
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // Stop video here when cell invisible from table view...
    if ([cell isEqual:cellActivityVideo] && btnActive.selected)
        [self stopAllreadyPlayedVideo];
}

#pragma mark - Configure ActivityAddPhotoCell
#pragma mark -

- (void)configurePhotoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityPhoto *)objActivityPhoto
{
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = YES;
    cell.imgAddedPhoto.hidden = NO;
    
    cell.btnDelete.hidden = ![objActivityPhoto.user_id isEqualToString:appDelegate.loginUser.user_id];

    [cell setImageHeightWidth:objActivityPhoto.img_width Height:objActivityPhoto.img_hieght PhotoActivity:YES];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityPhoto.activity_post_time];
    
    [cell.lblPhotoCaption hideByHeight:![objActivityPhoto.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityPhoto.caption;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityPhoto.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [cell.imgAddedPhoto setImageWithURL:[NSURL URLWithString:objActivityPhoto.act_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityPhoto.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivityPhoto.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityPhoto.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityPhoto.user_name;
    NSString *strSession = objActivityPhoto.session_name;
    NSString *strActText = objActivityPhoto.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];

    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityPhoto.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
         [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
    }
    
    cell.imgAddedPhoto.userInteractionEnabled = YES;
    [cell.doubleTapeGesture addTarget:self action:@selector(imgDoubleTapGesture:)];
    cell.doubleTapeGesture.numberOfTapsRequired = 2;
 
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         if (!objActivityPhoto.isAdminActivity.boolValue)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        
        if (!objActivityPhoto.isAdminActivity.boolValue)
        {
            if (appDelegate.configureBtnUserNameSelect)
                appDelegate.configureBtnUserNameSelect(strName,NO);
        }
        
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityPhoto.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityPhoto.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityPhoto.is_like = @YES;
        }
        
        objActivityPhoto.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
        [appDelegate likeActivity:objActivityPhoto.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityPhoto.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        if (self.configureBtnCommentCLK)
            self.configureBtnCommentCLK(objActivityPhoto.activity_id,objActivityPhoto.activity_main_type);
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityPhoto.activity_id completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 [TblActivityPhoto deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityPhoto.activity_id]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self fetchAllActivityFromLocal];
             }
         }];
    }];
}

-(void)imgDoubleTapGesture:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:tblActivity];
    NSIndexPath *tappedIndexPath = [tblActivity indexPathForRowAtPoint:point];
    
    ActivityAddPhotoCell *cell = [tblActivity cellForRowAtIndexPath:tappedIndexPath];
    
    cell.imgHeartLikeZoom.hidden = NO;
    cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 8.0, 8.0);} completion:^(BOOL finished){ }];
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);} completion:^(BOOL finished) {cell.imgHeartLikeZoom.hidden = YES;}];

    if (!cell.btnLike.selected)
        [cell.btnLike sendActionsForControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Configure ActivityAddVideoCell
#pragma mark -

- (void)configureVideoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityVideo *)objActivityVideo
{
    cell.objVideoActivity = objActivityVideo;
    [cell setImageHeightWidth:objActivityVideo.video_width Height:objActivityVideo.video_height PhotoActivity:NO];
    
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityVideo.activity_post_time];
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = NO;
    cell.imgAddedPhoto.hidden = YES;
    
    cell.btnDelete.hidden = ![objActivityVideo.user_id isEqualToString:appDelegate.loginUser.user_id];
    
    [cell.lblPhotoCaption hideByHeight:![objActivityVideo.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityVideo.caption;

    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityVideo.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [cell.imgVThumb setImageWithURL:[NSURL URLWithString:objActivityVideo.thumb_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    cell.btnLike.selected = objActivityVideo.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityVideo.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityVideo.total_comments.integerValue < 3];
    
    NSString *strName = objActivityVideo.user_name;
    NSString *strSession = objActivityVideo.session_name;
    NSString *strActText = objActivityVideo.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityVideo.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;

        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
         
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         if (!objActivityVideo.isAdminActivity.boolValue)
         {
         if (appDelegate.configureBtnUserNameSelect)
             appDelegate.configureBtnUserNameSelect(urlString,NO);
         }
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        
        if (!objActivityVideo.isAdminActivity.boolValue)
        {
            if (appDelegate.configureBtnUserNameSelect)
                appDelegate.configureBtnUserNameSelect(strName,NO);
        }
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        [appDelegate openSharingOption:nil VideoURL:objActivityVideo.video_url Video:YES TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityVideo.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityVideo.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityVideo.is_like = @YES;
        }
        
        objActivityVideo.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
        [appDelegate likeActivity:objActivityVideo.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityVideo.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        if (self.configureBtnCommentCLK)
            self.configureBtnCommentCLK(objActivityVideo.activity_id,objActivityVideo.activity_main_type);
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnVideo touchUpInsideClicked:^{
        if (cell.btnVideo.selected)
        {
            // Already Played
            [cell StopVideoPlaying];
            cellActivityVideo = nil;
        }
        else
        {
            if (cellActivityVideo)
                [self stopAllreadyPlayedVideo];
            
            cellActivityVideo = cell;
            cellActivityVideo.tag = 100;
            [cell setUpForVideoPlayer];
        }
    }];
    
    cell.btnMuteUnmute.selected = objActivityVideo.video_mute.boolValue;
    [cell.btnMuteUnmute touchUpInsideClicked:^{
        cell.btnMuteUnmute.selected = !cell.btnMuteUnmute.selected;
        cell.playerView.player.muted = cell.btnMuteUnmute.selected;
        objActivityVideo.video_mute = cell.btnMuteUnmute.selected ? @YES : @NO;
        [[Store sharedInstance].mainManagedObjectContext save];
    }];
    
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityVideo.activity_id completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 [TblActivityVideo deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityVideo.activity_id]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self fetchAllActivityFromLocal];
             }
         }];
    }];
}


#pragma mark - Configure Start Session Activity
#pragma mark -

- (void)configureStartSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityRunSession *)objActivityRun
{
//    cell.mapView = nil ;
    [cell drawMapPath:objActivityRun.runner_path.mutableCopy];
    
    cell.lblCity.text = objActivityRun.city;
    cell.lblSubLocality.text = objActivityRun.sublocality;
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityRun.activity_post_time];
    cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityRun.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityRun.total_comments.integerValue < 3];
    
    cell.lblSessionComTime.text = objActivityRun.session_complete_time;
    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityRun.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityRun.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";

    
    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityRun.position,objActivityRun.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityRun.total_runner];
    cell.lblCity.text = objActivityRun.city;
    cell.lblSubLocality.text = objActivityRun.sublocality;
    
    cell.btnLike.selected = objActivityRun.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityRun.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityRun.user_name;
    NSString *strSession = objActivityRun.session_name;
    NSString *strActText = objActivityRun.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivityRun.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;

        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
    }
    else
    {
        cell.lblComment1.text =cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
         [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         if (appDelegate.configureBtnUserNameSelect)
             appDelegate.configureBtnUserNameSelect(urlString,NO);
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        if (appDelegate.configureBtnUserNameSelect)
            appDelegate.configureBtnUserNameSelect(strName,NO);
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityRun.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityRun.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityRun.is_like = @YES;
        }
        
        objActivityRun.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
        [appDelegate likeActivity:objActivityRun.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityRun.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        if (self.configureBtnCommentCLK)
            self.configureBtnCommentCLK(objActivityRun.activity_id,objActivityRun.activity_main_type);
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];

}

#pragma mark - Configure Complete Session Activity
#pragma mark -

- (void)configureCompleteSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityCompleteSession *)objActivityCom
{
    cell.mapView.hidden = NO;
    cell.imgMapView.hidden = YES;
    [cell drawMapPath:objActivityCom.runner_path.mutableCopy];
    
    cell.lblCity.text = objActivityCom.city;
    cell.lblSubLocality.text = objActivityCom.sublocality;
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityCom.activity_post_time];
    
    if (objActivityCom.position.integerValue < 4)
    {
        cell.imgPosition.hidden = cell.viewPosition.hidden = NO;
        cell.lblWinPositon.text = objActivityCom.position;
    }
    else
        cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityCom.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityCom.total_comments.integerValue < 3];

    cell.lblSessionComTime.text = objActivityCom.session_complete_time;
    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityCom.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityCom.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";
    
    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityCom.position,objActivityCom.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityCom.total_runner];
    cell.lblCity.text = objActivityCom.city;
    cell.lblSubLocality.text = objActivityCom.sublocality;
    
    cell.btnLike.selected = objActivityCom.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityCom.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityCom.user_name;
    NSString *strSession = objActivityCom.session_name;
    NSString *strActText = objActivityCom.activity_text;

    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityCom.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;

        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";

        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;

            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];

    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         if (appDelegate.configureBtnUserNameSelect)
             appDelegate.configureBtnUserNameSelect(urlString,NO);
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        if (appDelegate.configureBtnUserNameSelect)
            appDelegate.configureBtnUserNameSelect(strName,NO);
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];

    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityCom.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityCom.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityCom.is_like = @YES;
        }
        
        objActivityCom.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
        [appDelegate likeActivity:objActivityCom.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityCom.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        if (self.configureBtnCommentCLK)
            self.configureBtnCommentCLK(objActivityCom.activity_id,objActivityCom.activity_main_type);
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}
#pragma mark - Configure SyncedActivityCell
#pragma mark -

- (void)configureSyncedActivityCell:(SyncedActivityCell *)cell Activity:(TblActivitySyncSession *)objActivitySync
{
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivitySync.activity_post_time];
    
    cell.objSessionTime = objActivitySync;
    [cell showSyncUser:objActivitySync.sync_user.mutableCopy];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivitySync.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivitySync.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivitySync.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivitySync.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    cell.lblSessionName.text = objActivitySync.session_name;
    
    NSString *strName = objActivitySync.user_name ? objActivitySync.user_name : @"";
    NSString *strSession = objActivitySync.session_name ? objActivitySync.session_name : @"";
    NSString *strActText = objActivitySync.activity_text ? objActivitySync.activity_text : @"";
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivitySync.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             if (appDelegate.configureBtnUserNameSelect)
                 appDelegate.configureBtnUserNameSelect(urlString,NO);
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
         [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }

    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         if (appDelegate.configureBtnUserNameSelect)
             appDelegate.configureBtnUserNameSelect(urlString,NO);
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        if (appDelegate.configureBtnUserNameSelect)
            appDelegate.configureBtnUserNameSelect(strName,NO);
    }];

    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivitySync.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivitySync.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivitySync.is_like = @YES;
        }
        
        objActivitySync.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
        [appDelegate likeActivity:objActivitySync.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivitySync.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        if (self.configureBtnCommentCLK)
            self.configureBtnCommentCLK(objActivitySync.activity_id,objActivitySync.activity_main_type);
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell startTimer];
    cell.configureSesionSyncTimeOver = ^(BOOL isOver)
    {
        if (objActivitySync)
        {
            [TblActivitySyncSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivitySync.activity_id]];
            [self fetchAllActivityFromLocal];
        }
    };
}


#pragma mark - NSMutableAttributedString
#pragma mark -

-(NSMutableAttributedString *)getAttributeStirng:(NSString *)strStartText andStartRange:(NSInteger)startRange Lable:(UILabel *)lbl strAttString:(NSMutableAttributedString *)text
{
//    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lbl.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(startRange, strStartText.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(lbl.font.pointSize) range: NSMakeRange(0,strStartText.length)];

    return text;
}
    
#pragma mark - Scroll View Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt
{
    [self playVideoAutomatically];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self playVideoAutomatically];
}


- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)ltargetContentOffset
{
    dragging = NO;
    
    if (ltargetContentOffset->y>=scrollView.contentSize.height-240-scrollView.frame.size.height && ltargetContentOffset->y<scrollView.contentOffset.y)
        [self LoadMoreDataFromServer];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
  if (sender.isDragging && sender.contentOffset.y>=sender.contentSize.height-240-sender.frame.size.height && lastScrollOffset<sender.contentOffset.y && !dragging)
    {
        dragging = YES;
        [self LoadMoreDataFromServer];
    }
    
    lastScrollOffset = sender.contentOffset.y;
}


-(void)stopAllreadyPlayedVideo
{
    if (cellActivityVideo)  // Pause Allready played video.....
    {
        if ([cellActivityVideo isKindOfClass:[ActivityAddPhotoCell class]])
        {
           // NSLog(@"View hidden = = %d",cellActivityVideo.viewVideo.hidden);
            
            if (!cellActivityVideo.viewVideo.hidden && cellActivityVideo && cellActivityVideo.tag == 100)
            {
                NSLog(@"stop Video here ==========>>>>>  ");
                [cellActivityVideo StopVideoPlaying];
                cellActivityVideo = nil;
            }
        }
    }
}

-(void)playVideoAutomatically
{
    if (btnActive.selected)
    {
        NSArray *arrVisibleCell = [tblActivity visibleCells];
        NSMutableArray *arrVideoCells = [NSMutableArray new];
        for (int c = 0; arrVisibleCell.count > c; c++)
        {
            UITableViewCell *cell = arrVisibleCell[c];
            if ([cell isKindOfClass:[ActivityAddPhotoCell class]])
            {
                // Get Video cell here....
                ActivityAddPhotoCell *cellVideo = (ActivityAddPhotoCell *)cell;
                if (!cellVideo.viewVideo.hidden)
                    [arrVideoCells addObject:cellVideo];
            }
        }
        
        for(ActivityAddPhotoCell *cell in arrVideoCells)
        {
            CGRect intersect = CGRectIntersection(tblActivity.frame, [tblActivity convertRect:[tblActivity rectForRowAtIndexPath:[tblActivity indexPathForCell:cell]] toView:tblActivity.superview]);
            
            //play here..
            if (![cell isEqual:cellActivityVideo] && !cell.viewVideo.hidden && intersect.size.height > CViewHeight(cell)*0.6) // only if 60% of the cell is visible
            {
                [self stopAllreadyPlayedVideo];
                cellActivityVideo = cell;
                cellActivityVideo.tag = 100;
                [cellActivityVideo setUpForVideoPlayer];
            }
        }
    }
}

#pragma mark - Action Event

-(IBAction)btnTapBarCLk:(UIButton *)sender
{
    [self showBottomAcitvityIndicator:NO];
    
    activityIndicator.hidden = YES;
    [activityIndicator stopAnimating];
    
    iOffset = 0;
    iOffsetActivity = 0;
    
    btnActive.selected = btnLeaderBoard.selected = NO;
    viewActive.hidden = viewLeaderBoard.hidden = YES;
    
    if (sender.tag == 0)
    {
        btnActive.selected = YES;
        viewActive.hidden = NO;
        [viewUser hideByHeight:YES];
        
        if (taskRuningActivityFeed && taskRuningActivityFeed.state == NSURLSessionTaskStateRunning)
            [taskRuningActivityFeed cancel];

        [tblActivity reloadData];
        [self getActivityDataFromServer:YES];
    }
    else
    {
        [self stopAllreadyPlayedVideo];
        btnLeaderBoard.selected = YES;
        viewLeaderBoard.hidden = NO;
        [viewUser hideByHeight:NO];
        
        viewSubUser.hidden = NO;
        
        if (taskRuningLeaderBoard && taskRuningLeaderBoard.state == NSURLSessionTaskStateRunning)
            [taskRuningLeaderBoard cancel];
        
        [tblActivity reloadData];
        [self getLeaderBoardFromServer];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


@end
