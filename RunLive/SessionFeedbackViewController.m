//
//  SessionFeedbackViewController.m
//  RunLive
//
//  Created by mac-0005 on 26/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "SessionFeedbackViewController.h"

@interface SessionFeedbackViewController ()

@end

@implementation SessionFeedbackViewController
{
    UIImage *imgSelectedScreenshot;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    btnSendFeedback.layer.cornerRadius = 3;
     self.automaticallyAdjustsScrollViewInsets = NO;
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma TextView Delegate methods
- (void)textViewDidChangeSelection:(UITextView *)textView
{
    viewPlaceHolder.hidden = textView.text.length > 0;
}

#pragma mark - Keyboard method
- (void)keyboardDidShow: (NSNotification *) notif
{
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    float bottomSpace = keyboardSize.height - (Is_iPhone_X ? 80 : 105);
    [btnSendFeedback setConstraintConstant:bottomSpace toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
}

- (void)keyboardWillHide: (NSNotification *) notif
{
    [btnSendFeedback setConstraintConstant:15 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
}

#pragma mark - Action Event

-(IBAction)btnCancelCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)btnAddRemoveImage:(UIButton *)sender
{
    if (sender.selected)
    {
        imgSelectedScreenshot = nil;
        [btnMainAttachment setTitle:@"Attach a screenshot" forState:UIControlStateNormal];
        btnIndicationAttachment.selected = NO;
    }
    else
    {
        [self btnAddAttachmentCLK:btnMainAttachment];
    }
}

-(IBAction)btnAddAttachmentCLK:(UIButton *)sender
{
    [self presentPhotoLibrayWithfullImageDetail:^(UIImage *image, NSDictionary *dicImageInfo) {
        if (image)
        {
            NSURL *refURL = [dicImageInfo valueForKey:UIImagePickerControllerReferenceURL];
            PHFetchResult *resultAsset = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
            NSString *filename = [[resultAsset firstObject] filename];
            
            imgSelectedScreenshot = image;
            [btnMainAttachment setTitle:filename forState:UIControlStateNormal];
            btnIndicationAttachment.selected = YES;
        }
            
    } AllowEditing:NO];
}

-(IBAction)btnRateUSCLK:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:CAppStoreUrl]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CAppStoreUrl] options:@{} completionHandler:nil];
}

-(IBAction)btnSendFeedbackCLK:(id)sender
{
    [appDelegate hideKeyboard];
    if (![txtViewFeedback.text isBlankValidationPassed])
    {
        [self customAlertViewWithOneButton:@"" Message:@"Please enter your feedback." ButtonText:@"OK" Animation:YES completed:nil];
    }
    else
    {
        // call api here...
        [[APIRequest request] giveSessionFeedback:self.strSessionId Message:txtViewFeedback.text SessionType:@"1" Image:imgSelectedScreenshot completed:^(id responseObject, NSError *error) {
            if (responseObject && !error)
            {
                [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:^{
                    [self btnCancelCLK:nil];
                }];
            }
        }];
    }
}


@end
