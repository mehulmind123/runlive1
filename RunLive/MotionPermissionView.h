//
//  MotionPermissionView.h
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MotionPermissionView : UIView

@property (nonatomic,strong) IBOutlet UIButton
*btnMotionGoForIt,
*btnMotionNotNow;

+ (id)initMotionPermissionView;

@end
