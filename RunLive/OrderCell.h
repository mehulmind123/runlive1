//
//  OrderCell.h
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UILabel *lblDate, *lblPoints,*lblStatus;
@end
