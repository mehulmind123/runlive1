//
//  TblChats+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 19/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblChats+CoreDataProperties.h"

@implementation TblChats (CoreDataProperties)

+ (NSFetchRequest<TblChats *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblChats"];
}

@dynamic channelid;
@dynamic isChannelDelete;
@dynamic isChatDeleted;
@dynamic isgroup;
@dynamic isOnline;
@dynamic isReadMessage;
@dynamic lastMessage;
@dynamic lastRead;
@dynamic msg_deleted_timestamp;
@dynamic msg_time;
@dynamic priority_index;
@dynamic typingMessage;
@dynamic typingUser;
@dynamic loginuser;
@dynamic participants;

@end
