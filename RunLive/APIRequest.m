
//
//  APIRequest.m
//  AFNetworking iOS Example
//
//  Created by mac-0001 on 5/11/14.
//  Copyright (c) 2014 uVite LLC. All rights reserved.
//

#import "APIRequest.h"
#import "MIAFNetworking.h"

static APIRequest *request = nil;

@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        request = [[APIRequest alloc] init];
        [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
    });
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser ? appDelegate.loginUser.user_token : @"" forHTTPHeaderField:@"token"];

    return request;
}

-(NSString *)setBaseUrlWithTag:(NSString *)strTag
{
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",BASEURL,strTag]);
    return [NSString stringWithFormat:@"%@%@",BASEURL,strTag];
}

#pragma mark - LRF
#pragma mark

- (void)login:(NSString *)emailOrUsername andPassword:(NSString *)password andFacebookID:(NSString *)fb_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
    if (emailOrUsername)
        [dicData setObject:emailOrUsername forKey:@"email"];
    
    if (password)
        [dicData setObject:password forKey:@"password"];
    
    if (fb_id)
        [dicData setObject:fb_id forKey:@"fb_id"];
    

    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CLOGIN] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];

         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CLOGIN])
         {
             [self saveLoginUserData:[responseObject valueForKey:CJsonData]];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
    failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CLOGIN];
     }];
}

- (void)signup:(NSString *)email andUserName:(NSString *)user_name andPassword:(NSString *)password andFacebookID:(NSString *)fb_id FirstName:(NSString *)first_name LastName:(NSString *)last_name Type:(NSString *)strSingUpType completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
    if (email)
        [dicData setObject:email forKey:@"email"];
    
    if (first_name)
        [dicData setObject:first_name forKey:@"first_name"];

    if (strSingUpType)
        [dicData setObject:strSingUpType forKey:@"login_type"];

    if (last_name)
        [dicData setObject:last_name forKey:@"last_name"];

    if (password)
        [dicData setObject:password forKey:@"password"];

    if (user_name)
        [dicData setObject:user_name forKey:@"user_name"];
    
    if (fb_id)
        [dicData setObject:fb_id forKey:@"fb_id"];

    NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    
    if (countryCode)
        [dicData setObject:countryCode forKey:@"country"];
    else
        [dicData setObject:@"" forKey:@"country"];
    
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSIGNUP] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSIGNUP])
         {
             // Store for social signup...
             if ([[responseObject valueForKey:CJsonExtra] numberForJson:@"login_type"].integerValue != 1)
                 [self saveLoginUserData:[responseObject valueForKey:CJsonData]];
         
             if (completion)
                 completion(responseObject,nil);
         }
     }
    failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CSIGNUP];
     }];
}

-(void)updateProfile:(NSString *)first_name LastName:(NSString *)last_name DOB:(NSString *)date_of_birth Weight:(NSString *)weight Height:(NSString *)height Gender:(NSString *)gender Notification:(NSString *)notification Sound:(NSString *)sound Vibrate:(NSString *)vibrate Audio:(NSString *)audio Units:(NSString *)units ProfileVisibility:(NSString *)profile_visibility Subscription:(NSString *)subscription MuteVoiceChat:(NSString *)strChatType VoiceChatEnable:(NSString *)strVoiceChatEnable AboutUs:(NSString *)about_us Link:(NSString *)strLink completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
    if (first_name)
        [dicData setObject:first_name forKey:@"first_name"];
    else
        [dicData setObject:appDelegate.loginUser.first_name forKey:@"first_name"];
   
    if (subscription)
        [dicData setObject:subscription forKey:@"subscription"];
    else
        [dicData setObject:appDelegate.loginUser.subscription forKey:@"subscription"];
    
    if (about_us)
        [dicData setObject:about_us forKey:@"about_us"];
    else
        [dicData setObject:appDelegate.loginUser.about_us forKey:@"about_us"];
    
    if (strLink)
        [dicData setObject:strLink forKey:@"website_url"];
    else
        [dicData setObject:appDelegate.loginUser.link forKey:@"website_url"];
    
    if (last_name)
        [dicData setObject:last_name forKey:@"last_name"];
    else
        [dicData setObject:appDelegate.loginUser.last_name forKey:@"last_name"];

    if (date_of_birth)
        [dicData setObject:date_of_birth forKey:@"date_of_birth"];
    else
        [dicData setObject:appDelegate.loginUser.date_of_birth forKey:@"date_of_birth"];

    if (weight)
        [dicData setObject:weight forKey:@"weight"];
    else
        [dicData setObject:appDelegate.loginUser.weight forKey:@"weight"];
    
    if (height)
        [dicData setObject:height forKey:@"height"];
    else
        [dicData setObject:appDelegate.loginUser.height forKey:@"height"];
    
    if (gender)
        [dicData setObject:gender forKey:@"gender"];
    else
        [dicData setObject:appDelegate.loginUser.gender forKey:@"gender"];

    if (strChatType)
        [dicData setObject:strChatType forKey:@"mute_voice_chat"];
    else
        [dicData setObject:appDelegate.loginUser.muteVoiceChat.boolValue ? @"on" : @"off" forKey:@"mute_voice_chat"];
    
    if (strVoiceChatEnable)
        [dicData setObject:strVoiceChatEnable forKey:@"is_voice_enable"];
    else
        [dicData setObject:appDelegate.loginUser.isVoiceChatEnable.boolValue ? @"on" : @"off" forKey:@"is_voice_enable"];

    if (notification)
        [dicData setObject:notification forKey:@"notification"];
    else
        [dicData setObject:appDelegate.loginUser.notification.boolValue ? @"on" : @"off" forKey:@"notification"];
 
    
    if (sound)
        [dicData setObject:sound forKey:@"sound"];
    else
        [dicData setObject:appDelegate.loginUser.notification_sound.boolValue ? @"on" : @"off" forKey:@"sound"];

    
    if (vibrate)
        [dicData setObject:vibrate forKey:@"vibrate"];
    else
        [dicData setObject:appDelegate.loginUser.notification_vibrate.boolValue ? @"on" : @"off" forKey:@"vibrate"];

    
    if (audio)
        [dicData setObject:audio forKey:@"audio"];
    else
        [dicData setObject:appDelegate.loginUser.audio forKey:@"audio"];
    
    if (units)
        [dicData setObject:units forKey:@"units"];
    else
        [dicData setObject:appDelegate.loginUser.units forKey:@"units"];

    if (profile_visibility)
        [dicData setObject:profile_visibility forKey:@"profile_visibility"];
    else
        [dicData setObject:appDelegate.loginUser.profile_visibility forKey:@"profile_visibility"];
        
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUPDATEPROFILE] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUPDATEPROFILE])
         {
             [self saveLoginUserData:[responseObject valueForKey:CJsonData]];
             if (completion)
                 completion(responseObject,nil);
         }
     }
        failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self updateProfile:first_name LastName:last_name DOB:date_of_birth Weight:weight Height:height Gender:gender Notification:notification Sound:sound Vibrate:vibrate Audio:audio Units:units ProfileVisibility:profile_visibility Subscription:subscription MuteVoiceChat:strChatType VoiceChatEnable:strVoiceChatEnable AboutUs:nil Link:nil completed:nil];
     }];
}

-(void)connectWithStrava:(NSString *)strStravaId completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCONNECTSTRAVA] parameters:@{@"strava_id" : strStravaId} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCONNECTSTRAVA])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCONNECTSTRAVA];
     }];
}

-(void)checkSocailIds:(NSString *)social_id Email:(NSString *)strEmail Type:(NSString *)type completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHECKFORSOCAIL] parameters:@{@"social_id" : social_id,@"email":strEmail,@"type":type} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHECKFORSOCAIL])
         {
             if ([[[responseObject valueForKey:CJsonExtra] numberForJson:@"status"] isEqual:@1])
                 [self saveLoginUserData:[responseObject valueForKey:CJsonData]];

             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHECKFORSOCAIL];
     }];
}

-(void)resendEmailVerification:(NSString *)email completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CRESENDEMAIL] parameters:@{@"email" : email} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CRESENDEMAIL])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CRESENDEMAIL];
     }];
}

-(void)ForgotPWD:(NSString *)email completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CFORGOTPASSWORD] parameters:@{@"email" : email} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CFORGOTPASSWORD])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
        failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CFORGOTPASSWORD];
     }];
}

-(void)changePWD:(NSString *)old_password NewPWD:(NSString *)new_password completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHANGEPASSWORD] parameters:@{@"old_password" : old_password,@"password":new_password} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHANGEPASSWORD])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHANGEPASSWORD];
     }];
}

- (void)uploadUserProfilePicture:(UIImage*)image completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSData *imgData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.9)];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    NSString *imgName = [NSString stringWithFormat:@"%f.jpg",currentTimestamp];
    
    if (!imgData)
        return;
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUPDATEPROFILEPICTURE] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imgData name:@"avatar" fileName:imgName mimeType:@"image/jpeg"];
     }success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUPDATEPROFILEPICTURE])
         {
             
             [[SDImageCache sharedImageCache] removeImageForKey:appDelegate.loginUser.picture];
             
             appDelegate.loginUser.picture = [[responseObject objectForKey:CJsonData] stringValueForJSON:@"url"];
             
             NSLog(@"image value is: %@", appDelegate.loginUser.picture);
             
             [[[Store sharedInstance] mainManagedObjectContext] save];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CUPDATEPROFILEPICTURE];
     }];
    
}


-(void)aboutUs:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CABOUTUS] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CABOUTUS])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
    failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CABOUTUS];
     }];
}

-(void)updateProfileStatus:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CPROFILESTATUS] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CPROFILESTATUS])
         {
             appDelegate.loginUser.profileStatus = @YES;
             [[Store sharedInstance].mainManagedObjectContext save];
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CPROFILESTATUS];
     }];
}


-(void)activeSubcription:(NSString *)type RecieptId:(NSString *)receipt_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicPara = [NSMutableDictionary new];
    [dicPara setObject:type forKey:@"type"];
    
    if (receipt_id)
        [dicPara setObject:receipt_id forKey:@"receipt_id"];
    
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSUBSCRIPTIONPLAN] parameters:dicPara success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSUBSCRIPTIONPLAN])
         {
             //appDelegate.loginUser.profileStatus = @YES;
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             appDelegate.loginUser.receipt_id = [dicData stringValueForJSON:@"receipt_id"];
             appDelegate.loginUser.subscription = [dicData stringValueForJSON:@"subscription"];
             appDelegate.loginUser.subscription_exp_date = [dicData stringValueForJSON:@"subscription_exp_date"];
             appDelegate.loginUser.subscription_plan = [dicData numberForJson:@"subscription_plan"];
             appDelegate.loginUser.subscription_trial = [dicData numberForJson:@"subscription_trial"];
             [[Store sharedInstance].mainManagedObjectContext save];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self activeSubcription:type RecieptId:receipt_id completed:nil];
     }];
}

-(void)updateDateOnServerForIAP:(double)timeStamp completed:(void (^)(id responseObject,NSError *error))completion
{
    //[GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicPara = [NSMutableDictionary new];
    [dicPara setObject:[NSString stringWithFormat:@"%f",timeStamp] forKey:@"subscription_exp_date"];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSUBSCRIPTIONPLANRENEW] parameters:dicPara success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSUBSCRIPTIONPLANRENEW])
         {
             //appDelegate.loginUser.profileStatus = @YES;
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             appDelegate.loginUser.subscription_exp_date = [dicData stringValueForJSON:@"subscription_exp_date"];
             [[Store sharedInstance].mainManagedObjectContext save];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self updateDateOnServerForIAP:timeStamp completed:nil];
     }];
}

-(void)checkRecieptID:(NSString *)receipt_id completed:(void (^)(id responseObject,NSError *error))completion
{
    if (!receipt_id) {
        return;
    }
    
    [GiFHUD showWithOverlay];
    
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSUBSCRIPTIONCHECKRECIEPTID] parameters:@{@"receipt_id" : receipt_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSUBSCRIPTIONCHECKRECIEPTID])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
              failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self checkRecieptID:receipt_id completed:nil];
     }];
}

#pragma mark - Reward Related Functions
#pragma mark
-(void)orderDetailWithOrderID:(NSString *)product_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CORDERTDETAILS] parameters:@{@"order_id":product_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CORDERTDETAILS])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CORDERTDETAILS];
     }];
}

-(void)getOrderList:(NSString *)offset completed:(void (^)(id responseObject,NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CORDERTLIST] parameters:@{@"offset":offset} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CORDERTLIST])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CORDERTLIST];
     }];

}

-(void)productList:(void (^)(id responseObject,NSError *error))completion
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CPRODUCTLIST] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CPRODUCTLIST])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
        failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CPRODUCTLIST];
     }];
}

-(void)basketList:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CBASKETLIST] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CBASKETLIST])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CBASKETLIST];
     }];

}

-(void)addProductToBasket:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CADDPRODUCT] parameters:@{@"productId":productId} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CADDPRODUCT])
         {
             appDelegate.loginUser.reward_point = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"reward_point"];
             [[Store sharedInstance].mainManagedObjectContext save];
             if (completion)
                 completion(responseObject,nil);
         }
     }
              failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CADDPRODUCT];
     }];
}

-(void)removeProductFromBasket:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CREMOVEPRODUCT] parameters:@{@"basketId":productId} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CREMOVEPRODUCT])
         {
             appDelegate.loginUser.reward_point = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"reward_point"];
             [[Store sharedInstance].mainManagedObjectContext save];

             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CREMOVEPRODUCT];
     }];
}

-(NSURLSessionDataTask *)productDetails:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion;
{
    
    return [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CRODUCTDETAILS] parameters:@{@"productId":productId} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CRODUCTDETAILS])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:NO api:CRODUCTDETAILS];
     }];
}

-(void)getLastAddress:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CGETLASTADDRESS] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CGETLASTADDRESS])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CGETLASTADDRESS];
     }];
}

-(void)GenerateOrder:(NSString *)name ForName:(NSString *)fore_name Street:(NSString *)street Number:(NSString *)number City:(NSString *)city PostCode:(NSString *)post_code Country:(NSString *)country PhoneNumber:(NSString *)phone_number State:(NSString *)stateprovinceRegion completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData = [NSMutableDictionary new];
    [dicData setObject:name forKey:@"name"];
    [dicData setObject:fore_name forKey:@"fore_name"];
    [dicData setObject:street forKey:@"street"];
    [dicData setObject:number forKey:@"number"];
    [dicData setObject:city forKey:@"city"];
    [dicData setObject:stateprovinceRegion forKey:@"state"];
    [dicData setObject:post_code forKey:@"post_code"];
    [dicData setObject:country forKey:@"country"];
    [dicData setObject:phone_number forKey:@"phone_number"];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CGENERATEORDER] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CGENERATEORDER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CGENERATEORDER];
     }];
}

#pragma mark - User Related APi
#pragma mark

-(void)otherUserProfile:(NSString *)user_name showLoader:(BOOL)showLoader completed:(void (^)(id responseObject,NSError *error))completion;
{
    if (showLoader)
        [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:COTHERPROFILE] parameters:@{@"user_name":user_name} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:COTHERPROFILE])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);

         [self actionOnAPIFailure:error showAlert:YES api:COTHERPROFILE];
     }];
}

-(void)BlockList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CBLOCKLIST] parameters:@{@"offset":offset,@"limit":@"20"} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CBLOCKLIST])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];
}

-(void)addBlockUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CBLOCKUSER] parameters:@{@"user_id":user_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CBLOCKUSER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CBLOCKUSER];
     }];
}

-(void)unblockUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUNBLOCKUSER] parameters:@{@"user_id":user_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUNBLOCKUSER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CUNBLOCKUSER];
     }];
}

-(void)followUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CFOLLOWUSER] parameters:@{@"user_id":user_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CFOLLOWUSER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CFOLLOWUSER];
     }];
}

-(void)unfollowUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUNFOLLOWUSER] parameters:@{@"user_id":user_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUNFOLLOWUSER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CUNFOLLOWUSER];
     }];
}

#pragma mark - Activity Feed
#pragma mark

-(void)GetActivityCommentList:(NSNumber *)offset ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion
{
      [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CACTIVITYCOMMENTLIST] parameters:@{@"offset":offset,@"limit":@"20",@"activity_id":activity_id} success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 [GiFHUD dismiss];
                 if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CACTIVITYCOMMENTLIST])
                 {
                     if (completion)
                         completion(responseObject,nil);
                 }
             }
                                    failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 [GiFHUD dismiss];
                 if (completion)
                     completion(nil,error);
             }];
}


-(NSURLSessionDataTask *)GetActivityFeedList:(NSNumber *)offset Type:(NSString *)type UserName:(NSString *)user_name completed:(void (^)(id responseObject,NSError *error))completion;
{
    NSMutableDictionary *dicData = [NSMutableDictionary new];
    
    if (type)
        [dicData setObject:type forKey:@"type"];
    
    if (user_name)
        [dicData setObject:user_name forKey:@"user_name"];
    
    if (offset)
        [dicData setObject:offset forKey:@"offset"];
    
    return  [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CACTIVITYFEED] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 [GiFHUD dismiss];
                 if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CACTIVITYFEED])
                 {
                     // STORE ALL ACTIVITY IN LOCAL....
                     [self saveActivityToLocal:[responseObject valueForKey:CJsonData] Offset:offset Type:type];
                     if (completion)
                         completion(responseObject,nil);
                 }
             }
                                          failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 [GiFHUD dismiss];
                 if (completion)
                     completion(nil,error);
             }];
}

-(void)addCommentOnActivity:(NSString *)text ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion;
{
    //    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CADDCOMMENTONACTIVITY] parameters:@{@"activity_id":activity_id,@"message":text} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CADDCOMMENTONACTIVITY])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CADDCOMMENTONACTIVITY];
     }];
}

-(void)likeActivityFeed:(NSString *)activity_id Status:(NSString *)status ActivityType:(NSString *)strActType completed:(void (^)(id responseObject,NSError *error))completion;
{
    //    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CLIKEACTIVITY] parameters:@{@"activity_id":activity_id,@"status":status} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CLIKEACTIVITY])
         {
             if (completion)
                 completion(responseObject,nil);
         }
         else
         {
             [self deleteActivityFromLocal:activity_id Type:strActType];
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         
         if (completion)
             completion(nil,error);
         
         
         [self actionOnAPIFailure:error showAlert:YES api:CLIKEACTIVITY];
     }];
}

-(void)deleteActivity:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion
{
    //    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDELETEACTIVITY] parameters:@{@"activity_id":activity_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDELETEACTIVITY])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
              failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CDELETEACTIVITY];
     }];
}

-(void)activityDetailWithType:(NSString *)type ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion
{
//        [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CACTIVITYDETAILS] parameters:@{@"activity_id":activity_id,@"type":type} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CACTIVITYDETAILS])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CACTIVITYDETAILS];
     }];
}


#pragma mark - LeaderBoard
#pragma mark
-(NSURLSessionDataTask *)serachUser:(NSNumber *)offset KeyWord:(NSString *)searchKeyword completed:(void (^)(id responseObject,NSError *error))completion;
{
    return  [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSEARCHUSER] parameters:@{@"offset":offset,@"limit":@"20",@"keyword" : searchKeyword} success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 [GiFHUD dismiss];
                 if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSEARCHUSER])
                 {
                     if (completion)
                         completion(responseObject,nil);
                 }
             }
                failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 [GiFHUD dismiss];
                 if (completion)
                     completion(nil,error);
             }];
}

-(NSURLSessionDataTask *)GetLeaderboardList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion;
{
    return  [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CLEADERBOARD] parameters:@{@"offset":offset,@"limit":@"20"} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CLEADERBOARD])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];
}

-(void)syncContactWithServer:(NSArray *)arrContact FromFacebook:(BOOL)isFacebook completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData  = [[NSMutableDictionary alloc] init];
    
    if (isFacebook)
        [dicData setObject:arrContact forKey:@"social_contact"];
    else
        [dicData setObject:arrContact forKey:@"phone_contact"];
    
    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];

    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CINVITEFRIEND] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CINVITEFRIEND])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];

}


#pragma mark - Chat related Api
#pragma mark

-(void)RegisterChannel:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;
{
    //[GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CREGISTERCHANNEL] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         //[GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CREGISTERCHANNEL])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         //[GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CREGISTERCHANNEL];
     }];
}

-(void)MessageSend:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATSEND] parameters:@{@"channel_id":channel_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHATSEND])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHATSEND];
     }];
}

-(void)ReadMessage:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion
{
//  [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATREAD] parameters:@{@"channel_id":channel_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:NO data:responseObject api:CCHATREAD])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHATREAD];
     }];
}

-(NSURLSessionDataTask *)ChannelList:(BOOL)isFirstTime completed:(void (^)(id responseObject,NSError *error))completion;
{
//    [GiFHUD showWithOverlay];
    return [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATLIST] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHATLIST])
         {
             if ([responseObject objectForKey:CJsonData] && [[responseObject objectForKey:CJsonData] isKindOfClass:[NSArray class]])
             {
                 [self saveChatUserListToLocal:(NSArray*)[responseObject objectForKey:CJsonData] FirstTime:isFirstTime];
             }
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:NO api:CCHATLIST];
     }];
}

-(void)ChannelDetail:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion
{
    //    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATLIST] parameters:@{@"channel_id":channel_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHATLIST])
         {
             if ([responseObject objectForKey:CJsonData] && [[responseObject objectForKey:CJsonData] isKindOfClass:[NSArray class]])
             {
//                 [TblChats deleteAllObjects];
//                 //                 [TblParticipants deleteAllObjects];
//                 
                 [self saveChatUserListToLocal:(NSArray*)[responseObject objectForKey:CJsonData] FirstTime:YES];
             }
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHATLIST];
     }];
}

-(void)UploadChatImage:(UIImage *)image completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    NSData *imgData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.8)];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    NSString *imgName = [NSString stringWithFormat:@"%f.jpg",currentTimestamp];
    
    if (!imgData)
        return;
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATIMAGE] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imgData name:@"image" fileName:imgName mimeType:@"image/jpeg"];
         
     }success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHATIMAGE])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHATIMAGE];
     }];
}

-(void)ChatMember:(NSString *)channel_id UserId:(NSArray *)user_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCHATMEMBER] parameters:@{@"channel_id":channel_id,@"user_ids":user_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCHATMEMBER])
         {
             
             [self saveChatUserListToLocal:(NSArray*)[responseObject objectForKey:CJsonData] FirstTime:YES];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCHATMEMBER];
     }];
}

-(void)deleteUserFromChannel:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDELETEUSERFROMCHANNEL] parameters:@{@"channel_id":channel_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDELETEUSERFROMCHANNEL])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CDELETEUSERFROMCHANNEL];
     }];
}


#pragma mark - Notification Related
#pragma mark

-(void)checkForNotificationBadge:(void (^)(id responseObject, NSError *error))completion
{
    if (!appDelegate.loginUser)
        return;
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CBADGENOTIFICATIONBADGE] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CBADGENOTIFICATIONBADGE])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self actionOnAPIFailure:error showAlert:NO api:CBADGENOTIFICATIONBADGE];
     }];
}

-(void)registerDeviceTokenOnserver:(NSString *)device_token completed:(void (^)(id responseObject,NSError *error))completion
{
    if (!device_token)
    {
        NSLog(@"Unable to found device token ========== >>>");
        return;
    }
    else
    {
        NSLog(@"Succefully found device token ========== >>>");
    }
    
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CREGISTERDEVCIETOKEN] parameters:@{@"device_token":device_token} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CREGISTERDEVCIETOKEN])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CREGISTERDEVCIETOKEN];
     }];
}

-(void)removeDeviceTokenFromServer:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CLOGOUTUSER] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:NO data:responseObject api:CLOGOUTUSER])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:NO api:CLOGOUTUSER];
     }];
}

-(NSURLSessionDataTask *)NotificationList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion
{
    NSMutableDictionary *dicData = [NSMutableDictionary new];
    
    [dicData addObject:@"20" forKey:@"limit"];
    [dicData addObject:offset forKey:@"offset"];
    
    return [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CNOTIFICATIONLIST] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CNOTIFICATIONLIST])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil,error);
     }];
}

-(void)readNotification:(NSString *)notification_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CNOTIFICATIONREAD] parameters:@{@"notification_id":notification_id} success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CNOTIFICATIONREAD])
                {
                    if (completion)
                        completion(responseObject,nil);
                }
            }
              failure:^(NSURLSessionDataTask *task, NSError *error)
            {
//                if (completion)
//                    completion(nil,error);
            }];
}

-(void)readUnClickableNotification:(void (^)(id responseObject,NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CNOTIFICATIONREADUNCLICKABLE] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CNOTIFICATIONREADUNCLICKABLE])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         //                if (completion)
         //                    completion(nil,error);
     }];
}

#pragma mark - Session related Api
#pragma mark -

-(void)giveSessionFeedback:(NSString *)session_id Message:(NSString *)message SessionType:(NSString *)session_type Image:(UIImage *)imgAttachment completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    NSData *imgData = [NSData dataWithData:UIImageJPEGRepresentation(imgAttachment, 0.9)];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    NSString *imgName = [NSString stringWithFormat:@"%f.jpg",currentTimestamp];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSESSIONFEEDBACK] parameters:@{@"session_id":session_id,@"message":message,@"session_type":session_type} constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (imgData) {
             [formData appendPartWithFileData:imgData name:@"file" fileName:imgName mimeType:@"image/jpeg"];
         }
         
     }success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSESSIONFEEDBACK])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CSESSIONFEEDBACK];
     }];
    
}


-(NSURLSessionDataTask *)friendList:(NSString *)keyword loader:(BOOL)shouldShowLoader completed:(void (^)(id responseObject,NSError *error))completion;
{
    if (shouldShowLoader)
        [GiFHUD showWithOverlay];
    
    NSMutableDictionary *dicData = [NSMutableDictionary new];
    if (keyword)
        [dicData addObject:keyword forKey:@"keyword"];
    
    
    return [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CFRIENDLIST] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 [GiFHUD dismiss];
                 if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CFRIENDLIST])
                 {
                     if (completion)
                         completion(responseObject,nil);
                 }
             }
                                          failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 [GiFHUD dismiss];
                 if (completion)
                     completion(nil,error);
             }];
}

-(void)createLiveSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCREATELIVESESSION] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCREATELIVESESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];

         if (completion)
             completion(nil,error);

         [self actionOnAPIFailure:error showAlert:YES api:CCREATELIVESESSION];
     }];
    
}

-(void)quitLiveSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CQUITLIVESESSION] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
//         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CQUITLIVESESSION])
//         {
             if (completion)
                 completion(responseObject,nil);
//         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self quitLiveSession:session_id completed:nil];
     }];
}

-(void)restartSearching:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CRSTARTSEARCHING] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CRSTARTSEARCHING])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CRSTARTSEARCHING];
     }];
}

-(void)createSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];

    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];

    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCREATESESSION] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCREATESESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CCREATESESSION];
     }];
    
}

-(void)EditSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];
 
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CEDITSESSION] parameters:dicData success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CEDITSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CEDITSESSION];
     }];
}

-(void)addFriendInSession:(NSString *)session_id Friends:(NSArray *)user_ids completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] sessionManager].responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [[[MIAFNetworking sharedInstance] sessionManager] setRequestSerializer:[AFJSONRequestSerializer  serializer]];
    
    [[[[MIAFNetworking sharedInstance] sessionManager] requestSerializer] setValue:appDelegate.loginUser.user_token forHTTPHeaderField:@"token"];

    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CADDPLAYERSESSION] parameters:@{@"session_id":session_id,@"user_ids":user_ids} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CADDPLAYERSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CADDPLAYERSESSION];
     }];
}

-(void)GETSessionList:(void (^)(id responseObject,NSError *error))completion
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSESSIONLIST] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSESSIONLIST])
         {
             [self SaveSessionListToLocal:[responseObject valueForKey:CJsonData]];
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];
}


-(void)GETSessionDetail:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSESSIONLIST] parameters:@{@"session_id" : session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSESSIONLIST])
         {
//             [self SaveSessionListToLocal:[responseObject valueForKey:CJsonData]];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];
}

-(void)JoinSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CJOINSESSION] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CJOINSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CJOINSESSION];
     }];
}


-(void)DeclineSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDECLINESESSION] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDECLINESESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         [self actionOnAPIFailure:error showAlert:YES api:CDECLINESESSION];
     }];
}

-(void)SyncedSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CSYNCEDSESSION] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CSYNCEDSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);

         [self actionOnAPIFailure:error showAlert:YES api:CSYNCEDSESSION];
     }];
}

-(void)DeleteSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDELETESESSION] parameters:@{@"session_id":session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDELETESESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CDELETESESSION];
     }];
}

-(void)addLocalityAndSubLocality:(NSString *)locality SubLocality:(NSString *)sub_locality SessionId:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    //    [GiFHUD showWithOverlay];
    
    locality = locality ? locality : @"";
    sub_locality = sub_locality ? sub_locality : @"";

    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CADDLOCALITY] parameters:@{@"session_id":session_id,@"locality":locality,@"sub_locality":sub_locality} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
//         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CADDLOCALITY])
//         {
             if (completion)
                 completion(responseObject,nil);
//         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
//         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
//         [self actionOnAPIFailure:error showAlert:YES api:CADDLOCALITY];
     }];
}

-(void)deleteActivityComment:(NSString *)comment_id completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDELETESEACTIVITYCOMMENT] parameters:@{@"comment_id":comment_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDELETESEACTIVITYCOMMENT])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CDELETESEACTIVITYCOMMENT];
     }];
}

-(void)deleteSessionComment:(NSString *)comment_id completed:(void (^)(id responseObject,NSError *error))completion
{
        [GiFHUD showWithOverlay];
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CDELETESESSIONCOMMENT] parameters:@{@"comment_id":comment_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CDELETESESSIONCOMMENT])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CDELETESESSIONCOMMENT];
     }];
}

-(void)addCommentOnSession:(NSString *)text SessionId:(NSString *)session_id UserName:(NSArray *)user_names completed:(void (^)(id responseObject,NSError *error))completion
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CADDCOMMENTONSESSION] parameters:@{@"session_id":session_id,@"text":text,@"user_names":user_names} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CADDCOMMENTONSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CADDCOMMENTONSESSION];
     }];
}

-(void)getCommentListOfSession:(NSString *)session_id Offset:(NSString *)offset completed:(void (^)(id responseObject,NSError *error))completion;
{
//    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCOMMENTLISTOFSESSION] parameters:@{@"session_id" : session_id,@"offset":offset} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCOMMENTLISTOFSESSION])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
          [self actionOnAPIFailure:error showAlert:YES api:CADDCOMMENTONSESSION];
     }];
}

-(void)uploadPhotoAndVideo:(NSString *)type SessionID:(NSString *)session_id Attachment:(NSData *)attachment Caption:(NSString *)caption Latitude:(double)latitude Longitude:(double)longitude completed:(void (^)(id responseObject,NSError *error))completion;
{
    if (!attachment)
        return;
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUPLOADMEDIA] parameters:@{@"type":type,@"session_id":session_id,@"latitude":[NSString stringWithFormat:@"%f",latitude],@"longitude":[NSString stringWithFormat:@"%f",longitude],@"caption":caption} constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (type.intValue == 1)
         {
             // photo
             double currentTimestamp = [[NSDate date] timeIntervalSince1970];
             NSString *imgName = [NSString stringWithFormat:@"%f.jpg",currentTimestamp];
             [formData appendPartWithFileData:attachment name:@"attachment" fileName:imgName mimeType:@"image/jpeg"];
         }
         else
         {
             // Video
             NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"VideoFile.mov"];
             [attachment writeToFile:filePath atomically:YES];
             NSURL *filepathURL = [NSURL fileURLWithPath:filePath];
             [formData appendPartWithFileURL:filepathURL name:@"attachment" error:nil];
         }
     }success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUPLOADMEDIA])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         UIViewController *viewController = [appDelegate getTopMostViewController];
         [viewController customAlertViewWithTwoButton:@"" Message:@"Request timed out while uploading media on server. Would you like to retry?" ButtonFirstText:@"Retry" ButtonSecondText:@"Cancel" Animation:YES completed:^(int index) {
             if (index == 1)
             {
                 [self uploadPhotoAndVideo:type SessionID:session_id Attachment:attachment Caption:caption Latitude:latitude Longitude:longitude completed:nil];
             }
         }];
     }];
    
}


#pragma mark - Running Related api
-(NSURLSessionDataTask *)getAllreadyRunningSeesion:(void (^)(id responseObject,NSError *error))completion
{
    //[GiFHUD showWithOverlay];
    
   return [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CGETALLREADYRUNSESSION] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CGETALLREADYRUNSESSION])
         {
             // Store premium status here....
             appDelegate.loginUser.is_premium = [[[responseObject valueForKey:CJsonData] numberForJson:@"premium"] isEqual:@1] ? @YES : @NO;
             [[Store sharedInstance].mainManagedObjectContext save];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
     }];
}

-(void)competeQuitRunning:(NSString *)session_id Type:(NSString *)type RunCompleteType:(NSString *)run_completed_type completed:(void (^)(id responseObject,NSError *error))completion;
{
    [GiFHUD showWithOverlay];
    
//    type: quit/complete
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CRUNNINGQUITCOMPLETE] parameters:@{@"session_id" : session_id,@"type":type,@"run_completed_type":run_completed_type} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CRUNNINGQUITCOMPLETE])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         [self actionOnAPIFailure:error showAlert:YES api:CRUNNINGQUITCOMPLETE];
     }];
}

-(void)runningResultOfSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    //    type: quit/complete
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CRUNNINGRESULT] parameters:@{@"session_id" : session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CRUNNINGRESULT])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CRUNNINGRESULT];
     }];
}

-(void)userCurrentRunningDetail:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CCURRENTRUNNINGDETIAL] parameters:@{@"session_id" : session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         //[GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CCURRENTRUNNINGDETIAL])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
        // [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
//         [self actionOnAPIFailure:error showAlert:YES api:CCURRENTRUNNINGDETIAL];
     }];
}

-(void)userDisqualifyFromRunning:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CUSERDISQUALIFY] parameters:@{@"session_id" : session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CUSERDISQUALIFY])
         {
             if (completion)
                 completion(responseObject,nil);
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CUSERDISQUALIFY];
     }];
}

-(void)toStartManualSessionRunning:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    [[MIAFNetworking sharedInstance] POST:[self setBaseUrlWithTag:CTOSTARTMANNUALSESSION] parameters:@{@"session_id" : session_id} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [GiFHUD dismiss];
//         if ([self checkResponseStatusAndShowAlert:YES data:responseObject api:CTOSTARTMANNUALSESSION])
//         {
             if (completion)
                 completion(responseObject,nil);
//         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [GiFHUD dismiss];
         if (completion)
             completion(nil,error);
         
         [self actionOnAPIFailure:error showAlert:YES api:CTOSTARTMANNUALSESSION];
     }];
}

#pragma mark - Common Functions

-(BOOL)checkResponseStatusAndShowAlert:(BOOL)showAlert data:(NSDictionary *)responseobject api:(NSString *)api
{
    [[PPLoader sharedLoader] HideHudLoader];
    
    if (responseobject && [responseobject isKindOfClass:[NSDictionary class]])
    {
        if([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:CStatusOne])
            return YES;
        
        else if ([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:CStatusNine])
        {
            // Login in another device...
            UIViewController *viewController = [appDelegate getTopMostViewController];
            [viewController customAlertViewWithOneButton:CMessageSorry Message:[responseobject stringValueForJSON:CJsonMessage] ButtonText:@"OK" Animation:YES completed:^{
                [appDelegate logoutUser];
            }];
            
            return NO;
        }
        
        else if ([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:CStatusBlockUser])
        {
            // For block user...
            UIViewController *viewController = [appDelegate getTopMostViewController];
            [viewController.navigationController popToRootViewControllerAnimated:YES];
            [viewController customAlertViewWithOneButton:@"" Message:[responseobject stringValueForJSON:CJsonMessage] ButtonText:@"Ok" Animation:NO completed:nil];
            [appDelegate updateActivityListData:CallActivityApi MainWis:YES];

            return NO;
        }
    }
    
    if (showAlert)
        [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:[responseobject stringValueForJSON:CJsonMessage] ButtonText:@"Ok" Animation:NO completed:nil];
    
    return NO;
}

-(void)actionOnAPIFailure:(NSError *)error showAlert:(BOOL)showAlert api:(NSString *)api
{
    [[PPLoader sharedLoader] HideHudLoader];
    
    if (error.code == -1009 || error.code == -1005)
        showAlert = NO;
    
     if (showAlert)
     {
         dispatch_async(GCDMainThread, ^{
             UIViewController *viewController = [appDelegate getTopMostViewController];
             if (error.code == 3840)
                 [viewController customAlertViewWithOneButton:CMessageSorry Message:@"Please stand by - We are currently working to resolve some issues." ButtonText:@"Ok" Animation:NO completed:nil];
             else
                 [viewController customAlertViewWithOneButton:CMessageSorry Message:error.localizedDescription ButtonText:@"Ok" Animation:NO completed:nil];
         });
    }
    
//    NSLog(@"API Error = %@ == %@",api,error);
}


#pragma mark - CoreData Functions
#pragma mark -

-(void)saveLoginUserData:(NSDictionary *)dic
{
    NSString *strToken = nil;
    if (appDelegate.loginUser && [UIApplication userId])
        strToken = appDelegate.loginUser.user_token;
    
    if ([dic numberForJson:@"_id"])
    {
        appDelegate.loginUser = (TblUser *)[TblUser findOrCreate:@{@"user_id":[dic stringValueForJSON:@"_id"]} context:[[Store sharedInstance] mainManagedObjectContext]];
        
        appDelegate.loginUser.user_id = [dic stringValueForJSON:@"_id"];
        
        if ([[dic stringValueForJSON:@"date_of_birth"] isEqualToString:@"0000-00-00"])
            appDelegate.loginUser.date_of_birth = @"";
        else
            appDelegate.loginUser.date_of_birth = [dic stringValueForJSON:@"date_of_birth"];
        
        appDelegate.loginUser.about_us = [dic stringValueForJSON:@"about_us"];
        appDelegate.loginUser.link = [dic stringValueForJSON:@"website_url"];
        appDelegate.loginUser.email = [dic stringValueForJSON:@"email"];
        appDelegate.loginUser.fb_id = [dic stringValueForJSON:@"fb_id"];
        appDelegate.loginUser.first_name = [dic stringValueForJSON:@"first_name"];
        appDelegate.loginUser.last_name = [dic stringValueForJSON:@"last_name"];
        appDelegate.loginUser.gender = [dic stringValueForJSON:@"gender"];
        appDelegate.loginUser.height = [dic stringValueForJSON:@"height"];
        appDelegate.loginUser.picture = [dic stringValueForJSON:@"picture"];
//        appDelegate.loginUser.picture_path = [dic stringValueForJSON:@"picture_path"];
        appDelegate.loginUser.reward_point = [dic stringValueForJSON:@"reward_point"];
        appDelegate.loginUser.updatedAt = [dic stringValueForJSON:@"updatedAt"];
        appDelegate.loginUser.createdAt = [dic stringValueForJSON:@"createdAt"];
        appDelegate.loginUser.user_name = [dic stringValueForJSON:@"user_name"];
        appDelegate.loginUser.verify = [dic numberForJson:@"verify"];
        appDelegate.loginUser.weight = [dic stringValueForJSON:@"weight"];
        appDelegate.loginUser.status = [dic numberForJson:@"status"];
        appDelegate.loginUser.country = [dic stringValueForJSON:@"country"];
        appDelegate.loginUser.subscription = [dic stringValueForJSON:@"subscription"];
        appDelegate.loginUser.subscription_exp_date = [dic stringValueForJSON:@"subscription_exp_date"];
        appDelegate.loginUser.subscription_plan = [dic numberForJson:@"subscription_plan"];
        appDelegate.loginUser.subscription_trial = [dic numberForJson:@"subscription_trial"];
        appDelegate.loginUser.notification_count = [dic stringValueForJSON:@"notification_count"];
        appDelegate.loginUser.strava_id = [dic stringValueForJSON:@"strava_id"];
        
        if ([dic stringValueForJSON:@"strava_id"] && [[dic stringValueForJSON:@"strava_id"] isBlankValidationPassed])
            appDelegate.loginUser.stravaConnection = @YES;
        else
            appDelegate.loginUser.stravaConnection = @NO;
        
        appDelegate.loginUser.profileStatus = [[dic numberForJson:@"profileStatus"] isEqual:@1] ? @YES : @NO;
        appDelegate.loginUser.is_premium = [[dic numberForJson:@"premium"] isEqual:@1] ? @YES : @NO;
        
        appDelegate.loginUser.receipt_id = [dic stringValueForJSON:@"receipt_id"];
        appDelegate.loginUser.is_default_image = [[dic numberForJson:@"picture_updated"] isEqual:@0] ? @YES : @NO;
        appDelegate.loginUser.muteVoiceChat = [[dic stringValueForJSON:@"mute_voice_chat"] isEqualToString:@"on"] ? @YES : @NO;
        appDelegate.loginUser.isVoiceChatEnable = [[dic stringValueForJSON:@"is_voice_enable"] isEqualToString:@"on"] ? @YES : @NO;;
        
        if (strToken)
            appDelegate.loginUser.user_token = strToken;
        else
            appDelegate.loginUser.user_token = [dic stringValueForJSON:@"token"];
        
        NSDictionary *dicNotification = [dic valueForKey:@"notification_settings"];
        appDelegate.loginUser.notification = [[dicNotification stringValueForJSON:@"notification"] isEqualToString:@"on"] ? @YES : @NO;
        appDelegate.loginUser.notification_sound = [[dicNotification stringValueForJSON:@"sound"] isEqualToString:@"on"] ? @YES : @NO;
        appDelegate.loginUser.notification_vibrate = [[dicNotification stringValueForJSON:@"vibrate"] isEqualToString:@"on"] ? @YES : @NO;
        
        NSDictionary *dicRun = [dic valueForKey:@"run"];
        appDelegate.loginUser.run_performance = [dicRun stringValueForJSON:@"performance"];
        appDelegate.loginUser.run_rank = [dicRun stringValueForJSON:@"rank"];
        appDelegate.loginUser.run_total_calories = [dicRun stringValueForJSON:@"total_calories"];
        appDelegate.loginUser.run_total_miles = [dicRun stringValueForJSON:@"total_distance"];
        appDelegate.loginUser.run_total_runs = [dicRun stringValueForJSON:@"total_runs"];
        appDelegate.loginUser.run_total_BPM = [dicRun stringValueForJSON:@"average_bpm"];
        
        NSDictionary *dicSetting = [dic valueForKey:@"settings"];
        appDelegate.loginUser.audio = [dicSetting numberForJson:@"audio"];
        appDelegate.loginUser.profile_visibility = [dicSetting stringValueForJSON:@"profile_visibility"];
        appDelegate.loginUser.units = [dicSetting stringValueForJSON:@"units"];

        [[[Store sharedInstance] mainManagedObjectContext] save];
        
        [UIApplication setUserId:appDelegate.loginUser.user_id];
        [CUserDefaults setObject:appDelegate.loginUser.user_id forKey:CUserId];
        [CUserDefaults synchronize];
        
        
        NSDateFormatter *dtForm = [[NSDateFormatter alloc] init];
        dtForm.dateFormat = @"dd MMMM yyyy";
        NSDate *userDate = [dtForm dateFromString:appDelegate.loginUser.date_of_birth];
        NSDate *curDate = [NSDate date];
        
        if (userDate)
        {
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian
                                    ];
            NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:userDate toDate:curDate options:0];
            
            NSLog(@"Difference in date components: %li/%li/%li", (long)components.day, (long)components.month, (long)components.year);
            
            if (components)
            {
                appDelegate.loginUser.age = [NSString stringWithFormat:@"%li",(long)components.year];
                [[Store sharedInstance].mainManagedObjectContext save];
            }
        }
    }
}

-(void)SaveSessionListToLocal:(NSArray *)arrSessionList
{
    [TblSessionListDate deleteAllObjects];
    [TblSessionList deleteAllObjects];
    [TblJoinedUser deleteAllObjects];
    [TblSessionOtherUser deleteAllObjects];
    [TblSessionSyncedUser deleteAllObjects];
    
    for (int i = 0; arrSessionList.count > i; i++)
    {
        NSDictionary *dicSession = arrSessionList[i];
     
        NSDate *date = [self convertDateFromString:[dicSession stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
        double sessionEndTime = [date timeIntervalSince1970];
        
        NSDateFormatter *dtForm = [NSDateFormatter initWithDateFormat:@"dd MMMM"];
        NSString *strDate = [dtForm stringFromDate:date];
        
        NSString *strCurrentDate = [dtForm stringFromDate:[NSDate date]];
        NSString *strTomorrowDate = [dtForm stringFromDate:[NSDate dateWithTimeInterval:(24*60*60) sinceDate:[NSDate date]]];
        
        if ([strDate isEqualToString:strCurrentDate])
            strDate = @"TODAY";
        else if ([strDate isEqualToString:strTomorrowDate])
            strDate = @"TOMORROW";
        else
        {
            NSLog(@"%@",strDate);
        }
        
        strDate = strDate.uppercaseString;
        
        // Store session date here....
        TblSessionListDate *objSessionDate = (TblSessionListDate *)[TblSessionListDate findOrCreate:@{@"date_text" : strDate}];
        objSessionDate.date_text = strDate;
        objSessionDate.date = [dicSession stringValueForJSON:@"run_time"];
        objSessionDate.timestamp = [NSNumber numberWithDouble:sessionEndTime];
        
        // Store session data here....
        TblSessionList *objSes = (TblSessionList *)[TblSessionList findOrCreate:@{@"session_id" : [dicSession stringValueForJSON:@"_id"], @"sessionDate" : objSessionDate}];
        
        objSes.sessionDate = objSessionDate;
        objSes.session_id = [dicSession stringValueForJSON:@"_id"];
        objSes.session_name = [dicSession stringValueForJSON:@"name"];
        objSes.user_id = [dicSession stringValueForJSON:@"user_id"];
        objSes.session_date = [dicSession stringValueForJSON:@"date"];
        objSes.session_time = [dicSession stringValueForJSON:@"time"];
        objSes.distance = [dicSession stringValueForJSON:@"distance"];
        objSes.isSolo = [[dicSession numberForJson:@"isSolo"] isEqual:@1] ? @YES : @NO;
        objSes.isRunning = [[dicSession numberForJson:@"isRunning"] isEqual:@1] ? @YES : @NO;
        objSes.session_end_time = [NSNumber numberWithDouble:sessionEndTime];
        
        
        // Store Joined user here.....
        if ([dicSession objectForKey:@"joinedUsers"] && [[dicSession valueForKey:@"joinedUsers"] isKindOfClass:[NSArray class]])
        {
            NSArray *arrJoinedUser = [dicSession valueForKey:@"joinedUsers"];
            
            for (int j = 0; arrJoinedUser.count > j; j++)
            {
                NSDictionary *dicJoined  = arrJoinedUser[j];
                
                TblJoinedUser *objJoinedUser = (TblJoinedUser *)[TblJoinedUser findOrCreate:@{@"user_id" : [dicJoined stringValueForJSON:@"_id"], @"sessionList" : objSes}];
                objJoinedUser.sessionList = objSes;
                objJoinedUser.user_id = [dicJoined stringValueForJSON:@"_id"];
                objJoinedUser.country = [dicJoined stringValueForJSON:@"country"];
                objJoinedUser.fb_id = [dicJoined stringValueForJSON:@"fb_id"];
                objJoinedUser.first_name = [dicJoined stringValueForJSON:@"first_name"];
                objJoinedUser.last_name = [dicJoined stringValueForJSON:@"last_name"];
                objJoinedUser.user_name = [dicJoined stringValueForJSON:@"user_name"];
                objJoinedUser.joine_image_url = [dicJoined stringValueForJSON:@"picture"];
            }
        }

        // Store Synced user here.....
        if ([dicSession objectForKey:@"syncedUsers"] && [[dicSession valueForKey:@"syncedUsers"] isKindOfClass:[NSArray class]])
        {
            NSArray *arrSyncedUser = [dicSession valueForKey:@"syncedUsers"];
            
            for (int j = 0; arrSyncedUser.count > j; j++)
            {
                NSDictionary *dicSynced  = arrSyncedUser[j];
                
                TblSessionSyncedUser *objSyncedUser = (TblSessionSyncedUser *)[TblSessionSyncedUser findOrCreate:@{@"user_id" : [dicSynced stringValueForJSON:@"_id"], @"sessionList" : objSes}];
                objSyncedUser.sessionList = objSes;
                objSyncedUser.user_id = [dicSynced stringValueForJSON:@"_id"];
                objSyncedUser.country = [dicSynced stringValueForJSON:@"country"];
                objSyncedUser.fb_id = [dicSynced stringValueForJSON:@"fb_id"];
                objSyncedUser.first_name = [dicSynced stringValueForJSON:@"first_name"];
                objSyncedUser.last_name = [dicSynced stringValueForJSON:@"last_name"];
                objSyncedUser.user_name = [dicSynced stringValueForJSON:@"user_name"];
                objSyncedUser.synced_image_url = [dicSynced stringValueForJSON:@"picture"];
            }
            
        }

        
        // Store Other user here.....
        if ([dicSession objectForKey:@"otherUsers"] && [[dicSession valueForKey:@"otherUsers"] isKindOfClass:[NSArray class]])
        {
            NSArray *arrOtherUser = [dicSession valueForKey:@"otherUsers"];
            
            for (int j = 0; arrOtherUser.count > j; j++)
            {
                NSDictionary *dicOtherUser  = arrOtherUser[j];
                
                TblSessionOtherUser *objOtherUser = (TblSessionOtherUser *)[TblSessionOtherUser findOrCreate:@{@"user_id" : [dicOtherUser stringValueForJSON:@"_id"], @"sessionList" : objSes}];
                objOtherUser.sessionList = objSes;
                objOtherUser.user_id = [dicOtherUser stringValueForJSON:@"_id"];
                objOtherUser.country = [dicOtherUser stringValueForJSON:@"country"];
                objOtherUser.fb_id = [dicOtherUser stringValueForJSON:@"fb_id"];
                objOtherUser.first_name = [dicOtherUser stringValueForJSON:@"first_name"];
                objOtherUser.last_name = [dicOtherUser stringValueForJSON:@"last_name"];
                objOtherUser.user_name = [dicOtherUser stringValueForJSON:@"user_name"];
                objOtherUser.other_image_url = [dicOtherUser stringValueForJSON:@"picture"];
            }
        }
        
        [[Store sharedInstance].mainManagedObjectContext save];
    }
}


-(void)saveChatUserListToLocal:(NSArray *)arrUserChatList FirstTime:(BOOL)isFirstTime
{
    
//    [TblChats deleteAllObjects];
//    [TblParticipants deleteAllObjects];

    NSArray *arrOldChannels  = [TblChats fetchAllObjects];
    
    for (int i = 0; arrOldChannels.count > i; i++)
    {
        TblChats *objChat = arrOldChannels[i];
        objChat.isChannelDelete = @YES;
    }
    
    [[Store sharedInstance].mainManagedObjectContext save];
    
    for (int i = 0; arrUserChatList.count > i; i++)
    {
        NSDictionary *dicUser = arrUserChatList[i];
        
        TblChats *objChat = (TblChats *)[TblChats findOrCreate:@{@"loginuser" : appDelegate.loginUser, @"channelid" : [dicUser stringValueForJSON:@"_id"]}];
        objChat.loginuser = appDelegate.loginUser;
        objChat.channelid = [dicUser stringValueForJSON:@"_id"];
        objChat.priority_index = [NSNumber numberWithInteger:i];
        objChat.isChannelDelete = @NO;
        
        if (isFirstTime)
            objChat.typingUser = @[];
        
        NSArray *arrParticipants = [dicUser valueForKey:@"participates"] ;
        objChat.isgroup = [NSNumber numberWithBool:arrParticipants.count > 2];
        
        [self saveParticipantsToLocal:[dicUser valueForKey:@"participates"] andChat:objChat];
        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
    [TblChats deleteObjects:[NSPredicate predicateWithFormat:@"isChannelDelete == %@",@YES]];
    [[Store sharedInstance].mainManagedObjectContext save];
}

-(void)saveParticipantsToLocal:(NSArray *)arrParticipants andChat:(TblChats *)objChat
{
    for (int i = 0; arrParticipants.count > i; i++)
    {
        NSDictionary *dicPart = arrParticipants[i];
        
        TblParticipants *objPart = (TblParticipants *)[TblParticipants findOrCreate:@{@"chatobject" : objChat, @"user_id" : [dicPart stringValueForJSON:@"user_id"]}];
        objPart.chatobject = objChat;
        objPart.user_id = [dicPart stringValueForJSON:@"user_id"];
        objPart.celebrity = [[dicPart numberForJson:@"celebrity"] isEqual:@1] ? @YES : @NO;
        objPart.user_name = [dicPart stringValueForJSON:@"user_name"];
        objPart.first_name = [dicPart stringValueForJSON:@"first_name"];
        objPart.last_name = [dicPart stringValueForJSON:@"last_name"];
        objPart.picture = [dicPart stringValueForJSON:@"picture"];
        objPart.fb_id = [dicPart stringValueForJSON:@"fb_id"];
        objPart.country = [dicPart stringValueForJSON:@"country"];
        objPart.priority_index = [NSNumber numberWithInteger:i];
        
        if ([[dicPart stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id])
        {   
            NSDate *date = [self convertDateFromString:[dicPart stringValueForJSON:@"lastRead"] isGMT:YES formate:CDateFormater];
            double lastReadTime = [date timeIntervalSince1970];
            objChat.lastRead = [NSNumber numberWithDouble:lastReadTime];

            // Store MSG deleted timestamp to load older message....
            objChat.isChatDeleted = [[dicPart numberForJson:@"isDelete"] isEqual:@1] ? @YES : @NO;
            
            NSDate *Deletedate = [self convertDateFromString:[dicPart stringValueForJSON:@"lastDeleted"] isGMT:YES formate:CDateFormater];
            double lastDeleteTime = [Deletedate timeIntervalSince1970];
            objChat.msg_deleted_timestamp = [NSNumber numberWithDouble:lastDeleteTime];
            
        }
        
        NSLog(@"participatnt object: %@", objPart);
    }
}

// Store Activity data to local here.....
-(void)saveActivityToLocal:(NSArray *)arrActivity Offset:(NSNumber *)offset Type:(NSString *)type
{
    
    if ([offset isEqual:@0] && type.intValue == 1)
    {
        [TblActivityDates deleteAllObjects];
        [TblActivityPhoto deleteAllObjects];
        [TblActivityVideo deleteAllObjects];
        [TblActivitySyncSession deleteAllObjects];
        [TblActivityRunSession deleteAllObjects];
        [TblActivityCompleteSession deleteAllObjects];
        
        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
    for (int k = 0; arrActivity.count > k; k++)
    {
        NSDictionary *dicActivity = arrActivity[k];
        NSDate *date = [self convertDateFromString:[dicActivity stringValueForJSON:@"createdAt"] isGMT:YES formate:CDateFormater];
        double activityTime = [date timeIntervalSince1970];
        
        NSDateFormatter *dtForm = [NSDateFormatter initWithDateFormat:@"MMMM yyyy"];
        NSString *strDate = [dtForm stringFromDate:date];
        
        dtForm.dateFormat = @"MMMM";
        NSString *strShowDate = [dtForm stringFromDate:date];;
        strShowDate = strShowDate.uppercaseString;

        
        // Store Activity date here....
//        TblActivityDates *objActivityDate = (TblActivityDates *)[TblActivityDates findOrCreate:@{@"activity_date" : strDate}];
        TblActivityDates *objActivityDate = (TblActivityDates *)[TblActivityDates findOrCreate:@{@"activity_date" : strDate,@"user_name":[dicActivity stringValueForJSON:@"user_name"]}];
        objActivityDate.activity_date = strDate;
        objActivityDate.show_date = strShowDate;
        objActivityDate.timestamp = [NSNumber numberWithDouble:activityTime];
        objActivityDate.user_id = [dicActivity stringValueForJSON:@"user_id"];
        objActivityDate.user_name = [dicActivity stringValueForJSON:@"user_name"];
        objActivityDate.activity_filter_type = @0;
        objActivityDate.activity_type = [dicActivity numberForJson:@"type"];
        objActivityDate.total_month_run = [dicActivity stringValueForJSON:@"total_month_run"];
        objActivityDate.total_month_distance = [dicActivity stringValueForJSON:@"total_month_distance"];
        
        [[Store sharedInstance].mainManagedObjectContext save];
        
//        1=joined | 2= started | 3= completed | 4 add photo | 5 video
        switch (objActivityDate.activity_type.integerValue)
        {
            case 1:
            {
                // Session Joined Activity....
                
                NSDate *date = [self convertDateFromString:[dicActivity stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
                double sessionEndTime = [date timeIntervalSince1970];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = CDateFormater;
                NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
                [dateFormatter setTimeZone:gmt];
                double currentTimestamp = [[NSDate date] timeIntervalSince1970];
                double difTimeStamp = sessionEndTime - currentTimestamp;

                // Do not store sync activity which is going to start within 5 sec.
                if (difTimeStamp < 5)
                    continue;
                
                TblActivitySyncSession *objActivitySync = (TblActivitySyncSession *)[TblActivitySyncSession findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
                objActivitySync.objActivityDate = objActivityDate;
                objActivitySync.session_endtime = [NSNumber numberWithDouble:sessionEndTime];

                objActivitySync.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
                objActivitySync.activity_id = [dicActivity stringValueForJSON:@"_id"];
                objActivitySync.user_id = [dicActivity stringValueForJSON:@"user_id"];
                objActivitySync.user_image = [dicActivity stringValueForJSON:@"picture"];
                objActivitySync.user_name = [dicActivity stringValueForJSON:@"user_name"];
                objActivitySync.activity_main_type = [dicActivity stringValueForJSON:@"type"];
                objActivitySync.timestamp = [NSNumber numberWithDouble:activityTime];
                objActivitySync.session_name = [dicActivity stringValueForJSON:@"session_name"];
                objActivitySync.session_id = [dicActivity stringValueForJSON:@"session_id"];

                objActivitySync.total_likes = [dicActivity stringValueForJSON:@"like_count"];
                objActivitySync.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
                objActivitySync.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivitySync.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
                objActivitySync.activity_text = [dicActivity stringValueForJSON:@"title_text"];
                
                objActivitySync.comments = [dicActivity valueForKey:@"comments"];
                objActivitySync.sync_user = [dicActivity valueForKey:@"userList"];

                break;
            }
            case 2:
            {
                // Session Started Activity....
                TblActivityRunSession *objActivityRun = (TblActivityRunSession *)[TblActivityRunSession findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
                objActivityRun.objActivityDate = objActivityDate;
                objActivityRun.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
                objActivityRun.activity_id = [dicActivity stringValueForJSON:@"_id"];
                objActivityRun.user_id = [dicActivity stringValueForJSON:@"user_id"];
                objActivityRun.user_name = [dicActivity stringValueForJSON:@"user_name"];
                objActivityRun.user_image = [dicActivity stringValueForJSON:@"picture"];
                objActivityRun.session_name = [dicActivity stringValueForJSON:@"session_name"];
                objActivityRun.session_id = [dicActivity stringValueForJSON:@"session_id"];
                objActivityRun.activity_main_type = [dicActivity stringValueForJSON:@"type"];
                objActivityRun.timestamp = [NSNumber numberWithDouble:activityTime];
                objActivityRun.comments = [dicActivity valueForKey:@"comments"];
                objActivityRun.total_likes = [dicActivity stringValueForJSON:@"like_count"];
                objActivityRun.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityRun.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
                objActivityRun.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
                objActivityRun.activity_text = [dicActivity stringValueForJSON:@"title_text"];
                objActivityRun.isMapLoaded = @NO;
                
                NSDictionary *dicUserDetails = [dicActivity valueForKey:@"user_run_detail"];
                objActivityRun.runner_path = [dicUserDetails valueForKey:@"running"];;
                objActivityRun.total_runner = [dicUserDetails stringValueForJSON:@"total_runner"];
                objActivityRun.capture = [dicUserDetails valueForKey:@"capture"];
                objActivityRun.city = [dicUserDetails stringValueForJSON:@"locality"];
                objActivityRun.sublocality = [dicUserDetails stringValueForJSON:@"sub_locality"];
                
                NSDictionary *dicCompleted = [dicUserDetails valueForKey:@"completed"];
                objActivityRun.session_complete_time = [dicCompleted stringValueForJSON:@"total_time"];
                objActivityRun.total_burn_cal = [dicCompleted stringValueForJSON:@"total_calories"];
                objActivityRun.position = [dicCompleted stringValueForJSON:@"position"];
                objActivityRun.total_distance = [dicCompleted stringValueForJSON:@"total_distance"];
                
                break;
            }
            case 3:
            {
                // Delete Run activity object if available....
                NSArray *arrRunActivity = [TblActivityRunSession fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",[dicActivity stringValueForJSON:@"_id"]] sortedBy:nil];
                
                if(arrRunActivity.count > 0)
                {
                    TblActivityRunSession *objRunAct = arrRunActivity[0];
                    [TblActivityRunSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objRunAct.activity_id]];
                    [[Store sharedInstance].mainManagedObjectContext save];
                }

                
                // Session Completed Activity....
                
                TblActivityCompleteSession *objActivityCompleteSession = (TblActivityCompleteSession *)[TblActivityCompleteSession findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
                objActivityCompleteSession.objActivityDate = objActivityDate;
                objActivityCompleteSession.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
                objActivityCompleteSession.activity_id = [dicActivity stringValueForJSON:@"_id"];
                objActivityCompleteSession.user_id = [dicActivity stringValueForJSON:@"user_id"];
                objActivityCompleteSession.user_name = [dicActivity stringValueForJSON:@"user_name"];
                objActivityCompleteSession.user_image = [dicActivity stringValueForJSON:@"picture"];
                objActivityCompleteSession.session_name = [dicActivity stringValueForJSON:@"session_name"];
                objActivityCompleteSession.session_id = [dicActivity stringValueForJSON:@"session_id"];
                objActivityCompleteSession.activity_main_type = [dicActivity stringValueForJSON:@"type"];
                objActivityCompleteSession.timestamp = [NSNumber numberWithDouble:activityTime];
                objActivityCompleteSession.comments = [dicActivity valueForKey:@"comments"];
                objActivityCompleteSession.total_likes = [dicActivity stringValueForJSON:@"like_count"];
                objActivityCompleteSession.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityCompleteSession.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
                objActivityCompleteSession.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
                objActivityCompleteSession.activity_text = [dicActivity stringValueForJSON:@"title_text"];
                objActivityCompleteSession.isMapLoaded = @NO;
                
                NSDictionary *dicUserDetails = [dicActivity valueForKey:@"user_run_detail"];
                objActivityCompleteSession.runner_path = [dicUserDetails valueForKey:@"running"];;
                objActivityCompleteSession.total_runner = [dicUserDetails stringValueForJSON:@"total_runner"];
                objActivityCompleteSession.city = [dicUserDetails stringValueForJSON:@"locality"];
                objActivityCompleteSession.sublocality = [dicUserDetails stringValueForJSON:@"sub_locality"];
                
                objActivityCompleteSession.capture = [dicUserDetails valueForKey:@"capture"];
                NSDictionary *dicCompleted = [dicUserDetails valueForKey:@"completed"];
                objActivityCompleteSession.session_complete_time = [dicCompleted stringValueForJSON:@"total_time"];
                objActivityCompleteSession.total_burn_cal = [dicCompleted stringValueForJSON:@"total_calories"];
                objActivityCompleteSession.position = [dicCompleted stringValueForJSON:@"rank"];
                objActivityCompleteSession.total_distance = [dicCompleted stringValueForJSON:@"total_distance"];
                break;
            }
                
            case 4:
            {
                // Add Photo Activity....
                TblActivityPhoto *objActivityPhoto = (TblActivityPhoto *)[TblActivityPhoto findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
                objActivityPhoto.objActivityDate = objActivityDate;
                objActivityPhoto.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
                objActivityPhoto.activity_id = [dicActivity stringValueForJSON:@"_id"];
                objActivityPhoto.user_id = [dicActivity stringValueForJSON:@"user_id"];
                objActivityPhoto.user_image = [dicActivity stringValueForJSON:@"picture"];
                objActivityPhoto.user_name = [dicActivity stringValueForJSON:@"user_name"];
                objActivityPhoto.activity_main_type = [dicActivity stringValueForJSON:@"type"];
                objActivityPhoto.session_name = [dicActivity stringValueForJSON:@"session_name"];
                objActivityPhoto.session_id = [dicActivity stringValueForJSON:@"session_id"];
                objActivityPhoto.img_width = [dicActivity stringValueForJSON:@"width"];
                objActivityPhoto.img_hieght = [dicActivity stringValueForJSON:@"height"];
                objActivityPhoto.timestamp = [NSNumber numberWithDouble:activityTime];
                objActivityPhoto.comments = [dicActivity valueForKey:@"comments"];;
                objActivityPhoto.act_image = [dicActivity stringValueForJSON:@"attachment"];
                objActivityPhoto.caption = [dicActivity stringValueForJSON:@"caption"];
                objActivityPhoto.total_likes = [dicActivity stringValueForJSON:@"like_count"];
                objActivityPhoto.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityPhoto.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
                objActivityPhoto.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
                objActivityPhoto.activity_text = [dicActivity stringValueForJSON:@"title_text"];
                objActivityPhoto.isAdminActivity = [[dicActivity numberForJson:@"user_id"] isEqual:@0] ? @YES : @NO;
                break;
            }
                
            case 5:
            {
                // Add Video Activity....
                TblActivityVideo *objActivityVideo = (TblActivityVideo *)[TblActivityVideo findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
                objActivityVideo.objActivityDate = objActivityDate;
                objActivityVideo.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
                objActivityVideo.activity_id = [dicActivity stringValueForJSON:@"_id"];
                objActivityVideo.user_id = [dicActivity stringValueForJSON:@"user_id"];
                objActivityVideo.activity_main_type = [dicActivity stringValueForJSON:@"type"];
                objActivityVideo.timestamp = [NSNumber numberWithDouble:activityTime];
                objActivityVideo.comments = [dicActivity valueForKey:@"comments"];
                
                objActivityVideo.user_image = [dicActivity stringValueForJSON:@"picture"];
                objActivityVideo.user_name = [dicActivity stringValueForJSON:@"user_name"];
                objActivityVideo.session_name = [dicActivity stringValueForJSON:@"session_name"];
                objActivityVideo.session_id = [dicActivity stringValueForJSON:@"session_id"];
                
                objActivityVideo.video_width = [dicActivity stringValueForJSON:@"width"];
                objActivityVideo.video_height = [dicActivity stringValueForJSON:@"height"];
                objActivityVideo.video_url = [dicActivity stringValueForJSON:@"attachment"];
                objActivityVideo.thumb_image = [dicActivity stringValueForJSON:@"video_thumb"];
                objActivityVideo.caption = [dicActivity stringValueForJSON:@"caption"];
                objActivityVideo.total_likes = [dicActivity stringValueForJSON:@"like_count"];
                objActivityVideo.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityVideo.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
                objActivityVideo.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
                objActivityVideo.activity_text = [dicActivity stringValueForJSON:@"title_text"];
                objActivityVideo.video_seek_time = 0;
                objActivityVideo.isAdminActivity = [[dicActivity numberForJson:@"user_id"] isEqual:@0] ? @YES : @NO;
                
                break;
            }
            default:
            {
                NSLog(@"Activity type is undefine =========== >>>>>>. ");
                break;
            }
                
        }
        [[Store sharedInstance].mainManagedObjectContext save];
    }
}

-(void)updateActivityDataInLocal:(NSString *)strActType ActivityId:(NSString *)activity_id UpdateData:(NSDictionary *)dicActivity
{
    //        1=joined | 2= started | 3= completed | 4 add photo | 5 video
    switch (strActType.integerValue)
    {
        case 1:
        {
            // Session Joined Activity....
            
            NSArray *arrJoined = [TblActivitySyncSession fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id] sortedBy:nil];
            if (arrJoined.count > 0)
            {
                TblActivitySyncSession *objActivitySync = arrJoined[0];
                objActivitySync.comments = [dicActivity valueForKey:@"comments"];
                objActivitySync.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivitySync.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
            }
        }
            break;
        case 2:
        {
            // Session Started Activity....
            NSArray *arrJoined = [TblActivityRunSession fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id] sortedBy:nil];
            if (arrJoined.count > 0)
            {
                TblActivityRunSession *objActivityRun = arrJoined[0];
                objActivityRun.comments = [dicActivity valueForKey:@"comments"];
                objActivityRun.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityRun.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
            }
        }
            break;
        case 3:
        {
            // Session Completed Activity....
            NSArray *arrJoined = [TblActivityCompleteSession fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id] sortedBy:nil];
            if (arrJoined.count > 0)
            {
                TblActivityCompleteSession *objActivityCompleteSession = arrJoined[0];
                objActivityCompleteSession.comments = [dicActivity valueForKey:@"comments"];
                objActivityCompleteSession.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityCompleteSession.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
            }
        }
            break;
        case 4:
        {
            // Add Photo Activity....
            NSArray *arrJoined = [TblActivityPhoto fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id] sortedBy:nil];
            if (arrJoined.count > 0)
            {
                TblActivityPhoto *objActivityPhoto = arrJoined[0];
                objActivityPhoto.comments = [dicActivity valueForKey:@"comments"];
                objActivityPhoto.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityPhoto.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
            }
        }
            break;
        case 5:
        {
            // Add Video Activity....
            NSArray *arrJoined = [TblActivityVideo fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id] sortedBy:nil];
            if (arrJoined.count > 0)
            {
                TblActivityVideo *objActivityVideo = arrJoined[0];
                objActivityVideo.comments = [dicActivity valueForKey:@"comments"];
                objActivityVideo.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
                objActivityVideo.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
            }
        }
            break;
            
        default:
            break;
    }
    [[Store sharedInstance].mainManagedObjectContext save];

    // Refresh Data for activity in whole app..... 
    if (appDelegate.configureRefreshMainActivityList)
        appDelegate.configureRefreshMainActivityList(RefreshActivityList);
}

-(void)deleteActivityFromLocal:(NSString *)activity_id Type:(NSString *)strActType
{
    //        1=joined | 2= started | 3= completed | 4 add photo | 5 video
    switch (strActType.integerValue)
    {
        case 1:
        {
            // Delete Joined Activity....
            [TblActivitySyncSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id]];
        }
            break;
        case 2:
        {
            // Delete Started Activity....
            [TblActivityRunSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id]];
        }
            break;
        case 3:
        {
            // Delete Completed Activity....
            [TblActivityCompleteSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id]];
        }
            break;
        case 4:
        {
            // Delete Photo Activity....
            [TblActivityPhoto deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id]];
        }
            break;
        case 5:
        {
            // Delete Video Activity....
            [TblActivityVideo deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",activity_id]];
        }
            break;
            
        default:
            break;
    }
    [[Store sharedInstance].mainManagedObjectContext save];
    
    // Refresh Data for activity in whole app.....
    [appDelegate updateActivityListData:DeleteActivity MainWis:NO];
}

@end
