//
//  ActivityUserCell.h
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^inviteUserDidSelect)(NSDictionary *dicUserData,BOOL isFacebook);

@interface ActivityUserCell : UICollectionViewCell
{
    IBOutlet UIButton *btnFacebook,*btnPhone;
    IBOutlet UIView *viewFacebbok,*viewPhone,*viewSearchBar;
    IBOutlet UICollectionView *clFreinds;
    IBOutlet UITextField *txtSearch;
    IBOutlet UITableView *tblSearch;
    
    IBOutlet UILabel *lblSearchNoData,*lblNoFacebookFrnd;
    IBOutlet UIView *viewFBNoDataFound,*viewPhoneNoDataFound;
    
}

@property(strong,nonatomic) inviteUserDidSelect configureInviteUserDidSelect;
@property(weak,nonatomic) IBOutlet UIButton *btnUserBack;
@property(weak,nonatomic) IBOutlet UIButton *btnFacebookShare;

@end
