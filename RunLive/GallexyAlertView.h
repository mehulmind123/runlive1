//
//  GallexyAlertView.h
//  RunLive
//
//  Created by mac-0005 on 14/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GallexyAlertView : UIView
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblMessage;
}
@property(strong,nonatomic) IBOutlet UIButton *btnFirst,*btnSecond,*btnThird;
@property(strong,nonatomic) IBOutlet UIView *viewContainer;

+(id)initGallexyAlertView;

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonFirstText:(NSString *)strBtnFirst ButtonSecondText:(NSString *)strBtnSecond ButtonThirdText:(NSString *)strBtnThird;

@end
