//
//  InAppPurchaseCell.m
//  RunLive
//
//  Created by mac-0005 on 14/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "InAppPurchaseCell.h"

@implementation InAppPurchaseCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialization];
}

-(void)initialization
{
    [self.lblTitle setFont:Is_iPhone_5?CFontSolidoCondensedBold(34.0):CFontSolidoCondensedBold(41.0)];
    self.lblTitle.textColor = self.lblDetails.textColor = CRGB(255, 255, 255);
    [self.lblDetails setFont:Is_iPhone_5?CFontGilroyRegular(13.0):CFontGilroyRegular(16.0)];
}

@end
