//
//  TblChats+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 19/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblChats+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblChats (CoreDataProperties)

+ (NSFetchRequest<TblChats *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *channelid;
@property (nullable, nonatomic, copy) NSNumber *isChannelDelete;
@property (nullable, nonatomic, copy) NSNumber *isChatDeleted;
@property (nullable, nonatomic, copy) NSNumber *isgroup;
@property (nullable, nonatomic, copy) NSNumber *isOnline;
@property (nullable, nonatomic, copy) NSNumber *isReadMessage;
@property (nullable, nonatomic, copy) NSString *lastMessage;
@property (nullable, nonatomic, copy) NSNumber *lastRead;
@property (nullable, nonatomic, copy) NSNumber *msg_deleted_timestamp;
@property (nullable, nonatomic, copy) NSString *msg_time;
@property (nullable, nonatomic, copy) NSNumber *priority_index;
@property (nullable, nonatomic, copy) NSString *typingMessage;
@property (nullable, nonatomic, retain) NSObject *typingUser;
@property (nullable, nonatomic, retain) TblUser *loginuser;
@property (nullable, nonatomic, retain) NSSet<TblParticipants *> *participants;

@end

@interface TblChats (CoreDataGeneratedAccessors)

- (void)addParticipantsObject:(TblParticipants *)value;
- (void)removeParticipantsObject:(TblParticipants *)value;
- (void)addParticipants:(NSSet<TblParticipants *> *)values;
- (void)removeParticipants:(NSSet<TblParticipants *> *)values;

@end

NS_ASSUME_NONNULL_END
