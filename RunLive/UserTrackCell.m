//
//  UserTrackCell.m
//  Krishna_MapGraph
//
//  Created by mac-00012 on 6/22/17.
//  Copyright © 2017 mac-00012. All rights reserved.
//

#import "UserTrackCell.h"
#import "MeterDistanceCell.h"



@implementation UserTrackCell
{
    PulsingHaloLayer *objPulsingHaloLayer;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    objPulsingHaloLayer = [PulsingHaloLayer layer];
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    
//    [self updateConstraintsIfNeeded];
    
    
//    self.viewImageBorder.layer.cornerRadius = CGRectGetHeight(self.viewImageBorder.frame)/2;
//    self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;
//    self.imgRankTag.layer.cornerRadius = CGRectGetHeight(self.imgRankTag.frame)/2;
}

#pragma mark - Collection View Zoom In/Out
-(void)setUserUIIfCollectionViewZoom:(BOOL)isZoom cellSize:(float)size UserData:(NSDictionary *)dicData
{
    BOOL isPlusZoom = NO;
    if (isZoom)
    {
        if (size >= 80)
        {
            self.cnUserViewHeight.constant = self.cnUserViewWidth.constant = 50;
            self.cnRankTagHeight.constant = self.cnRankTagWidth.constant = 20;
            self.cnViewBorderBottomSpace.constant = self.cnViewBorderTopSpace.constant = self.cnViewBorderLeadingSpace.constant = self.cnViewBorderTrallingSpace.constant = 5;
            isPlusZoom = NO;
        }
        else if (size >= 25)
        {
            if (size >= 50)
            {
                self.cnUserViewHeight.constant = self.cnUserViewWidth.constant = size - 10;
                self.cnRankTagHeight.constant = self.cnRankTagWidth.constant = 15;
                self.cnViewBorderBottomSpace.constant = self.cnViewBorderTopSpace.constant = self.cnViewBorderLeadingSpace.constant = self.cnViewBorderTrallingSpace.constant = 5;
            }
            else
            {
                self.cnUserViewHeight.constant = self.cnUserViewWidth.constant = size - 8;
                self.cnRankTagHeight.constant = self.cnRankTagWidth.constant = 10;
                self.cnViewBorderBottomSpace.constant = self.cnViewBorderTopSpace.constant = self.cnViewBorderLeadingSpace.constant = self.cnViewBorderTrallingSpace.constant = 1.5;
            }
            
            isPlusZoom = YES;
        }
        else
        {
            self.cnUserViewHeight.constant = self.cnUserViewWidth.constant = size - 2;
            self.cnRankTagHeight.constant = self.cnRankTagWidth.constant = 5;
            self.cnViewBorderBottomSpace.constant = self.cnViewBorderTopSpace.constant = self.cnViewBorderLeadingSpace.constant = self.cnViewBorderTrallingSpace.constant = 1;
            isPlusZoom = YES;
        }
    }
    else
    {
        // Noraml Size
        self.cnUserViewHeight.constant = self.cnUserViewWidth.constant = 50;
        self.cnRankTagHeight.constant = self.cnRankTagWidth.constant = 20;
        self.cnViewBorderBottomSpace.constant = self.cnViewBorderTopSpace.constant = self.cnViewBorderLeadingSpace.constant = self.cnViewBorderTrallingSpace.constant = 5;
        isPlusZoom = NO;
    }
    
    dispatch_async(GCDMainThread, ^{
        self.viewImageBorder.layer.cornerRadius = CGRectGetHeight(self.viewImageBorder.frame)/2;
        self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;
        self.imgRankTag.layer.cornerRadius = CGRectGetHeight(self.imgRankTag.frame)/2;
        
        if ([[dicData stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id])
        {
            [self.layer removeAllAnimations];
            _btnUser.hidden = NO;
            self.viewImageBorder.hidden = YES;
            _viewAnimation.hidden = NO;
            [self addAnimtedImageInMarker:isPlusZoom];
        }
        else
        {
            _btnUser.hidden = YES;
            self.viewImageBorder.hidden = NO;
            _viewAnimation.hidden = YES;
            self.viewImageBorder.layer.borderWidth = .7;
            self.viewImageBorder.layer.borderColor = CRGB(32, 35, 50).CGColor;
        }
    });
    
   
}

#pragma mark - Set track user data

-(void)SetUserData:(NSDictionary *)dicData
{
    [self.imgUser setImageWithURL:[appDelegate resizeImage:@"84" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
 
    
    NSDictionary *dicCompleted = [dicData valueForKey:@"completed"];
    
    
    if ([dicCompleted stringValueForJSON:@"rank"].intValue > 0)
    {
        self.imgRankTag.image = [UIImage imageNamed:[[dicCompleted numberForJson:@"run_completed_type"] isEqual:@1] ?@"ic_Activity_run_completed_green" : @"ic_Activity_run_completed_red"];
    }
    else
    {
        self.imgRankTag.backgroundColor = [UIColor clearColor];
        
        switch ([dicCompleted stringValueForJSON:@"running_status"].integerValue)
        {
            case 1:
            {
                // Stop Moving
                self.imgRankTag.image = [UIImage imageNamed:@"ic_Activity_Leader_red"];
            }
                break;
            case 2:
            {
                // Walking
                self.imgRankTag.image = [UIImage imageNamed:@"ic_Activity_Leader_yellow"];
            }
                break;
            case 3:
            {
                // Running
                self.imgRankTag.image = [UIImage imageNamed:@"ic_Activity_Leader_green"];
            }
                break;
                
            default:
                break;
        }
    }
}


-(void)addAnimtedImageInMarker:(BOOL)isZoomeIn
{
    if ([self.viewAnimation.layer.sublayers containsObject:objPulsingHaloLayer]) {
        return;
    }
    
    [objPulsingHaloLayer removeFromSuperlayer];
    
    objPulsingHaloLayer = [PulsingHaloLayer layer];
//    NSLog(@"%@",self.viewAnimation.layer.sublayers);
    
    [self.viewAnimation.layer insertSublayer:objPulsingHaloLayer below:self.imgLoginUserAnimation.layer];

    CViewSetX(objPulsingHaloLayer, 12.5);
    CViewSetY(objPulsingHaloLayer, 12.5);

    objPulsingHaloLayer.cornerRadius = CViewWidth(objPulsingHaloLayer)/2;
    _viewAnimation.layer.cornerRadius = CViewWidth(_viewAnimation)/2;

    objPulsingHaloLayer.haloLayerNumber = 5;
    objPulsingHaloLayer.radius = 35;
    objPulsingHaloLayer.animationDuration = 7;
    [objPulsingHaloLayer setBackgroundColor:CRGB(18, 216, 253).CGColor];
    [objPulsingHaloLayer start];
}




@end
