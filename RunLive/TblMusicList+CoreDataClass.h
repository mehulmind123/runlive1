//
//  TblMusicList+CoreDataClass.h
//  RunLive
//
//  Created by mac-0005 on 20/09/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TblMusicList : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblMusicList+CoreDataProperties.h"
