//
//  MotionPermissionView.m
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MotionPermissionView.h"

@implementation MotionPermissionView

+ (id)initMotionPermissionView
{
    MotionPermissionView *objMotionView = [[[NSBundle mainBundle]loadNibNamed:@"MotionPermissionView" owner:nil options:nil] lastObject];
    CViewSetWidth(objMotionView, CScreenWidth);
    CViewSetHeight(objMotionView, CScreenHeight);
    return objMotionView;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self Initialization];
}

#pragma mark - Configuration

-(void)Initialization
{
    self.btnMotionNotNow.layer.cornerRadius = self.btnMotionGoForIt.layer.cornerRadius = 3;
}

@end
