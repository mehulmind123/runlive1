//
//  ImgZoomCollectionViewCell.h
//  MetLife_Demo
//
//  Created by mac-0009 on 9/4/15.
//  Copyright (c) 2015 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgZoomCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVImageZoom;
@property (strong, nonatomic) IBOutlet UIScrollView *cellScrollView;

@end
