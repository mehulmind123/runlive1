//
//  SessionDetailSelectedUserCell.h
//  RunLive
//
//  Created by mac-0006 on 18/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionDetailSelectedUserCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *viewUser;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@property (strong, nonatomic) IBOutlet UIView *viewGray;

@end
