//
//  InviteFriendsViewController.m
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import "SessionTypeViewController.h"
#import "SessionTypeUser.h"


@interface InviteFriendsViewController ()
{
    NSMutableArray *arrSelectUser;
    NSMutableArray *arrActiveUser, *arrOtherUserData;
}

@end

@implementation InviteFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrOtherUserData = [NSMutableArray new];
    arrActiveUser = [NSMutableArray new];
    arrSelectUser = [NSMutableArray new];
    
    btnInviteFrd.layer.cornerRadius = 3;
    
    
    [clFriend registerNib:[UINib nibWithNibName:@"InviteFriendCell" bundle:nil] forCellWithReuseIdentifier:@"InviteFriendCell"];
    [clFriend registerNib:[UINib nibWithNibName:@"InviteFriendHeaderView" bundle:nil] forSupplementaryViewOfKind:@"UICollectionElementKindSectionHeader" withReuseIdentifier:@"InviteFriendHeaderView"];
    [clFriend registerNib:[UINib nibWithNibName:@"InviteFriendFooterView" bundle:nil] forSupplementaryViewOfKind:@"UICollectionElementKindSectionFooter" withReuseIdentifier:@"InviteFriendFooterView"];
    
       viewBottom.layer.masksToBounds = NO;
    viewBottom.layer.shadowRadius = 8;
    viewBottom.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewBottom.layer.shadowOffset = CGSizeMake(0, -3);
    viewBottom.layer.shadowOpacity = .5f;
    
    [self getFriendListFromServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isFromCerateSession)
        [btnInviteFrd setTitle:@"Next" forState:UIControlStateNormal];
    else
        [btnInviteFrd setTitle:@"Invite Friends" forState:UIControlStateNormal];
}

#pragma mark - APi Function

-(void)getFriendListFromServer
{
    NSLog(@"Session Id ======= >%@",_strSessionId);
    
    [[APIRequest request] friendList:nil loader:YES completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             [arrOtherUserData removeAllObjects];
             [arrActiveUser removeAllObjects];
             
             [arrSelectUser removeAllObjects];
             
             [arrActiveUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"]];
             [arrOtherUserData addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"other"]];
             
             NSMutableArray *arrTempOtherUser = [NSMutableArray new];
             [arrTempOtherUser addObjectsFromArray:arrOtherUserData];
             [arrOtherUserData removeAllObjects];

             for (int i = 0; arrTempOtherUser.count > i; i++)
             {
                 NSDictionary *dicData = arrTempOtherUser[i];
                 NSArray *arrSelectedID = [self.arrAllreadyInvitedUser valueForKey:@"_id"];
                 if (![arrSelectedID containsObject:[dicData stringValueForJSON:@"_id"]])
                     [arrOtherUserData addObject:dicData];
             }
             
             
             [arrTempOtherUser removeAllObjects];
             [arrTempOtherUser addObjectsFromArray:arrActiveUser];
             [arrActiveUser removeAllObjects];

             for (int i = 0; arrTempOtherUser.count > i; i++)
             {
                 NSDictionary *dicData = arrTempOtherUser[i];
                 NSArray *arrSelectedID = [self.arrAllreadyInvitedUser valueForKey:@"_id"];
                 if (![arrSelectedID containsObject:[dicData stringValueForJSON:@"_id"]])
                     [arrOtherUserData addObject:dicData];
             }
             
             [clFriend reloadData];
         }
     }];
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0)
         return arrActiveUser.count;
    else
        return  arrOtherUserData.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 50);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(0, 10);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        InviteFriendHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"InviteFriendHeaderView" forIndexPath:indexPath];

        if(indexPath.section == 0)
           headerView.lblHeaderTitle.text = @"MOST ACTIVE FRIENDS";
        else
           headerView.lblHeaderTitle.text = @"OTHER FRIENDS";
        
        reusableview = headerView;
    }
    else
    {
        InviteFriendFooterView * footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"InviteFriendFooterView" forIndexPath:indexPath];
        
        reusableview = footerView;
    }
    
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"InviteFriendCell";
    InviteFriendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.viewCircle.layer.borderColor = CRGB(235, 241, 245).CGColor;
    
    if(indexPath.section == 0)
    {
        NSDictionary *dicData = [arrActiveUser objectAtIndex:indexPath.item];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        cell.viewSelect.hidden = ![arrSelectUser containsObject:[arrActiveUser objectAtIndex:indexPath.item]];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    }
    else
    {
        NSDictionary *dicData = [arrOtherUserData objectAtIndex:indexPath.item];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        cell.viewSelect.hidden = ![arrSelectUser containsObject:[arrOtherUserData objectAtIndex:indexPath.item]];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth/4 , 126);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    InviteFriendCell *cell = (InviteFriendCell *)[clFriend cellForItemAtIndexPath:indexPath];

    NSMutableArray *arrTempUser = [NSMutableArray new];
    
    if(indexPath.section == 0)
        [arrTempUser addObjectsFromArray:arrActiveUser];
    else
        [arrTempUser addObjectsFromArray:arrOtherUserData];
    
    
    if ([arrSelectUser containsObject:[arrTempUser objectAtIndex:indexPath.item]])
    {
        [arrSelectUser removeObject:[arrTempUser objectAtIndex:indexPath.item]];
        cell.viewSelect.hidden = YES;
    }
    else
    {
        [arrSelectUser addObject:[arrTempUser objectAtIndex:indexPath.item]];
        cell.viewSelect.hidden = NO;
    }
 
    if (arrSelectUser.count > 0)
    {
        viewBottom.hidden = imgBottomShadow.hidden = NO;
        [collectionView setConstraintConstant:90 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
    }
    else
    {
        viewBottom.hidden = imgBottomShadow.hidden = YES;
        [collectionView setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
    }
}

#pragma mark - Invite Friends
-(void)showAlertForUser:(BOOL)isSolo
{
    SessionTypeUser *objSessionType = [SessionTypeUser viewFromXib];
    objSessionType.lblText.text = isSolo ? @"You can have solo session with only 9 other users." : @"You can have team session with only 19 other users.";
    
    [self presentViewOnPopUpViewController:objSessionType shouldClickOutside:NO dismissed:nil];
    
    [objSessionType.btnCancel touchUpInsideClicked:^{
        [self dismissOverlayViewControllerAnimated:YES completion:nil];
    }];
    
    [objSessionType.btnGoBack touchUpInsideClicked:^{
        [self.navigationController popViewControllerAnimated:YES];
        [objSessionType.btnCancel sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

-(void)addPlayerInSession
{
    [[APIRequest request] addFriendInSession:self.strSessionId Friends:arrSelectUser completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             imgInviteSent.hidden = NO;
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self.navigationController popViewControllerAnimated:YES];
             });
         }
     }];

}


#pragma mark - Action
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnInviteFriendsClicked:(id)sender
{
    if (self.isFromCerateSession)
    {
        SessionTypeViewController *objSes = [[SessionTypeViewController alloc] init];
        objSes.arrFriends = [[NSMutableArray alloc] initWithArray:arrSelectUser];
        [self.navigationController pushViewController:objSes animated:YES];
    }
    else
    {
        
        int TotalPlayer = (int)self.arrAllreadyInvitedUser.count + (int)arrSelectUser.count;
        
        if (self.isSoloSession)
        {
            // Check Max 09 user...
            if (TotalPlayer > 10)
                [self showAlertForUser:self.isSoloSession];
            else
                [self addPlayerInSession];
        }
        else
        {
            // Check Max 19 user...
             if (TotalPlayer > 20)
                [self showAlertForUser:self.isSoloSession];
            else
                [self addPlayerInSession];
            }
        }
}

@end
