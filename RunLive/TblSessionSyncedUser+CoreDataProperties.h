//
//  TblSessionSyncedUser+CoreDataProperties.h
//  RunLive
//
//  Created by mac-00012 on 1/2/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionSyncedUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblSessionSyncedUser (CoreDataProperties)

+ (NSFetchRequest<TblSessionSyncedUser *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *synced_image_url;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *fb_id;
@property (nullable, nonatomic, copy) NSString *first_name;
@property (nullable, nonatomic, copy) NSString *last_name;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, retain) TblSessionList *sessionList;

@end

NS_ASSUME_NONNULL_END
