//
//  PlayListCell.m
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "PlayListCell.h"

@implementation PlayListCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
