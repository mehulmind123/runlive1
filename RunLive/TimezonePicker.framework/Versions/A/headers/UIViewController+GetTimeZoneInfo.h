//
//  UIViewController+GetTimeZoneInfo.h
//  uvite
//
//  Created by mac-0009 on 12/17/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (GetTimeZoneInfo)
-(NSArray*)getAllTimeZones;
-(NSDictionary*)getTimeZoneByName:(NSString*)name;
-(NSDictionary *)getCurrentTimeZoneDetails;

+(NSArray*)getAllTimeZones;
+(NSDictionary*)getTimeZoneByName:(NSString*)name;
+(NSDictionary *)getCurrentTimeZoneDetails;

@end
