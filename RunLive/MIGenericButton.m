//
//  MIGenericButton.m
//  EdSmart
//
//  Created by mac-0007 on 15/07/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MIGenericButton.h"

@implementation MIGenericButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    if(Is_iPhone_4 || Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
    else if(Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 2)];
}

@end
