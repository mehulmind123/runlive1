//
//  InAppPurchaseCell.h
//  RunLive
//
//  Created by mac-0005 on 14/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InAppPurchaseCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDetails;


@end
