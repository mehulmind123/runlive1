//
//  CommentsViewController.h
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface CommentsViewController : SuperViewController <HPGrowingTextViewDelegate>
{
    IBOutlet UITableView *tblComments,*tblPeopleList;
    IBOutlet UIButton *btnSend;
    IBOutlet HPGrowingTextView *txtVComent;
    IBOutlet NSLayoutConstraint *txtCommentHeight,*tblpeopleHeight,*scrollIndY,*scrollIndheight;
    IBOutlet UIView *viewScrollIndicator,*ViewScrollSide;
}

@property (strong,nonatomic) NSString *strActivityId;
@property (strong,nonatomic) NSString *strActivityType;
@end
