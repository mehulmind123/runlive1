//
//  ActivityAddPhotoCell.h
//  RunLive
//
//  Created by mac-00012 on 11/2/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityAddPhotoCell : UITableViewCell


@property(weak,nonatomic) IBOutlet UIImageView
    *imgUser,
    *imgAddedPhoto,
    *imgHeart,
    *imgHeartLikeZoom,
    *imgComment,
    *imgVThumb;

@property(weak,nonatomic) IBOutlet UIView *viewUser,*viewScreenShot,*viewVideo,*viewCommentSeprator;

@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblActivityText;

@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment1;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment2;

@property(weak,nonatomic) IBOutlet UILabel *lblPhotoCaption;
@property(weak,nonatomic) IBOutlet UILabel *lblLikes,*lblTotalComments,*lblTime;

@property(weak,nonatomic) IBOutlet UIButton *btnLike,*btnComment,*btnViewAllComments,*btnShare,*btnVideo,*btnUser,*btnMuteUnmute,*btnDelete;

@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnTimeLabelTopSpace;

@property(nonatomic,strong) IBOutlet NSLayoutConstraint *cnImgHeight,*cnImgWidth;

@property(strong,nonatomic) UITapGestureRecognizer *doubleTapeGesture;

@property (strong, nonatomic) GUIPlayerView *playerView;

@property(weak,nonatomic) TblActivityVideo *objVideoActivity;

-(void)setUpForVideoPlayer;

-(void)StopVideoPlaying;

-(void)setImageHeightWidth:(NSString *)strImgWidth Height:(NSString *)strImgHeight PhotoActivity:(BOOL)isPhotoActivity;


@end
