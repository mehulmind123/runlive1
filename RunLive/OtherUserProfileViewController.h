//
//  OtherUserProfileViewController.h
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileHeaderCell.h"
#import "ProfileHeaderView.h"

@interface OtherUserProfileViewController : SuperViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblOtherUser;
    IBOutlet UIView *viewPopup;
    IBOutlet UIView *viewContent;
    IBOutlet UIButton *btnStartStopFollowing,*btnBlockUnblock,*btnBack;
    IBOutlet UIView *viewNoData;
    IBOutlet NSLayoutConstraint *cnTblTopSpace;
    
}

@property(strong,nonatomic) NSString *strUserId;
@property(strong,nonatomic) NSString *strUserName;

- (IBAction)btnPopupClicked:(UIButton *)sender;

@end
