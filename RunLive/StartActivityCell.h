//
//  StartActivityCell.h
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartActivityCell : UITableViewCell<MGLMapViewDelegate>

@property(weak,nonatomic) IBOutlet MGLMapView *mapView;

@property(weak,nonatomic) IBOutlet UIView
    *viewMap,
    *viewPosition,
    *viewScreenShot,
    *viewSubMapView,
     *viewCommentSeprator;

@property(weak,nonatomic) IBOutlet UIImageView
    *imgUser,
    *imgHeart,
    *imgPosition,
    *imgComment,
    *imgMapView;

@property(weak,nonatomic) IBOutlet UIView *viewUser;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblActivityText;

@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment1;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment2;

@property(weak,nonatomic) IBOutlet UILabel
    *lblLikes,
    *lblTotalComments,
    *lblTime,
    *lblWinPositon;

@property(weak,nonatomic) IBOutlet UILabel
    *lblSessionComTime,
    *lblAverageSpeed,
    *lblPosition,
    *lblTotalRunner,
    *lblCity,
    *lblSpeedTag,
    *lblSubLocality;

@property(weak,nonatomic) IBOutlet UIButton
    *btnLike,
    *btnComment,
    *btnViewAllComments,
    *btnShare,
    *btnUser;

@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnTimeLabelTopSpace;
@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnMapViewHeight;

-(void)drawMapPath:(NSArray *)arrPath;


@end
