//
//  SplashViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : SuperViewController
{
    IBOutlet UIImageView *imgSplash;
}
@end
