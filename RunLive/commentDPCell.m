//
//  commentDPCell.m
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "commentDPCell.h"

@implementation commentDPCell
@synthesize vwBorder,imgDP;
- (void)awakeFromNib
{
    [super awakeFromNib];
 
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [appDelegate roundedImageView:imgDP withSuperView:vwBorder];
    vwBorder.layer.borderColor = [CImageOuterBorderColor CGColor];
    vwBorder.layer.borderWidth = 1;
    
//    CGFloat radians = atan2f(self.transform.b, self.transform.a);
//    CGFloat degrees = radians * (180 / M_PI);
//    if (degrees == 0) {
//        self.transform = CGAffineTransformMakeRotation(M_PI);
//    }
    
    self.lblContent.preferredMaxLayoutWidth = CGRectGetWidth(self.lblContent.frame);

}
@end
