//
//  TblMusicList+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 20/09/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblMusicList+CoreDataProperties.h"

@implementation TblMusicList (CoreDataProperties)

+ (NSFetchRequest<TblMusicList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblMusicList"];
}

@dynamic album_image;
@dynamic artist_name;
@dynamic cover_image;
@dynamic followers;
@dynamic isApple;
@dynamic main_spotify_uri;
@dynamic playlist_name;
@dynamic song_image;
@dynamic song_name;
@dynamic song_url;
@dynamic songId;

@end
