//
//  MIWheelRotationGesture.h
//  Wheel
//
//  Created by mac-0005 on 17/02/18.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

@protocol MIWheelRotationGestureDelegate <UIGestureRecognizerDelegate>

-(void)autoCicrleFillDidStop:(BOOL)isStop;

-(void)autoCicrleFillDidStart:(BOOL)isStart;

@end

@interface MIWheelRotationGesture : UIGestureRecognizer<CAAnimationDelegate>
{
    BOOL touchesMoved;
    
    
    CGPoint lastPoint;
    NSTimeInterval lastTouchTimeStamp;
    
    
    CATransform3D currentTransform;
    NSInteger turnDirection;
}

@property(strong,nonatomic) UIViewController *parentView;
@property NSInteger rotationDirection;

@property double currentAngle;
@property double angularSpeed;

@property (nonatomic, weak) id <MIWheelRotationGestureDelegate> gestureDelegate; //define MyClassDelegate as delegate

@end
