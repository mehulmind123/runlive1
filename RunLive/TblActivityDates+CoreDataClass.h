//
//  TblActivityDates+CoreDataClass.h
//  RunLive
//
//  Created by mac-00012 on 7/26/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TblActivityCompleteSession, TblActivityPhoto, TblActivityRunSession, TblActivitySyncSession, TblActivityVideo;

NS_ASSUME_NONNULL_BEGIN

@interface TblActivityDates : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblActivityDates+CoreDataProperties.h"
