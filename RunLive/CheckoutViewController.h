//
//  CheckoutViewController.h
//  RunLive
//
//  Created by mac-0008 on 24/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

#define CLineSelected   CRGB(111,226,254)

@interface CheckoutViewController : SuperViewController
{
    IBOutlet UIView *viewNav,*viewOrderSuccess;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIButton *btnClose;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *viewScroll;
    IBOutlet ACFloatingTextField *txtFirstName;
    IBOutlet ACFloatingTextField *txtLastName;
    IBOutlet ACFloatingTextField *txtAddress1;
    IBOutlet ACFloatingTextField *txtAddress2;
    IBOutlet ACFloatingTextField *txtCity;
    IBOutlet ACFloatingTextField *txtPostcode;
    IBOutlet ACFloatingTextField *txtCountry;
    IBOutlet ACFloatingTextField *txtphoneNumber;
    IBOutlet ACFloatingTextField *txtStateProvinceRegion;
}
@end
