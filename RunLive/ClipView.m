
#import "ClipView.h"

@interface ClipView()
@property float angle;
@property float radius;
@property float angle2;

@end

@implementation ClipView {
    int numberOfDots;
    CGFloat dotStep;
    int additionalDotsRadiusSpacing;
    CGFloat startAngle;
    CGFloat lineWidth;
    CGFloat step;
    CGFloat endAngle;
    CGFloat finishAngle;
    CGFloat dotRadius;
    UIColor *activeColor;
    UIColor *inactiveColor;
    CGFloat activeShadowRadius;
}

@synthesize logo;

- (void)setupInitialAnimationProperties {
    numberOfDots = 19;
    dotStep = 0.260999472651826; // hardcoded step for 19 dots number because it doesn't calculate properly for some reason((((
    dotRadius = 1.0;
    additionalDotsRadiusSpacing = 40;
    
    startAngle = M_PI_4 * 3;
    step = M_PI_2 * 3;
    endAngle = MIN(self.angle / 360, 1) * step + startAngle;
    finishAngle = step + startAngle;
    

    activeColor = CRGB(17, 216, 253);
    inactiveColor = CRGB(48, 51, 66);
    lineWidth = 13;
    activeShadowRadius = 2;
}

- (void) updateAngle:(CGFloat)setAngle {
    self.angle = setAngle;
    [self setNeedsDisplay];
    
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self setupInitialAnimationProperties];
    
    [self drawDotsInRect:rect];
    
    [[UIColor clearColor] setFill];
    
    UIBezierPath *inactiveSectionPath = [self arcWithStartAngle:endAngle
                                                       endAngle:finishAngle
                                                         inRect:rect];
    [inactiveColor setStroke];
    [inactiveSectionPath fill];
    [inactiveSectionPath stroke];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIBezierPath *activeSectionPath = [self arcWithStartAngle:startAngle
                                                    endAngle:endAngle
                                                      inRect:rect];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, CGSizeZero, activeShadowRadius, [activeColor CGColor]);
    [activeColor setStroke];
    [activeSectionPath fill];
    [activeSectionPath stroke];
    
}

- (void)drawDotsInRect:(CGRect)rect {
    for (int i = 0; i < numberOfDots; i++) {
        CGFloat circleRad = (rect.size.width / 2) - additionalDotsRadiusSpacing;
        CGFloat angle = startAngle + i * dotStep;
        CGPoint point = CGPointMake(rect.size.width / 2 + circleRad * cos(angle),
                                    rect.size.height / 2 + circleRad * sin(angle));
        
        UIBezierPath *litledotPath = [UIBezierPath bezierPathWithArcCenter:point
                                                                    radius:dotRadius
                                                                startAngle:0
                                                                  endAngle:M_PI * 2
                                                                 clockwise:YES];
        if (angle < endAngle) {
            [activeColor setFill];
        } else {
            [inactiveColor setFill];
        }
        [litledotPath fill];
    }
}

- (UIBezierPath *)arcWithStartAngle:(CGFloat)startAngle
                           endAngle:(CGFloat)endAngle
                             inRect:(CGRect)rect {
    CGPoint center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
    CGFloat circleRadius = (rect.size.width / 4) - 10;
    UIBezierPath *sectorPath = [UIBezierPath bezierPathWithArcCenter:center
                                                              radius:circleRadius
                                                          startAngle:startAngle
                                                            endAngle:endAngle
                                                           clockwise:YES];
    sectorPath.lineWidth = lineWidth;
    sectorPath.lineCapStyle = kCGLineCapRound;
    
    return sectorPath;
}

@end
