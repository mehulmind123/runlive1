//
//  BlockedUserCell.m
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "BlockedUserCell.h"

@implementation BlockedUserCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.btnUnblock.layer.cornerRadius = 2;
    self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2;
    self.imgUser.layer.masksToBounds = YES;
    
}

@end
