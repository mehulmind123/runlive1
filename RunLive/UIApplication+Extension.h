//
//  UIApplication+Extension.h
//  Master
//
//  Created by mac-0001 on 01/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

typedef void (^Block)(void);



#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Contacts/Contacts.h>
#import "Master.h"
#import "MIEvent.h"



@interface UIApplication (Extension)

+(NSString *)applicationInformation;
+(NSString *)timeZoneInfomration;
+(NSString *)userDefaultsInfomration;


#pragma mark - General

+(UIImage *)screenshot;

+(UIViewController *)topMostController;

+(BOOL)isNewApplciaitonVersionInstalled;
+(BOOL)isNewInstallation;

+(NSString*)applicationName;

#pragma mark - Keychain

+(void)setKeychainObject:(id)value forKey:(NSString *)key;
+(id)keychainObjectForKey:(NSString *)key;

+(void)setKeychain:(NSString *)value forKey:(NSString *)key;
+(void)removeKeychainForKey:(NSString *)key;
+(NSString *)keychainForKey:(NSString *)key;



+(NSString *)uniqueIdentifier;

+(NSString *)appID;

+(NSString *)deviceToken;
+(void)setDeviceToken:(NSString *)deviceToken;
+(void)removeDeviceToken;

+(NSString *)accessToken;
+(void)setAccessToken:(NSString *)accessToken;
+(void)removeAccessToken;

+(NSString *)userId;
+(void)setUserId:(id)userId;
+(void)removeUserId;


#pragma mark - UIApplication Blocks

+ (void)applicationDidReceiveMemoryWarning:(Block)block;
+ (void)applicationDidBecomeActive:(Block)block;
+ (void)applicationWillResignActive:(Block)block;
+ (void)applicationDidEnterBackground:(Block)block;
+ (void)applicationWillEnterForeground:(Block)block;
+ (void)applicationWillTerminate:(Block)block;


#pragma mark - Event

//+(void)removeEventFromCalendar:(NSString*)eventIdentifier completion:(MIBooleanResultBlock)block;
//+(void)removeEvent:(MIEvent *)event completion:(MIBooleanResultBlock)block;
//+(void)addEventToCalendar:(MIEvent*)event competion:(MIBooleanResultBlock)block;

+(void)loadContactsWithDetails_completed:(void (^)(NSArray *contactArry , NSError *error))completion;


@end
