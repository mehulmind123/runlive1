//
//  IntroViewController.m
//  RunLive
//
//  Created by mac-0008 on 22/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "IntroViewController.h"
#import "RewardViewController.h"
#import "AccountSettingViewController.h"
#import "UserRunSummaryViewController.h"
#import "TermsAndPrivacyPoilicyViewController.h"
#import "FacebookSignUpViewController.h"


#define ACCESS_TOKEN_KEY @"99d028c6a51edfb984ac38a8668efc8c01947e29"


#define CTitle @"title"
#define CDescription @"des"

@interface IntroViewController ()

@end

@implementation IntroViewController
{
    NSArray *arrIntroduction;
    AVPlayer *player;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (Is_iPhone_5)
        cnImgRunliveLogoY.constant = -80;
    else if (Is_iPhone_6)
        cnImgRunliveLogoY.constant = -45;
    else if (Is_iPhone_6_PLUS)
        cnImgRunliveLogoY.constant = 15;
    else if (Is_iPhone_X)
        cnImgRunliveLogoY.constant = 40;
    
    btnRegular.layer.cornerRadius = btnSocial.layer.cornerRadius = btnStrava.layer.cornerRadius = 3;
    
    [self setInitInTextFiled:txtEmail andPlaceholder:@"Email address"];
    [self setInitInTextFiled:txtCommon andPlaceholder:@"Username"];
    [self setInitInTextFiled:txtPassword andPlaceholder:@"Password"];
    [self setInitInTextFiled:txtConfirmPwd andPlaceholder:@"Confirm password"];
    
    [collectionIntro registerNib:[UINib nibWithNibName:@"IntroCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"IntroCollectionViewCell"];
    
    viewBottom.hidden = YES;
    dispatch_async(GCDMainThread, ^{
        viewBottom.hidden = NO;
        viewBottomX.constant = -CViewHeight(viewBottom) + CViewHeight(btnSignUp);
         [self setForTermsAndPolicy];
    });
    
    if(IS_IPHONE_SIMULATOR)
    {
        txtEmail.text = @"krishnasoni2113@gmail.com";
        txtPassword.text = @"123456";
    }
    
    arrIntroduction = @[
                        @{CTitle:@"GET PAID",CDescription:@"Turn your sweat into sweet rewards when you earn RunCoins™ for every calorie burned with a chance to earn bonuses for running streaks and winning races.\nRedeem your RunCoins™ in our built-in Rewards Store for fitness gear & adventures OR trade them for Real TONUS™ cryptocurrency on the tonus.coin OR tonuscoin.com network."},

                        @{CTitle:@"THE ULTIMATE RUNNING EXPERIENCE",CDescription:@"Race Against Anyone. Connect with motivated runners across the world, run together in real-time and voice-chat live as you compete. Get spoken audio cues that announce real-time status updates during the race and chat in real time after you throw down. It's the world’s largest running group."},
                        
                        @{CTitle:@"SOCIAL RUNNING AT ITS BEST",CDescription:@"Track your friends’ running activities, see how you measure up against other runners. Real time Direct Message your running buddies, comment, voice-chat and even share a photo or video during your run."},
                        
                        @{CTitle:@"STAY MOTIVATED",CDescription:@"Running with a partner is proven to keep you motivated and covering more miles. RunLive means you’ll always have a running partner to push you towards your goals. Prefer to run against friends? Setup a custom session, select a distance and invite your favorite running buddies."}];
    
    [collectionIntro reloadData];
    pageControler.numberOfPages = arrIntroduction.count;
    
    
    appDelegate.configureStravaCallBackAfterLogin = ^(BOOL isConnected)
    {
        if (isConnected && [[appDelegate getTopMostViewController] isKindOfClass:[IntroViewController class]]) {
            [self btnStravaCLK:btnStrava];
        }
    };
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    appDelegate.isShowActivityScreen = YES;

    [self addBlureVideo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GiFHUD dismiss];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    player = nil;
}


#pragma mark - Blure Video Effect
-(void)addBlureVideo
{
    // Video setup for count down...
    
    dispatch_async(GCDMainThread, ^{
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:@"IntroScreenIphoneX" ofType:@"mp4"];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath] ;
        player = [AVPlayer playerWithURL:movieURL]; //
        player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        AVPlayerLayer *avLayer = [AVPlayerLayer layer];
        [avLayer setPlayer:player];
        [avLayer setFrame:CGRectMake(0, 0, CScreenWidth,CScreenHeight)];
        [avLayer setBackgroundColor:[UIColor clearColor].CGColor];
        [avLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        player.muted = YES;
        [imgRun.layer addSublayer:avLayer];
        [player play];
        
        player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
    });
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    [player play];
}

#pragma mark - Helper Method

-(void)setForTermsAndPolicy
{
    NSString *strTermsOffService = @"Terms of Service";
    NSString *strPrivacyAndPolicy = @"Privacy Policy.";
    
    
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"By signing up, you agree to our %@ and %@",strTermsOffService,strPrivacyAndPolicy]];
    [attrString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:(NSRange){32,strTermsOffService.length}];
    [attrString addAttribute: NSFontAttributeName value:CFontGilroyBold(lblTermsAndConditions.font.pointSize) range: NSMakeRange(32,strTermsOffService.length)];
    
    [attrString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:(NSRange){53,strPrivacyAndPolicy.length}];
    [attrString addAttribute: NSFontAttributeName value:CFontGilroyBold(lblTermsAndConditions.font.pointSize) range: NSMakeRange(53,strPrivacyAndPolicy.length)];
    
    __weak typeof (self) weakSelf = self;
    [lblTermsAndConditions addHashTagAndUserHandler:strPrivacyAndPolicy Complete:^(NSString *urlString) {
        NSLog(@"%@",urlString);
        TermsAndPrivacyPoilicyViewController *objPrivacyAndPolicy = [TermsAndPrivacyPoilicyViewController new];
        objPrivacyAndPolicy.isTOS = [urlString isEqualToString:strTermsOffService];
        [weakSelf.navigationController pushViewController:objPrivacyAndPolicy animated:YES];
    }];
    
    [lblTermsAndConditions addHashTagAndUserHandler:strTermsOffService Complete:^(NSString *urlString) {
        NSLog(@"%@",urlString);
        TermsAndPrivacyPoilicyViewController *objTOS = [TermsAndPrivacyPoilicyViewController new];
        objTOS.isTOS = [urlString isEqualToString:strTermsOffService];
        [weakSelf.navigationController pushViewController:objTOS animated:YES];
    }];
    
    lblTermsAndConditions.attributedText = attrString;
}

- (void)setInitInTextFiled:(ACFloatingTextField *)textfield andPlaceholder:(NSString *)placeholder
{
    [textfield setTextFieldPlaceholderText:placeholder];
    textfield.imageWrong = @"wrong_lrf";
    textfield.selectedLineColor = CLineSelected;
    textfield.placeHolderColor = CLRFPlaceholderColor;
    textfield.selectedPlaceHolderColor = CLRFPlaceholderSelectedColor;
    textfield.lineColor = CLRFTextFieldLineColor;
}

- (void)setViewAsPerSignUpAndLogin
{
    [txtEmail setText:@""];
    [txtEmail floatTheLabel];
    [txtCommon setText:@""];
    [txtCommon floatTheLabel];
    [txtConfirmPwd setText:@""];
    [txtConfirmPwd floatTheLabel];
    [txtPassword setText:@""];
    [txtPassword floatTheLabel];
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    
    viewLoginLine.backgroundColor = CLineUnSelected;
    viewSignUpLine.backgroundColor = CLineUnSelected;
    if (btnSignUp.selected == YES)
    {
        [btnForgetPwd hideByHeight:YES];
        [txtPassword setHidden:NO];
        [txtConfirmPwd setHidden:NO];

        [lblTermsAndConditions hideByHeight:NO];
        
        [btnRegular setConstraintConstant:10 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        [btnRegular setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];

        viewSignUpLine.backgroundColor = CLineSelected;
        [btnRegular setTitle:@"REGISTER" forState:UIControlStateNormal];
        [btnSocial setTitle:@"CONNECT WITH FACEBOOK" forState:UIControlStateNormal];

        [txtEmail setTextFieldPlaceholderText:@"Email address"];
        [txtCommon setTextFieldPlaceholderText:@"Username"];
         [txtCommon setSecureTextEntry:NO];
        [btnSocial setImageEdgeInsets:UIEdgeInsetsMake(0, Is_iPhone_5?-62:-90, 0, 0)];
    }
    else if (btnLogin.selected == YES)
    {
        [btnForgetPwd hideByHeight:NO];
        [lblTermsAndConditions hideByHeight:YES];

        [txtPassword setHidden:YES];
        [txtConfirmPwd setHidden:YES];

        [btnRegular setConstraintConstant:-CViewHeight(lblTermsAndConditions) toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        [btnRegular setConstraintConstant:-(CViewHeight(txtPassword) + CViewHeight(txtConfirmPwd)) toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
        viewLoginLine.backgroundColor = CLineSelected;
        [btnRegular setTitle:@"LOGIN" forState:UIControlStateNormal];
        [btnSocial setTitle:@"CONNECT WITH FACEBOOK" forState:UIControlStateNormal];
        [txtEmail setTextFieldPlaceholderText:@"Email address or username"];
        [txtCommon setTextFieldPlaceholderText:@"Password"];
        [txtCommon setSecureTextEntry:YES];
        [btnSocial setImageEdgeInsets:UIEdgeInsetsMake(0, Is_iPhone_5?-88:-114, 0, 0)];
     
        txtEmail.text = [CUserDefaults valueForKey:CUserName];
        [txtEmail becomeFirstResponder];
        [txtEmail resignFirstResponder];
        [txtEmail floatTheLabel:@"Email address or username" andText:txtEmail.text];
    }
}

-(NSAttributedString *)setTextWithCustomLineStyle:(NSString *)text maxheight:(CGFloat)maxheight linespacing:(CGFloat)linespacing
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:linespacing];
    paragraphStyle.maximumLineHeight = maxheight;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
   return attributedString ;

}

#pragma mark -  UITextfield Delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtEmail && btnSignUp.selected)
        [txtEmail floatTheLabel:@"email" andText:textField.text];
    else if(textField == txtCommon && btnSignUp.selected)
        [txtCommon floatTheLabel:@"username" andText:textField.text];
    else if (textField == txtEmail && btnLogin.selected)
        [txtEmail floatTheLabel:@"email and username" andText:textField.text];
    else if(textField == txtCommon && btnLogin.selected)
        [txtCommon floatTheLabel:@"password" andText:textField.text];
    else if(textField == txtPassword)
        [txtPassword floatTheLabel:@"password" andText:textField.text];
    else if(textField == txtConfirmPwd)
        [txtConfirmPwd floatTheLabel:@"confpassword" andText:textField.text];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txtEmail && btnSignUp.selected)
        [txtEmail floatTheLabel:@"" andText:textField.text];
    else if(textField == txtCommon && btnSignUp.selected)
        [txtCommon floatTheLabel:@"" andText:textField.text];
    else if (textField == txtEmail && btnLogin.selected)
        [txtEmail floatTheLabel:@"" andText:textField.text];
    else if (textField == txtCommon && btnLogin.selected)
        [txtCommon floatTheLabel:@"" andText:textField.text];
    else if(textField == txtPassword)
        [txtPassword floatTheLabel:@"" andText:textField.text];
    else if(textField == txtConfirmPwd)
        [txtConfirmPwd floatTheLabel:@"" andText:textField.text];

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (btnSignUp.selected && [textField isEqual:txtCommon])
    {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
        if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location != NSNotFound)
        {
            return NO;
        }
    }
    
    if ((btnLogin.selected && ![textField isEqual:txtCommon]) || ((btnSignUp.selected && ![textField isEqual:txtPassword]) && (btnSignUp.selected && ![textField isEqual:txtConfirmPwd])))
    {
        if (range.location == 0 && [string isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    if (textField == txtEmail && btnSignUp.selected)
        [txtEmail floatTheLabel:@"email" andText:strNew];
    else if(textField == txtCommon && btnSignUp.selected)
        [txtCommon floatTheLabel:@"username" andText:strNew];
    else if (textField == txtEmail && btnLogin.selected)
        [txtEmail floatTheLabel:@"email and username" andText:strNew];
    else if (textField == txtCommon && btnLogin.selected)
        [txtCommon floatTheLabel:@"password" andText:strNew];
    else if(textField == txtPassword)
        [txtPassword floatTheLabel:@"password" andText:strNew];
    else if(textField == txtConfirmPwd)
        [txtConfirmPwd floatTheLabel:@"confpassword" andText:strNew];
    
    return YES;
}

#pragma mark -  UICollectionView Delegates and Data Source

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth, CScreenHeight);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrIntroduction.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"IntroCollectionViewCell";
    IntroCollectionViewCell *cell = (IntroCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil)
        cell = [[IntroCollectionViewCell alloc] init];
    
    cell.imgCryptoCurrency.hidden = indexPath.item != 0;
    
    NSDictionary *dicData = arrIntroduction[indexPath.row];
    cell.lblTitle.attributedText = [self setTextWithCustomLineStyle:[dicData stringValueForJSON:CTitle] maxheight:40 linespacing:0.3];
    cell.lblDetails.attributedText = [self setTextWithCustomLineStyle:[dicData stringValueForJSON:CDescription] maxheight:30 linespacing:3];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (viewBottomX.constant == 0)
    {
        [self setHideShowBottomView:(btnLogin.selected)?btnLogin:btnSignUp];
    }
}

#pragma mark-
#pragma mark  UIScrollView Delegate
#pragma mark-

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
    pageControler.currentPage = page;
}

#pragma mark - Action Event

- (IBAction)btnSignUpClicked:(UIButton *)sender
{
    [self setHideShowBottomView:sender];
}

- (IBAction)btnLoginClicked:(UIButton *)sender
{
    [self setHideShowBottomView:sender];
}

- (void)setHideShowBottomView:(UIButton *)sender
{
    txtEmail.userInteractionEnabled = YES;
    
    if (sender.selected != YES)
    {
        sender.selected = YES;
        viewBottomX.constant = 0;
    }
    else
    {
        sender.selected = NO;
        viewBottomX.constant = -CViewHeight(viewBottom) + CViewHeight(btnSignUp);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    if(sender == btnLogin)
        btnSignUp.selected = NO;
    else
        btnLogin.selected = NO;
    
    [self setViewAsPerSignUpAndLogin];
}

- (IBAction)btnForgotPwdClicked:(UIButton *)sender
{
    ResetPasswordViewController *objReset = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];
    [self presentViewController:objReset animated:YES completion:nil];
}


-(IBAction)btnRegularCLK:(id)sender
{
    [appDelegate hideKeyboard];
    
    if (btnSignUp.selected)
    {
        // Sign up Code here...
        if (![txtEmail.text isBlankValidationPassed])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageValidEmail ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtEmail.text isValidEmailAddress])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageValidEmail ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtCommon.text isBlankValidationPassed])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageUsername ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtCommon.text isValidName:txtCommon.text])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageUsername ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtPassword.text isBlankValidationPassed] || txtPassword.text.length < 6)
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessagePassword ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtConfirmPwd.text isBlankValidationPassed])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageConfirmPassword ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtConfirmPwd.text isEqualToString:txtPassword.text])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessagePasswordDontMatch ButtonText:@"OK" Animation:YES completed:nil];
        else
        {
            [[APIRequest request] signup:txtEmail.text andUserName:txtCommon.text andPassword:txtConfirmPwd.text andFacebookID:nil FirstName:nil LastName:nil  Type:@"1" completed:^(id responseObject, NSError *error)
            {
               // Succesfully register.....
                [CUserDefaults setObject:txtEmail.text forKey:CUserName];
                [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:NO completed:nil];
                [self btnLoginClicked:btnLogin];
            }];
        }
    }
    else
    {
        // Login Code here...
        if (![txtEmail.text isBlankValidationPassed])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageLoginValidation ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtCommon.text isBlankValidationPassed])
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageLoginValidation ButtonText:@"OK" Animation:YES completed:nil];
        else if (![txtCommon.text isBlankValidationPassed] || txtCommon.text.length < 6)
            [self customAlertViewWithOneButton:CMessageSorry Message:CMessageLoginValidation ButtonText:@"OK" Animation:YES completed:nil];
        else
        {
            [self CallLoginAPi:txtEmail.text andPassword:txtCommon.text nsdFBID:nil];
        }
    }
}

-(IBAction)btnStravaCLK:(id)sender
{
    if (![FRDStravaClient sharedInstance].accessToken)
    {
        [[FRDStravaClient sharedInstance] authorizeWithCallbackURL:[NSURL URLWithString:@"FRDStravaClient://api.runlive.co"]
                                                         stateInfo:nil];
        return;
    }

    [GiFHUD showWithOverlay];
    [[FRDStravaClient sharedInstance] fetchCurrentAthleteWithSuccess:^(StravaAthlete *athlete) {
    [GiFHUD dismiss];
        NSLog(@"%@",athlete);
        
        // Call check STRAVA Id api here......
        if (![athlete.email isEqualToString:@""] && athlete.email)
            [self checkingForSocialLogin:NO SocailId:[NSString stringWithFormat:@"%ld",(long)athlete.id] Email:athlete.email FirstName:athlete.firstName LastName:athlete.lastName];
        else
            [self customAlertViewWithOneButton:@"" Message:CSTRAVAPermission ButtonText:@"Ok" Animation:YES completed:nil];
    }failure:^(NSError *error)
     {
         [GiFHUD dismiss];
         [[FRDStravaClient sharedInstance] setAccessToken:nil];
     }];
    
}

- (IBAction)btnSocialCLK:(id)sender
{
    if ([[FacebookClass sharedInstance] isAunthenticated])
    {
        [[SocialNetworks sharedInstance] logoutFromNetwork:SocialNetworkFacebook];
    }
    
    [GiFHUD showWithOverlay];
    [[SocialNetworks sharedInstance] loginWithSocialNetwork:SocialNetworkFacebook success:^(NSDictionary *result)
     {
         [GiFHUD dismiss];
         
         if (![[result stringValueForJSON:@"email"] isEqualToString:@""] && [result objectForKey:@"email"])
         {
             [self checkingForSocialLogin:YES SocailId:[result stringValueForJSON:@"id"] Email:[result stringValueForJSON:@"email"] FirstName:[result stringValueForJSON:@"first_name"] LastName:[result stringValueForJSON:@"last_name"]];
         }
         else
         {
             [self customAlertViewWithOneButton:@"" Message:CFacebookPermission ButtonText:@"Ok" Animation:YES completed:nil];
         }
     } failure:^(NSError *error)
     {
         [GiFHUD dismiss];
     }];
}

#pragma mark - Api Related Functions

-(void)checkingForSocialLogin:(BOOL)isFacebook SocailId:(NSString *)socialId Email:(NSString *)email FirstName:(NSString *)firstName LastName:(NSString *)lastName
{
    [[APIRequest request] checkSocailIds:socialId Email:email Type:isFacebook ? @"1" : @"2" completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             // First check facebook id with server...
             if ([[[responseObject valueForKey:CJsonExtra] numberForJson:@"status"] isEqual:@0])
             {
                 FacebookSignUpViewController *objFacebook = [FacebookSignUpViewController new];
                 objFacebook.strSocialEmail = email;
                 objFacebook.strSocialId = socialId;
                 objFacebook.strSocialFirstName = firstName;
                 objFacebook.strSocialLastName = lastName;
                 objFacebook.isLoginType = isFacebook;
                 
                 objFacebook.configureUsernameSuccessfullyAdded = ^(BOOL isAdded,NSString *strEmail)
                 {
                     if (isAdded)
                     {
                         // Continue with login flow...
                         [CUserDefaults setObject:strEmail forKey:CUserName];
                         
                         if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                             [self checkMotionPermissionView];
                         else
                             [self checkGPSPermissionView];
                     }
                 };
                 
                 [self presentViewController:objFacebook animated:YES completion:nil];
             }
             else
             {
                 // Continue with login flow...
                 [CUserDefaults setObject:email forKey:CUserName];
                 
                 if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                     [self checkMotionPermissionView];
                 else
                     [self checkGPSPermissionView];
             }
         }
     }];
}

-(void)CallLoginAPi:(NSString *)email andPassword:(NSString *)PWD nsdFBID:(NSString *)FBID
{
    [[APIRequest request] login:email andPassword:PWD andFacebookID:FBID completed:^(id responseObject, NSError *error)
     {
         [CUserDefaults setObject:txtEmail.text forKey:CUserName];
         
         if ([[[responseObject valueForKey:@"extra"] numberForJson:@"status"] isEqual:@0])
         {
             // not verified
             [self customAlertViewWithTwoButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonFirstText:@"Cancel" ButtonSecondText:@"Resend Email" Animation:YES completed:^(int index) {
                 if (index == 1)
                 {
                     // Call Api here
                     [[APIRequest request] resendEmailVerification:email completed:^(id responseObject, NSError *error)
                      {
                          if (responseObject && !error)
                          {
                              [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
                          }
                      }];
                 }
             }];
         }
         else
         {
             if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                 [self checkMotionPermissionView];
             else
                 [self checkGPSPermissionView];
         }
     }];
}

#pragma mark - Permission Related Methods
#pragma mark -

-(void)checkGPSPermissionView
{
    // Show GPS Permission View here...
    GPSPermissionView *objPermission = [GPSPermissionView initGPSPermissionView];
    [appDelegate.window addSubview:objPermission];
    
    [objPermission.btnGPSGoForIt touchUpInsideClicked:^{
        [appDelegate initialSetUpForLocaitonMananger];
        
        appDelegate.configureGpsPermissionStatus = ^(BOOL isAllow)
        {
            [objPermission.btnGPSNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
        };
    }];
    
    [objPermission.btnGPSNotNow touchUpInsideClicked:^{
        [self checkMotionPermissionView];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [objPermission removeFromSuperview];
        });
    }];
    
}

-(void)checkMotionPermissionView
{
        dispatch_async(GCDMainThread, ^{
            if (![CUserDefaults objectForKey:CMMotionManagerStatus])
            {
                // Show Motion Permission View here...
                MotionPermissionView *objMotion = [MotionPermissionView initMotionPermissionView];
                [appDelegate.window addSubview:objMotion];
                
                [objMotion.btnMotionGoForIt touchUpInsideClicked:^{
                    [appDelegate initializeMotionDetector];
                    
                    dispatch_async(GCDMainThread, ^{
                        [objMotion.btnMotionNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                    });
                }];
                
                [objMotion.btnMotionNotNow touchUpInsideClicked:^{
                    [self checkNotificationPermisssionView];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [objMotion removeFromSuperview];
                    });
                }];
            }
            else
            {
                dispatch_async(GCDMainThread, ^{
                    [self checkNotificationPermisssionView];
                });
            }
        });
}

-(void)checkNotificationPermisssionView
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus == UNAuthorizationStatusNotDetermined)
        {
            dispatch_async(GCDMainThread, ^{
                // Show Motion Permission View here...
                
                NotificationPermissionView *objNotification = [NotificationPermissionView initNotificationPermissionView];
                [appDelegate.window addSubview:objNotification];
                
                [objNotification.btnNotificationDoIt touchUpInsideClicked:^{
                    [appDelegate enableRemoteNotification];
                    [appDelegate oneSignalRegistration];
                    
                    appDelegate.configureNotificationPermissionStatus = ^(BOOL isGranted)
                    {
                        dispatch_async(GCDMainThread, ^{
                            [objNotification.btnNotificationNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                        });
                    };
                }];
                
                [objNotification.btnNotificationNotNow touchUpInsideClicked:^{
                    [self checkMicrophonePermissionView];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [objNotification removeFromSuperview];
                    });
                }];
            });
        }
        else
        {
            dispatch_async(GCDMainThread, ^{
                [self checkMicrophonePermissionView];
            });
        }
    }];
}

-(void)checkMicrophonePermissionView
{
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionUndetermined && appDelegate.loginUser.isVoiceChatEnable.boolValue)
    {
        dispatch_async(GCDMainThread, ^{
            // Show Motion Permission View here...
            
            VoiceChatPermissionView *objVoiceChat = [VoiceChatPermissionView initVoiceChatPermissionView];
            [appDelegate.window addSubview:objVoiceChat];
            
            [objVoiceChat.btnVoiceChatGotForIt touchUpInsideClicked:^{
                [objVoiceChat askForMicroPhonePermission];
                
                __weak typeof (objVoiceChat) weakObjVoiceChat = objVoiceChat;
                objVoiceChat.configurationVoiceChatPermissionAcceptReject = ^(BOOL isGranted)
                {
                    dispatch_async(GCDMainThread, ^{
                        [weakObjVoiceChat.btnVoiceChatNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                    });
                };
            }];
            
            [objVoiceChat.btnVoiceChatNotNow touchUpInsideClicked:^{
                [self checkHealthKitPermissionView];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [objVoiceChat removeFromSuperview];
                });
            }];
        });
    }
    else
    {
        dispatch_async(GCDMainThread, ^{
            [self checkHealthKitPermissionView];
        });

    }
    
}

-(void)checkHealthKitPermissionView
{
    if ([appDelegate.healthStore authorizationStatusForType:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning]] == HKAuthorizationStatusNotDetermined)
    {
        // Show Motion Permission View here...
        HealthKitPermissionView *objHealthKit = [HealthKitPermissionView initHealthKitPermissionView];
        [appDelegate.window addSubview:objHealthKit];
        
        [objHealthKit.btnHealthKitSure touchUpInsideClicked:^{
            if ([HKHealthStore isHealthDataAvailable])
            {
                NSSet *writeDataTypes = [appDelegate.healthStore dataTypesToWrite];
                NSSet *readDataTypes = [appDelegate.healthStore dataTypesToRead];
                
                [appDelegate.healthStore requestAuthorizationToShareTypes:writeDataTypes readTypes:readDataTypes completion:^(BOOL success, NSError *error)
                 {
                     dispatch_async(GCDMainThread, ^{
                         [objHealthKit.btnHealthKitNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                     });
                 }];
            }
        }];
        
        [objHealthKit.btnHealthKitNotNow touchUpInsideClicked:^{
            [self goInsideTheApp];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [objHealthKit removeFromSuperview];
            });
        }];
    }
    else
        [self goInsideTheApp];
}

-(void)goInsideTheApp
{
    if (appDelegate.loginUser.profileStatus.boolValue)
        appDelegate.window.rootViewController = [appDelegate setTabBarController];
    else
        appDelegate.window.rootViewController = appDelegate.objSuperAccountSeting;
    
    [appDelegate registerDeviceTokenForNotification];
    [appDelegate configurePubnub];
    [appDelegate pubNubAddPushNotifications];
    [appDelegate toCheckRunningSessionIsAvailable];
    //             [appDelegate checkInAppPurchaseStatus:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setHideShowBottomView:btnLogin];
    });
}

@end
