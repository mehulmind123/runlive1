
#import <UIKit/UIKit.h>

@interface ClipView : UIView
- (void) updateAngle:(CGFloat)setAngle;

@property (nonatomic, copy) IBInspectable UIImage * logo;

@end
