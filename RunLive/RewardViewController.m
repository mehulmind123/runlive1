//
//  RewardViewController.m
//  CollectionViewDemo
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "RewardViewController.h"
#import "RewardProductCell.h"
#import "MyBasketViewController.h"
#import "ProductDetailsViewController.h"
#import "OrdersViewController.h"

#define CELLWIDTH CScreenWidth * 80/100

@interface RewardViewController ()

@end

@implementation RewardViewController
{
    NSMutableArray *arrProduct;
    NSIndexPath *SelectedIndexPathTapBar;
    
    int currentPage;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:YES];
    
    currentPage = 0;
    
    [clProduct registerNib:[UINib nibWithNibName:@"RewardProductCell" bundle:nil] forCellWithReuseIdentifier:@"RewardProductCell"];
 
    arrProduct = [NSMutableArray new];
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:0 inSection:0];
    [clProduct reloadData];
    
    cnPageControllerBottomSpace.constant = Is_iPhone_X ? 77 : 57;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate showTabBar];
    
    lblUserName.text = [NSString stringWithFormat:@"%@, YOU HAVE",appDelegate.loginUser.user_name.uppercaseString];

    [[APIRequest request] otherUserProfile:appDelegate.loginUser.user_name showLoader:NO completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             appDelegate.loginUser.reward_point = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"reward_point"];
             //    lblTotalPoints.text = appDelegate.loginUser.reward_point;
             
             NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
             [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
             NSString *strPoint = [numberFormatter stringFromNumber:[NSNumber numberWithInt:appDelegate.loginUser.reward_point.intValue]];
             lblTotalPoints.text = strPoint;

         }
     }];
    
    [self getDataFromServer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - General method
-(void)getDataFromServer
{
    // Set User Reward Info here...
    
    activityIndicatorView.hidden = NO;
    [activityIndicatorView startAnimating];
    
    [[APIRequest request] productList:^(id responseObject, NSError *error)
    {
        activityIndicatorView.hidden = YES;
        [activityIndicatorView stopAnimating];
        
        [arrProduct removeAllObjects];
        [arrProduct addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"products"]];
        [clProduct reloadData];
        
        pageControl.numberOfPages = arrProduct.count;
        pageControl.currentPage = currentPage > arrProduct.count ? arrProduct.count : currentPage;
        
    }];
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrProduct.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"RewardProductCell";
    RewardProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.transform = [SelectedIndexPathTapBar isEqual:indexPath] ? CGAffineTransformIdentity : TRANSFORM_CELL_VALUE;
    
    NSDictionary *dicData = [arrProduct objectAtIndex:indexPath.item];
    cell.lblProductName.text = [dicData stringValueForJSON:@"name"];
    cell.lblDescription.text = [dicData stringValueForJSON:@"short_description"];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    cell.lblPoints.text = [numberFormatter stringFromNumber:[NSNumber numberWithInt:[dicData stringValueForJSON:@"points"].intValue]];
    [cell.imgProduct setImageWithURL:[appDelegate resizeImage:nil Height:[NSString stringWithFormat:@"%f",CViewHeight(cell.imgProduct)*2] imageURL:[dicData stringValueForJSON:@"image"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    
#pragma mark - More Details CLK
    [cell.btnMoreDetails touchUpInsideClicked:^{
        ProductDetailsViewController *objPro = [[ProductDetailsViewController alloc] init];
        objPro.strProductId = [dicData stringValueForJSON:@"_id"];
        [self.navigationController pushViewController:objPro animated:YES];
    }];
    
#pragma mark - Add To Cart
    [cell.btnAddToBasket touchUpInsideClicked:^{
        [[APIRequest request] addProductToBasket:[dicData stringValueForJSON:@"_id"] completed:^(id responseObject, NSError *error)
        {
            MyBasketViewController *obj = [[MyBasketViewController alloc] init];
            [self.navigationController pushViewController:obj animated:NO];
        }];
    }];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width * CELLWIDTH)/CScreenWidth , CGRectGetHeight(clProduct.frame) - 10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2, 0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (SelectedIndexPathTapBar)
    {
        RewardProductCell *cell = (RewardProductCell *)[clProduct cellForItemAtIndexPath:SelectedIndexPathTapBar];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
    }
    
    RewardProductCell *cell = (RewardProductCell *)[clProduct cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        cell.transform = CGAffineTransformIdentity;
    }];
    
    SelectedIndexPathTapBar = indexPath;
    [clProduct scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [pageControl setCurrentPage:(int)SelectedIndexPathTapBar.item];
    currentPage = (int)pageControl.currentPage;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
        //    float pageWidth = (((CGRectGetWidth(clPlayers.frame))/CScreenWidth) + CELLWIDTH); // width + space
        float pageWidth = CELLWIDTH;
        float currentOffset = scrollView.contentOffset.x;
        float targetOffset = targetContentOffset->x;
        float newTargetOffset = 0;
        
        if (targetOffset > currentOffset)
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
        else
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
        
        if (newTargetOffset < 0)
            newTargetOffset = 0;
        else if (newTargetOffset > scrollView.contentSize.width)
            newTargetOffset = scrollView.contentSize.width;
        
        targetContentOffset->x = currentOffset;
        [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
        int index = newTargetOffset / pageWidth;
    
    [pageControl setCurrentPage:index];
    currentPage = (int)pageControl.currentPage;
    
        SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:index inSection:0];
    
        if (index == 0) // If first index
        {
            UICollectionViewCell *cell1 = [clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
            RewardProductCell *cell = (RewardProductCell *)cell1;
            
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = CGAffineTransformIdentity;
            }];
            
            cell = (RewardProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
            }];
            
        }
        else
        {
            UICollectionViewCell *cell1 = [clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            RewardProductCell *cell = (RewardProductCell *)cell1;
            ;
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = CGAffineTransformIdentity;
            }];
            
           
            index --; // left
            cell = (RewardProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
            }];
            
            index ++;
            index ++; // right
            cell = (RewardProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
            }];
        }
    
    if(arrProduct.count == 0)
        return;
}

#pragma mark - Action Event
#pragma mark

-(IBAction)btnBasketCLK:(id)sender
{
    MyBasketViewController *obj = [[MyBasketViewController alloc] init];
    [self.navigationController pushViewController:obj animated:NO];
}

-(IBAction)btnOrderCLK:(id)sender
{
    OrdersViewController *objOrder = [[OrdersViewController alloc] init];
    [self.navigationController pushViewController:objOrder animated:NO];

}
@end
