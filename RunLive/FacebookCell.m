//
//  FacebookCell.m
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "FacebookCell.h"

@implementation FacebookCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];

}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    self.viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    self.viewUser.layer.borderWidth = 1;
    self.viewUser.layer.borderColor = CRGB(235, 241, 245).CGColor;
}

@end
