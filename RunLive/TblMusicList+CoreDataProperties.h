//
//  TblMusicList+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 20/09/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblMusicList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblMusicList (CoreDataProperties)

+ (NSFetchRequest<TblMusicList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *album_image;
@property (nullable, nonatomic, copy) NSString *artist_name;
@property (nullable, nonatomic, copy) NSString *cover_image;
@property (nullable, nonatomic, copy) NSString *followers;
@property (nullable, nonatomic, copy) NSNumber *isApple;
@property (nullable, nonatomic, copy) NSString *main_spotify_uri;
@property (nullable, nonatomic, copy) NSString *playlist_name;
@property (nullable, nonatomic, copy) NSString *song_image;
@property (nullable, nonatomic, copy) NSString *song_name;
@property (nullable, nonatomic, copy) NSString *song_url;
@property (nullable, nonatomic, copy) NSString *songId;

@end

NS_ASSUME_NONNULL_END
