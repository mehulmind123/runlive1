//
//  ChangePWDViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/7/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ChangePWDViewController.h"

@interface ChangePWDViewController ()

@end

@implementation ChangePWDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    btnSave.layer.cornerRadius = 3;
    
    [txtOldPWD setTextFieldPlaceholderText:@"Old password"];
    [txtNewPWD setTextFieldPlaceholderText:@"New password"];
    [txtConfirmPWD setTextFieldPlaceholderText:@"Confirm password"];
    
    txtOldPWD.imageWrong = txtNewPWD.imageWrong = txtConfirmPWD.imageWrong = @"worng_lrf";
    txtOldPWD.selectedLineColor = txtNewPWD.selectedLineColor = txtConfirmPWD.selectedLineColor = CLineSelected;
    txtOldPWD.placeHolderColor =  txtNewPWD.placeHolderColor = txtConfirmPWD.placeHolderColor = CLRFPlaceholderColor;
    txtOldPWD.selectedPlaceHolderColor = txtNewPWD.selectedPlaceHolderColor = txtConfirmPWD.selectedPlaceHolderColor = CLRFPlaceholderSelectedColor;
    txtOldPWD.lineColor = txtNewPWD.lineColor = txtConfirmPWD.lineColor = CLRFTextFieldLineColor;

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark  UITextfield Delegates
#pragma mark

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
     if(textField == txtOldPWD)
        [txtOldPWD floatTheLabel:@"password" andText:textField.text];
     else if(textField == txtNewPWD)
         [txtNewPWD floatTheLabel:@"password" andText:textField.text];
    else if(textField == txtConfirmPWD)
        [txtConfirmPWD floatTheLabel:@"confpassword" andText:textField.text];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == txtOldPWD)
        [txtOldPWD floatTheLabel:@"" andText:textField.text];
    else if(textField == txtNewPWD)
        [txtNewPWD floatTheLabel:@"" andText:textField.text];
    else if(textField == txtConfirmPWD)
        [txtConfirmPWD floatTheLabel:@"" andText:textField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
     if(textField == txtOldPWD)
        [txtOldPWD floatTheLabel:@"password" andText:strNew];
    else if(textField == txtNewPWD)
        [txtNewPWD floatTheLabel:@"password" andText:strNew];
    else if(textField == txtConfirmPWD)
        [txtConfirmPWD floatTheLabel:@"confpassword" andText:strNew];
    
    return YES;
}

#pragma mark - Action Event

-(IBAction)btnSaveCLK:(id)sender
{
    if (![txtOldPWD.text isBlankValidationPassed] || txtOldPWD.text.length < 6)
        [self customAlertViewWithOneButton:CMessageSorry Message:@"That doesn’t seem to be your old password." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtNewPWD.text isBlankValidationPassed] || txtNewPWD.text.length < 6)
        [self customAlertViewWithOneButton:CMessageSorry Message:@"Create a password at least 6 characters long." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtConfirmPWD.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:CMessageSorry Message:@"Please confirm your password." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtConfirmPWD.text isEqualToString:txtNewPWD.text])
        [self customAlertViewWithOneButton:CMessageSorry Message:CMessagePasswordDontMatch ButtonText:@"OK" Animation:YES completed:nil];
    else
    {
        [[APIRequest request] changePWD:txtOldPWD.text NewPWD:txtConfirmPWD.text completed:^(id responseObject, NSError *error)
        {
            [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
}

@end
