//
//  Copyright © 2012 BeauxApps, LLC
//

#import <UIKit/UIKit.h>

#import "Master.h"

@interface UIViewController (ADVOverlay)

- (void) presentOverlayViewController:(UIViewController *)controller
                             animated:(BOOL)animated
                           completion:(MIVoidBlock)completion;

- (void) dismissOverlayViewControllerAnimated:(BOOL)animated
                                   completion:(MIVoidBlock)completion;


-(PopUpViewController *)presentView:(UIView *)view;
-(PopUpViewController *)presentView_DissmissEffect:(UIView *)view;
-(PopUpViewController *)presentView:(UIView *)view shouldHideOnOutsideClick:(BOOL)shouldHideOnOutsideClick;
-(PopUpViewController *)presentView:(UIView *)view dismissed:(MIVoidBlock)completion;
-(PopUpViewController *)presentViewWithOutsideClickedDisable:(UIView *)view;
-(PopUpViewController *)presentViewOnPopUpViewController:(UIView *)view shouldClickOutside:(BOOL)click dismissed:(MIVoidBlock)completion;

-(void)customAlertViewWithOneButton:(NSString *)title Message:(NSString *)strMessage ButtonText:(NSString *)strButtonText Animation:(BOOL)isAnimate completed:(void (^)())completion;

-(void)customAlertViewWithTwoButton:(NSString *)title Message:(NSString *)strMessage ButtonFirstText:(NSString *)strButtonFirst ButtonSecondText:(NSString *)strButtonSecond Animation:(BOOL)isAnimate completed:(void (^)(int index))completion;

-(void)customAlertViewWithThreeButton:(NSString *)title Message:(NSString *)strMessage ButtonFirstText:(NSString *)strButtonFirst ButtonSecondText:(NSString *)strButtonSecond ButtonThirdText:(NSString *)strButtonThird Animation:(BOOL)isAnimate completed:(void (^)(int index))completion;

-(void)forcefullyRemoveCustomAlertFromSuperView;

@end


