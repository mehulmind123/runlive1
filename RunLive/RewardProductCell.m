//
//  RewardProductCell.m
//  CollectionViewDemo
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "RewardProductCell.h"

@implementation RewardProductCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgProduct.layer.masksToBounds = YES;
    self.viewContainer.layer.masksToBounds = YES;
    self.viewContainer.layer.cornerRadius = 3;

    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 3.0;
    self.layer.shadowOffset = CGSizeMake(.8, .8);
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 8;
}

@end
