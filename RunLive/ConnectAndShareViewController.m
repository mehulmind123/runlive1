//
//  ConnectAndShareViewController.m
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ConnectAndShareViewController.h"

@interface ConnectAndShareViewController ()
{
    NSArray *arrTitle;
}

@end

@implementation ConnectAndShareViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tblshare registerNib:[UINib nibWithNibName:@"ConnectAndShareCell" bundle:nil] forCellReuseIdentifier:@"ConnectAndShareCell"];
    
    [self Initialization];
    
    appDelegate.configureStravaCallBackAfterLogin = ^(BOOL isConnected)
    {
        if (isConnected && [[appDelegate getTopMostViewController] isKindOfClass:[ConnectAndShareViewController class]])
        {
            [self connectWithStrava];
        }
    };

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)Initialization
{
    arrTitle = @[@{@"title":@"Share to Facebook account",@"img":@"ic_share_facebook"},
                 @{@"title":@"Share to Twitter account",@"img":@"ic_share_twiter"},
                 @{@"title":@"Share to Instagram account",@"img":@"ic_share_instagrame"},
                 @{@"title":@"Share by email account",@"img":@"ic_share_mail"},
                 @{@"title": appDelegate.loginUser.stravaConnection.boolValue ? @"Connected" : @"Connect with Strava",@"img":@"ic_strava_connection"}];
    
    [tblshare reloadData];
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Datasource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"ConnectAndShareCell";
    ConnectAndShareCell *cell = [tblshare dequeueReusableCellWithIdentifier:strIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.lblTitle.text = [[arrTitle valueForKey:@"title"]objectAtIndex:indexPath.row];
    cell.imgShare.image = [UIImage imageNamed:[[arrTitle valueForKey:@"img"]objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row)
    {
        case 0: // Facebook
        {
                SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebookSheet addURL:[NSURL URLWithString:CAppStoreUrl]];
                [facebookSheet addImage:[UIImage imageNamed:@"appicon.jpg"]];
                [self presentViewController:facebookSheet animated:YES completion:Nil];
        }
            break;
        case 1: // Twitter
        {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [tweetSheet setInitialText:CAppStoreUrl];
                //                [tweetSheet addURL:[NSURL URLWithString:CAppStoreUrl]];
                [tweetSheet addImage:[UIImage imageNamed:@"appicon.jpg"]];
                [self presentViewController:tweetSheet animated:YES completion:nil];
        }
            break;
        case 2: // Instagrame
        {
            [self shareOnInstagram];
        }
            break;
        case 3: // Mail
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            if ([MFMailComposeViewController canSendMail])
            {
                mailer.mailComposeDelegate = self;
                mailer.navigationController.navigationBar.translucent = NO;
                [mailer setMessageBody:CAppStoreUrl isHTML:NO];
                [mailer setSubject:@""];
                
                [self presentViewController:mailer animated:YES completion:^(void){
                    [self hideShowStatusBar:YES];
                }];
            }
        }
            break;
            case 4:
        {
            if (!appDelegate.loginUser.stravaConnection.boolValue)
            {
                [self connectWithStrava];
            }
            else
            {
                NSLog(@"Starava already Connected ==== ");
            }
            
        }
            break;
        default:
            break;
    }
}

#pragma mark - Instagram Sharing

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
    NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}


-(void)shareOnInstagram
{

//    NSString *saveImagePath=[CDocumentsDirectory stringByAppendingPathComponent:@"Instaappicon.jpg"];
//    [UIImagePNGRepresentation([UIImage imageNamed:@"appicon.jpg"]) writeToFile:saveImagePath atomically:YES];
//    NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
    
    NSData *imgData = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"appicon.jpg"], 0.8)];
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://location?id=1"];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        [self instagram:[UIImage imageWithData:imgData]];
    }
    else
    {
        [self customAlertViewWithOneButton:@"" Message:@"You need to install Instagram and then retry." ButtonText:@"OK" Animation:YES completed:nil];
    }
}

-(void)instagram:(UIImage*)imgName
{
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
    NSData *imageData = UIImagePNGRepresentation(imgName);
    [imageData writeToFile:savedImagePath atomically:YES];
    NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
    
    self.documentController = [[UIDocumentInteractionController alloc] init];
    self.documentController.UTI = @"com.instagram.photo";
    [self.documentController setURL:imageUrl];
    self.documentController.delegate = self;
    self.documentController.annotation = [NSDictionary dictionaryWithObject:CAppStoreUrl forKey:@"InstagramCaption"];
    [self.documentController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
}

#pragma mark - STRAVA Connection

-(void)connectWithStrava
{
    if (![FRDStravaClient sharedInstance].accessToken)
    {
        [[FRDStravaClient sharedInstance] authorizeWithCallbackURL:[NSURL URLWithString:@"FRDStravaClient://api.runlive.co"] stateInfo:nil];
        return;
    }
    
    [GiFHUD showWithOverlay];
    [[FRDStravaClient sharedInstance] fetchCurrentAthleteWithSuccess:^(StravaAthlete *athlete) {
        [GiFHUD dismiss];
        
        [[APIRequest request] connectWithStrava:[NSString stringWithFormat:@"%ld",(long)athlete.id] completed:^(id responseObject, NSError *error)
        {
            if ([[responseObject valueForKey:CJsonStatus] isEqual:@1])
            {
                appDelegate.loginUser.strava_id = [NSString stringWithFormat:@"%ld",(long)athlete.id];
                appDelegate.loginUser.stravaConnection = @YES;
                [self Initialization];
            }
            else
            {
                [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"Ok" Animation:YES completed:nil];
            }
        }];
    }failure:^(NSError *error)
     {
         [GiFHUD dismiss];
         [[FRDStravaClient sharedInstance] setAccessToken:nil];
     }];
}

@end
