//
//  ReadMSGCell.m
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ReadMSGCell.h"
#import "ActivityMessageCell.h"

@implementation ReadMSGCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];

}

- (void)layoutSubviews
{
    [super layoutSubviews];

    self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;
}

@end
