//
//  IntroCollectionViewCell.h
//  RunLive
//
//  Created by mac-0008 on 24/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDetails;
@property (strong, nonatomic) IBOutlet UIImageView *imgCryptoCurrency;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnImgCryptoHieght,*cnImgCryptoWidth,*cnImgCryptoY;

@end
