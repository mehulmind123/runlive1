 //
//  SessionListCell.m
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionListCell.h"

@implementation SessionListCell
{
    int seconds;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.viewSubContainer.layer.masksToBounds = YES;
    self.viewContainer.layer.masksToBounds = NO;
    self.viewContainer.layer.shadowOffset = CGSizeMake(.8, .8);
    self.viewContainer.layer.shadowOpacity = 0.2;
    self.viewContainer.layer.shadowRadius = 5;
    self.viewContainer.layer.cornerRadius = self.viewSubContainer.layer.cornerRadius = 3.0;
    
    _imgCircleTag.layer.cornerRadius = CGRectGetHeight(_imgCircleTag.frame)/2;
    
    
    _btnSyncedUser.layer.cornerRadius = CGRectGetHeight(_btnSyncedUser.frame)/2;
    
    _viewSynced1.layer.cornerRadius = _viewSynced2.layer.cornerRadius = CGRectGetHeight(_viewSynced1.frame)/2;
    _viewSynced1.layer.masksToBounds = _viewSynced2.layer.masksToBounds = YES;
    
    _imgSynced1.layer.cornerRadius = _imgSynced2.layer.cornerRadius =  CGRectGetHeight(_imgSynced1.frame)/2;
    _imgSynced1.layer.masksToBounds = _imgSynced2.layer.masksToBounds = YES;

    self.viewBlueLine.hidden = YES;
}


#pragma mark - Timer Related functions


-(void)startTimer
{
    [self stopTimer];
    self.timer = nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];

    double sessionEndTimeStamp = self.objSessionTime.session_end_time.doubleValue;
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;
    
    if (difTimeStamp > 0)
    {
        seconds = difTimeStamp;
        _timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    else
        self.lblTime.text=@"00:00:00";
}

- (void)updateCountdown
{
//   NSLog(@"SESSION LIST TIMER VALUE ======== >>%@",self.objSessionTime.session_name);
    seconds--;
    if (seconds >= 0)
    {
        int tempSecond = seconds;

        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        int hours = tempSecond / (60 * 60);
          tempSecond = tempSecond - hours * (60 * 60);
       int minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;
        
        if (days > 0)
        {
            // Time with day... (DDd:HHh)
            self.lblTime.text=[NSString stringWithFormat:@"%02dd:%02dh",days,hours];
            self.viewBlueLine.hidden = YES;
        }
        else
        {
            //hh:mm:ss
            self.lblTime.text=[NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,tempSecond];
            self.viewBlueLine.hidden = (minutes < 15 && hours == 0) ? NO : YES;
        }
    }
    else
    {
        [self stopTimer];
        self.lblTime.text=@"00:00:00";
        
        if (self.configureSesionTimeOver)
            self.configureSesionTimeOver(self.objSessionTime);
    }
}

-(void)stopTimer
{
    [self.timer invalidate];
//    self.lblTime.text=@"00:00:00";
}



@end
