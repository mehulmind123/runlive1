//
//  MyProfileViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MyProfileViewController.h"
#import "SettingCell.h"
#import "SyncedActivityCell.h"
#import "StartActivityCell.h"
#import "ActivityAddPhotoCell.h"
#import "ProfileHeaderCell.h"
#import "ProfileHeaderView.h"
#import "SettingViewController.h"
#import "CommentsViewController.h"
#import "ActivityDetailViewController.h"
#import "NotificationViewController.h"
#import "SessionDetailsViewController.h"



@interface MyProfileViewController ()
@end

@implementation MyProfileViewController
{
    NSIndexPath *indexPathForVideoCell;
    ActivityAddPhotoCell *cellActivityVideo;
    BOOL shouldShowLoader;
    NSMutableArray *arrActivityList;
    
    BOOL isApiStarted;
    int iOffset;
    UIRefreshControl *refreshControl;
    NSURLSessionDataTask *taskRuningActivityFeed;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrActivityList = [NSMutableArray new];
    
    [tblUser registerNib:[UINib nibWithNibName:@"ProfileHeaderCell" bundle:nil] forCellReuseIdentifier:@"ProfileHeaderCell"];
    [tblUser registerNib:[UINib nibWithNibName:@"ActivityAddPhotoCell" bundle:nil] forCellReuseIdentifier:@"ActivityAddPhotoCell"];
    [tblUser registerNib:[UINib nibWithNibName:@"StartActivityCell" bundle:nil] forCellReuseIdentifier:@"StartActivityCell"];
    [tblUser registerNib:[UINib nibWithNibName:@"SyncedActivityCell" bundle:nil] forCellReuseIdentifier:@"SyncedActivityCell"];
    
    tblUser.estimatedRowHeight = 621;
    tblUser.rowHeight = UITableViewAutomaticDimension;
    shouldShowLoader = YES;
    
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblUser addSubview:refreshControl];
    
    iOffset = 0;
    [self getActivityDataFromServer];
    
    appDelegate.configureRefreshMyProfileActivityList = ^(ActivityData activityData)
    {
        if (activityData == CallActivityApi)
        {
            iOffset = 0;
            [self getActivityDataFromServer];
        }
        else if (activityData == RefreshActivityList)
        {
            [tblUser reloadData];
        }
        else if (activityData == DeleteActivity)
            [self fetchAllActivityFromLocal];
   };
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:YES];
    
    if (self.isBackButton)
    {
        [appDelegate hideTabBar];
        cnTblBottomSpace.constant = 0;
    }
    else
    {
        [appDelegate showTabBar];
        //cnTblBottomSpace.constant = 49;
        cnTblBottomSpace.constant = CScreenHeight - CViewY(appDelegate.objCustomTabBar);
    }
    
    if (appDelegate.isMoveOnNotificationScreen)
    {
        // Move on Notification Screen...        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            appDelegate.isMoveOnNotificationScreen = NO;
            NotificationViewController *objNot = [[NotificationViewController alloc] init];
            [self.navigationController pushViewController:objNot animated:YES];
        });
        return;
    }

    [self getUserBasicDetails];
    [self fetchAllActivityFromLocal];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // To Stop video when screen will disappear ........
    [self stopAllreadyPlayedVideo];
    
    if (taskRuningActivityFeed && taskRuningActivityFeed.state == NSURLSessionTaskStateRunning)
        [taskRuningActivityFeed cancel];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - API Related Function
#pragma mark -

-(void)getUserBasicDetails
{
    [[APIRequest request] otherUserProfile:appDelegate.loginUser.user_name showLoader:shouldShowLoader completed:^(id responseObject, NSError *error)
     {
         shouldShowLoader = NO;
         if (responseObject && !error)
         {
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             
             appDelegate.loginUser.is_verified = [[dicData numberForJson:@"is_verified"] isEqual:@1] ? @YES : @NO;
             appDelegate.loginUser.notification_count = [dicData stringValueForJSON:@"notification_count"];
             
             NSDictionary *dicRun = [dicData valueForKey:@"run"];
             appDelegate.loginUser.run_total_calories = [dicRun stringValueForJSON:@"total_calories"];
             appDelegate.loginUser.run_total_miles = [dicRun stringValueForJSON:@"total_distance"];
             appDelegate.loginUser.run_total_runs = [dicRun stringValueForJSON:@"total_runs"];
             appDelegate.loginUser.run_total_BPM = [dicRun stringValueForJSON:@"average_bpm"];
             
             [[Store sharedInstance].mainManagedObjectContext save];
             
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
             [tblUser reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             
             appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = appDelegate.loginUser.notification_count.intValue < 1;
             [appDelegate setNotificationBadgeOnAppIcon];
         }
     }];
}


#pragma mark - Pull Refresh
#pragma mark -

-(void)handleRefresh
{
    iOffset = 0;
    [self getActivityDataFromServer];
    
    [refreshControl endRefreshing];
}

- (void)loadMoreData
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self getActivityDataFromServer];
}


-(void)getActivityDataFromServer
{
    if (!appDelegate.loginUser.user_name)
        return;
    
    isApiStarted = YES;
    
    taskRuningActivityFeed = [[APIRequest request] GetActivityFeedList:[NSNumber numberWithInt:iOffset] Type:@"2" UserName:appDelegate.loginUser.user_name completed:^(id responseObject, NSError *error)
                              {
                                  isApiStarted = NO;
                                  [refreshControl endRefreshing];
                                  if (responseObject && !error)
                                  {
                                      if (iOffset == 0)
                                      {
                                          [arrActivityList removeAllObjects];
                                          [tblUser reloadData];
                                      }
                                      
                                      iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
                                      NSArray *arrTemp = [responseObject valueForKey:CJsonData];
                                      
                                      if (arrTemp.count > 0)
                                          [self fetchAllActivityFromLocal];
                                      
                                  }
                              }];
}

-(void)fetchAllActivityFromLocal
{
    [arrActivityList removeAllObjects];
    NSArray *arrTempData = [TblActivityDates fetch:[NSPredicate predicateWithFormat:@"user_name == %@",appDelegate.loginUser.user_name] orderBy:@"timestamp" ascending:NO];
    [arrActivityList addObjectsFromArray:arrTempData];
    [tblUser reloadData];
    [self playVideoAutomatically];
}

-(NSArray *)getActivityArray:(TblActivityDates *)objActivitySection
{
    NSMutableArray *arrActTemp = [NSMutableArray new];
        
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(user_name == %@)", appDelegate.loginUser.user_name];

    if (objActivitySection.objActivityPhoto.allObjects.count > 0 && [objActivitySection.activity_filter_type isEqual:@0])
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityPhoto.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityVideo.allObjects.count > 0  && [objActivitySection.activity_filter_type isEqual:@0])
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityVideo.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivitySyncSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivitySyncSession.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityRunSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityRunSession.allObjects filteredArrayUsingPredicate:predicate]];
    
    if (objActivitySection.objActivityCompleteSession.allObjects.count > 0)
        [arrActTemp addObjectsFromArray:[objActivitySection.objActivityCompleteSession.allObjects filteredArrayUsingPredicate:predicate]];
    
    NSArray *arrDataActivity = [arrActTemp sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
    
    return arrDataActivity;
}

-(void)stopAllreadyPlayedVideo
{
    if (cellActivityVideo)  // Pause Allready played video.....
    {
        if ([cellActivityVideo isKindOfClass:[ActivityAddPhotoCell class]])
        {
            // NSLog(@"View hidden = = %d",cellActivityVideo.viewVideo.hidden);
            if (!cellActivityVideo.viewVideo.hidden && cellActivityVideo && cellActivityVideo.tag == 100)
            {
                NSLog(@"stop Video here ==========>>>>>  ");
                [cellActivityVideo StopVideoPlaying];
                cellActivityVideo = nil;
            }
        }
    }
}
-(void)playVideoAutomatically
{
        NSArray *arrVisibleCell = [tblUser visibleCells];
        NSMutableArray *arrVideoCells = [NSMutableArray new];
        for (int c = 0; arrVisibleCell.count > c; c++)
        {
            UITableViewCell *cell = arrVisibleCell[c];
            if ([cell isKindOfClass:[ActivityAddPhotoCell class]])
            {
                // Get Video cell here....
                ActivityAddPhotoCell *cellVideo = (ActivityAddPhotoCell *)cell;
                if (!cellVideo.viewVideo.hidden)
                    [arrVideoCells addObject:cellVideo];
            }
        }
        
        for(ActivityAddPhotoCell *cell in arrVideoCells)
        {
            CGRect intersect = CGRectIntersection(tblUser.frame, [tblUser convertRect:[tblUser rectForRowAtIndexPath:[tblUser indexPathForCell:cell]] toView:tblUser.superview]);
            
            //play here..
            if (![cell isEqual:cellActivityVideo] && !cell.viewVideo.hidden && intersect.size.height > CViewHeight(cell)*0.6) // only if 60% of the cell is visible
            {
                [self stopAllreadyPlayedVideo];
                cellActivityVideo = cell;
                cellActivityVideo.tag = 100;
                [cellActivityVideo setUpForVideoPlayer];
            }
        }
}


#pragma mark - UITableView Delegate and Datasource
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrActivityList.count +1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return 1;
    else
    {
        TblActivityDates *objActivitySection = arrActivityList[section-1];
        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        return arrActivity.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return nil;
    
    TblActivityDates *objActivitySection = arrActivityList[section -1];
    
    ProfileHeaderView *viewHeader;
    viewHeader = [ProfileHeaderView viewFromXib];
    viewHeader.lblMonth.text = [NSString stringWithFormat:@"%@ -",objActivitySection.show_date];
    
    viewHeader.lblRunCount.text = objActivitySection.total_month_run.intValue == 1 ? [NSString stringWithFormat:@"%@ RUN",objActivitySection.total_month_run] : [NSString stringWithFormat:@"%@ RUNS",objActivitySection.total_month_run];
    
    viewHeader.lblMileCount.text = [[appDelegate getDistanceInWithSelectedFormate:objActivitySection.total_month_distance] uppercaseString];
    
    viewHeader.viewHeaderTopLine.hidden = !(section == 1);
    
    if ([objActivitySection.activity_filter_type isEqual:@0])
    {
        viewHeader.viewLineAll.hidden = NO;
        viewHeader.viewLineRuns.hidden = YES;
        viewHeader.btnRuns.selected = NO;
        viewHeader.btnAll.selected = YES;
    }
    else
    {
        viewHeader.viewLineAll.hidden = YES;
        viewHeader.viewLineRuns.hidden = NO;
        viewHeader.btnAll.selected = NO;
        viewHeader.btnRuns.selected = YES;
    }
    
    [viewHeader.btnAll touchUpInsideClicked:^{
        viewHeader.viewLineAll.hidden = NO;
        viewHeader.viewLineRuns.hidden = YES;
        viewHeader.btnRuns.selected = NO;
        viewHeader.btnAll.selected = YES;
        
        objActivitySection.activity_filter_type = @0;
        [[Store sharedInstance].mainManagedObjectContext save];
        [self stopAllreadyPlayedVideo];
        [tblUser reloadData];
        
        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        if(arrActivity.count > 0)
            [tblUser scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }];
    
    [viewHeader.btnRuns touchUpInsideClicked:^{
        viewHeader.viewLineAll.hidden = YES;
        viewHeader.viewLineRuns.hidden = NO;
        viewHeader.btnAll.selected = NO;
        viewHeader.btnRuns.selected = YES;
        
        [self stopAllreadyPlayedVideo];
        
        objActivitySection.activity_filter_type = @1;
        [[Store sharedInstance].mainManagedObjectContext save];
        [tblUser reloadData];
        
        NSArray *arrActivity = [self getActivityArray:objActivitySection];
        if(arrActivity.count > 0)
            [tblUser scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }];
    
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 0;
    
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        NSString *strIdentifier = @"ProfileHeaderCell";
        ProfileHeaderCell *cell = [tblUser dequeueReusableCellWithIdentifier:strIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSString *strName = [NSString stringWithFormat:@"%@ %@",appDelegate.loginUser.first_name,appDelegate.loginUser.last_name];
        cell.lblUserBio.text = [NSString stringWithFormat:@"%@ %@",strName, appDelegate.loginUser.about_us];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblUserBio.attributedText];
        [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
        [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblUserBio.font.pointSize) range: NSMakeRange(0,strName.length)];
        cell.lblUserBio.attributedText  = text;
        
        cell.lblWebsiteLink.text = appDelegate.loginUser.link;
        if(![cell.lblWebsiteLink.text isBlankValidationPassed])
            [cell.lblTotalEarnedPoints setConstraintConstant:-20 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        else
            [cell.lblTotalEarnedPoints setConstraintConstant:18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
        [cell.btnWebsiteLink touchUpInsideClicked:^{
            if([cell.lblWebsiteLink.text isBlankValidationPassed])
            {
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:cell.lblWebsiteLink.text]])
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:cell.lblWebsiteLink.text] options:@{} completionHandler:nil];
                else
                {
                    [self customAlertViewWithOneButton:CMessageSorry Message:@"Link Incorrect." ButtonText:@"Ok" Animation:YES completed:nil];
                }
            }
        }];
        
        cell.btnFollow.hidden = cell.viewFollow.hidden = YES;
        
        cell.btnBack.hidden = !self.isBackButton;
        
        cell.viewNotificationBadge.hidden = appDelegate.loginUser.notification_count.intValue < 1;
        cell.lblNotificationCount.text = appDelegate.loginUser.notification_count;
        
        cell.lblFirstName.text = appDelegate.loginUser.first_name.uppercaseString;
        cell.lblLastName.text = appDelegate.loginUser.last_name.uppercaseString;
        cell.lblUserName.text = [NSString stringWithFormat:@"@%@",appDelegate.loginUser.user_name];
        cell.lblTotalEarnedPoints.text = [NSString stringWithFormat:@"%d",appDelegate.loginUser.reward_point.intValue];

        cell.imgVerified.hidden = !appDelegate.loginUser.is_verified.boolValue;
        
        if (appDelegate.loginUser.is_default_image.boolValue)
            cell.imgUser.image = CPlaceholderUserImage;
        else
            [cell.imgUser setImageWithURL:[NSURL URLWithString:appDelegate.loginUser.picture] placeholderImage:CPlaceholderUserImage options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        NSMutableArray *arrRunData = [NSMutableArray new];
        NSMutableDictionary *dicRun = [NSMutableDictionary new];
        [dicRun addObject:@"RUNS" forKey:@"title"];
        [dicRun addObject:appDelegate.loginUser.run_total_runs forKey:@"count"];
        [arrRunData addObject:dicRun];
        
        NSMutableDictionary *dicDis = [NSMutableDictionary new];
        
        if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
        {
            [dicDis addObject:@"DISTANCE (KM)" forKey:@"title"];
            [dicDis addObject:[NSString stringWithFormat:@"%.2f",appDelegate.loginUser.run_total_miles.floatValue/1000] forKey:@"count"];
        }
        else
        {
            [dicDis addObject:@"DISTANCE (MILES)" forKey:@"title"];
            [dicDis addObject:[NSString stringWithFormat:@"%.2f",appDelegate.loginUser.run_total_miles.floatValue * 0.000621371] forKey:@"count"];
        }
        
        [arrRunData addObject:dicDis];
        
        NSMutableDictionary *dicCal = [NSMutableDictionary new];
        [dicCal addObject:[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"EST KCAL. BURNED" : @"EST CAL. BURNED" forKey:@"title"];
        
        [dicCal addObject:[NSString stringWithFormat:@"%d",appDelegate.loginUser.run_total_calories.intValue] forKey:@"count"];
        [arrRunData addObject:dicCal];
        
        NSMutableDictionary *dicBPM = [NSMutableDictionary new];
        [dicBPM addObject:@"AVG BPM" forKey:@"title"];
        [dicBPM addObject:[NSString stringWithFormat:@"%d",appDelegate.loginUser.run_total_BPM.intValue] forKey:@"count"];
        [arrRunData addObject:dicBPM];

        
        NSInteger rowCount =  arrRunData.count/3;
        if (arrRunData.count%3 > 0)
            rowCount += 1;
        
        cell.arrList = arrRunData;
        cell.pageControl.numberOfPages = rowCount;
        cell.pageControl.currentPage = 0;
        [cell.collview reloadData];
        
        [cell.btnPopup touchUpInsideClicked:^{
            SettingViewController *objSet = [[SettingViewController alloc] init];
            [self.navigationController pushViewController:objSet animated:YES];
        }];
        
        [cell.btnBack touchUpInsideClicked:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [cell.btnUserZoom touchUpInsideClicked:^{
            
            shouldShowLoader = NO;
            
            if (appDelegate.loginUser.is_default_image.boolValue)
                return;
            
            ProfileHeaderCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
            imageInfo.placeholderImage = cell.imgUser.image;
            imageInfo.image = cell.imgUser.image;
            imageInfo.referenceRect = cell.imgUser.frame;
            imageInfo.referenceView = cell.imgUser.superview;
            imageInfo.referenceContentMode = cell.imgUser.contentMode;
            imageInfo.referenceCornerRadius = cell.imgUser.layer.cornerRadius;
            
            JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo mode:JTSImageViewControllerMode_Image backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
            [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
        }];
        
        [cell.btnNotification touchUpInsideClicked:^{
            NotificationViewController *objNot = [[NotificationViewController alloc] init];
            [self.navigationController pushViewController:objNot animated:YES];
        }];
        
        return cell;
    }
    else
    {
        TblActivityDates *objActivitySection = arrActivityList[indexPath.section -1];
        NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
        NSManagedObject *objCoreData = (NSManagedObject *)arrDataActivity[indexPath.row];
        
        if ([objCoreData isKindOfClass:[TblActivityPhoto class]])
        {
            // Photo Activity
            TblActivityPhoto *objActPhoto = (TblActivityPhoto *)objCoreData;
            ActivityAddPhotoCell *cell = [tblUser dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configurePhotoActivityCell:cell Activity:objActPhoto];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivityVideo class]])
        {
            // Video Activity
            TblActivityVideo *objActVideo = (TblActivityVideo *)objCoreData;
            ActivityAddPhotoCell *cell = [tblUser dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureVideoActivityCell:cell Activity:objActVideo];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivitySyncSession class]])
        {
            // Sync Activity
            TblActivitySyncSession *objActSync = (TblActivitySyncSession *)objCoreData;
            SyncedActivityCell *cell = [tblUser dequeueReusableCellWithIdentifier:NSStringFromClass([SyncedActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureSyncedActivityCell:cell Activity:objActSync];
            return cell;
            
        }
        else if ([objCoreData isKindOfClass:[TblActivityRunSession class]])
        {
            // Ongoing Run Activity
            TblActivityRunSession *objActRun = (TblActivityRunSession *)objCoreData;
            StartActivityCell *cell = [tblUser dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureStartSessionActivityCell:cell Activity:objActRun];
            return cell;
        }
        else if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
        {
            // Complete Run Activity
            TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
            StartActivityCell *cell = [tblUser dequeueReusableCellWithIdentifier:NSStringFromClass([StartActivityCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self configureCompleteSessionActivityCell:cell Activity:objActCom];
            return cell;
        }
        
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
        return;
    
    TblActivityDates *objActivitySection = arrActivityList[indexPath.section -1];
    NSArray *arrDataActivity = [self getActivityArray:objActivitySection];
    NSManagedObject *objCoreData = (NSManagedObject *)arrDataActivity[indexPath.row];
    
    if ([objCoreData isKindOfClass:[TblActivityCompleteSession class]])
    {
        TblActivityCompleteSession *objActCom = (TblActivityCompleteSession *)objCoreData;
        ActivityDetailViewController *objAct = [[ActivityDetailViewController alloc] init];
        objAct.strActivityId = objActCom.activity_id;
        objAct.strResultSessionId = objActCom.session_id;
        [self.navigationController pushViewController:objAct animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // Stop video here when cell invisible from table view...
    if ([cell isEqual:cellActivityVideo])
        [self stopAllreadyPlayedVideo];
}

#pragma mark - Configure ActivityAddPhotoCell
#pragma mark -

- (void)configurePhotoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityPhoto *)objActivityPhoto
{
    cell.lblActivityText.hidden = cell.imgUser.hidden = cell.viewUser.hidden = YES;
    cell.lblTime.textAlignment = NSTextAlignmentRight;
    [cell.lblTime setConstraintConstant:-18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = YES;
    cell.imgAddedPhoto.hidden = NO;
    
    [cell setImageHeightWidth:objActivityPhoto.img_width Height:objActivityPhoto.img_hieght PhotoActivity:YES];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityPhoto.activity_post_time];
    
    [cell.lblPhotoCaption hideByHeight:![objActivityPhoto.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityPhoto.caption;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityPhoto.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgAddedPhoto setImageWithURL:[NSURL URLWithString:objActivityPhoto.act_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityPhoto.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivityPhoto.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityPhoto.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityPhoto.user_name;
    NSString *strSession = objActivityPhoto.session_name;
    NSString *strActText = objActivityPhoto.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityPhoto.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
    }
    
    cell.imgAddedPhoto.userInteractionEnabled = YES;
    [cell.doubleTapeGesture addTarget:self action:@selector(imgDoubleTapGesture:)];
    cell.doubleTapeGesture.numberOfTapsRequired = 2;
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityPhoto.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityPhoto.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityPhoto.is_like = @YES;
        }
        
        objActivityPhoto.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
        [appDelegate likeActivity:objActivityPhoto.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityPhoto.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityPhoto.activity_id ActType:objActivityPhoto.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityPhoto.activity_id completed:^(id responseObject, NSError *error)
        {
            if (responseObject && !error)
            {
                [TblActivityPhoto deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityPhoto.activity_id]];
                [[Store sharedInstance].mainManagedObjectContext save];
                [self fetchAllActivityFromLocal];
            }
        }];
    }];
}

-(void)imgDoubleTapGesture:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:tblUser];
    NSIndexPath *tappedIndexPath = [tblUser indexPathForRowAtPoint:point];
    
    ActivityAddPhotoCell *cell = [tblUser cellForRowAtIndexPath:tappedIndexPath];
    
    cell.imgHeartLikeZoom.hidden = NO;
    cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 8.0, 8.0);} completion:^(BOOL finished){ }];
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);} completion:^(BOOL finished) {cell.imgHeartLikeZoom.hidden = YES;}];
    
    if (!cell.btnLike.selected)
        [cell.btnLike sendActionsForControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Configure ActivityAddVideoCell
#pragma mark -
- (void)configureVideoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityVideo *)objActivityVideo
{
    cell.lblActivityText.hidden = cell.imgUser.hidden = cell.viewUser.hidden = YES;
    cell.lblTime.textAlignment = NSTextAlignmentRight;
    
    if (Is_iPhone_6)
        [cell.lblTime setConstraintConstant:-25 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    else
        [cell.lblTime setConstraintConstant:-18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];

    cell.objVideoActivity = objActivityVideo;
    [cell setImageHeightWidth:objActivityVideo.video_width Height:objActivityVideo.video_height PhotoActivity:NO];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityVideo.activity_post_time];
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = NO;
    cell.imgAddedPhoto.hidden = YES;
    
    [cell.lblPhotoCaption hideByHeight:![objActivityVideo.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityVideo.caption;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityVideo.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgVThumb setImageWithURL:[NSURL URLWithString:objActivityVideo.thumb_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.btnLike.selected = objActivityVideo.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityVideo.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityVideo.total_comments.integerValue < 3];
    
    NSString *strName = objActivityVideo.user_name;
    NSString *strSession = objActivityVideo.session_name;
    NSString *strActText = objActivityVideo.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityVideo.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.btnShare touchUpInsideClicked:^{
       [appDelegate openSharingOption:nil VideoURL:objActivityVideo.video_url Video:YES TabBar:YES];
        
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityVideo.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityVideo.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityVideo.is_like = @YES;
        }
        
        objActivityVideo.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
        [appDelegate likeActivity:objActivityVideo.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityVideo.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityVideo.activity_id ActType:objActivityVideo.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnVideo touchUpInsideClicked:^{
        
        if (cell.btnVideo.selected)
        {
            // Already Played
            [cell StopVideoPlaying];
            cellActivityVideo = nil;
        }
        else
        {
            if (cellActivityVideo)
                [self stopAllreadyPlayedVideo];
            
            cellActivityVideo = cell;
            cellActivityVideo.tag = 100;
            [cell setUpForVideoPlayer];
        }
    }];
    
    cell.btnMuteUnmute.selected = objActivityVideo.video_mute.boolValue;
    [cell.btnMuteUnmute touchUpInsideClicked:^{
        cell.btnMuteUnmute.selected = !cell.btnMuteUnmute.selected;
        cell.playerView.player.muted = cell.btnMuteUnmute.selected;
        objActivityVideo.video_mute = cell.btnMuteUnmute.selected ? @YES : @NO;
        [[Store sharedInstance].mainManagedObjectContext save];
    }];
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityVideo.activity_id completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 [TblActivityVideo deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityVideo.activity_id]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self fetchAllActivityFromLocal];
             }
         }];
    }];
    
}

#pragma mark - Configure StartActivityCell
#pragma mark -

- (void)configureStartSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityRunSession *)objActivityRun
{
    cell.lblActivityText.hidden = cell.imgUser.hidden = cell.viewUser.hidden = YES;
    cell.lblTime.textAlignment = NSTextAlignmentRight;
    [cell.lblTime setConstraintConstant:-18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];

    
    [cell drawMapPath:objActivityRun.runner_path.mutableCopy];
    
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityRun.activity_post_time];
    //    cell.mapView = nil ;
    
    cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityRun.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityRun.total_comments.integerValue < 3];
    
    cell.lblSessionComTime.text = objActivityRun.session_complete_time;
    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityRun.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityRun.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";
    
    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityRun.position,objActivityRun.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityRun.total_runner];
    cell.lblCity.text = objActivityRun.city;
    cell.lblSubLocality.text = objActivityRun.sublocality;
    
    cell.btnLike.selected = objActivityRun.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityRun.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    NSString *strName = objActivityRun.user_name;
    NSString *strSession = objActivityRun.session_name;
    NSString *strActText = objActivityRun.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivityRun.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text =cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];

    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityRun.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityRun.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityRun.is_like = @YES;
        }
        
        objActivityRun.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityRun.total_likes];
        [appDelegate likeActivity:objActivityRun.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityRun.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityRun.activity_id ActType:objActivityRun.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

#pragma mark - Configure Complete Session Activity
#pragma mark -

- (void)configureCompleteSessionActivityCell:(StartActivityCell *)cell Activity:(TblActivityCompleteSession *)objActivityCom
{
    //cell.mapView = nil ;
    
    cell.lblActivityText.hidden = cell.imgUser.hidden = cell.viewUser.hidden = YES;
    cell.lblTime.textAlignment = NSTextAlignmentRight;
    [cell.lblTime setConstraintConstant:-18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];


    [cell drawMapPath:objActivityCom.runner_path.mutableCopy];
    
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityCom.activity_post_time];
    
    if (objActivityCom.position.integerValue < 4)
    {
        cell.imgPosition.hidden = cell.viewPosition.hidden = NO;
        cell.lblWinPositon.text = objActivityCom.position;
    }
    else
        cell.imgPosition.hidden = cell.viewPosition.hidden = YES;
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityCom.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityCom.total_comments.integerValue < 3];
    
    cell.lblSessionComTime.text = objActivityCom.session_complete_time;

    cell.lblAverageSpeed.text = [appDelegate convertSpeedInWithSelectedFormate:objActivityCom.total_distance.floatValue overTime:[NSString stringWithFormat:@"%@",objActivityCom.session_complete_time]];
    cell.lblSpeedTag.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi";

    cell.lblPosition.text = [NSString stringWithFormat:@"%@/%@",objActivityCom.position,objActivityCom.total_runner];
    cell.lblTotalRunner.text = [NSString stringWithFormat:@"%@ RUNNERS",objActivityCom.total_runner];
    cell.lblCity.text = objActivityCom.city;
    cell.lblSubLocality.text = objActivityCom.sublocality;
    
    cell.btnLike.selected = objActivityCom.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivityCom.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    
    
    NSString *strName = objActivityCom.user_name;
    NSString *strSession = objActivityCom.session_name;
    NSString *strActText = objActivityCom.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityCom.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityCom.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityCom.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityCom.is_like = @YES;
        }
        
        objActivityCom.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityCom.total_likes];
        [appDelegate likeActivity:objActivityCom.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityCom.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityCom.activity_id ActType:objActivityCom.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}

#pragma mark - Configure SyncedActivityCell
#pragma mark -

- (void)configureSyncedActivityCell:(SyncedActivityCell *)cell Activity:(TblActivitySyncSession *)objActivitySync
{
    cell.lblActivityText.hidden = cell.imgUser.hidden = cell.viewUser.hidden = YES;
    cell.lblTime.textAlignment = NSTextAlignmentRight;
    [cell.lblTime setConstraintConstant:-18 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];

    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivitySync.activity_post_time];
    
    cell.objSessionTime = objActivitySync;
    [cell showSyncUser:objActivitySync.sync_user.mutableCopy];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivitySync.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivitySync.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivitySync.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    cell.imgComment.image = objActivitySync.is_commented.boolValue ? [UIImage imageNamed:@"ic_act_selected_comment"] :[UIImage imageNamed:@"ic_act_comment"] ;

    cell.lblSessionName.text = objActivitySync.session_name;
    
    NSString *strName = objActivitySync.user_name ? objActivitySync.user_name : @"";
    NSString *strSession = objActivitySync.session_name ? objActivitySync.session_name : @"";
    NSString *strActText = objActivitySync.activity_text ? objActivitySync.activity_text : @"";
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    
    NSArray *arrComments = objActivitySync.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            [cell.btnViewAllComments setConstraintConstant:8 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        UIGraphicsBeginImageContextWithOptions(cell.viewScreenShot.bounds.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:YES];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivitySync.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivitySync.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivitySync.is_like = @YES;
        }
        
        objActivitySync.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivitySync.total_likes];
        [appDelegate likeActivity:objActivitySync.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivitySync.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivitySync.activity_id ActType:objActivitySync.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell startTimer];
    cell.configureSesionSyncTimeOver = ^(BOOL isOver)
    {
        if (objActivitySync)
        {
            [TblActivitySyncSession deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivitySync.activity_id]];
            [self fetchAllActivityFromLocal];
        }
    };
}


#pragma mark - NSMutableAttributedString
#pragma mark -

-(NSMutableAttributedString *)getAttributeStirng:(NSString *)strStartText andStartRange:(NSInteger)startRange Lable:(UILabel *)lbl strAttString:(NSMutableAttributedString *)text
{
    //    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lbl.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(startRange, strStartText.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(lbl.font.pointSize) range: NSMakeRange(0,strStartText.length)];
    
    return text;
}

#pragma mark - Scroll View Delegate
#pragma mark -

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt
{
    [self playVideoAutomatically];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self playVideoAutomatically];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if ([aScrollView isEqual:tblUser] && Is_iPhone_X)
        cnTblTopSpace.constant = tblUser.contentOffset.y > 588 ? 30 : 0;
}


#pragma mark - Helper Function
#pragma mark -

-(void)moveOnCommentScreen:(NSString *)strActID ActType:(NSString *)strActType
{
    CommentsViewController *objCo = [[CommentsViewController alloc] init];
    objCo.strActivityId = strActID;
    objCo.strActivityType = strActType;
    [self.navigationController pushViewController:objCo animated:YES];
}

@end
