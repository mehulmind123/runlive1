//
//  MIImageCropperViewController.h
//  Engage
//
//  Created by mac-0007 on 05/12/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MIImageCropperDelegate
- (void)finish:(UIImage *)image didCancel:(BOOL)cancel;
@end

@interface MIImageCropperViewController : SuperViewController

@property(nonatomic, strong) id<MIImageCropperDelegate> delegate;

- (id)initWithImage:(UIImage *)originalImage withDelegate:(id<MIImageCropperDelegate>)delegate ImageSizeValidation:(BOOL)isSizeValidation withViewController:(UIViewController *)viewController;

@end
