//
//  RunSummaryViewController.m
//  RunLive
//
//  Created by mac-0006 on 17/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "RunSummaryViewController.h"
#import "OtherUserProfileViewController.h"
#import "TagpeopleCell.h"
#import "MyActivityResultCollectionViewCell.h"
#import "RunScoreBoardCell.h"
#import "RunSummaryCommentCell.h"
#import "RunSummaryResultCell.h"
#import "UserRunSummaryViewController.h"
#import "RunScoreBoardBlueTeamCell.h"
#import "RunnerPositionCell.h"
#import "SessionFeedbackViewController.h"

#define CTeamRed @"team_red"
#define CTeamBlue @"team_blue"
#define CTeamSolo @"team_solo"
#define CTeamType @"team_type"
#define CUserPosition @"user_position"
#define CUserPositionEmptyCell @"user_position_emptyCell"
#define CUserPositionEmptyCellType @"user_position_emptyCell_type"

#define TRANSFORM_RUNNERPOSITION_UP_CELL_VALUE CGAffineTransformMakeScale(1.1, 1.1)
#define TRANSFORM_RUNNERPOSITION_DOWN_CELL_VALUE CGAffineTransformMakeScale(0.9, .9)

#define CWinUserCheering @"crowdcheer"

@interface RunSummaryViewController ()
{
    int iOffset;
    UIRefreshControl *refreshControl;
    BOOL isRefershinData;
    
    int hours, minutes, seconds;
    double sessionEndTimeStamp;
    
    NSArray *arryMyData;
    NSArray *aryPeoples;
    NSArray *filterdPeople; // make ordered predicate
    
    //Reena
    NSMutableArray *arrCommentList,*arrUserList,*arrRunnerPosition,*arrWinRunner;
    BOOL isRunTypeSolo,isTeamWinRed,isVoiceText;
    NSTimer *timer;
    
    NSDictionary *dicSessionDetail;
    AVAudioPlayer *avAudioPlayer;
    int totalCommentCount;
}
@end

@implementation RunSummaryViewController

#pragma mark - Life cycle
#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    scrollRunView.hidden = viewComment.hidden = YES;

    appDelegate.configureRefreshSessionData = ^(RefreshSessionWithUpdate refreshSessionWithUpdate)
    {
        if ([[appDelegate getTopMostViewController] isKindOfClass:[RunSummaryViewController class]] || [[appDelegate getTopMostViewController] isKindOfClass:[SessionFeedbackViewController class]])
        {
            if (refreshSessionWithUpdate == DeleteSession)
                [self btnBackClicked:nil];
            else if (refreshSessionWithUpdate == ResultSession)
            {
                isVoiceText = YES;
                [self getResultFromServer:YES];
            }
            else if (refreshSessionWithUpdate == CommentOnSession)
            {
                iOffset = 0;
                [self GetCommentDataFromServer:YES];
            }
        }
    };

    [self Initialize];
    
    // If enter from backgorund....
    [UIApplication applicationWillEnterForeground:^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[RunSummaryViewController class]])
        {
            isVoiceText = NO;
            [self getResultFromServer:NO];
        }
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    isVoiceText = NO;
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    viewScrollIndicator.layer.cornerRadius = viewScrollIndicator.frame.size.width/2;
    ViewScrollSide.layer.cornerRadius = ViewScrollSide.frame.size.width/2;
    viewScrollIndicator.clipsToBounds = ViewScrollSide.clipsToBounds = true;
    
    [collActivity reloadData];
}


#pragma mark - Initialize Method
-(void)Initialize
{
    superTable = tblComments;
    
    viewLineComment.hidden = YES;
    viewComment.hidden = YES;
    arrCommentList = [NSMutableArray new];
    arrUserList = [NSMutableArray new];
    arrRunnerPosition = [NSMutableArray new];
    arrWinRunner = [NSMutableArray new];
    
    [clRunnerPositionl registerNib:[UINib nibWithNibName:@"RunnerPositionCell" bundle:nil] forCellWithReuseIdentifier:@"RunnerPositionCell"];
    
    [collActivity registerNib:[UINib nibWithNibName:@"MyActivityResultCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MyActivityResultCollectionViewCell"];
    [collActivity registerNib:[UINib nibWithNibName:@"RunSummaryResultCell" bundle:nil] forCellWithReuseIdentifier:@"RunSummaryResultCell"];
    [tblScore registerNib:[UINib nibWithNibName:@"RunScoreBoardCell" bundle:nil] forCellReuseIdentifier:@"RunScoreBoardCell"];
    [tblScore registerNib:[UINib nibWithNibName:@"RunScoreBoardBlueTeamCell" bundle:nil] forCellReuseIdentifier:@"RunScoreBoardBlueTeamCell"];
    [tblComments registerNib:[UINib nibWithNibName:@"RunSummaryCommentCell" bundle:nil] forCellReuseIdentifier:@"RunSummaryCommentCell"];
    [tblPeopleList registerNib:[UINib nibWithNibName:@"TagpeopleCell" bundle:nil] forCellReuseIdentifier:@"TagpeopleCell"];
    
    
    tblComments.estimatedRowHeight = 30.0;
    tblPeopleList.estimatedRowHeight = 60.0;
    tblPeopleList.rowHeight = 65.00;
    
    filterdPeople = @[];
    
    txtComment.placeholder = @"Type your comment here..";
    [txtComment setFont:[UIFont fontWithName:@"Gilroy-Regular" size:17]];
    txtComment.delegate = self;
    
    [self GetCommentDataFromServer:YES];
    [self getResultFromServer:YES];
    
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblComments addSubview:refreshControl];
 
    if (self.isNotificationComment)
        [self btnTabbarClicked:btnComments];
    else
        [self btnTabbarClicked:btnRun];
}



#pragma mark - Pull Refresh
-(void)handleRefresh
{
    NSLog(@"handleRefresh ========== >>>>>>>>>>>>>> ");
    
//    if (iOffset == 0)
//    {
//        [refreshControl endRefreshing];
//        return;
//    }
    
    isRefershinData = YES;
    [self GetCommentDataFromServer:NO];
}

#pragma mark - Comment Related API Functions
-(void)GetCommentDataFromServer:(BOOL)shouldShowLoader
{
    if (!self.strResultSessionId)
        return;
    
    [[APIRequest request] getCommentListOfSession:self.strResultSessionId Offset:[NSString stringWithFormat:@"%d",iOffset] completed:^(id responseObject, NSError *error)
     {
         [refreshControl endRefreshing];
         
         if (responseObject && !error)
         {
             if (iOffset == 0)
                 [arrCommentList removeAllObjects];
             
             NSArray *arrPreviousComment = arrCommentList.mutableCopy;
             
             [arrCommentList removeAllObjects];
             [arrCommentList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
             [arrCommentList addObjectsFromArray:arrPreviousComment];
             
             iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
             lblTotalComment.text = [NSString stringWithFormat:@"(%@)",[[responseObject valueForKey:@"extra"] stringValueForJSON:@"total"]];
             totalCommentCount = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"total"].intValue;
             
             [tblComments reloadData];
             
             if (isRefershinData)
                 [tblComments setContentOffset:CGPointMake(0, 0) animated:NO];
             else
             {
                 if (arrCommentList.count > 0)
                 {
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                         NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
                         [tblComments scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
                     });
                 }
             }
             isRefershinData = NO;
         }
     }];
}

-(void)addCommentOnSession
{
    if (!self.strResultSessionId)
        return;
    
    NSString * aString = txtComment.text;
    NSMutableArray *arrMentionUsers = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd])
    {
        NSString *substring = nil;
        [scanner scanString:@"@" intoString:nil]; // Scan the # character
        
        if([scanner scanUpToString:@" " intoString:&substring])
            [arrMentionUsers addObject:substring];
        
        [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before next #
    }
    
    NSString *strCommentText = txtComment.text;
    txtComment.text = @"";
    
    [[APIRequest request] addCommentOnSession:strCommentText SessionId:self.strResultSessionId UserName:arrMentionUsers completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             
             iOffset = 0;
             [self GetCommentDataFromServer:NO];
             return ;
             
//             [arrCommentList addObject:[responseObject valueForKey:CJsonData]];
//             [tblComments reloadData];
//             
//             lblTotalComment.text = [NSString stringWithFormat:@"(%@)",[[responseObject valueForKey:@"extra"] stringValueForJSON:@"total"]];
//
//             if (arrCommentList.count > 0)
//             {
//                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                     NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
//                     [tblComments scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
//                 });
//             }
         }
     }];
}

#pragma mark - Result Related API Functions

-(void)getResultFromServer:(BOOL)shouldShowLoader
{
    if (!self.strResultSessionId)
        return;

    if (shouldShowLoader)
        [GiFHUD showWithOverlay];
    
    [[APIRequest request] runningResultOfSession:self.strResultSessionId completed:^(id responseObject, NSError *error)
    {
        [GiFHUD dismiss];
        
        scrollRunView.hidden = viewComment.hidden = NO;
        
        if (self.isNotificationComment)
        {
            [self btnTabbarClicked:btnComments];
            self.isNotificationComment = NO;
        }
        else
        {
            [self btnTabbarClicked:btnRun];
        }

        
        if (responseObject && !error)
        {
            // Do your stuff here
            
            NSDictionary *dicData = [responseObject valueForKey:CJsonData];
            dicSessionDetail = dicData;
            
            if ([dicData objectForKey:@"name"])
                lblRunSessionName.text = [dicData stringValueForJSON:@"name"];
            
            if ([[dicData stringValueForJSON:@"run_type"] isEqualToString:@"solo"])
            {
                isRunTypeSolo = YES;
                [self setupForSoloSession:dicData];
            }
            else
            {
                isRunTypeSolo = NO;
                [self setupForTeamSession:dicData];
            }
        }
        
    }];
}


#pragma mark - Team Session
-(void)setupForTeamSession:(NSDictionary *)dicResult
{
    [arrWinRunner removeAllObjects];
    
    NSDictionary *dicInvition = [dicResult valueForKey:@"invitation"];
    
    isRunTypeSolo = NO;
    
    lblWinner.hidden = lblWinnerTeamName.hidden = NO;
    lblSoloScoreboard.hidden = YES;
    
    [viewTeamTitle hideByHeight:NO];
    [lblScoreboard hideByHeight:NO];
    
    [arrUserList removeAllObjects];
    NSMutableArray *arrTempUsers = [[NSMutableArray alloc] initWithArray:[dicInvition valueForKey:@"team_red"]];
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamRed forKey:CTeamType];
        [arrUserList addObject:dicData];
    }
    
   // store Win Red team player for position.....
    if ([[dicResult stringValueForJSON:@"win_team"] isEqualToString:@"team_red"])
    {
        lblWinnerTeamName.text = @"TEAM RED";
        lblWinnerTeamName.textColor = CRGB(226, 86, 97);
        isTeamWinRed = YES;
        NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
        [arrWinRunner addObjectsFromArray:sortedArray];
    }
    
    [arrTempUsers removeAllObjects];
    [arrTempUsers addObjectsFromArray:[dicInvition valueForKey:@"team_blue"]];

    NSMutableArray *arrWinPlayerTemp = [NSMutableArray new];
    
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamBlue forKey:CTeamType];
        [arrUserList addObject:dicData];
        [arrWinPlayerTemp addObject:dicData];
    }
    
    // store Win Blue team player for position.....
    if ([[dicResult stringValueForJSON:@"win_team"] isEqualToString:@"team_blue"])
    {
        lblWinnerTeamName.text = @"TEAM BLUE";
        lblWinnerTeamName.textColor = CRGB(61, 205, 252);
        isTeamWinRed = NO;
        [arrWinRunner removeAllObjects];
        NSArray *sortedArray = [arrWinPlayerTemp sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
        [arrWinRunner addObjectsFromArray:sortedArray];
    }
    
    NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
    
    [arrUserList removeAllObjects];
    [arrUserList addObjectsFromArray:sortedArray];

    [tblScore reloadData];
    cnTblHeight.constant = tblScore.contentSize.height;
    
    [self showResultPendingRelatedStuff:dicResult];
}

#pragma mark - Solo Session
-(void)setupForSoloSession:(NSDictionary *)dicResult
{
    NSDictionary *dicInvition = [dicResult valueForKey:@"invitation"];
    
    isRunTypeSolo = YES;
    
    lblWinner.hidden = lblWinnerTeamName.hidden = YES;
    lblSoloScoreboard.hidden = NO;
    
    [viewTeamTitle hideByHeight:YES];
    [lblScoreboard hideByHeight:YES];
    
    [arrUserList removeAllObjects];
    NSMutableArray *arrTempUsers = [[NSMutableArray alloc] initWithArray:[dicInvition valueForKey:@"solo"]];
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamSolo forKey:CTeamType];
        [arrUserList addObject:dicData];
    }

    NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
    
    [arrWinRunner removeAllObjects];
    [arrWinRunner addObjectsFromArray:sortedArray];
    
    [arrUserList removeAllObjects];
    [arrUserList addObjectsFromArray:sortedArray];
    
    [tblScore reloadData];
    cnTblHeight.constant = tblScore.contentSize.height;
    
    [self showResultPendingRelatedStuff:dicResult];
}

-(void)showResultPendingRelatedStuff:(NSDictionary *)dicResult
{
    // If result pending
    viewResultPending.hidden = YES;
    [activityIndicatorResultPending stopAnimating];
    
    [lblTime hideByHeight:YES];
    [lblTimeLeft hideByHeight:YES];


    // Find Login user...
    NSArray *arrLogin = [arrUserList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    NSDictionary *dicLogInUser;
    if (arrLogin.count > 0)
        dicLogInUser = arrLogin[0];
    
    NSDictionary *dicLoginComp = [dicLogInUser valueForKey:@"completed"];

    // Open feedback screen....
    if ([dicLoginComp objectForKey:@"feedback_status"] && [[dicLoginComp numberForJson:@"feedback_status"] isEqual:@0])
        [self showFeedbackAlert:NO];
    
    
    float mapTopIsset = 0;
    float mapBottomIsset = 0;
    
    if ([[dicResult numberForJson:@"session_complete"] isEqual:@0])
    {
        cnTblHeight.constant = 0;
        viewResultPending.hidden = NO;
        
        [activityIndicatorResultPending startAnimating];
        NSDate *date = [self convertDateFromString:[dicResult stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
        sessionEndTimeStamp = [date timeIntervalSince1970];
        float sessionComTime = ([dicResult stringValueForJSON:@"distance"].floatValue/1000 *7.5)*60;
        sessionEndTimeStamp = sessionEndTimeStamp + sessionComTime;
        
        if ([dicLoginComp doubleForKey:@"total_distance"] < [dicResult doubleForKey:@"distance"])
        {
            lblPosition.text = self.isRunnerTimerOut ? @"Please wait while the race results are loaded." : @"You have forfeited the race";
            lblPoint.text = [NSString stringWithFormat:@"%ld",(long)[dicLoginComp stringValueForJSON:@"points"].integerValue];
            lblPointstxt.transform = CGAffineTransformMakeRotation( -(90 * M_PI ) / 180 );
        }
        else
        {
//            lblPosition.text = @"You have forfeited the race";
            lblPosition.text = [NSString stringWithFormat:@"YOU HAVE GAINED %@ PLACE AND EARNED",[self appendStringInPosition:[dicLoginComp stringValueForJSON:@"rank"]]];
            lblPosition.attributedText = [self AttributedStringwith:[self appendStringInPosition:[dicLoginComp stringValueForJSON:@"rank"]]];
            lblPoint.text = [NSString stringWithFormat:@"%ld",(long)[dicLoginComp stringValueForJSON:@"points"].integerValue];
            lblPointstxt.transform = CGAffineTransformMakeRotation( -(90 * M_PI ) / 180 );
        }
        
        if (Is_iPhone_5)
        {
            mapTopIsset = 260;
            mapBottomIsset = 60;
        }
        else if (Is_iPhone_6_PLUS)
        {
            mapTopIsset = 230;
            mapBottomIsset = 30;
        }
        else
        {
            mapTopIsset = 230;
            mapBottomIsset = 30;
        }
        
        [self stopRunTimer];
        
        if (sessionEndTimeStamp > 0)
        {
            mapTopIsset = mapTopIsset+80;
            [lblTime hideByHeight:NO];
            [lblTimeLeft hideByHeight:NO];
            [self setTimerForSession];
        }
        
    }
    else
    {
        lblPosition.text = [NSString stringWithFormat:@"YOU HAVE GAINED %@ PLACE AND EARNED",[self appendStringInPosition:[dicLoginComp stringValueForJSON:@"rank"]]];
        lblPosition.attributedText = [self AttributedStringwith:[self appendStringInPosition:[dicLoginComp stringValueForJSON:@"rank"]]];
        lblPoint.text = [NSString stringWithFormat:@"%ld",(long)[dicLoginComp stringValueForJSON:@"points"].integerValue];
        lblPointstxt.transform = CGAffineTransformMakeRotation( -(90 * M_PI ) / 180 );
        
        if (!_isFromNotification && !_isNotificationComment)
            [self playCheeringSoundForWinRunner:[dicLoginComp stringValueForJSON:@"rank"]];
        

        if (Is_iPhone_5)
        {
            mapTopIsset = 290;
            mapBottomIsset = 60;
        }
        else if (Is_iPhone_6_PLUS)
        {
            mapTopIsset = 200;
            mapBottomIsset = 30;
        }
        else
        {
            mapTopIsset = 260;
            mapBottomIsset = 30;
        }
    }
    
    [self setupForMapView:UIEdgeInsetsMake(mapTopIsset, 30, mapBottomIsset, 30) userData:dicLogInUser] ;
    
    NSMutableArray *arrUserDataTemp = [NSMutableArray new];
    [arrUserDataTemp addObject:@{@"image":@"time",@"result":[dicLoginComp stringValueForJSON:@"total_time"],@"quantities":@"TIME",@"unit":@""}];

    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
        [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicLoginComp stringValueForJSON:@"total_distance"].floatValue/1000],@"quantities":@"DISTANCE",@"unit":@"km"}];
    else
        [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicLoginComp stringValueForJSON:@"total_distance"].floatValue * 0.000621371],@"quantities":@"DISTANCE",@"unit":@"miles"}];

    [arrUserDataTemp addObject:@{@"image":@"cal",@"result":[appDelegate convertCaloriesWithSelectedFormate:[dicLoginComp stringValueForJSON:@"total_calories"]] ,@"quantities":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"EST KCALS" : @"EST CALORIES",@"unit":@"cal"}];

    
    [arrUserDataTemp addObject:@{@"image":@"speed",@"result":[appDelegate convertSpeedInWithSelectedFormate:[dicLoginComp stringValueForJSON:@"total_distance"].floatValue overTime:[NSString stringWithFormat:@"%@",[dicLoginComp stringValueForJSON:@"total_time"]]],@"quantities":@"PACE",@"unit":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi"}];
    
    if ([dicLoginComp stringValueForJSON:@"average_bpm"].floatValue > 0)
        [arrUserDataTemp addObject:@{@"image":@"heartbeat",@"result":[NSString stringWithFormat:@"%.2f",[dicLoginComp stringValueForJSON:@"average_bpm"].floatValue],@"quantities":@"AVG. BPM",@"unit":@""}];
    
    arryMyData = arrUserDataTemp.mutableCopy;
    [collActivity reloadData];

    // Store People for comment suggestion...
    NSMutableArray *arrUserSuggestion = [[NSMutableArray alloc] initWithArray:arrUserList];
    if ([arrUserSuggestion containsObject:dicLogInUser])
        [arrUserSuggestion removeObject:dicLogInUser];
    
    aryPeoples = arrUserSuggestion;
    [tblPeopleList reloadData];
    
    [self getRunnerForTopPosition];
}

-(void)getRunnerForTopPosition
{
    [arrRunnerPosition removeAllObjects];
    
    if (arrWinRunner.count < 2)
    {
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
    }
    else if (arrWinRunner.count < 3)
    {
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:arrWinRunner[1]];
    }
    else
    {
        [arrRunnerPosition addObject:arrWinRunner[2]];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:arrWinRunner[1]];
    }
        
    [clRunnerPositionl reloadData];
}

#pragma mark - Timer Related functions

-(void)stopRunTimer
{
    [timer invalidate];
    timer = nil;
}

-(void)setTimerForSession
{
    [self stopRunTimer];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;
    
    if (difTimeStamp > 0)
    {
        seconds = difTimeStamp;
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateCounterResult) userInfo:nil repeats:YES];
        minutes = seconds / 60 % 60;
        
        int tempSecond = seconds;
        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        hours = tempSecond / (60 * 60);
        tempSecond = tempSecond - hours * (60 * 60);
        minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;
    }
    else
        lblTime.text=@"00:00:00";
    
}

-(void)updateCounterResult
{
    seconds--;
    if (seconds >= 0)
    {
        int tempSecond = seconds;
        
        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        hours = tempSecond / (60 * 60);
        tempSecond = tempSecond - hours * (60 * 60);
        minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;
        
        lblTime.text=[NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,tempSecond];
    }
    else
    {
        [self stopRunTimer];
        lblTime.text = @"00:00:00";
    }
}


#pragma mark - Map View Related Functions
-(void)setupForMapView:(UIEdgeInsets)inset userData:(NSDictionary *)dicUser
{
    NSArray *arrLatLong = [dicUser valueForKey:@"running"];
    
    [mapView clearMGLMapView];
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (arrLatLong.count > 0)
    {
        [mapView MGLMapViewCenterCoordinate:arrLatLong];
        [mapView drawPolylineByUsingCoordinates:arrLatLong];
        
        
        if (arrLatLong.count > 1)
            [self addMarkerOnMapView:arrLatLong IsStartPosition:YES];   // If Start and End position available then....
        else
            [self addMarkerOnMapView:arrLatLong IsStartPosition:NO]; // Only end position availalbe
        
        [mapView setPathCenterInMapView:arrLatLong edgeInset:UIEdgeInsetsMake(inset.top, inset.left, inset.bottom, inset.right)];
    }
    
}

-(void)addMarkerOnMapView:(NSArray *)arrMarker IsStartPosition:(BOOL)isStartPos
{
    NSDictionary *dicMarkerLast = arrMarker.lastObject;
    [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerLast floatForKey:@"latitude"], [dicMarkerLast floatForKey:@"longitude"]) Type:EndPossitionMarker UserInfo:dicMarkerLast];
    
    if (isStartPos)
    {
        NSDictionary *dicMarkerFirst = arrMarker.firstObject;
        [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerFirst floatForKey:@"latitude"], [dicMarkerFirst floatForKey:@"longitude"]) Type:StartPossitionMarker UserInfo:dicMarkerFirst];
    }
}

#pragma mark - Cheering Sound Functions
-(void)playCheeringSoundForWinRunner:(NSString *)rank
{
    // If result is declare then only play sound....
    if (!isVoiceText)
        return;
    
    if (rank.intValue == 1)
    {
        // Play Sound here....
        NSString *soundPath = soundPath = [[NSBundle mainBundle] pathForResource:CWinUserCheering ofType:@"wav"];
        
        NSError *setCategoryErr = nil;
        NSError *activationErr  = nil;
        
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&setCategoryErr];
        [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundPath];
        avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        avAudioPlayer.numberOfLoops = 0; //Infinite
        avAudioPlayer.volume = 1;
        [avAudioPlayer play];
    }
}


#pragma mark - Helper function
#pragma mark

-(NSAttributedString *)AttributedStringwith:(NSString *)position
{
    NSString *strFullText = lblPosition.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",strFullText]];
    NSRange rangeOfStr3 = [strFullText rangeOfString:position];
    [attributedString addAttribute:NSForegroundColorAttributeName value:CRGB(119, 218, 249) range:rangeOfStr3];
    [attributedString addAttribute:NSFontAttributeName value:CFontSolidoCondensedBold(29) range:rangeOfStr3];
    
    return attributedString;
}

-(NSString *)appendStringInPosition:(NSString *)strPosition
{
    NSString *strRank;
    
    if ([strPosition isEqualToString:@"1"])
        strRank = @"ST";
    else if ([strPosition isEqualToString:@"2"])
        strRank = @"ND";
    else if ([strPosition isEqualToString:@"3"])
        strRank = @"RD";
    else
        strRank = @"TH";
    
    strRank = [NSString stringWithFormat:@"%@%@",strPosition,strRank];
    
    return strRank;
}

-(NSAttributedString *)setAttributedString:(NSString *)position
{
    NSString *strRank;
    
    if ([position isEqualToString:@"1"])
        strRank = @"ST";
    else if ([position isEqualToString:@"2"])
        strRank = @"ND";
    else if ([position isEqualToString:@"3"])
        strRank = @"RD";
    else
        strRank = @"TH";
    
    NSString *strFullText =[NSString stringWithFormat:@"%@%@",position,strRank];
    
    NSMutableAttributedString *attributeText = [[NSMutableAttributedString alloc]initWithString:strFullText];
    
    [attributeText addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, position.length)];
    [attributeText addAttribute: NSFontAttributeName value:CFontSolidoCondensedBold(22) range: NSMakeRange(0,position.length)];
    
    [attributeText addAttribute:NSForegroundColorAttributeName value:CRGB(122, 126, 145) range:NSMakeRange(position.length, strRank.length)];
    [attributeText addAttribute: NSFontAttributeName value:CFontSolidoCondensedBold(15) range: NSMakeRange(position.length, strRank.length)];
    
    return attributeText;
}

#pragma mark - UICollectionView Delegate and Datasource
#pragma mark -


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clRunnerPositionl])
        return arrRunnerPosition.count;
    
    return arryMyData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clRunnerPositionl])
    {
        NSString *strIdentifier = @"RunnerPositionCell";
        RunnerPositionCell *cell = [clRunnerPositionl dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        NSDictionary *dicData = arrRunnerPosition[indexPath.item];
        
        if ([dicData objectForKey:CUserPositionEmptyCellType])
        {
            //
            cell.contentView.hidden = YES;
        }
        else
        {
            cell.contentView.hidden = NO;
            NSDictionary *dicComp = [dicData valueForKey:@"completed"];

            cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"140" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            
            cell.lblPosition.attributedText = [self setAttributedString:[dicComp stringValueForJSON:@"rank"]];
            
            cell.transform = [[dicComp numberForJson:@"rank"] isEqual:@1] ? TRANSFORM_RUNNERPOSITION_UP_CELL_VALUE : TRANSFORM_RUNNERPOSITION_DOWN_CELL_VALUE;
            
            if (isRunTypeSolo)
            {
                switch ([dicComp numberForJson:@"rank"].intValue)
                {
                    case 1:
                        cell.viewBorder.layer.borderColor = CRGB(249, 206, 0).CGColor;
                        break;
                    case 2:
                        cell.viewBorder.layer.borderColor = CRGB(190, 207, 210).CGColor;
                        break;
                    case 3:
                        cell.viewBorder.layer.borderColor = CRGB(244, 133, 40).CGColor;
                        break;
                    default:
                        break;
                }
            }
            else
                cell.viewBorder.layer.borderColor = isTeamWinRed ? CRGB(226, 86, 97).CGColor : CRGB(61, 205, 252).CGColor;
        }
        
        return cell;
    }
    else
    {
        if (arryMyData.count == 4)
        {
            NSString *strIdentifier = @"RunSummaryResultCell";
            RunSummaryResultCell *cell = [collActivity dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCureentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCureentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCureentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCureentValue valueForKey:@"unit"];
            
            cell.imgSide.image = [UIImage imageNamed:[dicCureentValue valueForKey:@"image"]];
            
            return cell;
        }
        else
        {
            NSString *strIdentifier = @"MyActivityResultCollectionViewCell";
            MyActivityResultCollectionViewCell *cell = [collActivity dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCureentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCureentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCureentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCureentValue valueForKey:@"unit"];
            
            cell.imgTop.image = [UIImage imageNamed:[dicCureentValue valueForKey:@"image"]];
            
            return cell;
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clRunnerPositionl])
        return CGSizeMake(CScreenWidth/arrRunnerPosition.count , CGRectGetHeight(collectionView.frame));
    else
    {
        if (arryMyData.count == 4)
            return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
        else
        {
            if (indexPath.row < 2)
                return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
            
            return CGSizeMake(CScreenWidth/3-.01 , CGRectGetHeight(collectionView.frame)/2);
        }
    }
}

#pragma mark - UITableView Delegate and Datasource
#pragma mark -

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblComments])
        return UITableViewAutomaticDimension;
    
        return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblComments])
        return UITableViewAutomaticDimension;
    
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:tblScore])
        return arrUserList.count;
    else if ([tableView isEqual:tblPeopleList])
        return filterdPeople.count;
    else
        return arrCommentList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblScore])
    {
        NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];
        NSDictionary *dicComp = [dicData valueForKey:@"completed"];

        if (isRunTypeSolo)
        {
            NSString *strIdentifier = @"RunScoreBoardCell";
            RunScoreBoardCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
            
            //--------- For Solo
            cell.imgNext.hidden = NO;
            cell.viewRedcircle.hidden = YES;
            cell.viewHorizontalLine.hidden = YES;
            cell.imgNext.hidden = NO;
            
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
            cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
            
            [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];

            cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
            cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
            cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
            cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
            
            return cell;
        }
        else
        {
            if ([[dicData stringValueForJSON:CTeamType] isEqualToString:CTeamRed])
            {
                NSString *strIdentifier = @"RunScoreBoardCell";
                RunScoreBoardCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
                
                cell.imgNext.hidden = YES;
                
                cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
                cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
                
                [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];
                
                cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
                cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
                cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
                cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
                return cell;
            }
            else
            {
                NSString *strIdentifier = @"RunScoreBoardBlueTeamCell";
                RunScoreBoardBlueTeamCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
                
                cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
                cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
                [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];
                
                cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
                cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
                cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
                cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
                
                return cell;
            }
        }
    }
    else if ([tableView isEqual:tblPeopleList])
    {
        static NSString *simpleTableIdentifier = @"TagpeopleCell";
        TagpeopleCell *cell = (TagpeopleCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = filterdPeople[indexPath.row];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        
        cell.lblContent.text = [dicData stringValueForJSON:@"user_name"];
        [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        return cell;

    }
    else
    {
        NSString *strIdentifier = @"RunSummaryCommentCell";
        RunSummaryCommentCell *cell = [tblComments dequeueReusableCellWithIdentifier:strIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
        cell.lblComment.text = [dicData stringValueForJSON:@"text"];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:[dicData stringValueForJSON:@"createdAt"]];
        
        [cell.lblComment addHashTagAndUserHandler:@"" Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        __weak typeof (cell) weakCell = cell;
        [cell.lblUserName addHashTagAndUserHandler:cell.lblUserName.text Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:weakCell.lblUserName.text UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.btnUser touchUpInsideClicked:^{
            [appDelegate moveOnProfileScreen:[dicData stringValueForJSON:@"user_name"] UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        return cell;
    }
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:tblPeopleList])
    {
        NSString *str = [txtComment.text substringToIndex:[txtComment.text rangeOfString:@"@" options:NSBackwardsSearch].location + 1];
        txtComment.text = [NSString stringWithFormat:@"%@%@ ",str,((TagpeopleCell *)[tblPeopleList cellForRowAtIndexPath:indexPath]).lblContent.text];
        filterdPeople = @[];
        [self updateTablePeople];
    }
    else if([tableView isEqual:tblScore])
    {
        NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];
        UserRunSummaryViewController *objOther = [[UserRunSummaryViewController alloc] initWithNibName:@"UserRunSummaryViewController" bundle:nil];
        objOther.arrRunners = arrUserList;
        objOther.strSelectedUserId = [dicData stringValueForJSON:@"user_id"];
        objOther.dicSession = dicSessionDetail;
        [self.navigationController pushViewController:objOther animated:true];
    }
    else
    {
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:tblComments])
    {
        // Return YES if you want the specified item to be editable.
        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
        
        if ([[dicData stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id] || ([[dicData stringValueForJSON:@"owner_id"] isEqualToString:appDelegate.loginUser.user_id] && ![[dicData stringValueForJSON:@"type"] isEqualToString:@"live"]))
        {
            return YES;
        }
        
        return NO;
    }
    
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self customAlertViewWithTwoButton:@"" Message:@"Are you sure you want to delete this comment?" ButtonFirstText:@"Yes" ButtonSecondText:@"NO" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
                [[APIRequest request] deleteSessionComment:[dicData stringValueForJSON:@"_id"] completed:^(id responseObject, NSError *error)
                {
                    if (responseObject && !error)
                    {
                        [arrCommentList removeObjectAtIndex:indexPath.row];
                        [tblComments reloadData];
                        
                        totalCommentCount--;
                        lblTotalComment.text = [NSString stringWithFormat:@"(%d)",totalCommentCount];
                    }
                }];
            }
        }];
    }
}

#pragma mark - ScrollView Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if ([scrollView isEqual:tblPeopleList])
    {
        [self updateScrollIndicator];
    }
}


#pragma mark - HPGrowingView Delegate
#pragma mark

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    txtCommentHeight.constant = height;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText =  [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if (finalText.length > 0)
    {
        btnSend.enabled = [finalText isBlankValidationPassed];
        NSRange rgy = [finalText rangeOfString:@"@" options:NSBackwardsSearch];
        
        if (rgy.location == finalText.length-1)
        {
            filterdPeople = @[];//[aryPeoples copy];
        }
        else if (rgy.length > 0)
        {
            NSString *AfterAd = [finalText substringFromIndex:[finalText rangeOfString:@"@" options:NSBackwardsSearch].location+1];
            
            if (AfterAd.length > 0)
            {
                if ([[AfterAd substringToIndex:1] isEqualToString:@" "])
                {
                    filterdPeople = @[];
                }
                else
                {
                    filterdPeople = [aryPeoples filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(user_name contains[cd] %@)", AfterAd]];
                }
            }
            else
            {
                
            }
        }
        else
        {
            filterdPeople = @[];
        }
    }
    else
    {
        btnSend.enabled = false;
        filterdPeople = @[];
    }
    
    [self updateTablePeople];
    return true;
}

-(void)updateTablePeople
{
    [tblPeopleList flashScrollIndicators];
    
    [tblPeopleList reloadData];
    tblpeopleHeight.constant = tblPeopleList.contentSize.height;
    [self.view setNeedsUpdateConstraints];
    [self updateScrollIndicator];
    [UIView animateWithDuration:0.25f animations:^{
        [tblPeopleList layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self updateScrollIndicator];
    }];
    
}

-(void)updateScrollIndicator
{
    if (tblPeopleList.contentSize.height != 0)
    {
        scrollIndheight.constant = (tblPeopleList.frame.size.height / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height ;
        scrollIndY.constant = (tblPeopleList.contentOffset.y / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height;
    }
}


#pragma mark - Feedback session
-(void)showFeedbackAlert:(BOOL)isGhostMode
{
    if (![[appDelegate getTopMostViewController] isKindOfClass:[SessionFeedbackViewController class]])
    {
        SessionFeedbackViewController *objFeedback = [SessionFeedbackViewController new];
        objFeedback.strSessionId = self.strResultSessionId;
        objFeedback.strSessionType = isGhostMode ? @"2" :@"1";
        [self.navigationController pushViewController:objFeedback animated:NO];
    }
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self stopRunTimer];
    
    [mapView clearMGLMapView];
   // [mapView stopRendering] ;
    [mapView removeFromSuperview] ;
    mapView = nil ;
 
    if (self.isFromNotification)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        appDelegate.window.rootViewController = appDelegate.objTabBarController;
        [self.navigationController popToRootViewControllerAnimated:NO];
        [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
    }
}

- (IBAction)btnShareClicked:(id)sender
{
    UIImage *screenShot = [viewRun screenshot];
    [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:NO];
}

- (IBAction)btnTabbarClicked:(UIButton *)sender
{
    btnRun.selected = btnComments.selected = NO;
    viewLineRun.hidden = viewLineComment.hidden = btnShare.hidden = YES;
    
    if (sender.tag == 0)
    {
        btnRun.selected = YES;
        viewLineRun.hidden = NO;
        scrollRunView.hidden = btnShare.hidden = NO;
        viewComment.hidden = YES;
    }
    else
    {
        btnComments.selected = YES;
        viewLineComment.hidden = viewComment.hidden = NO;
        scrollRunView.hidden = YES;
    }
    
}

- (IBAction)btnSendClicked:(id)sender
{
    [appDelegate hideKeyboard];
    
    if ([txtComment.text length] != 0)
        [self addCommentOnSession];

}

@end
