//
//  TblSessionOtherUser+CoreDataProperties.m
//  RunLive
//
//  Created by mac-00012 on 1/2/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionOtherUser+CoreDataProperties.h"

@implementation TblSessionOtherUser (CoreDataProperties)

+ (NSFetchRequest<TblSessionOtherUser *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblSessionOtherUser"];
}

@dynamic other_image_url;
@dynamic user_id;
@dynamic country;
@dynamic fb_id;
@dynamic first_name;
@dynamic last_name;
@dynamic user_name;
@dynamic sessionList;

@end
