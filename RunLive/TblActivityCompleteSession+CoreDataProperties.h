//
//  TblActivityCompleteSession+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivityCompleteSession+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblActivityCompleteSession (CoreDataProperties)

+ (NSFetchRequest<TblActivityCompleteSession *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *activity_id;
@property (nullable, nonatomic, copy) NSString *activity_main_type;
@property (nullable, nonatomic, copy) NSString *activity_post_time;
@property (nullable, nonatomic, copy) NSString *activity_text;
@property (nullable, nonatomic, retain) NSObject *capture;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, retain) NSObject *comments;
@property (nullable, nonatomic, copy) NSString *image_map;
@property (nullable, nonatomic, copy) NSNumber *is_like;
@property (nullable, nonatomic, copy) NSNumber *isMapLoaded;
@property (nullable, nonatomic, copy) NSString *position;
@property (nullable, nonatomic, retain) NSObject *runner_path;
@property (nullable, nonatomic, copy) NSString *session_complete_time;
@property (nullable, nonatomic, copy) NSString *session_id;
@property (nullable, nonatomic, copy) NSString *session_name;
@property (nullable, nonatomic, copy) NSString *sublocality;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, copy) NSString *total_burn_cal;
@property (nullable, nonatomic, copy) NSString *total_comments;
@property (nullable, nonatomic, copy) NSString *total_distance;
@property (nullable, nonatomic, copy) NSString *total_likes;
@property (nullable, nonatomic, copy) NSString *total_runner;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_image;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, copy) NSNumber *is_commented;
@property (nullable, nonatomic, retain) TblActivityDates *objActivityDate;

@end

NS_ASSUME_NONNULL_END
