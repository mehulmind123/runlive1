//
//  ResultRunnerCell.m
//  RunLive
//
//  Created by mac-0005 on 20/09/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "ResultRunnerCell.h"

@implementation ResultRunnerCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;
    self.viewUserColor.layer.cornerRadius = CGRectGetHeight(self.viewUserColor.frame)/2;
}

@end
