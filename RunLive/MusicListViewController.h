//
//  MusicListViewController.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListCell.h"

@interface MusicListViewController : SuperViewController <UITableViewDataSource, UITableViewDelegate,AVAudioPlayerDelegate>
{
    IBOutlet UITableView *tblList;
    IBOutlet UIButton *btnPlayList;
    IBOutlet UIView *viewBottom;
    IBOutlet UIImageView *imgLogo;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}
@property(atomic,assign) BOOL isApple;

@property(nonatomic, assign) NSUInteger lFollowerCount;
@property(nonatomic,strong) NSArray *arrAppleMusic;


- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnSelectPlayListClicked:(id)sender;


@property NSURL *imgURL;
@property NSURL *URI;
@property NSString *name;
@property NSString *countFollower;
@property(strong,nonatomic) MPMediaItemCollection *appleMusicCollection;



@end
