//
//  RunnerPositionCell.h
//  RunLive
//
//  Created by mac-00012 on 6/29/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunnerPositionCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIImageView *imgUser,*imgVerifiedUser;
@property(weak,nonatomic) IBOutlet UIView *viewBorder;
@property(weak,nonatomic) IBOutlet UILabel *lblPosition,*lblName;


@end
