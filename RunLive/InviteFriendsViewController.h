//
//  InviteFriendsViewController.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookCell.h"
#import "InviteFriendHeaderView.h"
#import "InviteFriendCell.h"
#import "InviteFriendFooterView.h"

@interface InviteFriendsViewController : SuperViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UICollectionView *clFriend;
    IBOutlet UIView *viewBottom;
    IBOutlet UIButton *btnInviteFrd;
    IBOutlet UIImageView *imgBottomShadow,*imgInviteSent;
    
}

@property(atomic,assign) BOOL isFromCerateSession;
@property(atomic,assign) BOOL isSoloSession;
@property(strong,nonatomic) NSMutableArray *arrAllreadyInvitedUser;
@property(strong,nonatomic) NSString *strSessionId;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnInviteFriendsClicked:(id)sender;

@end
