//
//  SuperNavigationController.m
//  ChamRDV
//
//  Created by mac-0001 on 12/17/13.
//  Copyright (c) 2013 MIND INVENTORY. All rights reserved.
//

#import "SuperNavigationController.h"

@interface SuperNavigationController ()

@end

@implementation SuperNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, -5, 0); // or (0, 0, -10.0, 0)
    UIImage *alignedImage = [[UIImage imageNamed:@"ic_navback"] imageWithAlignmentRectInsets:insets];
    [[UINavigationBar appearance] setBackIndicatorImage:alignedImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"ic_navback"]];
    
    [[UINavigationBar appearance] setBarTintColor:CNavigationBarColor];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, 0) forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    

    // To show status bar style..
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_navbarImage"] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.translucent = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
