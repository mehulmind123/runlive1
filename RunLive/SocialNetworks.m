//
//  SocialNetworks.m
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "SocialNetworks.h"

#import "FacebookClass.h"
//#import "MITwitter.h"
//#import "Google.h"
//#import "LinkedIn.h"
//#import "Tumblr.h"
//#import "PinterestClass.h"
//#import "Yahoo.h"
//#import "Flickr.h"
//#import "Dropbox.h"
//#import "Instagram.h"
//#import "Whatsapp.h"


//#import <Master/NSObject+NewProperty.h>


@implementation SocialNetworks

@synthesize facebookAppId,facebookCallBackURL,facebookRedirectURLForShareDialog,facebookReadPermissions;
@synthesize googlePlusClientId,googlePlusCallBackURL;
@synthesize pinterestClientId,pinterestCallBackURL;
@synthesize linkedInAPIKey,linkedInSecretKey,linkedInState,linkedInCallBackURL;
@synthesize twitterConsumerKey,twitterConsumerSecret;
@synthesize instagramClientID,instagramClientSecret,instagramCallBackURL,instagramScopes;

// To Check Authentication of Framework
+(void)load
{
    [super load];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        
//        if (![UIApplication keychainForKey:@"Application"])
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oooops...." message:@"\nUNAUTHORISED ACCESS" delegate:self cancelButtonTitle:@"EXIT" otherButtonTitles:nil];
//            [alert show:^(NSInteger buttonIndex) {
//                exit(0);
//            }];
//        }
//    });
}


+ (instancetype)sharedInstance
{
    static SocialNetworks *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[SocialNetworks alloc] init];
    });
    
    return _sharedInstance;
}

-(BOOL)isAunthenticatedForNetwork:(SocialNetworkOptions)network
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            return [[FacebookClass sharedInstance] isAunthenticated];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//            return [[MITwitter sharedInstance] isAunthenticated];
//        }
//            break;
//        case SocialNetworkInstagram:
//        {
//            return [[Instagram sharedInstance] isAunthenticated];
//        }
//            break;
        default:
            return NO;
            NSAssert(nil, @"Not Available or Not Implemented Yet");
            break;
    }

    return NO;
}

-(void)logoutFromAllSocialNetworks
{
    [self logoutFromNetwork:SocialNetworkFacebook];
    [self logoutFromNetwork:SocialNetworkTwitter];
    [self logoutFromNetwork:SocialNetworkGoogle];
    [self logoutFromNetwork:SocialNetworkLinkdIn];
    [self logoutFromNetwork:SocialNetworkTumblr];
    [self logoutFromNetwork:SocialNetworkPinterest];
    [self logoutFromNetwork:SocialNetworkYahoo];
    [self logoutFromNetwork:SocialNetworkFlickr];
    [self logoutFromNetwork:SocialNetworkDropbox];
    [self logoutFromNetwork:SocialNetworkInstagram];
    [self logoutFromNetwork:SocialNetworkWhatsApp];
}

-(void)logoutFromNetwork:(SocialNetworkOptions)network
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            [FacebookClass logout];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//            [MITwitter logout];
//        }
//            break;
//        case SocialNetworkInstagram:
//        {
//            [Instagram logout];
//        }
//            break;
//        case SocialNetworkLinkdIn:
//        {
//            [LinkedIn logout];
//        }
            break;
        default:
            break;
    }
}

-(void)loginWithSocialNetwork:(SocialNetworkOptions)network success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            [[FacebookClass sharedInstance] LoadFacebookUserDetails:^(NSDictionary *result, NSError *error, BOOL status)
             {
                 if (error)
                     failure(error);
                 else if(result)
                     success(result);
             }];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//                [[MITwitter sharedInstance] loginUserWithBasicProfle:^(NSDictionary *result, NSError *error, BOOL status) {
//                    if (error)
//                        failure(error);
//                    else if(result)
//                        success(result);
//                }];
//        }
//            break;
        case SocialNetworkGoogle:
        {
//                [[Google sharedInstance] LoginWithGooglePlus:^(NSDictionary *result, NSError *error, BOOL status)
//            {
//                    if (error)
//                        failure(error);
//                    else if(result)
//                        success(result);
//                }];
        }
            break;
//        case SocialNetworkInstagram:
//        {
//            [[Instagram sharedInstance] LoginWithInstagram:^(NSDictionary *result, NSError *error, BOOL status) {
//                if (error)
//                    failure(error);
//                else if(result)
//                    success(result);
//            }];
//        }
//            break;
//        case SocialNetworkLinkdIn:
//        {
//            [[LinkedIn sharedInstance] fetchUserProfile:^(NSDictionary *responseObject, NSError *error)
//            {
//                if (error)
//                    failure(error);
//                else if(responseObject)
//                    success(responseObject);
//            }];
//        }
//            break;
//        case SocialNetworkWhatsApp:
//        {
//            NSLog(@"Sorry, This feature is not available.");
//        }
//            break;
        default:
            NSAssert(nil, @"Not Implemented Yet");
            break;
    }
}


-(void)postOnSocialNetwork:(SocialNetworkOptions)network withData:(PostParameters *)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            [[FacebookClass sharedInstance] shareOnFacebook:data success:success failure:failure];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//            [[MITwitter sharedInstance] postOnTwitter:data success:success failure:failure];
//        }
            break;
        case SocialNetworkGoogle:
        {
//            [[Google sharedInstance] postOnGoogle:data success:success failure:failure];
        }
            break;
//        case SocialNetworkPinterest:
//        {
//            [[PinterestClass sharedInstance] shareOnPinterest:data completed:^(BOOL status) {
//                    if (status)
//                        success(@{@"status":@"success"});
//                    else
//                        failure([NSError errorWithDomain:@"pinterest" code:112123 userInfo:nil]);
//            }];
//        }
//            break;
//        case SocialNetworkWhatsApp:
//        {
//            if (data.comment)
//            {
//                if ([[Whatsapp sharedInstance] shareText:data.comment])
//                    success(@{@"status":@"success"});
//                else
//                    failure([NSError errorWithDomain:@"Whatsapp" code:112123 userInfo:nil]);
//            }
//            else if(data.websiteURL)
//            {
//                if ([[Whatsapp sharedInstance] shareFile:data.websiteURL.absoluteString])
//                    success(@{@"status":@"success"});
//                else
//                    failure([NSError errorWithDomain:@"Whatsapp" code:112123 userInfo:nil]);
//            }
//        }
//            break;
        default:
            NSAssert(nil, @"Not Implemented Yet");
            break;
    }
}

-(void)postOnSocialNetworkThroughAPIOnly:(SocialNetworkOptions)network withData:(PostParameters *)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            NSLog(@"Please make sure you have reviewed publish_action permissions by Facebook.");
            
                [[FacebookClass sharedInstance] postOnFacebook:data completion:^(NSError *error, BOOL status) {
                    if (status)
                        success(@{@"status":@"success"});
                    else
                        failure(error);
                }];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//            [[MITwitter sharedInstance] postThroughAPIOnly:data success:success failure:failure];
//        }
//            break;
        default:
            NSAssert(nil, @"Not Available or Not Implemented Yet");
            break;
    }
}

-(void)fetchFriendsForSocialNetwork:(SocialNetworkOptions)network success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    switch (network)
    {
        case SocialNetworkFacebook:
        {
            [[FacebookClass sharedInstance] fetchFriends:success failure:failure];
        }
            break;
//        case SocialNetworkTwitter:
//        {
//            [[MITwitter sharedInstance] fetchfollowing:success failure:failure];
//        }
//            break;
//        case SocialNetworkInstagram:
//        {
//                [[Instagram sharedInstance] fetchfollowing:success failure:failure];
//        }
//            break;
        default:
            NSAssert(nil, @"Not Available or Not Implemented Yet");
            break;
    }
}

#pragma mark - Appdelegate Handlers

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    BOOL status = YES;
    
    if (NSClassFromString(@"FBSDKApplicationDelegate"))     // Facebook SDK 4.0
        [[NSClassFromString(@"FBSDKApplicationDelegate") sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    return status;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[UIApplication sharedApplication] booleanForKey:@"CustomWebView"] && [[[[NSBundle  mainBundle] infoDictionary] valueForKey:@"NSPrincipalClass"] isEqualToString:@"Application"])
    {
        [[UIApplication topMostController] dismissViewControllerAnimated:YES completion:nil];
        [[UIApplication sharedApplication] setBoolean:NO forKey:@"CustomWebView"];
    }
    
    
    if ([FacebookClass handleOpenURL:url sourceApplication:sourceApplication annotation:annotation])
    {
        return YES;
    }
    //    else if ([LinkedIn shouldOpenUrl:url])
    //    {
    //        return [LinkedIn handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    //    }
//    else if ([Google shouldOpenUrl:url])
//    {
//        return [Google handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
//    }
    //    else if ([Instagram shouldOpenUrl:url])
    //    {
    //        return [Instagram handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    //    }
    //    else if ([PinterestClass shouldOpenUrl:url])
    //    {
    //        return [PinterestClass handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    //    }
    else
    {
        NSLog(@"Callback Return State = %@",url);
        return YES;
    }
}

@end
