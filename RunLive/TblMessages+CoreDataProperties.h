//
//  TblMessages+CoreDataProperties.h
//  RunLive
//
//  Created by mac-00012 on 7/31/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblMessages+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblMessages (CoreDataProperties)

+ (NSFetchRequest<TblMessages *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *channelId;
@property (nullable, nonatomic, copy) NSString *imgheight;
@property (nullable, nonatomic, copy) NSString *imgname;
@property (nullable, nonatomic, copy) NSString *imgurl;
@property (nullable, nonatomic, copy) NSString *imgwidth;
@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSString *msgdate;
@property (nullable, nonatomic, copy) NSString *msgdisplaytime;
@property (nullable, nonatomic, copy) NSString *msgtext;
@property (nullable, nonatomic, copy) NSString *msgtype;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, copy) NSString *userid;
@property (nullable, nonatomic, copy) NSString *userimgurl;
@property (nullable, nonatomic, copy) NSNumber *is_video;

@end

NS_ASSUME_NONNULL_END
