//
//  PlayListCell.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgAlbum;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayPause;
@property (strong, nonatomic) IBOutlet UIImageView *imgEquilizer;

@end
