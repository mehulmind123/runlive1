//
//  UINavigationController+Extension.m
//  MI API Example
//
//  Created by mac-0001 on 25/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UINavigationController+Extension.h"

@implementation UINavigationController (Extension)

+(UINavigationController *)navigationController
{
   return [[self alloc] init];
}

+(UINavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController
{
    return [[self alloc] initWithRootViewController:rootViewController];
}




-(void)viewDidLoad
{


#if NavigationBarShouldHideBackBarButtonText
    self.navigationBar.translucent = NavigationBarTranscluent;
#endif

    
#if NavigationBarShouldHideBackBarButtonText
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
#endif

}


-(UIViewController *)viewControllerOfClass:(Class)class
{
    NSArray *viewControllers = [[self.navigationController.viewControllers reverseObjectEnumerator] allObjects];
    
    for (int i = 0 ; i < [viewControllers count] ; i++)
    {
        if ([[viewControllers objectAtIndex:i] isKindOfClass:class])
        {
            return [viewControllers objectAtIndex:i];
        }
    }
    
    return nil;
}

-(BOOL)popToViewControllerOfClass:(Class)class animated:(BOOL)animated
{
    if ([self viewControllerOfClass:class])
    {
        [self.navigationController popToViewController:[self viewControllerOfClass:class] animated:animated];
        return YES;
    }

    return NO;
}


@end
