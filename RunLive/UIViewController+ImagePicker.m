//
//  UIViewController+ImagePicker.m
//  Corpository
//
//  Created by mac-00014 on 12/28/16.
//  Copyright © 2016 Ishwar-00014. All rights reserved.
//

#import "UIViewController+ImagePicker.h"

static NSString *const COMPLETIONHANDLER = @"COMPLETIONHANDLER";
static NSString *const ALLOWEDITING = @"ISALLOWEDITING";
static NSString *const IMAGEFULLDETAIL = @"IMAGEFULLDETIALS";

@implementation UIViewController (ImagePicker)

- (void)presentImagePickerSource:(CompletionHandler)handler AllowEditing:(BOOL)isEditing
{
    [self setBoolean:isEditing forKey:ALLOWEDITING];
    
    //.....ActionSheet.....
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoCameraOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        [self presentPhotoCamera:handler AllowEditing:isEditing];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoLibraryOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        [self presentPhotoLibray:handler AllowEditing:isEditing];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)presentPhotoLibray:(CompletionHandler)handler AllowEditing:(BOOL)isEditing
{
    [self setBoolean:NO forKey:IMAGEFULLDETAIL];
    [self setObject:handler forKey:COMPLETIONHANDLER];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = isEditing;
        imagePicker.navigationBar.barTintColor = CThemeColor;
        imagePicker.navigationBar.tintColor = [UIColor whiteColor];
        imagePicker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        imagePicker.delegate = self;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)presentPhotoLibrayWithfullImageDetail:(CompletionHandlerFullImageInfo)handler AllowEditing:(BOOL)isEditing;
{
    [self setBoolean:isEditing forKey:ALLOWEDITING];
    [self setBoolean:YES forKey:IMAGEFULLDETAIL];
    [self setObject:handler forKey:COMPLETIONHANDLER];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = isEditing;
        imagePicker.navigationBar.barTintColor = CThemeColor;
        imagePicker.navigationBar.tintColor = [UIColor whiteColor];
        imagePicker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        imagePicker.delegate = self;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)presentPhotoCamera:(CompletionHandler)handler AllowEditing:(BOOL)isEditing
{
    [self setBoolean:NO forKey:IMAGEFULLDETAIL];
    [self setObject:handler forKey:COMPLETIONHANDLER];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = isEditing;
        imagePicker.navigationBar.barTintColor = CThemeColor;
        imagePicker.navigationBar.tintColor = [UIColor whiteColor];
        imagePicker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." preferredStyle:UIAlertControllerStyleAlert];
        
        [alertView addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}



#pragma mark -
#pragma mark - UIImagePickerController Delagate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        BOOL isEditingImage = [self booleanForKey:ALLOWEDITING];
        UIImage *image;
        
        if (isEditingImage)
            image = [info objectForKey:UIImagePickerControllerEditedImage];
        else
            image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        BOOL isImageFullDetail = [self booleanForKey:IMAGEFULLDETAIL];
        
        if (isImageFullDetail)
        {
            CompletionHandlerFullImageInfo handler = [self objectForKey:COMPLETIONHANDLER];
            if (handler)
                handler(image,info);
        }
        else
        {
            CompletionHandler handler = [self objectForKey:COMPLETIONHANDLER];
            if (handler)
                handler(image);
        }
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{

        CompletionHandler handler = [self objectForKey:COMPLETIONHANDLER];
        if (handler)
            handler(nil);
    }];
}

@end
