//
//  ImageZoomViewController.h
//  MetLife_Demo
//
//  Created by mac-0009 on 9/4/15.
//  Copyright (c) 2015 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgZoomCollectionViewCell.h"

@interface ImageZoomViewController : SuperViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong,nonatomic) NSArray *imageArray;
@property (strong, nonatomic) IBOutlet UICollectionView *colVImageZoom;
@property (strong, nonatomic) IBOutlet UIPageControl *pcImageZoom;
@property (strong, nonatomic) NSString *strTitle;
@property int currentRow;

@end
