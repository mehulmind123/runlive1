//
//  RunSummaryResultCell.h
//  RunLive
//
//  Created by mac-0006 on 19/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunSummaryResultCell : UICollectionViewCell
@property IBOutlet UIImageView *imgSide;
@property IBOutlet UILabel *lblContent,*lblSubContent,*lblUnit;

@end
