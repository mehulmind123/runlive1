//
//  RunSummaryCommentCell.m
//  RunLive
//
//  Created by mac-0006 on 17/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "RunSummaryCommentCell.h"

@implementation RunSummaryCommentCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imgUserPic.layer.cornerRadius = self.imgUserPic.frame.size.width/2;
    self.imgUserPic.layer.masksToBounds = YES;
    
    self.lblUserName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblUserName.frame);
    self.lblComment.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment.frame);
    
}

@end
