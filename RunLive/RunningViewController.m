//
//  RunningViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "RunningViewController.h"
#import "QuitRunSession.h"
#import "RunSummaryViewController.h"
#import "SelectMusicViewController.h"
#import "RunningMusicViewController.h"
#import "UserTrackCell.h"
#import "MeterDistanceCell.h"
#import "InAppPurchaseViewController.h"
#import "PhotoVideoPreviewController.h"
#import "LocationManager.h"
#import "MotionManager.h"

#define cellViewUserHeight 50
#define cellViewUserWidth 50
#define cellViewUserX 15
#define collectionViewTrellingSpace 50
#define CLoginUserPasses @"loginuserpasses"
#define COtherUserPasses @"otheruserpasses"

@interface RunningViewController ()

@property (nonatomic, strong) MotionManager *motionManager;
@property (nonatomic, strong) NSMutableArray *arrLocations;
@property (nonatomic, assign) BOOL isFirstTrack;
@property (nonatomic, strong) didUpdateLocation didUpdateLocation;
@property (nonatomic, strong) didChangeLocationQuality didChangeLocationQuality;

@end

@implementation RunningViewController
{
    CLMBProgress *clProgress;
    AgoraRtcEngineKit *objAgoraRTCVoiceKit;
    
    NSTimer *timerLockProgress, *updateTimer, *TimerRunning,*timerCameraProgress,*timerVoiceChatProgress;
    
    int timeUnlockProcess,timeCameraProcess,timeVoiceChatProcess,currentplayMusicNo;
    
    NSMutableArray *arrMusic;
    
    BOOL isFirstTimePlay,isRepeat,isShuffle,isMusicScreen, isFromSpotify, isForwardStarted,isQuitRace, isShuffleSongStarted;
    NSTimer *timerNext, *timerPrevious;
    
    /*<--------------------- Map View Related --------------------->*/
    
    NSMutableArray *arrMapUser;
    
    
    /*<--------------------- Running Related --------------------->*/
    NSString *strSubscribedSessionId;
    float runningEndTime,sessionMaxCompleteTime,totalAverageSpeed,totalAverageBPM;
    int runningTime,runningTimeForShow;
    NSUInteger runningUserPossition;
    BOOL isRunningStarted,isMovingOnResultScreen;
    double lastLatitude,lastLongitude,totalRunDistance;
    CLLocation *currentLocation;
    NSString *strSessionShowRunnigTime;
    BOOL isAllowLocationUpdating;
    
    
    
    /*<--------------------- Track Related --------------------->*/
    NSMutableArray *arrTrackRunner,*arrMeter;
    CAShapeLayer *TriangleShapeLayer;
    float runDistanceLength;
    double trackMoveLatitude,trackMoveLongitude;
    BOOL isMeterDataLoaded; // To load track data in scroll view
    BOOL isCollectionViewNormalZoom;
    int userRunningStatus;
    NSUInteger myLastPosition;
    BOOL isLeadBy2000,isLeadBy1000,isLeadBy500,isLeadBy100,isOtherNear30,isOtherNear100,isGainingByOtherUser;
    BOOL isAppleAudioPlaying;
    
    AVSpeechSynthesizer *speechSynthesizer;
    BOOL MQTTReconnecting;
    NSMutableArray *arrPushAndVoiceNotification,*arrPushAndVoiceNotificationGaining;
 
    
    /*<--------------------- Voice Chat Related --------------------->*/
    BOOL isVoiceBroadCastingRunning,isRecevingAgoraVoice;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self askForGalaryPermission];
    
    speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    speechSynthesizer.delegate = self;
    
    viewLock.hidden = YES;
    self.arrLocations = [NSMutableArray new];
    
    arrPushAndVoiceNotification = [NSMutableArray new];
    arrPushAndVoiceNotificationGaining = [NSMutableArray new];
    
    // Music Related Code here.....
    
    arrMusic = [NSMutableArray new];
    [sliderMusic setThumbImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    
    // Set Gesture on lock unlock view here.........
    
    timeUnlockProcess = 0;
    UILongPressGestureRecognizer *longUnlockGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(unlockIncrementSpin:)];
    longUnlockGR.delegate = self;
    [viewUnlock setUserInteractionEnabled:YES];
    [viewUnlock addGestureRecognizer:longUnlockGR];
    
    lblUnlockProgress.startDegree = 0;
    lblUnlockProgress.endDegree = 0;
    lblUnlockProgress.isStartDegreeUserInteractive = YES;
    lblUnlockProgress.isEndDegreeUserInteractive = YES;

    timeCameraProcess = 0;
    UILongPressGestureRecognizer *longCameraGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cameraIncrementSpin:)];
    longCameraGR.delegate = self;
    [viewLockCamera setUserInteractionEnabled:YES];
    [viewLockCamera addGestureRecognizer:longCameraGR];

    timeVoiceChatProcess = 0;
    UILongPressGestureRecognizer *longVoiceChatGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(voiceChatIncrementSpin:)];
    longVoiceChatGR.delegate = self;
    [viewVoiceChat setUserInteractionEnabled:YES];
    [viewVoiceChat addGestureRecognizer:longVoiceChatGR];
    
    // Double tap on Voice chat button
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(voiceMuteUnMute)];
    doubleTap.numberOfTapsRequired = 2;
    [viewVoiceChat addGestureRecognizer:doubleTap];
    
    // Double tap on Runnner collection view..
    UITapGestureRecognizer *doubleTapOnRunnerCollectionView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomInOutCollectionView)];
    doubleTapOnRunnerCollectionView.numberOfTapsRequired = 2;
    [clGraph addGestureRecognizer:doubleTapOnRunnerCollectionView];
    
    
    lblCameraProgress.startDegree = 0;
    lblCameraProgress.endDegree = 0;
    lblCameraProgress.isStartDegreeUserInteractive = YES;
    lblCameraProgress.isEndDegreeUserInteractive = YES;
    
    
    
    clProgress = [[CLMBProgress alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight)];
    clProgress.hidden = YES;
    [self.view addSubview:clProgress];
    
    /*<------------------------- Map View Related code here ------------------------->*/
    
    arrMapUser = [NSMutableArray new];
    [self setInitialSetUpForRunner];
    
    
    /*<------------------------- Track Related code here ------------------------->*/
    
    arrMeter = [NSMutableArray new];
    arrTrackRunner = [NSMutableArray new];
    
    cnTrackViewHeight.constant = 568;
    [clMeter registerNib:[UINib nibWithNibName:@"MeterDistanceCell" bundle:nil] forCellWithReuseIdentifier:@"MeterDistanceCell"];
    [clGraph registerNib:[UINib nibWithNibName:@"UserTrackCell" bundle:nil] forCellWithReuseIdentifier:@"UserTrackCell"];
    
    
    // Resubcribe if MQTT connection was break from server............
    appDelegate.configureResubscribeOnMQTT = ^(BOOL isSubscribe)
    {
        if (appDelegate.objMQTTClient.connected && [[appDelegate getTopMostViewController] isKindOfClass:[RunningViewController class]] && strSubscribedSessionId)
        {
            // Subscribe Session On MQTT
            NSLog(@"RESUBSCRIBE ON MQTT HERE =============== >>>>>>> ");
            MQTTReconnecting = NO;
            NSString *strTopic = [NSString stringWithFormat:@"run_%@",strSubscribedSessionId];
            [appDelegate MQTTSubscribeWithTopic:strTopic];
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    appDelegate.isSharingVideoPhotoOnFacebook = NO;
    
    if (arrTrackRunner.count > 0)
        [clGraph reloadData];
    
    // Reconnect MQTT If not connected with server...
    if (!appDelegate.objMQTTClient.connected) {
        [appDelegate.objMQTTClient reconnect];
    }
    
    if (isQuitRace) {
        isQuitRace = NO;
        return;
    }
    
    [appDelegate hideTabBar];
    
    [self musicSetupCode];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (!isFromSpotify)
        [self addAndRemoveItunesMusicNotificationObserver:NO];
}


// Gesture Realted function here...............
#pragma mark - Lock Unlock Functions

-(void)showGetstureInfoLabel:(BOOL)isShow VoiceChat:(BOOL)isVoice
{
    lbLongPressGestureInfo.hidden = !isShow;
    lbLongPressGestureInfo.text = isVoice ? @"Tap and hold to talk, Double tap to mute voice chat audio" : @"Tap and hold";
}

- (void)unlockIncrementSpin:(UILongPressGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        [timerLockProgress invalidate];
        lblUnlockProgress.startDegree = 0;
        lblUnlockProgress.endDegree = 0;
        timeUnlockProcess = 0;
        [self showGetstureInfoLabel:NO VoiceChat:NO];
    }
    else if (sender.state == UIGestureRecognizerStateBegan)
    {
        [self showGetstureInfoLabel:YES VoiceChat:NO];
        timerLockProgress = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(updateLockProgressCounter) userInfo:nil repeats:YES];
    }
}

- (void)updateLockProgressCounter
{
    timeUnlockProcess+=30;
    [lblUnlockProgress setEndDegree:timeUnlockProcess];
    
    if(timeUnlockProcess >= 360)
    {
        timeUnlockProcess = 0;
        [timerLockProgress invalidate];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            viewUnlock.userInteractionEnabled = NO;
            viewLock.hidden = YES;
        });
    }
}

#pragma mark - Camera Gesture Related Functions

- (void)cameraIncrementSpin:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        [timerCameraProgress invalidate];
        lblCameraProgress.startDegree = 0;
        lblCameraProgress.endDegree = 0;
        timeCameraProcess = 0;
        [self showGetstureInfoLabel:NO VoiceChat:NO];
    }
    else if (sender.state == UIGestureRecognizerStateBegan)
    {
        [self showGetstureInfoLabel:YES VoiceChat:NO];
        timerCameraProgress = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(updateCameraProgressCounter) userInfo:nil repeats:YES];
    }
}

- (void)updateCameraProgressCounter
{
    timeCameraProcess+=30;
    [lblCameraProgress setEndDegree:timeCameraProcess];
    
    if(timeCameraProcess >= 360)
    {
        timeCameraProcess = 0;
        [timerCameraProgress invalidate];
        
        // Move on camera screen here..
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self btnCamera:nil];
            lblCameraProgress.startDegree = 0;
            lblCameraProgress.endDegree = 0;
            
        });
    }
}

#pragma mark - Voice Chat Related Functions

-(BOOL)checkSubcriptionPlanForVoiceChat
{
    BOOL isActivePlan = YES;
    
    NSDate *dtCreated = [self convertDateFromString:appDelegate.loginUser.subscription_exp_date isGMT:YES formate:CDateFormater];
    NSTimeInterval currentDateTimeStamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval SesisonExpireDateTimeStamp = [dtCreated timeIntervalSince1970];
    
    if (SesisonExpireDateTimeStamp > currentDateTimeStamp || appDelegate.loginUser.is_premium.boolValue)
    {
        isActivePlan = YES;
    }
    else
    {
        viewVoiceChat.alpha = 0.6;
        isActivePlan = NO;
    }
    
    return isActivePlan;
}

-(void)voiceMuteUnMute {
    
    // If voice chat feature disable from setting screen
    if (!appDelegate.loginUser.isVoiceChatEnable.boolValue)
    {
        [self customAlertViewWithOneButton:@"" Message:CMessageVoiceChatDisable ButtonText:@"OK" Animation:NO completed:nil];
        return;
    }
    
    // Stop music (If platying) while enable/Disable Agora
    BOOL isStopAnPlayMusic = btnPlay.selected;
    if (isStopAnPlayMusic)
        [self btnMusicCntrolCLK:btnPlay];
    
    
    [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:appDelegate.loginUser.muteVoiceChat.boolValue ? @"off" : @"on" VoiceChatEnable:nil AboutUs:nil Link:nil completed:^(id responseObject, NSError *error)
    {
        if (isStopAnPlayMusic)
            [self btnMusicCntrolCLK:btnPlay];
        
        [self setVoiceChatViewUI];
    }];
    
}

-(void)setVoiceChatViewUI
{
    if (appDelegate.loginUser.muteVoiceChat.boolValue)
    {
        viewVoiceChat.alpha = 0.6;
    }
    else
    {
        viewVoiceChat.alpha = 1;
    }

    // To stop voice streaming from other user
    [self AgoraMuteUnmuteRemoteAudioStrem:NO];
}

- (void)voiceChatIncrementSpin:(UILongPressGestureRecognizer*)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        // If voice chat feature disable from setting screen
        if (!appDelegate.loginUser.isVoiceChatEnable.boolValue)
            return;
        
        if (appDelegate.loginUser.muteVoiceChat.boolValue) // If voice chat is muted
        {
            return;
        }
        
        [timerVoiceChatProgress invalidate];
        lblVoiceChatProgress.startDegree = 0;
        lblVoiceChatProgress.endDegree = 0;
        timeVoiceChatProcess = 0;
        [self stopVoiceBroadcasting];
        
        [self showGetstureInfoLabel:NO VoiceChat:YES];
    }
    else if (sender.state == UIGestureRecognizerStateBegan)
    {
        // If voice chat feature disable from setting screen
        if (!appDelegate.loginUser.isVoiceChatEnable.boolValue)
        {
            [self customAlertViewWithOneButton:@"" Message:CMessageVoiceChatDisable ButtonText:@"OK" Animation:NO completed:nil];
            return;
        }
        
        if (appDelegate.loginUser.muteVoiceChat.boolValue) // If voice chat is muted
        {
            return;
        }
        
        [self showGetstureInfoLabel:YES VoiceChat:YES];
        timerVoiceChatProgress = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(updateVoiceChatProgressCounter) userInfo:nil repeats:YES];
    }
}

- (void)updateVoiceChatProgressCounter
{
    timeVoiceChatProcess+=2;
    [lblVoiceChatProgress setEndDegree:timeVoiceChatProcess];
    
    if (!isVoiceBroadCastingRunning)
        [self startVoiceBroadcasting];
    
    if(timeVoiceChatProcess >= 360)
    {
        timeVoiceChatProcess = 0;
        [timerVoiceChatProgress invalidate];
        
        // Move on camera screen here..
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            lblVoiceChatProgress.startDegree = 0;
            lblVoiceChatProgress.endDegree = 0;
            [self stopVoiceBroadcasting];
        });
    }
}

-(void)InitialSetupForVoiceChat
{
    /*
    if (![self checkSubcriptionPlanForVoiceChat])
    {
        return;
    }
     */

    // If voice chat feature disable from setting screen
    if (!appDelegate.loginUser.isVoiceChatEnable.boolValue)
    {
        return;
    }
    
    if (appDelegate.loginUser.muteVoiceChat.boolValue)
    {
        NSLog(@"Mute voice chat is enable ======");
        [self setVoiceChatViewUI];
    }
    
    isVoiceBroadCastingRunning = NO;
    isRecevingAgoraVoice = NO;
    
    objAgoraRTCVoiceKit = [AgoraRtcEngineKit sharedEngineWithAppId:CAgoraApiKey delegate:self];
    int channelProfileStatus =  [objAgoraRTCVoiceKit setChannelProfile:AgoraRtc_ChannelProfile_Game];
    int channelProfileAudio =  [objAgoraRTCVoiceKit setAudioProfile:AgoraRtc_AudioProfile_MusicHighQualityStereo scenario:AgoraRtc_AudioScenario_GameStreaming];
    
    NSLog(@"channelProfileStatus = %d",channelProfileStatus);
    NSLog(@"channelProfileAudio = %d",channelProfileAudio);
    
    // To stop voice brodcasting for other user
    [objAgoraRTCVoiceKit muteLocalAudioStream:YES];
    
    // Create voice mut setup here..
    [self setVoiceChatViewUI];
    
    // To Enable Speakerphon
    //[objAgoraRTCVoiceKit setEnableSpeakerphone:YES];
    
    // To Enable Default speakerphone
    //[objAgoraRTCVoiceKit setDefaultAudioRouteToSpeakerphone:YES];
    
    // Connect Agora voice chat
    [self connectAgoraForVoiceChat];
}

-(void)connectAgoraForVoiceChat
{
//    return;
//    int status = [objAgoraRTCVoiceKit joinChannel:strSubscribedSessionId info:nil uid:0];
    
    int status = [objAgoraRTCVoiceKit joinChannelByKey:nil channelName:strSubscribedSessionId info:nil uid:0 joinSuccess:^(NSString *channel, NSUInteger uid, NSInteger elapsed)
    {
            NSLog(@"joinChannelByKey =============== >>> ");
            
            //        NSDictionary *dicAudio = @{@"che.audio.use.remoteio": @"true"};
            NSDictionary *dicAudio = @{@"che.audio.use.vpio": @"true"};
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dicAudio options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            int parameterStatus = [objAgoraRTCVoiceKit setParameters:jsonString];
            NSLog(@"parameterStatus = %d",parameterStatus);
//            [objAgoraRTCVoiceKit enableAudioVolumeIndication:200 smooth:3];
  }];
    
    if (status == 0)
    {
        NSLog(@"Start Voice Chat here! ========== ");
    }
    else
    {
        NSLog(@"Failed To Joined Agora! ========== ");
        [self connectAgoraForVoiceChat];
    }
}

-(void)disconnectAgoraVoiceChat
{
    [objAgoraRTCVoiceKit leaveChannel:nil];
}

-(void)AgoraMuteUnmuteRemoteAudioStrem:(BOOL)isVoiceNotification
{
    isVoiceNotification ? [objAgoraRTCVoiceKit muteAllRemoteAudioStreams:YES] : [objAgoraRTCVoiceKit muteAllRemoteAudioStreams:appDelegate.loginUser.muteVoiceChat.boolValue];
}

-(void)startVoiceBroadcasting
{
    [self playPauseSoundForSpeechAndPushNotification:YES];
    
    isVoiceBroadCastingRunning = YES;
    [objAgoraRTCVoiceKit muteLocalAudioStream:NO];
    [objAgoraRTCVoiceKit resumeAllEffects];
}

-(void)stopVoiceBroadcasting
{
    [self playPauseSoundForSpeechAndPushNotification:NO];
    
    isVoiceBroadCastingRunning = NO;
    [objAgoraRTCVoiceKit muteLocalAudioStream:YES];
    [objAgoraRTCVoiceKit pauseAllEffects];
}


#pragma mark - AgoraRtcEngineKit Delegate Methods
#pragma mark -

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didAudioMuted:(BOOL)muted byUid:(NSUInteger)uid;
{
    NSLog(@"didAudioMuted ===");
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine reportRtcStats:(AgoraRtcStats*)stats;
{
    
    if (isVoiceBroadCastingRunning)
    {
        // While user speaking something.....
        return;
    }
    
    if (stats.rxAudioKBitrate > 0)
    {
        // Stop music here...
        [self playPauseSoundForSpeechAndPushNotification:YES];
        isRecevingAgoraVoice = YES;
    }
    else
    {
        if (isRecevingAgoraVoice)
        {
            // Play music here ...
            [self playPauseSoundForSpeechAndPushNotification:NO];
            isRecevingAgoraVoice = NO;
        }
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinChannel:(NSString*)channel withUid:(NSUInteger)uid elapsed:(NSInteger) elapsed;
{
    NSLog(@"Did joined channel: == %@",channel);
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinedOfUid:(NSUInteger)uid elapsed:(NSInteger)elapsed
{
    NSLog(@"Did joined of uid: == %lu",(unsigned long)uid);
}

- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine didLeaveChannelWithStats:(AgoraRtcStats * _Nonnull)stats;
{
    NSLog(@"didLeaveChannelWithStats ===");
}


- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOccurWarning:(AgoraRtcWarningCode)warningCode;
{
//            NSLog(@"didOccurWarning ===");
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine reportAudioVolumeIndicationOfSpeakers:(NSArray*)speakers totalVolume:(NSInteger)totalVolume;
{
    
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didAudioRouteChanged:(AgoraRtcAudioOutputRouting)routing;
{
    NSLog(@"AgoraRtcAudioOutputRouting == %d",routing);
}

- (void)rtcEngineConnectionDidInterrupted:(AgoraRtcEngineKit *)engine
{
    NSLog(@"Connection Interrupted == ");
}

- (void)rtcEngineConnectionDidLost:(AgoraRtcEngineKit *)engine
{
    NSLog(@"Connection Lost == ");
}

- (void)rtcEngineRequestChannelKey:(AgoraRtcEngineKit *)engine
{
    NSLog(@"rtcEngineRequestChannelKey == ");
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOccurError:(AgoraRtcErrorCode)errorCode;
{
    NSLog(@"Occur error == %ld",(long)errorCode);
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didRejoinChannel:(NSString*)channel withUid:(NSUInteger)uid elapsed:(NSInteger) elapsed;
{
    NSLog(@"didRejoinChannel == %@",channel);
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraRtcUserOfflineReason)reason
{
//    NSLog(@"Did offline of uid: == %u",(unsigned)reason);
}


#pragma mark - API Related Functions
#pragma mark -
-(void)completeQuitSession:(BOOL)isQuit
           RunCompleteType:(NSString *)runComType
                 completed:(void (^)(id responseObject,NSError *error))completion
{
    [GiFHUD showWithOverlay];
    
    if (lastLatitude !=0 && lastLongitude != 0)
        [self PublishOnMQTTUserUpdatedData:lastLatitude
                                 Longitude:lastLongitude
                               VoiceSpeech:NO];
    
    if (!strSubscribedSessionId)    // If session Id is not found..........
        return;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [GiFHUD dismiss];
        
        [[APIRequest request] competeQuitRunning:strSubscribedSessionId Type:isQuit ? @"quit" : @"complete" RunCompleteType:runComType completed:^(id responseObject, NSError *error)
         {
             if (completion)
                 completion(responseObject,error);
         }];
    });
}


/*
        . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
        . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
                                < ---------------------------- Start Music Related Stuff ---------------------------- >//
 
        . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
        . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 */


#pragma mark - MPMusicPlayerController Related Methods
#pragma mark -

-(void)updateMusicUIForItunes
{
    MPMediaItem *objAppleSong = appDelegate.appleMPMusicPlayerController.nowPlayingItem;
    NSString *strArtistApple = objAppleSong.artist ? objAppleSong.artist : @"";
    NSString *strSongNameApple = objAppleSong.title ? objAppleSong.title : @"";
    lblSongName.text = [NSString stringWithFormat:@"%@ %@",strArtistApple, strSongNameApple];
    NSMutableAttributedString *textApple = [[NSMutableAttributedString alloc] initWithAttributedString: lblSongName.attributedText];
    [textApple addAttribute: NSFontAttributeName value:CFontGilroyBold(14) range: NSMakeRange(0,strArtistApple.length)];
    lblSongName.attributedText  = textApple;
    NSNumber *duration=[objAppleSong valueForProperty:MPMediaItemPropertyPlaybackDuration];
    sliderMusic.maximumValue = duration.integerValue;
}


-(void)playItunesMusic
{
    [self updateMusicUIForItunes];
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateMusicTime:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:updateTimer forMode:NSRunLoopCommonModes];
}

-(BOOL)getMPMusicPlaySate
{
    BOOL isStop = NO;
    MPMusicPlaybackState playbackState = appDelegate.appleMPMusicPlayerController.playbackState;
    if (playbackState == MPMusicPlaybackStatePlaying)
    {
        isStop = NO;
    }
    else
    {
        isStop = YES;
    }
    
    return isStop;
}

-(void)mpMusicPlayerPayPause:(BOOL)isPlay
{
    if (isPlay)
    {
        [appDelegate.appleMPMusicPlayerController play];
        btnPlay.selected = YES;
        NSLog(@"Play song === ");
        isAppleAudioPlaying = YES;
    }
    else
    {
        [appDelegate.appleMPMusicPlayerController pause];
        btnPlay.selected = NO;
        isAppleAudioPlaying = NO;
        NSLog(@"Pause song === ");
    }
}

#pragma mark - Spotify Control Lock Screen
#pragma mark -
-(void)lockScreenMusicControlForSpotify
{
    if (!isFromSpotify)
    {
        return;
    }
    
    //...Control from Lock Screen Command Center
    MPRemoteCommandCenter *remoteCommandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    [remoteCommandCenter.previousTrackCommand setEnabled:YES];
    [remoteCommandCenter.previousTrackCommand addTarget:self action:@selector(spotiyLockPlayPrevious)];
    
    [remoteCommandCenter.pauseCommand setEnabled:YES];
    [remoteCommandCenter.pauseCommand addTarget:self action:@selector(spotiyLocktogglePlayPause)];
    
    [remoteCommandCenter.playCommand setEnabled:YES];
    [remoteCommandCenter.playCommand addTarget:self action:@selector(spotiyLocktogglePlayPause)];
    
    [remoteCommandCenter.nextTrackCommand setEnabled:YES];
    [remoteCommandCenter.nextTrackCommand addTarget:self action:@selector(spotiyLockPlayNext)];
}

-(void)spotiyLockPlayPrevious
{
    NSLog(@"spotiyLockPlayPrevious");
    [self btnMusicCntrolCLK:btnBackWard];
}

-(void)spotiyLocktogglePlayPause
{
    NSLog(@"spotiyLocktogglePlayPause");
    [self btnMusicCntrolCLK:btnPlay];
}

-(void)spotiyLockPlayNext
{
    NSLog(@"spotiyLockPlayNext");
    [self btnMusicCntrolCLK:btnForward];
}

- (void)playerUpdateReachTime
{
    if (!appDelegate.player.metadata.currentTrack)
    {
        return;
    }
    
    if (!isFromSpotify)
    {
        return;
    }
    
    if([lblSongName.text isEqualToString:@"Loading..."])
        return;
    
    
//    NSLog(@"playerUpdateReachTime =======  === >>>>>>>>>>>>>> ");
    
    NSMutableDictionary *nowPlayingInfo = [NSMutableDictionary dictionary];

    [nowPlayingInfo setObject:appDelegate.player.metadata.currentTrack.name forKey:MPMediaItemPropertyTitle];
    [nowPlayingInfo setObject:appDelegate.player.metadata.currentTrack.artistName forKey:MPMediaItemPropertyArtist];
    
    NSNumber *duration = [NSNumber numberWithInt:appDelegate.player.metadata.currentTrack.duration];
    NSNumber *reachTime = [NSNumber numberWithInt:appDelegate.player.playbackState.position];
    if (duration) [nowPlayingInfo setObject:duration forKey:MPMediaItemPropertyPlaybackDuration];
    if (reachTime) [nowPlayingInfo setObject:reachTime forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [nowPlayingInfo setObject:btnPlay.selected ? @1.0 : @0.0 forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nowPlayingInfo];
}

#pragma mark- Media player notification handlers
#pragma mark-
-(void)addAndRemoveItunesMusicNotificationObserver:(BOOL)isAddObserver
{
    if (isAddObserver)
    {
        // Register for music player notifications
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self
                               selector:@selector(handleNowPlayingItemChanged:)
                                   name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        [notificationCenter addObserver:self
                               selector:@selector(handlePlaybackStateChanged:)
                                   name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        [notificationCenter addObserver:self
                               selector:@selector(handleExternalVolumeChanged:)
                                   name:MPMusicPlayerControllerVolumeDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        
        [appDelegate.appleMPMusicPlayerController beginGeneratingPlaybackNotifications];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerVolumeDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [appDelegate.appleMPMusicPlayerController endGeneratingPlaybackNotifications];
    }
}


// When the now playing item changes, update song info labels and artwork display.
- (void)handleNowPlayingItemChanged:(id)notification
{
    // Ask the music player for the current song.
    NSLog(@"handleNowPlayingItemChanged ========= >>> ");
    [self updateMusicUIForItunes];
}

// When the playback state changes, set the play/pause button appropriately.
- (void)handlePlaybackStateChanged:(id)notification
{
    NSLog(@"handlePlaybackStateChanged ========= >>> ");
    btnPlay.selected = ![self getMPMusicPlaySate];
}

// When the volume changes, sync the volume slider
- (void)handleExternalVolumeChanged:(id)notification
{
    NSLog(@"handleExternalVolumeChanged ========= >>> ");
}

#pragma mark - Music Functions
#pragma mark -
-(void)musicSetupCode
{
    
    if(appDelegate.isMusicSelected)
    {
        currentplayMusicNo = 0;
        [self getMusicPlayList];
        appDelegate.isMusicSelected = NO;
    }
    
    
    // Spotify music set up...
    isFromSpotify = [self isFromSpotify];
    
    if (isFromSpotify)
    {
        [self lockScreenMusicControlForSpotify];
        
        [appDelegate.appleMPMusicPlayerController pause];
        
        if(updateTimer)
        {
            [updateTimer invalidate];
            updateTimer = nil;
        }
    }
    else
    {
        [appDelegate.player setIsPlaying:NO callback:^(NSError *error) {
            
        }];
    }
    
    isMusicScreen = NO;
    
    appDelegate.configureSpotifyLogin = ^(NSError *error)
    {
        [self updateUI];
    };
    
    appDelegate.configureDidChangePosition = ^(NSTimeInterval position)
    {
        [self setTimeOnSpotifyMusic];
    };
    
    appDelegate.configureStartedPlaying = ^{
        btnPlay.selected = YES;
        [self updateUI];
    };
    
    appDelegate.configureTrackChanged = ^(SPTPlaybackMetadata *metadata)
    {
        if (isRepeat)
            return;
        
        if (isShuffle)
        {
            if (isShuffleSongStarted)
                return;
            
            [self shuffleSpotifyMusic];
            return;
        }
        
        currentplayMusicNo = (int)appDelegate.player.metadata.currentTrack.indexInContext;
        [self updateUI];
    };
    
    appDelegate.configureNewTrackStarted = ^(NSError *error)
    {
        btnPlay.selected = YES;
    };
    
    appDelegate.configureTrackStopped = ^(BOOL stopPlaying)
    {
        // Stop music here..
        if (stopPlaying)
        {
            if (btnPlay.selected)
            {
                [self btnMusicCntrolCLK:btnPlay];
            }
            
            return ;
        }
        
        isShuffleSongStarted = NO;
        
        //For last song getting finished automatically
        
        if (!btnPlay.selected)
            return;
        
        [self btnMusicCntrolCLK:btnForward];
    };
    
}

-(void)hideShowMusicPlayerControl:(BOOL)isHide
{
    //    isHide = NO;
    btnRepeat.hidden = btnShuffle.hidden = isHide;
}

-(void)getMusicPlayList
{
    NSArray *arr = [TblMusicList fetchAllObjects];
    arrMusic = [[NSMutableArray alloc]initWithArray:arr];
    
    if (arrMusic.count > 0)
    {
        TblMusicList *objMusic = (TblMusicList *)[arrMusic objectAtIndex:0];
        
        if(objMusic.isApple.boolValue)
        {
            if ([CUserDefaults objectForKey:CAppleMusicCollection] && [CUserDefaults valueForKey:CAppleMusicCollection])
            {
                [self mpMusicPlayerPayPause:YES];
                
                // Hide Music control here for apple music..
                [self hideShowMusicPlayerControl:YES];
                [self addAndRemoveItunesMusicNotificationObserver:YES];
                
                viewNoMusic.hidden = YES;
                viewMusic.hidden = NO;
                isFromSpotify = NO;
                [self PlayMusicWithPath:(int)currentplayMusicNo];
            }
            else
            {
                viewNoMusic.hidden = NO;
                viewMusic.hidden = YES;
            }
            
        }
        else
        {
            // Show Music control here for Spotify music..
            [self hideShowMusicPlayerControl:NO];
            
            [appDelegate.player setIsPlaying:NO callback:nil];
            
            isFromSpotify = YES;
            SPTAuth* auth = [SPTAuth defaultInstance];
            if ([appDelegate isSpotifySessionValid:auth])
            {
                lbNoSongSelected.text = @"No Song Selected";
                viewNoMusic.hidden = YES;
                viewMusic.hidden = NO;
                
                appDelegate.strMainURI = objMusic.main_spotify_uri;
                
                if (!appDelegate.player)
                    [appDelegate setSpotifyPlayer];
                else
                {
                    [appDelegate.player playSpotifyURI:appDelegate.strMainURI startingWithIndex:0 startingWithPosition:0 callback:^(NSError *error)
                     {
                         if (error != nil)
                         {
                             NSLog(@"*** failed to play: %@", error);
                             
                             return;
                         }
                         
                         btnPlay.selected = YES;
                     }];
                }
            }
            else
            {
                viewNoMusic.hidden = NO;
                viewMusic.hidden = YES;
                //lblSongName.text = @"Spotify session is lost. Click here to reconnect.";
                lbNoSongSelected.text = @"Spotify session is lost. Click here to reconnect.";
            }
        }
    }
    else
    {
        viewNoMusic.hidden = NO;
        viewMusic.hidden = YES;
        
        
        [appDelegate.appleMPMusicPlayerController pause];
        [appDelegate.player setIsPlaying:NO callback:nil];
    }
}


-(void)PlayMusicWithPath:(int)index
{
    if (isFromSpotify)
        return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSLog(@"Check Music Album is availabe or not === >> %@",[CUserDefaults valueForKey:CAppleMusicCollection]);
        if (arrMusic.count > 0 && [CUserDefaults objectForKey:CAppleMusicCollection] && [CUserDefaults valueForKey:CAppleMusicCollection])
        {
            [self playItunesMusic];
        }
        else
        {
            viewNoMusic.hidden = NO;
            viewMusic.hidden = YES;
        }
    });
    
}

-(void)songAllreadyPlaying:(int)index CurrentTime:(int)currentTime
{
    if (arrMusic.count > 0)
    {
        if (isFromSpotify)
            sliderMusic.value = appDelegate.player.playbackState.position;
        else
        {
            [self playItunesMusic];
            sliderMusic.value = appDelegate.appleMPMusicPlayerController.currentPlaybackRate;
        }
    }
}

- (void)updateMusicTime:(NSTimer *)timer
{
    if (isFromSpotify)
        return;
    
    sliderMusic.value = appDelegate.appleMPMusicPlayerController.currentPlaybackTime;
}

#pragma mark - Random Audio Player (For Suffle)
#pragma mark -

-(void)getRandomTrackNo
{
    int randomInt1 = arc4random() % [arrMusic count];
    currentplayMusicNo = randomInt1;
}



/*
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 < ---------------------------- End Music Related Stuff ---------------------------- >//
 
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 */




/*
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 < ---------------------------- Start Runner Related Stuff ---------------------------- >//
 
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 */



#pragma mark - MQTT Function
#pragma mark -

-(void)addMQTTRunningNotificationObserver
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MQTTRunningPlayer:) name:CMQTTRunningPlayer object:nil];
    });
}

-(void)removeMQTTRunningNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CMQTTRunningPlayer object:nil];
}


-(void)MQTTRunningPlayer:(NSNotification *)notification
{
    NSDictionary *dicMQTT = [notification userInfo];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[RunningViewController class]] || [[appDelegate getTopMostViewController] isKindOfClass:[RunningMusicViewController class]])
        {
         //   NSLog(@"Running view controller============>>>>>>>>>>");
            if ([dicMQTT isKindOfClass:[NSDictionary class]])
            {
                [self getDataFromMQTT:dicMQTT];
            }
        }
    });
}

#pragma mark - Running Timer Functions
#pragma mark -

-(void)setTimerForRunning:(NSDictionary *)dicSessionData
{
    sessionMaxCompleteTime = ([dicSessionData stringValueForJSON:@"distance"].floatValue/1000) * 7.5;
    sessionMaxCompleteTime = sessionMaxCompleteTime*60;
    
    NSDate *date = [self convertDateFromString:[dicSessionData stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
    double sessionStartTimeStamp = [date timeIntervalSince1970];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    double difTimeStamp = currentTimestamp - sessionStartTimeStamp;
    
    double difTimeStampShow = (sessionMaxCompleteTime +sessionStartTimeStamp) - currentTimestamp;
    
    //    if (difTimeStamp >= 0)
    //    {
    
    if (difTimeStamp < 0)
        difTimeStamp = 0;
    
    runningTime = difTimeStamp;
    
    if (difTimeStampShow < 0)
        difTimeStampShow = 0;
    
    runningTimeForShow = difTimeStampShow;
    
    TimerRunning = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateRunningTime) userInfo:nil repeats:YES];
}

-(void)updateRunningTime
{
    runningTime++;
    runningTimeForShow--;
    
    if(runningTimeForShow < 0)
        runningTimeForShow = 0;
    
    if (runningTime <= sessionMaxCompleteTime)
    {
        lblTime.text= [self sessionTimeValueWithSpecificFormate:runningTimeForShow];
        strSessionShowRunnigTime = [self sessionTimeValueWithSpecificFormate:runningTime];
        
        lblAverageSpeed.text = [NSString stringWithFormat:@"%@/%@",[self stringifyAvgPaceFromDist:totalRunDistance overTime:runningTime ServerData:NO],[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"km" : @"mi"];
    }
    else
    {
        // Quit session here.....
        [TimerRunning invalidate];
        TimerRunning = nil;
        
        lblTime.text= [self sessionTimeValueWithSpecificFormate:runningTimeForShow];
        
        // Auto Quit when time is over...
        [self completeQuitSession:YES RunCompleteType:@"3" completed:^(id responseObject, NSError *error)
        {
            [self moveOnResultScreen:YES TimeOut:YES];
        }];
    }
    
    if(runningTime%2 == 0)
    {
        if ([appDelegate.healthStore authorizationStatusForType:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning]] == HKAuthorizationStatusSharingAuthorized)
        {
            __block float averateBPM = 0;
            [appDelegate.healthStore fetchBPMValueFromHealthKit:nil completion:^(HKQuantity *quantity, NSError *error)
             {
                 averateBPM = [quantity doubleValueForUnit:[[HKUnit countUnit] unitDividedByUnit:[HKUnit minuteUnit]]];
                 dispatch_async(GCDMainThread, ^{
                     lblHearBeat.text = averateBPM > 0 ? [NSString stringWithFormat:@"%.f",averateBPM] : @"—";
                 });
             }];
        }
        else
            lblHearBeat.text = @"—";
    }
    
    [self fetchSSIDInfoToCheckMQTTConnection];
}

-(NSString *)sessionTimeValueWithSpecificFormate:(int)timestamp
{
    int tempSecond = timestamp;
    int hours, minutes;
    
    int days = tempSecond / (60 * 60 * 24);
    tempSecond = tempSecond - days * (60 * 60 * 24);
    hours = tempSecond / (60 * 60);
    tempSecond = tempSecond - hours * (60 * 60);
    minutes = tempSecond / 60;
    tempSecond = tempSecond - minutes*60;
    
    return hours > 0 ? [NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,tempSecond] : [NSString stringWithFormat:@"00:%02d:%02d",minutes,tempSecond];
}


#pragma mark - Runnner Initail Set up
#pragma mark -

-(void)clearAllDataFromScreen
{
    // Reset data here for new session....
    
    viewUnlock.userInteractionEnabled = NO;
    viewLock.hidden = YES;
    
    mapView.hidden = viewBasicDetail.hidden = viewRunnerCount.hidden = scrollView.hidden = lblTotalRunDistance.hidden = YES;
    isRunningStarted = NO;
    isMeterDataLoaded = NO;
    isMovingOnResultScreen = NO;
    MQTTReconnecting = NO;
    isCollectionViewNormalZoom = YES;
    
    isLeadBy2000 = isLeadBy1000 = isLeadBy500 = isLeadBy100 = isOtherNear30 = isOtherNear100 = isGainingByOtherUser = NO;
    
    myLastPosition = 0;
    totalRunDistance = 0;
    runDistanceLength = 0;
    totalAverageSpeed = 0;
    runningUserPossition = 0;
    totalAverageBPM = 0;
    runningTime = 0;
    TimerRunning = nil;
    strSubscribedSessionId = nil;
    lblTotalRunDistance.text = lblTime.text = lblHearBeat.text = lblAverageSpeed.text = lblCurrentPosstion.text = nil;
    strSessionShowRunnigTime = nil;
    userRunningStatus = 1;
    
    [self.arrLocations removeAllObjects];
    
    [CUserDefaults removeObjectForKey:@"BSSID"];
    [CUserDefaults synchronize];
    
    [arrPushAndVoiceNotification removeAllObjects];
    [arrPushAndVoiceNotificationGaining removeAllObjects];
    
    [self removeTriangle];
    [arrMeter removeAllObjects];
    [clMeter reloadData];
    
    [arrTrackRunner removeAllObjects];
    [clGraph reloadData];
}

-(void)setInitialSetUpForRunner {
    [GiFHUD showWithOverlay];
    appDelegate.configureRunningSessionStart = ^(NSString *strSessionId)
    {
        [self startRunningAtTheFirstTime:strSessionId];
    };
}

-(void)startRunningAtTheFirstTime:(NSString *)strSessionId
{
    [self clearAllDataFromScreen];

    // MQTT Notification configuration
    [self addMQTTRunningNotificationObserver];
    
    strSubscribedSessionId = strSessionId;
    
    // Location manager configuration
   
    [self setupLocationManager];
    
    // Motion Detector configuration
    [self startMotionDetectorToGetSpeedAndUserMovement];

    // Agora voice chat configuration..
    [self InitialSetupForVoiceChat];
    
    // Muisc configuraiton..
    currentplayMusicNo = 0;
    [self getMusicPlayList];
    
    // Calling runing api here....
}

-(void)startRunningWithServerApi {
    self.view.userInteractionEnabled  = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.view.userInteractionEnabled  = YES;
        [appDelegate
         startRunningWithSessionID:strSubscribedSessionId
         currentLocation:[LocationManager sharedInstance].currentLocation
         completed:^(NSString *strSessionId,BOOL isError) {
             if (!isError) {
                 if (strSessionId){
                     lblHearBeat.text = @"—";
                     [GiFHUD dismiss];
                     // Publish on MQTT for first time when screen loaded......
                 } else {
                     // Send user to back on Session create screen
                     [GiFHUD dismiss];
                     [self moveOnResultScreen:NO TimeOut:NO];
                     [self customAlertViewWithOneButton:@"" Message:CMessageQuitFromSessionDueToInternet ButtonText:@"OK" Animation:YES completed:nil];
                     
                 }
             } else {
                 [GiFHUD dismiss];
                 [self customAlertViewWithOneButton:@"" Message:CMessageApiRequestTimeOut ButtonText:@"Retry" Animation:YES completed:^{
                     [self startRunningWithServerApi];
                 }];
             }
         }];
    });
    
}

- (void)fetchSSIDInfoToCheckMQTTConnection
{
    if (MQTTReconnecting)
    {
        [self hideShowGPSView:NO ConnectionMQTT:YES];
        return;
    }
    else
    {
        [self hideShowGPSView:YES ConnectionMQTT:YES];
    }
    
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    //    NSLog(@"%s: Supported interfaces: %@", __func__, interfaceNames);
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames)
    {
        SSIDInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        //        NSLog(@"%s: %@ => %@", __func__, interfaceName, SSIDInfo);
        
        if ([CUserDefaults objectForKey:@"BSSID"])
        {
            if ([[CUserDefaults valueForKey:@"BSSID"] isEqualToString:[SSIDInfo valueForKey:@"BSSID"]])
            {
                // NSLog(@"Same Network Connection ======== >>> ");
            }
            else
            {
                NSLog(@" Network Connection Changed Now ======== >>> ");
                MQTTReconnecting = YES;
                
                [appDelegate.objMQTTClient disconnectWithCompletionHandler:^(NSUInteger code)
                 {
                     [appDelegate MQTTInitialSetup];
                     NSLog(@"MQTT Disconnected from server ========== >>>> %lu",(unsigned long)code);
                 }];
                
                [CUserDefaults removeObjectForKey:@"BSSID"];
                [CUserDefaults setObject:[SSIDInfo valueForKey:@"BSSID"] forKey:@"BSSID"];
                [CUserDefaults synchronize];
            }
        }
        else
        {
            [CUserDefaults setObject:[SSIDInfo valueForKey:@"BSSID"] forKey:@"BSSID"];
            [CUserDefaults synchronize];
        }
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty)
        {
            break;
        }
    }
    
}

-(void)setDistanceAccordingToUserSelectedUnit
{
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
    {
        lblDistanceUnit.text = @"KM";
        lblTotalRunDistance.text = [NSString stringWithFormat:@"%.2f",totalRunDistance/1000];
    }
    else
    {
        lblDistanceUnit.text = @"MILES";
        float  kmData = totalRunDistance / 1000;
        float miles = kmData/ 1.60934;
        lblTotalRunDistance.text = [NSString stringWithFormat:@"%.1f",miles];
    }
    
}

#pragma mark - Get User Running position
#pragma mark -
-(NSArray *)sortRunnerAccordingToTotalDistance:(NSArray *)arrUserPossition Accending:(BOOL)isAcceding
{
    NSArray *sorters = @[[NSSortDescriptor sortDescriptorWithKey:@"total_distance" ascending:isAcceding comparator:^(id obj1, id obj2) {
        NSNumber *n1;
        NSNumber *n2;
        // Either use obj1/2 as numbers or try to convert them to numbers
        if ([obj1 isKindOfClass:[NSNumber class]]) {
            n1 = obj1;
        } else {
            n1 = @([[NSString stringWithFormat:@"%@", obj1] doubleValue]);
        }
        if ([obj2 isKindOfClass:[NSNumber class]]) {
            n2 = obj2;
        } else {
            n2 = @([[NSString stringWithFormat:@"%@", obj2] doubleValue]);
        }
        return [n1 compare:n2];
    }]];
    
    NSArray *sortedArray = [arrUserPossition sortedArrayUsingDescriptors:sorters];
    
    return sortedArray;
}

-(void)findUserRunningPossition
{
    NSMutableArray *arrUserPossition = [[NSMutableArray alloc] initWithArray:arrMapUser];
    
    NSArray *arrLogin = [arrUserPossition filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    
    NSDictionary *dicLoginUser;
    if (arrLogin.count > 0)
        dicLoginUser = arrLogin[0];
    
    NSArray *sortedArray = [self sortRunnerAccordingToTotalDistance:arrUserPossition.mutableCopy Accending:NO];
    
    [arrUserPossition removeAllObjects];
    [arrUserPossition addObjectsFromArray:sortedArray];
    
    NSUInteger index = [arrUserPossition indexOfObject:dicLoginUser];
    
    //    NSLog(@"user possition ========== >>>> %lu",(unsigned long)index+1);
    runningUserPossition = index+1;
    
    NSString *strPossition = [NSString stringWithFormat:@"%lu",(unsigned long)index+1];
    NSString *strTotalRunner = [NSString stringWithFormat:@"%lu",(unsigned long)arrUserPossition.count];;
    
    lblCurrentPosstion.text = [NSString stringWithFormat:@"%@/%@",strPossition, strTotalRunner];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lblCurrentPosstion.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, strPossition.length)];
    [text addAttribute: NSFontAttributeName value:lblCurrentPosstion.font range: NSMakeRange(0,strPossition.length)];
    lblCurrentPosstion.attributedText = text;
    
    // Set Speech notification alert ..........
    
    [arrUserPossition removeObject:dicLoginUser];
    
    for (int i = 0; arrUserPossition.count > i; i++)
    {
        NSDictionary *dicData = arrUserPossition[i];
        [self setAlertSpeechTextFor:dicData LoginUser:dicLoginUser CurrentPosition:runningUserPossition CompareUserPosition:i+1];
    }
    
}

-(NSDictionary *)findLoginRunner:(NSArray *)arrRunner
{
    NSMutableArray *arrLogin = [[NSMutableArray alloc] initWithArray:arrRunner];
    
    arrRunner = [arrLogin filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    
    NSDictionary *dicLoginUser;
    if (arrRunner.count > 0)
        dicLoginUser = arrRunner[0];
    
    return dicLoginUser;
}

// Sorted user from login user according to lat long
-(void)calculateDistannceBetweenUser
{
    NSArray *arrLogin = [arrMapUser filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    
    double loginUserLat = 0;
    double loginUserLong = 0;
    
    NSDictionary *dicLoginUser;
    if (arrLogin.count > 0)
    {
        dicLoginUser = arrLogin[0];
        loginUserLat = [dicLoginUser stringValueForJSON:@"latitude"].doubleValue;
        loginUserLong = [dicLoginUser stringValueForJSON:@"longitude"].doubleValue;
        
        if (!isRunningStarted) {
            totalRunDistance = [dicLoginUser stringValueForJSON:@"total_distance"].integerValue;
            lastLatitude = loginUserLat;
            lastLongitude = loginUserLong;
            
            if (lastLatitude !=0 && lastLongitude != 0) {
                CLLocation *oldLocation = [[CLLocation alloc]initWithLatitude:lastLatitude
                                                                    longitude:lastLongitude];
                [self.arrLocations addObject:oldLocation];
            }
        }
        
        // Remove login user from array...
        if ([arrMapUser containsObject:arrLogin[0]])
            [arrMapUser removeObject:arrLogin[0]];
    }
    
    NSMutableArray *arrTempData = [NSMutableArray new];
    
    for (int i = 0; arrMapUser.count>i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrMapUser[i]];
        double otherUserLat = [dicData stringValueForJSON:@"latitude"].doubleValue;
        double otherUserLong = [dicData stringValueForJSON:@"longitude"].doubleValue;
        
        CLLocation *locA1 = [[CLLocation alloc] initWithLatitude:loginUserLat longitude:loginUserLong];
        CLLocation *locB1 = [[CLLocation alloc] initWithLatitude:otherUserLat longitude:otherUserLong];
        CLLocationDistance distance1 = [locA1 distanceFromLocation:locB1];
        
        [dicData setObject:[NSString stringWithFormat:@"%.2f",distance1] forKey:@"distance"];
        [arrTempData addObject:dicData];
    }
    
    NSArray *sortedArray = [arrTempData sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES]]];
    
    [arrMapUser removeAllObjects];
    [arrMapUser addObject:dicLoginUser];    // Add login user at first Possition here....
    [arrMapUser addObjectsFromArray:sortedArray]; // Add other user here....
    
    [arrTempData removeAllObjects];
}

// Check user available in race..
-(BOOL)toCheckUserIsAvailableOrNot:(NSDictionary *)dicRunner
{
    NSMutableArray *arrRunner = [[NSMutableArray alloc] initWithArray:[dicRunner valueForKey:@"runner"]];
    NSDictionary *dicLogin = [self findLoginRunner:arrRunner];
    NSDictionary *dicComp = [dicLogin valueForKey:@"completed"];
    return [[dicComp numberForJson:@"rank"] isEqual:@0];
}

#pragma mark - Running On Going Related Functions

-(void)getDataFromMQTT:(NSDictionary *)dicRunner
{
    // To check user is apear or not in run......
    if (![self toCheckUserIsAvailableOrNot:dicRunner])
    {
        [self moveOnResultScreen:YES TimeOut:YES];
        return;
    }
    /*-------------------- Update map view data here --------------------*/
    
    // If change login user position and app in active mode then only update map...
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        [self moveMapAccordingToUserMovement];
    
    // Update map when app enter in forgroud mode.........
    [UIApplication applicationWillEnterForeground:^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[RunningViewController class]])
        {
            [self moveMapAccordingToUserMovement];
            [clGraph reloadData];
        }
    }];
    
    [arrMapUser removeAllObjects];
    [arrMapUser addObjectsFromArray:[dicRunner valueForKey:@"runner"]];
    
    [self findUserRunningPossition];
    [self calculateDistannceBetweenUser];
    
    // Check here if tootal distance is equal to session distance then call complete session api here
    if (totalRunDistance >= [dicRunner doubleForKey:@"distance"])
    {
        // Call Session complete api here..
        [TimerRunning invalidate];
        TimerRunning = nil;
        
        [self removeMQTTRunningNotificationObserver];
        
        // Complete Session Here..
        [self completeQuitSession:NO RunCompleteType:@"1" completed:^(id responseObject, NSError *error)
         {
             NSLog(@"Complete session api call here =========== >>> %f", totalRunDistance);
             [self moveOnResultScreen:YES TimeOut:NO];
         }];
        
        return;
    }
    
    // Call Running function at the first time only...
    if (!isRunningStarted)
    {
        [self setTimerForRunning:dicRunner];
        isRunningStarted = YES;
    }
    
    // Load Track Meter data
    if (isMeterDataLoaded)
    {
        // Get Runner from MQTT....
        [self storeTrackRunnerFromServer:dicRunner];
    }
    else
        [self calculateSideDistanceMeterRatio:dicRunner];
    
    [self setDistanceAccordingToUserSelectedUnit];
}

// When user Location update
-(void)PublishOnMQTTUserUpdatedData:(double)lat Longitude:(double)longi VoiceSpeech:(BOOL)isSpeech
{
    
    if(!isRunningStarted) {
        NSLog(@"SYSTEM FORCEFULLY TRY TO PUBLISH ON MQTT ======= >>>");
        return;
    }
    
    lastLatitude = lat;
    lastLongitude = longi;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // Publish Session here (When get new lat long)...
        NSMutableDictionary *dicPublishRun = [NSMutableDictionary new];
        [dicPublishRun setObject:[NSString stringWithFormat:@"%f",lastLatitude] forKey:@"latitude"];
        [dicPublishRun setObject:[NSString stringWithFormat:@"%f",lastLongitude] forKey:@"longitude"];
        [dicPublishRun setObject:strSubscribedSessionId forKey:@"session_id"];
        [dicPublishRun setObject:appDelegate.loginUser.user_id forKey:@"user_id"];
        [dicPublishRun setObject:[NSString stringWithFormat:@"%d",userRunningStatus] forKey:@"running_status"];
        [dicPublishRun setObject:isSpeech ? @"1" : @"0" forKey:@"sound_alert"];
        
        [dicPublishRun setObject:[NSString stringWithFormat:@"%lu",(unsigned long)runningUserPossition] forKey:@"position"];
        [dicPublishRun setObject:@"2" forKey:@"publish_status"];
        
        // Average speed
        [dicPublishRun setObject:[self stringifyAvgPaceFromDist:totalRunDistance overTime:runningTime ServerData:YES] forKey:@"average_speed"];
        
        // Total Time
        if (strSessionShowRunnigTime)
            [dicPublishRun setObject:strSessionShowRunnigTime forKey:@"total_time"];
        else
            [dicPublishRun setObject:@"00:00:00" forKey:@"total_time"];
        
        double tempTotalDistance = 0;
        if (runDistanceLength > totalRunDistance)
        {
            [dicPublishRun setObject:[NSString stringWithFormat:@"%f",totalRunDistance] forKey:@"total_distance"];
            tempTotalDistance = totalRunDistance;
        }
        else
        {
            [dicPublishRun setObject:[NSString stringWithFormat:@"%f",runDistanceLength] forKey:@"total_distance"];
            tempTotalDistance = runDistanceLength;
        }
        
        //        float weightKG = appDelegate.loginUser.weight.floatValue/2.20462; // Convert Weight into KG from Pounds
        float weightKG = appDelegate.loginUser.weight.floatValue;
        //                Kcal/Min ~= 0.0005 * bodyMassKg * metersWalkedInAMin + 0.0035
        float totalCalBurned = 0;
        if (tempTotalDistance > 0)
            totalCalBurned =0.0005*weightKG*tempTotalDistance+0.0035;
        
        [dicPublishRun setObject:[NSString stringWithFormat:@"%f",totalCalBurned] forKey:@"total_calories"];
        
        // Caculate Avergae BPM Value here....
        float averateBPM = lblHearBeat.text.floatValue;
        
        if (averateBPM > 1)
            [dicPublishRun setObject:[NSString stringWithFormat:@"%.f",averateBPM] forKey:@"average_bpm"];
        else
            [dicPublishRun setObject:@"0" forKey:@"average_bpm"];
        
        NSString *strTopic = [NSString stringWithFormat:@"run_%@",strSubscribedSessionId];
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dicPublishRun options:0 error:&err];
        [appDelegate MQTTPublishWithTopic:jsonData Topic:strTopic];
        
    });
    
}


#pragma mark - Location Related Methods

- (NSString *)stringifyAvgPaceFromDist:(float)meters
                              overTime:(int)seconds
                            ServerData:(BOOL)isServer
{
    if (seconds == 0 || meters == 0)
    {
        return @"00:00";
    }
    
    float avgPaceSecMeters = seconds / meters;
    
    float unitMultiplier;
    NSString *unitName;
    
    if (isServer)
    {
        unitName = @"min/km";
        unitMultiplier = 1000;
    }
    else
    {
        // metric
        if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
        {
            unitName = @"min/km";
            unitMultiplier = 1000;
        }
        else
        {
            unitName = @"min/mi";
            unitMultiplier = 1609.344;
        }
    }
    
    int paceMin = (int) ((avgPaceSecMeters * unitMultiplier) / 60);
    int paceSec = (int) (avgPaceSecMeters * unitMultiplier - (paceMin*60));
    
    return [NSString stringWithFormat:@"%i:%02i", paceMin, paceSec];
}

-(void)hideShowGPSView:(BOOL)isHide ConnectionMQTT:(BOOL)isMQTT
{
    lblConnectionIssue.text = isMQTT ? @"Connecting with server…" : @"weak gps signal, distance may not increase.";
    
    if (isHide)
    {
        // Hide View
        [UIView animateWithDuration:.3 animations:^{
            [viewNoGPS setConstraintConstant:-60 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        } completion:^(BOOL finished) {
            viewNoGPS.hidden = YES;
        }];
    }
    else
    {
        // Show View
        viewNoGPS.hidden = NO;
        [UIView animateWithDuration:.3 animations:^{
            [viewNoGPS setConstraintConstant:0 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }];
    }
}

-(void)stopLocationUpdate {
    self.didUpdateLocation = nil;
    self.didChangeLocationQuality = nil;
}

- (void)setupLocationManager {
    
    __weak __typeof(self) weakSelf = self;
    self.didUpdateLocation = ^(CLLocation *location) {
        float sppedKMhr = location.speed * 3.6f;
        if (sppedKMhr > 44.7) {
            [weakSelf disqualifyUser];
        }
        [weakSelf handleMotionType];
        if (userRunningStatus != 1) {
            if (weakSelf.arrLocations.count > 0) {
                totalRunDistance += [location distanceFromLocation:weakSelf.arrLocations.lastObject];
            }
            [weakSelf setDistanceAccordingToUserSelectedUnit];
            [weakSelf PublishOnMQTTUserUpdatedData:location.coordinate.latitude
                                         Longitude:location.coordinate.longitude
                                       VoiceSpeech:YES];
            [weakSelf.arrLocations addObject:location];
        }
    };
    
    self.didChangeLocationQuality = ^(BOOL isAccurate) {
        [weakSelf hideShowGPSView:isAccurate ConnectionMQTT:NO];
    };
    
    [LocationManager sharedInstance].didUpdateLocation = self.didUpdateLocation;
    [LocationManager sharedInstance].didChangeLocationQuality = self.didChangeLocationQuality;
    [[LocationManager sharedInstance] stopUpdatingLocation];
    [[LocationManager sharedInstance] startUpdatingLocation];
    [self startRunningWithServerApi];
}


- (void)handleMotionType {
    CMMotionActivity *activity = self.motionManager.currentActivity;
    if (activity.walking) {
        userRunningStatus = 2;
    } else if (activity.running) {
        userRunningStatus = 3;
    } else if (activity.stationary) {
        userRunningStatus = 1;
    }
}

- (void)disqualifyUser {
    [self.motionManager stopMotionDetection];
    [self stopLocationUpdate];
    NSString *strTopic = [NSString stringWithFormat:@"run_%@",strSubscribedSessionId];
    [appDelegate.objMQTTClient unsubscribe:strTopic withCompletionHandler:nil];
    
    [TimerRunning invalidate];
    TimerRunning = nil;
    
    [[APIRequest request] userDisqualifyFromRunning:strSubscribedSessionId completed:^(id responseObject, NSError *error) {
         if (responseObject && !error) {
             NSLog(@"User disqualify from Running =========== >>>");
             [self moveOnResultScreen:NO TimeOut:NO];
             [self customAlertViewWithOneButton:@"" Message:CMessageDisqualifiedFromRace ButtonText:@"OK" Animation:NO completed:nil];
         }
     }];
}

/*
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 < ---------------------------- End Runner Related Stuff ---------------------------- >//
 
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . .. . . . . . . . . . . .
 
 */


#pragma mark - AVSpeechUtterance Methods
#pragma mark -
-(NSString *)appendStringInPositionForLocalPushNotification:(NSUInteger)strPosition
{
    NSString *strRank;
    
    if (strPosition == 1)
        strRank = @"ST";
    else if (strPosition == 2)
        strRank = @"ND";
    else if (strPosition == 3)
        strRank = @"RD";
    else
        strRank = @"TH";
    
    strRank = [NSString stringWithFormat:@"%lu%@",(unsigned long)strPosition,strRank];
    
    return strRank;
}

-(NSString *)appendStringInPositionForVoiceText:(NSUInteger)strPosition
{
    NSString *strRank;
    
    switch (strPosition) {
        case 1:
            strRank = @"First";
            break;
        case 2:
            strRank = @"Second";
            break;
        case 3:
            strRank = @"Third";
            break;
        case 4:
            strRank = @"Fourth";
            break;
        case 5:
            strRank = @"Fifth";
            break;
        case 6:
            strRank = @"Sixth";
            break;
        case 7:
            strRank = @"Seventh";
            break;
        case 8:
            strRank = @"Eighth";
            break;
        case 9:
            strRank = @"Nineth";
            break;
        case 10:
            strRank = @"Tenth";
            break;
        case 11:
            strRank = @"Eleventh";
            break;
        case 12:
            strRank = @"Twelfth";
            break;
        case 13:
            strRank = @"Thirteenth";
            break;
        case 14:
            strRank = @"Fourteenth";
            break;
        case 15:
            strRank = @"Fifteenth";
            break;
        case 16:
            strRank = @"Sixteenth";
            break;
        case 17:
            strRank = @"Seventeenth";
            break;
        case 18:
            strRank = @"Eighteenth";
            break;
        case 19:
            strRank = @"Nineteenth";
            break;
        case 20:
            strRank = @"Twentieth";
            break;
            
        default:
            break;
    }
    
    return strRank;
}


-(void)setAlertSpeechTextFor:(NSDictionary *)dicOtherUser LoginUser:(NSDictionary *)dicLoginUser CurrentPosition:(NSUInteger)currentPosition CompareUserPosition:(NSUInteger)comparePosition
{
    NSDictionary *dicCompleted = [dicOtherUser valueForKey:@"completed"];
    
    if ([dicCompleted stringValueForJSON:@"rank"].intValue > 0 || [dicCompleted stringValueForJSON:@"sound_alert"].intValue == 0)
    {
        myLastPosition = currentPosition;
        return;
    }
    
    BOOL isSendPushNotification = NO;
    BOOL isSpokenAudio = NO;
    BOOL isPlaySound = NO;
    BOOL isLeadingVoicePush = NO;
    
    int soundType = 0;
    
    NSString *strSpeechText;
    NSString *strPushText;
    
    if (myLastPosition == currentPosition)
    {
        isLeadingVoicePush = YES;
    }
    else
    {
        [arrPushAndVoiceNotification removeAllObjects];
        [arrPushAndVoiceNotificationGaining removeAllObjects];
        
        isLeadingVoicePush = NO;
        isLeadBy2000 = isLeadBy1000 = isLeadBy500 = isLeadBy100 = isOtherNear30 = isOtherNear100 = isGainingByOtherUser = NO;
    }
    
    float distanceMeter = [dicLoginUser stringValueForJSON:@"total_distance"].floatValue - [dicOtherUser stringValueForJSON:@"total_distance"].floatValue;
    
    // If Login user behind from other user...
    if([dicOtherUser stringValueForJSON:@"total_distance"].floatValue > [dicLoginUser stringValueForJSON:@"total_distance"].floatValue)
    {
        // When login user behind from other user
        float distanceMeterFromOtherUser = [dicOtherUser stringValueForJSON:@"total_distance"].floatValue - [dicLoginUser stringValueForJSON:@"total_distance"].floatValue;
        //When someone passes user, Play Sound Notification and PUSH text notification: “User First Name has passed you, speed up!”
        
        NSLog(@"Compare Position = ====== >> %lu",(unsigned long)comparePosition);
        NSLog(@"Current Position = ====== >> %lu",(unsigned long)currentPosition);
        
        if (myLastPosition < currentPosition && comparePosition == currentPosition-1)
        {
            // Play sound when any passes login user
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            isPlaySound = YES;
            
            strSpeechText = [NSString stringWithFormat:@"%@ has passed you, speed up!",[dicOtherUser stringValueForJSON:@"user_name"]];
            strPushText = strSpeechText;
            soundType = 2;
            
            myLastPosition = currentPosition;
        }
        else if (distanceMeterFromOtherUser >= 30 && distanceMeterFromOtherUser <= 70  && [arrPushAndVoiceNotification containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
        {
            //            When you’re within 30 yards of passing someone: “You’re gaining on User First Name, Run Faster"
            [arrPushAndVoiceNotification removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            strSpeechText = [NSString stringWithFormat:@"You’re gaining on %@, Run Faster",[dicOtherUser stringValueForJSON:@"user_name"]];
            strPushText = strSpeechText;
        }
        // If login user distance diff. from other user is more then 70 meter..
        else if (distanceMeterFromOtherUser > 70)
        {
            if ([arrPushAndVoiceNotification containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
                [arrPushAndVoiceNotification removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            
            [arrPushAndVoiceNotification addObject:[dicOtherUser stringValueForJSON:@"user_id"]];
        }
    }
    
    // When login user passes some one
    else if (myLastPosition > currentPosition)
    {
        NSLog(@"I AM ON THE 111111111111111111111111111 POSTION");
        // Play Cheering sound here for passing someone...
        isPlaySound = YES;
        soundType = 1;
        
        myLastPosition = currentPosition;
    }
    // If login user position not changed
    else if (isLeadingVoicePush)
    {
        if (distanceMeter >= 2000 && !isLeadBy2000)
        {
            //When I am in the lead by 2 miles: “You have the lead by two miles”
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            isLeadBy2000 = YES;
            
            strSpeechText = [NSString stringWithFormat:@"You have %@ place by two kilometers",[self appendStringInPositionForVoiceText:currentPosition]];
            strPushText = [NSString stringWithFormat:@"You have %@ place by two kilometers",[self appendStringInPositionForLocalPushNotification:currentPosition]];;
        }
        else if (distanceMeter >= 1000  && !isLeadBy1000)
        {
            //When I am in the lead by 1 miles: “You have the lead by one mile”
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            isLeadBy1000 = YES;
            
            strSpeechText = [NSString stringWithFormat:@"You have %@ place by one kilometer",[self appendStringInPositionForVoiceText:currentPosition]];
            strPushText = [NSString stringWithFormat:@"You have %@ place by one kilometer",[self appendStringInPositionForLocalPushNotification:currentPosition]];;
            
        }
        else if (distanceMeter >= 500 && !isLeadBy500)
        {
            //When I am in the lead by .5 miles: "You have the lead by half a mile”
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            isLeadBy500 = YES;
            
            strSpeechText = [NSString stringWithFormat:@"You have %@ place by half a kilometer",[self appendStringInPositionForVoiceText:currentPosition]];
            strPushText = [NSString stringWithFormat:@"You have %@ place by half a kilometer",[self appendStringInPositionForLocalPushNotification:currentPosition]];;
        }
        else if (distanceMeter >= 100 && !isLeadBy100)
        {
            //When I am in the lead by 100 meters: "You have the lead by 100 meters"
            isSendPushNotification = YES;
            isSpokenAudio = YES;
            isLeadBy100 = YES;
            
            strSpeechText = [NSString stringWithFormat:@"You have %@ place by 100 meters",[self appendStringInPositionForVoiceText:currentPosition]];
            strPushText = [NSString stringWithFormat:@"You have %@ place by 100 meters",[self appendStringInPositionForLocalPushNotification:currentPosition]];;
        }
        
        else if (distanceMeter >= 30 && distanceMeter <= 70  && [arrPushAndVoiceNotification containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
        {
            //        When someone comes within 30 yards of user: “User First Name is about to pass you, run faster!”
            [arrPushAndVoiceNotification removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            
            isSpokenAudio = YES;
            strSpeechText = [NSString stringWithFormat:@"%@ is about to pass you, run faster!",[dicOtherUser stringValueForJSON:@"user_name"]];
        }
        
        else if (distanceMeter >= 70 && distanceMeter <= 100 && [arrPushAndVoiceNotificationGaining containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
        {
            //            When someone comes within 100 yards of user: “User First Name is gaining on you, run faster!”
            [arrPushAndVoiceNotificationGaining removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            isSpokenAudio = YES;
            strSpeechText = [NSString stringWithFormat:@"%@ is gaining on you, run faster!",[dicOtherUser stringValueForJSON:@"user_name"]];
        }
        
        // If login user distance diff. from other user is greater then 100 meter..
        else if (distanceMeter > 100)
        {
            if ([arrPushAndVoiceNotificationGaining containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
                [arrPushAndVoiceNotificationGaining removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            
            [arrPushAndVoiceNotificationGaining addObject:[dicOtherUser stringValueForJSON:@"user_id"]];
        }
        
        // If login user distance diff. from other user is greater then 70 meter..
        else if (distanceMeter > 70)
        {
            if ([arrPushAndVoiceNotification containsObject:[dicOtherUser stringValueForJSON:@"user_id"]])
                [arrPushAndVoiceNotification removeObject:[dicOtherUser stringValueForJSON:@"user_id"]];
            
            [arrPushAndVoiceNotification addObject:[dicOtherUser stringValueForJSON:@"user_id"]];
        }
    }
    
    if (isPlaySound)
    {
        [self setSoundForUserMovment:soundType];
        __weak typeof (self) weakSelf = self;
        self.configurePlaySpeechAndLocalPushNotification = ^(BOOL isSpeechAndPush)
        {
            if (isSpokenAudio || isSendPushNotification)
            {
                [weakSelf setVoiceAndPushNotification:strSpeechText Push:strPushText isSpoken:isSpokenAudio isPushNotification:isSendPushNotification];
            }
            else
            {
                [weakSelf playPauseSoundForSpeechAndPushNotification:NO];
            }
        };
    }
    else
    {
        [self setVoiceAndPushNotification:strSpeechText Push:strPushText isSpoken:isSpokenAudio isPushNotification:isSendPushNotification];
    }
    
}

-(void)playPauseSoundForSpeechAndPushNotification:(BOOL)isStop
{
    if (isStop)
    {
        // Stop Music Here
        
        // For Apple Music
        BOOL isTempPlaying = isAppleAudioPlaying;
        if (![self getMPMusicPlaySate])
        {
            [self mpMusicPlayerPayPause:NO];
            isAppleAudioPlaying = isTempPlaying;
        }
        
        // For Spotify Music
        if (appDelegate.player.playbackState.isPlaying)
            [appDelegate.player setVolume:0.5 callback:nil];
    }
    else
    {
        
        // For Spotify Music
        if (appDelegate.player.playbackState.isPlaying)
            [appDelegate.player setVolume:1 callback:nil];
        
        // For Apple Music
        if (isAppleAudioPlaying && !isFromSpotify)
            [self mpMusicPlayerPayPause:YES];
    }
}

// Set user movement Sound files here...
-(void)setSoundForUserMovment:(int)soundType
{
    if (soundType == 1 || soundType == 2)
    {
        [self playPauseSoundForSpeechAndPushNotification:YES];
    }
    
    // Play Audio file...
    switch (soundType)
    {
        case 1:
        {
            NSLog(@"Play login user sound ======= >>> ");
            [self playUserMovmentSound:YES];
        }
            break;
        case 2:
        {
            NSLog(@"Play other user sound ======= >>> ");
            [self playUserMovmentSound:NO];
        }
            break;
        default:
            break;
    }
}

// Set up for speech and local push notification..
-(void)setVoiceAndPushNotification:(NSString *)strVoiceSpeech Push:(NSString *)strPushNotification isSpoken:(BOOL)isSpokenAudio isPushNotification:(BOOL)isSendPushNotification
{
    // Voice speech
    if (isSpokenAudio)
    {
        NSLog(@"Play sopken audio ======== >> ");
        [self playPauseSoundForSpeechAndPushNotification:YES];
        
        dispatch_async(GCDMainThread, ^{
            AVSpeechUtterance *speechUtterance = [AVSpeechUtterance speechUtteranceWithString:strVoiceSpeech];
            speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate;
            speechUtterance.pitchMultiplier = 1;
            speechUtterance.volume = 1;
            [speechSynthesizer speakUtterance:speechUtterance];
            
            [objAgoraRTCVoiceKit setEnableSpeakerphone:NO];
            [objAgoraRTCVoiceKit setEnableSpeakerphone:YES];
        });
    }
    
    
    // Local Push notification
    if (isSendPushNotification && [[UIApplication sharedApplication] applicationState] != UIApplicationStateActive)
    {
        if (!isSpokenAudio)
            [self playPauseSoundForSpeechAndPushNotification:YES];
        
        // Send local push notification ..
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.body = [NSString localizedUserNotificationStringForKey:strPushNotification arguments:nil];
        content.sound = [UNNotificationSound defaultSound];
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"OneSecond" content:content trigger:trigger];
        /// 3. schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
            }
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (!isSpokenAudio)
                [self playPauseSoundForSpeechAndPushNotification:NO];
        });
    }
}

#pragma mark - AVSpeechSynthesizer Delegate Method
#pragma mark -
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    NSLog(@"Increase Music volume here ======== >> ");
    [self playPauseSoundForSpeechAndPushNotification:NO];
}


#pragma mark - AVAudioPlayer Delegate Method
#pragma mark -

-(void)playUserMovmentSound:(BOOL)isLoginUserPasses
{
    NSString *soundPath = isLoginUserPasses ? [[NSBundle mainBundle] pathForResource:CLoginUserPasses ofType:@"wav"] : [[NSBundle mainBundle] pathForResource:COtherUserPasses ofType:@"wav"];
    
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&setCategoryErr];
    [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundPath];
    self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    self.avAudioPlayer.numberOfLoops = 0; //Infinite
    self.avAudioPlayer.delegate = self;
    self.avAudioPlayer.volume = 1;
    [self.avAudioPlayer play];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"audioPlayerDidFinishPlaying ==== > >>>>>>>>>>");
    
    // Play and send local push notification
    if (self.configurePlaySpeechAndLocalPushNotification)
        self.configurePlaySpeechAndLocalPushNotification(YES);
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error
{
    NSLog(@"error is; :%@", error);
}


#pragma mark - Running Completed related Fuctions

// Move on result Screen
-(void)moveOnResultScreen:(BOOL)isResultScreen TimeOut:(BOOL)isTimeOut
{
    // Return If Allredy request to going for Result screen.
    if (isMovingOnResultScreen)
        return;
    
    isMovingOnResultScreen = YES;
    
    // Stop Runnig timer here........
    [TimerRunning invalidate];
    TimerRunning = nil;
    
    [self.motionManager stopMotionDetection];
    [self removeMQTTRunningNotificationObserver];
    [self disconnectAgoraVoiceChat];
    
    [mapView clearMGLMapView];
    
    if (isResultScreen) // Move on Result screen
    {
        // Call User disqualified api here....
        RunSummaryViewController *objRun = [[RunSummaryViewController alloc] initWithNibName:@"RunSummaryViewController" bundle:nil];
        objRun.strResultSessionId = strSubscribedSessionId;
        objRun.isRunnerTimerOut = isTimeOut;
        [self.navigationController pushViewController:objRun animated:YES];
    }
    else // Move on Session Screen...........
    {
        appDelegate.window.rootViewController = appDelegate.objTabBarController;
        [self.navigationController popToRootViewControllerAnimated:NO];
        [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab3];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        // Unsubccribe MQTT connection here.....
        if (strSubscribedSessionId)
            [appDelegate MQTTUnsubscribeWithTopic:strSubscribedSessionId];
        
        [appDelegate MQTTDisconnetFromServer];
        [self stopLocationUpdate];
        
        [updateTimer invalidate];
        updateTimer = nil;

        [appDelegate.appleMPMusicPlayerController pause];
        
        btnPlay.selected = NO;
        [appDelegate.player setIsPlaying:NO callback:^(NSError *error) {
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self clearAllDataFromScreen];
        });
    });
}

// Calculate User Runinng speed according to change user location......

#pragma mark - User Running Speed

-(void)startMotionDetectorToGetSpeedAndUserMovement {
    self.motionManager = [MotionManager new];
    [self.motionManager startMotionDetection];
}


#pragma mark - UICollectionView data source and delegate
#pragma mark -

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clMeter])
        return arrMeter.count;
    
    return arrTrackRunner.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clMeter])
        return CGSizeMake(CGRectGetWidth(clMeter.frame), 50);
    
//    return CGSizeMake(80, cnTrackViewHeight.constant);
    if (isCollectionViewNormalZoom) {
        return CGSizeMake(80, cnTrackViewHeight.constant);
    }
    else
        return CGSizeMake(CGRectGetWidth(clGraph.frame)/arrTrackRunner.count, cnTrackViewHeight.constant);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clMeter])
    {
        static NSString *identifier = @"MeterDistanceCell";
        MeterDistanceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        NSString *strDist = arrMeter[indexPath.item];
        if (strDist.floatValue >= 1000)
        {
            if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
            {
                cell.lblUnit.text = @"km";
                cell.lblDistance.text = [NSString stringWithFormat:@"%.1f",strDist.floatValue/1000];
            }
            else
            {
                cell.lblUnit.text = @"mi";
                float  kmData = strDist.floatValue / 1000;
                float miles = kmData/ 1.60934;
                cell.lblDistance.text = [NSString stringWithFormat:@"%.1f",miles];
            }
        }
        else
        {
            if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
            {
                cell.lblUnit.text = @"m";
                cell.lblDistance.text = strDist;
            }
            else
            {
                cell.lblUnit.text = @"mi";
                float  kmData = strDist.floatValue / 1000;
                float miles = kmData/ 1.60934;
                cell.lblDistance.text = [NSString stringWithFormat:@"%.1f",miles];
            }
        }
        
        return cell;
    }
    else
    {
        NSDictionary *dicData = arrTrackRunner[indexPath.row];
        static NSString *identifier = @"UserTrackCell";
        UserTrackCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        NSString *strDistance = [dicData valueForKey:@"total_distance"];
        
        if (isCollectionViewNormalZoom)
            [cell setUserUIIfCollectionViewZoom:NO cellSize:80 UserData:dicData];
        else
        {
            // Short Size
            float cellSize = CGRectGetWidth(clGraph.frame)/arrTrackRunner.count;
            [cell setUserUIIfCollectionViewZoom:YES cellSize:cellSize UserData:dicData];
        }
        
        [cell SetUserData:dicData];
        
        if (TriangleShapeLayer)
        {
            [TriangleShapeLayer removeFromSuperlayer];
            [clGraph.layer insertSublayer:TriangleShapeLayer atIndex:0];
        }
        
        float space = (strDistance.floatValue / runDistanceLength) * cnTrackViewHeight.constant;
        space = space > 50 ? space - cellViewUserHeight : 0;
        cell.cnViewBottomSpace.constant = space;
        
        [cell.btnUser touchUpInsideClicked:^{
            
            if (isCollectionViewNormalZoom)
            {
                if (TriangleShapeLayer)
                    [self removeTriangle];
                else
                    [self drawTriangleBetweenNearestUser:NO];
            }
        }];
        
        return cell;
    }
}


#pragma mark - ScrollView Delegate Method
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(!decelerate)
    {
//        NSLog(@"scrollViewDidEndDragging ============ ");
        if ([scrollView isEqual:clGraph])
            [self showTotalRuunersCountBubble];

    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt
{
    if ([scrollView isEqual:clGraph])
        [self showTotalRuunersCountBubble];
}

#pragma mark - Track Related Stuff
#pragma mark -

-(void)showTotalRuunersCountBubble
{
    NSArray *arrVisibleIndexPath = [clGraph indexPathsForVisibleItems];
    arrVisibleIndexPath = [arrVisibleIndexPath sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"item" ascending:YES]]];
    
    if(arrVisibleIndexPath.count > 0)
    {
        NSIndexPath *cellIndexPath = arrVisibleIndexPath.lastObject;
        if(cellIndexPath.item == arrTrackRunner.count -1)
        {
            // HideView here
            viewRunnerCount.hidden = YES;
        }
        else
        {
            // Show View here...
            float screenSpace = CScreenWidth-cellViewUserHeight;
            viewRunnerCount.hidden = arrTrackRunner.count*80 > screenSpace ? NO : YES;
            
            int totalUserCount = (int)arrTrackRunner.count;
            int lastUserVisible = (int)cellIndexPath.item+1;
            int remainingUser = totalUserCount - lastUserVisible;
            
            NSLog(@"Invisible users ===  === %d",remainingUser);
            lblUserCount.text = [NSString stringWithFormat:@"+%d",remainingUser];
        }
    }
}

-(void)moveMapAccordingToUserMovement
{
    [mapView clearMGLMapView];
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [mapView setCenterCoordinate:CLLocationCoordinate2DMake(lastLatitude, lastLongitude) zoomLevel:16 animated:NO];
}

-(void)calculateSideDistanceMeterRatio:(NSDictionary *)dicRunner
{
    runDistanceLength = [dicRunner doubleForKey:@"distance"];
    
    int i = 0;
    int totalDistnace = runDistanceLength;
    
    if (runDistanceLength > 10000)
    {
        // slot 500 meter
        i = 1000;
    }
    else if (runDistanceLength > 5000)
    {
        // slot 500 meter
        i = 500;
    }
    else if (runDistanceLength > 500)
    {
        i = 200;
    }
    else
    {
        // slote 20 meter
        i = 20;
    }
    
    
    [arrMeter removeAllObjects];
    while (totalDistnace >= i)
    {
        [arrMeter addObject:[NSString stringWithFormat:@"%d",totalDistnace]];
        totalDistnace = totalDistnace-i;
    }
    
    [clMeter reloadData];
    
    if(!isMeterDataLoaded)
    {
        isMeterDataLoaded = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [arrTrackRunner removeAllObjects];
            [arrTrackRunner addObjectsFromArray:[dicRunner valueForKey:@"runner"]];
            
            cnTrackViewHeight.constant = clMeter.contentSize.height;
            
            if (CGRectGetHeight(scrollView.frame) - cnTrackViewHeight.constant > 0)
                cnTrackViewTopSpace.constant = CGRectGetHeight(scrollView.frame) - cnTrackViewHeight.constant;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - CGRectGetHeight(scrollView.frame));
                [scrollView setContentOffset:CGPointMake(0, bottomOffset.y)];
                
                [clGraph reloadData];
                [self drawTriangleBetweenNearestUser:YES];
                scrollView.hidden = mapView.hidden = viewBasicDetail.hidden = lblTotalRunDistance.hidden = NO;
                
                [clGraph performBatchUpdates:^{
                } completion:^(BOOL finished) {
                    [self showTotalRuunersCountBubble];
                }];
            });
        });
    }
}


-(void)storeTrackRunnerFromServer:(NSDictionary *)dicRunner
{
    scrollView.hidden = mapView.hidden = viewBasicDetail.hidden = lblTotalRunDistance.hidden = NO;
    [self removeTriangle];
    
    [arrTrackRunner removeAllObjects];
    [arrTrackRunner addObjectsFromArray:[dicRunner valueForKey:@"runner"]];
    
    // Scroll to user position...
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [clGraph reloadData];
        [self moveGraphAccordingToUser];
        
        [clGraph performBatchUpdates:^{
        } completion:^(BOOL finished) {
            [self showTotalRuunersCountBubble];
        }];
    });
}

-(void)moveGraphAccordingToUser
{
    
    // For auto scroll to the user...
    NSDictionary *dicDataLogin = [self findLoginRunner:arrTrackRunner];
    
    double latitude = [dicDataLogin doubleForKey:@"latitude"];
    double longitude = [dicDataLogin doubleForKey:@"longitude"];
    
    if (trackMoveLatitude == latitude && trackMoveLongitude == longitude)
        return;
    
    
    NSString *strDistance = [dicDataLogin valueForKey:@"total_distance"];
    float space = (strDistance.floatValue / runDistanceLength) * cnTrackViewHeight.constant;
    space = space > 50 ? space - cellViewUserHeight : 0;
    
    float scrollContent = cnTrackViewHeight.constant - (space+cellViewUserHeight);
    if (strDistance.integerValue > 0)
    {
        if(cnTrackViewHeight.constant <= CGRectGetHeight(scrollView.frame))
        {
//                        NSLog(@"Scroll view Height ====== >>>>>>%f",cnTrackViewHeight.constant);
//                            NSLog(@"Scroll view content Offset 00000000000000 ====== >>>>>>%f",scrollView.contentOffset.y);
        }
        else if (scrollContent < (cnTrackViewHeight.constant - CGRectGetHeight(scrollView.frame)))
        {
            dispatch_async(GCDMainThread, ^{
                [scrollView setContentOffset:CGPointMake(0, scrollContent) animated:YES];
            });

        }
        else
        {
            dispatch_async(GCDMainThread, ^{
                [scrollView setContentOffset:CGPointMake(0, cnTrackViewHeight.constant - CGRectGetHeight(scrollView.frame)) animated:YES];
            });
        }
    }
    
    trackMoveLatitude = [dicDataLogin doubleForKey:@"latitude"];
    trackMoveLongitude = [dicDataLogin doubleForKey:@"longitude"];
}

-(void)drawTriangleBetweenNearestUser:(BOOL)isScroll
{
    if (arrTrackRunner.count < 1) // If No runner found....
        return;
    
    NSMutableArray *arrLogin = [[NSMutableArray alloc] initWithArray:arrTrackRunner];
    NSArray *sortedArray = [self sortRunnerAccordingToTotalDistance:arrLogin.mutableCopy Accending:YES];
    
    // SORT ARRAY
    [arrLogin removeAllObjects];
    [arrLogin addObjectsFromArray:sortedArray];
    
    
    // GET LOGIN USER
    NSDictionary *dicDataLogin = [self findLoginRunner:arrLogin];
    
    if (dicDataLogin.allKeys.count > 0)
    {
        if ([arrLogin containsObject:dicDataLogin])
        {
            NSUInteger loginIndex = [arrLogin indexOfObject:dicDataLogin];
            
            if (isScroll)
            {
                // Scroll Collection view at login user state for fist time only.......
                NSUInteger loginSelIndex = [arrTrackRunner indexOfObject:dicDataLogin];
                NSIndexPath *loginIndexPath = [NSIndexPath indexPathForItem:loginSelIndex inSection:0];
                [clGraph scrollToItemAtIndexPath:loginIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
            }
            else
            {
                
                if (loginIndex > 1)
                {
                    CGPoint colletionContentOffset = clGraph.contentOffset;
                    
                    NSIndexPath *loginIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                    [clGraph scrollToItemAtIndexPath:loginIndexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSDictionary *dicFirstUesr = arrLogin[loginIndex-1];
                        NSDictionary *dicSecondUesr = arrLogin[loginIndex-2];
                        
                        NSIndexPath *loginUserIndexPath = [NSIndexPath indexPathForItem:[arrTrackRunner indexOfObject:dicDataLogin] inSection:0];
                        NSIndexPath *firstUserIndexpath = [NSIndexPath indexPathForItem:[arrTrackRunner indexOfObject:dicFirstUesr] inSection:0];
                        NSIndexPath *secondUserIndexpath = [NSIndexPath indexPathForItem:[arrTrackRunner indexOfObject:dicSecondUesr] inSection:0];
                        
                        
                        // Don't create tringle if user distance more 30 miles....
                        float firstUserDif = ([dicDataLogin doubleForKey:@"total_distance"] - [dicFirstUesr doubleForKey:@"total_distance"]) * 0.000621371;
                        float secondUserDif = ([dicDataLogin doubleForKey:@"total_distance"] - [dicSecondUesr doubleForKey:@"total_distance"]) * 0.000621371;
                        
                        if ( firstUserDif >= 30 ||  secondUserDif >= 30)
                            return ;
                        
                        
                        
                        UIBezierPath *path = [UIBezierPath bezierPath];
                        CGPoint startPoint = CGPointMake(0, 0);
                        if (loginUserIndexPath)
                        {
                            CGRect cellFrameInSuperview = [clGraph convertRect:[clGraph layoutAttributesForItemAtIndexPath:loginUserIndexPath].frame toView:[clGraph superview]];
                            
                            NSString *strDistance = [dicDataLogin valueForKey:@"total_distance"];
                            float space = (strDistance.floatValue / runDistanceLength) * cnTrackViewHeight.constant;
                            space = space > 50 ? space - cellViewUserHeight : 0;
                            space = cnTrackViewHeight.constant - (space + cellViewUserHeight);
                            startPoint = CGPointMake(cellFrameInSuperview.origin.x + cellViewUserX - collectionViewTrellingSpace + cellViewUserWidth/2, space + cellViewUserHeight/2);
                        }
                        
                        CGPoint secondPoint = CGPointMake(0, 0);
                        if (firstUserIndexpath)
                        {
                            CGRect cellFrameInSuperview = [clGraph convertRect:[clGraph layoutAttributesForItemAtIndexPath:firstUserIndexpath].frame toView:[clGraph superview]];
                            NSString *strDistance = [dicFirstUesr valueForKey:@"total_distance"];
                            float space = (strDistance.floatValue / runDistanceLength) * cnTrackViewHeight.constant;
                            space = space > 50 ? space - cellViewUserHeight : 0;
                            space = cnTrackViewHeight.constant - (space + cellViewUserHeight);
                            secondPoint = CGPointMake(cellFrameInSuperview.origin.x + cellViewUserX - collectionViewTrellingSpace + cellViewUserWidth/2, space + cellViewUserHeight/2);
                        }
                        
                        CGPoint thirdPoint = CGPointMake(0, 0);
                        if (secondUserIndexpath)
                        {
                            CGRect cellFrameInSuperview = [clGraph convertRect:[clGraph layoutAttributesForItemAtIndexPath:secondUserIndexpath].frame toView:[clGraph superview]];
                            
                            NSString *strDistance = [dicSecondUesr valueForKey:@"total_distance"];
                            float space = (strDistance.floatValue / runDistanceLength) * cnTrackViewHeight.constant;
                            space = space > 50 ? space - cellViewUserHeight : 0;
                            space = cnTrackViewHeight.constant - (space + cellViewUserHeight);
                            thirdPoint = CGPointMake(cellFrameInSuperview.origin.x + cellViewUserX - collectionViewTrellingSpace + cellViewUserWidth/2, space + cellViewUserHeight/2);
                        }
                        
                        [clGraph setContentOffset:colletionContentOffset animated:NO];
                        
                        [path moveToPoint:startPoint];
                        [path addLineToPoint:secondPoint];
                        [path addLineToPoint:thirdPoint];
                        [path addLineToPoint:startPoint];
                        
                        UILabel *lblDis1 = [[UILabel alloc] initWithFrame:CGRectMake((startPoint.x+secondPoint.x)/2-30, (startPoint.y+secondPoint.y)/2-13, 60, 26)];
                        lblDis1.backgroundColor = CBlueColor;
                        lblDis1.layer.cornerRadius = CGRectGetHeight(lblDis1.frame)/2;
                        lblDis1.layer.borderWidth = 2;
                        lblDis1.layer.masksToBounds = YES;
                        lblDis1.textAlignment = NSTextAlignmentCenter;
                        lblDis1.textColor = CRGB(21, 23, 40);
                        [lblDis1 setFont:CFontSolidoCondensedMedium(13)];
                        lblDis1.text = [NSString stringWithFormat:@"%.1f mi",firstUserDif];
                        [clGraph addSubview:lblDis1];
                        
                        UILabel *lblDis2 = [[UILabel alloc] initWithFrame:CGRectMake((startPoint.x+thirdPoint.x)/2 - 30, (startPoint.y+thirdPoint.y)/2-13, 60, 26)];
                        lblDis2.backgroundColor = CBlueColor;
                        lblDis2.layer.cornerRadius = CGRectGetHeight(lblDis2.frame)/2;
                        lblDis2.layer.borderWidth = 2;
                        lblDis2.layer.masksToBounds = YES;
                        lblDis2.textAlignment = NSTextAlignmentCenter;
                        lblDis2.textColor = CRGB(21, 23, 40);
                        [lblDis2 setFont:CFontSolidoCondensedMedium(13)];
                        lblDis2.text = [NSString stringWithFormat:@"%.1f mi",secondUserDif];
                        [clGraph addSubview:lblDis2];
                        
                        
                        TriangleShapeLayer = [CAShapeLayer layer];
                        TriangleShapeLayer.path = [path CGPath];
                        TriangleShapeLayer.strokeColor = CBlueColor.CGColor;
                        TriangleShapeLayer.lineWidth = 3.0;
                        TriangleShapeLayer.fillColor = [[UIColor clearColor] CGColor];
                        [clGraph.layer insertSublayer:TriangleShapeLayer atIndex:0];
                        
                    });
                }
            }
        }
        
    }
    
}

-(void)removeTriangle
{
    if (TriangleShapeLayer)
    {
        [TriangleShapeLayer removeFromSuperlayer];
        TriangleShapeLayer = nil;
        
        for (UIView *obj in clGraph.subviews)
        {
            if ([obj isKindOfClass:[UILabel class]])
                [obj removeFromSuperview];
        }
    }
}

- (void)zoomInOutCollectionView
{
    NSLog(@"zoomInOutCollectionView ===== ");
}


#pragma mark - Camera Related Functions

-(void)openCamera
{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage,(NSString *) kUTTypeMovie];
        imagePicker.allowsEditing = NO;
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        imagePicker.videoMaximumDuration = 10;
        imagePicker.navigationBar.barTintColor = CThemeColor;
        imagePicker.navigationBar.tintColor = [UIColor whiteColor];
        imagePicker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." preferredStyle:UIAlertControllerStyleAlert];
        
        [alertView addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:NO completion:^{
        if([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"])
        {
            VideoCropViewController *objVideoCrop = [[VideoCropViewController alloc] initWithVideo:[info objectForKey:UIImagePickerControllerMediaURL]];
            objVideoCrop.delegate = self;
            [self.navigationController presentViewController:objVideoCrop animated:YES completion:nil];
        }
        else
        {
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            MIImageCropViewController *objCrop = [[MIImageCropViewController alloc] initWithImage:image withDelegate:self];
            [self.navigationController presentViewController:objCrop animated:YES completion:nil];
        }
    }];
}
- (void)saveCaptureImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo
{
    if (error)
    {
        clProgress.hidden = YES;
        NSLog(@"Error while saving image in library ==== ");
    }
    else
    {
        [self getAllPhotosFromCameraIsFromSave:YES];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)askForGalaryPermission
{
    [self getAllPhotosFromCameraIsFromSave:NO];
}


-(void)getAllPhotosFromCameraIsFromSave:(BOOL)isSaved
{
    dispatch_async(dispatch_get_main_queue(), ^{
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.sortDescriptors =@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        PHFetchResult *resultAsset = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
        
        NSMutableOrderedSet *recentsDataSource = [[NSMutableOrderedSet alloc]init];
        
        for (PHAsset *asset in resultAsset)
        {
            [recentsDataSource addObject:asset];
        }
        
        NSArray *array = [recentsDataSource sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]]];
        [recentsDataSource removeAllObjects];
        [recentsDataSource addObjectsFromArray:array];
        
        
        if (isSaved)
        {
            if (resultAsset.count > 0)
            {
                PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
                requestOptions.resizeMode   = PHImageRequestOptionsResizeModeNone;
                requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                requestOptions.synchronous = true;
                
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestImageForAsset:[resultAsset objectAtIndex:0] targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFill options:requestOptions resultHandler:^void(UIImage *image, NSDictionary *info)
                 {
                     clProgress.hidden = YES;
                     [self pushOnPhotoVideoPreviewScreen:image Asset:[resultAsset objectAtIndex:0]];
                 }];
            }
        }
        else
        {
            clProgress.hidden = YES;
        }
        
    });
}

-(void)pushOnPhotoVideoPreviewScreen:(UIImage *)image Asset:(PHAsset *)selectedAsset
{
    clProgress.hidden = YES;
    
    PhotoVideoPreviewController *objPhotoVideo = [[PhotoVideoPreviewController alloc]initWithNibName:@"PhotoVideoPreviewController" bundle:nil];
    objPhotoVideo.asset = selectedAsset;
    
    if (image)
        objPhotoVideo.imgCapture = image;
    
    NSMutableDictionary *dicWaterMarkData = [NSMutableDictionary new];
    [dicWaterMarkData setObject:lblTime.text forKey:@"time"];
    [dicWaterMarkData setObject:lblAverageSpeed.text forKey:@"pace"];
    [dicWaterMarkData setObject:[NSString stringWithFormat:@"%@ %@",lblTotalRunDistance.text,lblDistanceUnit.text] forKey:@"distance"];
    [dicWaterMarkData setObject:lblCurrentPosstion.text forKey:@"rank"];
    objPhotoVideo.dicPhotoVideoWaterMark = dicWaterMarkData;
    objPhotoVideo.strPreviewMediaSessionID = strSubscribedSessionId;
    objPhotoVideo.mediaLocation = currentLocation;
    objPhotoVideo.configureBtnBackCLK = ^(BOOL isBack)
    {
        if (isBack)
            [self openCamera];
    };
    
    [self.navigationController pushViewController:objPhotoVideo animated:YES];
    
}

#pragma mark - Crop Delegate Methods
- (void)finishCroping:(PHAsset *)asset didCancel:(BOOL)cancel;
{
    clProgress.hidden = YES;
    if (cancel)
        [self openCamera];
    else
        [self pushOnPhotoVideoPreviewScreen:nil Asset:asset];
}

- (void)finishImageCroping:(UIImage *)image didCancel:(BOOL)cancel;
{
    clProgress.hidden = YES;
    if (cancel)
        [self openCamera];
    else
    {
        [self.view bringSubviewToFront:clProgress];
        clProgress.hidden = NO;
        clProgress.progressHUD.labelText = @"Saving Photo...";
        UIImageWriteToSavedPhotosAlbum(image,self,@selector(saveCaptureImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:),nil);

    }
}

#pragma mark - Action Event
#pragma mark -
-(IBAction)btnMapLocationCLK:(id)sender
{
    SelectMusicViewController *objSelect = [[SelectMusicViewController alloc] init];
    [self.navigationController pushViewController:objSelect animated:YES];
}

-(IBAction)btnQuitRace:(id)sender
{
    isQuitRace = YES;
    
    QuitRunSession *objQuite = [QuitRunSession viewFromXib];
    [self presentViewOnPopUpViewController:objQuite shouldClickOutside:NO dismissed:nil];
    
    [objQuite.btnNo touchUpInsideClicked:^{
        [self dismissOverlayViewControllerAnimated:YES completion:nil];
    }];
    
    [objQuite.btnYes touchUpInsideClicked:^{
    
        [self.motionManager stopMotionDetection];
        [self stopLocationUpdate];
        NSString *strTopic = [NSString stringWithFormat:@"run_%@",strSubscribedSessionId];
        [appDelegate.objMQTTClient unsubscribe:strTopic withCompletionHandler:nil];
        
        [TimerRunning invalidate];
        TimerRunning = nil;
        
        // Quit by user...
        [self completeQuitSession:YES RunCompleteType:@"2" completed:^(id responseObject, NSError *error)
         {
             if (!error)
             {
                 [self dismissOverlayViewControllerAnimated:YES completion:^{
                     [self moveOnResultScreen:YES TimeOut:NO];
                 }];
             }
             else
             {
                 [self customAlertViewWithOneButton:@"" Message:CMessageApiRequestTimeOut ButtonText:@"Retry" Animation:YES completed:^{
                     [objQuite.btnYes sendActionsForControlEvents:UIControlEventTouchUpInside];
                 }];
             }
         }];
    }];
    
}

-(IBAction)btnLockCLK:(id)sender
{
    viewLock.hidden = NO;
    viewUnlock.userInteractionEnabled = YES;
}

-(IBAction)btnSelectedMusic:(id)sender
{
    SelectMusicViewController *objSelect = [[SelectMusicViewController alloc] init];
    [self.navigationController pushViewController:objSelect animated:YES];
}

-(IBAction)btnCamera:(id)sender
{
    [self openCamera];
}

-(IBAction)btnMusicListCLK:(id)sender
{
    if([lblSongName.text isEqualToString:@"Loading..."])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:@"Please wait! Still it's loading the track.."];
        return;
    }
    
    isMusicScreen = YES;
    
    RunningMusicViewController *objRuning = [[RunningMusicViewController alloc] initWithNibName:@"RunningMusicViewController" bundle:nil];
    
    // If song is allready playing from Previous screen........
    objRuning.configureGetMusicData = ^(int isMusicNum, int currentTime, BOOL isSongPlaying, BOOL Repeat, BOOL Shuffle)
    {
        // Play music if this class is Visible......
        if ([[appDelegate getTopMostViewController] isKindOfClass:[RunningViewController class]])
        {
            currentplayMusicNo = isMusicNum;
            btnPlay.selected = isSongPlaying;
            isShuffle = Shuffle;
            isRepeat = Repeat;
            btnRepeat.selected = isRepeat;
            btnShuffle.selected = isShuffle;
            
            if (isSongPlaying)
            {
                [self songAllreadyPlaying:isMusicNum CurrentTime:currentTime+0.5];
            }
            
            if (isFromSpotify)
                [self updateUI];
        }
    };
    
    objRuning.currentplayMusicNo = currentplayMusicNo;
    objRuning.arrMusicList = arrMusic;
    objRuning.isSongPlaying = btnPlay.selected;
    objRuning.isRepeatMusic = isRepeat;
    objRuning.isShuffleMusic = isShuffle;
    [self.navigationController presentViewController:objRuning animated:YES completion:nil];
    
}

-(IBAction)btnMusicCntrolCLK:(UIButton *)sender
{
    if (isFromSpotify)
    {
        if([lblSongName.text isEqualToString:@"Loading..."])
        {
            [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:@"Please wait! Still it's loading the track.."];
            return;
        }
    }
    
    switch (sender.tag)
    {
        case 0: //Repeat
        {
            btnRepeat.selected = !btnRepeat.selected;
            isRepeat = btnRepeat.selected;
            
            if (!isFromSpotify)
            {
                if (isRepeat)
                    appDelegate.appleMPMusicPlayerController.repeatMode =isRepeat ? MPMusicRepeatModeOne : MPMusicRepeatModeNone;
            }
            
        }
            break;
        case 1: //Play Previous
        {
            if (currentplayMusicNo > 0)
            {
                currentplayMusicNo--;
                
                if (isFromSpotify)
                {
                    [self shuffleSpotifyMusic];
                    return;
                }
                
                // If Suffle is ON....
                
                if (isShuffle)
                    [self getRandomTrackNo];
                
                
                [self mpMusicPlayerPayPause:YES];
                [appDelegate.appleMPMusicPlayerController skipToPreviousItem];
                [self playItunesMusic];
            }
        }
            break;
        case 2: //Play
        {
            if (isFromSpotify)
            {
                if([lblSongName.text isEqualToString:@"Loading..."])
                {
                    [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:@"Please wait! Still it's loading the track.."];
                    return;
                }
                
                [appDelegate.player setIsPlaying:!appDelegate.player.playbackState.isPlaying callback:nil];
                btnPlay.selected = !appDelegate.player.playbackState.isPlaying;
                
                return;
            }
            if ([self getMPMusicPlaySate])
                [self mpMusicPlayerPayPause:YES];
            else
                [self mpMusicPlayerPayPause:NO];
            
        }
            break;
            
        case 3: //Play Next
        {
            if (arrMusic.count-1 > currentplayMusicNo)
                currentplayMusicNo++;
            else
                currentplayMusicNo = 0;
            
            btnPlay.selected = YES;
            
            if (isFromSpotify)
            {
                [self shuffleSpotifyMusic];
                return;
            }
            
            // If Suffle is ON.....
            
            if (isShuffle)
                [self getRandomTrackNo];
            
            [self mpMusicPlayerPayPause:YES];
            [appDelegate.appleMPMusicPlayerController skipToNextItem];
            [self playItunesMusic];
            
        }
            break;
            
        case 4: //Shuffle
        {
            btnShuffle.selected = !btnShuffle.selected;
            isShuffle = btnShuffle.selected;
            
            if (!isFromSpotify)
            {
                if (isShuffle)
                    appDelegate.appleMPMusicPlayerController.shuffleMode =isShuffle ? MPMusicShuffleModeSongs : MPMusicShuffleModeOff;
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)setTimeOnSpotifyMusic
{
    if(appDelegate.isChangingProgress)
        return;
    
    if(isForwardStarted)
        return;
    
    NSTimeInterval currentDuration = appDelegate.player.playbackState.position;
    
    sliderMusic.value = currentDuration;
    [self playerUpdateReachTime];
    
    if(isRepeat && roundf(appDelegate.player.playbackState.position) == roundf(appDelegate.player.metadata.currentTrack.duration) - 1)
    {
        isForwardStarted = YES;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [appDelegate.player seekTo:0.0 callback:^(NSError *error) {
                [appDelegate.player setIsPlaying:YES callback:nil];
                [self setTimeOnSpotifyMusic];
                isForwardStarted = NO;
            }];
        });
    }
    else if(isRepeat && roundf(appDelegate.player.playbackState.position) == roundf(appDelegate.player.metadata.currentTrack.duration) - 1)
    {
        [self shuffleSpotifyMusic];
    }
}

- (void)updateUI
{
    if (arrMusic.count <= 0)
        return;
    
    if (currentplayMusicNo > arrMusic.count-1)
        return;
    
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    TblMusicList *objMusic = [arrMusic objectAtIndex:currentplayMusicNo];

    [SPTTrack trackWithURI: [NSURL URLWithString:objMusic.song_url]
               accessToken:auth.session.accessToken
                    market:nil
                  callback:^(NSError *error, SPTTrack *track)
     {
         sliderMusic.maximumValue = appDelegate.player.metadata.currentTrack.duration;
         
         [self setTimeOnSpotifyMusic];
         
         if (appDelegate.player.metadata.currentTrack.artistName && appDelegate.player.metadata.currentTrack.name)
         {
             lblSongName.text = [NSString stringWithFormat:@"%@ %@", appDelegate.player.metadata.currentTrack.artistName, appDelegate.player.metadata.currentTrack.name];
             NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lblSongName.attributedText];
             [text addAttribute: NSFontAttributeName value:CFontGilroyBold(14) range: NSMakeRange(0,appDelegate.player.metadata.currentTrack.artistName.length)];
             lblSongName.attributedText = text;
         }
         
         objMusic.song_name = appDelegate.player.metadata.currentTrack.name;
         objMusic.artist_name = appDelegate.player.metadata.currentTrack.artistName;
         [[[Store sharedInstance] mainManagedObjectContext] save];
     }];
}

- (void)updateSongName
{
    TblMusicList *objMusic = [arrMusic objectAtIndex:currentplayMusicNo];
    
    if (objMusic.artist_name && objMusic.song_name)
    {
        lblSongName.text = [NSString stringWithFormat:@"%@ %@", objMusic.artist_name, objMusic.song_name];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lblSongName.attributedText];
        [text addAttribute: NSFontAttributeName value:CFontGilroyBold(14) range: NSMakeRange(0,objMusic.artist_name.length)];
        lblSongName.attributedText  = text;
    }
}

- (void)shuffleSpotifyMusic
{
    if(isShuffle)
    {
        int randomInt1 = arc4random() % [arrMusic count];
        currentplayMusicNo = randomInt1;
    }
    
    [self setCurrentSongUI:YES];
}

- (void)setCurrentSongUI:(BOOL)shouldStartSong
{
    if(shouldStartSong)
    {
        [appDelegate.player playSpotifyURI:appDelegate.strMainURI startingWithIndex:currentplayMusicNo startingWithPosition:0 callback:^(NSError *error)
         {
             if (error != nil)
             {
                 NSLog(@"*** failed to play: %@", error);
                 return;
             }
             else
                 isShuffleSongStarted = YES;
         }];
    }
}

- (BOOL)isFromSpotify
{
    if (arrMusic.count > 0)
    {
        TblMusicList *objMusic = (TblMusicList *)[arrMusic objectAtIndex:0];
        return !objMusic.isApple.boolValue;
    }
    
    return YES;
}

@end
