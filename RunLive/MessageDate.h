//
//  MessageDate.h
//  RunLive
//
//  Created by mac-0004 on 03/11/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageDate : UITableViewHeaderFooterView

@property(strong,nonatomic) IBOutlet UILabel *lblDate;

@end
