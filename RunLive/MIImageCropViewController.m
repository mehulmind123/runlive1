//
//  MIImageCropViewController.m
//  RunLive
//
//  Created by mac-0005 on 19/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MIImageCropViewController.h"
#import "MIImageCropScrollView.h"

@interface MIImageCropViewController ()

@property (nonatomic, strong) UIImage *originalImage;

@end



@implementation MIImageCropViewController



- (id)initWithImage:(UIImage *)originalImage withDelegate:(id<MIImageCustomCropDelegate>)delegate
{
    self = [super initWithNibName:@"MIImageCropViewController" bundle:nil];
    
    if (self)
    {
        self.delegate = delegate;
        self.originalImage = [self fixOrientation:originalImage];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnCrop.layer.cornerRadius = btnCancel.layer.cornerRadius = 3;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self displayImageInScrollView:self.originalImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (UIImage *)fixOrientation:(UIImage *)srcImg
{
    if (srcImg.imageOrientation == UIImageOrientationUp) return srcImg;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, srcImg.size.width, srcImg.size.height,
                                             CGImageGetBitsPerComponent(srcImg.CGImage), 0,
                                             CGImageGetColorSpace(srcImg.CGImage),
                                             CGImageGetBitmapInfo(srcImg.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (srcImg.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.height,srcImg.size.width), srcImg.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.width,srcImg.size.height), srcImg.CGImage);
            break;
    }
    
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark - Image Crop View Methods
-(void)displayImageInScrollView:(UIImage *)image
{
    [scrollView imageToDisplay:image];
    btnZoom.hidden = [self isSquareImage:image];
}

-(BOOL)isSquareImage:(UIImage *)image
{
    return image.size.width == image.size.height;
}

-(UIImage *)captureVisibleRect
{
    CGRect croprect = CGRectZero;
    float xOffset = (scrollView.imageView.image.size.width) / scrollView.contentSize.width;
    float yOffset = (scrollView.imageView.image.size.height) / scrollView.contentSize.height;
    
    croprect.origin.x = scrollView.contentOffset.x * xOffset;
    croprect.origin.y = scrollView.contentOffset.y * yOffset;
    
    float normalizedWidth = (scrollView.frame.size.width) / (scrollView.contentSize.width);
    float normalizedHeight = (scrollView.frame.size.height) / (scrollView.contentSize.height);
    
    
    croprect.size.width = scrollView.imageView.image.size.width * normalizedWidth;
    croprect.size.height = scrollView.imageView.image.size.height * normalizedHeight;
    
    CGImageRef imageRef = scrollView.imageView.image.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, croprect);
    UIImage* cropedImage = [UIImage imageWithCGImage:subImageRef];
    
    
    return cropedImage;
    
}

#pragma mark - Action Event

-(IBAction)btnZoomCLK:(id)sender
{
    [scrollView zoom];
}

-(IBAction)btnCropCLK:(id)sender
{
    if (self.delegate)
        [self.delegate finishImageCroping:[self captureVisibleRect] didCancel:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnCancelCLK:(id)sender
{
    if (self.delegate)
        [self.delegate finishImageCroping:nil didCancel:YES];

    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
