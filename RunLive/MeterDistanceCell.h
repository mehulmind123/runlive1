//
//  MeterDistanceCell.h
//  Krishna_MapGraph
//
//  Created by mac-00012 on 7/1/17.
//  Copyright © 2017 mac-00012. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeterDistanceCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UILabel *lblDistance,*lblUnit;

@end
