//
//  CustomAlertView.m
//  RunLive
//
//  Created by mac-00012 on 5/10/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "CustomAlertView.h"

@implementation CustomAlertView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    
}

-(void)initWithTitle:(NSString *)title message:(NSString *)message firstButton:(NSString *)btnFirst secondButton:(NSString *)btnSecond
{
    [self.lblTitle setText:title];
    [self.lblTitle setPreferredMaxLayoutWidth:CViewWidth(self) - 16];
    [self.lblTitle setNeedsUpdateConstraints];
    
    [self.lblMessage setText:message];
    [self.lblMessage setPreferredMaxLayoutWidth:CViewWidth(self) - 52];
    [self.lblMessage setNeedsUpdateConstraints];
    
    [self.btnFirst setTitle:btnFirst forState:UIControlStateNormal];
    [self.btnSecond setTitle:btnSecond forState:UIControlStateNormal];

    
    CGFloat vHeight = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CViewSetHeight(self, vHeight);
    
    self.layer.cornerRadius = self.btnFirst.layer.cornerRadius = self.btnSecond.layer.cornerRadius = 3;
    self.center = CScreenCenter;
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.center = CScreenCenter;
}

@end
