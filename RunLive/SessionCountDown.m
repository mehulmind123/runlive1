//
//  SessionCountDown.m
//  RunLive
//
//  Created by mac-00012 on 6/27/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "SessionCountDown.h"

@implementation SessionCountDown
{
    AVPlayer *player;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    // Video setup for count down...
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:@"Counting" ofType:@"mp4"];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath] ;
        player = [AVPlayer playerWithURL:movieURL]; //
        player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        AVPlayerLayer *avLayer = [AVPlayerLayer layer];
        [avLayer setPlayer:player];
//        [avLayer setFrame:CGRectMake(0, 0, viewVideo.frame.size.width,viewVideo.frame.size.height)];
        [avLayer setFrame:CGRectMake(0, 0, CScreenWidth,CScreenHeight)];
        [avLayer setBackgroundColor:[UIColor clearColor].CGColor];
        [avLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        player.muted = NO;
        [self.layer addSublayer:avLayer];
        [player play];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
    });
}

#pragma mark - AVPlayer Delegate
- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    NSLog(@"playerItemDidReachEnd ============= >>>>>>");
    [self moveOnRunningScreen];
}


#pragma mark - Move On Runnig Screen screen
-(void)moveOnRunningScreen
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [player pause];
    player = nil;
    
    appDelegate.window.rootViewController = appDelegate.objSuperRunning;
    [self removeFromSuperview];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        UIViewController *viewController = [appDelegate getTopMostViewController];
        [viewController.navigationController popToRootViewControllerAnimated:NO];
        
        if (appDelegate.configureRunningSessionStart)
            appDelegate.configureRunningSessionStart(self.strSessionId);
    });
}


@end
