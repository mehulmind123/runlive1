//
//  RunningMusicCell.h
//  RunLive
//
//  Created by mac-0006 on 22/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunningMusicCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgSongCoverPic;

@end
