//
//  IntroCollectionViewCell.m
//  RunLive
//
//  Created by mac-0008 on 24/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "IntroCollectionViewCell.h"

@implementation IntroCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialization];
}

-(void)initialization
{
    [self.lblTitle setFont:Is_iPhone_5?CFontSolidoCondensedBold(21.0):CFontSolidoCondensedBold(24.0)];
    self.lblTitle.textColor = self.lblDetails.textColor = CRGB(255, 255, 255);
    [self.lblDetails setFont:Is_iPhone_5?CFontGilroyRegular(12.0):CFontGilroyRegular(14.0)];
    
    if (Is_iPhone_5) {
        self.cnImgCryptoWidth.constant = self.cnImgCryptoHieght.constant = 20;
        self.cnImgCryptoY.constant = 0;
    }
    else
    {
        self.cnImgCryptoWidth.constant = self.cnImgCryptoHieght.constant = 25;
        self.cnImgCryptoY.constant = 0;
    }
    
}

@end
