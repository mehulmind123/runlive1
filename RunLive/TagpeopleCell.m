//
//  TagpeopleCell.m
//  RunLive
//
//  Created by mac-0004 on 27/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "TagpeopleCell.h"

@implementation TagpeopleCell

@synthesize vwBorder,imgDP;
- (void)awakeFromNib {
    [super awakeFromNib];
    [appDelegate roundedImageView:imgDP withSuperView:vwBorder];
    vwBorder.layer.borderColor = [CImageOuterBorderColor CGColor];
    vwBorder.layer.borderWidth = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
