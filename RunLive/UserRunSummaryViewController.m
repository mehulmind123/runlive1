//
//  UserRunSummaryViewController.m
//  RunLive
//
//  Created by mac-0006 on 21/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "UserRunSummaryViewController.h"
#import "MyActivityResultCollectionViewCell.h"
#import "ResultRunCategoryCell.h"
#import "ResultRunnerCell.h"
#import "RunSummaryResultCell.h"

#define ViewGraphHeight 207
#define CGraphStartMinValue 5


@interface UserRunSummaryViewController ()
{
    NSArray *arryMyData;
    NSIndexPath *selectedCategoryIndexpath;
}

@end

@implementation UserRunSummaryViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    selectedCategoryIndexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [clUserRunDetail registerNib:[UINib nibWithNibName:@"MyActivityResultCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MyActivityResultCollectionViewCell"];
    [clUserRunDetail registerNib:[UINib nibWithNibName:@"RunSummaryResultCell" bundle:nil] forCellWithReuseIdentifier:@"RunSummaryResultCell"];
    [clCategory registerNib:[UINib nibWithNibName:@"ResultRunCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"ResultRunCategoryCell"];
    [clRunners registerNib:[UINib nibWithNibName:@"ResultRunnerCell" bundle:nil] forCellWithReuseIdentifier:@"ResultRunnerCell"];
    
    [self getUserRunDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [mapView clearMGLMapView];
  
}

#pragma mark - User Details

-(void)getUserRunDetails
{
    if (!self.strSelectedUserId)
        return;
    
    
    // Session Detail
    NSDate *date = [self convertDateFromString:[self.dicSession stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
    NSDateFormatter *dtFrom = [NSDateFormatter initWithDateFormat:@"dd MMM yyyy"];
    NSString *strDate = [dtFrom stringFromDate:date];
    lblDate.text = strDate;
    
    
    // Find user...
    NSArray *arrUser = [self.arrRunners filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", self.strSelectedUserId]];
    NSDictionary *dicUserDetail;
    if (arrUser.count > 0)
        dicUserDetail = arrUser[0];
    
    
    NSString *strTitle = [NSString stringWithFormat:@"%@'s RUN",[dicUserDetail stringValueForJSON:@"user_name"]];
    lblTitleName.text = [strTitle uppercaseString];
    lblLocality.text = [dicUserDetail stringValueForJSON:@"locality"];
    lblSubLocality.text = [dicUserDetail stringValueForJSON:@"sub_locality"];
    
    NSDictionary *dicUserComp = [dicUserDetail valueForKey:@"completed"];
    
    [self setupForMapView:UIEdgeInsetsMake(50, 30, 50, 30) userData:dicUserDetail] ;
    
    NSMutableArray *arrUserDataTemp = [NSMutableArray new];
    [arrUserDataTemp addObject:@{@"image":@"time",@"result":[dicUserComp stringValueForJSON:@"total_time"],@"quantities":@"TIME",@"unit":@""}];
    
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
        [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicUserComp stringValueForJSON:@"total_distance"].floatValue/1000],@"quantities":@"DISTANCE",@"unit":@"km"}];
    else
        [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicUserComp stringValueForJSON:@"total_distance"].floatValue * 0.000621371],@"quantities":@"DISTANCE",@"unit":@"miles"}];

    [arrUserDataTemp addObject:@{@"image":@"cal",@"result":[appDelegate convertCaloriesWithSelectedFormate:[dicUserComp stringValueForJSON:@"total_calories"]] ,@"quantities":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"EST KCALS" : @"EST CALORIES",@"unit":@"cal"}];

    [arrUserDataTemp addObject:@{@"image":@"speed",@"result":[appDelegate convertSpeedInWithSelectedFormate:[dicUserComp stringValueForJSON:@"total_distance"].floatValue overTime:[NSString stringWithFormat:@"%@",[dicUserComp stringValueForJSON:@"total_time"]]],@"quantities":@"PACE",@"unit":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi"}];
    
    if ([dicUserComp stringValueForJSON:@"average_bpm"].floatValue > 0)
        [arrUserDataTemp addObject:@{@"image":@"heartbeat",@"result":[NSString stringWithFormat:@"%.2f",[dicUserComp stringValueForJSON:@"average_bpm"].floatValue],@"quantities":@"AVG. BPM",@"unit":@""}];
    
    arryMyData = arrUserDataTemp.mutableCopy;
    [clUserRunDetail reloadData];
    
    [self drawGraph];
}

#pragma mark - Map View Related Functions
-(void)setupForMapView:(UIEdgeInsets)inset userData:(NSDictionary *)dicUser
{
    dispatch_async(GCDMainThread, ^{
        NSArray *arrLatLong = [dicUser valueForKey:@"running"];
        
        [mapView clearMGLMapView];
        mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        if (arrLatLong.count > 0)
        {
            [mapView MGLMapViewCenterCoordinate:arrLatLong];
            [mapView drawPolylineByUsingCoordinates:arrLatLong];
            
            if (arrLatLong.count > 1)
                [self addMarkerOnMapView:arrLatLong IsStartPosition:YES];   // If Start and End position available then....
            else
                [self addMarkerOnMapView:arrLatLong IsStartPosition:NO]; // Only end position availalbe
            
            [mapView setPathCenterInMapView:arrLatLong edgeInset:UIEdgeInsetsMake(inset.top, inset.left, inset.bottom, inset.right)];
        }
    });
}

    -(void)addMarkerOnMapView:(NSArray *)arrMarker IsStartPosition:(BOOL)isStartPos
    {
        NSDictionary *dicMarkerLast = arrMarker.lastObject;
        [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerLast floatForKey:@"latitude"], [dicMarkerLast floatForKey:@"longitude"]) Type:EndPossitionMarker UserInfo:dicMarkerLast];
        
        if (isStartPos)
        {
            NSDictionary *dicMarkerFirst = arrMarker.firstObject;
            [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerFirst floatForKey:@"latitude"], [dicMarkerFirst floatForKey:@"longitude"]) Type:StartPossitionMarker UserInfo:dicMarkerFirst];
        }
    }


#pragma mark - UICollectionView Delegate and Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clUserRunDetail])
        return arryMyData.count;
    else if ([collectionView isEqual:clCategory])
        return 3;
    else
        return self.arrRunners.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clUserRunDetail])
    {
        if (arryMyData.count == 4)
        {
            NSString *strIdentifier = @"RunSummaryResultCell";
            RunSummaryResultCell *cell = [clUserRunDetail dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCureentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCureentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCureentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCureentValue valueForKey:@"unit"];
            
            cell.imgSide.image = [UIImage imageNamed:[dicCureentValue valueForKey:@"image"]];
            
            return cell;
        }
        else
        {
            NSString *strIdentifier = @"MyActivityResultCollectionViewCell";
            MyActivityResultCollectionViewCell *cell = [clUserRunDetail dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCureentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCureentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCureentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCureentValue valueForKey:@"unit"];
            
            cell.imgTop.image = [UIImage imageNamed:[dicCureentValue valueForKey:@"image"]];
            
            return cell;
        }
    }
    else if ([collectionView isEqual:clCategory])
    {
        NSString *strIdentifier = @"ResultRunCategoryCell";
        ResultRunCategoryCell *cell = [clCategory dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        if ([indexPath isEqual:selectedCategoryIndexpath])
        {
            cell.viewSelectedCat.hidden = NO;
            cell.btnCategory.selected = YES;
        }
        else
        {
            cell.viewSelectedCat.hidden = YES;
            cell.btnCategory.selected = NO;
        }
        
        switch (indexPath.item) {
            case 0:
                [cell.btnCategory setTitle:@"PACE" forState:UIControlStateNormal];
                break;
            case 1:
                [cell.btnCategory setTitle:@"TIME" forState:UIControlStateNormal];
                break;
            case 2:
                [cell.btnCategory setTitle:@"POSITION" forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        NSString *strIdentifier = @"ResultRunnerCell";
        ResultRunnerCell *cell = [clRunners dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        NSDictionary *dicData = [self.arrRunners objectAtIndex:indexPath.row];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"60" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        switch (indexPath.item) {
            case 0:
                cell.viewUserColor.backgroundColor = CGraphColor1;
                break;
            case 1:
                cell.viewUserColor.backgroundColor = CGraphColor2;
                break;
            case 2:
                cell.viewUserColor.backgroundColor = CGraphColor3;
                break;
            case 3:
                cell.viewUserColor.backgroundColor = CGraphColor4;
                break;
            case 4:
                cell.viewUserColor.backgroundColor = CGraphColor5;
                break;
            case 5:
                cell.viewUserColor.backgroundColor = CGraphColor6;
                break;
            case 6:
                cell.viewUserColor.backgroundColor = CGraphColor7;
                break;
            case 7:
                cell.viewUserColor.backgroundColor = CGraphColor8;
                break;
            case 8:
                cell.viewUserColor.backgroundColor = CGraphColor9;
                break;
            case 9:
                cell.viewUserColor.backgroundColor = CGraphColor10;
                break;
            case 10:
                cell.viewUserColor.backgroundColor = CGraphColor11;
                break;
            case 11:
                cell.viewUserColor.backgroundColor = CGraphColor12;
                break;
            case 12:
                cell.viewUserColor.backgroundColor = CGraphColor13;
                break;
            case 13:
                cell.viewUserColor.backgroundColor = CGraphColor14;
                break;
            case 14:
                cell.viewUserColor.backgroundColor = CGraphColor15;
                break;
            case 15:
                cell.viewUserColor.backgroundColor = CGraphColor16;
                break;
            case 16:
                cell.viewUserColor.backgroundColor = CGraphColor17;
                break;
            case 17:
                cell.viewUserColor.backgroundColor = CGraphColor18;
                break;
            case 18:
                cell.viewUserColor.backgroundColor = CGraphColor19;
                break;
            case 19:
                cell.viewUserColor.backgroundColor = CGraphColor20;
                break;
            default:
                break;
        }
        
        return cell;
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clUserRunDetail])
    {
        if (arryMyData.count == 4)
            return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
        else
        {
            if (indexPath.row < 2)
                return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
            
            return CGSizeMake(CScreenWidth/3-.01 , CGRectGetHeight(collectionView.frame)/2);
        }
    }
    else if ([collectionView isEqual:clCategory])
        return CGSizeMake(80 , CGRectGetHeight(clCategory.frame));
    else
        return CGSizeMake(50 , CGRectGetHeight(clRunners.frame));
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clCategory])
    {
        if ([selectedCategoryIndexpath isEqual:indexPath])
            return;
        
        if (selectedCategoryIndexpath)
        {
            ResultRunCategoryCell *cell = (ResultRunCategoryCell *)[clCategory cellForItemAtIndexPath:selectedCategoryIndexpath];
            cell.btnCategory.selected = NO;
            cell.viewSelectedCat.hidden = YES;
            selectedCategoryIndexpath = nil;
        }
        
        ResultRunCategoryCell *cell = (ResultRunCategoryCell *)[clCategory cellForItemAtIndexPath:indexPath];
        cell.btnCategory.selected = YES;
        cell.viewSelectedCat.hidden = NO;
        selectedCategoryIndexpath = indexPath;
        
        [self drawGraph];
    }
    
}

#pragma mark - Graph Related Functions
-(void)removeLayerFromGraphView
{
    [viewGraph.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    NSLog(@"%@",viewGraph.layer.sublayers);
}


-(void)setGraphStartAndEndValue
{
    NSString *strDistnace = [self.dicSession stringValueForJSON:@"distance"];
    
    switch (selectedCategoryIndexpath.item) {
        case 0: // Pace Value
        case 2:// Possition Value
        {
            if (strDistnace.intValue >= 1000)
            {
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%d",strDistnace.intValue/1000];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"km";
            }
            else
            {
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%d",strDistnace.intValue];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"m";
            }
        }
            break;
        case 1:
        {
            // Time Value
            float sessionMaxCompleteTime = ([self.dicSession stringValueForJSON:@"distance"].floatValue/1000) * 7.5;
            
            if (strDistnace.intValue <= 200)
            {
                // Show in Second
                sessionMaxCompleteTime = sessionMaxCompleteTime*60;
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%.1f",sessionMaxCompleteTime];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"Sec";
            }
            else
            {
                // Show in mint
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%.1f",sessionMaxCompleteTime];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"Min";
            }
        }
            break;
        
        default:
            break;
    }
}


// Draw Graph with pace and Distance
-(NSArray *)getGraphPointsForPaceAndDistance:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSString *strDistnace = [self.dicSession stringValueForJSON:@"distance"];
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    
    int distanceRatio = 0;
    if (strDistnace.intValue >= 1000)
        distanceRatio = 1000;
    else
        distanceRatio = 20;

    float distanceSlot = (CScreenWidth-CGraphStartMinValue)/(strDistnace.floatValue/distanceRatio);
    float xPoint = CGraphStartMinValue;
    float yPoint = 0;
    
    NSArray *arrRunning = [dicUser valueForKey:@"running"];
    
    for (int i = 0; arrRunning.count > i; i++)
    {
        NSDictionary *dicRunData = arrRunning[i];
        
        NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
        int pace = stRPaceSpeed.intValue;
        
        float coverDistance = [dicRunData stringValueForJSON:@"distance"].floatValue/distanceRatio;
        xPoint = coverDistance * distanceSlot;

        if (xPoint < CGraphStartMinValue)
            xPoint = CGraphStartMinValue;
        
        if (xPoint > CScreenWidth)
            xPoint = CScreenWidth;
        
        if (pace < 5 && pace > 0)
            pace = 5;
        
        pace  = ViewGraphHeight - pace;
        
        if (pace < 0 || pace >= ViewGraphHeight)
            pace = 5;
        
        yPoint = pace;
        [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
    }
    
    arrGraphPoins = arrPoints.mutableCopy;
    
    return arrGraphPoins;
}

// Draw Graph with pace and Time
-(NSArray *)getGraphPointsForPaceAndTime:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
 
    // Time Value
    float sessionMaxCompleteTime = ([self.dicSession stringValueForJSON:@"distance"].floatValue/1000) * 7.5;
    NSString *strDistnace = [self.dicSession stringValueForJSON:@"distance"];
    
    if (strDistnace.intValue <= 200)
    {
        // Show in Second
        sessionMaxCompleteTime = sessionMaxCompleteTime*60;
        float timeSlot = (CScreenWidth-CGraphStartMinValue)/(sessionMaxCompleteTime);
        
        float xPoint = CGraphStartMinValue;
        float yPoint = 0;
        
        NSArray *arrRunning = [dicUser valueForKey:@"running"];
        for (int i = 0; arrRunning.count > i; i++)
        {
            NSDictionary *dicRunData = arrRunning[i];
            NSArray *arrTimeSlot = [[dicRunData stringValueForJSON:@"time"] componentsSeparatedByString:@":"];
            
            float totalSecond = 0;
            if (arrTimeSlot.count > 2)
            {
                
                NSString *strMint = arrTimeSlot[1];
                NSString *strSecond = arrTimeSlot[2];
                
                float mint = strMint.floatValue*60;
                totalSecond = mint+strSecond.floatValue;
            }
            
            xPoint = timeSlot*totalSecond;
            
            NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
            int pace = stRPaceSpeed.intValue;
            
            if (xPoint < CGraphStartMinValue)
                xPoint = CGraphStartMinValue;
            
            if (xPoint > CScreenWidth)
                xPoint = CScreenWidth;
            
            if (pace < 5 && pace > 0)
                pace = 5;
            
            pace  = ViewGraphHeight - pace;
            
            if (pace < 0 || pace >= ViewGraphHeight)
                pace = 5;
            
            yPoint = pace;
            [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
        }
    }
    else
    {
        // Show in mint
        
        float timeSlot = (CScreenWidth-CGraphStartMinValue)/(sessionMaxCompleteTime);
     
        float xPoint = CGraphStartMinValue;
        float yPoint = 0;
        
        NSArray *arrRunning = [dicUser valueForKey:@"running"];
        for (int i = 0; arrRunning.count > i; i++)
        {
            NSDictionary *dicRunData = arrRunning[i];
            NSArray *arrTimeSlot = [[dicRunData stringValueForJSON:@"time"] componentsSeparatedByString:@":"];
            
            float totalMint = 0;
            if (arrTimeSlot.count > 2)
            {
                NSString *strHour = arrTimeSlot[0];
                NSString *strMint = arrTimeSlot[1];
                
                float hour = strHour.floatValue*60;
                totalMint = hour+strMint.floatValue;
            }
            
            xPoint = timeSlot*totalMint;
            
            NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
            int pace = stRPaceSpeed.intValue;
            
            if (xPoint < CGraphStartMinValue)
                xPoint = CGraphStartMinValue;
            
            if (xPoint > CScreenWidth)
                xPoint = CScreenWidth;
            
            if (pace < 5 && pace > 0)
                pace = 5;
            
            pace  = ViewGraphHeight - pace;
            
            if (pace < 0 || pace >= ViewGraphHeight)
                pace = 5;
            
            yPoint = pace;
            [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
        }
    }

    arrGraphPoins = arrPoints.mutableCopy;
    
    return arrGraphPoins;
}


// Draw Graph with position and Distance
-(NSArray *)getGraphPointsForPositionAndDistance:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSString *strDistnace = [self.dicSession stringValueForJSON:@"distance"];
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    
    int distanceRatio = 0;
    if (strDistnace.intValue >= 1000)
        distanceRatio = 1000;
    else
        distanceRatio = 20;
    
    float xPoint = CGraphStartMinValue;
    float yPoint = 0;
    
    NSArray *arrRunning = [dicUser valueForKey:@"running"];
    
    
    float heightSlot = ViewGraphHeight/(self.arrRunners.count);
    float distanceSlot = (CScreenWidth-CGraphStartMinValue)/(strDistnace.floatValue/distanceRatio);
    
    for (int i = 0; arrRunning.count > i; i++)
    {
        NSDictionary *dicRunData = arrRunning[i];
        float coverDistance = [dicRunData stringValueForJSON:@"distance"].floatValue/distanceRatio;
        xPoint = coverDistance * distanceSlot;
        
        if (xPoint < CGraphStartMinValue)
            xPoint = CGraphStartMinValue;
        
        if (xPoint > CScreenWidth)
            xPoint = CScreenWidth;
        
       int isUserPossition = [dicRunData stringValueForJSON:@"position"].intValue;
        
        // If any user disqualify from race..
        if (isUserPossition > self.arrRunners.count)
            isUserPossition = (int)self.arrRunners.count;

        float graphPosition = 0;
        
        if (isUserPossition > 0)
        {
            graphPosition = (float)self.arrRunners.count / (float)isUserPossition;
        }
        

        yPoint = heightSlot *graphPosition;

//        yPoint = heightSlot *isUserPossition;
        [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
    }
    
    arrGraphPoins = arrPoints.mutableCopy;
    return arrGraphPoins;
}


-(void)drawGraph
{
    [self removeLayerFromGraphView]; // To remove all layer from View
    [self setGraphStartAndEndValue];  // To set Graph Start and End point
    
    for (int i = 0; self.arrRunners.count > i; i++)
    {
        BOOL isVisibleUser = NO;
        
        NSDictionary *dicUser = self.arrRunners[i];
        
        NSArray *arrPoints;
        switch (selectedCategoryIndexpath.item) {
            case 0: // Pace Value
            {
                arrPoints = [self getGraphPointsForPaceAndDistance:dicUser];
            }
                break;
            case 1:
            {
                // Time Value
                arrPoints = [self getGraphPointsForPaceAndTime:dicUser];
            }
                break;
            case 2:// Possition Value
            {
                arrPoints = [self getGraphPointsForPositionAndDistance:dicUser];
            }
                break;

            default:
                break;
        }
        
        if ([[dicUser stringValueForJSON:@"user_id"] isEqualToString:self.strSelectedUserId])
            isVisibleUser = YES;
        
        UIColor *strokColor;
        switch (i) {
            case 0:
                strokColor = CGraphColor1;
                break;
            case 1:
                strokColor = CGraphColor2;
                break;
            case 2:
                strokColor = CGraphColor3;
                break;
            case 3:
                strokColor = CGraphColor4;
                break;
            case 4:
                strokColor = CGraphColor5;
                break;
            case 5:
                strokColor = CGraphColor6;
                break;
            case 6:
                strokColor = CGraphColor7;
                break;
            case 7:
                strokColor = CGraphColor8;
                break;
            case 8:
                strokColor = CGraphColor9;
                break;
            case 9:
                strokColor = CGraphColor10;
                break;
            case 10:
                strokColor = CGraphColor11;
                break;
            case 11:
                strokColor = CGraphColor12;
                break;
            case 12:
                strokColor = CGraphColor13;
                break;
            case 13:
                strokColor = CGraphColor14;
                break;
            case 14:
                strokColor = CGraphColor15;
                break;
            case 15:
                strokColor = CGraphColor16;
                break;
            case 16:
                strokColor = CGraphColor17;
                break;
            case 17:
                strokColor = CGraphColor18;
                break;
            case 18:
                strokColor = CGraphColor19;
                break;
            case 19:
                strokColor = CGraphColor20;
                break;
            default:
                break;
        }
        
        if (arrPoints.count > 0)
            [self addBezierPathBetweenPoints:arrPoints toView:viewGraph withColor:strokColor isVisibleUser:isVisibleUser];
        
    }
}

- (void)addBezierPathBetweenPoints:(NSArray *)points toView:(UIView *)view withColor:(UIColor *)storkeColor isVisibleUser:(BOOL)isVisibleUser
{

    NSUInteger strokeWidth = 3;
    float granularity = 100;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path fill];
    [path stroke];
    
    [path moveToPoint:[[points firstObject] CGPointValue]];
    CGPoint lastPoint = CGPointMake(0, 0);
    
    
    for (int index = 1; index < (int)points.count - 2 ; index++)
    {
        CGPoint point0 = [[points objectAtIndex:index - 1] CGPointValue];
        CGPoint point1 = [[points objectAtIndex:index] CGPointValue];
        CGPoint point2 = [[points objectAtIndex:index + 1] CGPointValue];
        CGPoint point3 = [[points objectAtIndex:index + 2] CGPointValue];
        
        for (int i = 1; i < granularity ; i++)
        {
            float t = (float) i * (1.0f / (float) granularity);
            float tt = t * t;
            float ttt = tt * t;
            
            CGPoint pi;
            pi.x = 0.5 * (2*point1.x+(point2.x-point0.x)*t + (2*point0.x-5*point1.x+4*point2.x-point3.x)*tt + (3*point1.x-point0.x-3*point2.x+point3.x)*ttt);
            pi.y = 0.5 * (2*point1.y+(point2.y-point0.y)*t + (2*point0.y-5*point1.y+4*point2.y-point3.y)*tt + (3*point1.y-point0.y-3*point2.y+point3.y)*ttt);
            
            if (pi.y > view.frame.size.height) {
                pi.y = view.frame.size.height;
            }
            else if (pi.y < 0){
                pi.y = 0;
            }
            
            if (pi.x > point0.x)
            {
                [path addLineToPoint:pi];
                lastPoint = point2;
            }
        }
        
        [path addLineToPoint:point2];
    }
    
    [path addLineToPoint:[[points objectAtIndex:[points count] - 1] CGPointValue]];

    
    CAShapeLayer *_graphLayer = [CAShapeLayer layer];
    [_graphLayer setFrame:[view bounds]];
    [_graphLayer setGeometryFlipped:YES];
    [_graphLayer setStrokeColor:[storkeColor CGColor]];
    [_graphLayer setFillColor:nil];
    [_graphLayer setLineWidth:strokeWidth];
    [_graphLayer setLineJoin:kCALineCapRound];
    [[view layer] addSublayer:_graphLayer];
    [_graphLayer setPath:path.CGPath];
    
    
    CAShapeLayer *_maskLayer = [CAShapeLayer layer];
    [_maskLayer setFrame:[view bounds]];
    [_maskLayer setGeometryFlipped:YES];
    [_maskLayer setStrokeColor:[[UIColor clearColor] CGColor]];
    [_maskLayer setFillColor:[[UIColor blackColor] CGColor]];
    [_maskLayer setLineWidth:strokeWidth];
    [_maskLayer setLineJoin:kCALineCapRound];
    
    if (isVisibleUser)
    {
        // Add gradient
        CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
        CGColorRef color1 = storkeColor.CGColor;
        CGColorRef color2 = storkeColor.CGColor;
        _gradientLayer.opacity = 0.5;
        [_gradientLayer setColors:@[(__bridge id)color1,(__bridge id)color2]];
        [_gradientLayer setLocations:@[@0.0,@0.9]];
        [_gradientLayer setFrame:[viewGraph bounds]];
        [[viewGraph layer] addSublayer:_gradientLayer];
        
        UIBezierPath *copyPath = [UIBezierPath bezierPathWithCGPath:path.CGPath];
        CGPoint gradinetLastPoint = [[points lastObject] CGPointValue];
        [copyPath addLineToPoint:CGPointMake(gradinetLastPoint.x, 0)];
        
        [_maskLayer setPath:copyPath.CGPath];
        [_gradientLayer setMask:_maskLayer];
    }
}

#pragma mark - Action
#pragma mark

- (IBAction)btnBackClicked:(id)sender
{
    [mapView clearMGLMapView];
    [mapView removeFromSuperview] ;
    mapView = nil ;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShareClicked:(id)sender
{
    UIGraphicsBeginImageContext(viewMain.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [viewMain.layer renderInContext:context];
    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:NO];
}

@end
