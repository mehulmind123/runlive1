//
//  TblActivitySyncSession+CoreDataClass.h
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject, TblActivityDates;

NS_ASSUME_NONNULL_BEGIN

@interface TblActivitySyncSession : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblActivitySyncSession+CoreDataProperties.h"
