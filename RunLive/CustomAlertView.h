//
//  CustomAlertView.h
//  RunLive
//
//  Created by mac-00012 on 5/10/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

@property(strong,nonatomic) IBOutlet UILabel *lblTitle,*lblMessage;
@property(strong,nonatomic) IBOutlet UIButton *btnFirst,*btnSecond;

-(void)initWithTitle:(NSString *)title message:(NSString *)message firstButton:(NSString *)btnFirst secondButton:(NSString *)btnSecond;


@end
