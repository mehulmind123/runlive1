//
//  ResultRunCategoryCell.h
//  RunLive
//
//  Created by mac-0005 on 20/09/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultRunCategoryCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIButton *btnCategory;
@property (nonatomic,strong) IBOutlet UIView *viewSelectedCat;
@end
