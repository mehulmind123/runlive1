//
//  MyBasketViewController.h
//  RunLive
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBasketViewController : SuperViewController
{
    IBOutlet UICollectionView *clProduct;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIView *viewEmptyBasket;
    IBOutlet UILabel *lblUserName,*lblProductCount,*lblEmptyUserName;

}
@end
