//
//  SubProductCell.h
//  RunLive
//
//  Created by mac-00012 on 10/24/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubProductCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIView *viewContainer;
@property(weak,nonatomic) IBOutlet UIImageView *imgProduct ;

@property(weak,nonatomic) IBOutlet UILabel *lblProductName;
@property(weak,nonatomic) IBOutlet UILabel *lblProductPrice;
@end
