//
//  NSObject+TimeIntervalConverter.h
//  RunLive
//
//  Created by mac-00012 on 12/21/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (TimeIntervalConverter)


-(NSString *)convertedDateFromTimeStamp:(double)timeStamp isGMT:(BOOL)isGMT formate:(NSString *)dateFormate;

-(NSDate *)convertDateFromString:(NSString *)strDate1 isGMT:(BOOL)isGMT formate:(NSString *)dateFormate;
@end
