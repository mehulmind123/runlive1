//
//  SessionDrag.m
//  RunLive
//
//  Created by mac-00012 on 11/21/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionDrag.h"

@implementation SessionDrag

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self)
//    {
//        self.backgroundColor    = [UIColor whiteColor];
//        self.layer.cornerRadius = 3.0f;
//        
//        self.imgUser.backgroundColor = [UIColor redColor];
//    }
//    
//    return self;
//}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.backgroundColor = [UIColor clearColor];
//
    self.imgUser.layer.cornerRadius = CGRectGetWidth(self.imgUser.frame)/2;
    self.viewUser.layer.cornerRadius = CGRectGetWidth(self.viewUser.frame)/2;
}

#pragma mark - Public methods

-(void)addBorderColor:(BOOL)isRed
{
    self.viewUser.layer.borderWidth = 1;
    self.viewUser.layer.borderColor = isRed ? [UIColor redColor].CGColor : CRGB(62, 205, 252).CGColor;

    self.layer.borderColor = [UIColor clearColor].CGColor;
    self.layer.borderWidth = 1.0f;

}

- (void)setHighlightSelection:(BOOL)highlight
{
    
    if (highlight)
    {
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.layer.borderWidth = 2.0f;
    }
    else
    {
        self.layer.borderWidth = 0.0f;
    }
}


@end
