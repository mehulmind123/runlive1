//
//  AboutViewController.h
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : SuperViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webAbout;
}
- (IBAction)btnBackClicked:(id)sender;

@end
