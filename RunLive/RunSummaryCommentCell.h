//
//  RunSummaryCommentCell.h
//  RunLive
//
//  Created by mac-0006 on 17/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunSummaryCommentCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnUser;

@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblUserName;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblComment;

@end
