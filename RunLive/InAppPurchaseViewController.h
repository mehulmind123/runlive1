//
//  InAppPurchaseViewController.h
//  RunLive
//
//  Created by mac-0005 on 13/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^inAppPurhaseClose)(BOOL isClose);

@interface InAppPurchaseViewController : SuperViewController
{
    IBOutlet UIPageControl *pageControler;
    
    IBOutlet UIView
    *viewMonthly,
    *viewYearly,
    *viewNotNow,
    *viewStartTrial,
    *viewPriceContainer,
    *viewContentContainer,
    *viewTrialContainer;
    
    IBOutlet UIButton
    *btnMonthly,
    *btnYearly,
    *btnNotNow,
    *btnStartTrial;
    
    IBOutlet UILabel
    *lblMonthly,
    *lblMonthlyPrice,
    *lblYearly,
    *lblYearlyPrice,
    *lblNotNow,
    *lblStartTrail,
    *lblTitle,
    *lblDetails;
}

@property(copy,nonatomic) inAppPurhaseClose configureInAppPurhaseClose;


@end
