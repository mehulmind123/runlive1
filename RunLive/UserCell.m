//
//  UserCell.m
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.viewContainer.layer.masksToBounds = NO;
    self.viewContainer.layer.shadowOffset = CGSizeMake(.8, .8);
    self.viewContainer.layer.shadowOpacity = 0.2;
    self.viewContainer.layer.shadowRadius = 5;
    self.viewContainer.layer.cornerRadius = 3.0;
    
    self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;

}
@end
