//
//  CustomOneButtonAlertView.m
//  RunLive
//
//  Created by mac-0005 on 13/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "CustomOneButtonAlertView.h"

@implementation CustomOneButtonAlertView

+(id)initCustomOneButtonAlertView
{
    CustomOneButtonAlertView *objPopUp = [[[NSBundle mainBundle]loadNibNamed:@"CustomOneButtonAlertView" owner:nil options:nil] lastObject];
    CViewSetWidth(objPopUp, CScreenWidth);
    CViewSetHeight(objPopUp, CScreenHeight);
    return objPopUp;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.viewContainer.layer.cornerRadius = self.btnMain.layer.cornerRadius = 3;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.center = CScreenCenter;
}

#pragma mark - Configuartion

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonTitle:(NSString *)strBtnTitle
{
    lblTitle.text = strTitle;
    lblMessage.text = strMessage;
    [self.btnMain setTitle:strBtnTitle forState:UIControlStateNormal];
}


@end
