//
//  TeamMatchedHeader.m
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "TeamMatchedHeader.h"

@implementation TeamMatchedHeader

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgTag.layer.cornerRadius = CGRectGetHeight(_imgTag.frame)/2;
}
@end
