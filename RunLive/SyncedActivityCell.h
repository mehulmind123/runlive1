//
//  SyncedActivityCell.h
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SyncedActivityCell : UITableViewCell

typedef void(^sesionSyncTimeOver)(BOOL isOver);

@property(weak,nonatomic) IBOutlet UIView *viewContainer,*viewSubContainer;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblActivityText;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment1;
@property(weak,nonatomic) IBOutlet TTTAttributedLabel *lblComment2;

@property(weak,nonatomic) IBOutlet UIImageView
    *imgUser,
    *imgComment,
    *imgHeart;

@property(weak,nonatomic) IBOutlet UIView
    *viewUser,
    *viewSynced,
    *viewCommentSeprator,
    *viewScreenShot;

@property(weak,nonatomic) IBOutlet UILabel
    *lblLikes,
    *lblTotalComments,
    *lblTime,
    *lblSessionTime,
    *lblSessionName;

@property(weak,nonatomic) IBOutlet UIButton
    *btnLike,
    *btnComment,
    *btnShare,
    *btnSyncedUser,
    *btnUser,
    *btnViewAllComments;

@property(weak,nonatomic) IBOutlet UIView
    *viewSynced1,
    *viewSynced2,
    *viewSynced3,
    *viewSynced4,
    *viewSynced5,
    *viewSynced6;

@property(weak,nonatomic) IBOutlet UIImageView
    *imgSynced1,
    *imgSynced2,
    *imgSynced3,
    *imgSynced4,
    *imgSynced5,
    *imgSynced6;

@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnTimeLabelTopSpace;

@property(strong,nonatomic) sesionSyncTimeOver configureSesionSyncTimeOver;

@property (strong, nonatomic) NSTimer *timer;
-(void)startTimer;
-(void)stopTimer;

@property(weak,nonatomic) TblActivitySyncSession *objSessionTime;

-(void)showSyncUser:(NSArray *)arrSyncedUser;


@end
