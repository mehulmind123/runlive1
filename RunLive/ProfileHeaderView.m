//
//  ProfileHeaderView.m
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ProfileHeaderView.h"

@implementation ProfileHeaderView

+(id)InitProfileHeaderView
{
    ProfileHeaderView * objView;
    
    objView = [[[NSBundle mainBundle] loadNibNamed:@"ProfileHeaderView" owner:nil options:nil] lastObject];
    
    objView.btnAll.selected = YES;
    objView.btnRuns.selected = NO;
    objView.viewLineRuns.hidden = YES;
    objView.viewLineAll.hidden = NO;
    
    if ([objView isKindOfClass:[ProfileHeaderView class]])
        return objView;
    else
        return nil;
 
}

@end
