//
//  QuitRunSession.h
//  RunLive
//
//  Created by mac-00012 on 11/18/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuitRunSession : UIView

@property(weak,nonatomic) IBOutlet UIButton *btnYes,*btnNo;

    @property(weak,nonatomic) IBOutlet UILabel *lblText;
@end
