//
//  TblMessages+CoreDataClass.h
//  RunLive
//
//  Created by mac-00012 on 7/31/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TblMessages : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblMessages+CoreDataProperties.h"
