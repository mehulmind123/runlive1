//
//  ChatTableViewCell.h
//  TextToU
//
//  Created by mac-0009 on 7/25/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell
{
    IBOutlet UIView *viewLabel;
}
@property IBOutlet UILabel *lblText;
@property IBOutlet UILabel *lblShowText;
@property IBOutlet UILabel *lblTime;

@end
