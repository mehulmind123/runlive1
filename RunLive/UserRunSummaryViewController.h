//
//  UserRunSummaryViewController.h
//  RunLive
//
//  Created by mac-0006 on 21/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserRunSummaryViewController : SuperViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    
    IBOutlet UILabel *lblTitleName;
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblLocality,*lblSubLocality;
    
    IBOutlet UIView *viewMain,*viewGraph;
    IBOutlet MGLMapView *mapView;
    IBOutlet UICollectionView *clUserRunDetail,*clCategory,*clRunners;
    IBOutlet UILabel *lblGraphStartValue,*lblGraphStartUpValue,*lblGraphEndValue,*lblGraphEndUpValue;
    
}

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnShareClicked:(id)sender;

@property(nonatomic,strong) NSArray *arrRunners;
@property(nonatomic,strong) NSString *strSelectedUserId;
@property(nonatomic,strong) NSDictionary *dicSession;


@end
