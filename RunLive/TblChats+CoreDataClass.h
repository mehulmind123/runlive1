//
//  TblChats+CoreDataClass.h
//  RunLive
//
//  Created by mac-0005 on 19/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject, TblParticipants, TblUser;

NS_ASSUME_NONNULL_BEGIN

@interface TblChats : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblChats+CoreDataProperties.h"
