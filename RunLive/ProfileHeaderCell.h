//
//  ProfileHeaderCell.h
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileNoPhotoCell.h"

@interface ProfileHeaderCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>


@property (strong, nonatomic) IBOutlet UIView *viewFollow;

@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalEarnedPoints;
@property (strong, nonatomic) IBOutlet UILabel *lblNotificationCount;
@property (strong, nonatomic) IBOutlet UILabel *lblUserBio;
@property (strong, nonatomic) IBOutlet UILabel *lblWebsiteLink;
@property (strong, nonatomic) IBOutlet UIButton *btnWebsiteLink;

@property (strong, nonatomic) IBOutlet UIView *viewNotificationBadge;

@property (strong, nonatomic) IBOutlet UIButton *btnPopup;
@property (weak, nonatomic) IBOutlet UIButton *btnUserZoom;
@property (strong, nonatomic) IBOutlet UIButton *btnNotification;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UIButton *btnFollow;
@property (strong, nonatomic) IBOutlet UICollectionView *collview;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) IBOutlet UIImageView *imgUser,*imgVerified;

@property (strong,nonatomic) NSArray *arrList;

@end
