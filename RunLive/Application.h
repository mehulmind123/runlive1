//
//  Application.h
//  SocialNetworks
//
//  Created by mac-0001 on 16/04/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Application : UIApplication <UIWebViewDelegate>

@end
