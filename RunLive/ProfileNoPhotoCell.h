//
//  ProfileNoPhotoCell.h
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileNoPhotoCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UILabel *lblCount;
@property(nonatomic,strong) IBOutlet UILabel *lblTitle;

@end
