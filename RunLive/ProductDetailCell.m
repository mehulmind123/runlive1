//
//  ProductDetailCell.m
//  RunLive
//
//  Created by mac-00012 on 10/24/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ProductDetailCell.h"

@implementation ProductDetailCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgProduct.layer.masksToBounds = YES;
}

@end
