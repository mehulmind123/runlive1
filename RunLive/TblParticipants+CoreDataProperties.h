//
//  TblParticipants+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 05/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblParticipants+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblParticipants (CoreDataProperties)

+ (NSFetchRequest<TblParticipants *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *fb_id;
@property (nullable, nonatomic, copy) NSString *first_name;
@property (nullable, nonatomic, copy) NSString *last_name;
@property (nullable, nonatomic, copy) NSString *picture;
@property (nullable, nonatomic, copy) NSNumber *priority_index;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, copy) NSNumber *celebrity;
@property (nullable, nonatomic, retain) TblChats *chatobject;

@end

NS_ASSUME_NONNULL_END
