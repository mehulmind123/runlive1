//
//  PostParameters.h
//  SocialNetworks
//
//  Created by mac-0001 on 04/05/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface PostParameters : NSObject

@property (nonatomic,strong) NSURL *websiteURL;
@property (nonatomic,strong) NSURL *imageURL;

@property (nonatomic,strong) UIImage *image;

@property (nonatomic,strong) NSString *comment;


@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *description;

@property (nonatomic,strong) NSString *caption;


@end
