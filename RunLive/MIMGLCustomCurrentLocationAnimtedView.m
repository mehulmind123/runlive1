//
//  MIMGLCustomCurrentLocationAnimtedView.m
//  RunLive
//
//  Created by mac-0005 on 16/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MIMGLCustomCurrentLocationAnimtedView.h"

@implementation MIMGLCustomCurrentLocationAnimtedView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    dispatch_async(GCDMainThread, ^{
        [self addAnimtedImageInMarker];
    });
    
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)addAnimtedImageInMarker
{
    PulsingHaloLayer *layer = [PulsingHaloLayer layer];
    [self.viewAnimation.layer insertSublayer:layer below:self.imgUserPosition.layer];
    
    CViewSetX(layer, 7.5);
    CViewSetY(layer, 7.5);
    
    layer.cornerRadius = CViewWidth(layer)/2;
    self.viewAnimation.layer.cornerRadius = CViewWidth(self.viewAnimation)/2;
    self.imgUserPosition.layer.cornerRadius = CViewWidth(self.imgUserPosition)/2;
    self.layer.cornerRadius = CViewWidth(self)/2;
    layer.haloLayerNumber = 5;
    layer.radius = 30;
    layer.animationDuration = 10;
    [layer setBackgroundColor:CRGB(18, 216, 253).CGColor];
    [layer start];
}
@end
