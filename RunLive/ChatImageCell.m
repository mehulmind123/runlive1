//
//  ChatImageCell.m
//  RunLive
//
//  Created by mac-0004 on 03/11/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ChatImageCell.h"

@implementation ChatImageCell
@synthesize imgChat,imgUser;

- (void)awakeFromNib
{
    [super awakeFromNib];
    imgUser.layer.cornerRadius = imgUser.frame.size.width/2;
    imgUser.clipsToBounds = true;
//    [imgChat setAlignRight:true];
    CGFloat radians = atan2f(self.transform.b, self.transform.a);
    CGFloat degrees = radians * (180 / M_PI);
    if (degrees == 0)
    {
        self.transform = CGAffineTransformMakeRotation(M_PI);
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)setImageHeightWidth:(NSString *)strImgWidth Height:(NSString *)strImgHeight
{
    float width = 0;
    float height = 0;
    
    
    float origenalHeight = strImgHeight.floatValue;
    float origenalWidth = strImgWidth.floatValue;

    
    self.imgChat.contentMode = origenalWidth > CScreenWidth*2 ? UIViewContentModeScaleAspectFill : UIViewContentModeScaleAspectFit;
    
    float imgPer = (origenalHeight*100)/origenalWidth;
    
    if (isnan(imgPer))
        imgPer = 0;
    
    if (origenalHeight > CScreenWidth-88)
        height = ((CScreenWidth-88)*imgPer)/100;
    else if (origenalHeight < 60)
        height = 60;
    else
        height = (origenalHeight*imgPer)/100;
    
    if (origenalWidth >= CScreenWidth-88)
        width = CScreenWidth-88;
    else if (origenalWidth < 60)
        width = 60;
    else
        width = origenalWidth;
    
    
    self.cnImgWidth.constant = width;
    self.cnImgHeight.constant = height;

}


@end
