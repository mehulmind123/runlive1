//
//  ChatViewController.h
//  TextToU
//
//  Created by mac-0009 on 7/25/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

typedef void(^addUserToGroup)(NSMutableArray *);

@interface ChatViewController : SuperViewController <HPGrowingTextViewDelegate>
{
    IBOutlet UITableView *tblChat;
    
    IBOutlet UIButton *btnSend,*btnImagePicker;
    
    IBOutlet HPGrowingTextView *txtVChat;
    IBOutlet NSLayoutConstraint *txtChatHeight;
    IBOutlet UIScrollView *scrollOutter;
    IBOutlet UILabel *lbTopTitle,*lbTypingMessage;
    IBOutlet UIView *viewTypingIndicator;
    
}
//@property TblCustomer *objcustomer;
@property NSNumber *user_id;

@property(nonatomic,weak) NSNumber *numStartTimestamp;

@property(assign) BOOL isFromMSGScreen;
@property(nonatomic,strong) NSArray *arrSelectedChat;
@property (nonatomic, strong) NSString *strChannelId;
@property (nonatomic, strong) TblChats *chatObject;

@property (nonatomic, copy) addUserToGroup configureAddUserToGroup;

@end
