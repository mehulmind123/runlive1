//
//  TblSessionListDate+CoreDataProperties.m
//  RunLive
//
//  Created by mac-00012 on 4/28/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionListDate+CoreDataProperties.h"

@implementation TblSessionListDate (CoreDataProperties)

+ (NSFetchRequest<TblSessionListDate *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblSessionListDate"];
}

@dynamic date;
@dynamic date_text;
@dynamic timestamp;
@dynamic sessionList;

@end
