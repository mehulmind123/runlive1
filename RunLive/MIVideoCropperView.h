//
//  MIVideoCropperView.h
//  Runlive
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIVideoCropperOverlayView.h"

@interface MIVideoCropperView : UIView

@property (nonatomic, strong) MIVideoCropperOverlayView *overlayView;
- (id)initWithFrame:(CGRect)frame;

@end
