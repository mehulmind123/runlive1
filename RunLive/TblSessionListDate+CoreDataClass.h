//
//  TblSessionListDate+CoreDataClass.h
//  RunLive
//
//  Created by mac-00012 on 4/28/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TblSessionList;

NS_ASSUME_NONNULL_BEGIN

@interface TblSessionListDate : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblSessionListDate+CoreDataProperties.h"
