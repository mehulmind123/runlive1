//
//  SplashViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController
{
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSURL *url = [[NSBundle mainBundle] URLForResource:@"splash_animation" withExtension:@"gif"];
    imgSplash.image = [UIImage animatedImageWithAnimatedGIFURL:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    imgSplash.image = nil;
}

@end
