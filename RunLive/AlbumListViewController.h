//
//  AlbumListViewController.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListCell.h"
#import "MusicListViewController.h"
#import "InviteFriendsViewController.h"

@interface AlbumListViewController : SuperViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblList;
    IBOutlet UIView *viewSearchBar;
    IBOutlet UITextField *txtSearch;
    IBOutlet UIButton *btnSearch;
    IBOutlet UIImageView *imgLogo;
    IBOutlet UIView *viewAppleCategory;
    IBOutlet UIButton *btnPlaylists,*btnAlbums,*btnArtists;
}

@property(atomic,assign) BOOL isApple;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnSearchClicked:(id)sender;




@end
