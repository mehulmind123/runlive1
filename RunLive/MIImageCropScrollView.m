//
//  MIImageCropScrollView.m
//  RunLive
//
//  Created by mac-0005 on 19/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MIImageCropScrollView.h"

@implementation MIImageCropScrollView
{
 
}
@synthesize imageView;

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self viewConfigurations];
}


-(void)viewConfigurations
{
    
    self.clipsToBounds = false;
    self.showsVerticalScrollIndicator = false;
    self.showsHorizontalScrollIndicator = false;
    self.alwaysBounceHorizontal = true;
    self.alwaysBounceVertical = true;
    self.bouncesZoom = true;
    self.decelerationRate = UIScrollViewDecelerationRateFast;
    self.delegate = self;
    self.maximumZoomScale = 5.0;
    imageView = [UIImageView new];
    imageView.backgroundColor = [UIColor clearColor];
    [self addSubview:imageView];
}

-(void)imageToDisplay:(UIImage *)image
{
    dispatch_async(GCDMainThread, ^{
        self.zoomScale = 1.0;
        self.minimumZoomScale = 1.0;
        imageView.image = image;
        CGSize imageSize = [self sizeForImageToDisplay];
        CViewSetWidth(imageView, imageSize.width);
        CViewSetHeight(imageView, imageSize.height);
        imageView.center = self.center;
        self.contentSize = imageView.frame.size;
        self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        [self updateLayout];
    });
}

-(void)updateLayout
{
    
    imageView.center = self.center;
    CGRect frame = imageView.frame;
    
    if (frame.origin.x < 0) { frame.origin.x = 0; }
    if (frame.origin.y < 0) { frame.origin.y = 0; }
    imageView.frame = frame;
}

-(CGSize)sizeForImageToDisplay
{
    CGFloat actualWidth = imageView.image.size.width;
    CGFloat actualHeight = imageView.image.size.height;
    
    CGFloat imgRatio = actualWidth/actualHeight;
    CGFloat maxRatio = self.frame.size.width/self.frame.size.height;
    
    
    if (imgRatio != maxRatio)
    {
        if(imgRatio < maxRatio)
        {
            imgRatio = self.frame.size.height / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = self.frame.size.height;
        }
        else{
            imgRatio = self.frame.size.width / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = self.frame.size.width;
        }
    }
    else {
        imgRatio = self.frame.size.width / actualWidth;
        actualHeight = imgRatio * actualHeight;
        actualWidth = self.frame.size.width;
    }
    
    return  CGSizeMake(actualWidth, actualHeight);
    
}

-(CGFloat)zoomScaleWithNoWhiteSpaces
{
    CGSize imageViewSize = imageView.bounds.size;
    CGSize scrollViewSize = self.bounds.size;
    CGFloat widthScale = scrollViewSize.width / imageViewSize.width;
    CGFloat heightScale = scrollViewSize.height / imageViewSize.height;
    return MAX(widthScale, heightScale);
}

-(void)zoom
 {
    if (self.zoomScale <= 1.0)
        [self setZoomScale:[self zoomScaleWithNoWhiteSpaces] animated:YES];
    else
        [self setZoomScale:self.minimumZoomScale animated:YES];
    
     [self updateLayout];
}

#pragma mark - ScrollView Delegate Methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self updateLayout];
}




@end
