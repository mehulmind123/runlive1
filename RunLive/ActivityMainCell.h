//
//  ActivityMainCell.h
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityAddPhotoCell.h"



typedef void(^activityDidSelect)(NSDictionary *dicUserData,BOOL isLeaderBoard,NSManagedObject *objCoreData);
typedef void(^btnCommentCLK)(NSString *activityId,NSString *strActtype);

@interface ActivityMainCell : UICollectionViewCell
{
    IBOutlet UIButton *btnActive,*btnLeaderBoard;
    IBOutlet UIView *viewActive,*viewLeaderBoard,*viewUser,*viewSubUser;
    IBOutlet UIImageView *imgUser,*imgVerifiedUser;
    IBOutlet UIView *viewUserImg;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIActivityIndicatorView *BottomActivityIndicator;
    IBOutlet UIView *viewLoadMoreData;
}

@property(weak,nonatomic) IBOutlet UILabel *lblLoginUserName;
@property(weak,nonatomic) IBOutlet UILabel *lblLoginUserMiles;
@property(weak,nonatomic) IBOutlet UILabel *lblLoginUserCal;
@property(weak,nonatomic) IBOutlet UILabel *lblLoginUserRuns;
@property(weak,nonatomic) IBOutlet UILabel *lblLoginUserRank;
@property(weak,nonatomic) IBOutlet UIImageView *imgPerformanceTag;

@property(weak,nonatomic) IBOutlet UITableView *tblActivity;
@property(strong,nonatomic) NSMutableArray *arrData;

@property(weak,nonatomic) IBOutlet UIButton *btnUser;
@property(weak,nonatomic) IBOutlet UIButton *btnMessage;
@property(weak,nonatomic) IBOutlet UIButton *btnMyProfile;

@property(strong,nonatomic) activityDidSelect configureActivityDidSelect;
@property(strong,nonatomic) btnCommentCLK configureBtnCommentCLK;
@property(strong,nonatomic) ActivityAddPhotoCell *cellActivityVideo;

@end
