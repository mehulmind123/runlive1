//
//  MusicListViewController.m
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MusicListViewController.h"

@interface MusicListViewController ()

@end

@implementation MusicListViewController
{
    
    SPTPlaylistSnapshot *objPlayListSnap;
    
    NSMutableArray *arrMusicList;
//    AVAudioPlayer *audioPlayer;
    AVPlayer *audioPlayer;
    
    
    NSIndexPath *selectedIndexPath;
}
@synthesize imgURL,URI,name;


#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [tblList registerNib:[UINib nibWithNibName:@"PlayListCell" bundle:nil] forCellReuseIdentifier:@"PlayListCell"];
    
    btnPlayList.layer.cornerRadius = 2;
    btnPlayList.layer.masksToBounds = YES;
    [self setViewShadow];
    
     imgLogo.image = self.isApple ? [UIImage imageNamed:@"ic_appple_logo"] : [UIImage imageNamed:@"ic_spotify_logo"];
    
    arrMusicList = [NSMutableArray new];
    
    self.isApple ? [self getAppleTrackListFromPlayList] : [self getSpotifyTrackListFromPlayList];
    
    viewBottom.layer.masksToBounds = NO;
    viewBottom.layer.shadowRadius = 8;
    viewBottom.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewBottom.layer.shadowOffset = CGSizeMake(0, -3);
    viewBottom.layer.shadowOpacity = .5f;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    activityIndicator.hidden = YES;
    [activityIndicator stopAnimating];
    [self.view setUserInteractionEnabled:YES];
}

-(void)setViewShadow
{
    // drop shadow
    [viewBottom.layer setShadowColor:CRGB(200, 200, 200).CGColor];
    [viewBottom.layer setShadowOpacity:0.8];
    [viewBottom.layer setShadowRadius:10.0];
    [viewBottom.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

#pragma mark - Get All Songs From playlist

-(void)getAppleTrackListFromPlayList
{
    [arrMusicList removeAllObjects];
    [tblList reloadData];
    
    [arrMusicList addObjectsFromArray:self.arrAppleMusic];
    [tblList reloadData];
}

-(void)getSpotifyTrackListFromPlayList
{
    [[PPLoader sharedLoader] ShowHudLoader];
    
    [SPTPlaylistSnapshot playlistWithURI:URI session:[SPTAuth defaultInstance].session callback:^(NSError *error, SPTPlaylistSnapshot* playablePlaylist)
     {
         [[PPLoader sharedLoader] HideHudLoader];
         if (error != nil)
         {
             NSLog(@"*** Getting playlists got error: %@", error);
             return;
         }
         
         if(!playablePlaylist)
         {
             NSLog(@"PlaylistSnapshot from call back is nil");
             return;
         }
         objPlayListSnap = (SPTPlaylistSnapshot *)playablePlaylist;
         [arrMusicList addObjectsFromArray:objPlayListSnap.tracksForPlayback];
         [tblList reloadData];
     }];
}

#pragma mark - UITableview Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMusicList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"PlayListCell";
    PlayListCell *cell = [tblList dequeueReusableCellWithIdentifier:strIdentifier];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    cell.btnPlayPause.selected = [selectedIndexPath isEqual:indexPath];
    cell.btnPlayPause.hidden = self.isApple;
    
    [cell.lblTitle setFont:CFontGilroyBold(15)];
    [cell.lblSubTitle setFont:CFontGilroyMedium(12)];
    
    if (self.isApple)
    {
        // Code For Apple music...
        MPMediaItem *objAppleSong = arrMusicList[indexPath.row];
        cell.lblTitle.text = objAppleSong.title;
        
        cell.lblSubTitle.text = objAppleSong.artist ? [NSString stringWithFormat:@"%@ - %@",objAppleSong.artist,objAppleSong.title]: objAppleSong.title;
        
        cell.imgAlbum.contentMode = UIViewContentModeScaleAspectFit;
        
        UIImage *albumArtworkImage = NULL;
        MPMediaItemArtwork *itemArtwork = [objAppleSong valueForProperty:MPMediaItemPropertyArtwork];
        
        if (itemArtwork != nil)
            albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
        
        cell.imgAlbum.image = albumArtworkImage ? albumArtworkImage : [UIImage imageNamed:@"ic_music"];
    }
    else
    {
        SPTPartialTrack *objTrack = arrMusicList[indexPath.row];
        cell.lblTitle.text = objTrack.name;
        
        NSString *nameArtist = @"";
        for (int i = 0; i < objTrack.artists.count; i++)
        {
            SPTPartialArtist *artist = objTrack.artists[i];
            nameArtist = [NSString stringWithFormat:@"%@%@",artist.name,(i == objTrack.artists.count-1)?@"":[NSString stringWithFormat:@",%@",nameArtist]];
        }
        
        cell.lblSubTitle.text = [NSString stringWithFormat:@"%@ - %@",nameArtist,objTrack.name];
        
        SPTPartialAlbum *album = objTrack.album;
        SPTImage *image = album.smallestCover;
        [cell.imgAlbum setImageWithURL:image.imageURL usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (self.isApple)
        return;
    
    if (appDelegate.configureTrackStopped)
        appDelegate.configureTrackStopped(YES);
    
    if ([selectedIndexPath isEqual:indexPath])
    {
        PlayListCell *cell = [tblList cellForRowAtIndexPath:selectedIndexPath];
        cell.btnPlayPause.selected = NO;
        selectedIndexPath = nil;
        [audioPlayer pause];
        
        return;
    }
    
    if (selectedIndexPath)
    {
        PlayListCell *cell = [tblList cellForRowAtIndexPath:selectedIndexPath];
        cell.btnPlayPause.selected = NO;
        selectedIndexPath = nil;
        [audioPlayer pause];
    }
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    [self.view setUserInteractionEnabled:NO];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.isApple)
        {
            MPMediaItem *objAppleSong = arrMusicList[indexPath.row];
            NSLog(@"Play url ======= %@",[objAppleSong valueForProperty: MPMediaItemPropertyPersistentID]);
        }
        else
        {
            SPTPartialTrack *objTrack = arrMusicList[indexPath.row];
//            NSData *_objectData = [NSData dataWithContentsOfURL:objTrack.previewURL];
//            NSError *error;
//            audioPlayer = [[AVAudioPlayer alloc] initWithData:_objectData error:&error];
//            audioPlayer.numberOfLoops = 1;
            
            NSLog(@"Song URL ====== >>> %@",objTrack.previewURL);
            AVPlayerItem *aPlayerItem = [[AVPlayerItem alloc] initWithURL:objTrack.previewURL];
            audioPlayer = [[AVPlayer alloc] initWithPlayerItem:aPlayerItem];
            
            if (audioPlayer == nil)
            {
                NSLog(@"AVAudio player is not found");
                [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:@"Track not found."];
            }
            else
            {
                [audioPlayer play];
                
                PlayListCell *cell = [tblList cellForRowAtIndexPath:indexPath];
                cell.btnPlayPause.selected = YES;
                selectedIndexPath = indexPath;

            }
            
            activityIndicator.hidden = YES;
            [activityIndicator stopAnimating];
            [self.view setUserInteractionEnabled:YES];
            
            // You may find a test stream at <http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8>.
            
            //        AVPlayerItem *itm = [AVPlayerItem playerItemWithURL:objTrack.previewURL];
            //
            //        //(optional) [playerItem addObserver:self forKeyPath:@"status" options:0 context:&ItemStatusContext];
            //
            //        AVPlayer *player1  = [AVPlayer playerWithPlayerItem:itm];
            //        
            //        //AVPlayer *player2 = [AVPlayer playerWithURL:objTrack.previewURL];
            //        
            //        [player1 play];
        }
    });
    
    
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSelectPlayListClicked:(id)sender
{
    [appDelegate.appleMPMusicPlayerController pause];
    appDelegate.appleMPMusicPlayerController = nil;
    
    if (arrMusicList.count > 0)
    {
        [TblMusicList deleteAllObjects];
        [CUserDefaults removeObjectForKey:CAppleMusicCollection];
        [CUserDefaults synchronize];
        
        [appDelegate deleteSongImagePath];
        [appDelegate deleteAlbumImagePath];
        
        NSString *strAlbImage = [NSString stringWithFormat:@"Runlive_%d%d.jpg", (int)[[NSDate date] timeIntervalSince1970], 0];
        NSData *imgAlbData = [NSData dataWithData:UIImageJPEGRepresentation(appDelegate.imgTempMusic, 0.9)];
        [imgAlbData writeToFile:[[appDelegate createAlbumImagePath] stringByAppendingPathComponent:strAlbImage] atomically:YES];
        
        for (int i = 0; arrMusicList.count > i; i++)
        {
            TblMusicList *objMusic = [TblMusicList create:nil];
            
            if (self.isApple)
            {
                MPMediaItem *objAppleSong = arrMusicList[i];
                objMusic.song_name = objAppleSong.title;
                objMusic.artist_name = objAppleSong.artist ? objAppleSong.artist : @"";
                objMusic.song_url = objAppleSong.assetURL.absoluteString;
                objMusic.isApple = [NSNumber numberWithBool:self.isApple];
                objMusic.playlist_name = appDelegate.strPlaylistName;
                objMusic.songId = [NSString stringWithFormat:@"%llu",objAppleSong.persistentID];
                
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                UIImage *albumArtworkImage = nil;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (!albumArtworkImage)
                    albumArtworkImage = [UIImage imageNamed:@"ic_music"];
                
                NSString *strImageName = [NSString stringWithFormat:@"Runlive_%d%d.jpg", (int)[[NSDate date] timeIntervalSince1970], i];
                NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(albumArtworkImage, 0.9)];
                [imageData writeToFile:[[appDelegate createSongImagePath] stringByAppendingPathComponent:strImageName] atomically:YES];
                
                objMusic.song_image = strImageName;
                objMusic.album_image = strAlbImage;
                
                //objMusic.album_image
                
                NSLog(@"%@",[[appDelegate createSongImagePath] stringByAppendingPathComponent:strImageName]);
                
                appDelegate.appleMPMusicPlayerController = [MPMusicPlayerController systemMusicPlayer];
                [appDelegate.appleMPMusicPlayerController setQueueWithItemCollection:self.appleMusicCollection];
                appDelegate.appleMPMusicPlayerController.shuffleMode = MPMusicShuffleModeOff;
                NSData *collectionData=[NSKeyedArchiver archivedDataWithRootObject:self.appleMusicCollection];
                [CUserDefaults setObject:collectionData forKey:CAppleMusicCollection];
                [CUserDefaults synchronize];
                
            }
            else
            {
                objMusic.followers =  [NSString stringWithFormat:@"%ld", appDelegate.lFollowerCount];
                objMusic.album_image = self.imgURL.absoluteString;
                
                SPTPartialTrack *objTrack = arrMusicList[i];
                objMusic.song_name = objTrack.name;
                objMusic.song_url = objTrack.uri.absoluteString;
                objMusic.main_spotify_uri = URI.absoluteString;
                objMusic.isApple = [NSNumber numberWithBool:self.isApple];
                objMusic.playlist_name = appDelegate.strPlaylistName;
                
                NSString *nameArtist = @"";
                for (int i = 0; i < objTrack.artists.count; i++)
                {
                    SPTPartialArtist *artist = objTrack.artists[i];
                    nameArtist = [NSString stringWithFormat:@"%@%@",artist.name,(i == objTrack.artists.count-1)?@"":[NSString stringWithFormat:@",%@",nameArtist]];
                }
                objMusic.artist_name = nameArtist ? nameArtist : @"";
                
                SPTPartialAlbum *album = objTrack.album;
                SPTImage *image = album.smallestCover;

                NSString *strImageName = [NSString stringWithFormat:@"Runlive_%@.jpg",  objTrack.identifier];

                [[SDWebImageManager sharedManager] downloadImageWithURL:image.imageURL options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
                {
                    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.9)];
                    [imageData writeToFile:[[appDelegate createSongImagePath] stringByAppendingPathComponent:strImageName] atomically:YES];
                }];
                
                objMusic.song_image = strImageName;
            }
            
            [[[Store sharedInstance] mainManagedObjectContext] save];
        }
    }
    
    appDelegate.isMusicSelected = YES;
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
