//
//  ActivityAddPhotoCell.m
//  RunLive
//
//  Created by mac-00012 on 11/2/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityAddPhotoCell.h"

@implementation ActivityAddPhotoCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _doubleTapeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    [_doubleTapeGesture setNumberOfTouchesRequired:1];
    [_imgAddedPhoto addGestureRecognizer: _doubleTapeGesture];
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = 1;
    _viewUser.layer.borderColor = CRGB(235, 241, 245).CGColor;
    
//    _imgAddedPhoto.layer.cornerRadius = _viewVideo.layer.cornerRadius = 3;
    _imgAddedPhoto.layer.masksToBounds = _viewVideo.layer.masksToBounds = YES;
    
    _imgAddedPhoto.userInteractionEnabled = YES;

    self.lblActivityText.preferredMaxLayoutWidth = CGRectGetWidth(self.lblActivityText.frame);
    self.lblComment1.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment1.frame);
    self.lblComment2.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment2.frame);
    self.lblPhotoCaption.preferredMaxLayoutWidth = CGRectGetWidth(self.lblPhotoCaption.frame);
}


//- (void)playerItemDidReachEnd:(NSNotification *)notification
//{
//    AVPlayerItem *p = [notification object];
//    [p seekToTime:kCMTimeZero];
//}

#pragma mark - Photo Related Functionality
-(void)setImageHeightWidth:(NSString *)strImgWidth Height:(NSString *)strImgHeight PhotoActivity:(BOOL)isPhotoActivity
{
    _imgAddedPhoto.contentMode = UIViewContentModeScaleAspectFill;
    //......Available space for Video
    CGFloat wAvailable = CScreenWidth;
    
    //......Original width and height of video.
    CGFloat wOriginalVideo = strImgWidth.floatValue ;
    CGFloat hOriginalVideo = strImgHeight.floatValue;
    
    //......Scale to fit in (<= screen square rect)
    CGFloat hAspect = wOriginalVideo == 0 ? 0 : (hOriginalVideo) * (wAvailable / wOriginalVideo);
    self.cnImgHeight.constant = hAspect;//MIN(hAspect, hAvailable);
    
    if (isPhotoActivity)
    {
        if (Is_iPhone_5)
        {
            if (hAspect > 250)
            {
                self.cnImgHeight.constant = 250;
                _imgAddedPhoto.contentMode = UIViewContentModeScaleAspectFit;
            }
        }
        else if (Is_iPhone_6)
        {
            if (hAspect > 360)
            {
                self.cnImgHeight.constant = 360;
                _imgAddedPhoto.contentMode = UIViewContentModeScaleAspectFit;
            }
        }
        else if (Is_iPhone_6_PLUS)
        {
            if (hAspect > 400)
            {
                self.cnImgHeight.constant = 400;
                _imgAddedPhoto.contentMode = UIViewContentModeScaleAspectFit;
            }
        }
        else
        {
            if (hAspect > 460)
            {
                self.cnImgHeight.constant = 460;
                _imgAddedPhoto.contentMode = UIViewContentModeScaleAspectFit;
            }
        }
    }
    
}


#pragma mark - Video Related Functionality

-(void)setUpForVideoPlayer
{
    self.playerView = [[GUIPlayerView alloc] initWithFrame:CGRectMake(0, 0, CViewWidth(_viewVideo), CViewHeight(_viewVideo))];
    [_viewVideo addSubview:self.playerView];
    _imgVThumb.hidden = YES;
    

    NSURL *URL = [NSURL URLWithString:self.objVideoActivity.video_url];
    [self.playerView setVideoURL:URL];
//    NSLog(@"Video Seek Time ======= >>>> %@",self.objVideoActivity.video_seek_time);
    self.playerView.seekTime = CMTimeMakeWithSeconds(self.objVideoActivity.video_seek_time.floatValue, 1);
    [self.playerView prepareAndPlayAutomatically:YES];
    [_viewVideo bringSubviewToFront:_btnVideo];
    [_viewVideo bringSubviewToFront:_btnMuteUnmute];
    self.playerView.player.muted = self.objVideoActivity.video_mute.boolValue;
    self.btnVideo.selected = YES;
    self.btnMuteUnmute.selected = self.objVideoActivity.video_mute.boolValue;
    self.playerView.player.muted = self.btnMuteUnmute.selected;
}

-(void)StopVideoPlaying
{
    self.objVideoActivity.video_seek_time = [NSNumber numberWithFloat:CMTimeGetSeconds(self.playerView.player.currentTime)];
    [[Store sharedInstance].mainManagedObjectContext save];
    [self.playerView pause];
    self.imgVThumb.hidden = NO;
    self.btnVideo.selected = NO;
    [self.playerView clean];
    self.playerView = nil;
}


@end
