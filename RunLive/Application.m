//
//  Application.m
//  SocialNetworks
//
//  Created by mac-0001 on 16/04/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import "Application.h"

#import "SocialNetworks.h"

//#import <Master/Master.h>
//#import <Master/NSObject+NewProperty.h>

@implementation Application

- (BOOL)openURL:(NSURL*)url
{
    [[UIApplication sharedApplication] setBoolean:YES forKey:@"CustomWebView"];
    
    if (([[url absoluteString] hasPrefix:@"https://accounts.google.com/o/oauth2/auth"] || [[url absoluteString] hasPrefix:@"googlechrome-x-callback:"]) && ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"gplus://plus.google.com/"]]) // Prevent Google+ Safari Login
    {
        if ([[url absoluteString] hasPrefix:@"googlechrome-x-callback:"])
        {
            return NO;
        }
        else if ([[url absoluteString] hasPrefix:@"https://accounts.google.com/o/oauth2/auth"])
        {
            [self openLoginController:url socialNetwork:SocialNetworkGoogle];
            return NO;
        }
    }
    else if ([[url absoluteString] hasPrefix:@"https://www.linkedin.com/uas/oauth2/authorization"])  // Prevent LnkedIn Safari Login
    {
        [self openLoginController:url socialNetwork:SocialNetworkLinkdIn];
        return NO;
    }
    else if ([[url absoluteString] hasPrefix:@"https://api.instagram.com/oauth/authorize/?"])  // Prevent Instagram Safari Login
    {
        [self openLoginController:url socialNetwork:SocialNetworkInstagram];
        return NO;
    }
    
    return [super openURL:url];
}

-(void)openLoginController:(NSURL*)url socialNetwork:(SocialNetworkOptions)option
{
    
    NSString *network = nil;
    UIColor *barTintColor = nil;
    
    
    switch (option)
    {
        case SocialNetworkGoogle:
            network = @"Google+";
            barTintColor = CRGB(97,153,255);
            break;
        case SocialNetworkLinkdIn:
            network = @"LinkedIn";
            barTintColor = CRGB(97,153,255);
            break;
        case SocialNetworkInstagram:
            network = @"Instagram";
            barTintColor = CRGB(97,153,255);
            break;
        default:
            return;
            break;
    }
    
    UIViewController *controller = [[UIViewController alloc] init];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, controller.view.frame.size.width, controller.view.frame.size.height)];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
//    webView.delegate = self;
    [controller.view addSubview:webView];
    [[UIApplication topMostController] presentViewController:navigationController animated:YES completion:nil];
    
    controller.navigationController.navigationBar.barTintColor = barTintColor;
    controller.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    controller.navigationController.navigationBar.translucent = YES;
    controller.title = network;
    
    controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:nil];
    [controller.navigationItem.leftBarButtonItem clicked:^{
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
}

@end
