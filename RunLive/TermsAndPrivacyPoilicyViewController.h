//
//  TermsAndPrivacyPoilicyViewController.h
//  RunLive
//
//  Created by mac-0005 on 25/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndPrivacyPoilicyViewController : SuperViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UIWebView *webView;
    
}

@property (nonatomic,assign) BOOL isTOS;

@end
