//
//  OrdersViewController.m
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrderCell.h"
#import "OrderHeaderView.h"
#import "OrderDetailViewController.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController
{
    int iOffset;
    BOOL isApiStarted;
    
    NSMutableArray *arrOrderList;
    NSDateFormatter *dateFormatter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    superTable = tblOrder;
    [tblOrder registerNib:[UINib nibWithNibName:@"OrderCell" bundle:nil] forCellReuseIdentifier:@"OrderCell"];

    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMMM dd, yyyy";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];

    
    arrOrderList = [NSMutableArray new];
    [self getOrderListFromServer];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [appDelegate hideTabBar];
}

#pragma mark - Api Fuctions

- (void)loadMoreData
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self getOrderListFromServer];
}


-(void)getOrderListFromServer
{
 
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    
    isApiStarted = YES;
    
    [[APIRequest request] getOrderList:[NSString stringWithFormat:@"%d",iOffset] completed:^(id responseObject, NSError *error)
    {
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];
        
        isApiStarted = NO;
        
        if (responseObject && !error)
        {
            if (iOffset == 0)
                [arrOrderList removeAllObjects];
            
            iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
            
            [arrOrderList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            [tblOrder reloadData];
        }
    }];
}

#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOrderList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"OrderCell";
    OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
        cell = [[OrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    NSDictionary *dicData = arrOrderList[indexPath.row];
    
    cell.lblPoints.text = [dicData stringValueForJSON:@"points"];
    cell.lblStatus.text = [dicData stringValueForJSON:@"status"];
    cell.lblDate.text = [dicData stringValueForJSON:@"updatedAt"];

    NSDate *date = [self convertDateFromString:[dicData stringValueForJSON:@"updatedAt"] isGMT:YES formate:CDateFormater];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    cell.lblDate.text = strDate;
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    OrderHeaderView *viewHeader = [OrderHeaderView viewFromXib];
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = arrOrderList[indexPath.row];
    OrderDetailViewController *objOrderDet = [[OrderDetailViewController alloc] init];
    objOrderDet.strOrderId = [dicData stringValueForJSON:@"_id"];
    [self.navigationController pushViewController:objOrderDet animated:YES];
    
}


#pragma mark - Action Event

-(IBAction)btnCloseCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
