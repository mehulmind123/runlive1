//
//  ConnectAndShareViewController.h
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectAndShareCell.h"

@interface ConnectAndShareViewController : SuperViewController <UITableViewDelegate, UITableViewDataSource,UIDocumentInteractionControllerDelegate>
{
    IBOutlet UITableView *tblshare;
}

@property (nonatomic, retain) UIDocumentInteractionController *documentController;
- (IBAction)btnBackClicked:(id)sender;
@end
