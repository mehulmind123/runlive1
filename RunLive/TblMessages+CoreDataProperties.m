//
//  TblMessages+CoreDataProperties.m
//  RunLive
//
//  Created by mac-00012 on 7/31/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblMessages+CoreDataProperties.h"

@implementation TblMessages (CoreDataProperties)

+ (NSFetchRequest<TblMessages *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblMessages"];
}

@dynamic channelId;
@dynamic imgheight;
@dynamic imgname;
@dynamic imgurl;
@dynamic imgwidth;
@dynamic isNew;
@dynamic msgdate;
@dynamic msgdisplaytime;
@dynamic msgtext;
@dynamic msgtype;
@dynamic timestamp;
@dynamic userid;
@dynamic userimgurl;
@dynamic is_video;

@end
