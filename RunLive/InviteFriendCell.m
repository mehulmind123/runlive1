//
//  InviteFriendCell.m
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "InviteFriendCell.h"

@implementation InviteFriendCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.viewSelect.layer.borderColor = CRGB(71, 216, 253).CGColor;
    self.viewSelect.layer.borderWidth = 2;
    self.viewSelect.layer.cornerRadius = CGRectGetHeight(self.viewSelect.frame)/2;
    self.viewSelectInner.layer.cornerRadius = CGRectGetHeight(self.viewSelectInner.frame)/2;

    
    self.viewCircle.layer.cornerRadius = self.viewCircle.frame.size.width/2;
    self.viewCircle.layer.masksToBounds = YES;
    self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2;
    self.imgUser.layer.masksToBounds = YES;
//    self.viewCircle.layer.borderColor = CRGB(235, 241, 245).CGColor;
    self.viewCircle.layer.borderWidth = 1;

}

@end
