//
//  RunningViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoCropViewController.h"
#import "MIImageCropViewController.h"

typedef void(^playSpeechAndLocalPushNotification)(BOOL isSpeechPush);

@interface RunningViewController : SuperViewController<UIGestureRecognizerDelegate,AVAudioPlayerDelegate, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate,AVSpeechSynthesizerDelegate,CLLocationManagerDelegate,AgoraRtcEngineDelegate,VideoCropViewDelegate,MIImageCustomCropDelegate>
{
    IBOutlet UIView *viewLock,
    *viewUnlock,
    *viewVoiceChat,
    *viewLockCamera,
    *viewHeartBeat,
    *viewNoMusic,
    *viewMusic,
    *viewNextPlay,
    *viewPrevoiusPlay,
    *viewRunnerCount,
    *viewBasicDetail;
    
    IBOutlet KAProgressLabel *lblUnlockProgress;
    IBOutlet KAProgressLabel *lblCameraProgress;
    IBOutlet KAProgressLabel *lblVoiceChatProgress;
    
    IBOutlet UILabel *lblConnectionIssue;
    
    IBOutlet MGLMapView *mapView;
    IBOutlet UILabel
    *lblTime,
    *lblHearBeat,
    *lblAverageSpeed,
    *lblCurrentPosstion,
    *lblTotalRunDistance,
    *lblDistanceUnit;
    
    IBOutlet UISlider *sliderMusic;
    
    IBOutlet UIButton
    *btnRepeat,
    *btnBackWard,
    *btnPlay,
    *btnForward,
    *btnShuffle,
    *btnUpArrow;
    
    IBOutlet UILabel
    *lblSongName,
    *lblUserCount,
    *lbLongPressGestureInfo,
    *lbNoSongSelected;
    
    IBOutlet UIView *viewNoGPS;
    
    //----------Track Related UI Stuff-----------
    
    IBOutlet UICollectionView *clGraph,*clMeter;
    IBOutlet UIScrollView *scrollView;
    IBOutlet NSLayoutConstraint *cnTrackViewHeight,*cnTrackViewTopSpace;
}

@property (nonatomic, strong) AVAudioPlayer *avAudioPlayer;

@property (nonatomic, copy) playSpeechAndLocalPushNotification configurePlaySpeechAndLocalPushNotification;

@end
