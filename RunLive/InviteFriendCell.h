//
//  InviteFriendCell.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *viewCircle,*viewSelect,*viewSelectInner;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelect;
@property (strong, nonatomic) IBOutlet UIImageView *imgVerifiedUser;




@end
