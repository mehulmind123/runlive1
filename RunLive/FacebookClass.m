//
//  Facebook.m
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "FacebookClass.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>



@interface NSObject ()


// FBSDKAccessToken.h

+ (id)currentAccessToken;

- (BOOL)hasGranted:(NSString *)permission;


// FBSDKLoginManager.h


@property (assign, nonatomic) NSUInteger loginBehavior;

- (void)logOut;

- (void)logInWithReadPermissions:(NSArray *)permissions handler:(id)handler;

- (void)logInWithPublishPermissions:(NSArray *)permissions handler:(id)handler;


// FBSDKGraphRequest.h

- (instancetype)initWithGraphPath:(NSString *)graphPath parameters:(NSDictionary *)parameters;

- (instancetype)initWithGraphPath:(NSString *)graphPath parameters:(NSDictionary *)parameters HTTPMethod:(NSString *)HTTPMethod;

- (id)startWithCompletionHandler:(id)handler;



// FBSDKApplicationDelegate.h

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

+ (instancetype)sharedInstance;


// FBSDKShareDialog.h

+ (instancetype)showFromViewController:(UIViewController *)viewController withContent:(id)content delegate:(id)delegate;


// FBSDKShareLinkContent.h

@property (nonatomic, copy) NSString *contentDescription;
@property (nonatomic, copy) NSString *contentTitle;
@property (nonatomic, copy) NSURL *imageURL;


// FBSDKSharingContent.h

@property (nonatomic, copy) NSURL *contentURL;


@end



@interface FacebookClass ()
{
    Class FBSDKAccessToken;
    Class FBSDKLoginManager;
    Class FBSDKGraphRequest;
}
@end



@implementation FacebookClass

+ (instancetype)sharedInstance
{
    static FacebookClass *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[FacebookClass alloc] init];

        NSAssert(NSClassFromString(@"FBSDKAccessToken"), @"Please import FBSDKCoreKit.framework");
        NSAssert(NSClassFromString(@"FBSDKLoginManager"), @"Please import FBSDKLoginKit.framework");

        
        if (([[SocialNetworks sharedInstance] facebookAppId]).length<=0)
            NSAssert(nil, @"Add FacebookAppId using [[SocialNetworks sharedInstance] setFacebookAppId:]");
        
        if ([[SocialNetworks sharedInstance] facebookCallBackURL].length<=0)
            NSAssert(nil, @"Add FacebookCallBackURL (This must be in format \"fb<facebook_app_id>\") to URLSchemes with name \"Facebook\" to Info.plist and [[SocialNetworks sharedInstance] setFacebookCallBackURL:]");

            _sharedInstance->FBSDKAccessToken = NSClassFromString(@"FBSDKAccessToken");
            _sharedInstance->FBSDKLoginManager = NSClassFromString(@"FBSDKLoginManager");
            _sharedInstance->FBSDKGraphRequest = NSClassFromString(@"FBSDKGraphRequest");
        
    });
    
    return _sharedInstance;
}

- (BOOL)isAunthenticated
{
    return ([FBSDKAccessToken currentAccessToken]?YES:NO);
}

+ (void)logout
{
    [[[NSClassFromString(@"FBSDKLoginManager") alloc] init] logOut];
}

#pragma mark - # MI-Facebook

-(void)LoginWithFacebook:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    id loginManager = [[FBSDKLoginManager alloc] init];
    
    [loginManager setLoginBehavior:0];  //  FBSDKLoginBehaviorSystemAccount
    
    [loginManager logInWithReadPermissions: @[@"public_profile",@"email", @"user_friends", @"read_custom_friendlists"] handler:^(id result, NSError *error) {
        
        NSLog(@"Facebook Login Response Error = %@",error);
        
        if (completion)
            completion(result,error,error?NO:YES);
    }];
    
    
//    [loginManager logInWithReadPermissions:[[SocialNetworks sharedInstance] facebookReadPermissions]?[[SocialNetworks sharedInstance] facebookReadPermissions]:nil handler:^(id result, NSError *error) {
//        
//        NSLog(@"Facebook Login Response Error = %@",error);
//        
//        if (completion)
//            completion(result,error,error?NO:YES);
//        
//    }];
}

-(void)checkAndLoadActiveSession:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    if (![self isAunthenticated])
    {
        [self LoginWithFacebook:^(NSDictionary *result, NSError *error, BOOL status) {
            
            if (completion)
                completion(result,error,status);
            
        }];
    }
    else
    {
        if (completion)
            completion(nil,nil,YES);
    }
    
}

-(void)requestPublishPermissions:(NSArray *)permissions completion:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    [self requestPermissions:permissions readPermissions:NO completion:completion];
}

-(void)requestReadPermissions:(NSArray *)permissions completion:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    [self requestPermissions:permissions readPermissions:YES completion:completion];
}

-(void)requestPermissions:(NSArray *)permissions readPermissions:(BOOL)isReadPermissions completion:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    [self checkAndLoadActiveSession:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (completion)
                completion(result,error,status);
            
            return;
        }
        
        
        NSMutableArray *arrayPermissionsRequired = [[NSMutableArray alloc] init];
        
            for (NSString *permission in permissions)
            {
                if (![[FBSDKAccessToken currentAccessToken] hasGranted:permission])
                    [arrayPermissionsRequired addObject:permission];
            }
            
            if (arrayPermissionsRequired.count == 0)
            {
                if (completion)
                    completion(nil,error,YES);
            }
            else
            {
                id handler = ^(id result, NSError *error) {
                    
                    NSLog(@"Facebook %@ Permission Response = %@",isReadPermissions?@"Read":@"Publish",error);
                    
                    if (completion)
                        completion(result,error,error?NO:YES);
                    
                };
                
                
                id loginManager = [[FBSDKLoginManager alloc] init];
                
                [loginManager setLoginBehavior:2];  //  FBSDKLoginBehaviorSystemAccount
                
                if (isReadPermissions)
                    [loginManager logInWithReadPermissions:arrayPermissionsRequired handler:handler];
                else
                    [loginManager logInWithPublishPermissions:arrayPermissionsRequired handler:handler];
            }
            
    }];
}

-(void)LoadFacebookUserDetails:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion
{
    [self checkAndLoadActiveSession:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (completion)
                completion(result,error,status);
            
            return;
        }
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"first_name,last_name,email,name,gender"}] performSelector:NSSelectorFromString(@"startWithCompletionHandler:") withObject:^(id connection, id result, NSError *error) {
            if (completion)
                completion(result,error,error?NO:YES);
        }];
        
    }];
}

-(void)postOnFacebook:(PostParameters *)data completion:(void (^)(NSError *error,BOOL status))completion
{
    [self requestPublishPermissions:@[@"publish_actions"] completion:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (completion)
                completion(error,status);
            
            return;
        }

        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        
        if (data.comment)
            [postParams setObject:data.comment forKey:@"message"];
        
        if (data.websiteURL)
            [postParams setObject:data.websiteURL forKey:@"link"];
        
        if (data.imageURL)
            [postParams setObject:data.imageURL forKey:@"picture"];
        
        if (data.name)
            [postParams setObject:data.name forKey:@"name"];
        
        if (data.caption)
            [postParams setObject:data.caption forKey:@"caption"];
        
        if (data.description)
            [postParams setObject:data.description forKey:@"description"];
        
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed" parameters:postParams HTTPMethod:@"POST"] performSelector:NSSelectorFromString(@"startWithCompletionHandler:") withObject:^(id connection, id result, NSError *error) {

            NSLog(@"Facebook Post Response = %@",error?error:result);

            if (completion)
                completion(error,error?NO:YES);
        }];
        
    }];
    
}


-(void)shareOnFacebook:(PostParameters*)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    NSAssert(NSClassFromString(@"FBSDKShareLinkContent"), @"Please import FBSDKShareKit.framework");
    
    
    NSObject *linkcontent = [[NSClassFromString(@"FBSDKShareLinkContent") alloc] init];

    linkcontent.contentURL = data.websiteURL;

    linkcontent.imageURL = data.imageURL;
    
    linkcontent.contentTitle = data.name?:data.comment;
    
    linkcontent.contentDescription = data.description;
    
    [NSClassFromString(@"FBSDKShareDialog") showFromViewController:[UIApplication topMostController] withContent:linkcontent delegate:nil];
}

-(void)fetchFriends:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    [self checkAndLoadActiveSession:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (failure)
                failure(error);
            
            return;
        }
        
        [self processRequest:@"me/friends" method:@"GET" parameters:nil success:success failure:failure];
    }];
}


- (void)createAbum:(NSDictionary *)album success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    [self requestReadPermissions:@[@"user_photos"] completion:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (failure)
                failure(error);
            
            return;
        }
        
        [self processRequest:@"/me/albums" method:@"POST" parameters:album success:success failure:failure];
    }];
}

- (void)uploadPhoto:(NSDictionary *)image onAlbum:(NSString *)album success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    [self requestPublishPermissions:@[@"publish_actions"] completion:^(NSDictionary *result, NSError *error, BOOL status) {
        
        if (!status)
        {
            if (failure)
                failure(error);
            
            return;
        }
        
        [self processRequest:[NSString stringWithFormat:@"/%@/photos",album] method:@"POST" parameters:image success:success failure:failure];
    }];
}

-(void)processRequest:(NSString *)request method:(NSString *)httpMethod parameters:(NSDictionary *)parameters success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:request parameters:parameters HTTPMethod:httpMethod] performSelector:NSSelectorFromString(@"startWithCompletionHandler:") withObject:^(id connection, id result, NSError *error) {
        
        NSLog(@"Facebook Graph Request - %@ === %@",request,error?error:result);
        
        if (error)
        {
            if (failure)
                failure(error);
        }
        else
        {
            if (success)
                success(result);
        }
    }];
}

+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[NSClassFromString(@"FBSDKApplicationDelegate") sharedInstance] application:[UIApplication sharedApplication]
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end

