//
//  DayCell.h
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lblDay;
@property(weak,nonatomic) IBOutlet UILabel *lblName;
@end
