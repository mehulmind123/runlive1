//
//  SessionTypeEvenUser.m
//  RunLive
//
//  Created by mac-00012 on 11/19/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionTypeEvenUser.h"

@implementation SessionTypeEvenUser

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.btnGoBack.layer.cornerRadius = self.btnProceed.layer.cornerRadius = 3;
    self.center = CScreenCenter;
}

@end
