//
//  ResetPasswordViewController.h
//  RunLive
//
//  Created by mac-0008 on 22/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface ResetPasswordViewController : SuperViewController
{
    IBOutlet UIButton *btnBack;
    IBOutlet UILabel *lblResetPassword;
    IBOutlet UILabel *lblInfoText;
    IBOutlet ACFloatingTextField *txtEmail;
    IBOutlet UIButton *btnReset;
    
}

@end
