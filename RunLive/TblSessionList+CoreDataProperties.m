//
//  TblSessionList+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 29/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionList+CoreDataProperties.h"

@implementation TblSessionList (CoreDataProperties)

+ (NSFetchRequest<TblSessionList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblSessionList"];
}

@dynamic distance;
@dynamic isSolo;
@dynamic session_date;
@dynamic session_end_time;
@dynamic session_id;
@dynamic session_name;
@dynamic session_time;
@dynamic user_id;
@dynamic isRunning;
@dynamic joinedUser;
@dynamic otherUser;
@dynamic sessionDate;
@dynamic syncedUser;

@end
