//
//  AppDelegate.h
//  RunLive
//
//  Created by mac-00015 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

#import "CustomTabBar.h"
#import "SuperNavigationController.h"
#import "ReadMSGCell.h"
#import "TblUser+CoreDataClass.h"
#import "TblSessionList+CoreDataClass.h"
#import <CoreLocation/CoreLocation.h>
#import <OneSignal/OneSignal.h>
#import <PubNub/PubNub.h>
#import <UserNotifications/UserNotifications.h>
#import "MQTTKit.h"

#pragma mark - Session Update Definitions
#pragma mark -

typedef enum
{
    DeleteSession,
    JoinSession,
    UnjoinSession,
    SyncSession,
    EditSession,
    ResultSession,
    CommentOnSession,
    CommentOnActivity,
} RefreshSessionWithUpdate;

typedef enum
{
    DeleteActivity,
    RefreshActivityList,
    CallActivityApi,
} ActivityData;


#pragma mark - Block Definitions
#pragma mark -

typedef void(^runningSessionSetMapPossition)(double latitude, double longitude,CLLocationManager *manager, NSError *error);
typedef void(^runningSessionStart)(NSString *strSubscribeSessionId);
typedef void(^btnUserNameSelect)(NSString *strUserName, BOOL isLoginUser);
typedef void(^spotifyLogin)(NSError *);
typedef void(^didChangePosition)(NSTimeInterval);
typedef void(^trackChanged)(SPTPlaybackMetadata* );
typedef void(^newTrackStarted)(NSError* );
typedef void(^trackStopped)(BOOL stopPlaying);
typedef void(^startedPlaying)();
typedef void(^gotMessage)(NSDictionary *);

typedef void(^showTypingIndicator)(TblChats *objChat);

typedef void(^refreshSessionData)(RefreshSessionWithUpdate refreshSessionWithUpdate);
typedef void(^refreshNotificationListScreen)(BOOL isRefreshSessionList);
typedef void(^refreshMainActivityList)(ActivityData activityData);
typedef void(^refreshMyProfileActivityList)(ActivityData activityData);
typedef void(^refreshOtherProfileActivityList)(ActivityData activityData);
typedef void(^resubscribeOnMQTT)(BOOL isSubscribe);
typedef void(^refreshChatListScreen)(BOOL isRefresh);
typedef void(^planPurchaseSuccessfully)(SKPaymentTransaction *transctions,NSError *error);

typedef void(^gpsPermissionStatus)(BOOL isAllow);
typedef void(^notificationPermissionStatus)(BOOL isGranted);
typedef void(^stravaCallBackAfterLogin)(BOOL isConnected);


@import HealthKit;

@interface AppDelegate : BasicAppDelegate <UIApplicationDelegate,UIDocumentInteractionControllerDelegate, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate,MFMailComposeViewControllerDelegate, UNUserNotificationCenterDelegate,OSSubscriptionObserver,OSPermissionObserver,UIActionSheetDelegate /*,SKPaymentTransactionObserver,SKProductsRequestDelegate*/ >

#pragma mark - Block Declarations
#pragma mark -

@property (nonatomic, copy) gpsPermissionStatus configureGpsPermissionStatus;
@property (nonatomic, copy) notificationPermissionStatus configureNotificationPermissionStatus;
@property (nonatomic, copy) stravaCallBackAfterLogin configureStravaCallBackAfterLogin;

@property (nonatomic, copy) runningSessionSetMapPossition configureRunningSessionSetMapPossition;
@property (nonatomic, copy) runningSessionStart configureRunningSessionStart;
@property (nonatomic, copy) btnUserNameSelect configureBtnUserNameSelect;
@property (nonatomic, copy) spotifyLogin configureSpotifyLogin;
@property (nonatomic, copy) didChangePosition configureDidChangePosition;
@property (nonatomic, copy) trackChanged configureTrackChanged;
@property (nonatomic, copy) newTrackStarted configureNewTrackStarted;
@property (nonatomic, copy) trackStopped configureTrackStopped;
@property (nonatomic, copy) startedPlaying configureStartedPlaying;
@property (nonatomic, copy) refreshNotificationListScreen configureRefreshNotificationListScreen;
@property (nonatomic, copy) refreshSessionData configureRefreshSessionData;
@property (nonatomic, copy) refreshMainActivityList configureRefreshMainActivityList;
@property (nonatomic, copy) refreshMyProfileActivityList configureRefreshMyProfileActivityList;
@property (nonatomic, copy) refreshOtherProfileActivityList configureRefreshOtherProfileActivityList;
@property (nonatomic, copy) resubscribeOnMQTT configureResubscribeOnMQTT;
@property (nonatomic, copy) refreshChatListScreen configureRefreshChatListScreen;
@property (nonatomic, copy) gotMessage configureGotMessage;
@property (nonatomic, copy) showTypingIndicator configureShowTypingIndicator;

@property(strong,nonatomic) planPurchaseSuccessfully configurePlanPurchaseSuccessfully;

@property (strong, nonatomic) TblUser *loginUser;
@property (strong, nonatomic) TblSessionList *objSessionGoingToStart;
@property (nonatomic, assign) BOOL isStartSessionPopUp;
@property (nonatomic, assign) BOOL isSharingVideoPhotoOnFacebook;



#pragma mark - Properties
#pragma mark - 

@property (strong, nonatomic) OneSignal *oneSignal;

@property (nonatomic, strong) MQTTClient *objMQTTClient;

@property (strong, nonatomic) CMMotionActivityManager *objMotionManager;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property (nonatomic, assign) Twitter *objTwitter;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *objTabBarController;
@property (nonatomic, strong) CustomTabBar *objCustomTabBar;
@property (nonatomic, assign) NSUInteger tabBarSelectedIndex;
@property (nonatomic, assign) BOOL isChangingProgress;
@property (nonatomic, assign) BOOL isShowActivityScreen;
@property (nonatomic, assign) BOOL isMusicSelected;
@property (nonatomic, assign) BOOL isMoveOnNotificationScreen;
@property (nonatomic, assign) BOOL isUserOpenSessionListScreen;

@property (nonatomic, assign) long lFollowerCount;
@property (nonatomic, strong) UIImage *imgTempMusic;
@property (nonatomic, assign) NSString *strPlaylistName;

@property (nonatomic, assign) NSString *strMainURI;

@property(strong,nonatomic) MPMediaItemCollection *appleRunningMusicCollection;
@property(strong,nonatomic) MPMusicPlayerController *appleMPMusicPlayerController;


@property (nonatomic, strong) SPTAuth *auth;
@property (nonatomic, strong) SPTSession *session;
@property (nonatomic, strong) SPTAudioStreamingController *player;
@property (nonatomic ,strong) SuperNavigationController *objSuperReward,*objSuperIntro,*objSuperActivityFeed,*objSuperMyProfile,*objSuperSession,*objSuperSessionList,*objSuperAccountSeting,*objSuperRunning;

- (UITabBarController *)setTabBarController;

@property (nonatomic) HKHealthStore *healthStore;

@property(nonatomic, strong) PubNub *client;
@property(nonatomic, strong) PNConfiguration *myConfig;
@property(nonatomic, strong) NSString *channel1;
@property(nonatomic, strong) NSString *channel2;
@property(nonatomic, strong) NSString *channelGroup1;
@property(nonatomic, strong) NSString *subKey;
@property(nonatomic, strong) NSString *pubKey;
@property(nonatomic, strong) NSString *authKey;



#pragma mark - Methods
#pragma mark -


                /* ...................................................Common Function...................................................*/
- (void)keepAppInIdleMode:(BOOL)isIdle;
- (UIViewController *)getTopMostViewController;
- (void)hideTabBar;
- (void)showTabBar;
- (void)openSharingOption:(UIImage *)image
                VideoURL:(NSString *)strVideoURL
                   Video:(BOOL)isVideo
                  TabBar:(BOOL)isTapBar;

- (void)roundedImageView:(UIImageView *)imgView
           withSuperView:(UIView *)vwBorder;

- (void)moveOnProfileScreen:(NSString *)userName
                     UserId:(NSString *)userId
              andNavigation:(UINavigationController *)navigation
                isLoginUser:(BOOL)isLogin;

- (void)logoutUser;
- (NSString *)getDistanceInWithSelectedFormate:(NSString *)strDistance;
- (NSString *)convertKMToMeter:(NSString *)strMeter IsKM:(BOOL)isKm;
- (NSString *)convertCaloriesWithSelectedFormate:(NSString *)calories;
- (NSString *)convertSpeedInWithSelectedFormate:(float)meters
                                       overTime:(NSString *)strTotalTime;
- (void)showMessageBadgeOnTabBar;
- (void)setNotificationBadgeOnAppIcon;


            /* ...................................................Activity Related Function...................................................*/

- (void)hideKeyboard;
- (NSString *)getCommentAndLikeCountWithFormate:(NSString *)strCount;
- (NSString *)getPostTimeInSpecificFormate:(NSString *)strDate;
- (UIImage *)GetImageWithUserPosition:(int)position;
- (UIImage *)GetImageWithUserPerformance:(NSString *)strPosition;
- (void)likeActivity:(NSString *)act_id
              Status:(NSString *)status
        ActivityType:(NSString *)strActType;
- (void)updateActivityListData:(ActivityData)actData
                       MainWis:(BOOL)isMain;
- (void)deletePhotoVideoActivity:(NSString *)strActivityId
                       completed:(void (^)(id responseObject,NSError *error))completion;


            /* ...................................................Push Notification Related Function...................................................*/

-(void)registerDeviceTokenForNotification;
-(void)enableRemoteNotification;
-(void)oneSignalRegistration;


#pragma mark - Spotify Methods
#pragma mark -

- (void)setSpotifyPlayer;
- (void)closeSession;
- (void)activateAudioSession;
- (void)deactivateAudioSession;
- (BOOL)isSpotifySessionValid:(SPTAuth*)auth;


#pragma mark - Pubnub
#pragma mark -

- (void)handleStatus:(PNStatus *)status;

- (void)pubNubAddPushNotifications;

- (void)SetPubNubClientState:(NSString *)state;

- (void)configurePubnub;

-(void)addPushNotificationToPubnubChannels:(NSArray *)arrChannels;

-(void)SetPubNubClientTypingState:(BOOL)isTyping
                          Channel:(NSString *)strChannelId;

#pragma mark - Image Related Functions
#pragma mark -

- (NSString*)createSongImagePath;
- (void)deleteSongImagePath;
- (NSString*)createAlbumImagePath;
- (void)deleteAlbumImagePath;
-(NSURL *)resizeImage:(NSString *)width Height:(NSString *)height imageURL:(NSString *)url;


/*...............................Live Session Related function...............................*/
#pragma mark - Live Session Related Functions

-(void)createLiveSession:(NSDictionary *)dicData
               completed:(void (^)(id responseObject,NSError *error))completion;

-(void)QuitJoinedLiveRunSession:(NSString *)strSessionId
                      completed:(void (^)(id responseObject,NSError *error))completion;

-(void)startRunningWithSessionID:(NSString *)session_id
                 currentLocation:(CLLocation *)currentLocation
                       completed:(void (^)(NSString *strSessionId,BOOL isError))completion;

-(void)toCheckRunningSessionIsAvailable;

-(void)showCountDownScreenForManualSession:(NSString *)session_id
                                 completed:(void (^)(id responseObject,NSError *error))completion;

/*...............................MQTT Related function...............................*/

#pragma mark - MQTT Functions

-(void)MQTTInitialSetup;

-(void)MQTTDisconnetFromServer;

-(void)MQTTUnsubscribeWithTopic:(NSString *)strTopic;

-(void)MQTTSubscribeWithTopic:(NSString *)strTopic;

-(void)MQTTPublishWithTopic:(NSData *)data
                      Topic:(NSString *)strTopic;

-(void)MQTTNotifyToServerForUserAvailability:(NSDictionary *)dicData
                                       Topic:(NSString *)strTopic;

#pragma mark - Motion Detector Methods
-(void)initializeMotionDetector;

#pragma mark - Location Manager Methods

-(void)initialSetUpForLocaitonMananger;

-(void)getLocationNameFromLatLong:(double)lat
                          andLong:(double)lng
                        completed:(void(^)(NSDictionary *dicAddress))completion;

@end
