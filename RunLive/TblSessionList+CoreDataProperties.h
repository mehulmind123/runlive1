//
//  TblSessionList+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 29/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblSessionList (CoreDataProperties)

+ (NSFetchRequest<TblSessionList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *distance;
@property (nullable, nonatomic, copy) NSNumber *isSolo;
@property (nullable, nonatomic, copy) NSString *session_date;
@property (nullable, nonatomic, copy) NSNumber *session_end_time;
@property (nullable, nonatomic, copy) NSString *session_id;
@property (nullable, nonatomic, copy) NSString *session_name;
@property (nullable, nonatomic, copy) NSString *session_time;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSNumber *isRunning;
@property (nullable, nonatomic, retain) NSSet<TblJoinedUser *> *joinedUser;
@property (nullable, nonatomic, retain) NSSet<TblSessionOtherUser *> *otherUser;
@property (nullable, nonatomic, retain) TblSessionListDate *sessionDate;
@property (nullable, nonatomic, retain) NSSet<TblSessionSyncedUser *> *syncedUser;

@end

@interface TblSessionList (CoreDataGeneratedAccessors)

- (void)addJoinedUserObject:(TblJoinedUser *)value;
- (void)removeJoinedUserObject:(TblJoinedUser *)value;
- (void)addJoinedUser:(NSSet<TblJoinedUser *> *)values;
- (void)removeJoinedUser:(NSSet<TblJoinedUser *> *)values;

- (void)addOtherUserObject:(TblSessionOtherUser *)value;
- (void)removeOtherUserObject:(TblSessionOtherUser *)value;
- (void)addOtherUser:(NSSet<TblSessionOtherUser *> *)values;
- (void)removeOtherUser:(NSSet<TblSessionOtherUser *> *)values;

- (void)addSyncedUserObject:(TblSessionSyncedUser *)value;
- (void)removeSyncedUserObject:(TblSessionSyncedUser *)value;
- (void)addSyncedUser:(NSSet<TblSessionSyncedUser *> *)values;
- (void)removeSyncedUser:(NSSet<TblSessionSyncedUser *> *)values;

@end

NS_ASSUME_NONNULL_END
