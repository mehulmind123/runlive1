//
//  CustomTwoButtonAlertView.m
//  RunLive
//
//  Created by mac-0005 on 14/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "CustomTwoButtonAlertView.h"

@implementation CustomTwoButtonAlertView

+(id)initCustomTwoButtonAlertView
{
    CustomTwoButtonAlertView *objPopUp = [[[NSBundle mainBundle]loadNibNamed:@"CustomTwoButtonAlertView" owner:nil options:nil] lastObject];
    CViewSetWidth(objPopUp, CScreenWidth);
    CViewSetHeight(objPopUp, CScreenHeight);
    return objPopUp;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.viewContainer.layer.cornerRadius = self.btnFirst.layer.cornerRadius = self.btnSecond.layer.cornerRadius = 3;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.center = CScreenCenter;
}

#pragma mark - Configuartion

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonFirstText:(NSString *)strBtnFirst ButtonSecondText:(NSString *)strBtnSecond
{
    lblTitle.text = strTitle;
    lblMessage.text = strMessage;
    [self.btnFirst setTitle:strBtnFirst forState:UIControlStateNormal];
    [self.btnSecond setTitle:strBtnSecond forState:UIControlStateNormal];
    
}

@end
