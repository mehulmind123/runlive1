//
//  RunningMusicViewController.h
//  RunLive
//
//  Created by mac-0006 on 22/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^getMusicData)(int isMusicNum, int currentTime, BOOL isSongPlaying, BOOL isRepeat, BOOL isShuffle);

@interface RunningMusicViewController : SuperViewController <UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate, UIGestureRecognizerDelegate>
{
    
    IBOutlet UILabel *lblArtistName;
    IBOutlet UILabel *lblSongName;
    IBOutlet UILabel *lblCurrentAndTotalTime;
    
    IBOutlet UIButton *btnGrid;
    IBOutlet UIButton *btnRepeat;
    IBOutlet UIButton *btnPlayBack;
    IBOutlet UIButton *btnPlay;
    IBOutlet UIButton *btnPlayNext;
    IBOutlet UIButton *btnShuffleLast;
    
    IBOutlet UIView *viewPrevoiusPlay;
    IBOutlet UIView *viewNextPlay;
    IBOutlet UIView *viewGrid;
    IBOutlet UIView *viewBtnContent;
    IBOutlet UISlider *sliderMusic;
    
    IBOutlet UITableView *tblMusicList;
    IBOutlet UICollectionView *collMusic;
    
    AVAudioPlayer *avAudioPlayer;
}

@property int currentplayMusicNo;
@property int currentTimeForSong;

@property(strong,nonatomic) NSArray *arrMusicList;
@property(atomic,assign) BOOL isSongPlaying;
@property(atomic,assign) BOOL isRepeatMusic;
@property(atomic,assign) BOOL isShuffleMusic;

@property(strong,nonatomic) getMusicData configureGetMusicData;

- (IBAction)btnCloseClicked:(id)sender;
- (IBAction)btnGridAndListClicked:(id)sender;
- (IBAction)MoveMusicSlider:(UISlider *)sender;

@end
