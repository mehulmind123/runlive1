//
//  SettingViewController.m
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SettingViewController.h"
#import "AudioSettingViewController.h"
#import "BlockedUserViewController.h"
#import "AboutViewController.h"
#import "PrivacySettingViewController.h"
#import "NotificationSettingViewController.h"
#import "PremiumView.h"
#import "InAppPurchaseViewController.h"
#import "VoiceChatSettingViewController.h"



@interface SettingViewController ()
{
    NSArray *arrSection, *arrAccTitle, *arrRunTitle, *arrAppTitle;
}
@end

@implementation SettingViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

//    if ([self isPremiumPlanIsActive])
//        arrAccTitle = [NSArray arrayWithObjects:@"Account",@"Subscribed", nil];
//    else
//        arrAccTitle = [NSArray arrayWithObjects:@"Account",@"Subscription", nil];
    
    arrAccTitle = [NSArray arrayWithObjects:@"Account", nil];

//    arrRunTitle = [NSArray arrayWithObjects:@"Workout Settings",@"Audio Settings",@"Smartwatches",@"Accessory Settings",@"Voice Chat Setting",nil];

    arrRunTitle = [NSArray arrayWithObjects:@"Audio Settings",@"Voice Chat Setting",nil];

    arrAppTitle = [NSArray arrayWithObjects:@"Connect & Share",@"Notifications Settings",@"Privacy",@"Location Settings",@"Blocked Users", nil];
    
    btnLogout.layer.cornerRadius = 3;
    
    [tblSetting registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [appDelegate hideTabBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView Datasource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section != 3)
        return 50;
    else
        return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblSetting.frame.size.width, 50)];
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, viewHeader.frame.size.width, viewHeader.frame.size.height-5)];
    
    if(section == 0)
        lblTitle.text = @"ACCOUNT SETTINGS";
    else if (section == 1)
        lblTitle.text = @"RUNNING SETTINGS";
    else if (section == 2)
        lblTitle.text = @"APPLICATION SETTINGS";
    else
        lblTitle.text = @"";
    
    viewHeader.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = CRGB(122, 126, 147);
    lblTitle.font = [UIFont fontWithName:@"SolidoCondensed-Bold" size:15];
    [viewHeader addSubview:lblTitle];
    return viewHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return arrAccTitle.count;
    else if (section == 1)
        return arrRunTitle.count;
    else if (section == 2)
        return arrAppTitle.count;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"SettingCell";
    SettingCell *cell = [tblSetting dequeueReusableCellWithIdentifier:strIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.section == 0)
        cell.lblTitle.text = [arrAccTitle objectAtIndex:indexPath.row];
    else if (indexPath.section == 1)
        cell.lblTitle.text = [arrRunTitle objectAtIndex:indexPath.row];
    else if (indexPath.section == 2)
        cell.lblTitle.text = [arrAppTitle objectAtIndex:indexPath.row];
    else
        cell.lblTitle.text = @"Help & Support";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)         // Account
        {
            AccountSettingViewController *objAccount = [[AccountSettingViewController alloc]initWithNibName:@"AccountSettingViewController" bundle:nil];
            [self.navigationController pushViewController:objAccount animated:YES];
        }
        /*
        else
        {

            NSDate *dtCreated = [self convertDateFromString:appDelegate.loginUser.subscription_exp_date isGMT:YES formate:CDateFormater];
            NSTimeInterval currentDateTimeStamp = [[NSDate date] timeIntervalSince1970];
            NSTimeInterval SesisonExpireDateTimeStamp = [dtCreated timeIntervalSince1970];
            
            if ([self isPremiumPlanIsActive])
            {
                if (appDelegate.loginUser.is_premium.boolValue)
                {
                    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:@"You are currently subscribed to RunLive Premium." buttonTitle:@"Ok" handler:^(UIAlertAction *action) {
                        //
                    } inView:self];
                }
                else if ([appDelegate.loginUser.subscription_plan isEqual:@0])
                {
                    int iDaysDiff = (SesisonExpireDateTimeStamp - currentDateTimeStamp) / 86400;
                    [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:[NSString stringWithFormat:@"You currently have a free trial for %d more days. Enjoy using RunLive for free!",iDaysDiff] buttonTitle:@"Ok" handler:^(UIAlertAction *action) {
                        //
                    } inView:self];
                }
                else
                {
                    NSString *strAlertMessage = [appDelegate.loginUser.subscription_plan isEqual:@1] ? CMessageMonthlySubcrition : CMessageYearlySubcrition;
                    
                    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:strAlertMessage firstButton:@"Cancel Subscription" firstHandler:^(UIAlertAction *action)
                     {
                         // Cancel Subscription here....
                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
                     } secondButton:@"Dismiss" secondHandler:^(UIAlertAction *action)
                     {
                         //
                     } inView:self];
                }
            }
            else
            {
                InAppPurchaseViewController *objInApp = [[InAppPurchaseViewController alloc] init];
                [self presentViewController:objInApp animated:YES completion:nil];
            }
        }
        */
    }
    else if(indexPath.section == 1)
    {
        switch (indexPath.row) {
//            case 0:
//            case 2:
//            case 3:
//            {
//                [[PPAlerts sharedAlerts]showAlertWithType:0 withMessage:@"Feature coming soon." withTitle:[arrRunTitle objectAtIndex:indexPath.row]];
//            }
//                break;
            case 0: // Audio Setting
            {
                AudioSettingViewController *objAudio = [[AudioSettingViewController alloc]initWithNibName:@"AudioSettingViewController" bundle:nil];
                [self.navigationController pushViewController:objAudio animated:YES];
            }
                break;
            case 1: // Voice chat Setting
            {
                VoiceChatSettingViewController *objAudio = [[VoiceChatSettingViewController alloc]initWithNibName:@"VoiceChatSettingViewController" bundle:nil];
                [self.navigationController pushViewController:objAudio animated:YES];
                
                /*
                if ([self isPremiumPlanIsActive])
                {
                    VoiceChatSettingViewController *objAudio = [[VoiceChatSettingViewController alloc]initWithNibName:@"VoiceChatSettingViewController" bundle:nil];
                    [self.navigationController pushViewController:objAudio animated:YES];
                }
                else
                {
                    InAppPurchaseViewController *objInApp = [[InAppPurchaseViewController alloc] init];
                    [self presentViewController:objInApp animated:YES completion:nil];
                }
                 */
            }
                break;

            default:
                break;
        }
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == 0)        // Connect & Share
        {
            ConnectAndShareViewController *objConnect = [[ConnectAndShareViewController alloc]initWithNibName:@"ConnectAndShareViewController" bundle:nil];
            [self.navigationController pushViewController:objConnect animated:YES];
        }
        else if(indexPath.row == 1)   // Notification Setting
        {
            NotificationSettingViewController *objNot = [[NotificationSettingViewController alloc] init];
            [self.navigationController pushViewController:objNot animated:YES];
        }
        else if(indexPath.row == 2)   // Privacy
        {
            PrivacySettingViewController *objPri = [[PrivacySettingViewController alloc]initWithNibName:@"PrivacySettingViewController" bundle:nil];
            [self.navigationController pushViewController:objPri animated:YES];
        }
        else if(indexPath.row == 3)   // Location Setting
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }
        else                         // Blocked user
        {
            BlockedUserViewController *objBlo = [[BlockedUserViewController alloc]initWithNibName:@"BlockedUserViewController" bundle:nil];
            [self.navigationController pushViewController:objBlo animated:YES];
        }
    }
    else       // About
    {
        AboutViewController *objAbo = [[AboutViewController alloc]initWithNibName:@"AboutViewController" bundle:nil];
        [self.navigationController pushViewController:objAbo animated:YES];
    }
}

#pragma mark - Checked for Premium Plan
-(BOOL)isPremiumPlanIsActive
{
    NSDate *dtCreated = [self convertDateFromString:appDelegate.loginUser.subscription_exp_date isGMT:YES formate:CDateFormater];
    NSTimeInterval currentDateTimeStamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval SesisonExpireDateTimeStamp = [dtCreated timeIntervalSince1970];
    
    if (SesisonExpireDateTimeStamp > currentDateTimeStamp || appDelegate.loginUser.is_premium.boolValue)
    {
        return YES;
    }
    
    return NO;
}


#pragma mark - Action Event
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnLogoutClicked:(id)sender
{
    [self customAlertViewWithTwoButton:@"" Message:@"Do you want to log out?" ButtonFirstText:@"Ok" ButtonSecondText:@"Cancel" Animation:YES completed:^(int index) {
        if (index == 0)
        {
            [[APIRequest request] removeDeviceTokenFromServer:^(id responseObject, NSError *error)
             {
                 if (responseObject && !error)
                 {
                     NSLog(@"Device token is deleted succesfully ========== >>>");
                 }
                 else
                     NSLog(@"Error while deleting device token ========== >>>");
             }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [appDelegate MQTTDisconnetFromServer];
                appDelegate.loginUser = nil;
                [CUserDefaults removeObjectForKey:CUserId];
                [CUserDefaults removeObjectForKey:CIAPAppleRecieptID];
                
                [UIApplication removeUserId];
                [TblUser deleteAllObjects];
                appDelegate.loginUser = nil;
                
                [TblActivityPhoto deleteAllObjects];
                [TblActivityVideo deleteAllObjects];
                [TblActivitySyncSession deleteAllObjects];
                [TblActivityRunSession deleteAllObjects];
                [TblActivityCompleteSession deleteAllObjects];
                [TblActivityDates deleteAllObjects];
                [[Store sharedInstance].mainManagedObjectContext save];
            });
            
            appDelegate.window.rootViewController = appDelegate.objSuperIntro;
            appDelegate.objTabBarController = nil;
        }
    }];
}



@end
