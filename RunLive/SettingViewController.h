//
//  SettingViewController.h
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingCell.h"
#import "AccountSettingViewController.h"
#import "ConnectAndShareViewController.h"

@interface SettingViewController : SuperViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblSetting;
    IBOutlet UIButton *btnLogout;
}
- (IBAction)btnBackClicked:(id)sender;

@end
