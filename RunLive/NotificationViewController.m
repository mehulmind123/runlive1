//
//  NotificationViewController.m
//  RunLive
//
//  Created by mac-0006 on 19/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//


#import "NotificationViewController.h"
#import "NotificationCell.h"
#import "NotificationSettingViewController.h"
#import "NotificationLikePhotoCell.h"
#import "SessionDetailsViewController.h"
#import "PhotoDetailsViewController.h"
#import "CommentsViewController.h"
#import "RunSummaryViewController.h"
#import "PhotoDetailsViewController.h"
#import "ActivityDetailViewController.h"


@interface NotificationViewController ()

@end

@implementation NotificationViewController
{
    BOOL isAllreadyClicked;
    
    BOOL isApiStarted;
    int iOffset;
    
    NSMutableArray *arrNotification;
    NSURLSessionDataTask *taskRunning;
    UIRefreshControl *refreshControl;
}

#pragma mark - Life cycle
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    superTable = tblNotification;
    
    arrNotification = [NSMutableArray new];
    
    [tblNotification registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"NotificationCell"];
    [tblNotification registerNib:[UINib nibWithNibName:@"NotificationLikePhotoCell" bundle:nil] forCellReuseIdentifier:@"NotificationLikePhotoCell"];
    
    tblNotification.estimatedRowHeight = 65;
    tblNotification.rowHeight = UITableViewAutomaticDimension;
    
    iOffset = 0;
    [self getNotificationDataFromServer:YES];
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblNotification addSubview:refreshControl];


    // Refresh screen when any notification come...
    appDelegate.configureRefreshNotificationListScreen = ^(BOOL isRefresh)
    {
        iOffset = 0;
        [self getNotificationDataFromServer:YES];
    };
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [appDelegate hideTabBar];
    
    isAllreadyClicked = NO;
    
    appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (taskRunning && taskRunning.state == NSURLSessionTaskStateRunning)
        [taskRunning cancel];
}



#pragma mark - Pull Refresh
#pragma mark -

-(void)handleRefresh
{
    iOffset = 0;
    [self getNotificationDataFromServer:NO];
}

- (void)loadMoreData
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self getNotificationDataFromServer:NO];
}

#pragma mark - Api Functions
#pragma mark -

-(void)getNotificationDataFromServer:(BOOL)shouldShowLoader
{
    if (taskRunning && taskRunning.state == NSURLSessionTaskStateRunning)
        [taskRunning cancel];
    
    if (shouldShowLoader)
    {
        activityIndicator.hidden = NO;
        [activityIndicator startAnimating];
    }
    
    isApiStarted = YES;
    
    taskRunning = [[APIRequest request] NotificationList:[NSNumber numberWithInt:iOffset] completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];
        
        isApiStarted = NO;
        
        if (responseObject && !error)
        {
            // Get Data...
            if (iOffset == 0)
                [arrNotification removeAllObjects];
            
            iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
            
            [arrNotification addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            [tblNotification reloadData];
        }
        
        // To read all unclickable notification...
//        [[APIRequest request] readUnClickableNotification:nil];
    }];
}

#pragma mark - Table View Delegate
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotification.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [arrNotification objectAtIndex:indexPath.row];
    
    
    if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeVideoLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoComment] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeVideoComment])
    {
        static NSString *simpleTableIdentifier = @"NotificationLikePhotoCell";
        NotificationLikePhotoCell *cell = (NotificationLikePhotoCell *)[tblNotification dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.backgroundColor = [[dicData numberForJson:@"read_status"] isEqual:@0] ? CRGBA(71, 216, 253,.1) : [UIColor whiteColor];

        NSDictionary *dicUser = [dicData valueForKey:@"from_user"];
        [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"84" Height:nil imageURL:[dicUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        if ([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoLike])
        {
            [cell.imgLikePic hideByWidth:NO];
            [cell.imgLikePic setImageWithURL:[appDelegate resizeImage:@"112" Height:nil imageURL:[dicData stringValueForJSON:@"attachment"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
            [cell.imgLikePic hideByWidth:YES];
            
        
        NSString *strUserName1 = [dicUser stringValueForJSON:@"user_name"];
        NSString *strNotificationText = [[dicData stringValueForJSON:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        cell.lblContent.text = [NSString stringWithFormat:@"%@ %@",strUserName1,strNotificationText];
        cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:[dicData stringValueForJSON:@"date"]];
        
        [cell.lblContent addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString) {
            if (isAllreadyClicked)
                return ;
            
            isAllreadyClicked = YES;
            [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        
        [cell.btnUser touchUpInsideClicked:^{
            if (isAllreadyClicked)
                return ;
            
            isAllreadyClicked = YES;
            [appDelegate moveOnProfileScreen:strUserName1 UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"NotificationCell";
        NotificationCell *cell = (NotificationCell *)[tblNotification dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionInvite])
            [cell.imgSwipeIcon hideByWidth:NO];
        else
            [cell.imgSwipeIcon hideByWidth:YES];
        
        cell.backgroundColor = [[dicData numberForJson:@"read_status"] isEqual:@0] ? CRGBA(71, 216, 253,.1) : [UIColor whiteColor];
        cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:[dicData stringValueForJSON:@"date"]];
        
        if ([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionResult] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStart] || ![dicData objectForKey:@"from_user"])
        {
            //
            NSString *strNotificationText = [[dicData stringValueForJSON:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            cell.lblContent.text = strNotificationText;
            cell.imgDP.image = [UIImage imageNamed:@"appicon.jpg"];
        }
        else
        {
            NSDictionary *dicUser = [dicData valueForKey:@"from_user"];
            
            [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"84" Height:nil imageURL:[dicUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            NSString *strComment1, *strSessionName, *strUserName1;
            strUserName1 = [dicUser stringValueForJSON:@"user_name"];
            strComment1 = [[dicData stringValueForJSON:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            strSessionName = [dicData stringValueForJSON:@"session_name"];
            
            // To remove session name from notification.....
            if ([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionComment] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivitySessionComment])
                strSessionName = @"";

            if ([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionInvite])
            {
                strSessionName = @"";
                if (!cell.rightUtilityButtons)
                {
                    [cell setRightUtilityButtons:@[[UIButton buttonWithColor:CRGB(229, 86, 97) icon:[UIImage imageNamed:@"ic_notification_cancel"]],[UIButton buttonWithColor:CRGB(115, 213, 248) icon:[UIImage imageNamed:@"ic_notification_accept"]]]WithButtonWidth:65];
                }
            }
            
            if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionSynced] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionDeclined] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivitySessionLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionJoin])
                cell.lblContent.text = [NSString stringWithFormat:@"%@ %@ %@.",strUserName1, strComment1, strSessionName];
            else
                cell.lblContent.text = [NSString stringWithFormat:@"%@ %@ %@",strUserName1, strComment1, strSessionName];
            
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblContent.attributedText];
            [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strUserName1.length)];
            [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblContent.font.pointSize) range: NSMakeRange(0,strUserName1.length)];
            [text addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(strUserName1.length + strComment1.length +2, strSessionName.length)];
            [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblContent.font.pointSize) range: NSMakeRange(strUserName1.length + strComment1.length + 2, strSessionName.length)];
            cell.lblContent.attributedText  = text;
            
            [cell.lblContent addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
             {
                 if (isAllreadyClicked)
                     return ;
                 
                 isAllreadyClicked = YES;
                 [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
             }];
            
            [cell.btnUser touchUpInsideClicked:^{
                if (isAllreadyClicked)
                    return ;
                
                isAllreadyClicked = YES;
                [appDelegate moveOnProfileScreen:strUserName1 UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
            }];
            
            __weak typeof(cell) weakCell = cell;
            [cell setLeftRightUtilityButtonsClicked:^(SWCellState state, NSUInteger index)
             {
                 if (index == 1)
                 {
                     [self readNotification:[dicData stringValueForJSON:@"_id"]];
#pragma mark - Accept Session
                     [[APIRequest request] JoinSession:[dicData stringValueForJSON:@"session_id"] completed:^(id responseObject, NSError *error)
                      {
                          weakCell.rightUtilityButtons = nil;
                          [arrNotification removeObjectAtIndex:indexPath.row];
                          [tblNotification reloadData];
                          [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
                      }];
                 }
                 else
                 {
#pragma mark - Decline Session
                     [self customAlertViewWithTwoButton:@"" Message:@"Do you want to decline?" ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
                         if (index == 0)
                         {
                             [self readNotification:[dicData stringValueForJSON:@"_id"]];
                             [[APIRequest request] DeclineSession:[dicData stringValueForJSON:@"session_id"] completed:^(id responseObject, NSError *error)
                              {
                                  weakCell.rightUtilityButtons = nil;
                                  
                                  // Remove notification from list
                                  [arrNotification removeObjectAtIndex:indexPath.row];
                                  [tblNotification reloadData];
                                  [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
                              }];
                         }
                     }];
                 }
             }];
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [arrNotification objectAtIndex:indexPath.row];
    
    if (isAllreadyClicked)
        return ;
    
    if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeVideoLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoComment] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeVideoComment])
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        PhotoDetailsViewController *objDetail = [[PhotoDetailsViewController alloc] init];
        objDetail.strActivityType = [dicData stringValueForJSON:@"activity_type"];
        objDetail.strActivityID = [dicData stringValueForJSON:@"activity_id"];
        [self.navigationController pushViewController:objDetail animated:YES];
    }
    
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivitySessionLike] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivitySessionComment])
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        if ([dicData stringValueForJSON:@"activity_type"].integerValue == 3)
        {
            ActivityDetailViewController *objActDet = [[ActivityDetailViewController alloc] init];
            objActDet.strActivityId = [dicData stringValueForJSON:@"activity_id"];
            objActDet.strResultSessionId = [dicData stringValueForJSON:@"session_id"];
            [self.navigationController pushViewController:objActDet animated:YES];
        }
    }
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeFollow]) // Other User Detail
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        isAllreadyClicked = YES;
        NSDictionary *dicUser = [dicData valueForKey:@"from_user"];
        [appDelegate moveOnProfileScreen:[dicUser stringValueForJSON:@"user_name"] UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoComment] || [[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypePhotoLike]) //Photo detail and Photo Like screen
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        isAllreadyClicked = YES;
        PhotoDetailsViewController *objDetail = [[PhotoDetailsViewController alloc]init];
        [self.navigationController pushViewController:objDetail animated:YES];
    }
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionComment]) // Session Comment screen
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        
        isAllreadyClicked = YES;
        int sessionLevel = [dicData stringValueForJSON:@"session_level"].intValue;
        
        if (sessionLevel == 1)
        {
            SessionDetailsViewController *objDetail = [[SessionDetailsViewController alloc]init];
            objDetail.isCommentView = YES;
            objDetail.strSessionId = [dicData stringValueForJSON:@"session_id"];
            [self.navigationController pushViewController:objDetail animated:YES];
            return;
        }
        
        if (sessionLevel == 3)
        {
            RunSummaryViewController *objResult = [[RunSummaryViewController alloc] init];
            objResult.isFromNotification = YES;
            objResult.isNotificationComment = YES;
            objResult.strResultSessionId = [dicData stringValueForJSON:@"session_id"];
            [self.navigationController pushViewController:objResult animated:YES];
        }
    }
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeMentionComment]) // Comment screen
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        isAllreadyClicked = YES;
//        CommentsViewController *objDetail = [[CommentsViewController alloc]init];
//        [self.navigationController pushViewController:objDetail animated:YES];
    }
    else if([[dicData stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionResult]) // Comment screen
    {
        [self readNotification:[dicData stringValueForJSON:@"_id"]];
        isAllreadyClicked = YES;
        RunSummaryViewController *objResult = [[RunSummaryViewController alloc]init];
        objResult.isFromNotification = YES;
        objResult.isNotificationComment = NO;
        objResult.strResultSessionId = [dicData stringValueForJSON:@"session_id"];
        [self.navigationController pushViewController:objResult animated:YES];
    }
}

#pragma mark - Read Notification
#pragma mark -
-(void)readNotification:(NSString *)not_id
{
    if (!not_id)
        return;
    
    [[APIRequest request] readNotification:not_id completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSLog(@"Succefully read ======== ");
         }
     }];
}

#pragma mark - Action
#pragma mark -

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNotificationSettingClicked:(id)sender
{
    NotificationSettingViewController *objNot = [[NotificationSettingViewController alloc] init];
    [self.navigationController pushViewController:objNot animated:YES];
}

@end
