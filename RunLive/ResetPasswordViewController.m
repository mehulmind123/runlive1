//
//  ResetPasswordViewController.m
//  RunLive
//
//  Created by mac-0008 on 22/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnReset.layer.cornerRadius = 3;
    
    [txtEmail setTextFieldPlaceholderText:@"Email address or Username"];
    txtEmail.imageWrong = @"worng_lrf";
    txtEmail.selectedLineColor = CLineSelected;
    txtEmail.placeHolderColor = CLRFPlaceholderColor;
    txtEmail.selectedPlaceHolderColor = CLRFPlaceholderSelectedColor;
    txtEmail.lineColor = CLRFTextFieldLineColor;
    lblInfoText.text = @"Enter your email or username to receive instructions on how to reset your password.";
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark  UITextfield Delegates
#pragma mark

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtEmail)
        [txtEmail floatTheLabel:@"email and username" andText:textField.text];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txtEmail)
        [txtEmail floatTheLabel:@"" andText:textField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (range.location == 0 && [string isEqualToString:@" "])
        return NO;
    
    if (textField == txtEmail)
        [txtEmail floatTheLabel:@"email and username" andText:strNew];
    
    return YES;
}


#pragma mark - Action Event
#pragma mark

- (IBAction)btnBackClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnResetCLK:(id)sender
{
    [appDelegate hideKeyboard];
    
    if (![txtEmail.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter your email or username." ButtonText:@"OK" Animation:YES completed:nil];
    else
    {
        [[APIRequest request] ForgotPWD:txtEmail.text completed:^(id responseObject, NSError *error)
        {
            [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}


@end
