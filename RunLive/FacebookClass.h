//
//  Facebook.h
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import <SocialNetworks/SocialNetworks.h>
#import "SocialNetworks.h"


@interface FacebookClass : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isAunthenticated;
+ (void)logout;

-(void)LoginWithFacebook:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion;
-(void)LoadFacebookUserDetails:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion;

-(void)postOnFacebook:(PostParameters *)data completion:(void (^)(NSError *error,BOOL status))completion;
-(void)shareOnFacebook:(PostParameters*)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;

-(void)fetchFriends:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;

- (void)createAbum:(NSDictionary *)album success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;
- (void)uploadPhoto:(NSDictionary *)image onAlbum:(NSString *)album success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;



-(void)requestPublishPermissions:(NSArray *)permissions completion:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion;
-(void)requestReadPermissions:(NSArray *)permissions completion:(void (^)(NSDictionary *result,NSError *error,BOOL status))completion;



+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
