//
//  CreateSessionViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "CreateSessionViewController.h"
#import "SelectedSessionUserCell.h"
#import "SessionListViewController.h"
#import "InviteFriendsViewController.h"
#import "TimeAndDateViewController.h"

#define anglelimit 16.363

@interface CreateSessionViewController ()

@end

@implementation CreateSessionViewController
{
    
    BOOL isSoloSession;
    
    NSMutableArray *arrSelectedUser,*arrEditUser;
    NSString *strDateAndTimeForApi;
    NSTimer *rotationTimer;

    BOOL isViewApear;
    NSString *strDistanceForServer;
    
    
    CGFloat updatedAngle;
    NSInteger rotationDirection;
    double rotationSpeed;
    CADisplayLink *redrawTimer;
    BOOL isTimerRunning, isUserMovingFinger, isFromAutoRotation;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewmeter.transform = Is_iPhone_5 ? CGAffineTransformMakeScale(1.08, 1.08) : CGAffineTransformMakeScale(1.2, 1.2);
    
    objRotateGesture = [[MIWheelRotationGesture alloc] initWithTarget:self action:@selector(rotationGesture:)];
    objRotateGesture.parentView = self;
    objRotateGesture.gestureDelegate = self;
    [imgRotation addGestureRecognizer:objRotateGesture];


    arrSelectedUser = [NSMutableArray new];
    arrEditUser = [NSMutableArray new];
    
    [clUser registerNib:[UINib nibWithNibName:@"SelectedSessionUserCell" bundle:nil] forCellWithReuseIdentifier:@"SelectedSessionUserCell"];
    [txtName setValue:CRGB(146, 150, 163) forKeyPath:@"_placeholderLabel.textColor"];
    

    // Hide scroll bar for iphone x or iphone plus device....
    if (Is_iPhone_X || Is_iPhone_6_PLUS)
        viewSideScrollContainer.hidden = YES;
    else
        viewSideScrollContainer.hidden = NO;
    
    btnCreateSession.layer.cornerRadius = 3;
    objClip.hidden = YES;

    if (self.isEditSession)
    {
        // Get Session data from Server here.......
//        [self getSessionDataFromServer];
    }
    else
    {
        updatedAngle = 10;
        [self setValuewithAngle:updatedAngle];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    if (self.dicSelectedFriend)         // Add selected friend in collection view here......
    {
        [arrSelectedUser removeAllObjects];
        
        // To check Team Type
        if ([self.dicSelectedFriend objectForKey:@"solo"])
            [arrSelectedUser addObjectsFromArray:[self.dicSelectedFriend valueForKey:@"solo"]];
        else
        {
            [arrSelectedUser addObjectsFromArray:[self.dicSelectedFriend valueForKey:@"team_red"]];
            [arrSelectedUser addObjectsFromArray:[self.dicSelectedFriend valueForKey:@"team_blue"]];
        }
        
        // Remove login user from seleceted
        NSMutableArray *arrTempSelected = [NSMutableArray new];
        for (int i = 0; arrSelectedUser.count > i; i++)
        {
            NSDictionary *dicData = arrSelectedUser[i];
            if (![[dicData stringValueForJSON:@"_id"] isEqualToString:[NSString stringWithFormat:@"%@",appDelegate.loginUser.user_id]])
                [arrTempSelected addObject:dicData];
        }
        
        [arrSelectedUser removeAllObjects];
        [arrSelectedUser addObjectsFromArray:arrTempSelected];
        
//        if (self.isEditSession) // If Session is on Edit state......
//            [arrSelectedUser addObjectsFromArray:arrEditUser];
        
        lblSelectedFriend.text = arrSelectedUser.count>1 ? [NSString stringWithFormat:@" %lu FRIENDS SELECTED",(unsigned long)arrSelectedUser.count] : @"1 FRIEND SELECTED";
        [clUser reloadData];
    }
    else
    {
        if (arrSelectedUser.count > 0)
            lblSelectedFriend.text = arrSelectedUser.count > 1 ? [NSString stringWithFormat:@" %lu FRIENDS SELECTED",(unsigned long)arrSelectedUser.count] : @"1 FRIEND SELECTED";
        else
            lblSelectedFriend.text = @"No friends selected";
    }
    
    if (self.isEditSession)
    {
        // Get Session data from Server here.......
        btnCreateSession.enabled = NO;
        [self getSessionDataFromServer];
        [btnCreateSession setTitle:@"EDIT SESSION" forState:UIControlStateNormal];
    }
    else
        [btnCreateSession setTitle:@"CREATE SESSION" forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setValuewithAngle:updatedAngle];
    
    viewSideScroll.layer.cornerRadius = CGRectGetWidth(viewSideScroll.frame)/2;
    [self updateScrollIndicator];
    viewSideScroll.hidden = NO;
    objClip.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopMeterTimer];
}

#pragma mark - Api Related funciton
-(void)getSessionDataFromServer
{
    
    NSString *strDistance = [appDelegate convertKMToMeter:self.objSessionEdit.distance IsKM:YES];
    
    updatedAngle = (strDistance.intValue*14.174) + anglelimit;
    [self setValuewithAngle:updatedAngle];
    
    if (!self.objSessionEdit)
        return;
    
    [[APIRequest request] GETSessionDetail:self.objSessionEdit.session_id completed:^(id responseObject, NSError *error)
     {
         btnCreateSession.enabled = YES;
         NSLog(@"%@",responseObject);
         
         NSArray *arrResponse = [responseObject valueForKey:CJsonData];
         
         if (arrResponse.count > 0)
         {
             NSDictionary *dicSessionData = arrResponse[0];
             
             isSoloSession = [[dicSessionData numberForJson:@"isSolo"] isEqual:@1] ? YES : NO;
             
             NSDate *date = [self convertDateFromString:[dicSessionData stringValueForJSON:@"run_time"] isGMT:NO formate:CDateFormater];
             NSDateFormatter *dtFor = [NSDateFormatter initWithDateFormat:@"dd MMMM yyyy"];
             NSString *strDate = [dtFor stringFromDate:date];
             
             if (!isViewApear)
             {
                 isViewApear = YES;
                 strDateAndTimeForApi = strDate;
                 
                 txtName.text = self.objSessionEdit.session_name;
                 txtDate.text = self.objSessionEdit.session_date;
                 txtTime.text = self.objSessionEdit.session_time;
             }
             
             
             NSMutableArray *arrUsers = [NSMutableArray new];
             [arrUsers addObjectsFromArray:[dicSessionData valueForKey:@"joinedUsers"]];
             [arrUsers addObjectsFromArray:[dicSessionData valueForKey:@"syncedUsers"]];
             [arrUsers addObjectsFromArray:[dicSessionData valueForKey:@"otherUsers"]];
             
             [arrSelectedUser removeAllObjects];
             for (int s = 0; arrUsers.count > s; s++)
             {
                 NSMutableDictionary *dicData = [NSMutableDictionary new];
                 NSDictionary *dicInvite = arrUsers[s];
                 
                 if (![appDelegate.loginUser.user_id isEqualToString:[dicInvite stringValueForJSON:@"_id"]])
                 {
                     [dicData addObject:[dicInvite stringValueForJSON:@"_id"] forKey:@"_id"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"country"] forKey:@"country"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"fb_id"] forKey:@"fb_id"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"first_name"] forKey:@"first_name"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"last_name"] forKey:@"last_name"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"picture"] forKey:@"picture"];
                     [dicData addObject:[dicInvite stringValueForJSON:@"user_name"] forKey:@"user_name"];
                     [arrSelectedUser addObject:dicData];
                 }
             }
             
             lblSelectedFriend.text = arrSelectedUser.count > 1 ? [NSString stringWithFormat:@" %lu FRIENDS SELECTED",(unsigned long)arrSelectedUser.count] : @"1 FRIEND SELECTED";
             [clUser reloadData];
         }
     }];
}


#pragma mark - Collection View Delegate
#pragma mark -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.isEditSession)
    {
        if (self.objSessionEdit.isSolo.boolValue)
            return arrSelectedUser.count < 9 ? arrSelectedUser.count+1 : arrSelectedUser.count;
        
        return arrSelectedUser.count < 19 ? arrSelectedUser.count+1 : arrSelectedUser.count;
    }
    else
    {
        return arrSelectedUser.count+1;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"SelectedSessionUserCell";
    SelectedSessionUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
        if ((arrSelectedUser.count < 19 && !self.objSessionEdit.isSolo.boolValue) || (arrSelectedUser.count < 9 && self.objSessionEdit.isSolo.boolValue) || !self.isEditSession)  // If user count is less then 19 user then Add button will show.....
        {
            if (indexPath.item == 0)
            {
                cell.btnAdd.hidden = NO;
                cell.viewContainer.hidden = YES;
            }
            else
            {
                cell.btnAdd.hidden = YES;
                cell.viewContainer.hidden = NO;
                
                NSDictionary *dicData = [arrSelectedUser objectAtIndex:indexPath.item-1];
                cell.imgUser.image = nil;
                
                [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            }
        }
        else
        {
            cell.btnAdd.hidden = YES;
            cell.viewContainer.hidden = NO;
            
            NSDictionary *dicData = [arrSelectedUser objectAtIndex:indexPath.item];
            cell.imgUser.image = nil;
            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }

    [cell.btnAdd touchUpInsideClicked:^{
        InviteFriendsViewController *objInvite = [[InviteFriendsViewController alloc] init];
        objInvite.isFromCerateSession = !self.isEditSession;
        objInvite.isSoloSession = isSoloSession;

        if (self.isEditSession)
        {
            objInvite.arrAllreadyInvitedUser = [[NSMutableArray alloc] initWithArray:arrSelectedUser];
            objInvite.strSessionId = self.objSessionEdit.session_id;
        }
        
        [self.navigationController pushViewController:objInvite animated:YES];
    }];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50 , 50);
}

#pragma mark - ScrollView Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView1;
{
    if ([scrollView1 isEqual:ScrollView])
    {
        [self updateScrollIndicator];
    }
}

-(void)updateScrollIndicator
{
    if (ScrollView.contentSize.height != 0)
    {
        scrollIndheight.constant = (ScrollView.frame.size.height / ScrollView.contentSize.height) * ScrollView.frame.size.height ;
        scrollIndY.constant = (ScrollView.contentOffset.y / ScrollView.contentSize.height) * ScrollView.frame.size.height;
    }
}

#pragma mark - MIRotateGestureRecognizer Methods

- (void)updateCountdown
{
    
    [self fillInnerCircleWithAngle];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.003 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (!isFromAutoRotation)
        {
            if (!isUserMovingFinger)
                [self stopMeterTimer];
            
            isUserMovingFinger = NO;
        }
        
    });
    
}

-(void)startMeterTimer
{
    [self stopMeterTimer];
    redrawTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateCountdown)];
    [redrawTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    isTimerRunning = YES;
}

-(void)stopMeterTimer
{
    isTimerRunning = NO;
    [redrawTimer invalidate];
    rotationTimer = nil;
}



-(void)autoCicrleFillDidStop:(BOOL)isStop;
{
    isFromAutoRotation = NO;
    [self stopMeterTimer];
}

-(void)autoCicrleFillDidStart:(BOOL)isStart;
{
    isFromAutoRotation = YES;
    [self startMeterTimer];
}

- (void)rotationGesture:(id)sender
{
    NSInteger direction = ((MIWheelRotationGesture*)sender).rotationDirection;
    rotationSpeed = ((MIWheelRotationGesture*)sender).angularSpeed;
    
    rotationDirection = direction;
    
    switch ([(MIWheelRotationGesture*)sender state])
    {
        case UIGestureRecognizerStateBegan:
        {
            // Start Timer here....
            if (rotationSpeed > 0.001)
                [self updateCountdown];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            isUserMovingFinger = YES;
            
            if (rotationSpeed > 0.001 && !isTimerRunning)
                [self updateCountdown];
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
        }
            break;
        case UIGestureRecognizerStateCancelled:
        {
            [self stopMeterTimer];
        }
            break;
            
        default:
            break;
    }
    
    
}

-(void)fillInnerCircleWithAngle
{
    double angle = 0;
    float increaseAgnglePer = 0.90;
    
    if (rotationSpeed > 0.5) {
        increaseAgnglePer = 0.2; // speed of wheel rotation when swiping
    } else {
        increaseAgnglePer = 0.3; // speed of wheel rotation when panning
    }
    
    if (rotationDirection == 1) {
        angle = 360*increaseAgnglePer/100;
    } else {
        angle = -(360*increaseAgnglePer/100);
    }
    
    updatedAngle += angle;
    
    if (updatedAngle >= 360)
        updatedAngle = 360;
    
    if (updatedAngle <= 0)
        updatedAngle = 0;
    
    [self setValuewithAngle:((float)lroundf(updatedAngle))];
    
}


#pragma mark - Distance Meter Related Functions

-(void)setValuewithAngle:(CGFloat)angle //360 formate
{
    [objClip updateAngle:angle];
    
    float Distance = 0;
    if (angle < 0)
    {
        float nagativeAngle = fabs(angle);
        if (nagativeAngle < anglelimit)
            Distance = 283.494;
        else
        {
            Distance = 180 + angle;
            Distance = Distance+180-anglelimit;
        }
    }
    else
    {
        if (angle < anglelimit)
            Distance = 0;
        else
        {
            Distance = angle;
            Distance = Distance-anglelimit;
        }
    }
    
    Distance = Distance/16.3636;
    
    //Possible values: 1k, 2k, 3k, 4k, 5k, 6k, 7k, 8k, 9k, 10k, 11k, 12k, 13k, 14k, 15k, 16k, 17k, 18k, 19k, 20k, 21k, 22k
    NSString *strSelectedDistance;
    if (Distance <= 0 )
        strSelectedDistance = @"0";
    else if (Distance <= 1.0 )
        strSelectedDistance = @"1";
    else if (Distance <= 2.0 )
        strSelectedDistance = @"2";
    else if (Distance <= 3.0 )
        strSelectedDistance = @"3";
    else if (Distance <= 4.0 )
        strSelectedDistance = @"4";
    else if (Distance <= 5.0 )
        strSelectedDistance = @"5";
    else if (Distance <= 6.0 )
        strSelectedDistance = @"6";
    else if (Distance <= 7.0 )
        strSelectedDistance = @"7";
    else if (Distance <= 8.0 )
        strSelectedDistance = @"8";
    else if (Distance <= 9.0 )
        strSelectedDistance = @"9";
    else if (Distance <= 10 )
        strSelectedDistance = @"10";
    else if (Distance <= 11 )
        strSelectedDistance = @"11";
    else if (Distance <= 12 )
        strSelectedDistance = @"12";
    else if (Distance <= 12.85 )
        strSelectedDistance = @"13";
    else if (Distance <= 13.35 )
        strSelectedDistance = @"14";
    else if (Distance <= 14 )
        strSelectedDistance = @"15";
    else if (Distance <= 14.75 )
        strSelectedDistance = @"15";
    else if (Distance <= 15.25 )
        strSelectedDistance = @"16";
    else if (Distance <= 16 )
        strSelectedDistance = @"17";
    else if (Distance <= 16.85 )
        strSelectedDistance = @"18";
    else if (Distance <= 17.35 )
        strSelectedDistance = @"19";
    else if (Distance <= 18.95 )
        strSelectedDistance = @"20";
    else if (Distance <= 19.653996 )
        strSelectedDistance = @"21";
    else
        strSelectedDistance = @"22";
    
    strDistanceForServer = strSelectedDistance;
    
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
    {
        lblMKM.text = @"km";
        lblDistance.text = strSelectedDistance;
    }
    else
    {
        lblMKM.text = @"miles";
        float miles = strSelectedDistance.floatValue / 1.60934;
        lblDistance.text = [NSString stringWithFormat:@"%.1f",miles];
    }
}

#pragma mark - Action Event

-(IBAction)btnTimeAndDateCLK:(UIButton *)sender
{
    btnDate.selected = btnTime.selected = NO;
    switch (sender.tag)
    {
        case 0:
        {
            btnDate.selected = YES;
        }
            break;
        case 1:
        {
            btnTime.selected = YES;
        }
            break;
            
        default:
            break;
    }
    
    
    TimeAndDateViewController *objTimeDate = [[TimeAndDateViewController alloc] init];
    objTimeDate.isTime = btnTime.selected;
    objTimeDate.strSelectedTime = txtTime.text;
    
    objTimeDate.configureGetTimeDate = ^(NSString *strTimeDate,BOOL isTime,NSString *strDateForApi)
    {
        if (isTime)
            txtTime.text = strTimeDate;
        else
        {
            txtDate.text = strTimeDate;
            strDateAndTimeForApi = strDateForApi;
        }
    };
    
    [self presentViewController:objTimeDate animated:YES completion:nil];
}

// Create Session CLK here.......
-(IBAction)btnCreateSession:(id)sender
{
    if (![txtName.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter session name." ButtonText:@"OK" Animation:YES completed:nil];
    else if (strDistanceForServer.intValue < 1)
        [self customAlertViewWithOneButton:@"" Message:@"Please select minimum 1 km distance." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtDate.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please select date." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtTime.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please select time." ButtonText:@"OK" Animation:YES completed:nil];
    else if(arrSelectedUser.count < 1)
        [self customAlertViewWithOneButton:@"" Message:@"Please select at least one friend to invite." ButtonText:@"OK" Animation:YES completed:nil];
    else
    {
        NSString *strFinalDate = [NSString stringWithFormat:@"%@ %@",strDateAndTimeForApi,txtTime.text];
        NSDateFormatter *dtForm = [NSDateFormatter initWithDateFormat:@"dd MMMM yyyy hh:mm a"];
        [dtForm setTimeZone:[NSTimeZone systemTimeZone]];
        NSDate *date = [dtForm dateFromString:strFinalDate];
        NSDate *currentDate = [[NSDate date] dateByAddingTimeInterval:(60 * 2.5)]; // Increase current date with 2.5 mint.
        NSComparisonResult result = [currentDate compare:date];
        
        if (result == NSOrderedDescending || result == NSOrderedSame)
        {
            [self customAlertViewWithOneButton:@"" Message:@"Session creation date and time must be greater than current date and time." ButtonText:@"OK" Animation:YES completed:nil];
            return;
        }
        
        NSDate *dateGMT = [dtForm dateFromString:strFinalDate];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = CDateFormater;
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
        NSString *strTomorrowDate = [dateFormatter stringFromDate:dateGMT];
        
        NSString *strDistance = [appDelegate convertKMToMeter:strDistanceForServer IsKM:NO];
//        strDistance = @"100";
        
        if (self.isEditSession)
        {
            NSMutableDictionary *dicEdit = [NSMutableDictionary new];
            [dicEdit addObject:self.objSessionEdit.session_id forKey:@"session_id"];
            [dicEdit addObject:strDistance forKey:@"distance"];
            [dicEdit addObject:txtName.text forKey:@"name"];
            [dicEdit addObject:txtDate.text forKey:@"date"];
            [dicEdit addObject:txtTime.text forKey:@"time"];
            [dicEdit addObject:strTomorrowDate forKey:@"run_time"];
            
            [[APIRequest request] EditSession:dicEdit completed:^(id responseObject, NSError *error)
            {
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
                [self.navigationController popToRootViewControllerAnimated:NO];
                [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:nil];
            }];
        }
        else
        {
            NSMutableDictionary *dicServer = [NSMutableDictionary new];
            [dicServer addObject:txtName.text forKey:@"name"];
            [dicServer addObject:txtDate.text forKey:@"date"];
            [dicServer addObject:txtTime.text forKey:@"time"];
            [dicServer addObject:strTomorrowDate forKey:@"run_time"];
            [dicServer addObject:strDistance forKey:@"distance"];
            [dicServer addObject:self.dicSelectedFriend forKey:@"invitation"];
            
            [[APIRequest request] createSession:dicServer completed:^(id responseObject, NSError *error)
             {
                 [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
                 [self.navigationController popToRootViewControllerAnimated:NO];
             }];
        }
    }
}

@end
