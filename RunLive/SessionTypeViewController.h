//
//  SessionTypeViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/19/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionTypeViewController : SuperViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIButton *btnSolo,*btnTeam,*btnOk;
    IBOutlet UICollectionView *clFriends,*clTeamRed,*clTeamBlue;
    
    IBOutlet UIView *viewTeam,*viewBottom;
}

@property(strong,nonatomic) NSMutableArray *arrFriends;
@end
