//
//  MSGSelectFriendsViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/4/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSGSelectFriendsViewController : SuperViewController
{
    IBOutlet UITextField *txtSearch;
    IBOutlet UITableView *tblUser;
    IBOutlet UIButton *btnDone;
}

@property (nonatomic, strong) NSArray *arrAlreadyAdded;

@property (atomic, assign) BOOL isFromOldChat;
@end
