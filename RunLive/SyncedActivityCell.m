//
//  SyncedActivityCell.m
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SyncedActivityCell.h"

@implementation SyncedActivityCell
{
    int seconds;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];

}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = 1;
    _viewUser.layer.borderColor = CRGB(235, 241, 245).CGColor;
    
    _viewSubContainer.layer.cornerRadius = 3;
    _viewSubContainer.layer.masksToBounds = NO;
    
    _viewContainer.layer.cornerRadius = 3.0;
    _viewContainer.layer.shadowOffset = CGSizeMake(.8, .8);
    _viewContainer.layer.shadowOpacity = 0.2;
    _viewContainer.layer.shadowRadius = 3.5;
    
    _btnSyncedUser.layer.cornerRadius = CGRectGetHeight(_btnSyncedUser.frame)/2;
    
    _viewSynced1.layer.cornerRadius = _viewSynced2.layer.cornerRadius = _viewSynced3.layer.cornerRadius = _viewSynced4.layer.cornerRadius = _viewSynced5.layer.cornerRadius = _viewSynced6.layer.cornerRadius = CGRectGetHeight(_viewSynced1.frame)/2;
    _viewSynced1.layer.masksToBounds = _viewSynced2.layer.masksToBounds = _viewSynced3.layer.masksToBounds = _viewSynced4.layer.masksToBounds = _viewSynced5.layer.masksToBounds = _viewSynced6.layer.masksToBounds = YES;
    
    _imgSynced1.layer.cornerRadius = _imgSynced2.layer.cornerRadius = _imgSynced3.layer.cornerRadius = _imgSynced4.layer.cornerRadius = _imgSynced5.layer.cornerRadius = _imgSynced6.layer.cornerRadius = CGRectGetHeight(_imgSynced1.frame)/2;
    _imgSynced1.layer.masksToBounds = _imgSynced2.layer.masksToBounds = _imgSynced3.layer.masksToBounds = _imgSynced4.layer.masksToBounds = _imgSynced5.layer.masksToBounds = _imgSynced6.layer.masksToBounds = YES;
    
    

    self.lblActivityText.preferredMaxLayoutWidth = CGRectGetWidth(self.lblActivityText.frame);
    self.lblComment1.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment1.frame);
    self.lblComment2.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment2.frame);

}

#pragma mark - Timer Related functions


-(void)startTimer
{
    [self stopTimer];
    self.timer = nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    double sessionEndTimeStamp = self.objSessionTime.session_endtime.doubleValue;
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;
    
    if (difTimeStamp > 0)
    {
        seconds = difTimeStamp;
        _timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    }
    else
    {
        self.lblSessionTime.text=@"00:00:00";
        
        if (self.configureSesionSyncTimeOver)
            self.configureSesionSyncTimeOver(YES);

    }
}

- (void)updateCountdown
{
    seconds--;
    if (seconds >= 0)
    {
        int tempSecond = seconds;
        
        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        int hours = tempSecond / (60 * 60);
        tempSecond = tempSecond - hours * (60 * 60);
        int minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;
        
        if (days > 0)
        {
            // Time with day... (DDd:HHh)
            self.lblSessionTime.text=[NSString stringWithFormat:@"%02dd:%02dh",days,hours];
        }
        else
        {
            //hh:mm:ss
            self.lblSessionTime.text=[NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,tempSecond];
        }
    }
    else
    {
        [self stopTimer];
        self.lblSessionTime.text=@"00:00:00";
        
        if (self.configureSesionSyncTimeOver)
            self.configureSesionSyncTimeOver(YES);
    }
}

-(void)stopTimer
{
    [self.timer invalidate];
    
}

-(void)showSyncUser:(NSArray *)arrSyncedUser
{
    if (arrSyncedUser.count == 0)
    {
        self.viewSynced.hidden = YES;
        return;
    }
    
    self.viewSynced.hidden = NO;
    _viewSynced1.hidden = _viewSynced2.hidden = _viewSynced3.hidden = _viewSynced4.hidden = _viewSynced5.hidden = _viewSynced6.hidden = YES;
    
    if (arrSyncedUser.count == 1)
    {
        _viewSynced1.hidden = NO;
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    else if (arrSyncedUser.count == 2)
    {
        _viewSynced1.hidden = _viewSynced2.hidden = NO;
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        NSDictionary *dicSecondUser = arrSyncedUser[1];
        [_imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSecondUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    }
    else if (arrSyncedUser.count == 3)
    {
        _viewSynced1.hidden = _viewSynced2.hidden = _viewSynced3.hidden = NO;
        
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicSecondUser = arrSyncedUser[1];
        [_imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSecondUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
         NSDictionary *dicThirdUser = arrSyncedUser[2];
        [_imgSynced3 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicThirdUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    }
    else if (arrSyncedUser.count == 4)
    {
        _viewSynced1.hidden = _viewSynced2.hidden = _viewSynced3.hidden =  _viewSynced4.hidden = NO;
        
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicSecondUser = arrSyncedUser[1];
        [_imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSecondUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicThirdUser = arrSyncedUser[2];
        [_imgSynced3 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicThirdUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        NSDictionary *dicFourthUser = arrSyncedUser[3];
        [_imgSynced4 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFourthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];


    }
    else if (arrSyncedUser.count == 5)
    {
        _viewSynced1.hidden = _viewSynced2.hidden = _viewSynced3.hidden =  _viewSynced4.hidden = _viewSynced5.hidden = NO;
        
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicSecondUser = arrSyncedUser[1];
        [_imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSecondUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicThirdUser = arrSyncedUser[2];
        [_imgSynced3 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicThirdUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicFourthUser = arrSyncedUser[3];
        [_imgSynced4 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFourthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicFifthUser = arrSyncedUser[4];
        [_imgSynced5 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFifthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    }
    else
    {
        _viewSynced1.hidden = _viewSynced2.hidden = _viewSynced3.hidden =  _viewSynced4.hidden = _viewSynced5.hidden = _viewSynced6.hidden = NO;
        
        NSDictionary *dicFirstUser = arrSyncedUser[0];
        [_imgSynced1 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFirstUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicSecondUser = arrSyncedUser[1];
        [_imgSynced2 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSecondUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicThirdUser = arrSyncedUser[2];
        [_imgSynced3 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicThirdUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicFourthUser = arrSyncedUser[3];
        [_imgSynced4 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFourthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicFifthUser = arrSyncedUser[4];
        [_imgSynced5 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicFifthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSDictionary *dicSixthUser = arrSyncedUser[5];
        [_imgSynced6 setImageWithURL:[appDelegate resizeImage:@"52" Height:nil imageURL:[dicSixthUser stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    }
    
    [_btnSyncedUser setTitle:[NSString stringWithFormat:@"%lu SYNCED",(unsigned long)arrSyncedUser.count] forState:UIControlStateNormal];
    
    if (Is_iPhone_5)
    {
        _viewSynced6.hidden = _viewSynced5.hidden = YES;
    }
}

@end
