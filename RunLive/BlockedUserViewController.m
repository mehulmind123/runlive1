//
//  BlockedUserViewController.m
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "BlockedUserViewController.h"

@interface BlockedUserViewController ()
{
    NSMutableArray *arrUserList;
    BOOL isApiStarted;
    int iOffset;
}
@end

@implementation BlockedUserViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    superTable = tblUserList;
    
    arrUserList = [NSMutableArray new];
    [tblUserList registerNib:[UINib nibWithNibName:@"BlockedUserCell" bundle:nil] forCellReuseIdentifier:@"BlockedUserCell"];
    [self getBlockListFromServer:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - API Functions

- (void)loadMoreData
{
    if (isApiStarted)
        return;
    
    if (iOffset == 0)
        return;
    
    [self getBlockListFromServer:NO];
}


-(void)getBlockListFromServer:(BOOL)shouldShowLoader
{
    if (shouldShowLoader)
    {
        activityIndicator.hidden = NO;
        [activityIndicator startAnimating];
    }
    
    isApiStarted = YES;
    
    [[APIRequest request] BlockList:[NSNumber numberWithInt:iOffset] completed:^(id responseObject, NSError *error)
    {
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];

        isApiStarted = NO;
        
        if (responseObject && !error)
        {
            if (iOffset == 0)
                [arrUserList removeAllObjects];
            
            //iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
            
            [arrUserList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            [tblUserList reloadData];
        }
    }];
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableview Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrUserList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"BlockedUserCell";
    BlockedUserCell *cell = [tblUserList dequeueReusableCellWithIdentifier:strIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];

    NSString *strFirstname = [[dicData stringValueForJSON:@"first_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *strLastname = [[dicData stringValueForJSON:@"last_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (!strFirstname || [strFirstname isEqualToString:@""])
        cell.lblUserName.text = [NSString stringWithFormat:@"%@", [dicData stringValueForJSON:@"user_name"]];
    else
        cell.lblUserName.text = [NSString stringWithFormat:@"%@ %@", strFirstname,strLastname];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [cell.btnUnblock touchUpInsideClicked:^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Are you sure want to Unblock %@?",cell.lblUserName.text] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //
        }];
        
        UIAlertAction *btnUnblock = [UIAlertAction actionWithTitle:@"Unblock" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[APIRequest request] unblockUser:[dicData stringValueForJSON:@"to_user_id"] completed:^(id responseObject, NSError *error)
             {
                 [arrUserList removeObjectAtIndex:indexPath.row];
                 [tblUserList reloadData];
             }];
        }];
        
        [alertController addAction:btnCancel];
        [alertController addAction:btnUnblock];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    
    return cell;
}

@end
