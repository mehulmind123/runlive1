//
//  InviteSearchCell.h
//  RunLive
//
//  Created by mac-00012 on 11/2/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteSearchCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UIImageView *imgFreind,*imgVerifiedUser;
@property(weak,nonatomic) IBOutlet UIView *viewSelect,*viewSelectInner;

@property(weak,nonatomic) IBOutlet UILabel *lblUserName;

@end
