//
//  FacebookSignUpViewController.h
//  RunLive
//
//  Created by mac-0005 on 21/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

typedef void(^usernameSuccessfullyAdded)(BOOL isAdded,NSString *strEmail);

@interface FacebookSignUpViewController : UIViewController
{
    IBOutlet UIButton *btnRegister;
    IBOutlet ACFloatingTextField *txtUserName;
    IBOutlet UILabel *lblInfoText;
}

@property (nonatomic,strong) NSString *strSocialId;
@property (nonatomic,strong) NSString *strSocialEmail;
@property (nonatomic,strong) NSString *strSocialFirstName;
@property (nonatomic,strong) NSString *strSocialLastName;
@property (atomic,assign) BOOL isLoginType;
@property (nonatomic,copy) usernameSuccessfullyAdded configureUsernameSuccessfullyAdded;


@end
