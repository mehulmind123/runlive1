//
//  PhotoDetailsViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/14/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoDetailsViewController : SuperViewController
{
    IBOutlet UITableView *tblPhoto;
    IBOutlet UILabel *lblTitle;
}

@property(strong,nonatomic) NSString *strActivityID;
@property(strong,nonatomic) NSString *strActivityType;

@end
