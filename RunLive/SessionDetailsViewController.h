//
//  SessionDetailsViewController.h
//  RunLive
//
//  Created by mac-0006 on 18/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionDetailSelectedUserCell.h"
#import "AudioSettingViewController.h"
#import "HPGrowingTextView.h"
#import "RunSummaryCommentCell.h"
#import "InviteFriendsViewController.h"


@interface SessionDetailsViewController : SuperViewController<UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, HPGrowingTextViewDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet MGLMapView *mapView;
    IBOutlet UILabel *lblSessionTitle;
    IBOutlet UILabel *lblCommCount;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblSyncedCount;
    IBOutlet UILabel *lblPendingCount;
    IBOutlet UILabel *lblSynced;
    
    IBOutlet KAProgressLabel *lblProgress;
    
    IBOutlet HPGrowingTextView *txtComment;
    IBOutlet UIButton *btnRun,*btnSend;
    IBOutlet UIButton *btnComment;
    
    IBOutlet UIView *viewRun;
    IBOutlet UIView *viewComment;
    IBOutlet UIView *viewLineComment;
    IBOutlet UIView *viewLineRun;
    IBOutlet UIView *viewProgress;
    IBOutlet UIView *viewFillCirle;
    IBOutlet UIImageView *imgShadow;
    
    IBOutlet UITableView *tblComment,*tblPeopleList;
    IBOutlet UICollectionView *collInviteFrd;
    
    IBOutlet UIView *viewScrollIndicator,*ViewScrollSide;
    
    IBOutlet UIButton *btnSpotify;
    
    NSTimer *timer, *timerProgress;
    IBOutlet NSLayoutConstraint *txtCommentHeight,*tblpeopleHeight,*scrollIndY,*scrollIndheight;
}

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnTabbarClicked:(UIButton *)sender;
- (IBAction)btnAudioSettingClicked:(id)sender;


@property(atomic,assign) BOOL isCommentView;
@property(strong,nonatomic) NSString *strSessionId;


@end
