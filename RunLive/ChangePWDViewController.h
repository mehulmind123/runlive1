//
//  ChangePWDViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/7/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface ChangePWDViewController : SuperViewController
{
    IBOutlet ACFloatingTextField *txtOldPWD,*txtNewPWD,*txtConfirmPWD;
    IBOutlet UIButton *btnSave;
}
@end
