//
//  NSObject+TimeIntervalConverter.m
//  RunLive
//
//  Created by mac-00012 on 12/21/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "NSObject+TimeIntervalConverter.h"

@implementation NSObject (TimeIntervalConverter)


-(NSString *)convertedDateFromTimeStamp:(double)timeStamp isGMT:(BOOL)isGMT formate:(NSString *)dateFormate
{
    NSString *strDate;

    if(isGMT)
    {
        NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:dateFormate];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
//        convetTimeStamp = [date timeIntervalSince1970];
        
        strDate = [dateFormatter stringFromDate:date];
        
//        NSLog(@"GMT time Zone date =========== >>>>>>>>  %@",strDate);
    }
    else
    {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:dateFormate];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        strDate = [dateFormatter stringFromDate:date];
        
//        NSLog(@"%@",strDate);
    }
    return strDate;
}


-(NSDate *)convertDateFromString:(NSString *)strDate1 isGMT:(BOOL)isGMT formate:(NSString *)dateFormate
{
//    strDate1 = @"2016-10-20 11:11:11";
    
    if ([strDate1 rangeOfString:@"T"].location != NSNotFound)
    {
        strDate1 = [strDate1 stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        strDate1 = [strDate1 substringToIndex:[strDate1 rangeOfString:@"."].location];
    }
    
    NSDate *resultDate;
    
    if(isGMT)
    {
        NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:dateFormate];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
        resultDate = [dateFormatter dateFromString:strDate1];
    }
    else
    {
        NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:dateFormate];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        resultDate = [dateFormatter dateFromString:strDate1];
    }
    
    return resultDate;
}


@end
