//
//  shareCollectionViewCell.h
//  RunLive
//
//  Created by mac-0004 on 27/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface shareCollectionViewCell : UICollectionViewCell
@property IBOutlet UIImageView *imgProfile;
@property IBOutlet UILabel *lblName,*lblSubName;
@property IBOutlet UIView *viewOuter,*viewBlue;

@end
