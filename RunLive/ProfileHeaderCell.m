//
//  ProfileHeaderCell.m
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ProfileHeaderCell.h"

@implementation ProfileHeaderCell
@synthesize collview,pageControl,arrList,viewFollow;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//    });
    
    
    viewFollow.layer.cornerRadius = 15;
    viewFollow.layer.borderWidth = 1;
    viewFollow.layer.borderColor = CRGB(152, 159, 170).CGColor;
    viewFollow.layer.masksToBounds = YES;
    
    [collview registerNib:[UINib nibWithNibName:@"ProfileNoPhotoCell" bundle:nil] forCellWithReuseIdentifier:@"ProfileNoPhotoCell"];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.viewNotificationBadge.layer.cornerRadius = CGRectGetHeight(_lblNotificationCount.frame)/2;
    self.viewNotificationBadge.layer.masksToBounds = YES;
}

#pragma mark - UICollectionView Datasource And Delegate

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"ProfileNoPhotoCell";
    ProfileNoPhotoCell *cell = [collview dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
    
    NSDictionary *dicData = arrList[indexPath.item];
//    cell.lblCount.text = [NSString stringWithFormat:@"%d",[dicData stringValueForJSON:@"count"].intValue];
    cell.lblCount.text = [dicData stringValueForJSON:@"count"];
    cell.lblTitle.text = [dicData stringValueForJSON:@"title"];
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth/3 , CGRectGetHeight(collview.frame));
}

- (void)scrollViewDidScroll:(UIScrollView *)lscrollView
{
        if(arrList.count == 0)
            return;

        int page = round(lscrollView.contentOffset.x/lscrollView.bounds.size.width);
    if ((lscrollView.contentOffset.x/lscrollView.bounds.size.width) - page > 0.3) {
        page = page +1;
    }
        pageControl.currentPage = page;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
    if(arrList.count == 0)
        return;
    int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
    if ((scrollView.contentOffset.x/scrollView.bounds.size.width) - page > 0.3) {
        page = page +1;
    }
    pageControl.currentPage = page;
}
@end
