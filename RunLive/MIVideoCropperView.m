//
//  MIVideoCropperView.m
//  Runlive
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import "MIVideoCropperView.h"

@interface MIVideoCropperView ()

@property (nonatomic, assign) CGRect baseRect;
@property (nonatomic, strong) UIView *clearView;

@end

@implementation MIVideoCropperView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        // Overlay
        self.overlayView = [[MIVideoCropperOverlayView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.overlayView.maxSize = frame.size;
        [self.overlayView drawRect:frame];
        [self addSubview:self.overlayView];
        
        self.baseRect = CGRectInset(frame, 0, 0);
        self.overlayView.maxSize = self.baseRect.size;
        
        
        // Add pan clearview
        _clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.baseRect.size.width, self.baseRect.size.height)];
        _clearView.backgroundColor = [UIColor clearColor];
        _clearView.layer.cornerRadius = 40;
        [self.overlayView addSubview:_clearView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(clearPanGesture:)];
        panGesture.maximumNumberOfTouches = 1;
        [_clearView addGestureRecognizer:panGesture];
        
        [self SetOverlayClearArea];
    }
    
    return self;
}

-(float)cropViewHeight:(CGSize)size
{
    float cameraAspectRatio = 3.0 / 4.0;
    float cropViewHeight = floorf(size.height * cameraAspectRatio);
    return cropViewHeight;
    
}

- (void)SetOverlayClearArea
{
    //    self.currentScale = 1.0;
    CGSize screenSize = self.frame.size;
//    self.overlayView.clearRect = CGRectMake(1, 0, screenSize.width-2, [self cropViewHeight:screenSize]);
    self.overlayView.clearRect = CGRectMake(0, 0, screenSize.width, [self cropViewHeight:screenSize]);
    [self.overlayView setNeedsDisplay];
}


- (void)clearPanGesture:(UIPanGestureRecognizer *)sender
{
    CGPoint translation = [sender translationInView:sender.view.superview];
//    CGPoint newCenter = CGPointMake(sender.view.center.x + translation.x, sender.view.center.y + translation.y);
    CGPoint newCenter = CGPointMake(sender.view.center.x, sender.view.center.y + translation.y);
    
    CGSize size = sender.view.frame.size;
    CGRect newRect = CGRectMake(newCenter.x - size.width / 2, newCenter.y - size.height / 2, size.width, [self cropViewHeight:size]);
    
    CGRect superViewFrame = CGRectInset(sender.view.superview.frame, 0, 0);
   
    if (CGRectContainsRect(superViewFrame, newRect))
    {
        sender.view.center = newCenter;
        self.overlayView.clearRect = CGRectInset(newRect, 0, 0);
        
        [UIView animateWithDuration:.3 animations:^{
            [self.overlayView setNeedsDisplay];
        }];
    }

    // Reset points
    [sender setTranslation:CGPointZero inView:self];
}



@end
