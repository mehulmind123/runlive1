//
//  VoiceChatPermissionView.m
//  RunLive
//
//  Created by mac-0005 on 12/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "VoiceChatPermissionView.h"

@implementation VoiceChatPermissionView

+ (id)initVoiceChatPermissionView
{
    VoiceChatPermissionView *objVoiceChat = [[[NSBundle mainBundle]loadNibNamed:@"VoiceChatPermissionView" owner:nil options:nil] lastObject];
    CViewSetWidth(objVoiceChat, CScreenWidth);
    CViewSetHeight(objVoiceChat, CScreenHeight);
    return objVoiceChat;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self Initialization];
}

#pragma mark - Configuration

-(void)Initialization
{
    self.btnVoiceChatGotForIt.layer.cornerRadius = self.btnVoiceChatNotNow.layer.cornerRadius = 3;
}

-(void)askForMicroPhonePermission
{
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (self.configurationVoiceChatPermissionAcceptReject)
            self.configurationVoiceChatPermissionAcceptReject(granted);
    }];
}



@end
