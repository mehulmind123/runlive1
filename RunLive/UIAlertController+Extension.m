//
//  UIAlertController+Extension.m
//  Master
//
//  Created by mac-0001 on 08/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import "UIAlertController+Extension.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
@implementation UIAlertController (Extension)
#else
@implementation UIAlertView (Extension)
#endif



#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000

+(void)alertControllerWithOneButtonWithStyle:(UIAlertControllerStyle)style title:(NSString*)title message:(NSString*)message buttonTitle:(NSString*)buttonTitle handler:(void (^)(UIAlertAction *action))handler inView:(UIViewController*)view {
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:handler];
    
    [alertView addAction:okAction];
    [view presentViewController:alertView animated:YES completion:nil];
}

+(void) alertControllerWithTwoButtonsWithStyle:(UIAlertControllerStyle)style  title:(NSString*)title message:(NSString*)message firstButton:(NSString*)firstButtonTitle firstHandler:(void (^)(UIAlertAction *action))firstHandler secondButton:(NSString*)secondButtonTitle secondHandler:(void (^)(UIAlertAction *action))secondHandler inView:(UIViewController*)view{
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:firstHandler];
    [firstAction setObject:alertView forKey:@"alert"];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:secondHandler];
    [secondAction setObject:alertView forKey:@"alert"];
    
    [alertView addAction:firstAction];
    [alertView addAction:secondAction];
    
    if (style == UIAlertControllerStyleActionSheet) {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertView addAction:cancel];
    }
    
    [view presentViewController:alertView animated:YES completion:nil];
}

+(void) alertControllerWithThreeButtonsWithStyle:(UIAlertControllerStyle)style  title:(NSString*)title message:(NSString*)message firstButton:(NSString*)firstButtonTitle firstHandler:(void (^)(UIAlertAction *action))firstHandler secondButton:(NSString*)secondButtonTitle secondHandler:(void (^)(UIAlertAction *action))secondHandler thirdButton:(NSString*)thirdButtonTitle thirdHandler:(void (^)(UIAlertAction *action))thirdHandler inView:(UIViewController*)view{
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:firstHandler];
    [firstAction setObject:alertView forKey:@"alert"];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:secondHandler];
    [secondAction setObject:alertView forKey:@"alert"];
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:thirdButtonTitle style:UIAlertActionStyleDefault handler:thirdHandler];
    
    [alertView addAction:firstAction];
    [alertView addAction:secondAction];
    [alertView addAction:thirdAction];
    [view presentViewController:alertView animated:YES completion:nil];
}

+(void) alertControllerWithOneTextFieldWithStyle:(UIAlertControllerStyle)style  title:(NSString*)title message:(NSString*)message firstButton:(NSString*)firstButtonTitle firstHandler:(void (^)(UIAlertAction *action))firstHandler secondButton:(NSString*)secondButtonTitle secondHandler:(void (^)(UIAlertAction *action))secondHandler inView:(UIViewController*)view withDelegate:(id)delegate
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:firstHandler];
    [firstAction setObject:alertView forKey:@"alert"];

    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:secondHandler];
    [secondAction setObject:alertView forKey:@"alert"];
    
    //textField
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"";
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    
    [alertView addAction:firstAction];
    [alertView addAction:secondAction];
    [view presentViewController:alertView animated:YES completion:nil];
}

+(void) alertControllerSignInWithStyle:(UIAlertControllerStyle)style  title:(NSString*)title message:(NSString*)message firstButton:(NSString*)firstButtonTitle firstHandler:(void (^)(UIAlertAction *action))firstHandler secondButton:(NSString*)secondButtonTitle secondHandler:(void (^)(UIAlertAction *action))secondHandler inView:(UIViewController*)view withDelegate:(id)delegate{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:firstHandler];
    [firstAction setObject:alertView forKey:@"alert"];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:secondHandler];
    [secondAction setObject:alertView forKey:@"alert"];
    
    //userName textField
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Username";
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    
    //password texfield
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    
    [alertView addAction:firstAction];
    [alertView addAction:secondAction];
    [view presentViewController:alertView animated:YES completion:nil];
}

+(void) alertControllerSignUpWithStyle:(UIAlertControllerStyle)style  title:(NSString*)title message:(NSString*)message firstButton:(NSString*)firstButtonTitle firstHandler:(void (^)(UIAlertAction *action))firstHandler secondButton:(NSString*)secondButtonTitle secondHandler:(void (^)(UIAlertAction *action))secondHandler inView:(UIViewController*)view withDelegate:(id)delegate {
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:firstHandler];
    [firstAction setObject:alertView forKey:@"alert"];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:secondHandler];
    [secondAction setObject:alertView forKey:@"alert"];
    
    //userName textField
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Username";
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    
    //password texfield
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    
    
    //password texfield
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Confirm Password";
        textField.secureTextEntry = YES;
        // insert tag number
        //        textField.tag = ;
        textField.delegate = delegate;
    }];
    //Check password textfield
    [alertView addAction:firstAction];
    [alertView addAction:secondAction];
    [view presentViewController:alertView animated:YES completion:nil];
}

#else



#endif

@end
