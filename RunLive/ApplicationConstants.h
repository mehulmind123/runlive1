
#import "AppDelegate.h"

#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#pragma mark - API Related Messages
#pragma mark -

#define CJsonStatus @"status"
#define CJsonMessage @"message"
#define CJsonTitle @"title"
#define CJsonData @"data"
#define CJsonExtra @"extra"
#define CJsonResponse @"response"
#define CMessageWait @"Please Wait..."
#define CMessageSorry @"Sorry!"
#define CInternetNotAvailable @"Please connect to internet to proceed."

#pragma mark - Social Network API Keys
#pragma mark -

#define CFacebookApiId @"721136141383056"
#define CFacebookCallBackUrl @"fb721136141383056"

#define kClientId "3c92f1b76e584a9982be79f18b47576f"
#define kCallbackURL "spotifyiossdkexample://"
#define kSessionUserDefaultsKey "SpotifySession"

#define CGoogleApiKey @"AIzaSyB3ZD0_B8Q9Lb0U9O7sNPIg-wxS_eXvg6s"
#define CAgoraApiKey @"356a790770c4414187640ca8fe1cf1cd"

#define CTwitterConsumerKey @"W3wWbkZ8kmQJ1lK4dvTYHnkie"
#define CTwitterSecretKey @"m2OKQwxJjSInKlO7Um9cj1FGzKydzYcNoybtG3XXgLSYwuupMH"

#define COneSignalAppId @"517ebf70-2ef0-49c1-8222-fd532d9f7d6d"
#define CDeviceToken @"DeviceToken"
#define CDeviceTokenPubNub @"DeviceTokenPubNub"
#define CPlayerId @"Player ID"

#define CSubscribed @"subscribed"
#define CUNSubscribed @"unsubscribed"

#define CAppStoreUrl @"https://itunes.apple.com/us/app/runlive/id1296744083?ls=1&mt=8"

#define FirstTimeInstall @"FirstTimeInstall"
#define CMMotionManagerStatus @"motionMangerStatus"
#define CUserName @"username"

#define CPubnubPublishKey @"pub-c-7d4c9898-92a0-4e88-b453-66e0de7a80e7"
#define CPubnubSubscribleKey @"sub-c-cad2932a-c9d9-11e6-90ff-0619f8945a4f"

#define CSTRAVAClientSecretKey @"48b8b3bf90d56952a23f61b03adbf9e01296de86"
#define CSTRAVAAccessTokenKey @"99d028c6a51edfb984ac38a8668efc8c01947e29"
#define CSTRAVADomain @"api.runlive.co"
#define CSTRAVAClientID 23343

#define CLastReadDateKey @"PNLastReadDateKey"
#define CDeviceTokenKey @"PNDeviceTokenKey"

//2016-12-26T11:31:17.693Z
#define CDateFormater @"yyyy-MM-dd HH:mm:ss"

#pragma mark - Colors
#pragma mark -

#define CBackGroundColor  CRGB(18, 18, 17)
#define CBlueColor  CRGB(70, 205, 254)

#define CNavigationBarColor  CRGB(30, 30, 30)
#define CThemeColor  CRGB(21, 24, 41)
#define CImageOuterBorderColor [UIColor colorWithWhite:0.5 alpha:0.08]

#define CPlaceholderUserImage [UIImage imageNamed:@"user_no_image"]

#pragma mark - Device Constants
#pragma mark -

#define CDeviceHeight [UIScreen mainScreen].bounds.size.height
#define Is_iPhone_4 CDeviceHeight == 480
#define Is_iPhone_5 CDeviceHeight == 568
#define Is_iPhone_6 CDeviceHeight == 667
#define Is_iPhone_6_PLUS CDeviceHeight == 736

#pragma mark - Fonts
#pragma mark -


#pragma mark - App Related Constants
#pragma mark -

#define CUserId @"LoginUserId"

#define CWrongValidationCheckoutImage [UIImage imageNamed:@"wrong"]
#define CWrongValidationLRFImage [UIImage imageNamed:@"worng_lrf"]
#define CCorrectValidationLRFImage [UIImage imageNamed:@"correct"]
#define CLRFPlaceHolderFont [UIFont fontWithName:self.font.fontName size:12];
#define CLineSelected   CRGB(111,226,254)
#define CLineUnSelected [UIColor clearColor]
#define CLRFPlaceholderColor CRGB(32, 34, 49)
#define CLRFPlaceholderSelectedColor CRGB(122,126,147)
#define CLRFTextFieldLineColor CRGB(219, 222, 231)

/*======== FONT =========*/
#define CFontSolidoCondensedRegular(fontSize) [UIFont fontWithName:@"SolidoCondensed" size:fontSize]
#define CFontSolidoCondensedBold(fontSize)    [UIFont fontWithName:@"SolidoCondensed-Bold" size:fontSize]
#define CFontSolidoCondensedMedium(fontSize)  [UIFont fontWithName:@"SolidoCondensed-Medium" size:fontSize]
#define CFontSolidoCondensedLight(fontSize)   [UIFont fontWithName:@"SolidoCondensed-Light" size:fontSize]
#define CFontSolidoCondensedItalic(fontSize)  [UIFont fontWithName:@"SolidoCondensed-Italic" size:fontSize]

#define CFontGilroyRegular(fontSize) [UIFont fontWithName:@"Gilroy" size:fontSize]
#define CFontGilroyBold(fontSize)    [UIFont fontWithName:@"Gilroy-Bold" size:fontSize]
#define CFontGilroyMedium(fontSize)  [UIFont fontWithName:@"Gilroy-Medium" size:fontSize]
#define CFontGilroyLight(fontSize)   [UIFont fontWithName:@"Gilroy-Light" size:fontSize]
#define CFontGilroyItalic(fontSize)  [UIFont fontWithName:@"Gilroy-Italic" size:fontSize]


/*======== GRAPH COLOR =========*/

#define CGraphColor1     CRGB(71, 216, 253)
#define CGraphColor2     CRGB(247, 112, 56)
#define CGraphColor3     CRGB(127, 194, 18)
#define CGraphColor4     CRGB( 74, 35, 90 )
#define CGraphColor5     CRGB(100, 30, 22)
#define CGraphColor6     CRGB( 27, 79, 114 )
#define CGraphColor7     CRGB(14, 98, 81)
#define CGraphColor8     CRGB(125, 102, 8)
#define CGraphColor9     CRGB( 156, 100, 12)
#define CGraphColor10     CRGB( 98, 101, 103)
#define CGraphColor11     CRGB( 66, 73, 73)
#define CGraphColor12     CRGB( 255, 111, 0)
#define CGraphColor13     CRGB(62, 39, 35)
#define CGraphColor14     CRGB(130, 119, 23)
#define CGraphColor15     CRGB( 51, 0, 153)
#define CGraphColor16     CRGB(102, 0, 153)
#define CGraphColor17     CRGB(255, 255, 0)
#define CGraphColor18     CRGB( 255, 51, 255)
#define CGraphColor19     CRGB(51, 255, 255)
#define CGraphColor20     CRGB( 0, 51, 255)


/*======== LANGUAGE =========*/

#define CUserDefaultLanguage @"UserSelectedLanguage"
#define CUserDefaultRegion @"UserSelectedRegion"
#define CLanguageEnglish @"en"
#define CLanguageFrench @"fr"

#define CCurrentLatitude @"CurrentLatitude"
#define CCurrentLongitude @"CurrentLongitude"
#define CCurrentLocation @"CurrentLocation"

#define CDistanceImperial @"imperial"
#define CDistanceMetric @"metric"

#define CDeviceUUID @"device_uuid"

#define CMQTTSearchingPlayer @"SearchingPlayer"
#define CMQTTOpponentSearchingPlayer @"SearchingPlayer"
#define CMQTTRunningPlayer @"RunningPlayer"

// Client Live
//#define CMQTTHOST @"72.52.133.120"
#define CMQTTHOST @"174.138.127.63"

// Mind Local
//#define CMQTTHOST @"174.138.127.63"

#define CMQTTUSERNAME @"runLiveMQTTAuth0"
#define CMQTTPASSWORD @"Thom*&%$()"


// In App Purchase product
#define CIAPMonthlyProduct @"com.runliveapp.MonthlySubscription_4.99"
#define CIAPYearlyProduct @"com.runliveapp.AnnualSubscription_49.99"



#define CNotificationTypeFollow @"follow"
#define CNotificationTypePhotoComment @"photoComment"
#define CNotificationTypePhotoLike @"photoLike"
#define CNotificationTypeMentionComment @"mentionComment"
#define CNotificationTypeSessionComment @"sessionComment"
#define CNotificationTypeSessionJoin @"sessionJoin"
#define CNotificationTypeSessionInvite @"sessionInvite"
#define CNotificationTypeSessionStart @"sessionStart"
#define CNotificationTypeSessionDelete @"sessionDeleted"
#define CNotificationTypeSessionStartSync @"sessionStartSync"
#define CNotificationTypeSessionResult @"sessionResult"
#define CNotificationTypePhotoLike @"photoLike"
#define CNotificationTypeVideoLike @"videoLike"
#define CNotificationTypeSessionStart1Mint @"sessionStartIn1Min"
#define CNotificationTypePhotoComment @"photoComment"
#define CNotificationTypeVideoComment @"videoComment"
#define CNotificationTypeActivitySessionComment @"activitySessionComment"
#define CNotificationTypeActivitySessionLike @"activitySessionLike"
#define CNotificationTypeSessionDeclined @"sessionDeclined"
#define CNotificationTypeSessionSynced @"sessionSynced"
#define CNotificationTypeRunStart @"runStart"
#define CNotificationTypeRunCancel @"runCancel"
#define CNotificationTypeChatMessage @"chatMessage"
#define CNotificationTypeExtendSubscriptionDate @"extendSubscriptionDate"
#define CNotificationTypeActivePremiumPlan @"markAsPremium"
#define CNotificationTypeDeleteProduct @"deleteProductUserInfo"
#define CNotificationTypePubNub @"PubNubChatMessage"
#define CAppleMusicCollection @"appleCollectionData"

#define CIAPAppleRecieptID @"RecieptIDAppleAccount"




#define CMapStrokeWidth 4

#define CCustomMapImage [CCachesDirectory stringByAppendingPathComponent:@"/RunLiveMapImages/"]

#define CLocalize(text) [[LocalizationSystem sharedLocalSystem] localizedStringForKey:text value:text]

#define CSetUserLanguage(language) [[LocalizationSystem sharedLocalSystem] setLanguage:language]
//#define CGetUserLanguage [[LocalizationSystem sharedLocalSystem] getLanguage]
#define CGetUserLanguage [CUserDefaults valueForKey:CUserDefaultLanguage]


#define CSongImagePath [CCachesDirectory stringByAppendingPathComponent:@"/RunLiveSongImages/"]

#define CAlbumImagePath [CCachesDirectory stringByAppendingPathComponent:@"/RunliveAlbumImages/"]
