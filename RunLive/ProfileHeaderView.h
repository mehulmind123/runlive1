//
//  ProfileHeaderView.h
//  RunLive
//
//  Created by mac-0006 on 04/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileHeaderView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblMonth;
@property (strong, nonatomic) IBOutlet UILabel *lblRunCount;
@property (strong, nonatomic) IBOutlet UILabel *lblMileCount;

@property (strong, nonatomic) IBOutlet UIButton *btnAll;
@property (strong, nonatomic) IBOutlet UIButton *btnRuns;

@property (strong, nonatomic) IBOutlet UIView *viewLineAll;
@property (strong, nonatomic) IBOutlet UIView *viewLineRuns;
@property (strong, nonatomic) IBOutlet UIView *viewHeaderTopLine;

+(id)InitProfileHeaderView;
@end
