//
//  CustomPhotoVideoMarker.m
//  RunLive
//
//  Created by mac-0005 on 13/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "CustomPhotoVideoMarker.h"

@implementation CustomPhotoVideoMarker

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imgCapture.layer.cornerRadius = CGRectGetHeight(self.imgCapture.frame)/2;
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;
    self.layer.borderWidth = .5;
    self.layer.borderColor = CRGB(69, 200, 254).CGColor;
}


@end
