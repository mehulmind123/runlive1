//
//  CustomTwoButtonAlertView.h
//  RunLive
//
//  Created by mac-0005 on 14/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTwoButtonAlertView : UIView
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblMessage;
}

@property(strong,nonatomic) IBOutlet UIButton *btnFirst,*btnSecond;
@property(strong,nonatomic) IBOutlet UIView *viewContainer;

+(id)initCustomTwoButtonAlertView;

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonFirstText:(NSString *)strBtnFirst ButtonSecondText:(NSString *)strBtnSecond;

@end
