//
//  SessionViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/8/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClipView.h"
#import "MIWheelRotationGesture.h"


@interface SessionViewController : SuperViewController<SPTAuthViewDelegate,MPMediaPickerControllerDelegate,AVSpeechSynthesizerDelegate,UIGestureRecognizerDelegate,CAAnimationDelegate,MIWheelRotationGestureDelegate>
{
    IBOutlet UIButton *btnSolo,*btnTeam,*btnFindSession;
    IBOutlet UILabel *lblDistance,*lblMKM;
    IBOutlet UILabel *lblSolo,*lblTeam;
    
    //Speedometer
    IBOutlet ClipView *objClip;
    IBOutlet UIView *viewmeter;
    IBOutlet NSLayoutConstraint *cnViewMeterTopSpace,*cnBtnSoloTopSpace;    
    IBOutlet UIImageView *imgRotation;
    MIWheelRotationGesture *objRotateGesture;
    
}

@end
