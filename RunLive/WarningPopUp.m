//
//  WarningPopUp.m
//  RunLive
//
//  Created by mac-00012 on 5/10/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "WarningPopUp.h"

@implementation WarningPopUp


-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)initWithTitle:(NSString *)title message:(NSString *)message
{
    [self.lblTitle setText:title];
    [self.lblTitle setPreferredMaxLayoutWidth:CViewWidth(self) - 16];
    [self.lblTitle setNeedsUpdateConstraints];
    
    [self.lblMessage setText:message];
    [self.lblMessage setPreferredMaxLayoutWidth:CViewWidth(self) - 52];
    [self.lblMessage setNeedsUpdateConstraints];
    
    
    CGFloat vHeight = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CViewSetHeight(self, vHeight);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.btnOk.layer.cornerRadius = 3;
    
    self.center = CScreenCenter;
    
    
}


@end
