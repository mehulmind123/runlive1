//
//  PrivacySettingViewController.m
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "PrivacySettingViewController.h"

@interface PrivacySettingViewController ()

@end

@implementation PrivacySettingViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    btnEveryone.selected = YES;
    btnEveryone.layer.cornerRadius = btnMyFriend.layer.cornerRadius =  2;
 
    [self btnProfileVisibilityClicked:[appDelegate.loginUser.profile_visibility isEqualToString:@"public"] ? btnEveryone : btnMyFriend];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action

- (IBAction)btnBackCLK:(id)sender
{
    
    if (([appDelegate.loginUser.profile_visibility isEqualToString:@"public"] && btnEveryone.selected) || ([appDelegate.loginUser.profile_visibility isEqualToString:@"private"] && btnMyFriend.selected))
    {
        // No change
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self customAlertViewWithTwoButton:@"" Message:CMessageSaveChanges ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:btnEveryone.selected ? @"public" : @"private" Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:^(id responseObject, NSError *error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
                [self.navigationController popViewControllerAnimated:YES];
        }];
    }
        
}

- (IBAction)btnProfileVisibilityClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    btnMyFriend.selected = btnEveryone.selected = NO;
    btnMyFriend.backgroundColor = btnEveryone.backgroundColor = CRGB(203, 210, 211);
    
    if(sender.tag == 0)  // Private
    {
        btnMyFriend.selected = YES;
        btnMyFriend.backgroundColor = CRGB(70, 216, 252);
    }
    else                // Public
    {
        btnEveryone.selected = YES;
        btnEveryone.backgroundColor = CRGB(70, 216, 252);
    }
}

@end
