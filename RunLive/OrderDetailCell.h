//
//  OrderDetailCell.h
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIView *viewContainer;
@property(weak,nonatomic) IBOutlet UIImageView *imgProduct;
@property(weak,nonatomic) IBOutlet UILabel *lblProductName;
@property(weak,nonatomic) IBOutlet UILabel *lblDescription;

@end
