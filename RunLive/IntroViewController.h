//
//  IntroViewController.h
//  RunLive
//
//  Created by mac-0008 on 22/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "IntroCollectionViewCell.h"
#import "ResetPasswordViewController.h"



@interface IntroViewController : SuperViewController
{
    IBOutlet UICollectionView *collectionIntro;
    IBOutlet TTTAttributedLabel *lblTermsAndConditions;

    IBOutlet NSLayoutConstraint *viewBottomX,*cnImgRunliveLogoY;
    IBOutlet UIView *viewBottom;
    IBOutlet UIButton *btnSignUp;
    IBOutlet UIView *viewSignUpLine;
    IBOutlet UIView *viewButtonSapartor;
    
    IBOutlet UIButton *btnLogin;
    IBOutlet UIView *viewLoginLine;
    IBOutlet ACFloatingTextField *txtEmail;
    IBOutlet ACFloatingTextField *txtCommon;
    IBOutlet ACFloatingTextField *txtPassword;
    IBOutlet ACFloatingTextField *txtConfirmPwd;
    IBOutlet UIButton *btnRegular;
    IBOutlet UIButton *btnSocial;
    IBOutlet UIButton *btnStrava;
    IBOutlet UIButton *btnForgetPwd;
    IBOutlet UIPageControl *pageControler;
    IBOutlet UIImageView *imgRun;
    
    
}


@end
