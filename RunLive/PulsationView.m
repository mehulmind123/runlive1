//
//  PulsationView.m
//  RunLive
//
//  Created by Eugene on 12.02.2018.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "PulsationView.h"

@interface PulsationView()

@property (nonatomic, strong) CAReplicatorLayer *replicatorCircleLayer;
@property (nonatomic, strong) CAReplicatorLayer *replicatorReverceCircleLayer;

@property (nonatomic, strong) UIBezierPath *initialCirclePath;
@property (nonatomic, strong) UIBezierPath *finaCirclePath;

@property (nonatomic, strong) CAShapeLayer *circleShapeLayer;
@property (nonatomic, strong) CAShapeLayer *reverceCircleShapeLayer;

@property (nonatomic, strong) CALayer *starsLayer;
@property (nonatomic, strong) CALayer *reverceStarsLayer;

@end

@implementation PulsationView {
    int initialCircleRadius;
    int finalCircleRadius;
    int additionalSpacingBehindWindow;
    int starsImageScalingValue;
    int starsImageSwingValue;
    float repeatInstanceDelay;
    float initialCircleLineWidth;
    float circleShadowOpacity;
    float circleShadowRadius;
    float secondRepeatInstanceStartDelay;
    int repeatInstanceCount;
    NSArray *starsImageOpacityValues;
    NSArray *circleOpacityValues;
    NSArray *circlelineWidthValues;
    CFTimeInterval animationDuration;
    UIColor *circleColor;
    NSString *starsImageName;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupInitialAnimationProperties];
        [self setupAnimationPathes];
        [self setupReplicators];
    }
    return self;
}

- (void)setupInitialAnimationProperties {
    starsImageName = @"ic_galaxy_ring4";
    
    // Krishna
    circleColor = CRGB(17, 216, 253);
    
    animationDuration = 3.0;
    circleShadowOpacity = 0.5;
    circleShadowRadius = 4.0;
    repeatInstanceCount = 2;
    initialCircleLineWidth = 2;
    additionalSpacingBehindWindow = 100;
    initialCircleRadius = 15;
    finalCircleRadius = self.bounds.size.width / 2 + additionalSpacingBehindWindow;
    starsImageOpacityValues = @[@1.0, @1.0, @1.0, @0.9, @0.9, @0.85, @0.75, @0.0];
    circleOpacityValues = @[@1.0, @1.0, @0.75, @0.75, @0.5, @0.25, @0];
    circlelineWidthValues = @[@2, @6, @7, @7, @5, @2];
    starsImageScalingValue = finalCircleRadius / initialCircleRadius;
    starsImageSwingValue = 10;
    repeatInstanceDelay = animationDuration / repeatInstanceCount;
    secondRepeatInstanceStartDelay = repeatInstanceDelay / 2;
}

- (void)setupAnimationPathes {
    self.initialCirclePath = [UIBezierPath
                              bezierPathWithArcCenter:self.center
                              radius:initialCircleRadius
                              startAngle:0
                              endAngle:M_PI * 2
                              clockwise:YES];
    self.finaCirclePath = [UIBezierPath
                           bezierPathWithArcCenter:self.center
                           radius:finalCircleRadius
                           startAngle:0
                           endAngle:M_PI * 2
                           clockwise:YES];
}

- (void)setupReplicators {
    self.circleShapeLayer = [self circleLayer];
    self.reverceCircleShapeLayer = [self circleLayer];
    
    self.starsLayer = [self imageLayer];
    self.reverceStarsLayer = [self imageLayer];
    
    self.replicatorCircleLayer = [self animationReplicator];
    self.replicatorReverceCircleLayer = [self animationReplicator];
    
    [self.replicatorCircleLayer addSublayer:self.circleShapeLayer];
    [self.replicatorCircleLayer insertSublayer:self.starsLayer
                                         above:self.circleShapeLayer];
    
    [self.replicatorReverceCircleLayer addSublayer:self.reverceCircleShapeLayer];
    [self.replicatorReverceCircleLayer insertSublayer:self.reverceStarsLayer
                                                above:self.reverceCircleShapeLayer];
    
    [self.layer addSublayer:self.replicatorCircleLayer];
    [self.layer addSublayer:self.replicatorReverceCircleLayer];
}

- (CAAnimationGroup *)imageAnimationGroup:(BOOL)reversed {
    CAKeyframeAnimation *imageOpacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    imageOpacityAnimation.values = starsImageOpacityValues;
    
    CABasicAnimation *imageFrameAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
    imageFrameAnimation.fromValue = @1.0;
    imageFrameAnimation.toValue = @(starsImageScalingValue);
    
    CABasicAnimation *imageFrameSwing = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    imageFrameSwing.fromValue = reversed ? @(starsImageSwingValue) : @1.0;
    imageFrameSwing.toValue = reversed ? @1.0 : @(starsImageSwingValue);
    
    CAAnimationGroup *imageGroupAnimation = [CAAnimationGroup new];
    imageGroupAnimation.fillMode = kCAFillModeForwards;
    imageGroupAnimation.duration = animationDuration;
    imageGroupAnimation.repeatCount = INFINITY;
    imageGroupAnimation.animations = @[imageOpacityAnimation, imageFrameAnimation, imageFrameSwing];
    imageGroupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    return imageGroupAnimation;
}

- (CAAnimationGroup *)circleAnimationGroup {
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    pathAnimation.fromValue = (__bridge id)(self.initialCirclePath.CGPath);
    pathAnimation.toValue = (__bridge id)(self.finaCirclePath.CGPath);
    
    CAKeyframeAnimation *lineWidthAnimation = [CAKeyframeAnimation animationWithKeyPath:@"lineWidth"];
    lineWidthAnimation.values = circlelineWidthValues;
    
    CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.values = circleOpacityValues;
    
    CAAnimationGroup *circleGroupAnimation = [CAAnimationGroup new];
    circleGroupAnimation.fillMode = kCAFillModeForwards;
    circleGroupAnimation.duration = animationDuration;
    circleGroupAnimation.repeatCount = INFINITY;
    circleGroupAnimation.animations = @[pathAnimation, lineWidthAnimation, opacityAnimation];
    circleGroupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    return circleGroupAnimation;
}

- (CAReplicatorLayer *)animationReplicator {
    CAReplicatorLayer *replicator = [CAReplicatorLayer new];
    replicator.instanceDelay = repeatInstanceDelay;
    replicator.instanceCount = repeatInstanceCount;
    replicator.repeatCount = INFINITY;
    
    return replicator;
}

- (CALayer *)imageLayer {
    CALayer *imageLayer = [CALayer new];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:starsImageName]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.frame = CGRectMake([self center].x - initialCircleRadius + 1,
                                 [self center].y - initialCircleRadius + 1,
                                 (initialCircleRadius + 1) * 2,
                                 (initialCircleRadius + 1) * 2);
    imageLayer = imageView.layer;
    
    return imageLayer;
}

- (CAShapeLayer *)circleLayer {
    CAShapeLayer *circleLayer = [CAShapeLayer new];
    circleLayer.path = self.initialCirclePath.CGPath;
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    circleLayer.strokeColor = circleColor.CGColor;
    circleLayer.lineWidth = initialCircleLineWidth;
    circleLayer.shadowColor = circleColor.CGColor;
    circleLayer.shadowOpacity = circleShadowOpacity;
    circleLayer.shadowRadius = circleShadowRadius;
    circleLayer.shadowOffset = CGSizeZero;

    return circleLayer;
}

- (void)start {
    [self.starsLayer addAnimation:[self imageAnimationGroup:NO]
                           forKey:@"imageAnimation"];
    [self.circleShapeLayer addAnimation:[self circleAnimationGroup]
                                 forKey:@"pathAnimation"];
    
    __weak __typeof(self)weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(secondRepeatInstanceStartDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.reverceStarsLayer addAnimation:[weakSelf imageAnimationGroup:YES]
                                          forKey:@"reverceImageAnimation"];
        [weakSelf.reverceCircleShapeLayer addAnimation:[weakSelf circleAnimationGroup]
                                                forKey:@"revercePathAnimation"];
    });
}

@end
