//
//  TeamMatchedHeader.h
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamMatchedHeader : UIView

@property(weak,nonatomic) IBOutlet UILabel *lblTeamName;
@property(weak,nonatomic) IBOutlet UIImageView *imgTag;
@end
