//
//  MessageDate.m
//  RunLive
//
//  Created by mac-0004 on 03/11/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MessageDate.h"

@implementation MessageDate

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    
    CGFloat radians = atan2f(self.transform.b, self.transform.a);
    CGFloat degrees = radians * (180 / M_PI);
    if (degrees == 0) {
        self.transform = CGAffineTransformMakeRotation(M_PI);
    }
}
@end
