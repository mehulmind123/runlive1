//
//  MIGenericTextField.m
//  Aiminaabee
//
//  Created by mac-00014 on 10/21/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import "MIGenericTextField.h"

@implementation MIGenericTextField

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if(Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 1)];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
}

@end
