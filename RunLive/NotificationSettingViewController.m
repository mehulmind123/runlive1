//
//  NotificationSettingViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/8/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "NotificationSettingViewController.h"
#import "NotificationSettingCell.h"

@interface NotificationSettingViewController ()

@end

@implementation NotificationSettingViewController
{
    NSArray *arrNotifications;
    
    BOOL isNotification,isSound,isVibrate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [tblNotification registerNib:[UINib nibWithNibName:@"NotificationSettingCell" bundle:nil] forCellReuseIdentifier:@"NotificationSettingCell"];

//    arrNotifications = @[@{@"title":@"Notifications ",@"SubTitle":@"Receive push notifications"},
//                    @{@"title":@"Sound",@"SubTitle":@"Play sound on push notification"},
//                    @{@"title":@"Vibrate",@"SubTitle":@"Vibrate on push notifications"}];

    arrNotifications = @[@{@"title":@"Notifications ",@"SubTitle":@"Receive push notifications"},
                         @{@"title":@"Sound",@"SubTitle":@"Play sound on push notification"}];

    [tblNotification reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableview Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotifications.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"NotificationSettingCell";
    NotificationSettingCell *cell = [tblNotification dequeueReusableCellWithIdentifier:strIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dicData = [arrNotifications objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = [dicData stringValueForJSON:@"title"];
    cell.lblSubTitle.text = [dicData stringValueForJSON:@"SubTitle"];
    cell.btnSwitch.tag = indexPath.row;
    switch (indexPath.row)
    {
        case 0:
            cell.btnSwitch.on = isNotification =  appDelegate.loginUser.notification.boolValue;
            break;
        case 1:
            cell.btnSwitch.on = isSound = appDelegate.loginUser.notification_sound.boolValue;
            break;
        case 2:
            cell.btnSwitch.on = isVibrate = appDelegate.loginUser.notification_vibrate.boolValue;
            break;
        default:
            break;
    }
    
    [cell.btnSwitch addTarget:self action:@selector(switchCLK:) forControlEvents:UIControlEventValueChanged];
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 75;
}

- (void)switchCLK:(UISwitch *)sender
{
    switch (sender.tag)
    {
        case 0:
            isNotification = sender.on;
            break;
        case 1:
            isSound = sender.on;
            break;
        case 2:
            isVibrate = sender.on;
            break;
        default:
            break;
    }
}

-(IBAction)btnBackCLK:(id)sender
{
    if (isNotification ==  appDelegate.loginUser.notification.boolValue && isSound ==  appDelegate.loginUser.notification_sound.boolValue && isVibrate ==  appDelegate.loginUser.notification_vibrate.boolValue)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self customAlertViewWithTwoButton:@"" Message:CMessageSaveChanges ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:isNotification ? @"on" : @"off" Sound:isSound ? @"on" : @"off" Vibrate:isVibrate ? @"on" : @"off" Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:^(id responseObject, NSError *error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
                [self.navigationController popViewControllerAnimated:YES];
        }];
    }
}

@end
