//
//  MotionManager.m
//  RunLive
//
//  Created by Eugene on 11.03.2018.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MotionManager.h"

@interface MotionManager()

@property (strong, nonatomic) CMMotionActivityManager *motionActivityManager;
@property (strong, nonatomic) NSOperationQueue *motionQueue;
@end

@implementation MotionManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.motionQueue = [NSOperationQueue new];
    }
    return self;
}

- (void)requestPermission {
    self.motionActivityManager = [CMMotionActivityManager new];
    [self startMotionDetection];
    [self stopMotionDetection];
}

- (void)startMotionDetection {
    if (!self.motionActivityManager) {
        self.motionActivityManager = [CMMotionActivityManager new];
    }
    __weak __typeof(self) weakSelf = self;
    
    [self.motionActivityManager
     startActivityUpdatesToQueue:self.motionQueue
     withHandler:^(CMMotionActivity * _Nullable activity) {
         dispatch_async(dispatch_get_main_queue(), ^{
             weakSelf.currentActivity = activity;
         });
     }];
}

- (void)checkMotionManagerStatus:(void (^_Nonnull)(BOOL isDenied))completion;{
    if (@available(iOS 11.0, *)) {
        completion([CMMotionActivityManager authorizationStatus] != CMAuthorizationStatusAuthorized);
    } else {
        if (!self.motionActivityManager) {
            self.motionActivityManager = [CMMotionActivityManager new];
        }
        if (![CUserDefaults objectForKey:CMMotionManagerStatus])
        {
            completion(YES);
        }
        else
        {
            [self.motionActivityManager
             queryActivityStartingFromDate:[NSDate new]
             toDate:[NSDate new]
             toQueue:self.motionQueue
             withHandler:^(NSArray<CMMotionActivity *> * _Nullable activities, NSError * _Nullable error) {
                 BOOL isDenied = (error.code == CMErrorMotionActivityNotAuthorized ||
                                  error.code == CMErrorMotionActivityNotAvailable ||
                                  error.code == CMErrorMotionActivityNotEntitled);
                 
                 completion(isDenied);
             }];
        }
        
    }
}

- (void)stopMotionDetection {
    if (!self.motionActivityManager) {
        [self.motionActivityManager stopActivityUpdates];
    }
}

@end
