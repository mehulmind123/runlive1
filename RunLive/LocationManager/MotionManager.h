//
//  MotionManager.h
//  RunLive
//
//  Created by Eugene on 11.03.2018.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MotionManager : NSObject

@property (nonatomic, strong) CMMotionActivity * __nullable currentActivity;

- (void)requestPermission;
- (void)checkMotionManagerStatus:(void (^_Nonnull)(BOOL isDenied))completion;
- (void)startMotionDetection;
- (void)stopMotionDetection;

@end
