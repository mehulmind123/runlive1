//
//  TblActivityCompleteSession+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivityCompleteSession+CoreDataProperties.h"

@implementation TblActivityCompleteSession (CoreDataProperties)

+ (NSFetchRequest<TblActivityCompleteSession *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblActivityCompleteSession"];
}

@dynamic activity_id;
@dynamic activity_main_type;
@dynamic activity_post_time;
@dynamic activity_text;
@dynamic capture;
@dynamic city;
@dynamic comments;
@dynamic image_map;
@dynamic is_like;
@dynamic isMapLoaded;
@dynamic position;
@dynamic runner_path;
@dynamic session_complete_time;
@dynamic session_id;
@dynamic session_name;
@dynamic sublocality;
@dynamic timestamp;
@dynamic total_burn_cal;
@dynamic total_comments;
@dynamic total_distance;
@dynamic total_likes;
@dynamic total_runner;
@dynamic user_id;
@dynamic user_image;
@dynamic user_name;
@dynamic is_commented;
@dynamic objActivityDate;

@end
