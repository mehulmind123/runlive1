//
//  OrderDetailViewController.m
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderDetailCell.h"
#import "TrackOrderViewController.h"
#import "ProductDetailsViewController.h"

#define CELLWIDTH CScreenWidth * 80/100

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController
{
    NSIndexPath *SelectedIndexPathTapBar;
    int currentPage;
    NSMutableArray *arrProduct;
    NSString *strTrackId;
    }

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnProblemOrder.layer.cornerRadius = btnTrackMyOrder.layer.cornerRadius = 3;
    
    currentPage = 0;
    [clOrder registerNib:[UINib nibWithNibName:@"OrderDetailCell" bundle:nil] forCellWithReuseIdentifier:@"OrderDetailCell"];
    
    arrProduct = [NSMutableArray new];

    [scrollViewContainer updateConstraintsIfNeeded];
    [self setAutomaticallyAdjustsScrollViewInsets:YES];
    
    [self getOrderDetail];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Attribute String

-(NSMutableAttributedString *)setAtributedString:(NSString *)strTitle FromString:(NSString *)strFromSring
{
    NSRange range = [strFromSring rangeOfString:strTitle];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:strFromSring];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(101, 107, 132) range:range];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(15) range: range];
    
    return text;
}


#pragma mark - Api Functions

-(void)getOrderDetail
{
    
    if (!self.strOrderId)
        return;
    
    [[APIRequest request] orderDetailWithOrderID:self.strOrderId completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            NSDictionary *dicData = [responseObject valueForKey:CJsonData];
            
            [arrProduct removeAllObjects];
            [arrProduct addObjectsFromArray:[dicData valueForKey:@"product_data"]];
            SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:0 inSection:0];
            [clOrder reloadData];
            
            pageControl.numberOfPages = arrProduct.count;
            pageControl.currentPage = currentPage > arrProduct.count ? arrProduct.count : currentPage;
            pageControl.hidden = arrProduct.count < 2;
//            [pageControl hideByHeight:arrProduct.count < 2];
            
            lblOrderStatus.text = [dicData stringValueForJSON:@"status"];
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
            lblTotalPoints.text = [numberFormatter stringFromNumber:[NSNumber numberWithInt:[dicData stringValueForJSON:@"points"].intValue]];
            
            NSDictionary *dicAddress = [dicData valueForKey:@"address"];
            
            lblName.text = [NSString stringWithFormat:@"Name: %@ %@",[dicAddress stringValueForJSON:@"name"],[dicAddress stringValueForJSON:@"fore_name"]];
            lblName.attributedText  = [self setAtributedString:@"Name:" FromString:lblName.text];
            
            lblPhone.text = [NSString stringWithFormat:@"Phone: %@",[dicAddress stringValueForJSON:@"phone_number"]];
            lblPhone.attributedText  = [self setAtributedString:@"Phone:" FromString:lblPhone.text];
            
            lblAdderss.text =[NSString stringWithFormat:@"Shipping address: %@, %@",[dicAddress stringValueForJSON:@"street"],[dicAddress stringValueForJSON:@"number"]];
            lblAdderss.attributedText  = [self setAtributedString:@"Shipping address:" FromString:lblAdderss.text];
            
            lblCode.text = [NSString stringWithFormat:@"Code: %@",[dicAddress stringValueForJSON:@"post_code"]];
            lblCode.attributedText  = [self setAtributedString:@"Code:" FromString:lblCode.text];
            
            strTrackId = nil;
            if (![[dicData stringValueForJSON:@"status"] isEqualToString:@"pending"])
                strTrackId = [dicData stringValueForJSON:@"track_number"];
            
            btnTrackMyOrder.enabled = strTrackId ? YES:NO;
        }
    }];
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrProduct.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"OrderDetailCell";
    OrderDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.transform = [SelectedIndexPathTapBar isEqual:indexPath] ? CGAffineTransformIdentity : TRANSFORM_CELL_VALUE;
    
    NSDictionary *dicData = [arrProduct objectAtIndex:indexPath.item];
    cell.lblProductName.text = [dicData stringValueForJSON:@"name"];
    cell.lblDescription.text = [dicData stringValueForJSON:@"short_description"];
    [cell.imgProduct setImageWithURL:[appDelegate resizeImage:nil Height:[NSString stringWithFormat:@"%f",CViewHeight(cell.imgProduct)*2] imageURL:[dicData stringValueForJSON:@"images"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width * CELLWIDTH)/CScreenWidth , CGRectGetHeight(clOrder.frame) - 10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2, 0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [arrProduct objectAtIndex:indexPath.item];
    ProductDetailsViewController *objProductDetails = [[ProductDetailsViewController alloc] init];
    objProductDetails.strProductId = [dicData stringValueForJSON:@"_id"];
    [self.navigationController pushViewController:objProductDetails animated:YES];
    
//    if (SelectedIndexPathTapBar)
//    {
//        OrderDetailCell *cell = (OrderDetailCell *)[clOrder cellForItemAtIndexPath:SelectedIndexPathTapBar];
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = TRANSFORM_CELL_VALUE;
//        }];
//        
//    }
//    
//    OrderDetailCell *cell = (OrderDetailCell *)[clOrder cellForItemAtIndexPath:indexPath];
//    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//        cell.transform = CGAffineTransformIdentity;
//    }];
//    
//    SelectedIndexPathTapBar = indexPath;
//    [clOrder scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
//    
//    [pageControl setCurrentPage:(int)SelectedIndexPathTapBar.item];
//    currentPage = (int)pageControl.currentPage;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    //    float pageWidth = (((CGRectGetWidth(clPlayers.frame))/CScreenWidth) + CELLWIDTH); // width + space
    float pageWidth = CELLWIDTH;
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = currentOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
    int index = newTargetOffset / pageWidth;
    
    [pageControl setCurrentPage:index];
    currentPage = (int)pageControl.currentPage;
    
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:index inSection:0];
    
    if (index == 0) // If first index
    {
        UICollectionViewCell *cell1 = [clOrder cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
        OrderDetailCell *cell = (OrderDetailCell *)cell1;
        
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        cell = (OrderDetailCell *)[clOrder cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
    }
    else
    {
        UICollectionViewCell *cell1 = [clOrder cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        OrderDetailCell *cell = (OrderDetailCell *)cell1;
        
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        
        index --; // left
        cell = (OrderDetailCell *)[clOrder cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
        index ++;
        index ++; // right
        cell = (OrderDetailCell *)[clOrder cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }
}


#pragma mark - Action Event

-(IBAction)btnBackCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnProblemCLK:(id)sender
{
    [self openMailComposer:nil recepients:@[@"support@runlive.fit"] body:nil isHTML:NO];
}

-(IBAction)btnTrackOrderCLK:(id)sender
{
    if (!strTrackId)
        return;
    
    TrackOrderViewController *objTrack = [[TrackOrderViewController alloc] init];
    objTrack.sttTrackId = strTrackId;
    [self.navigationController pushViewController:objTrack animated:YES];
}


@end
