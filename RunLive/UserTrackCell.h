//
//  UserTrackCell.h
//  Krishna_MapGraph
//
//  Created by mac-00012 on 6/22/17.
//  Copyright © 2017 mac-00012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPulseView.h"

@interface UserTrackCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIView *viewUser,*viewImageBorder;
@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnViewBottomSpace;

@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnUserViewHeight,*cnUserViewWidth;
@property(weak,nonatomic) IBOutlet NSLayoutConstraint *cnRankTagHeight,*cnRankTagWidth;

@property(weak,nonatomic) IBOutlet NSLayoutConstraint
    *cnViewBorderTrallingSpace,
    *cnViewBorderLeadingSpace,
    *cnViewBorderTopSpace,
    *cnViewBorderBottomSpace;


@property(weak,nonatomic) IBOutlet UIImageView *imgUser,*imgRankTag;
@property(weak,nonatomic) IBOutlet UIButton *btnUser;
@property(weak,nonatomic) IBOutlet UIView *viewAnimation;
@property(weak,nonatomic) IBOutlet UIImageView *imgLoginUserAnimation;


-(void)SetUserData:(NSDictionary *)dicData;

-(void)setUserUIIfCollectionViewZoom:(BOOL)isZoom cellSize:(float)size UserData:(NSDictionary *)dicData;

@end
