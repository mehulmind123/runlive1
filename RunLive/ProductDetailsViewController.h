//
//  ProductDetailsViewController.h
//  RunLive
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailsViewController : SuperViewController<UIScrollViewDelegate>
{
    IBOutlet UICollectionView *clProduct,*clSubProduct,*clProductZoom;
    IBOutlet UIButton *btnGetThis;
    IBOutlet UIWebView *webView;
    IBOutlet UILabel *lblProduct,*lblPrice,*lblTitle;
    IBOutlet UIPageControl *pageControlProduct,*pageControlSubProduct;
    
    IBOutlet NSLayoutConstraint *cnWebViewHeight;
}

@property(strong,nonatomic) NSString *strProductId;
@end
