//
//  TblActivityVideo+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivityVideo+CoreDataProperties.h"

@implementation TblActivityVideo (CoreDataProperties)

+ (NSFetchRequest<TblActivityVideo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblActivityVideo"];
}

@dynamic activity_id;
@dynamic activity_main_type;
@dynamic activity_post_time;
@dynamic activity_text;
@dynamic caption;
@dynamic comments;
@dynamic is_like;
@dynamic isAdminActivity;
@dynamic session_id;
@dynamic session_name;
@dynamic thumb_image;
@dynamic timestamp;
@dynamic total_comments;
@dynamic total_likes;
@dynamic user_id;
@dynamic user_image;
@dynamic user_name;
@dynamic video_height;
@dynamic video_mute;
@dynamic video_seek_time;
@dynamic video_url;
@dynamic video_width;
@dynamic is_commented;
@dynamic objActivityDate;

@end
