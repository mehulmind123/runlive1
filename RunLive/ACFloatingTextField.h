//
//  AppTextField.h
//  TFDemoApp
//
//  Created by Abhishek Chandani on 19/05/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

typedef enum {
    ACFloatingTextFiedEmail,
} ACFloatingTextFiedType;

#import <UIKit/UIKit.h>

@interface ACFloatingTextField : UITextField
{
    UIView *bottomLineView;
    
    NSString *strCorrect;
    NSString *strWrong;
}

@property (nonatomic,strong) UIColor *lineColor;
@property (nonatomic,strong) UIColor *placeHolderColor;
@property (nonatomic,strong) UIColor *selectedPlaceHolderColor;
@property (nonatomic,strong) UIColor *selectedLineColor;

@property (nonatomic,strong) UILabel *labelPlaceholder;

@property (nonatomic,strong) NSString *imageCorrect;
@property (nonatomic,strong) NSString *imageWrong;
@property (nonatomic,strong) UIImageView *txtRightView;
//@property (nonatomic,assign) BOOL isContinueCheck;

@property (assign) ACFloatingTextFiedType *textFieldType;

@property (assign) BOOL disableFloatingLabel;

-(instancetype)init;
-(instancetype)initWithFrame:(CGRect)frame;

-(void)setTextFieldPlaceholderText:(NSString *)placeholderText;
-(void)upadteTextField:(CGRect)frame;
-(void)floatTheLabel:(NSString *)checkType andText:(NSString *)strText;
-(void)floatTheLabel;

@end
