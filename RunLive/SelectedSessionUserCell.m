//
//  SelectedSessionUserCell.m
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SelectedSessionUserCell.h"

@implementation SelectedSessionUserCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    _btnAdd.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = .7;
    _viewUser.layer.borderColor = CRGB(53, 54, 76).CGColor;
    
    
}
@end
