//
//  MonthCell.m
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MonthCell.h"

@implementation MonthCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.lblMonth.layer.cornerRadius = CGRectGetHeight(_lblMonth.frame)/2;
    self.lblMonth.layer.masksToBounds = YES;
}

@end
