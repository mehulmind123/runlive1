//
//  VoiceChatPermissionView.h
//  RunLive
//
//  Created by mac-0005 on 12/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^voiceChatPermissionAcceptReject)(BOOL isGranted);

@interface VoiceChatPermissionView : UIView

@property (nonatomic,strong) IBOutlet UIButton
*btnVoiceChatGotForIt,
*btnVoiceChatNotNow;

@property(nonatomic,copy) voiceChatPermissionAcceptReject configurationVoiceChatPermissionAcceptReject;

+ (id)initVoiceChatPermissionView;

-(void)askForMicroPhonePermission;

@end
