//
//  StartActivityCell.m
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "StartActivityCell.h"

@implementation StartActivityCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    self.cnMapViewHeight.constant = (CScreenHeight*40)/100;
//    self.cnMapViewHeight.constant = 300;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = 1;
    _viewUser.layer.borderColor = CRGB(235, 241, 245).CGColor;
    
    _viewMap.layer.cornerRadius = _viewSubMapView.layer.cornerRadius = 3;
    _viewMap.layer.masksToBounds = NO;
    _viewSubMapView.layer.masksToBounds = YES;
    
    _viewMap.layer.cornerRadius = _viewSubMapView.layer.cornerRadius = 3.0;
    _viewMap.layer.shadowOffset = CGSizeMake(.8, .8);
    _viewMap.layer.shadowOpacity = 0.2;
    _viewMap.layer.shadowRadius = 3.5;
    
    _viewPosition.transform = CGAffineTransformMakeRotation(-M_PI/4);
    
    self.lblActivityText.preferredMaxLayoutWidth = CGRectGetWidth(self.lblActivityText.frame);
    self.lblComment1.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment1.frame);
    self.lblComment2.preferredMaxLayoutWidth = CGRectGetWidth(self.lblComment2.frame);
}

-(void)drawMapPath:(NSArray *)arrPath
{
    dispatch_async(GCDMainThread, ^{
        [self.mapView clearMGLMapView];
        self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        if (arrPath.count > 0)
        {
            [self.mapView MGLMapViewCenterCoordinate:arrPath];
            [self.mapView drawPolylineByUsingCoordinates:arrPath];
            [self.mapView setPathCenterInMapView:arrPath edgeInset:UIEdgeInsetsMake(50, 50, 50, 50)];
            
        }
    });
}

@end
