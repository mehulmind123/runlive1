//
//  ProductDetailsViewController.m
//  RunLive
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "ProductDetailCell.h"
#import "SubProductCell.h"
#import "MyBasketViewController.h"
#import "ImageZoomViewController.h"

@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController
{
    NSMutableArray *arrProduct;
    NSMutableArray *arrSubProduct;
    NSURLSessionDataTask *taskRunning;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:YES];
    
    btnGetThis.layer.cornerRadius = 3;
    
    [clSubProduct registerNib:[UINib nibWithNibName:@"SubProductCell" bundle:nil] forCellWithReuseIdentifier:@"SubProductCell"];
    [clProduct registerNib:[UINib nibWithNibName:@"ProductDetailCell" bundle:nil] forCellWithReuseIdentifier:@"ProductDetailCell"];
    
    arrProduct = [NSMutableArray new];
    arrSubProduct = [NSMutableArray new];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    [self getDataFromServer];
}


#pragma mark - General Functions

-(void)getDataFromServer
{
    if (!self.strProductId)
        return;
    
    if (taskRunning && taskRunning.state == NSURLSessionTaskStateRunning)
        [taskRunning cancel];
    
   taskRunning =  [[APIRequest request] productDetails:self.strProductId completed:^(id responseObject, NSError *error)
    {
        NSLog(@"%@",responseObject);
        
        NSDictionary *dicData = [responseObject valueForKey:CJsonData];
        
        lblTitle.text = [dicData stringValueForJSON:@"name"].uppercaseString;
        lblProduct.text = [dicData stringValueForJSON:@"name"];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        lblPrice.text = [numberFormatter stringFromNumber:[NSNumber numberWithInt:[dicData stringValueForJSON:@"points"].intValue]];

        
        NSString *strDescription = [NSString stringWithFormat:@"<html>" "<style type=\"text/css\">" "body { background-color:transparent; font-family:Gilroy; font-size:14;color: #7A7E93;}" "</style>" "<body>" "<p>%@</p>" "</body></html>", [dicData stringValueForJSON:@"description"]];

        [webView loadHTMLString:strDescription baseURL:nil];
//        [webView loadHTMLString:[dicData stringValueForJSON:@"description"] baseURL:nil];
        
        // Current product images here......
        [arrProduct removeAllObjects];
        [arrProduct addObjectsFromArray:[dicData valueForKey:@"images"]];
        [clProduct reloadData];
        
        pageControlProduct.numberOfPages = arrProduct.count;
        pageControlProduct.currentPage = 0;
        pageControlProduct.hidden = arrProduct.count < 2 ? YES : NO;
        [pageControlProduct reloadInputViews];
        
        
        // Sub Product data here......
        [arrSubProduct removeAllObjects];
        [arrSubProduct addObjectsFromArray:[dicData valueForKey:@"products"]];
        [clSubProduct reloadData];
        
        NSInteger rowCount =  arrSubProduct.count/3;
        if (arrSubProduct.count%3 > 0)
            rowCount += 1;
        pageControlSubProduct.numberOfPages = rowCount;
        pageControlSubProduct.currentPage = 0;
        pageControlSubProduct.hidden = arrSubProduct.count < 4 ? YES : NO;
        [pageControlSubProduct reloadInputViews];
        
    }];
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clProduct])
        return arrProduct.count;
    
    return arrSubProduct.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clProduct])
    {
        static NSString *identifier = @"ProductDetailCell";
        ProductDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.imgProduct.contentMode = UIViewContentModeScaleAspectFit;
        [cell.imgProduct setImageWithURL:[appDelegate resizeImage:nil Height:[NSString stringWithFormat:@"%f",CViewHeight(clProduct)*2] imageURL:[arrProduct objectAtIndex:indexPath.item]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        return cell;
    }
    else
    {
        static NSString *identifier = @"SubProductCell";
        SubProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        NSDictionary *dicProduct = [arrSubProduct objectAtIndex:indexPath.item];
        cell.lblProductName.text = [dicProduct stringValueForJSON:@"name"];
        cell.lblProductPrice.text = [dicProduct stringValueForJSON:@"points"];
        [cell.imgProduct setImageWithURL:[NSURL URLWithString:[dicProduct stringValueForJSON:@"image"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clProduct])
        return CGSizeMake(CScreenWidth , CGRectGetHeight(clProduct.frame));
    
    return CGSizeMake(CScreenWidth/3 , CGRectGetHeight(clSubProduct.frame));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clProduct])
    {
        ImageZoomViewController *objZoom = [[ImageZoomViewController alloc] init];
        objZoom.imageArray = arrProduct;
        [self.navigationController pushViewController:objZoom animated:NO];
    }
    else
    {
        NSDictionary *dicProduct = [arrSubProduct objectAtIndex:indexPath.item];
        self.strProductId = [dicProduct stringValueForJSON:@"_id"];
        [self getDataFromServer];
    }
}


#pragma mark - ScrollView Method

- (void)scrollViewDidScroll:(UIScrollView *)lscrollView
{
    if ([lscrollView isEqual:clProduct])
    {
        if(arrProduct.count == 0)
            return;
        
        int page =round(lscrollView.contentOffset.x/lscrollView.bounds.size.width);
        [pageControlProduct setCurrentPage:page];
    }
    else if([lscrollView isEqual:clSubProduct])
    {
        if(arrSubProduct.count == 0)
            return;
        
        int page =round(lscrollView.contentOffset.x/lscrollView.bounds.size.width);
        if ((lscrollView.contentOffset.x/lscrollView.bounds.size.width) - page > 0.3) {
            page = page +1;
        }
        [pageControlSubProduct setCurrentPage:page];

    }
}

#pragma mark - WebView Method
- (void)webViewDidFinishLoad:(UIWebView *)webView1
{
    CGRect frame = webView1.frame;
    frame.size.height = 1;
    webView1.frame = frame;
    CGSize fittingSize = [webView1 sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    webView1.frame = frame;
    cnWebViewHeight.constant = fittingSize.height;
}

#pragma mark - Action Event

-(IBAction)btnClikedEvent:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
            if ([[self navigationController] viewControllers].count>=2)
            {
                if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[MyBasketViewController class]])
                {
                    [self.navigationController popViewControllerAnimated:NO];
                }
                else
                {
                    MyBasketViewController *obj = [[MyBasketViewController alloc] init];
                    [self.navigationController pushViewController:obj animated:NO];
                }
            }
            else
            {
                MyBasketViewController *obj = [[MyBasketViewController alloc] init];
                [self.navigationController pushViewController:obj animated:NO];
            }
        }
            break;
        case 2:
        case 3:
        {
            if (!self.strProductId)
                return;
            
            [[APIRequest request] addProductToBasket:self.strProductId completed:^(id responseObject, NSError *error)
            {
                if ([[self navigationController] viewControllers].count>=2)
                {
                    if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[MyBasketViewController class]])
                    {
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    else
                    {
                        MyBasketViewController *obj = [[MyBasketViewController alloc] init];
                        [self.navigationController pushViewController:obj animated:NO];
                    }
                }
                else
                {
                    MyBasketViewController *obj = [[MyBasketViewController alloc] init];
                    [self.navigationController pushViewController:obj animated:NO];
                }
            }];
        }
            break;
        default:
            break;
    }
}


@end
