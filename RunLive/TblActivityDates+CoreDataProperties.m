//
//  TblActivityDates+CoreDataProperties.m
//  RunLive
//
//  Created by mac-00012 on 7/26/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblActivityDates+CoreDataProperties.h"

@implementation TblActivityDates (CoreDataProperties)

+ (NSFetchRequest<TblActivityDates *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblActivityDates"];
}

@dynamic activity_date;
@dynamic activity_filter_type;
@dynamic activity_type;
@dynamic show_date;
@dynamic timestamp;
@dynamic total_month_distance;
@dynamic total_month_run;
@dynamic user_id;
@dynamic user_name;
@dynamic objActivityCompleteSession;
@dynamic objActivityPhoto;
@dynamic objActivityRunSession;
@dynamic objActivitySyncSession;
@dynamic objActivityVideo;

@end
