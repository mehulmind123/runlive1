//
//  ReadMSGCell.h
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadMSGCell : SWTableViewCell

@property(weak,nonatomic) IBOutlet UIImageView *imgUser,*imgOnline,*imgVerifiedUser;

@property(weak,nonatomic) IBOutlet UIView *viewContainer ;
@property(weak,nonatomic) IBOutlet UILabel *lblName,*lbMessage,*lblTime;


@end
