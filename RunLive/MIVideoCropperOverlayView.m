//
//  MIVideoCropperOverlayView.m
//  Runlive
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import "MIVideoCropperOverlayView.h"

@implementation MIVideoCropperOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled=YES;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(c, YES);
    
    // Fill black
    CGContextSetFillColorWithColor(c, [UIColor colorWithWhite:0 alpha:0.7].CGColor);
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGContextAddRect(c, CGRectMake(0, 0, screenSize.width, rect.size.height));
    CGContextFillPath(c);
    
    // Clear inside
    CGContextClearRect(c, self.clearRect);
    CGContextFillPath(c);
    
    
    CGContextSaveGState(c);
    CGContextSetShouldAntialias(c, NO);
    
    // Grid
    CGContextSetStrokeColorWithColor(c, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(c, 3);
    
    CGContextAddRect(c, self.clearRect);
    CGContextStrokePath(c);
}

@end
