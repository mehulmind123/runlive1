//
//  ChatTableViewCell.m
//  TextToU
//
//  Created by mac-0009 on 7/25/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import "ChatTableViewCell.h"

@implementation ChatTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    viewLabel.layer.cornerRadius = 2.0f;
    viewLabel.layer.masksToBounds = YES;
    
    CGFloat radians = atan2f(self.transform.b, self.transform.a);
    CGFloat degrees = radians * (180 / M_PI);
    if (degrees == 0) {
        self.transform = CGAffineTransformMakeRotation(M_PI);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
