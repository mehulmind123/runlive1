//
//  AudioSettingViewController.h
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudioSettingViewController : SuperViewController
{
    IBOutlet UISlider *sldAudio;
    IBOutlet UILabel *lblCount;
    
    IBOutlet NSLayoutConstraint *cnLblXPoint;
}

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnMinusClicked:(id)sender;
- (IBAction)btnPlusClicked:(id)sender;
- (IBAction)lblAudioSliderValueChanged:(UISlider *)sender;

@end
