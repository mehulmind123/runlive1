//
//  RunningMusicViewController.m
//  RunLive
//
//  Created by mac-0006 on 22/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "RunningMusicViewController.h"
#import "RunningMusicCell.h"
#import "PlayListCell.h"
#import "RunningViewController.h"


#define CELLWIDTH (CScreenWidth*80)/100

@interface RunningMusicViewController () <SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate>
{
    NSMutableArray *arrMusic;
    NSIndexPath *SelectedIndexPathTapBar;
    NSInteger minTotal, secTotal, minCurrent, secCurrent;
    NSTimer *timerNext, *timerPrevious;
    BOOL isRepeat, isShuffle, isPlayFirstTime, isFromSpotify, isForwardStarted, isShuffleSongStarted;
    
}

//@property (nonatomic, strong) SPTAudioStreamingController *player;
//@property (nonatomic) BOOL isChangingProgress;

@end

@implementation RunningMusicViewController
@synthesize currentplayMusicNo;

#pragma mark - Life cycle
#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrMusic = [[NSMutableArray alloc]initWithArray:self.arrMusicList];
    
    if(!(Is_iPhone_6) || !(Is_iPhone_6_PLUS))
        [viewBtnContent setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
    
    btnGrid.selected = NO;
    tblMusicList.hidden = YES;
    
    [sliderMusic setThumbImage:[UIImage imageNamed:@"ic_music_progress_dot"] forState:UIControlStateNormal];
    [collMusic registerNib:[UINib nibWithNibName:@"RunningMusicCell" bundle:nil] forCellWithReuseIdentifier:@"RunningMusicCell"];
    [tblMusicList registerNib:[UINib nibWithNibName:@"PlayListCell" bundle:nil] forCellReuseIdentifier:@"PlayListCell"];
    

    btnRepeat.hidden = btnShuffleLast.hidden = YES;
    sliderMusic.userInteractionEnabled  = NO;
    collMusic.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setCurrentSongUI:NO shouldReload:YES shouldAnimate:NO];
    });
    
    //TODO: Initialize the spotify player
    
    if (arrMusic.count > 0)
    {
        TblMusicList *objMusic = arrMusic[0];
        isFromSpotify = !objMusic.isApple.boolValue;
        
        isRepeat = self.isRepeatMusic;
        isShuffle = self.isShuffleMusic;
        
        btnRepeat.selected = isRepeat;
        btnShuffleLast.selected = isShuffle;
        
        if (isFromSpotify)
        {
            [self lockScreenMusicControlForSpotify];
            
            appDelegate.strMainURI = objMusic.main_spotify_uri;
            
            btnPlay.selected = self.isSongPlaying;
            
            if (!appDelegate.player)
                [appDelegate setSpotifyPlayer];
            
            appDelegate.configureSpotifyLogin = ^(NSError *error)
            {
                [self updateUI];
            };
            
            appDelegate.configureStartedPlaying = ^{
                
                btnPlay.selected = YES;
                
//                currentplayMusicNo = (int)appDelegate.player.metadata.currentTrack.indexInContext;
//                [self updateUI];
                
            };
            
//            appDelegate.configureTrackChanged = ^(SPTPlaybackMetadata *metadata)
//            {
//                if (isRepeat)
//                    return;
//                
//                if (isShuffle)
//                {
//                    if (isShuffleSongStarted)
//                        return;
//                    
//                    [self shuffleSpotifyMusic];
//                    return;
//                }
//                
//                currentplayMusicNo = (int)appDelegate.player.metadata.currentTrack.indexInContext;
//                [self updateUI];
//            };

            
            appDelegate.configureDidChangePosition = ^(NSTimeInterval position)
            {
                [self setTimeOnSpotifyMusic];
            };
            
            appDelegate.configureNewTrackStarted = ^(NSError *error)
            {
                btnPlay.selected = YES;
            };
            
            appDelegate.configureTrackStopped = ^(BOOL stopPlaying)
            {
                // Stop music here..
                if (stopPlaying)
                {
                    if (btnPlay.selected)
                    {
                        [self btnMusicCntrolCLK:btnPlay];
                    }
                    
                    return ;
                }

                
                isShuffleSongStarted = NO;
                
//For last song getting finished automatically
                
                if (!btnPlay.selected)
                    return;
                
                [self btnMusicCntrolCLK:btnPlayNext];
            };
        }
        else
        {
            
            [self addAndRemoveItunesMusicNotificationObserver:YES];
            btnPlay.selected = self.isSongPlaying;
            [self songAllreadyPlaying:currentplayMusicNo];
           
        }
    }
};

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (!isFromSpotify)
        [self addAndRemoveItunesMusicNotificationObserver:NO];
}


#pragma mark - Music Related Method
#pragma mark -

-(void)PlayMusicWithPath:(int)index
{
    if (isFromSpotify)
        return;
    
    if (arrMusic.count > 0)
    {
        TblMusicList *objMusic = arrMusic[index];
        lblArtistName.text = objMusic.artist_name;
        lblSongName.text = objMusic.song_name;
    }
}


#pragma mark - Random Audio Player (For Suffle)
-(void)getRandomTrackNo
{
    int randomInt1 = arc4random() % [arrMusic count];
    currentplayMusicNo = randomInt1;
    
    [self scrollCollectionViewAtTheSpecificIndex:currentplayMusicNo];
}

#pragma mark - Collection View Delegate
#pragma mark

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"RunningMusicCell";
    
    RunningMusicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if ([SelectedIndexPathTapBar isEqual:indexPath])
    {
        cell.transform = CGAffineTransformIdentity;
        cell.alpha = 1.0;
    }
    else
    {
        cell.transform = TRANSFORM_CELL_VALUE; // the new cell will always be transform and without animation
        cell.alpha = 1.0;
    }
    
    TblMusicList *objMusic = arrMusic[indexPath.item];
    
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:[[appDelegate createSongImagePath] stringByAppendingPathComponent:objMusic.song_image]], 0.9)];
    
    if (isFromSpotify)
    {
        if (objMusic.cover_image)
            [cell.imgSongCoverPic setImageWithURL:[NSURL URLWithString:objMusic.cover_image] placeholderImage:nil options:SDWebImageRetryFailed completed:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        else
            cell.imgSongCoverPic.image = nil;
        
        [self updateUI];
    }
    else
        cell.imgSongCoverPic.image = [UIImage imageWithData:imageData];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrMusic.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width * CELLWIDTH)/CScreenWidth , CGRectGetHeight(collMusic.frame) - 10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2, 0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([SelectedIndexPathTapBar isEqual:indexPath])
        return;
    
    if (SelectedIndexPathTapBar)
    {
        RunningMusicCell *cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:SelectedIndexPathTapBar];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
    }
    
    RunningMusicCell *cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        cell.transform = CGAffineTransformIdentity;
    }];
    
    SelectedIndexPathTapBar = indexPath;
    currentplayMusicNo  = (int)indexPath.item;
    
    [collMusic scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self PlayMusicWithPath:(int)indexPath.row];
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    //return;
    
    if ([scrollView isEqual:collMusic])
    {
        float pageWidth = CELLWIDTH;
        float currentOffset = scrollView.contentOffset.x;
        float targetOffset = targetContentOffset->x;
        float newTargetOffset = 0;
        
        if (targetOffset > currentOffset)
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
        else
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
        
        if (newTargetOffset < 0)
            newTargetOffset = 0;
        else if (newTargetOffset > scrollView.contentSize.width)
            newTargetOffset = scrollView.contentSize.width;
        
        targetContentOffset->x = currentOffset;
        [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
        int index = newTargetOffset / pageWidth;
        
        SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:index inSection:0];
        
        if (index == 0) // If first index
        {
            UICollectionViewCell *cell1 = [collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
            RunningMusicCell *cell = (RunningMusicCell *)cell1;
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = CGAffineTransformIdentity;
                cell.alpha = 1.0;
            }];
            
            cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
                cell.alpha = 1.0;
            }];
        }
        else
        {
            UICollectionViewCell *cell1 = [collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            RunningMusicCell *cell = (RunningMusicCell *)cell1;
            ;
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = CGAffineTransformIdentity;
                cell.alpha = 1.0;
            }];
            
            index --; // left
            cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
                cell.alpha = 1.0;
            }];
            
            index ++;
            index ++; // right
            cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            
            [UIView animateWithDuration:ANIMATION_SPEED animations:^{
                cell.transform = TRANSFORM_CELL_VALUE;
                cell.alpha = 1.0;
            }];
        }
        
        currentplayMusicNo = (int)SelectedIndexPathTapBar.item;
        
        if(isFromSpotify)
        {
            btnPlay.selected = YES;
            [self shuffleSpotifyMusic];
            return;
        }
        
        [self PlayMusicWithPath:(int)SelectedIndexPathTapBar.item];
    }
}

-(void)scrollCollectionViewAtTheSpecificIndex:(int)index
{
    if (SelectedIndexPathTapBar)
    {
        RunningMusicCell *cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:SelectedIndexPathTapBar];
        cell.transform = TRANSFORM_CELL_VALUE; // the new cell will always be transform and without animation
        cell.alpha = 1.0;
        SelectedIndexPathTapBar = nil;
    }
    
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:(int)currentplayMusicNo inSection:0];
    [collMusic reloadItemsAtIndexPaths:[NSArray arrayWithObject:SelectedIndexPathTapBar]];
    [collMusic scrollToItemAtIndexPath:SelectedIndexPathTapBar atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];

}

#pragma mark - UITableview Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMusic.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"PlayListCell";
    PlayListCell *cell = [tblMusicList dequeueReusableCellWithIdentifier:strIdentifier];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    cell.btnPlayPause.hidden = YES;
    
    [cell.lblTitle setFont:CFontGilroyBold(15)];
    [cell.lblSubTitle setFont:CFontGilroyMedium(12)];
    
    TblMusicList *objMusic = arrMusic[indexPath.item];
    
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:[[appDelegate createSongImagePath] stringByAppendingPathComponent:objMusic.song_image]], 0.9)];
    
    cell.lblTitle.text = objMusic.song_name;
    cell.lblSubTitle.text = [NSString stringWithFormat:@"%@ - %@",objMusic.artist_name,objMusic.song_name];
    
    if (isFromSpotify)
    {
        if (objMusic.song_image)
            cell.imgAlbum.image = [UIImage imageWithData:imageData];
        else
            cell.imgAlbum.image = nil;
    }
    else
        cell.imgAlbum.image = [UIImage imageWithData:imageData];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentplayMusicNo = (int)indexPath.row;
    SelectedIndexPathTapBar = indexPath;
    
    if (isFromSpotify)
    {
        [self shuffleSpotifyMusic];
        return;
    }
    
    [self PlayMusicWithPath:(int)indexPath.row];
}

#pragma mark - Action
#pragma mark

- (IBAction)btnCloseClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (self.configureGetMusicData)
            self.configureGetMusicData(currentplayMusicNo, isFromSpotify ? appDelegate.player.playbackState.position : avAudioPlayer.currentTime, btnPlay.selected, isRepeat, isShuffle);
    }];
}

- (IBAction)btnMusicCntrolCLK:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:      // Repeat
        {
            btnRepeat.selected = !btnRepeat.selected;
            isRepeat = btnRepeat.selected;
            
            if (!isFromSpotify)
            {
                if (isRepeat)
                    appDelegate.appleMPMusicPlayerController.repeatMode =isRepeat ? MPMusicRepeatModeOne : MPMusicRepeatModeNone;
            }

        }
            break;
        case 1:      // Play Previous
        {
            if (currentplayMusicNo > 0)
            {
                currentplayMusicNo--;
                
                if (isFromSpotify)
                {
                    [self shuffleSpotifyMusic];
                    return;
                }
                
                // If Suffle is ON....
                
                if (isShuffle)
                    [self getRandomTrackNo];
                
                [appDelegate.appleMPMusicPlayerController skipToPreviousItem];
            }
        }
            break;
        case 2:      // Play
        {
            if (isFromSpotify)
            {
                [appDelegate.player setIsPlaying:!appDelegate.player.playbackState.isPlaying callback:nil];
                btnPlay.selected = !appDelegate.player.playbackState.isPlaying;
                return;
            }

            if ([self getMPMusicPlaySate])
                [self mpMusicPlayerPayPause:YES];
            else
                [self mpMusicPlayerPayPause:NO];
        }
            break;
        case 3:      // Play Next
        {
            if (arrMusic.count-1 > currentplayMusicNo)
                currentplayMusicNo++;
            else
                currentplayMusicNo = 0;
            
            if (isFromSpotify)
            {
                [self shuffleSpotifyMusic];
                return;
            }
            
            // If Suffle is ON....
            if (isShuffle)
                [self getRandomTrackNo];
            
            [appDelegate.appleMPMusicPlayerController skipToNextItem];

        }
            break;
        case 4:      // Shuffle
        {
            btnShuffleLast.selected = !btnShuffleLast.selected;
            isShuffle = btnShuffleLast.selected;
            
            if (!isFromSpotify)
            {
                if (isShuffle)
                    appDelegate.appleMPMusicPlayerController.shuffleMode =isShuffle ? MPMusicShuffleModeSongs : MPMusicShuffleModeOff;
            }

        }
            break;
    }
}


- (IBAction)btnGridAndListClicked:(UIButton *)sender
{
    if(btnGrid.selected)
    {
        viewGrid.hidden = btnGrid.selected = NO;
        tblMusicList.hidden = YES;
        [self scrollCollectionViewAtTheSpecificIndex:(int)SelectedIndexPathTapBar.item];
        
        if (isFromSpotify)
            [self updateUI];
    }
    else
    {
        viewGrid.hidden = btnGrid.selected = YES;
        tblMusicList.hidden = NO;
        [tblMusicList reloadData];
    }
}

#pragma mark - Slider Events
#pragma mark -

- (IBAction)MoveMusicSlider:(UISlider *)sender
{
    return;
    
//    if(isFromSpotify)
//        return;
//    
//    if (avAudioPlayer.isPlaying)
//        [avAudioPlayer setCurrentTime:sender.value];
//    else
//        sliderMusic.value = 0.0;
}

- (IBAction)proggressTouchDown:(id)sender
{
    if (isFromSpotify)
        appDelegate.isChangingProgress = YES;
}

- (IBAction)seekValueChanged:(UISlider *)sender
{
    if(!isFromSpotify)
        return;
    
    [appDelegate.player seekTo:sender.value callback:^(NSError *error) {
        [appDelegate.player setIsPlaying:YES callback:nil];
        appDelegate.isChangingProgress = NO;
    }];
}


#pragma mark - Spotify Functions
#pragma mark -

- (void)setTimeOnSpotifyMusic
{
    if(appDelegate.isChangingProgress)
        return;
    
    if(isForwardStarted)
        return;
    
    NSTimeInterval totalDuration = appDelegate.player.metadata.currentTrack.duration;
    NSTimeInterval currentDuration = appDelegate.player.playbackState.position;
    
    NSInteger ti = (NSInteger)totalDuration;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    
    NSInteger ti1 = (NSInteger)currentDuration;
    NSInteger curSeconds = (ti1 % 60);
    NSInteger curMinutes = ((ti1 / 60) % 60);
    
    sliderMusic.value = currentDuration;
    [self playerUpdateReachTime];
    
    
    lblCurrentAndTotalTime.text = [NSString stringWithFormat:@"%ld:%02ld | %ld:%02ld", (long)curMinutes, (long)curSeconds,(long)minutes, seconds];
    
    if(isRepeat && roundf(appDelegate.player.playbackState.position) == roundf(appDelegate.player.metadata.currentTrack.duration) - 1)
    {
        isForwardStarted = YES;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [appDelegate.player seekTo:0.0 callback:^(NSError *error) {
                [appDelegate.player setIsPlaying:YES callback:nil];
                [self setTimeOnSpotifyMusic];
                isForwardStarted = NO;
            }];
        });
    }
    else if(isRepeat && roundf(appDelegate.player.playbackState.position) == roundf(appDelegate.player.metadata.currentTrack.duration) - 1)
    {
        [self shuffleSpotifyMusic];
    }
}

- (void)updateUI
{
    if (!isFromSpotify)
        return;
    
    btnRepeat.hidden = btnShuffleLast.hidden = NO;
    sliderMusic.userInteractionEnabled  = YES;
    collMusic.userInteractionEnabled  = YES;
    
    if (arrMusic.count <= 0)
        return;
    
    if (currentplayMusicNo > arrMusic.count-1)
        return;
    
    RunningMusicCell *cell = (RunningMusicCell *)[collMusic cellForItemAtIndexPath:[NSIndexPath indexPathForItem:currentplayMusicNo inSection:0]];
    
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    TblMusicList *objMusic = [arrMusic objectAtIndex:currentplayMusicNo];
    
    if (objMusic.song_name)
        lblSongName.text = objMusic.song_name;
    
    if (objMusic.artist_name)
        lblArtistName.text = objMusic.artist_name;
    
    [SPTTrack trackWithURI: [NSURL URLWithString:objMusic.song_url]
               accessToken:auth.session.accessToken
                    market:nil
                  callback:^(NSError *error, SPTTrack *track) {
                      
                      sliderMusic.maximumValue = appDelegate.player.metadata.currentTrack.duration;
                      
                      [self setTimeOnSpotifyMusic];
                      
                      NSURL *imageURL = track.album.largestCover.imageURL;
                      
                      if (!imageURL)
                      {
                          NSLog(@"Album %@ doesn't have any images!", track.album);
                          cell.imgSongCoverPic = nil;
                          [collMusic reloadItemsAtIndexPaths:[NSArray arrayWithObject:SelectedIndexPathTapBar]];
                          return;
                      }
                      
                      [cell.imgSongCoverPic setImageWithURL:imageURL placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          
                          if (!image)
                          {
                              NSLog(@"Couldn't load cover image with error: %@", error);
                              return;
                          }
                          
                          objMusic.cover_image = [imageURL absoluteString];
                          [[[Store sharedInstance] mainManagedObjectContext] save];
                          
                      } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                      
                  }];
}

- (void)shuffleSpotifyMusic
{
    if(isShuffle)
    {
        int randomInt1 = arc4random() % [arrMusic count];
        currentplayMusicNo = randomInt1;
    }

    [self setCurrentSongUI:YES shouldReload:YES shouldAnimate:!isShuffle];
}

- (void)setCurrentSongUI:(BOOL)shouldStartSong shouldReload:(BOOL)shouldReload shouldAnimate:(BOOL)shouldAnimate
{
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:(int)currentplayMusicNo inSection:0];
    
    if (shouldReload)
        [collMusic reloadData];
    
    [collMusic scrollToItemAtIndexPath:SelectedIndexPathTapBar atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:shouldAnimate];
    
    if(shouldStartSong)
    {
        [appDelegate.player playSpotifyURI:appDelegate.strMainURI startingWithIndex:currentplayMusicNo startingWithPosition:0 callback:^(NSError *error)
         {
             if (error != nil)
             {
                 NSLog(@"*** failed to play: %@", error);
                 return;
             }
             else
                 isShuffleSongStarted = YES;
         }];
    }
    else
        [self updateUI];
}


#pragma mark - Spotify Control Lock Screen
-(void)lockScreenMusicControlForSpotify
{
    if (!isFromSpotify)
    {
        return;
    }
    
    //...Control from Lock Screen Command Center
    MPRemoteCommandCenter *remoteCommandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    [remoteCommandCenter.previousTrackCommand setEnabled:YES];
    [remoteCommandCenter.previousTrackCommand addTarget:self action:@selector(spotiyLockPlayPrevious)];
    
    [remoteCommandCenter.pauseCommand setEnabled:YES];
    [remoteCommandCenter.pauseCommand addTarget:self action:@selector(spotiyLocktogglePlayPause)];
    
    [remoteCommandCenter.playCommand setEnabled:YES];
    [remoteCommandCenter.playCommand addTarget:self action:@selector(spotiyLocktogglePlayPause)];
    
    [remoteCommandCenter.nextTrackCommand setEnabled:YES];
    [remoteCommandCenter.nextTrackCommand addTarget:self action:@selector(spotiyLockPlayNext)];
}

-(void)spotiyLockPlayPrevious
{
    NSLog(@"spotiyLockPlayPrevious");
    [self btnMusicCntrolCLK:btnPlayBack];
}

-(void)spotiyLocktogglePlayPause
{
    NSLog(@"spotiyLocktogglePlayPause");
    [self btnMusicCntrolCLK:btnPlay];
}

-(void)spotiyLockPlayNext
{
    NSLog(@"spotiyLockPlayNext");
    [self btnMusicCntrolCLK:btnPlayNext];
}

- (void)playerUpdateReachTime
{
    if (!isFromSpotify)
    {
        return;
    }
    
    if([lblSongName.text isEqualToString:@"Loading..."])
        return;
    
    NSMutableDictionary *nowPlayingInfo = [NSMutableDictionary dictionary];
    
    [nowPlayingInfo setObject:appDelegate.player.metadata.currentTrack.name forKey:MPMediaItemPropertyTitle];
    [nowPlayingInfo setObject:appDelegate.player.metadata.currentTrack.artistName forKey:MPMediaItemPropertyArtist];
    
    NSNumber *duration = [NSNumber numberWithInt:appDelegate.player.metadata.currentTrack.duration];
    NSNumber *reachTime = [NSNumber numberWithInt:appDelegate.player.playbackState.position];
    if (duration) [nowPlayingInfo setObject:duration forKey:MPMediaItemPropertyPlaybackDuration];
    if (reachTime) [nowPlayingInfo setObject:reachTime forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [nowPlayingInfo setObject:btnPlay.selected ? @1.0 : @0.0 forKey:MPNowPlayingInfoPropertyPlaybackRate];
    
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nowPlayingInfo];
}


#pragma mark  - APPLE MUSIC RELATED FUNCTIONS

-(void)songAllreadyPlaying:(int)index
{
    [self playItunesMusic];
    sliderMusic.value = appDelegate.appleMPMusicPlayerController.currentPlaybackRate;
}

- (void)updateTime:(NSTimer *)timer
{
    if (isFromSpotify)
        return;
    
    
    sliderMusic.value = appDelegate.appleMPMusicPlayerController.currentPlaybackTime;
    
    secCurrent = (int)appDelegate.appleMPMusicPlayerController.currentPlaybackTime % 60;
    minCurrent = (int)appDelegate.appleMPMusicPlayerController.currentPlaybackTime / 60 % 60;
    
    secTotal = (int)sliderMusic.maximumValue % 60;
    minTotal = (int)sliderMusic.maximumValue / 60 % 60;
    
    lblCurrentAndTotalTime.text = [NSString stringWithFormat:@"%ld:%02ld | %ld:%02ld", (long)minCurrent, (long)secCurrent,(long)minTotal, (long)secTotal];
}


#pragma mark - MPMusicPlayerController Related Methods

-(void)playItunesMusic
{
    [self updateMusicUIForItunes];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
}

-(BOOL)getMPMusicPlaySate
{
    BOOL isStop = NO;
    MPMusicPlaybackState playbackState = appDelegate.appleMPMusicPlayerController.playbackState;
    if (playbackState == MPMusicPlaybackStatePlaying)
    {
        isStop = NO;
    }
    else
    {
        isStop = YES;
    }
    
    return isStop;
}

-(void)mpMusicPlayerPayPause:(BOOL)isPlay
{
    if (isPlay)
    {
        [appDelegate.appleMPMusicPlayerController play];
        btnPlay.selected = YES;
        NSLog(@"Play song === ");
    }
    else {
        [appDelegate.appleMPMusicPlayerController pause];
        btnPlay.selected = NO;
        
        NSLog(@"Pause song === ");
    }
}

-(void)updateMusicUIForItunes
{
    MPMediaItem *objAppleSong = appDelegate.appleMPMusicPlayerController.nowPlayingItem;
    NSString *strArtistApple = objAppleSong.artist ? objAppleSong.artist : @"";
    NSString *strSongNameApple = [objAppleSong valueForProperty:MPMediaItemPropertyTitle];
    lblArtistName.text = strArtistApple;
    lblSongName.text = strSongNameApple;
    
    NSNumber *duration=[objAppleSong valueForProperty:MPMediaItemPropertyPlaybackDuration];
    sliderMusic.maximumValue = duration.integerValue;

    NSArray *arrTemp = arrMusic.mutableCopy;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(songId == %@)", [NSString stringWithFormat:@"%llu",objAppleSong.persistentID]];
    NSArray *arrLogin = [arrTemp filteredArrayUsingPredicate:predicate];
    
    
    if (arrLogin.count > 0)
    {
//        MPMediaItem *objFindItem = arrLogin[0];
        TblMusicList *objFindItem = arrLogin[0];
        NSUInteger index = [arrMusic indexOfObject:objFindItem];
        NSLog(@"APPLE SONG NO ======= >>> %ld ",(long)index);
        currentplayMusicNo = (int)index;
        
        [self scrollCollectionViewAtTheSpecificIndex:(int)index];
    }
}



#pragma mark Media player notification handlers

-(void)addAndRemoveItunesMusicNotificationObserver:(BOOL)isAddObserver
{
    if (isAddObserver)
    {
        // Register for music player notifications
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self
                               selector:@selector(handleNowPlayingItemChanged:)
                                   name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        [notificationCenter addObserver:self
                               selector:@selector(handlePlaybackStateChanged:)
                                   name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        [notificationCenter addObserver:self
                               selector:@selector(handleExternalVolumeChanged:)
                                   name:MPMusicPlayerControllerVolumeDidChangeNotification
                                 object:appDelegate.appleMPMusicPlayerController];
        
        [appDelegate.appleMPMusicPlayerController beginGeneratingPlaybackNotifications];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMusicPlayerControllerVolumeDidChangeNotification
                                                      object:appDelegate.appleMPMusicPlayerController];
        [appDelegate.appleMPMusicPlayerController endGeneratingPlaybackNotifications];
    }
    
}

// When the now playing item changes, update song info labels and artwork display.
- (void)handleNowPlayingItemChanged:(id)notification
{
    // Ask the music player for the current song.
    NSLog(@"handleNowPlayingItemChanged ========= >>> ");
    [self updateMusicUIForItunes];
}

// When the playback state changes, set the play/pause button appropriately.
- (void)handlePlaybackStateChanged:(id)notification
{
    NSLog(@"handlePlaybackStateChanged ========= >>> ");
    btnPlay.selected = ![self getMPMusicPlaySate];
}

// When the volume changes, sync the volume slider
- (void)handleExternalVolumeChanged:(id)notification
{
    NSLog(@"handleExternalVolumeChanged ========= >>> ");
}



@end
