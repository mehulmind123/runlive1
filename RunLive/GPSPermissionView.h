//
//  GPSPermissionView.h
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GPSPermissionView : UIView

@property (nonatomic,strong) IBOutlet UIButton
    *btnGPSGoForIt,
    *btnGPSNotNow;

+ (id)initGPSPermissionView;

@end
