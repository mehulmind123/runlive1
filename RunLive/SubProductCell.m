//
//  SubProductCell.m
//  RunLive
//
//  Created by mac-00012 on 10/24/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SubProductCell.h"

@implementation SubProductCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.imgProduct.layer.masksToBounds = YES;
    
    self.viewContainer.layer.cornerRadius = 3;
    self.viewContainer.layer.masksToBounds = YES;
}

@end
