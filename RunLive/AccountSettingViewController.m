//
//  AccountSettingViewController.m
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "AccountSettingViewController.h"
#import "ChangePWDViewController.h"
#import "HKHealthStore+AAPLExtensions.h"

@interface AccountSettingViewController ()

@end

@implementation AccountSettingViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    txtViewBio.font = CFontGilroyRegular(17);
    txtViewBio.textColor = CRGB(32, 34, 49);
    txtViewBio.maxNumberOfLines = 0;
    txtViewBio.maxHeight = 200;
    txtViewBio.minNumberOfLines = 0;
    txtViewBio.animateHeightChange = YES;
    txtViewBio.delegate = self;
    
    btnMale.layer.cornerRadius = btnSave.layer.cornerRadius = btnFemale.layer.cornerRadius = btnImperial.layer.cornerRadius = btnMetric.layer.cornerRadius = 2;
    
    btnMale.selected = YES;
    
    [txtFeet setPickerData:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12"]];
    [txtInches setPickerData:@[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11"]];
    
    
    [self setUserDataFromLocal];
    
    if (!appDelegate.loginUser.profileStatus.boolValue)
    {
        // Show pop here
        WarningPopUp *objWar = [WarningPopUp viewFromXib];
        [objWar initWithTitle:@"Who are you?" message:@"Please tell us about yourself!"];
        [self presentViewOnPopUpViewController:objWar shouldClickOutside:NO dismissed:nil];
        [objWar.btnOk touchUpInsideClicked:^{
            [self dismissOverlayViewControllerAnimated:YES completion:nil];
            [self askPermissionForHealhKit];
        }];
    }
    else
    {
            [self askPermissionForHealhKit];
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)askPermissionForHealhKit
{
    
//        if ([HKHealthStore isHealthDataAvailable])
//        {
//            NSSet *writeDataTypes = [appDelegate.healthStore dataTypesToWrite];
//            NSSet *readDataTypes = [appDelegate.healthStore dataTypesToRead];
//
//            [appDelegate.healthStore requestAuthorizationToShareTypes:writeDataTypes readTypes:readDataTypes completion:^(BOOL success, NSError *error)
//             {
//                 if (!success)
//                 {
//                     NSLog(@"You didn't allow HealthKit to access these read/write data types. In your app, try to handle this error gracefully when a user decides not to provide access. The error was: %@. If you're using a simulator, try it on a device.", error);
//                 }
//                 else
//                 {
//                     NSLog(@"Premission granted =========== >>>> ");
//                 }
//             }];
//        }
}

#pragma mark - General Functions

-(void)setUserDataFromLocal
{
    
    [self btnGenderClicked:[appDelegate.loginUser.gender isEqualToString:@"male"] ? btnMale : btnFemale];
    [self btnUnitsClicked:[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? btnMetric : btnImperial];
    
    txtFirstName.text = appDelegate.loginUser.first_name;
    txtLastName.text = appDelegate.loginUser.last_name;
    txtEmail.text = appDelegate.loginUser.email;
    txtViewBio.text = appDelegate.loginUser.about_us;
    txtLink.text = appDelegate.loginUser.link;

    if ([appDelegate.loginUser.weight isBlankValidationPassed])
        txtWeight.text = [appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? appDelegate.loginUser.weight : [NSString stringWithFormat:@"%.1f",appDelegate.loginUser.weight.floatValue*2.20462];
    else
    {
        txtWeight.text = nil;
    }
    
    if ([appDelegate.loginUser.height isBlankValidationPassed])
    {
        txtHeight.text = appDelegate.loginUser.height;
        [self getfeetAndInches:appDelegate.loginUser.height.floatValue];
    }
    else
    {
        txtHeight.text = nil;
        txtFeet.text = txtInches.text = nil;
    }
    
    
    if (appDelegate.loginUser.is_default_image.boolValue)
        imgUser.image = CPlaceholderUserImage;
    else
    {
        [imgUser setImageWithURL:[NSURL URLWithString:appDelegate.loginUser.picture] placeholderImage:CPlaceholderUserImage options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    if (![appDelegate.loginUser.date_of_birth isEqualToString:@"0000-00-00"] && appDelegate.loginUser.date_of_birth)
    {
        txtdob.text = appDelegate.loginUser.date_of_birth;
        
        NSDateFormatter *dtFomrater = [[NSDateFormatter alloc] init];
        dtFomrater.dateFormat = @"dd MMMM yyyy";
        NSDate *dtSelected = [dtFomrater dateFromString:appDelegate.loginUser.date_of_birth];
        
        [txtdob setDatePickerWithDateFormat:@"dd MMMM yyyy"defaultDate:dtSelected ? dtSelected : [NSDate date]];
        [txtdob setDatePickerMode:UIDatePickerModeDate];
    }
}

#pragma mark - Crop Image Delegate

- (void)finish:(UIImage *)image didCancel:(BOOL)cancel
{
    if (!cancel)
    {
        [[APIRequest request] uploadUserProfilePicture:image completed:^(id responseObject, NSError *error)
         {
             appDelegate.loginUser.is_default_image = @NO;
             imgUser.image = image;
         }];
    }
}


#pragma mark - Units Related Functions

-(void)updateUIAccordingToUnit
{
    /*
     Metric
     
     Distance - KM
     Weight - KG
     Height - CM
     */
    
    /*
     Imperial
     
     Distance - miles
     Weight - pounds
     Height - feet
     */
    
    if (btnImperial.selected)
    {
        lblWeigth.text = @"Weight (pound)";
        
        if ([txtWeight.text isBlankValidationPassed])
            txtWeight.text = [NSString stringWithFormat:@"%.1f",txtWeight.text.floatValue*2.20462];
        
        lblHeight.text = @"Height";
        viewHeightImperial.hidden = NO;
        txtHeight.hidden = viewHeightSeprator.hidden = YES;
    }
    else
    {
        lblWeigth.text = @"Weight (kg)";
        
        if ([txtWeight.text isBlankValidationPassed])
            txtWeight.text = [NSString stringWithFormat:@"%.1f",txtWeight.text.floatValue/2.20462];

        lblHeight.text = @"Height (cm)";
        viewHeightImperial.hidden = YES;
        txtHeight.hidden = viewHeightSeprator.hidden = NO;
    }
    
}

-(NSString *)getFinalWeight
{
    NSString *strFinalWeigth;
    
    if (btnImperial.selected)
    {
        //Weight - Pounds
        // kg * 2.20462
        strFinalWeigth = [NSString stringWithFormat:@"%.1f",txtWeight.text.floatValue/2.20462];
    }
    else
    {
        //Weight - KG
        strFinalWeigth = txtWeight.text;
    }
    
    
    return strFinalWeigth;
    
}

// Height Related Functions

-(void)getfeetAndInches:(float)centimeter
{
    const float INCH_IN_CM = 2.54;
    
    NSInteger numInches = (NSInteger) roundf(centimeter / INCH_IN_CM);
    NSInteger feet = numInches / 12;
    NSInteger inches = numInches % 12;
    
    txtFeet.text = [NSString stringWithFormat:@"%@", @(feet)];
    txtInches.text = [NSString stringWithFormat:@"%@", @(inches)];
}

-(NSString *)getFinalHeight
{
    NSString *strFinalHeight;
    
    if (btnImperial.selected)
    {
        //height - CM
        // 1 * 30.48   -- > feet
        // 1 * 2.54  -- >  inch
        
        float feetCM = txtFeet.text.floatValue * 30.48;
        float InchCM = txtInches.text.floatValue * 2.54;
        float totalHeight = feetCM + InchCM;
        
        strFinalHeight = [NSString stringWithFormat:@"%.1f",totalHeight];
    }
    else
    {
        //Weight - KG
        strFinalHeight = txtHeight.text;
    }
    
    return strFinalHeight;
}


#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    cnTextViewBioHeight.constant = height > 30 ? height : 30;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText =  [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    return finalText.length < 251;
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    NSString *strFinalWeight = [self getFinalWeight];
    NSString *strFinalHeight = [self getFinalHeight];
    
    if (![txtdob.text isBlankValidationPassed])
    {
        [self customAlertViewWithOneButton:@"" Message:CMessageBirthDate ButtonText:@"OK" Animation:YES completed:nil];
        return;
    }
    
    if (strFinalWeight.floatValue < 1)
    {
        [self customAlertViewWithOneButton:@"" Message:CMessageWieght ButtonText:@"OK" Animation:YES completed:nil];
        return;
    }

    if (strFinalHeight.floatValue < 1)
    {
        [self customAlertViewWithOneButton:@"" Message:CMessageHeight ButtonText:@"OK" Animation:YES completed:nil];
        return;
    }
    
    if ([appDelegate.loginUser.first_name isEqualToString:txtFirstName.text] && [appDelegate.loginUser.about_us isEqualToString:txtViewBio.text] && [appDelegate.loginUser.link isEqualToString:txtLink.text] && [appDelegate.loginUser.last_name isEqualToString:txtLastName.text] && [appDelegate.loginUser.date_of_birth isEqualToString:txtdob.text] && [appDelegate.loginUser.weight isEqualToString:strFinalWeight] && [appDelegate.loginUser.height isEqualToString:strFinalHeight] && (([appDelegate.loginUser.gender isEqualToString:@"male"] && btnMale.selected) || ([appDelegate.loginUser.gender isEqualToString:@"female"] && btnFemale.selected)) && (([appDelegate.loginUser.units isEqualToString:CDistanceMetric] && btnMetric.selected) || ([appDelegate.loginUser.units isEqualToString:CDistanceImperial] && btnImperial.selected)))
    {
        // No change.....
        [self moveToBackPreviousScreen];
    }
    else
    {
        [self customAlertViewWithTwoButton:@"" Message:CMessageSaveChanges ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                [[APIRequest request] updateProfile:txtFirstName.text LastName:txtLastName.text DOB:txtdob.text Weight:strFinalWeight Height:strFinalHeight Gender:btnMale.selected ? @"male" : @"female" Notification:nil Sound:nil Vibrate:nil Audio:nil Units:btnImperial.selected ? CDistanceImperial : CDistanceMetric ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:txtViewBio.text Link:txtLink.text completed:^(id responseObject, NSError *error)
                 {
                     [self moveToBackPreviousScreen];
                 }];
            }
            else
                [self moveToBackPreviousScreen];
        }];
    }
}

-(void)moveToBackPreviousScreen
{
    [self storeAgeCountToLocal];
    if (appDelegate.loginUser.profileStatus.boolValue)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        // Call api here for update profile status...
        appDelegate.window.rootViewController = [appDelegate setTabBarController];
//        appDelegate.loginUser.profileStatus = @YES;
//        [[Store sharedInstance].mainManagedObjectContext save];
        
        [[APIRequest request] updateProfileStatus:^(id responseObject, NSError *error)
        {
            // Check profile status..
        }];
    }
    
}

-(void)storeAgeCountToLocal
{
    NSDateFormatter *dtForm = [[NSDateFormatter alloc] init];
    dtForm.dateFormat = @"dd MMMM yyyy";
    NSDate *userDate = [dtForm dateFromString:appDelegate.loginUser.date_of_birth];
    NSDate *curDate = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian
                            ];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:userDate toDate:curDate options:0];
    
    NSLog(@"Difference in date components: %li/%li/%li", (long)components.day, (long)components.month, (long)components.year);

    if (components)
    {
        appDelegate.loginUser.age = [NSString stringWithFormat:@"%li",(long)components.year];
        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
}

- (IBAction)btnSelectImageClicked:(id)sender
{
    
    [self presentImagePickerSource:^(UIImage *image) {
        
        if (image)
        {
            MIImageCropperViewController *objCrop = [[MIImageCropperViewController alloc] initWithImage:image withDelegate:self ImageSizeValidation:YES withViewController:self];
            [self.navigationController presentViewController:objCrop animated:YES completion:nil];
        }
    } AllowEditing:NO];
}

- (IBAction)btnGenderClicked:(UIButton *)sender
{
   btnFemale.selected = btnMale.selected = NO;
    btnFemale.backgroundColor = btnMale.backgroundColor = CRGB(203, 210, 211);
    
    if(sender.tag == 0)  // Male
    {
        btnMale.selected = YES;
        btnMale.backgroundColor = CRGB(70, 216, 252);
    }
    else                // Female
    {
        btnFemale.selected = YES;
        btnFemale.backgroundColor = CRGB(70, 216, 252);
    }
}

- (IBAction)btnUnitsClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    btnImperial.selected = btnMetric.selected = NO;
    btnImperial.backgroundColor = btnMetric.backgroundColor = CRGB(203, 210, 211);
    
    if(sender.tag == 0)  // Kilometer
    {
        btnImperial.selected = YES;
        btnImperial.backgroundColor = CRGB(70, 216, 252);
        [self getfeetAndInches:txtHeight.text.floatValue];
    }
    else                // Miles
    {
        float feetCM = txtFeet.text.floatValue * 30.48;
        float InchCM = txtInches.text.floatValue * 2.54;
        float totalHeight = feetCM + InchCM;
        txtHeight.text = [NSString stringWithFormat:@"%.1f",totalHeight];
        
        btnMetric.selected = YES;
        btnMetric.backgroundColor = CRGB(70, 216, 252);
    }
    
    [self updateUIAccordingToUnit];
    
}

- (IBAction)btnChangePasswordClicked:(id)sender
{
    ChangePWDViewController *objCha = [[ChangePWDViewController alloc] init];
    [self.navigationController pushViewController:objCha animated:YES];
}

- (IBAction)btnSaveClicked:(id)sender
{
    [self btnBackClicked:nil];
}

@end
