//
//  DayCell.m
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "DayCell.h"

@implementation DayCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.lblDay.layer.cornerRadius = CGRectGetHeight(_lblDay.frame)/2;
    self.lblDay.layer.masksToBounds = YES;
}

@end
