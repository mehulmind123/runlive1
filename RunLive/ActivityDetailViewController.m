//
//  ActivityDetailViewController.m
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "MyActivityResultCollectionViewCell.h"
#import "shareView.h"
#import "PhotoDetailsViewController.h"
#import "RunSummaryResultCell.h"
#import "RunnerPositionCell.h"
#import "RunScoreBoardCell.h"
#import "RunScoreBoardBlueTeamCell.h"
#import "ResultRunCategoryCell.h"
#import "ResultRunnerCell.h"
#import "UserRunSummaryViewController.h"



#define ViewGraphHeight 207
#define CGraphStartMinValue 5



#define CTeamRed @"team_red"
#define CTeamBlue @"team_blue"
#define CTeamSolo @"team_solo"
#define CTeamType @"team_type"
#define CUserPosition @"user_position"
#define CUserPositionEmptyCell @"user_position_emptyCell"
#define CUserPositionEmptyCellType @"user_position_emptyCell_type"

#define TRANSFORM_RUNNERPOSITION_UP_CELL_VALUE CGAffineTransformMakeScale(1.1, 1.1)
#define TRANSFORM_RUNNERPOSITION_DOWN_CELL_VALUE CGAffineTransformMakeScale(0.9, .9)


@interface ActivityDetailViewController ()
{
    NSArray *arryMyData;
    NSDictionary *dicSessionDetail;
    BOOL isRunTypeSolo,isTeamWinRed;
    
    NSMutableArray *arrWinRunner,*arrUserList,*arrRunnerPosition;
    NSIndexPath *selectedCategoryIndexpath;
}

@end

@implementation ActivityDetailViewController

#pragma mark - life cycle
#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    arrWinRunner = [NSMutableArray new];
    arrUserList = [NSMutableArray new];
    arrRunnerPosition = [NSMutableArray new];
    
    selectedCategoryIndexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    imgUser.layer.cornerRadius = imgUser.frame.size.width/2;
    btnProfileImage.clipsToBounds = true;
    
    [clRunnerPositionl registerNib:[UINib nibWithNibName:@"RunnerPositionCell" bundle:nil] forCellWithReuseIdentifier:@"RunnerPositionCell"];
    [clmyActvityValue registerNib:[UINib nibWithNibName:@"RunSummaryResultCell" bundle:nil] forCellWithReuseIdentifier:@"RunSummaryResultCell"];
    [clmyActvityValue registerNib:[UINib nibWithNibName:@"MyActivityResultCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MyActivityResultCollectionViewCell"];
    [tblScore registerNib:[UINib nibWithNibName:@"RunScoreBoardCell" bundle:nil] forCellReuseIdentifier:@"RunScoreBoardCell"];
    [tblScore registerNib:[UINib nibWithNibName:@"RunScoreBoardBlueTeamCell" bundle:nil] forCellReuseIdentifier:@"RunScoreBoardBlueTeamCell"];
    [clCategory registerNib:[UINib nibWithNibName:@"ResultRunCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"ResultRunCategoryCell"];
    [clRunners registerNib:[UINib nibWithNibName:@"ResultRunnerCell" bundle:nil] forCellWithReuseIdentifier:@"ResultRunnerCell"];
    
    viewTriangle.transform = CGAffineTransformMakeRotation(-M_PI/4);
    [self getResultFromServer:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    [self getActivityDetailFromServer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Api Functions

-(void)getActivityDetailFromServer
{
    if (!self.strActivityId)
        return;
    
    // Competed session details only ,..............
    [[APIRequest request] activityDetailWithType:@"3" ActivityId:self.strActivityId completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            // Do your stuff here...
            
            NSDictionary *dicData = [responseObject valueForKey:CJsonData];
            self.strSelectedUserId = [dicData stringValueForJSON:@"user_id"];
            lblTitle.text = [NSString stringWithFormat:@"%@'S RUN",[dicData stringValueForJSON:@"user_name"]].uppercaseString;
            [imgUser setImageWithURL:[appDelegate resizeImage:@"82" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

            [btnProfileImage touchUpInsideClicked:^{
                //
                [appDelegate moveOnProfileScreen:[dicData stringValueForJSON:@"user_name"] UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
            }];
            
            NSDictionary *dicUserRunDetail = [dicData valueForKey:@"user_run_detail"];
            NSDictionary *dicCompleted = [dicUserRunDetail valueForKey:@"completed"];
            
            lblCity.text = [dicUserRunDetail stringValueForJSON:@"locality"];
            lblSubLocality.text = [dicUserRunDetail stringValueForJSON:@"sub_locality"];

            
            if ([dicCompleted stringValueForJSON:@"rank"].integerValue < 4)
            {
                imgPosition.hidden = viewTriangle.hidden = NO;
                lblWinPosition.text = [dicCompleted stringValueForJSON:@"rank"];
            }
            else
                imgPosition.hidden = viewTriangle.hidden = YES;

            NSDate *date = [self convertDateFromString:[dicData stringValueForJSON:@"createdAt"] isGMT:YES formate:CDateFormater];
            NSDateFormatter *dtForm = [NSDateFormatter initWithDateFormat:@"dd MMMM yyyy"];
            NSString *strDate = [dtForm stringFromDate:date];
            lblDate.text = strDate;
            
            [self drawMapPath:[dicUserRunDetail valueForKey:@"running"] Capture:[dicUserRunDetail valueForKey:@"capture"]];
            
            NSMutableArray *arrUserDataTemp = [NSMutableArray new];
            [arrUserDataTemp addObject:@{@"image":@"time",@"result":[dicCompleted stringValueForJSON:@"total_time"],@"quantities":@"TIME",@"unit":@""}];
            
            if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
                [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicCompleted stringValueForJSON:@"total_distance"].floatValue/1000],@"quantities":@"DISTANCE",@"unit":@"km"}];
            else
                [arrUserDataTemp addObject:@{@"image":@"distance",@"result":[NSString stringWithFormat:@"%.2f",[dicCompleted stringValueForJSON:@"total_distance"].floatValue * 0.000621371],@"quantities":@"DISTANCE",@"unit":@"miles"}];
            
            [arrUserDataTemp addObject:@{@"image":@"cal",@"result":[appDelegate convertCaloriesWithSelectedFormate:[dicCompleted stringValueForJSON:@"total_calories"]] ,@"quantities":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"EST KCALS" : @"EST CALORIES",@"unit":@"cal"}];

            
            [arrUserDataTemp addObject:@{@"image":@"speed",@"result":[appDelegate convertSpeedInWithSelectedFormate:[dicCompleted stringValueForJSON:@"total_distance"].floatValue overTime:[NSString stringWithFormat:@"%@",[dicCompleted stringValueForJSON:@"total_time"]]],@"quantities":@"PACE",@"unit":[appDelegate.loginUser.units isEqualToString:CDistanceMetric] ? @"/km" : @"/mi"}];

            
            if ([dicCompleted stringValueForJSON:@"average_bpm"].floatValue > 0)
                [arrUserDataTemp addObject:@{@"image":@"heartbeat",@"result":[NSString stringWithFormat:@"%.2f",[dicCompleted stringValueForJSON:@"average_bpm"].floatValue],@"quantities":@"AVG. BPM",@"unit":@""}];
            
            arryMyData = arrUserDataTemp.mutableCopy;
            [clmyActvityValue reloadData];
            
            [self drawGraph];
        }
    }];
}

#pragma mark - Map View Related Functions
- (void)mapviewDidTapMarker:(NSDictionary *)userInfo;
{
    NSDictionary *dicData = userInfo;
    PhotoDetailsViewController *objPhoto = [[PhotoDetailsViewController alloc] initWithNibName:@"PhotoDetailsViewController" bundle:nil];
    objPhoto.strActivityID = [dicData stringValueForJSON:@"_id"];
    objPhoto.strActivityType = [dicData stringValueForJSON:@"type"];
    [self.navigationController pushViewController:objPhoto animated:true];
}

-(void)drawMapPath:(NSArray *)arrPath Capture:(NSArray *)arrCapture
{
    [self setupForMapView:arrPath];
    [self addMarkerOnCapture:arrCapture];
}


-(void)setupForMapView:(NSArray *)arrLatLong
{
    [mapView clearMGLMapView];
    
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [mapView setMapViewDelegate:self];
    
    if (arrLatLong.count > 0)
    {
        [mapView MGLMapViewCenterCoordinate:arrLatLong];
        [mapView drawPolylineByUsingCoordinates:arrLatLong];
        
        if (arrLatLong.count > 1)
            [self addMarkerOnMapView:arrLatLong IsStartPosition:YES];   // If Start and End position available then....
        else
            [self addMarkerOnMapView:arrLatLong IsStartPosition:NO]; // Only end position availalbe
    }
}


-(void)addMarkerOnMapView:(NSArray *)arrMarker IsStartPosition:(BOOL)isStartPos
{
    NSDictionary *dicMarkerLast = arrMarker.lastObject;
    [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerLast floatForKey:@"latitude"], [dicMarkerLast floatForKey:@"longitude"]) Type:EndPossitionMarker UserInfo:dicMarkerLast];
    
    if (isStartPos)
    {
        NSDictionary *dicMarkerFirst = arrMarker.firstObject;
        [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicMarkerFirst floatForKey:@"latitude"], [dicMarkerFirst floatForKey:@"longitude"]) Type:StartPossitionMarker UserInfo:dicMarkerFirst];
    }
    
    [mapView setPathCenterInMapView:arrMarker edgeInset:UIEdgeInsetsMake(100, 50, 50, 50)];
}

-(void)addMarkerOnCapture:(NSArray *)arrCapture
{
    for (int i = 0; arrCapture.count > i; i++)
    {
        NSDictionary *dicCapture = arrCapture[i];
        [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake([dicCapture floatForKey:@"latitude"], [dicCapture floatForKey:@"longitude"]) Type:PhotoVideoMarker UserInfo:dicCapture];
    }
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clRunnerPositionl])
        return arrRunnerPosition.count;
    else if ([collectionView isEqual:clCategory])
        return 3;
    else if ([collectionView isEqual:clRunners])
        return arrUserList.count;
    
    return arryMyData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clRunnerPositionl])
    {
        NSString *strIdentifier = @"RunnerPositionCell";
        RunnerPositionCell *cell = [clRunnerPositionl dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        NSDictionary *dicData = arrRunnerPosition[indexPath.item];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        
        if ([dicData objectForKey:CUserPositionEmptyCellType])
        {
            //
            cell.contentView.hidden = YES;
        }
        else
        {
            cell.contentView.hidden = NO;
            NSDictionary *dicComp = [dicData valueForKey:@"completed"];
            
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"140" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            
            cell.lblPosition.attributedText = [self setAttributedString:[dicComp stringValueForJSON:@"rank"]];
            
            cell.transform = [[dicComp numberForJson:@"rank"] isEqual:@1] ? TRANSFORM_RUNNERPOSITION_UP_CELL_VALUE : TRANSFORM_RUNNERPOSITION_DOWN_CELL_VALUE;
            
            if (isRunTypeSolo)
            {
                switch ([dicComp numberForJson:@"rank"].intValue)
                {
                    case 1:
                        cell.viewBorder.layer.borderColor = CRGB(249, 206, 0).CGColor;
                        break;
                    case 2:
                        cell.viewBorder.layer.borderColor = CRGB(190, 207, 210).CGColor;
                        break;
                    case 3:
                        cell.viewBorder.layer.borderColor = CRGB(244, 133, 40).CGColor;
                        break;
                    default:
                        break;
                }
            }
            else
                cell.viewBorder.layer.borderColor = isTeamWinRed ? CRGB(226, 86, 97).CGColor : CRGB(61, 205, 252).CGColor;
        }
        
        return cell;
    }
    else if ([collectionView isEqual:clCategory])
    {
        NSString *strIdentifier = @"ResultRunCategoryCell";
        ResultRunCategoryCell *cell = [clCategory dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        if ([indexPath isEqual:selectedCategoryIndexpath])
        {
            cell.viewSelectedCat.hidden = NO;
            cell.btnCategory.selected = YES;
        }
        else
        {
            cell.viewSelectedCat.hidden = YES;
            cell.btnCategory.selected = NO;
        }
        
        switch (indexPath.item) {
            case 0:
                [cell.btnCategory setTitle:@"PACE" forState:UIControlStateNormal];
                break;
            case 1:
                [cell.btnCategory setTitle:@"TIME" forState:UIControlStateNormal];
                break;
            case 2:
                [cell.btnCategory setTitle:@"POSITION" forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
        return cell;

    }
    else if ([collectionView isEqual:clRunners])
    {
        NSString *strIdentifier = @"ResultRunnerCell";
        ResultRunnerCell *cell = [clRunners dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"60" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        switch (indexPath.item) {
            case 0:
                cell.viewUserColor.backgroundColor = CGraphColor1;
                break;
            case 1:
                cell.viewUserColor.backgroundColor = CGraphColor2;
                break;
            case 2:
                cell.viewUserColor.backgroundColor = CGraphColor3;
                break;
            case 3:
                cell.viewUserColor.backgroundColor = CGraphColor4;
                break;
            case 4:
                cell.viewUserColor.backgroundColor = CGraphColor5;
                break;
            case 5:
                cell.viewUserColor.backgroundColor = CGraphColor6;
                break;
            case 6:
                cell.viewUserColor.backgroundColor = CGraphColor7;
                break;
            case 7:
                cell.viewUserColor.backgroundColor = CGraphColor8;
                break;
            case 8:
                cell.viewUserColor.backgroundColor = CGraphColor9;
                break;
            case 9:
                cell.viewUserColor.backgroundColor = CGraphColor10;
                break;
            case 10:
                cell.viewUserColor.backgroundColor = CGraphColor11;
                break;
            case 11:
                cell.viewUserColor.backgroundColor = CGraphColor12;
                break;
            case 12:
                cell.viewUserColor.backgroundColor = CGraphColor13;
                break;
            case 13:
                cell.viewUserColor.backgroundColor = CGraphColor14;
                break;
            case 14:
                cell.viewUserColor.backgroundColor = CGraphColor15;
                break;
            case 15:
                cell.viewUserColor.backgroundColor = CGraphColor16;
                break;
            case 16:
                cell.viewUserColor.backgroundColor = CGraphColor17;
                break;
            case 17:
                cell.viewUserColor.backgroundColor = CGraphColor18;
                break;
            case 18:
                cell.viewUserColor.backgroundColor = CGraphColor19;
                break;
            case 19:
                cell.viewUserColor.backgroundColor = CGraphColor20;
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        if (arryMyData.count == 4)
        {
            NSString *strIdentifier = @"RunSummaryResultCell";
            RunSummaryResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCurrentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCurrentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCurrentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCurrentValue valueForKey:@"unit"];
            
            cell.imgSide.image = [UIImage imageNamed:[dicCurrentValue valueForKey:@"image"]];
            
            return cell;
        }
        else
        {
            NSString *strIdentifier = @"MyActivityResultCollectionViewCell";
            MyActivityResultCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
            
            NSDictionary *dicCurrentValue = arryMyData[indexPath.row];
            cell.lblContent.text = [dicCurrentValue valueForKey:@"result"];
            cell.lblSubContent.text = [dicCurrentValue valueForKey:@"quantities"];
            cell.lblUnit.text = [dicCurrentValue valueForKey:@"unit"];
            
            cell.imgTop.image = [UIImage imageNamed:[dicCurrentValue valueForKey:@"image"]];
            
            return cell;
        }
    }
        
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clRunnerPositionl])
        return CGSizeMake(CScreenWidth/arrRunnerPosition.count , CGRectGetHeight(collectionView.frame));
    else if ([collectionView isEqual:clCategory])
        return CGSizeMake(80 , CGRectGetHeight(clCategory.frame));
    else if ([collectionView isEqual:clRunners])
        return CGSizeMake(50 , CGRectGetHeight(clRunners.frame));
    else
    {
        if (arryMyData.count == 4)
            return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
        else
        {
            if (indexPath.row < 2)
                return CGSizeMake(CScreenWidth/2 , CGRectGetHeight(collectionView.frame)/2);
            
            return CGSizeMake(CScreenWidth/3-.01 , CGRectGetHeight(collectionView.frame)/2);
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clCategory])
    {
        if ([selectedCategoryIndexpath isEqual:indexPath])
            return;
        
        if (selectedCategoryIndexpath)
        {
            ResultRunCategoryCell *cell = (ResultRunCategoryCell *)[clCategory cellForItemAtIndexPath:selectedCategoryIndexpath];
            cell.btnCategory.selected = NO;
            cell.viewSelectedCat.hidden = YES;
            selectedCategoryIndexpath = nil;
        }
        
        ResultRunCategoryCell *cell = (ResultRunCategoryCell *)[clCategory cellForItemAtIndexPath:indexPath];
        cell.btnCategory.selected = YES;
        cell.viewSelectedCat.hidden = NO;
        selectedCategoryIndexpath = indexPath;
        
        [self drawGraph];
    }
    
}



#pragma mark - UITableView Delegate and Datasource
#pragma mark

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrUserList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];
        NSDictionary *dicComp = [dicData valueForKey:@"completed"];
        
        if (isRunTypeSolo)
        {
            NSString *strIdentifier = @"RunScoreBoardCell";
            RunScoreBoardCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
            
            //--------- For Solo
            cell.imgNext.hidden = NO;
            cell.viewRedcircle.hidden = YES;
            cell.viewHorizontalLine.hidden = YES;
            cell.imgNext.hidden = NO;
            
            cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
            cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
            
            [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];
            
            cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
            cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
            cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
            cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
            
            return cell;
        }
        else
        {
            if ([[dicData stringValueForJSON:CTeamType] isEqualToString:CTeamRed])
            {
                NSString *strIdentifier = @"RunScoreBoardCell";
                RunScoreBoardCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
                
                cell.imgNext.hidden = YES;
                cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
                cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
                [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];
                
                cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
                cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
                cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
                cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
                return cell;
            }
            else
            {
                NSString *strIdentifier = @"RunScoreBoardBlueTeamCell";
                RunScoreBoardBlueTeamCell *cell = [tblScore dequeueReusableCellWithIdentifier:strIdentifier];
                
                cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
                cell.lblName.text = [dicData stringValueForJSON:@"user_name"];
                [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                cell.imgTriangle.image = [appDelegate GetImageWithUserPerformance:[dicData stringValueForJSON:@"performance"]];
                
                cell.lblPosition.text = [dicComp stringValueForJSON:@"rank"];
                cell.lblTime.text = [dicComp stringValueForJSON:@"total_time"];
                cell.lblSpeed.text = [NSString stringWithFormat:@"%@/km",[dicComp stringValueForJSON:@"average_speed"]];
                cell.lblCal.text = [NSString stringWithFormat:@"%.2f cal",[dicComp stringValueForJSON:@"total_calories"].floatValue];
                
                return cell;
            }
        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    NSDictionary *dicData = [arrUserList objectAtIndex:indexPath.row];
    
    if ([[dicData stringValueForJSON:@"user_id"] isEqualToString:self.strSelectedUserId])
        return;
    
    UserRunSummaryViewController *objOther = [[UserRunSummaryViewController alloc] initWithNibName:@"UserRunSummaryViewController" bundle:nil];
    objOther.arrRunners = arrUserList;
    objOther.strSelectedUserId = [dicData stringValueForJSON:@"user_id"];
    objOther.dicSession = dicSessionDetail;
    [self.navigationController pushViewController:objOther animated:true];
    
    
}



#pragma mark - Result Related API Functions

-(void)getResultFromServer:(BOOL)shouldShowLoader
{
    if (!self.strResultSessionId)
        return;
    
    if (shouldShowLoader)
        [GiFHUD showWithOverlay];
    
    [[APIRequest request] runningResultOfSession:self.strResultSessionId completed:^(id responseObject, NSError *error)
     {
         [GiFHUD dismiss];
         if (responseObject && !error)
         {
             // Do your stuff here
             
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             dicSessionDetail = dicData;
             
             if ([[dicData stringValueForJSON:@"run_type"] isEqualToString:@"solo"])
             {
                 isRunTypeSolo = YES;
                 [self setupForSoloSession:dicData];
             }
             else
             {
                 isRunTypeSolo = NO;
                 [self setupForTeamSession:dicData];
             }
         }
     }];
}


#pragma mark - Team Session
-(void)setupForTeamSession:(NSDictionary *)dicResult
{
    [arrWinRunner removeAllObjects];
    
    NSDictionary *dicInvition = [dicResult valueForKey:@"invitation"];
    
    isRunTypeSolo = NO;
    
    lblWinner.hidden = lblWinnerTeamName.hidden = NO;
    lblSoloScoreboard.hidden = YES;
    
    [viewTeamTitle hideByHeight:NO];
    [lblScoreboard hideByHeight:NO];
    
    [arrUserList removeAllObjects];
    NSMutableArray *arrTempUsers = [[NSMutableArray alloc] initWithArray:[dicInvition valueForKey:@"team_red"]];
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamRed forKey:CTeamType];
        [arrUserList addObject:dicData];
    }
    
    // store Win Red team player for position.....
    if ([[dicResult stringValueForJSON:@"win_team"] isEqualToString:@"team_red"])
    {
        lblWinnerTeamName.text = @"TEAM RED";
        lblWinnerTeamName.textColor = CRGB(226, 86, 97);
        isTeamWinRed = YES;
        NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
        [arrWinRunner addObjectsFromArray:sortedArray];
    }
    
    [arrTempUsers removeAllObjects];
    [arrTempUsers addObjectsFromArray:[dicInvition valueForKey:@"team_blue"]];
    
    NSMutableArray *arrWinPlayerTemp = [NSMutableArray new];
    
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamBlue forKey:CTeamType];
        [arrUserList addObject:dicData];
        [arrWinPlayerTemp addObject:dicData];
    }
    
    // store Win Blue team player for position.....
    if ([[dicResult stringValueForJSON:@"win_team"] isEqualToString:@"team_blue"])
    {
        lblWinnerTeamName.text = @"TEAM BLUE";
        lblWinnerTeamName.textColor = CRGB(61, 205, 252);
        isTeamWinRed = NO;
        [arrWinRunner removeAllObjects];
        NSArray *sortedArray = [arrWinPlayerTemp sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
        [arrWinRunner addObjectsFromArray:sortedArray];
    }
    
    NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
    
    [arrUserList removeAllObjects];
    [arrUserList addObjectsFromArray:sortedArray];
    
    [tblScore reloadData];
    cnTblHeight.constant = tblScore.contentSize.height;
 
    [self getRunnerForTopPosition];
 
}

#pragma mark - Solo Session
-(void)setupForSoloSession:(NSDictionary *)dicResult
{
    NSDictionary *dicInvition = [dicResult valueForKey:@"invitation"];
    
    isRunTypeSolo = YES;
    
    lblWinner.hidden = lblWinnerTeamName.hidden = YES;
    lblSoloScoreboard.hidden = NO;
    
    [viewTeamTitle hideByHeight:YES];
    [lblScoreboard hideByHeight:YES];
    
    [arrUserList removeAllObjects];
    NSMutableArray *arrTempUsers = [[NSMutableArray alloc] initWithArray:[dicInvition valueForKey:@"solo"]];
    for (int i = 0; arrTempUsers.count > i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrTempUsers[i]];
        NSDictionary *dicCom = [dicData valueForKey:@"completed"];
        [dicData setObject:[dicCom stringValueForJSON:@"rank"] forKey:CUserPosition];
        [dicData setObject:CTeamSolo forKey:CTeamType];
        [arrUserList addObject:dicData];
    }
    
    NSArray *sortedArray = [arrUserList sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:CUserPosition ascending:YES]]];
    
    [arrWinRunner removeAllObjects];
    [arrWinRunner addObjectsFromArray:sortedArray];
    
    [arrUserList removeAllObjects];
    [arrUserList addObjectsFromArray:sortedArray];
    
    [tblScore reloadData];
    cnTblHeight.constant = tblScore.contentSize.height;
    
    [self getRunnerForTopPosition];
}


-(void)getRunnerForTopPosition
{
    [arrRunnerPosition removeAllObjects];
    
    if (arrWinRunner.count < 2)
    {
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
    }
    else if (arrWinRunner.count < 3)
    {
        [arrRunnerPosition addObject:@{CUserPositionEmptyCellType : CUserPositionEmptyCell}];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:arrWinRunner[1]];
    }
    else
    {
        [arrRunnerPosition addObject:arrWinRunner[2]];
        [arrRunnerPosition addObject:arrWinRunner[0]];
        [arrRunnerPosition addObject:arrWinRunner[1]];
    }
    
    [clRunnerPositionl reloadData];
    [clRunners reloadData];
    
    [self drawGraph];
}

-(NSAttributedString *)setAttributedString:(NSString *)position
{
    NSString *strRank;
    
    if ([position isEqualToString:@"1"])
        strRank = @"ST";
    else if ([position isEqualToString:@"2"])
        strRank = @"ND";
    else if ([position isEqualToString:@"3"])
        strRank = @"RD";
    else
        strRank = @"TH";
    
    NSString *strFullText =[NSString stringWithFormat:@"%@%@",position,strRank];
    
    NSMutableAttributedString *attributeText = [[NSMutableAttributedString alloc]initWithString:strFullText];
    
    [attributeText addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, position.length)];
    [attributeText addAttribute: NSFontAttributeName value:CFontSolidoCondensedBold(22) range: NSMakeRange(0,position.length)];
    
    [attributeText addAttribute:NSForegroundColorAttributeName value:CRGB(122, 126, 145) range:NSMakeRange(position.length, strRank.length)];
    [attributeText addAttribute: NSFontAttributeName value:CFontSolidoCondensedBold(15) range: NSMakeRange(position.length, strRank.length)];
    
    return attributeText;
}


#pragma mark - Graph Related Functions
-(void)removeLayerFromGraphView
{
    [viewGraph.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    NSLog(@"%@",viewGraph.layer.sublayers);
}


-(void)setGraphStartAndEndValue
{
    NSString *strDistnace = [dicSessionDetail stringValueForJSON:@"distance"];
    
    switch (selectedCategoryIndexpath.item) {
        case 0: // Pace Value
        case 2:// Possition Value
        {
            if (strDistnace.intValue >= 1000)
            {
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%d",strDistnace.intValue/1000];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"km";
            }
            else
            {
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%d",strDistnace.intValue];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"m";
            }
        }
            break;
        case 1:
        {
            // Time Value
            float sessionMaxCompleteTime = ([dicSessionDetail stringValueForJSON:@"distance"].floatValue/1000) * 7.5;
            
            if (strDistnace.intValue <= 200)
            {
                // Show in Second
                sessionMaxCompleteTime = sessionMaxCompleteTime*60;
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%.1f",sessionMaxCompleteTime];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"Sec";
            }
            else
            {
                // Show in mint
                lblGraphStartValue.text = @"0";
                lblGraphEndValue.text = [NSString stringWithFormat:@"%.1f",sessionMaxCompleteTime];
                lblGraphStartUpValue.text = lblGraphEndUpValue.text = @"Min";
            }
        }
            break;
            
        default:
            break;
    }
}


// Draw Graph with pace and Distance
-(NSArray *)getGraphPointsForPaceAndDistance:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSString *strDistnace = [dicSessionDetail stringValueForJSON:@"distance"];
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    
    int distanceRatio = 0;
    if (strDistnace.intValue >= 1000)
        distanceRatio = 1000;
    else
        distanceRatio = 20;
    
    float distanceSlot = (CScreenWidth-CGraphStartMinValue)/(strDistnace.floatValue/distanceRatio);
    float xPoint = CGraphStartMinValue;
    float yPoint = 0;
    
    NSArray *arrRunning = [dicUser valueForKey:@"running"];
    
    for (int i = 0; arrRunning.count > i; i++)
    {
        NSDictionary *dicRunData = arrRunning[i];
        
        NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
        int pace = stRPaceSpeed.intValue;
        
        float coverDistance = [dicRunData stringValueForJSON:@"distance"].floatValue/distanceRatio;
        xPoint = coverDistance * distanceSlot;
        
        if (xPoint < CGraphStartMinValue)
            xPoint = CGraphStartMinValue;
        
        if (xPoint > CScreenWidth)
            xPoint = CScreenWidth;
        
        if (pace < 5 && pace > 0)
            pace = 5;
        
        pace  = ViewGraphHeight - pace;
        
        if (pace < 0 || pace >= ViewGraphHeight)
            pace = 5;
        
        yPoint = pace;
        [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
    }
    
    arrGraphPoins = arrPoints.mutableCopy;
    
    return arrGraphPoins;
}

// Draw Graph with pace and Time
-(NSArray *)getGraphPointsForPaceAndTime:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    
    // Time Value
    float sessionMaxCompleteTime = ([dicSessionDetail stringValueForJSON:@"distance"].floatValue/1000) * 7.5;
    NSString *strDistnace = [dicSessionDetail stringValueForJSON:@"distance"];
    
    if (strDistnace.intValue <= 200)
    {
        // Show in Second
        sessionMaxCompleteTime = sessionMaxCompleteTime*60;
        float timeSlot = (CScreenWidth-CGraphStartMinValue)/(sessionMaxCompleteTime);
        
        float xPoint = CGraphStartMinValue;
        float yPoint = 0;
        
        NSArray *arrRunning = [dicUser valueForKey:@"running"];
        for (int i = 0; arrRunning.count > i; i++)
        {
            NSDictionary *dicRunData = arrRunning[i];
            NSArray *arrTimeSlot = [[dicRunData stringValueForJSON:@"time"] componentsSeparatedByString:@":"];
            
            float totalSecond = 0;
            if (arrTimeSlot.count > 2)
            {
                
                NSString *strMint = arrTimeSlot[1];
                NSString *strSecond = arrTimeSlot[2];
                
                float mint = strMint.floatValue*60;
                totalSecond = mint+strSecond.floatValue;
            }
            
            xPoint = timeSlot*totalSecond;
            
            NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
            int pace = stRPaceSpeed.intValue;
            
            if (xPoint < CGraphStartMinValue)
                xPoint = CGraphStartMinValue;
            
            if (xPoint > CScreenWidth)
                xPoint = CScreenWidth;
            
            if (pace < 5 && pace > 0)
                pace = 5;
            
            pace  = ViewGraphHeight - pace;
            
            if (pace < 0 || pace >= ViewGraphHeight)
                pace = 5;
            
            yPoint = pace;
            [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
        }
    }
    else
    {
        // Show in mint
        
        float timeSlot = (CScreenWidth-CGraphStartMinValue)/(sessionMaxCompleteTime);
        
        float xPoint = CGraphStartMinValue;
        float yPoint = 0;
        
        NSArray *arrRunning = [dicUser valueForKey:@"running"];
        for (int i = 0; arrRunning.count > i; i++)
        {
            NSDictionary *dicRunData = arrRunning[i];
            NSArray *arrTimeSlot = [[dicRunData stringValueForJSON:@"time"] componentsSeparatedByString:@":"];
            
            float totalMint = 0;
            if (arrTimeSlot.count > 2)
            {
                NSString *strHour = arrTimeSlot[0];
                NSString *strMint = arrTimeSlot[1];
                
                float hour = strHour.floatValue*60;
                totalMint = hour+strMint.floatValue;
            }
            
            xPoint = timeSlot*totalMint;
            
            NSString *stRPaceSpeed = [dicRunData stringValueForJSON:@"average_speed"];
            int pace = stRPaceSpeed.intValue;
            
            if (xPoint < CGraphStartMinValue)
                xPoint = CGraphStartMinValue;
            
            if (xPoint > CScreenWidth)
                xPoint = CScreenWidth;
            
            if (pace < 5 && pace > 0)
                pace = 5;
            
            pace  = ViewGraphHeight - pace;
            
            if (pace < 0 || pace >= ViewGraphHeight)
                pace = 5;
            
            yPoint = pace;
            [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
        }
    }
    
    arrGraphPoins = arrPoints.mutableCopy;
    
    return arrGraphPoins;
}


// Draw Graph with position and Distance
-(NSArray *)getGraphPointsForPositionAndDistance:(NSDictionary *)dicUser
{
    NSArray *arrGraphPoins;
    
    NSString *strDistnace = [dicSessionDetail stringValueForJSON:@"distance"];
    
    NSMutableArray *arrPoints  = [NSMutableArray new];
    [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    
    int distanceRatio = 0;
    if (strDistnace.intValue >= 1000)
        distanceRatio = 1000;
    else
        distanceRatio = 20;
    
    float xPoint = CGraphStartMinValue;
    float yPoint = 0;
    
    NSArray *arrRunning = [dicUser valueForKey:@"running"];
    
    
    float heightSlot = ViewGraphHeight/(arrUserList.count);
    float distanceSlot = (CScreenWidth-CGraphStartMinValue)/(strDistnace.floatValue/distanceRatio);
    
    for (int i = 0; arrRunning.count > i; i++)
    {
        NSDictionary *dicRunData = arrRunning[i];
        float coverDistance = [dicRunData stringValueForJSON:@"distance"].floatValue/distanceRatio;
        xPoint = coverDistance * distanceSlot;
        
        if (xPoint < CGraphStartMinValue)
            xPoint = CGraphStartMinValue;
        
        if (xPoint > CScreenWidth)
            xPoint = CScreenWidth;
        
        int isUserPossition = [dicRunData stringValueForJSON:@"position"].intValue;
        
        // If any user disqualify from race..
        if (isUserPossition > arrUserList.count)
            isUserPossition = (int)arrUserList.count;
        
        float graphPosition = 0;
        
        if (isUserPossition > 0)
        {
            graphPosition = (float)arrUserList.count / (float)isUserPossition;
        }
        
        
        yPoint = heightSlot *graphPosition;
        
        //        yPoint = heightSlot *isUserPossition;
        [arrPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xPoint, yPoint)]];
    }
    
    arrGraphPoins = arrPoints.mutableCopy;
    return arrGraphPoins;
}


-(void)drawGraph
{
    [self removeLayerFromGraphView]; // To remove all layer from View
    [self setGraphStartAndEndValue];  // To set Graph Start and End point
    
    for (int i = 0; arrUserList.count > i; i++)
    {
        BOOL isVisibleUser = NO;
        
        NSDictionary *dicUser = arrUserList[i];
        
        NSArray *arrPoints;
        switch (selectedCategoryIndexpath.item) {
            case 0: // Pace Value
            {
                arrPoints = [self getGraphPointsForPaceAndDistance:dicUser];
            }
                break;
            case 1:
            {
                // Time Value
                arrPoints = [self getGraphPointsForPaceAndTime:dicUser];
            }
                break;
            case 2:// Possition Value
            {
                arrPoints = [self getGraphPointsForPositionAndDistance:dicUser];
            }
                break;
                
            default:
                break;
        }
        
        if ([[dicUser stringValueForJSON:@"user_id"] isEqualToString:self.strSelectedUserId])
            isVisibleUser = YES;
        
        UIColor *strokColor;
        switch (i) {
            case 0:
                strokColor = CGraphColor1;
                break;
            case 1:
                strokColor = CGraphColor2;
                break;
            case 2:
                strokColor = CGraphColor3;
                break;
            case 3:
                strokColor = CGraphColor4;
                break;
            case 4:
                strokColor = CGraphColor5;
                break;
            case 5:
                strokColor = CGraphColor6;
                break;
            case 6:
                strokColor = CGraphColor7;
                break;
            case 7:
                strokColor = CGraphColor8;
                break;
            case 8:
                strokColor = CGraphColor9;
                break;
            case 9:
                strokColor = CGraphColor10;
                break;
            case 10:
                strokColor = CGraphColor11;
                break;
            case 11:
                strokColor = CGraphColor12;
                break;
            case 12:
                strokColor = CGraphColor13;
                break;
            case 13:
                strokColor = CGraphColor14;
                break;
            case 14:
                strokColor = CGraphColor15;
                break;
            case 15:
                strokColor = CGraphColor16;
                break;
            case 16:
                strokColor = CGraphColor17;
                break;
            case 17:
                strokColor = CGraphColor18;
                break;
            case 18:
                strokColor = CGraphColor19;
                break;
            case 19:
                strokColor = CGraphColor20;
                break;
            default:
                break;
        }
        
        if (arrPoints.count > 0)
            [self addBezierPathBetweenPoints:arrPoints toView:viewGraph withColor:strokColor isVisibleUser:isVisibleUser];
        
    }
}

- (void)addBezierPathBetweenPoints:(NSArray *)points toView:(UIView *)view withColor:(UIColor *)storkeColor isVisibleUser:(BOOL)isVisibleUser
{
    
    NSUInteger strokeWidth = 3;
    float granularity = 100;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path fill];
    [path stroke];
    
    [path moveToPoint:[[points firstObject] CGPointValue]];
    CGPoint lastPoint = CGPointMake(0, 0);
    
    
    for (int index = 1; index < (int)points.count - 2 ; index++)
    {
        CGPoint point0 = [[points objectAtIndex:index - 1] CGPointValue];
        CGPoint point1 = [[points objectAtIndex:index] CGPointValue];
        CGPoint point2 = [[points objectAtIndex:index + 1] CGPointValue];
        CGPoint point3 = [[points objectAtIndex:index + 2] CGPointValue];
        
        for (int i = 1; i < granularity ; i++)
        {
            float t = (float) i * (1.0f / (float) granularity);
            float tt = t * t;
            float ttt = tt * t;
            
            CGPoint pi;
            pi.x = 0.5 * (2*point1.x+(point2.x-point0.x)*t + (2*point0.x-5*point1.x+4*point2.x-point3.x)*tt + (3*point1.x-point0.x-3*point2.x+point3.x)*ttt);
            pi.y = 0.5 * (2*point1.y+(point2.y-point0.y)*t + (2*point0.y-5*point1.y+4*point2.y-point3.y)*tt + (3*point1.y-point0.y-3*point2.y+point3.y)*ttt);
            
            if (pi.y > view.frame.size.height) {
                pi.y = view.frame.size.height;
            }
            else if (pi.y < 0){
                pi.y = 0;
            }
            
            if (pi.x > point0.x)
            {
                [path addLineToPoint:pi];
                lastPoint = point2;
            }
        }
        
        [path addLineToPoint:point2];
    }
    
    [path addLineToPoint:[[points objectAtIndex:[points count] - 1] CGPointValue]];
    
    
    CAShapeLayer *_graphLayer = [CAShapeLayer layer];
    [_graphLayer setFrame:[view bounds]];
    [_graphLayer setGeometryFlipped:YES];
    [_graphLayer setStrokeColor:[storkeColor CGColor]];
    [_graphLayer setFillColor:nil];
    [_graphLayer setLineWidth:strokeWidth];
    [_graphLayer setLineJoin:kCALineCapRound];
    [[view layer] addSublayer:_graphLayer];
    [_graphLayer setPath:path.CGPath];
    
    
    CAShapeLayer *_maskLayer = [CAShapeLayer layer];
    [_maskLayer setFrame:[view bounds]];
    [_maskLayer setGeometryFlipped:YES];
    [_maskLayer setStrokeColor:[[UIColor clearColor] CGColor]];
    [_maskLayer setFillColor:[[UIColor blackColor] CGColor]];
    [_maskLayer setLineWidth:strokeWidth];
    [_maskLayer setLineJoin:kCALineCapRound];
    
    if (isVisibleUser)
    {
        // Add gradient
        CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
        CGColorRef color1 = storkeColor.CGColor;
        CGColorRef color2 = storkeColor.CGColor;
        _gradientLayer.opacity = 0.5;
        [_gradientLayer setColors:@[(__bridge id)color1,(__bridge id)color2]];
        [_gradientLayer setLocations:@[@0.0,@0.9]];
        [_gradientLayer setFrame:[viewGraph bounds]];
        [[viewGraph layer] addSublayer:_gradientLayer];
        
        UIBezierPath *copyPath = [UIBezierPath bezierPathWithCGPath:path.CGPath];
        CGPoint gradinetLastPoint = [[points lastObject] CGPointValue];
        [copyPath addLineToPoint:CGPointMake(gradinetLastPoint.x, 0)];
        
        [_maskLayer setPath:copyPath.CGPath];
        [_gradientLayer setMask:_maskLayer];
    }
}



#pragma mark - Action Events
#pragma mark

-(IBAction)btnBackCLK:(id)sender
{
    [mapView clearMGLMapView];
    [mapView removeFromSuperview] ;
    mapView = nil ;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
