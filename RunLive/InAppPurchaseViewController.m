//
//  InAppPurchaseViewController.m
//  RunLive
//
//  Created by mac-0005 on 13/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "InAppPurchaseViewController.h"

@interface InAppPurchaseViewController ()

@end

@implementation InAppPurchaseViewController
{
    int swipeCount;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self Initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    [appDelegate hideTabBar];
    
//    [appDelegate checkInAppPurchaseStatus:YES];
    
    swipeCount = 1;
    
    UISwipeGestureRecognizer *swipeGestureRightSide = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSideSwipe:)];
    [swipeGestureRightSide setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [viewContentContainer addGestureRecognizer:swipeGestureRightSide];
    
    UISwipeGestureRecognizer *swipeGestureLeftSide = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSideSwipe:)];
    [swipeGestureLeftSide setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [viewContentContainer addGestureRecognizer:swipeGestureLeftSide];

    pageControler.numberOfPages = 2;
}


#pragma mark - Gesture Event
-(void)rightSideSwipe:(UISwipeGestureRecognizer *)recognizer
{
    if (swipeCount == 1)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [viewContentContainer.layer addAnimation:transition forKey:nil];
   
    [self setInformativeText:YES];
    
    swipeCount--;
    pageControler.currentPage = swipeCount-1;
    
}

-(void)leftSideSwipe:(UISwipeGestureRecognizer *)recognizer
{
    
    if (swipeCount == 2)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [viewContentContainer.layer addAnimation:transition forKey:nil];
    
    [self setInformativeText:NO];
    
    swipeCount++;
    pageControler.currentPage = swipeCount-1;
}


#pragma mark - Initialize
-(void)Initialize
{
    
    [lblTitle setFont:Is_iPhone_5?CFontSolidoCondensedBold(34.0):CFontSolidoCondensedBold(41.0)];
    lblTitle.textColor = lblDetails.textColor = CRGB(255, 255, 255);
    [lblDetails setFont:Is_iPhone_5?CFontGilroyRegular(13.0):CFontGilroyRegular(16.0)];

    [self setInformativeText:YES];
    
    if ([appDelegate.loginUser.subscription_trial isEqual:@0])
    {
        viewTrialContainer.hidden = NO;
        viewPriceContainer.hidden = YES;
    }
    else
    {
        viewTrialContainer.hidden = YES;
        viewPriceContainer.hidden = NO;
    }
    
    viewYearly.layer.cornerRadius = viewMonthly.layer.cornerRadius = 2;
    viewNotNow.layer.cornerRadius = viewStartTrial.layer.cornerRadius = 2;
}

-(NSAttributedString *)setTextWithCustomLineStyle:(NSString *)text maxheight:(CGFloat)maxheight linespacing:(CGFloat)linespacing
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:linespacing];
    paragraphStyle.maximumLineHeight = maxheight;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    return attributedString ;
}

-(void)setInformativeText:(BOOL)isVoice
{
    if (isVoice)
    {
        NSString *strBeta = @"(Beta)";
        NSString *strVoiceText = @"In-Run Voice Chat ";
        lblTitle.attributedText = [self setTextWithCustomLineStyle:[NSString stringWithFormat:@"%@%@",strVoiceText,strBeta] maxheight:40 linespacing:0.3];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: lblTitle.attributedText];
        [text addAttribute: NSFontAttributeName value:CFontSolidoCondensedBold(lblTitle.font.pointSize - 20) range: NSMakeRange(strVoiceText.length,strBeta.length)];
        lblTitle.attributedText = text;
        
        lblDetails.attributedText = [self setTextWithCustomLineStyle:@"Banter & Brag! RunLive is excited to be the first running platform that lets you chat it up with other runners instantly in Real Time with just the push of a button. Winning couldn't be more fun!\n\nYour subscription will start automatically at the end of your 1 month trial and you will be billed $4.99 every month after. Cancel at any time." maxheight:30 linespacing:3];
    }
    else
    {
        lblTitle.attributedText = [self setTextWithCustomLineStyle:@"Custom Sessions" maxheight:40 linespacing:0.3];
        lblDetails.attributedText = [self setTextWithCustomLineStyle:@"Create private, invite-only running sessions and compete against your friends from all over the world in real-time! Try it now and start your own anytime, anywhere virtual running club.\n\nYour subscription will start automatically at the end of your 1 month trial and you will be billed $4.99 every month after. Cancel at any time." maxheight:30 linespacing:3];
    }
    
}

#pragma mark - In App Purchase 
-(void)subscribePlan
{
 //   [self restoreInApp];
    
    // Call api to check reciept id
    
    if ([CUserDefaults objectForKey:CIAPAppleRecieptID] && [[CUserDefaults objectForKey:CIAPAppleRecieptID] isBlankValidationPassed])
    {
        [[APIRequest request] checkRecieptID:[CUserDefaults objectForKey:CIAPAppleRecieptID] completed:^(id responseObject, NSError *error)
         {
             
             NSLog(@"Apple = %@",[CUserDefaults objectForKey:CIAPAppleRecieptID]);
             NSLog(@"Local = %@",appDelegate.loginUser.receipt_id);
             
             if (responseObject && !error)
             {
                 NSDictionary *dicData = [responseObject valueForKey:CJsonData];
                 if ([[dicData numberForJson:@"receipt_status"] isEqual:@1])
                 {
                     [self preccessForIAP];
                 }
                 else
                 {
                     [self customAlertViewWithOneButton:@"" Message:@"Please use same apple account to purchase plan." ButtonText:@"OK" Animation:YES completed:nil];
                 }
             }
         }];
    }
    else
    {
        if (![appDelegate.loginUser.receipt_id isBlankValidationPassed] || [appDelegate.loginUser.receipt_id isEqualToString:@"0"])
        {
            // If user not purchased any plan yet.
            [self preccessForIAP];
        }
        else
        {
            // If user purchased plan
            [self customAlertViewWithOneButton:@"" Message:@"Please use same apple account to purchase plan." ButtonText:@"OK" Animation:YES completed:nil];
        }
    }
}

-(void)preccessForIAP
{
    if (btnYearly.selected)
        [self purchaseInApp:CIAPYearlyProduct];
    else
        [self purchaseInApp:CIAPMonthlyProduct];
    
    appDelegate.configurePlanPurchaseSuccessfully = ^(SKPaymentTransaction *transctions,NSError *error)
    {
        NSLog(@"transactionIdentifier ====== >> %@",transctions.transactionIdentifier);
        NSLog(@"Original Transaction ==== >> %@",transctions.originalTransaction);
        NSLog(@"Original transactionIdentifier ==== >> %@",transctions.originalTransaction.transactionIdentifier);
        NSLog(@"downloads ==== >> %@",transctions.downloads);
        NSLog(@"transactionDate ==== >> %@",transctions.transactionDate);
        //NSLog(@"transactionReceipt ==== >> %@",transctions.transactionReceipt);
        NSLog(@"transactionState ==== >> %ld",(long)transctions.transactionState);
        NSLog(@"username ==== >> %@",transctions.payment.applicationUsername);
        
        if (!error)
        {
            [self activeSubscriptionPlan:NO ReciepId:transctions.originalTransaction.transactionIdentifier];
        }
        else
        {
            NSLog(@"Purchase process failed========>");
        }
    };
}

-(void)activeSubscriptionPlan:(BOOL)isFree ReciepId:(NSString *)receipt_id
{
    NSString *strType = nil;
    
    if (isFree)
        strType = @"0";
    else
    {
        if (btnYearly.selected)
            strType = @"2";
        else
            strType = @"1";
    }
        
    
    [[APIRequest request] activeSubcription:strType RecieptId:receipt_id completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            // show alert here...
            [self customAlertViewWithOneButton:@"" Message:[responseObject valueForKey:CJsonMessage] ButtonText:@"OK" Animation:YES completed:^{
                [self btnCancelCLK:nil];
            }];
        }
    }];

}

#pragma mark - Action Event

-(IBAction)btnCancelCLK:(id)sender
{
    if (self.configureInAppPurhaseClose)
        self.configureInAppPurhaseClose(YES);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnPlanCLK:(UIButton *)sender
{
    viewMonthly.backgroundColor = viewYearly.backgroundColor = [UIColor whiteColor];
    lblMonthly.textColor = lblMonthlyPrice.textColor = lblYearly.textColor = lblYearlyPrice.textColor = [UIColor blackColor];
    btnMonthly.selected = btnYearly.selected = NO;
    
    switch (sender.tag)
    {
        case 0:
        {
            btnMonthly.selected = YES;
            viewMonthly.backgroundColor = CRGB(71, 216, 253);
            lblMonthly.textColor = lblMonthlyPrice.textColor = [UIColor whiteColor];
        }
            break;
        case 1:
        {
            btnYearly.selected = YES;
            viewYearly.backgroundColor = CRGB(71, 216, 253);
            lblYearly.textColor = lblYearlyPrice.textColor = [UIColor whiteColor];
        }
            break;
            
        default:
            break;
    }
    
    [self subscribePlan];
}

-(IBAction)btnStartTrialCLK:(UIButton *)sender
{
    viewNotNow.backgroundColor = viewStartTrial.backgroundColor = [UIColor whiteColor];
    lblNotNow.textColor = lblStartTrail.textColor = [UIColor blackColor];
    btnNotNow.selected = btnStartTrial.selected =  NO;
    switch (sender.tag) {
        case 0:
        {
            btnNotNow.selected = YES;
            viewNotNow.backgroundColor = CRGB(71, 216, 253);
            lblNotNow.textColor = lblMonthlyPrice.textColor = [UIColor whiteColor];
            [self btnCancelCLK:nil];
        }
            break;
        case 1:
        {
            btnStartTrial.selected = YES;
            viewStartTrial.backgroundColor = CRGB(71, 216, 253);
            lblStartTrail.textColor = lblYearlyPrice.textColor = [UIColor whiteColor];
//            [self activeSubscriptionPlan:YES ReciepId:nil];
            
            btnMonthly.selected = YES;
            [self subscribePlan];
        }
            break;
            
        default:
            break;
    }
}


@end
