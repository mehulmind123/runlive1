//
//  BlockedUserViewController.h
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockedUserCell.h"

@interface BlockedUserViewController : SuperViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblUserList;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (IBAction)btnBackClicked:(id)sender;

@end
