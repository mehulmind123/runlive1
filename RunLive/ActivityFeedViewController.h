//
//  ActivityFeedViewController.h
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^stopPlayVideo)(BOOL isStop);

@interface ActivityFeedViewController : SuperViewController<UIActionSheetDelegate>
{
    IBOutlet UICollectionView *clActivityMain;
    IBOutlet NSLayoutConstraint *cnCLActivityBottomSpace;
    
}

@property(strong,nonatomic) stopPlayVideo configureStopPlayVideo;

@end
