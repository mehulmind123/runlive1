//
//  ChatImageCell.h
//  RunLive
//
//  Created by mac-0004 on 03/11/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImageCell : UITableViewCell

@property IBOutlet UIImageView *imgChat,*imgUser;
@property IBOutlet UILabel *lblTime;

@property(nonatomic,strong) IBOutlet NSLayoutConstraint *cnImgHeight,*cnImgWidth;
-(void)setImageHeightWidth:(NSString *)strImgWidth Height:(NSString *)strImgHeight;

@end
