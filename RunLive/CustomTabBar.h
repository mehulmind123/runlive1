//
//  CustomTabBar.h
//  DigitalAddress
//
//  Created by mac-0002 on 08/07/13.
//  Copyright (c) 2013 Mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LoginViewController.h"
//#import "ProfileViewController.h"

@interface CustomTabBar : UIView
{
    IBOutlet NSLayoutConstraint *cnViewMessageBadgeTralling,*cnViewNotificationBadgeTralling;
}
@property (weak, nonatomic) IBOutlet UIButton *btnTab1;
@property (weak, nonatomic) IBOutlet UIButton *btnTab2;
@property (weak, nonatomic) IBOutlet UIButton *btnTab3;
@property (weak, nonatomic) IBOutlet UIButton *btnTab4;
@property (weak, nonatomic) IBOutlet UIButton *btnTab5;
@property (weak, nonatomic) IBOutlet UIView *viewTabNotificationBadge,*viewNotificationBadge;
@property (weak, nonatomic) IBOutlet UIView *viewTabMessageBadge,*viewMessageBadge;


    
@property (weak, nonatomic) IBOutlet UIImageView *imgTab1,*imgTab2,*imgTab3,*imgTab4,*imgTab5;

-(IBAction)btnTabBarClicked:(UIButton *)btnTab;

+(id)CustomTabBar;

@property int selectedTab;
@end
