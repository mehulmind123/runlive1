//
//  ActivityDetailViewController.h
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsViewController.h"
@interface ActivityDetailViewController : SuperViewController<MGLMapViewDelegate,MIMGLMapViewDelegate>
{
    IBOutlet UICollectionView *clmyActvityValue;
    IBOutlet UILabel *lblCity,*lblSubLocality,*lblDate,*lblTitle,*lblWinPosition;
    IBOutlet UIButton *btnProfileImage;
    IBOutlet UIImageView *imgUser,*imgPosition;
    IBOutlet UIView *viewTriangle;
    IBOutlet MGLMapView *mapView;
    

    IBOutlet UILabel *lblWinnerTeamName,*lblWinner;
    IBOutlet UILabel *lblScoreboard,*lblSoloScoreboard;
    IBOutlet UIView *viewTeamTitle;
    IBOutlet NSLayoutConstraint *cnTblHeight;
    IBOutlet UITableView *tblScore;
    IBOutlet UICollectionView *clRunnerPositionl,*clCategory,*clRunners;
    
    
    IBOutlet UIView *viewGraph;
    IBOutlet UILabel *lblGraphStartValue,*lblGraphStartUpValue,*lblGraphEndValue,*lblGraphEndUpValue;
}

@property(strong,nonatomic) NSString *strActivityId;
@property(strong,nonatomic) NSString *strResultSessionId;
@property(nonatomic,strong) NSString *strSelectedUserId;

@end
