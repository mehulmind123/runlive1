//
//  RunnerPositionCell.m
//  RunLive
//
//  Created by mac-00012 on 6/29/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "RunnerPositionCell.h"

@implementation RunnerPositionCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgUser.layer.cornerRadius = CGRectGetHeight(self.imgUser.frame)/2;
    self.viewBorder.layer.cornerRadius = CGRectGetHeight(self.viewBorder.frame)/2;
    self.viewBorder.layer.borderWidth = 2;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}


@end
