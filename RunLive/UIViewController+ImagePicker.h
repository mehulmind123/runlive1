//
//  UIViewController+ImagePicker.h
//  Corpository
//
//  Created by mac-00014 on 12/28/16.
//  Copyright © 2016 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>


#define ChoosePhotoOption   @"Select Photo"
#define PhotoLibraryOption  @"From Gallery"
#define PhotoCameraOption   @"From Camera"


typedef void (^CompletionHandler)(UIImage *image);

typedef void (^CompletionHandlerFullImageInfo)(UIImage *image,NSDictionary *dicImageInfo);

@interface UIViewController (ImagePicker) <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

//- (void)presentImagePickerSource:(CompletionHandler)handler;

- (void)presentImagePickerSource:(CompletionHandler)handler AllowEditing:(BOOL)isEditing;
- (void)presentPhotoLibray:(CompletionHandler)handler;
- (void)presentPhotoCamera:(CompletionHandler)handler;

- (void)presentPhotoLibrayWithfullImageDetail:(CompletionHandlerFullImageInfo)handler AllowEditing:(BOOL)isEditing;

@end
