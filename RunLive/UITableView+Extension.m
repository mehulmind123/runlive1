//
//  UITableView+Extension.m
//  Master
//
//  Created by mac-0001 on 21/02/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import "UITableView+Extension.h"

@implementation UITableView (Extension)

-(NSIndexPath *)lastIndexPath
{
    NSUInteger sections = [self numberOfSections];
    
    if (sections<=0)
        return nil;
    
    NSUInteger rows = [self numberOfRowsInSection:sections-1];
    
    if (rows<=0)
        return nil;
    
    return [NSIndexPath indexPathForRow:rows-1 inSection:sections-1];
}
//-(NSIndexPath *)lastIndexPathofsection:(int)section;
//{
//    NSUInteger sections = [self numberOfSections];
//    
//    if (sections<=0)
//        return nil;
//
//    NSUInteger rows = [self numberOfRowsInSection:section];
//    
//    if (rows<=0)
//        return nil;
//    
//    return [NSIndexPath indexPathForRow:rows-1 inSection:section];
//}
@end
