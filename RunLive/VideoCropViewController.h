//
//  VideoCropViewController.h
//  Krishna_Location
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoCropViewDelegate
- (void)finishCroping:(PHAsset *)asset didCancel:(BOOL)cancel;
@end


@interface VideoCropViewController : UIViewController
{
    IBOutlet UIView *viewVideoPreview;
}
- (id)initWithVideo:(NSURL *)videoUrl;

@property(nonatomic, strong) id<VideoCropViewDelegate> delegate;

@end
