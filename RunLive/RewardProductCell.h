//
//  RewardProductCell.h
//  CollectionViewDemo
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardProductCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIView *viewContainer;
@property(weak,nonatomic) IBOutlet UIImageView *imgProduct;
@property(weak,nonatomic) IBOutlet UIButton *btnMoreDetails;
@property(weak,nonatomic) IBOutlet UIButton *btnAddToBasket;
@property(weak,nonatomic) IBOutlet UILabel *lblPoints;
@property(weak,nonatomic) IBOutlet UILabel *lblProductName;
@property(weak,nonatomic) IBOutlet UILabel *lblDescription;


@end
