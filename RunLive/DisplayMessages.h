

#define CMessageErrorSlowInternet @"Please check your internet connection. and try again later!"
#define CMessageCongratulation @"Congratulations!"
#define CMessageSorry @"Sorry!"
#define CMessageLoading @"Loading..."
#define CMessageSearching @"Searching..."
#define CMessageVerifying @"Verifying..."
#define CMessageWait @"Please Wait..."
#define CMessageAuthenticating @"Authenticating..."

#define CMessageLoginValidation @"Email OR password is wrong."
#define CMessageValidEmail @"Please enter valid email address."
#define CMessageWieght @"Please enter weight."
#define CMessageHeight @"Please enter height."
#define CMessageBirthDate @"Please enter date of birth."

#define CMessageLoginFirst @"Please login to access this."
#define CMessageUsername @"Username can only use letters, numbers, underscores and periods."
#define CMessagePassword @"Create a password at least 6 characters long."
#define CMessageVoiceChatDisable @"In order to use voice chat, you must enable it from the settings menu first."

#define CSTRAVAPermission @"Thank you for registering for RunLive. Your STRAVA account does not provide your email address. Please allow STRAVA permissions or register for RunLive with your email address."
#define CFacebookPermission @"Thank you for registering for RunLive. Your Facebook account does not provide your email address. Please allow Facebook permissions or register for RunLive with your email address."

#define CMessagePhone @"Please enter mobile number."
#define CMessageCategory @"Please select a category."
#define CMessageOldPassword @"Please enter old password."
#define CMessageNewPassword @"Please enter new password."
#define CMessageConfirmPassword @"Please confirm your password."
#define CMessageCompareOldandNewPassword @"New Password can not be your old password"
#define CMessagePasswordDontMatch @"Password and confirm password field values does not match."
#define CMessageFullname @"Full Name is required"
#define CMessageVerificationCode @"Please enter the verification code."
#define CMessageSelectPicture @"Please select a photo."
#define CMessageText @"You forgot to write something"
#define CMessageSafariCantOpen @"Safari can't open this URL."
#define CMessageDOB @"Please enter date of birth."

#define CMessageSaveChanges @"Do you want to save changes?"
#define CMessageDeleteChat @"Do you want to delete?"

#define CMessageSearchingSession @"There is no one ready to run yet. Continue searching for one more minute or try a new match?"

#define CMessageDisqualifiedFromRace @"We only like Cheetah’s if they’re animals 😊. Sorry you’ve been disqualified. If you feel this was an error, please contact support@runlive.com"

#define CMessageRunningNotice @"It is recommend that you turn OFF wifi and keep the RunLive app open on your screen while we locate and match you against runners. You can minimize the app or lock your phone once your race has started."

#define CMessageApiRequestTimeOut @"Request time out please try again."
#define CMessageMonthlySubcrition @"You are already subscribed to RunLive membership. Your subscription will be automatically renewed every month. To cancel auto subscription, click Cancel Subscription."
#define CMessageYearlySubcrition @"You are already subscribed to RunLive membership. Your subscription will be automatically renewed every year. To cancel auto subscription, click Cancel Subscription."
#define CMessageQuitFromSessionDueToInternet @"Looks like you lost your internet connection during match set-up, Please try searching for a new match."

#define CMessageQuitFromSessionDueToLocation @"Sorry, the GPS signal in your current location is too weak to track your run. Please try again later."

#define CMessageSessionNotFound @"Sorry! We couldn’t find a match ☹️ Please try choosing a different running mode."

#define CMessageSessionStartWarning @"For best running experience, it is recommended that you stay on this screen."


#define CMessageCompanName @"Please enter your company name."
#define CMessageCompanyAddress @"Please enter your company full address."
#define CMessageCompanyProvideService @"Please enter List primary industries your company provide service."
#define CMessageCompanyOperationHours @"Please enter Operating buisness hours."
#define CMessageCompanyWebsiteInfo @"Please enter website information."
#define CMessageCompanyNameDetails @"Please enter Name & Contact details of authorized person who will manage your company's iKYDen account."
#define CMessageCompanyListProductService @"Please enter List Product & Services provided by your company."
#define CMessageSelectCategory @"Please select category."

#define CAlertTitleForSpam @"Why are you reporting this comment?"
#define CAlertTitleForProfilePhoto @"Change Profile Picture"
#define CAlertTitleForCoverPhoto @"Change Cover Picture"
#define CAlertTitleForPostPhoto @"Post Picture"
