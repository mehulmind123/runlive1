//
//  CreateSessionViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClipView.h"
#import "MIWheelRotationGesture.h"

@interface CreateSessionViewController : SuperViewController<MIWheelRotationGestureDelegate>
{
    IBOutlet UITextField *txtName,*txtDate,*txtTime;
    IBOutlet UICollectionView *clUser;
    IBOutlet UILabel *lblDistance,*lblSelectedFriend,*lblMKM;
    IBOutlet UIScrollView *ScrollView;
    IBOutlet UIButton *btnCreateSession,*btnDate,*btnTime;
    
    //Speedometer
    IBOutlet ClipView *objClip;
    IBOutlet UIView *viewmeter,*viewSideScroll,*viewSideScrollContainer;
    
    IBOutlet NSLayoutConstraint *scrollIndY,*scrollIndheight;
    
    IBOutlet UIImageView *imgRotation;
    MIWheelRotationGesture *objRotateGesture;
}

@property(strong,nonatomic) NSDictionary *dicSelectedFriend;

@property(atomic,assign) BOOL isEditSession;

@property(strong,nonatomic) TblSessionList *objSessionEdit;
@end
