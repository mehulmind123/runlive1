//
//  SessionFeedbackViewController.h
//  RunLive
//
//  Created by mac-0005 on 26/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionFeedbackViewController : SuperViewController
{
    IBOutlet UITextView *txtViewFeedback;
    IBOutlet UIView *viewPlaceHolder;
    
    IBOutlet UIButton
    *btnMainAttachment,
    *btnIndicationAttachment,
    *btnSendFeedback;
}

@property(nonatomic,strong) NSString *strSessionId;
@property(nonatomic,strong) NSString *strSessionType;
@end
