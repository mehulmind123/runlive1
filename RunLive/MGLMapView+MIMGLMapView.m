//
//  MGLMapView+MIMGLMapView.m
//  RunLive
//
//  Created by mac-0005 on 13/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "MGLMapView+MIMGLMapView.h"

static NSString *const DRAWPOLYLINE = @"DRAWPOLYLINE";
static NSString *const POLYLINECOORDINATES = @"POLYLINECOORDINATES";
static NSString *const MapViewDelegate = @"MapViewDelegate";
static NSString *const POLYLINE = @"polyline";

@implementation MGLMapView (MIMGLMapView)


-(void)clearMGLMapView
{
    self.logoView.hidden = YES;
    self.compassView.hidden = YES;
    self.attributionButton.alpha = 0;
    self.showsUserLocation = NO;
    
    [self removeAnnotations:self.annotations];
    
    for (MGLStyleLayer *objLayer in self.style.layers)
    {
        if ([objLayer.identifier isEqualToString:POLYLINE])
            [self.style removeLayer:objLayer];
    }
    
    for (MGLSource *objSources in self.style.sources)
    {
        if ([objSources.identifier isEqualToString:POLYLINE])
            [self.style removeSource:objSources];
    }
    
}

-(void)drawPolylineByUsingCoordinates:(NSArray *)arrCoordinates
{
    BOOL isDrawing = YES;
    [self setBoolean:isDrawing forKey:DRAWPOLYLINE];
    [self setObject:arrCoordinates forKey:POLYLINECOORDINATES];
    self.delegate = self;

    [self clearMGLMapView];
    [self addMapLayerForPolyline:arrCoordinates];
}

- (void)setMapViewDelegate:(id<MIMGLMapViewDelegate>)delegate
{
    [self setObject:delegate forKey:MapViewDelegate];
}

- (void)addMapLayerForPolyline:(NSArray *)arrCoordinates
{
    // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
    MGLShapeSource *source = [[MGLShapeSource alloc] initWithIdentifier:POLYLINE features:@[] options:nil];
    [self.style addSource:source];
    
    // Add a layer to style our polyline.
    MGLLineStyleLayer *layer = [[MGLLineStyleLayer alloc] initWithIdentifier:POLYLINE source:source];
    layer.lineJoin = [MGLStyleValue valueWithRawValue:[NSValue valueWithMGLLineJoin:MGLLineJoinRound]];
    layer.lineCap = [MGLStyleValue valueWithRawValue:[NSValue valueWithMGLLineCap:MGLLineCapRound]];
    layer.lineColor = [MGLStyleValue valueWithRawValue:CRGB(70, 213, 255)];
    layer.lineWidth = [MGLStyleValue valueWithInterpolationMode:MGLInterpolationModeExponential
                                                    cameraStops: @{
                                                                   @14: [MGLStyleValue valueWithRawValue:@4],
                                                                   @18: [MGLStyleValue valueWithRawValue:@4]
                                                                   }
                                                        options:@{MGLStyleFunctionOptionDefaultValue:@4}];
    
    [self.style addLayer:layer];
    
    source.shape = [self drawPolylinePath:arrCoordinates];
}

-(MGLPolylineFeature *)drawPolylinePath:(NSArray *)arrCoordinates
{
    NSArray *arrPoints = arrCoordinates;
    CLLocationCoordinate2D coordinates[arrPoints.count];
    for (int i = 0; arrPoints.count > i; i++)
    {
        NSDictionary *dicCor = arrPoints[i];
        NSString *strLat = [dicCor valueForKey:@"latitude"];
        NSString *strLong = [dicCor valueForKey:@"longitude"];
        coordinates[i] = CLLocationCoordinate2DMake(strLat.floatValue, strLong.floatValue);
    }
    
    MGLPolylineFeature *polyline;
    
    if (arrPoints.count > 0)
        polyline = [MGLPolylineFeature polylineWithCoordinates:coordinates count:arrPoints.count];
    
    return polyline;
}

// Wait until the map is loaded before adding to the map.
- (void)mapView:(MGLMapView *)mapView didFinishLoadingStyle:(MGLStyle *)style
{
    BOOL isDrawingPolyline = [self booleanForKey:DRAWPOLYLINE];
    
    if (isDrawingPolyline)  // To Draw polyline path in map view......
    {
        [self clearMGLMapView];
        [self addMapLayerForPolyline:[self objectForKey:POLYLINECOORDINATES]];
    }
    
}


#pragma mark - Add Custom Marker
-(void)addCustomMarkerOnMapView:(CLLocationCoordinate2D)coordinate Type:(MarkerType)markerType UserInfo:(NSDictionary *)dicInfo;
{
    MIMapBoxPointAnnotation *point = [[MIMapBoxPointAnnotation alloc] init];
    point.coordinate = coordinate;
    point.tag = markerType;
    point.userInfo = dicInfo;
    self.delegate = self;
    [self addAnnotation:point];
}

- (MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id <MGLAnnotation>)annotation {
    // This example is only concerned with point annotations.
    if (![annotation isKindOfClass:[MGLPointAnnotation class]])
        return nil;
    
    MIMapBoxPointAnnotation *objPoint = (MIMapBoxPointAnnotation *)annotation;
    
    switch (objPoint.tag) {
        case StartPossitionMarker:
        {
            // start Marker
            CustomStartEndPointView *objstartStart = [CustomStartEndPointView viewFromXib];
            objstartStart.backgroundColor = CRGB(201, 247, 98);
            return objstartStart;
        }
            break;
        case EndPossitionMarker:
        {
            // End Marker
            CustomStartEndPointView *objstartEnd = [CustomStartEndPointView viewFromXib];
            objstartEnd.backgroundColor = CRGB(247, 98, 98);
            return objstartEnd;
        }
            break;
        case PhotoVideoMarker:
        {
            CustomPhotoVideoMarker *objPhotoVideo = [CustomPhotoVideoMarker viewFromXib];
            [objPhotoVideo.btnMarker touchUpInsideClicked:^{
                
                id mapViewdelegate = [self objectForKey:MapViewDelegate];
                
                
                if (mapViewdelegate)
                    [mapViewdelegate mapviewDidTapMarker:objPoint.userInfo];
                
            }];
            
            return objPhotoVideo;
        }
            break;
        case CurrentLocationMarker:
        {
            MIMGLCustomCurrentLocationAnimtedView *objCurrentLocation = [MIMGLCustomCurrentLocationAnimtedView viewFromXib];
            return objCurrentLocation;
        }
            break;
            
        default:
            break;
    }
 
    return nil;
}

- (BOOL)mapView:(MGLMapView *)mapView annotationCanShowCallout:(id<MGLAnnotation>)annotation {
    return YES;
}


#pragma mark - Center View

-(void)MGLMapViewCenterCoordinate:(NSArray *)arrCoord
{
    float lat=0, lng=0;
    for(int i=0; i<arrCoord.count; ++i) {
        NSDictionary *dicData = arrCoord[i];
        lat += [dicData stringValueForJSON:@"latitude"].doubleValue;
        lng += [dicData stringValueForJSON:@"longitude"].doubleValue;
    }
    
    lat /= arrCoord.count;
    lng /= arrCoord.count;

    [self setCenterCoordinate:CLLocationCoordinate2DMake(lat, lng)];
}

-(void)setPathCenterInMapView:(NSArray *)arrLocation edgeInset:(UIEdgeInsets)inset
{
    NSMutableArray *arrMarker = [NSMutableArray new];
    for (int i = 0; arrLocation.count > i; i++)
    {
        NSDictionary *dicData = arrLocation[i];
        MGLPointAnnotation *marker=[[MGLPointAnnotation alloc]init];
        marker.coordinate = CLLocationCoordinate2DMake([dicData stringValueForJSON:@"latitude"].doubleValue, [dicData stringValueForJSON:@"longitude"].doubleValue);
        [arrMarker addObject:marker];
    }
    
    [self showAnnotations:arrMarker edgePadding:inset animated:NO];
    
}

@end
