//
//  TblActivitySyncSession+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivitySyncSession+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblActivitySyncSession (CoreDataProperties)

+ (NSFetchRequest<TblActivitySyncSession *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *activity_id;
@property (nullable, nonatomic, copy) NSString *activity_main_type;
@property (nullable, nonatomic, copy) NSString *activity_post_time;
@property (nullable, nonatomic, copy) NSString *activity_text;
@property (nullable, nonatomic, retain) NSObject *comments;
@property (nullable, nonatomic, copy) NSNumber *is_like;
@property (nullable, nonatomic, copy) NSNumber *session_endtime;
@property (nullable, nonatomic, copy) NSString *session_id;
@property (nullable, nonatomic, copy) NSString *session_name;
@property (nullable, nonatomic, retain) NSObject *sync_user;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, copy) NSString *total_comments;
@property (nullable, nonatomic, copy) NSString *total_likes;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_image;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, copy) NSNumber *is_commented;
@property (nullable, nonatomic, retain) TblActivityDates *objActivityDate;

@end

NS_ASSUME_NONNULL_END
