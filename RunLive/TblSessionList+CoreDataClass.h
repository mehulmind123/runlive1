//
//  TblSessionList+CoreDataClass.h
//  RunLive
//
//  Created by mac-0005 on 29/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TblJoinedUser, TblSessionListDate, TblSessionOtherUser, TblSessionSyncedUser;

NS_ASSUME_NONNULL_BEGIN

@interface TblSessionList : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblSessionList+CoreDataProperties.h"
