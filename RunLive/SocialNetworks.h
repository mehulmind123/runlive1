

//
//  SocialNetworks.h
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//


#ifndef _SocialNetworks_H
    #define _SocialNetworks_H
#endif



#define SOCIAL_NETWORKS_VERSION @"2.0"





#import <Foundation/Foundation.h>
#import <Social/Social.h>


#import "PostParameters.h"


typedef void (^SocialNetworkSuccessBlock)(NSDictionary *result);
typedef void (^SocialNetworkFailureBlock)(NSError *error);


typedef NS_ENUM(NSInteger, SocialNetworkOptions) {
    SocialNetworkFacebook          = 0,
    SocialNetworkTwitter,
    SocialNetworkGoogle,
    SocialNetworkLinkdIn,
    SocialNetworkTumblr,
    SocialNetworkPinterest,
    SocialNetworkYahoo,
    SocialNetworkFlickr,
    SocialNetworkDropbox,
    SocialNetworkInstagram,
    SocialNetworkWhatsApp,
};


//#import <Master/Master.h>
//#import <AFNetworking/AFNetworking.h>
#import "AFNetworking.h"


@interface SocialNetworks : NSObject

@property (nonatomic,strong) NSString *facebookAppId;
@property (nonatomic,strong) NSString *facebookCallBackURL;
@property (nonatomic,strong) NSString *facebookRedirectURLForShareDialog;

@property (nonatomic,strong) NSArray *facebookReadPermissions;

@property (nonatomic,strong) NSString *googlePlusClientId;
@property (nonatomic,strong) NSString *googlePlusCallBackURL;

@property (nonatomic,strong) NSString *pinterestClientId;
@property (nonatomic,strong) NSString *pinterestCallBackURL;

@property (nonatomic,strong) NSString *linkedInAPIKey;
@property (nonatomic,strong) NSString *linkedInSecretKey;
@property (nonatomic,strong) NSString *linkedInState;
@property (nonatomic,strong) NSString *linkedInCallBackURL;

@property (nonatomic,strong) NSString *twitterConsumerKey;
@property (nonatomic,strong) NSString *twitterConsumerSecret;

@property (nonatomic,strong) NSString *instagramClientID;
@property (nonatomic,strong) NSString *instagramClientSecret;
@property (nonatomic,strong) NSString *instagramCallBackURL;
@property (nonatomic,strong) NSArray *instagramScopes;



+ (instancetype)sharedInstance;

-(void)logoutFromAllSocialNetworks;
-(void)logoutFromNetwork:(SocialNetworkOptions)network;

-(BOOL)isAunthenticatedForNetwork:(SocialNetworkOptions)network;


-(void)loginWithSocialNetwork:(SocialNetworkOptions)network success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;



-(void)postOnSocialNetwork:(SocialNetworkOptions)network withData:(PostParameters *)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;
-(void)postOnSocialNetworkThroughAPIOnly:(SocialNetworkOptions)network withData:(PostParameters *)data success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;


-(void)fetchFriendsForSocialNetwork:(SocialNetworkOptions)network success:(SocialNetworkSuccessBlock)success failure:(SocialNetworkFailureBlock)failure;


@end



// To-Do - Maintain Autologin
// To-Do - Manage login history for all social networks

// To-Do Post with background and user dialog










// ================================================================================================================

// Version Changes

// ================================================================================================================

/*
 
 // 2.0
 
 - Add Support for FacebookSDK 4.0 and TwitterKit from Fabric
 - Remove Support for STTwitter and FacebookSDK 3.0
 
 
*/

