//
//  CustomPhotoVideoMarker.h
//  RunLive
//
//  Created by mac-0005 on 13/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <Mapbox/Mapbox.h>

@interface CustomPhotoVideoMarker : MGLAnnotationView

@property(weak,nonatomic) IBOutlet UIImageView *imgCapture;

@property(weak,nonatomic) IBOutlet UIButton *btnMarker;

@end
