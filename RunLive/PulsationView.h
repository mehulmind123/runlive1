//
//  PulsationView.h
//  RunLive
//
//  Created by Eugene on 12.02.2018.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PulsationView : UIView

- (void)start;

@end
