//
//  PhotoVideoPreviewController.h
//  RunLive Demo
//
//  Created by mac-00014 on 11/21/16.
//  Copyright © 2016 MI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

typedef void(^btnBackCLK)(BOOL isBack);

@interface PhotoVideoPreviewController : SuperViewController<UIDocumentInteractionControllerDelegate,FBSDKSharingDelegate>
{
    IBOutlet UIButton *btnShareFB,*btnShareTwitter,*btnShareEmail,*btnShareInstagram,*btnUpload,*btnPlayVideo,*btnZoomImage;
    IBOutlet UITextField *txtAddCaption;
    IBOutlet UIImageView *imgVCapture;
    IBOutlet UIView *viewVideo,*viewImageContainer,*viewVideoContainer;
    IBOutlet UIView *viewImageWaterMark;
    
    IBOutlet NSLayoutConstraint *cnPreviewItemHeight;
    
    IBOutlet UILabel
    *lblPhotoTotalDistance,
    *lblPhotoMovingTime,
    *lblPhotoPace,
    *lblPhotoRank;
    
}

@property(nonatomic,copy) btnBackCLK configureBtnBackCLK;
@property(nonatomic,strong) UIImage *imgCapture;
@property(nonatomic,strong) PHAsset *asset;
@property(nonatomic,strong) NSString *strPreviewMediaSessionID;
@property(nonatomic,strong) NSDictionary *dicPhotoVideoWaterMark;
@property(nonatomic, strong) CLLocation *mediaLocation;

@end
