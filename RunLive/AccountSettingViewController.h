//
//  AccountSettingViewController.h
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIImageCropperViewController.h"
#import "HPGrowingTextView.h"

@interface AccountSettingViewController : SuperViewController<MIImageCropperDelegate,HPGrowingTextViewDelegate>
{

    IBOutlet HPGrowingTextView *txtViewBio;
    IBOutlet NSLayoutConstraint *cnTextViewBioHeight;
    
    IBOutlet UIImageView *imgUser;
    IBOutlet UIView
    *viewHeightSeprator,
    *viewHeightImperial;
    
    IBOutlet UILabel
    *lblWeigth,
    *lblHeight;
    
    IBOutlet UITextField
    *txtFeet,
    *txtInches;
    
    
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtdob;
    IBOutlet UITextField *txtWeight;
    IBOutlet UITextField *txtHeight;
    IBOutlet UITextField *txtLink;
    
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnImperial;
    IBOutlet UIButton *btnMetric;
    IBOutlet UIButton *btnSave;
}

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnSelectImageClicked:(id)sender;
- (IBAction)btnGenderClicked:(UIButton *)sender;
- (IBAction)btnUnitsClicked:(UIButton *)sender;
- (IBAction)btnChangePasswordClicked:(id)sender;

@end
