//
//  FacebookSignUpViewController.m
//  RunLive
//
//  Created by mac-0005 on 21/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "FacebookSignUpViewController.h"

@interface FacebookSignUpViewController ()

@end

@implementation FacebookSignUpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnRegister.layer.cornerRadius = 3;
    
    [txtUserName setTextFieldPlaceholderText:@"Username"];
    txtUserName.imageWrong = @"worng_lrf";
    txtUserName.selectedLineColor = CLineSelected;
    txtUserName.placeHolderColor = CLRFPlaceholderColor;
    txtUserName.selectedPlaceHolderColor = CLRFPlaceholderSelectedColor;
    txtUserName.lineColor = CLRFTextFieldLineColor;
    lblInfoText.text = @"Please choose your username.";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark  UITextfield Delegates
#pragma mark

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtUserName)
        [txtUserName floatTheLabel:@"username" andText:textField.text];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txtUserName)
        [txtUserName floatTheLabel:@"" andText:textField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (range.location == 0 && [string isEqualToString:@" "])
        return NO;
    
    if (textField == txtUserName)
        [txtUserName floatTheLabel:@"username" andText:strNew];
    
    return YES;
}

#pragma mark - Action Event
-(IBAction)btnBackCLK:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.configureUsernameSuccessfullyAdded)
            self.configureUsernameSuccessfullyAdded(NO,self.strSocialEmail);
    }];
}

-(IBAction)btnRegisterCLK:(id)sender
{
    [appDelegate hideKeyboard];
    
     if (![txtUserName.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:CMessageSorry Message:CMessageUsername ButtonText:@"OK" Animation:YES completed:nil];
    else
    {
        [[APIRequest request] signup:self.strSocialEmail andUserName:txtUserName.text andPassword:nil andFacebookID:self.strSocialId FirstName:self.strSocialFirstName LastName:self.strSocialLastName Type:self.isLoginType ? @"2" : @"3" completed:^(id responseObject, NSError *error)
        {
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.configureUsernameSuccessfullyAdded)
                    self.configureUsernameSuccessfullyAdded(YES,self.strSocialEmail);
            }];
        }];
    }
}

@end
