//
//  ImageZoomViewController.m
//  MetLife_Demo
//
//  Created by mac-0009 on 9/4/15.
//  Copyright (c) 2015 mac-0009. All rights reserved.
//

#import "ImageZoomViewController.h"

@interface ImageZoomViewController ()
{
   BOOL isFirstTimeViewDidLayoutSubviews;
}
@end

@implementation ImageZoomViewController
@synthesize pcImageZoom,colVImageZoom,imageArray,strTitle,currentRow;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = nil;
    isFirstTimeViewDidLayoutSubviews = YES;
    self.title = strTitle;
    
    [self.colVImageZoom registerNib:[UINib nibWithNibName:@"ImgZoomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ImgZoomCollectionViewCell"];
    
    self.pcImageZoom.numberOfPages = imageArray.count;
//    self.pcImageZoom.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.pcImageZoom];
    
    self.pcImageZoom.hidden = imageArray.count < 2 ? YES : NO;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapRecognizer.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapRecognizer];

}
- (void)viewDidLayoutSubviews
{
    // only after layoutSubviews executes for subviews, do constraints and frames agree (WWDC 2012 video "Best Practices for Mastering Auto Layout")
    [super viewDidLayoutSubviews];
    if (isFirstTimeViewDidLayoutSubviews)
    {
        self.pcImageZoom.currentPage = currentRow;
        float temp = [[NSNumber numberWithInt:currentRow] floatValue];
        temp = temp*self.view.frame.size.width;
        [self.colVImageZoom setContentOffset:CGPointMake(temp , self.colVImageZoom.contentOffset.y) animated:NO];
        // execute geometry-related code...
        
        // good place to set scroll view's content offset, if its subviews are added dynamically (in code)
        
        isFirstTimeViewDidLayoutSubviews = NO;
    }
}
- (void)viewDidUnload
{
  //  [self setImageView:nil];
    [super viewDidUnload];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark - CollectionView delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imageArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"ImgZoomCollectionViewCell";
    
    ImgZoomCollectionViewCell *cell = (ImgZoomCollectionViewCell *)[colVImageZoom dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil)
        cell = [[ImgZoomCollectionViewCell alloc] init];
    cell.cellScrollView.zoomScale = 1.0;
    cell.cellScrollView.maximumZoomScale=3.0;
    cell.cellScrollView.delegate = self;
    
    [cell.imgVImageZoom setImageWithURL:[NSURL URLWithString:[imageArray objectAtIndex:indexPath.item]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CViewWidth(collectionView),CViewHeight(collectionView));
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    if (scrollView.tag == 99) {
      UIImageView *imgCurrent  = (UIImageView *) [scrollView viewWithTag:101];
        return imgCurrent;
    }
    
    return nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:colVImageZoom])
        self.pcImageZoom.currentPage = self.colVImageZoom.contentOffset.x / CViewWidth(scrollView);
}

#pragma mark - Gesture Recognizers

- (void)tapDetected:(UITapGestureRecognizer *)tapRecognizer
{
 ImgZoomCollectionViewCell *cell =  (ImgZoomCollectionViewCell *) [colVImageZoom visibleCells][0];
    if (cell.cellScrollView.zoomScale == 1)
    {
        CGPoint pt = [tapRecognizer locationInView:colVImageZoom.backgroundView];
        [cell.cellScrollView zoomToRect:CGRectMake(pt.x-CViewWidth(colVImageZoom)/4, pt.y-CViewHeight(colVImageZoom)/4, CViewWidth(colVImageZoom)/2, CViewHeight(colVImageZoom)/2) animated:YES];
    }
    else
    [cell.cellScrollView setZoomScale:1.0 animated:YES];

}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - Action Event
-(IBAction)btnCancelCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
