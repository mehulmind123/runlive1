//
//  TblSessionListDate+CoreDataProperties.h
//  RunLive
//
//  Created by mac-00012 on 4/28/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionListDate+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblSessionListDate (CoreDataProperties)

+ (NSFetchRequest<TblSessionListDate *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *date_text;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, retain) NSSet<TblSessionList *> *sessionList;

@end

@interface TblSessionListDate (CoreDataGeneratedAccessors)

- (void)addSessionListObject:(TblSessionList *)value;
- (void)removeSessionListObject:(TblSessionList *)value;
- (void)addSessionList:(NSSet<TblSessionList *> *)values;
- (void)removeSessionList:(NSSet<TblSessionList *> *)values;

@end

NS_ASSUME_NONNULL_END
