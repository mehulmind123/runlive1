//
//  UILabel+TTTAttributedLabel.m
//  Journey
//
//  Created by mac-0001 on 12/06/15.
//  Copyright (c) 2015 MI. All rights reserved.
//

#import "UILabel+TTTAttributedLabel.h"

@implementation UILabel (TTTAttributedLabel) 

-(void)addHashTagAndUserHandler:(NSString *)strUserName Complete:(void(^)(NSString *urlString))block;
{
    if (strUserName.length < 1)
        return;
    
    NSString *string = self.text;
    NSArray *matches = [[NSRegularExpression regularExpressionWithPattern:@"(#(\\w+|-|_|\\w+)+|@(\\w+|-|_|\\w+)+)" options:NO error:nil] matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    
//    NSArray *matches = [[NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"(#(\\w+|-|_|\\w+)+|@(\\w+|-|_|\\w+)+|%@)",strUserName] options:NO error:nil] matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    
    NSArray *matchesName = [[NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"%@",strUserName] options:NO error:nil] matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    NSRange userRange = [self.text rangeOfString:strUserName];
    
    // Check User Name only.....
    for (NSTextCheckingResult *match in matchesName)
    {
        if (userRange.location == match.range.location && userRange.length == match.range.length)
        {
            [(TTTAttributedLabel*)self addLinkWithTextCheckingResult:match attributes:@{(id)kCTForegroundColorAttributeName:CRGB(60, 60, 65),(id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:kCTUnderlineStyleNone],(id)kCTFontAttributeName:CFontGilroyBold(14)}];
        }
    }
    
    // Check Name With "@" prefix here......
    for (NSTextCheckingResult *match in matches)
    {
        [(TTTAttributedLabel*)self addLinkWithTextCheckingResult:match attributes:@{(id)kCTForegroundColorAttributeName:CRGB(115, 216, 245),(id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:kCTUnderlineStyleNone],(id)kCTFontAttributeName:CFontGilroyRegular(14)}];
//        if (userRange.location == match.range.location && userRange.length == match.range.length)
//        {
//             [(TTTAttributedLabel*)self addLinkWithTextCheckingResult:match attributes:@{(id)kCTForegroundColorAttributeName:CRGB(60, 60, 65),(id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:kCTUnderlineStyleNone],(id)kCTFontAttributeName:CFontGilroyBold(14)}];
//        }
//        else
//        {
//             [(TTTAttributedLabel*)self addLinkWithTextCheckingResult:match attributes:@{(id)kCTForegroundColorAttributeName:CRGB(115, 216, 245),(id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:kCTUnderlineStyleNone],(id)kCTFontAttributeName:CFontGilroyRegular(14)}];
//        }
    }

    self.userInteractionEnabled = YES;
    [(TTTAttributedLabel *)self setDelegate:self];
    
    [self setObject:block forKey:@"blockKey"];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{
    NSString *absoluteString = [label.text substringWithRange:result.range];
    
    void(^handler)(NSString *string) = [self objectForKey:@"blockKey"];
    
    if (handler)
        handler(absoluteString);
}



@end
