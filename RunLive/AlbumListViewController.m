//
//  AlbumListViewController.m
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "AlbumListViewController.h"

@interface AlbumListViewController ()

@end

@implementation AlbumListViewController
{
    SPTUser *objSTPUser;
    
    SPTPlaylistList *objPlayList;
    SPTPartialPlaylist *objPartialList;
    
    SPTPlaylistSnapshot *objPlayListSnap;

    NSMutableArray *arrApplePlaylist, *arrSpotifyPlaylist;
    
}

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [tblList registerNib:[UINib nibWithNibName:@"PlayListCell" bundle:nil] forCellReuseIdentifier:@"PlayListCell"];
    
    viewSearchBar.layer.cornerRadius = 2;
    viewSearchBar.layer.masksToBounds = YES;
    
    [txtSearch addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    imgLogo.image = self.isApple ? [UIImage imageNamed:@"ic_appple_logo"] : [UIImage imageNamed:@"ic_spotify_logo"];
    
    //arrPlaylist = [NSMutableArray new];
    
    arrApplePlaylist = [[NSMutableArray alloc] init];
    
    arrSpotifyPlaylist = [[NSMutableArray alloc] init];
    
    if (self.isApple)
    {
        [viewAppleCategory hideByHeight:NO];
        [self btnAppleCategoryCLK:btnPlaylists];
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 9.3)
        {
            [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status)
             {
                 switch (status)
                 {
                     case MPMediaLibraryAuthorizationStatusDenied:
                     {
                     }
                         break;
                     case MPMediaLibraryAuthorizationStatusAuthorized:
                     {
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self getApplePlayList];
                         });
                     }
                         break;
                         
                     default:
                         break;
                 }
             }];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getApplePlayList];
            });
        }
    }
    else
    {
        [viewAppleCategory hideByHeight:YES];
        [self getSpotifyPlaylist];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
}

#pragma mark - Textfield Methods

-(void)textfieldDidChange:(UITextField *)txt
{
    if ([txt.text length] == 0)
    {
        [self btnCancelSearch:nil];
        self.isApple ? [self getApplePlayList] : [self getSpotifyPlaylist];
    }
    else
    {
        if (self.isApple)
        {
            // Searching from Apple List
            [self getAppleDataBySearching:txt.text];
        }
        else
        {
            [self getSpotifyDataBySearching:txt.text];
        }
    }
}

#pragma mark - Get Apple Playlist

-(void)getAppleDataBySearching:(NSString *)searchText
{
    MPMediaQuery *songsQuery;
    
    if (btnPlaylists.selected)
    {
        MPMediaPropertyPredicate *artistPredicate = [MPMediaPropertyPredicate predicateWithValue:searchText forProperty:MPMediaPlaylistPropertyName comparisonType:MPMediaPredicateComparisonContains];
        NSSet *predicates = [NSSet setWithObjects: artistPredicate, nil];
        
        songsQuery =  [[MPMediaQuery alloc] initWithFilterPredicates: predicates];
        [songsQuery setGroupingType: MPMediaGroupingPlaylist];
    }
    else if (btnAlbums.selected)
    {
        MPMediaPropertyPredicate *artistPredicate = [MPMediaPropertyPredicate predicateWithValue:searchText forProperty:MPMediaItemPropertyAlbumTitle comparisonType:MPMediaPredicateComparisonContains];
        NSSet *predicates = [NSSet setWithObjects: artistPredicate, nil];
        
        songsQuery =  [[MPMediaQuery alloc] initWithFilterPredicates: predicates];
        [songsQuery setGroupingType: MPMediaGroupingAlbum];
    }
    else
    {
        MPMediaPropertyPredicate *artistPredicate =
        [MPMediaPropertyPredicate predicateWithValue:searchText forProperty:MPMediaItemPropertyArtist comparisonType:MPMediaPredicateComparisonContains];
        NSSet *predicates = [NSSet setWithObjects: artistPredicate, nil];
        
        songsQuery =  [[MPMediaQuery alloc] initWithFilterPredicates: predicates];
        [songsQuery setGroupingType: MPMediaGroupingArtist];

    }
    
    
    [arrApplePlaylist removeAllObjects];
    [tblList reloadData];
    
    [arrApplePlaylist addObjectsFromArray:[songsQuery collections]];
    [tblList reloadData];
}

-(void)getApplePlayList
{
 

    NSArray *arrPlaylist;
    if (btnPlaylists.selected)
    {
        MPMediaQuery *myPlaylistsQuery = [MPMediaQuery playlistsQuery];
        [myPlaylistsQuery setGroupingType: MPMediaGroupingPlaylist];
        arrPlaylist = [myPlaylistsQuery collections];
        
//        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrPlaylist];
//        NSArray *arrayWithoutDuplicates = [orderedSet array];
//        arrPlaylist = arrayWithoutDuplicates;

    }
    else if (btnAlbums.selected)
    {
        MPMediaQuery *myPlaylistsQuery = [MPMediaQuery albumsQuery];
        [myPlaylistsQuery setGroupingType: MPMediaGroupingAlbum];
        arrPlaylist = [myPlaylistsQuery collections];
    }
    else
    {
        MPMediaQuery *myPlaylistsQuery = [MPMediaQuery artistsQuery];
        [myPlaylistsQuery setGroupingType: MPMediaGroupingArtist];
        arrPlaylist = [myPlaylistsQuery collections];
    }
    
    [arrApplePlaylist removeAllObjects];
    [tblList reloadData];
    
    [arrApplePlaylist addObjectsFromArray:arrPlaylist];
    [tblList reloadData];
}

#pragma mark - Get Spotify List
-(void)getSpotifyDataBySearching:(NSString *)searchText
{
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    //[SPTSearch]
    
    [SPTSearch performSearchWithQuery:searchText queryType:SPTQueryTypePlaylist accessToken:auth.session.accessToken callback:^(NSError *error, id object)
     {
         if (!error)
         {
             objPlayList = (SPTPlaylistList *)object;
             [arrSpotifyPlaylist removeAllObjects];
             [tblList reloadData];
             
             [arrSpotifyPlaylist addObjectsFromArray:objPlayList.items];
             [tblList reloadData];
         }
     }];
}

-(void)getSpotifyPlaylist
{
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    [SPTUser requestCurrentUserWithAccessToken:auth.session.accessToken callback:^(NSError *error, id object)
     {
         if(error)
             [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:error.localizedDescription];
         else
         {
             //[[PPLoader sharedLoader] HideHudLoader];
             objSTPUser = object;
             
             appDelegate.lFollowerCount = objSTPUser.followerCount;
             
             [SPTPlaylistList playlistsForUser:objSTPUser.canonicalUserName withAccessToken:auth.session.accessToken callback:^(NSError *error, id object)
              {
                  objPlayList = (SPTPlaylistList *)object;
                  [arrSpotifyPlaylist removeAllObjects];
                  [arrSpotifyPlaylist addObjectsFromArray:objPlayList.items];
                  [tblList reloadData];
              }];
         }
     }];
}

#pragma mark - UITableview Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isApple)
        return arrApplePlaylist.count;
    
    return arrSpotifyPlaylist.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"PlayListCell";
    PlayListCell* cell = [tblList dequeueReusableCellWithIdentifier:strIdentifier];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnPlayPause.hidden = YES;
    
    if (self.isApple)
    {
        if (btnPlaylists.selected)
        {
            // Code for apple Playlist
            MPMediaPlaylist *objApplePlayList = [arrApplePlaylist objectAtIndex:indexPath.row];
            cell.lblTitle.text = objApplePlayList.name;
            cell.lblSubTitle.text = [NSString stringWithFormat:@"%ld songs",(long)objApplePlayList.items.count];
            
            cell.imgAlbum.contentMode = UIViewContentModeScaleAspectFit;
            if(objApplePlayList.items.count > 0)
            {
                MPMediaItem *objAppleSong = objApplePlayList.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    cell.imgAlbum.image = albumArtworkImage;
                else
                    cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];
            }
            else
                cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];
        }
        else if (btnAlbums.selected)
        {
            // Album data
            MPMediaItemCollection *objAppleAlbum = [arrApplePlaylist objectAtIndex:indexPath.row];
            MPMediaItem *objItem = [objAppleAlbum representativeItem];
            cell.lblTitle.text = objItem.albumTitle;
            cell.lblSubTitle.text = [NSString stringWithFormat:@"%ld songs",(long)objAppleAlbum.items.count];

            if(objAppleAlbum.items.count > 0)
            {
                MPMediaItem *objAppleSong = objAppleAlbum.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    cell.imgAlbum.image = albumArtworkImage;
                else
                    cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];
            }
            else
                cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];
            
            return cell;
        }
        else
        {
            // Artists data
            MPMediaItemCollection *objAppleArtists = [arrApplePlaylist objectAtIndex:indexPath.row];
            MPMediaItem *objItem = [objAppleArtists representativeItem];
            cell.lblTitle.text = objItem.artist;
            cell.lblSubTitle.text = [NSString stringWithFormat:@"%ld songs",(long)objAppleArtists.items.count];
            
            if(objAppleArtists.items.count > 0)
            {
                MPMediaItem *objAppleSong = objAppleArtists.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    cell.imgAlbum.image = albumArtworkImage;
                else
                    cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];
            }
            else
                cell.imgAlbum.image = [UIImage imageNamed:@"ic_music"];

            return cell;
        }
        
    }
    else
    {
        SPTPartialPlaylist *item = [arrSpotifyPlaylist objectAtIndex:indexPath.row];
        cell.lblTitle.text = item.name;
        cell.lblSubTitle.text = [NSString stringWithFormat:@"%ld songs",(long)item.trackCount];
        SPTImage *image = item.largestImage;
        [cell.imgAlbum setImageWithURL:image.imageURL usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isApple)
    {
        if (btnPlaylists.selected)
        {
            MPMediaPlaylist *objApplePlayList = [arrApplePlaylist objectAtIndex:indexPath.row];
            if (objApplePlayList.count > 0)
            {
                MPMediaItem *objAppleSong = objApplePlayList.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    appDelegate.imgTempMusic = albumArtworkImage;
                else
                    appDelegate.imgTempMusic = [UIImage imageNamed:@"ic_music"];
                
                appDelegate.strPlaylistName = objApplePlayList.name;
                
                MPMediaItemCollection *appleCollection = [[MPMediaItemCollection alloc] initWithItems:objApplePlayList.items];
                
                MusicListViewController *objMusic = [[MusicListViewController alloc]initWithNibName:@"MusicListViewController" bundle:nil];
                objMusic.isApple = self.isApple;
                objMusic.arrAppleMusic = objApplePlayList.items;
                objMusic.appleMusicCollection = appleCollection;
                
                [self.navigationController pushViewController:objMusic animated:YES];
            }
        }
        else if(btnAlbums.selected)
        {
            MPMediaItemCollection *objAppleAlbum = [arrApplePlaylist objectAtIndex:indexPath.row];
            if(objAppleAlbum.items.count > 0)
            {
                MPMediaItem *objAppleSong = objAppleAlbum.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    appDelegate.imgTempMusic = albumArtworkImage;
                else
                    appDelegate.imgTempMusic = [UIImage imageNamed:@"ic_music"];
                
                appDelegate.strPlaylistName = objAppleSong.albumTitle;
                
                MPMediaItemCollection *appleCollection = [[MPMediaItemCollection alloc] initWithItems:objAppleAlbum.items];
                
                MusicListViewController *objMusic = [[MusicListViewController alloc]initWithNibName:@"MusicListViewController" bundle:nil];
                objMusic.isApple = self.isApple;
                objMusic.arrAppleMusic = objAppleAlbum.items;
                objMusic.appleMusicCollection = appleCollection;
                
                [self.navigationController pushViewController:objMusic animated:YES];
            }
        }
        else
        {
            MPMediaItemCollection *objAppleArtists = [arrApplePlaylist objectAtIndex:indexPath.row];
            
            if(objAppleArtists.items.count > 0)
            {
                MPMediaItem *objAppleSong = objAppleArtists.items[0];
                
                UIImage *albumArtworkImage = NULL;
                MPMediaItemArtwork *itemArtwork = objAppleSong.artwork;
                
                if (itemArtwork != nil)
                    albumArtworkImage = [itemArtwork imageWithSize:CGSizeMake(100, 100)];
                
                if (albumArtworkImage)
                    appDelegate.imgTempMusic = albumArtworkImage;
                else
                    appDelegate.imgTempMusic = [UIImage imageNamed:@"ic_music"];
                
                appDelegate.strPlaylistName = objAppleSong.artist;
                
                MPMediaItemCollection *appleCollection = [[MPMediaItemCollection alloc] initWithItems:objAppleArtists.items];
                
                MusicListViewController *objMusic = [[MusicListViewController alloc]initWithNibName:@"MusicListViewController" bundle:nil];
                objMusic.isApple = self.isApple;
                objMusic.arrAppleMusic = objAppleArtists.items;
                objMusic.appleMusicCollection = appleCollection;
                
                [self.navigationController pushViewController:objMusic animated:YES];
            }
        }
    }
    else
    {
        NSLog(@"follower count: %ld", objSTPUser.followerCount);
        MusicListViewController *objMusic = [[MusicListViewController alloc]initWithNibName:@"MusicListViewController" bundle:nil];
        objMusic.isApple = self.isApple;
        SPTPartialPlaylist *item = [arrSpotifyPlaylist objectAtIndex:indexPath.row];
        appDelegate.strPlaylistName = item.name;
        objMusic.URI = item.uri;
        objMusic.name = item.name;
        objMusic.imgURL = item.largestImage.imageURL;
        objMusic.lFollowerCount = appDelegate.lFollowerCount;
        [self.navigationController pushViewController:objMusic animated:YES];
    }
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnCancelSearch:(id)sender
{
    [appDelegate hideKeyboard];
    viewSearchBar.hidden = YES;
    btnSearch.hidden = NO;
    
    self.isApple ? [self getApplePlayList] : [self getSpotifyPlaylist];
}

- (IBAction)btnSearchClicked:(id)sender
{
    [txtSearch becomeFirstResponder];
    viewSearchBar.hidden = NO;
    btnSearch.hidden = YES;
}

-(IBAction)btnAppleCategoryCLK:(UIButton *)sender
{
    btnAlbums.selected = btnPlaylists.selected = btnArtists.selected = NO;
    switch (sender.tag)
    {
        case 0:
        {
            // Playlist click..
            btnPlaylists.selected = YES;
        }
            break;
        case 1:
        {
            // Album click..
            btnAlbums.selected = YES;
        }
            break;
        case 2:
        {
            // Artists click..
            btnArtists.selected = YES;
        }
            break;
            
        default:
            break;
    }
    
    [self getApplePlayList];
    
}
@end
