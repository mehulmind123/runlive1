//
//  UIViewController+CustomPermissions.h
//  Sellit
//
//  Created by mac-0004 on 05/12/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PermissionView.h"



@interface UIViewController (CustomPermissions)

+(void)displayCustomAlertWithType:(UIPrivacyAlertStyle)alertType;
-(void)displayCustomAlertWithType:(UIPrivacyAlertStyle)alertType;


-(void)displayCustomAlertWithType:(UIPrivacyAlertStyle)alertType withMessage:(NSString *)message withTitle:(NSString *)title;

@end
