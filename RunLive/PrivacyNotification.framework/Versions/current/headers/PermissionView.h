//
//  PermissionView.h
//  Sellit
//
//  Created by mac-0004 on 04/12/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GotItHandler)(void);
typedef void(^SettingsHandler)(void);

typedef NS_ENUM(NSInteger, UIPrivacyAlertStyle) {
    UIPrivacyAlertStyleNoFacebookAccount,
    UIPrivacyAlertStyleNoFacebookPermission,
    UIPrivacyAlertStyleNoTwitterAccount,
    UIPrivacyAlertStyleNoTwitterPermission,
    UIPrivacyAlertStyleNoLocationAccess,
    UIPrivacyAlertStyleNoContactsAccess,
    UIPrivacyAlertStyleNoCalendarsAccess,
    UIPrivacyAlertStyleNoRemindersAccess,
    UIPrivacyAlertStyleNoPhotosAccess,
    UIPrivacyAlertStyleNoBluetoothAccess,
    UIPrivacyAlertStyleNoMicrophoneAccess,
    UIPrivacyAlertStyleNoCameraAccess,
    UIPrivacyAlertStyleNoHealthAccess,
    UIPrivacyAlertStyleNoHomekitAccess,
};

@interface PermissionView : UIView <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrStepsDetail;
    NSMutableArray *arrStepsImage;
    
    IBOutlet UITableView *tblSteps;
    
}
-(void)GotItClicked:(GotItHandler)clicked;
-(void)SettingsClicked:(SettingsHandler)clicked;

+(id)loadViewFromXib;

@property(nonatomic,assign) UIPrivacyAlertStyle type;
@property(nonatomic,strong) NSString *appname;
@property(nonatomic,strong) IBOutlet UILabel *lblTitle;
@property(nonatomic,strong) IBOutlet UILabel *lblDescription;
@property(nonatomic,strong) IBOutlet UIButton *btnSettings;
@property(nonatomic,strong) IBOutlet UIButton *btnGotIt;
@end
