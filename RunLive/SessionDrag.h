//
//  SessionDrag.h
//  RunLive
//
//  Created by mac-00012 on 11/21/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionDrag : UIView

@property(weak,nonatomic) IBOutlet UIView *viewUser;

@property(weak,nonatomic) IBOutlet UIImageView *imgUser;
@property(weak,nonatomic) IBOutlet UILabel *lblName;
@property(weak,nonatomic) IBOutlet UILabel *lblCity;
@property(weak,nonatomic) IBOutlet UIImageView *imgVerifyUser;

- (void)setHighlightSelection:(BOOL)highlight;
-(void)addBorderColor:(BOOL)isRed;
@end
