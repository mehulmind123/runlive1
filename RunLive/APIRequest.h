//
//  APIRequest.h
//  AFNetworking iOS Example
//
//  Created by mac-0001 on 5/11/14.
//  Copyright (c) 2014 uVite LLC. All rights reserved.
//

// Staus/Response Constants
// Framework

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "UIImageView+AFNetworking.h"

#pragma mark - API Status
#pragma mark

#define CJsonStatus @"status"
#define CJsonMessage @"message"
#define CJsonTitle @"title"
#define CJsonData @"data"
#define CJsonResponce @"response"
#define CStatusZero @"0"
#define CStatusOne @"1"
#define CStatusNine @"9"
#define CStatusBlockUser @"8"
#define CVERSION @"v1"
#define CLimit @"20"


#pragma mark - API Tags
#pragma mark

#define CLOGIN                                                               @"login"
#define CSIGNUP                                                             @"signup"
#define CFORGOTPASSWORD                                         @"forgotpassword"
#define CRESENDEMAIL                                                  @"resendVerifyAccount"
#define CCHECKFORSOCAIL                                           @"checkLoginWithSocial"
#define CCONNECTSTRAVA                                           @"user/connectWithStrava"
#define CFACEBOOKUSERNAME                                           @"resendVerifyAccount"

#define CCHANGEPASSWORD                                         @"user/changePassword"
#define CUPDATEPROFILE                                               @"user/updateProfile"
#define CUPDATEPROFILEPICTURE                                 @"user/updateProfilePicture"
#define CUPLOADMEDIA                                                   @"activity/uploadMedia"

#define CABOUTUS                                                           @"cms/aboutUs"
#define CPROFILESTATUS                                               @"user/profileStatus"
#define CSUBSCRIPTIONPLAN                                         @"user/activeSubscription"
#define CSUBSCRIPTIONPLANRENEW                                         @"user/renewSubscription"
#define CSUBSCRIPTIONCHECKRECIEPTID                                         @"user/checkRecieptId"

#define CORDERTLIST                                                     @"order/list"
#define CORDERTDETAILS                                               @"order/orderDetail"
#define CPRODUCTLIST                                                    @"product/list"
#define CADDPRODUCT                                                     @"basket/add"
#define CREMOVEPRODUCT                                              @"basket/delete"
#define CBASKETLIST                                                      @"basket/list"
#define CRODUCTDETAILS                                              @"product/details"
#define CGETLASTADDRESS                                            @"order/address"
#define CGENERATEORDER                                              @"order/generate"
#define CBLOCKLIST                                                         @"block/list"
#define CBLOCKUSER                                                         @"block/user"
#define COTHERPROFILE                                                   @"user/profile"
#define CUNBLOCKUSER                                                    @"unBlock/user"
#define CFOLLOWUSER                                                     @"follow/user"
#define CUNFOLLOWUSER                                                @"unFollow/user"
#define CLOGOUTUSER                                                     @"logout/user"
#define CREGISTERDEVCIETOKEN                                    @"deviceToken/register"
#define CINVITEFRIEND                                                   @"friend/invite"
#define CLEADERBOARD                                                    @"leaderBoard/get"
#define CACTIVITYFEED                                                    @"activity/list"
#define CACTIVITYCOMMENTLIST                                  @"activityComment/list"


#define CSEARCHUSER                                                       @"user/searchUser"
#define CFRIENDLIST                                                        @"follow/list"
#define CLIVEPLAYERLIST                                                  @"session/player_list"
#define CCREATESESSION                                                @"session/create"
#define CCREATELIVESESSION                                         @"session/liveSessionCreation"
#define CQUITLIVESESSION                                           @"session/declinedLive"
#define CRSTARTSEARCHING                                           @"session/reStartSearch"
#define CREGISTERCHANNEL                                            @"chat/registerChannel"
#define CCHATSEND                                                           @"chat/send"
#define CCHATREAD                                                           @"chat/read"
#define CCHATMEMBER                                                      @"chat/member"
#define CDELETEUSERFROMCHANNEL                              @"chat/remove"
#define CBADGENOTIFICATIONBADGE                            @"user/userNotificationCount"
#define CCHATLIST                                                            @"chat/list"
#define CCHATIMAGE                                                         @"chat/upload"
#define CNOTIFICATIONLIST                                          @"notification/list"
#define CNOTIFICATIONREAD                                          @"notification/read"
#define CNOTIFICATIONREADUNCLICKABLE                   @"notification/readUnClicked"
#define CEDITSESSION                                                     @"session/edit"
#define CSESSIONLIST                                                     @"session/list"
#define CJOINSESSION                                                    @"session/join"
#define CADDPLAYERSESSION                                          @"session/addAnotherFriend"
#define CDECLINESESSION                                               @"session/declined"
#define CSYNCEDSESSION                                                 @"session/synced"
#define CDELETESESSION                                                  @"session/remove"
#define CADDCOMMENTONSESSION                                 @"sessionComment/create"
#define CDELETESESSIONCOMMENT                                 @"sessionComment/delete"
#define CDELETESEACTIVITYCOMMENT                                 @"activityComment/delete"
#define CADDLOCALITY                                                        @"session/userLocality"
#define CSESSIONFEEDBACK                                                        @"session/sessionFeedback"
#define CADDCOMMENTONACTIVITY                                 @"activityComment/create"
#define CLIKEACTIVITY                                                       @"activityLike/create"
#define CDELETEACTIVITY                                                       @"activity/deleteActivity"
#define CACTIVITYDETAILS                                                       @"activity/runActivityDetail"
#define CCOMMENTLISTOFSESSION                                 @"sessionComment/list"
#define CRUNNINGQUITCOMPLETE                                   @"session/quitRunning"
#define CRUNNINGRESULT                                                  @"session/sessionResult"

#define CCURRENTRUNNINGDETIAL                                   @"session/userRunningSessionDetail"
#define CUSERDISQUALIFY                                                 @"session/userDisqualify"
#define CTOSTARTMANNUALSESSION                              @"session/manualSessionStart"
#define CGETALLREADYRUNSESSION                                 @"session/findUserCurrentSession"










@interface APIRequest : NSObject

+ (id)request;

#pragma mark - LRF
#pragma mark

- (void)login:(NSString *)emailOrUsername andPassword:(NSString *)password andFacebookID:(NSString *)fb_id completed:(void (^)(id responseObject,NSError *error))completion;

- (void)signup:(NSString *)email andUserName:(NSString *)user_name andPassword:(NSString *)password andFacebookID:(NSString *)fb_id FirstName:(NSString *)first_name LastName:(NSString *)last_name Type:(NSString *)strSingUpType completed:(void (^)(id responseObject,NSError *error))completion;

-(void)checkSocailIds:(NSString *)social_id Email:(NSString *)strEmail Type:(NSString *)type completed:(void (^)(id responseObject,NSError *error))completion;

-(void)connectWithStrava:(NSString *)strStravaId completed:(void (^)(id responseObject,NSError *error))completion;

-(void)ForgotPWD:(NSString *)email completed:(void (^)(id responseObject,NSError *error))completion;

-(void)resendEmailVerification:(NSString *)email completed:(void (^)(id responseObject,NSError *error))completion;

-(void)changePWD:(NSString *)old_password NewPWD:(NSString *)new_password completed:(void (^)(id responseObject,NSError *error))completion;

-(void)updateProfile:(NSString *)first_name LastName:(NSString *)last_name DOB:(NSString *)date_of_birth Weight:(NSString *)weight Height:(NSString *)height Gender:(NSString *)gender Notification:(NSString *)notification Sound:(NSString *)sound Vibrate:(NSString *)vibrate Audio:(NSString *)audio Units:(NSString *)units ProfileVisibility:(NSString *)profile_visibility Subscription:(NSString *)subscription MuteVoiceChat:(NSString *)strChatType VoiceChatEnable:(NSString *)strVoiceChatEnable AboutUs:(NSString *)about_us Link:(NSString *)strLink completed:(void (^)(id responseObject,NSError *error))completion;

- (void)uploadUserProfilePicture:(UIImage*)image completed:(void (^)(id responseObject,NSError *error))completion;

-(void)aboutUs:(void (^)(id responseObject,NSError *error))completion;

-(void)updateProfileStatus:(void (^)(id responseObject,NSError *error))completion;

-(void)activeSubcription:(NSString *)type RecieptId:(NSString *)receipt_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)updateDateOnServerForIAP:(double)timeStamp completed:(void (^)(id responseObject,NSError *error))completion;

-(void)checkRecieptID:(NSString *)receipt_id completed:(void (^)(id responseObject,NSError *error))completion;


#pragma mark - Reward Related Functions
#pragma mark

-(void)orderDetailWithOrderID:(NSString *)product_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)getOrderList:(NSString *)offset completed:(void (^)(id responseObject,NSError *error))completion;

-(void)productList:(void (^)(id responseObject,NSError *error))completion;

-(void)basketList:(void (^)(id responseObject,NSError *error))completion;

-(void)addProductToBasket:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion;

-(void)removeProductFromBasket:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion;

-(NSURLSessionDataTask *)productDetails:(NSString *)productId completed:(void (^)(id responseObject,NSError *error))completion;

-(void)getLastAddress:(void (^)(id responseObject,NSError *error))completion;

-(void)GenerateOrder:(NSString *)name ForName:(NSString *)fore_name Street:(NSString *)street Number:(NSString *)number City:(NSString *)city PostCode:(NSString *)post_code Country:(NSString *)country PhoneNumber:(NSString *)phone_number State:(NSString *)stateprovinceRegion completed:(void (^)(id responseObject,NSError *error))completion;





#pragma mark - User Related APi
#pragma mark

-(void)BlockList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion;

-(void)addBlockUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)unblockUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)followUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)unfollowUser:(NSString *)user_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)otherUserProfile:(NSString *)user_name showLoader:(BOOL)showLoader completed:(void (^)(id responseObject,NSError *error))completion;


#pragma mark - Activity Feed
#pragma mark

-(NSURLSessionDataTask *)GetActivityFeedList:(NSNumber *)offset Type:(NSString *)type UserName:(NSString *)user_name completed:(void (^)(id responseObject,NSError *error))completion;

-(void)GetActivityCommentList:(NSNumber *)offset ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion;


-(void)addCommentOnActivity:(NSString *)text ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)likeActivityFeed:(NSString *)activity_id Status:(NSString *)status ActivityType:(NSString *)strActType completed:(void (^)(id responseObject,NSError *error))completion;

-(void)deleteActivity:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)activityDetailWithType:(NSString *)type ActivityId:(NSString *)activity_id completed:(void (^)(id responseObject,NSError *error))completion;




#pragma mark - LeaderBoard
#pragma mark

-(NSURLSessionDataTask *)GetLeaderboardList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion;

-(void)syncContactWithServer:(NSArray *)arrContact FromFacebook:(BOOL)isFacebook completed:(void (^)(id responseObject,NSError *error))completion;

-(NSURLSessionDataTask *)serachUser:(NSNumber *)offset KeyWord:(NSString *)searchKeyword completed:(void (^)(id responseObject,NSError *error))completion;


#pragma mark - Chat related Api
#pragma mark

-(void)RegisterChannel:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;

-(void)MessageSend:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)ReadMessage:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion;

-(NSURLSessionDataTask *)ChannelList:(BOOL)isFirstTime completed:(void (^)(id responseObject,NSError *error))completion;

-(void)ChannelDetail:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)UploadChatImage:(UIImage *)image completed:(void (^)(id responseObject,NSError *error))completion;

-(void)ChatMember:(NSString *)channel_id UserId:(NSArray *)user_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)deleteUserFromChannel:(NSString *)channel_id completed:(void (^)(id responseObject,NSError *error))completion;

#pragma mark - Notification Related
#pragma mark

-(void)checkForNotificationBadge:(void (^)(id responseObject, NSError *error))completion;

-(void)registerDeviceTokenOnserver:(NSString *)device_token completed:(void (^)(id responseObject,NSError *error))completion;

-(void)removeDeviceTokenFromServer:(void (^)(id responseObject,NSError *error))completion;

-(NSURLSessionDataTask *)NotificationList:(NSNumber *)offset completed:(void (^)(id responseObject,NSError *error))completion;

-(void)readNotification:(NSString *)notification_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)readUnClickableNotification:(void (^)(id responseObject,NSError *error))completion;

#pragma mark - Session related Api
#pragma mark

-(NSURLSessionDataTask *)friendList:(NSString *)keyword loader:(BOOL)shouldShowLoader completed:(void (^)(id responseObject,NSError *error))completion;

-(void)createLiveSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;

-(void)quitLiveSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)restartSearching:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)createSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;

-(void)addFriendInSession:(NSString *)session_id Friends:(NSArray *)user_ids completed:(void (^)(id responseObject,NSError *error))completion;

-(void)EditSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion;

-(void)GETSessionList:(void (^)(id responseObject,NSError *error))completion;

-(void)GETSessionDetail:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)JoinSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)DeclineSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)SyncedSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)DeleteSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)addCommentOnSession:(NSString *)text SessionId:(NSString *)session_id UserName:(NSArray *)user_names completed:(void (^)(id responseObject,NSError *error))completion;

-(void)deleteSessionComment:(NSString *)comment_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)deleteActivityComment:(NSString *)comment_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)getCommentListOfSession:(NSString *)session_id Offset:(NSString *)offset completed:(void (^)(id responseObject,NSError *error))completion;

-(void)uploadPhotoAndVideo:(NSString *)type SessionID:(NSString *)session_id Attachment:(NSData *)attachment Caption:(NSString *)caption Latitude:(double)latitude Longitude:(double)longitude completed:(void (^)(id responseObject,NSError *error))completion;

-(void)addLocalityAndSubLocality:(NSString *)locality SubLocality:(NSString *)sub_locality SessionId:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)giveSessionFeedback:(NSString *)session_id Message:(NSString *)message SessionType:(NSString *)session_type Image:(UIImage *)imgAttachment completed:(void (^)(id responseObject,NSError *error))completion;

#pragma mark - Running Related api

-(NSURLSessionDataTask *)getAllreadyRunningSeesion:(void (^)(id responseObject,NSError *error))completion;

-(void)userCurrentRunningDetail:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)userDisqualifyFromRunning:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)toStartManualSessionRunning:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;

-(void)competeQuitRunning:(NSString *)session_id Type:(NSString *)type RunCompleteType:(NSString *)run_completed_type completed:(void (^)(id responseObject,NSError *error))completion;

-(void)runningResultOfSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion;


#pragma mark - Core Data Related Functions

-(void)updateActivityDataInLocal:(NSString *)strActType ActivityId:(NSString *)activity_id UpdateData:(NSDictionary *)dicActivity;

@end


