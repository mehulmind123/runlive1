//
//  TrackOrderViewController.m
//  RunLive
//
//  Created by mac-00012 on 5/31/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TrackOrderViewController.h"

@interface TrackOrderViewController ()

@end

@implementation TrackOrderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.google.co.in/search?q=%@",self.sttTrackId]]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action Event


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad ================ >>> ");
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad ================ >>> ");
    
    activityIndicator.hidden = YES;
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError ================ >>> ");
    
    activityIndicator.hidden = YES;
    [activityIndicator stopAnimating];
}


#pragma mark - Action Event

-(IBAction)btnBackCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}





@end
