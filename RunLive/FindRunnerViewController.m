//
//  FindRunnerViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "FindRunnerViewController.h"
#import "OpponentsViewController.h"
#import "PulsationView.h"

@interface FindRunnerViewController ()

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *usersIcons;
@property (nonatomic, strong) PulsationView *pulsationView;

@end

@implementation FindRunnerViewController
{
    int seconds;
    NSTimer *timer;
    BOOL isFirstTimeLoad,isRestartSearching,isSearchingDone;
    NSMutableArray *arrFindUser;
    NSString *strMyTeamType;
    AVSpeechSynthesizer *speechSynthesizer;
    
    BOOL isPlaySoundNotification;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    speechSynthesizer.delegate = self;
    
    isFirstTimeLoad = YES;
    
    self.view.layer.masksToBounds = YES;
    self.view.clipsToBounds = YES;
    
    lblNotice.text = CMessageRunningNotice;
    viewQuit.layer.cornerRadius = btnYes.layer.cornerRadius = btnNoWay.layer.cornerRadius = 3;
    viewRunnerNotice.layer.cornerRadius = btnGotIt.layer.cornerRadius = 3;
    
    viewRunnerNoticeContainer.hidden = appDelegate.loginUser.session_notice_checkbox.boolValue;
    
    arrFindUser = [NSMutableArray new];
    [self setupForMapView];
    
    [imgLoginUser setImageWithURL:[appDelegate resizeImage:[NSString stringWithFormat:@"%f",CViewWidth(imgLoginUser)*2] Height:nil imageURL:appDelegate.loginUser.picture] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
 

    // Resubcribe if MQTT connection was break from server............
    appDelegate.configureResubscribeOnMQTT = ^(BOOL isSubscribe)
    {
        if (appDelegate.objMQTTClient.connected && [[appDelegate getTopMostViewController] isKindOfClass:[FindRunnerViewController class]] && self.strSessoinId)
        {
                // Subscribe Session On MQTT
                [appDelegate MQTTSubscribeWithTopic:self.strSessoinId];
        }
    };
    
    [self setupPulsationView];
    
    
    [UIApplication applicationWillEnterForeground:^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[FindRunnerViewController class]] && self.pulsationView)
        {
            [self.pulsationView start];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    [appDelegate hideTabBar];
    
    isPlaySoundNotification = YES;
    
    [self addMQTTNotificationObserver];
    
    viewQuitContainer.hidden = YES;
    isRestartSearching = YES;
    btnQuite.userInteractionEnabled = YES;
    
    [appDelegate keepAppInIdleMode:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [appDelegate keepAppInIdleMode:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (isFirstTimeLoad)
    {
        [self designGalaxyview];
        isFirstTimeLoad = NO;
        [self getFindPlayerFromServer:self.dicSessionPlayer];
    }
    
    [UIView animateWithDuration:1.2 animations:^{
      self.pulsationView.alpha = 1;
    }];
    [self.pulsationView start];
}


#pragma mark - MQTT Functions

-(void)addMQTTNotificationObserver
{
    isSearchingDone = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MQTTSearchPlayer:) name:CMQTTSearchingPlayer object:nil];
    });
}

-(void)removeMQTTNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CMQTTSearchingPlayer object:nil];
}


-(void)MQTTSearchPlayer:(NSNotification *)notification
{
    NSDictionary *dicMQTT = [notification userInfo];
    
    if(![dicMQTT isKindOfClass:[NSDictionary class]] || ![dicMQTT objectForKey:@"invitation"])
        return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.dicSessionPlayer = nil;
        self.dicSessionPlayer = dicMQTT;
        
        if ([[appDelegate getTopMostViewController] isKindOfClass:[FindRunnerViewController class]])
        {
            NSLog(@"Find Runner view controller============>>>>>>>>>> %@",dicMQTT);
            
            if (isSearchingDone)
                return ;
            
            // Searching completed and found suffiecent player for Running...
            if ([[dicMQTT numberForJson:@"session_status"] isEqual:@1])
            {
                isSearchingDone = YES;
                if ([self checkUserKickOutFromLobby:dicMQTT])
                {
                    // If Login user Kicked out from loby then create new lobby here
                    
                    [appDelegate MQTTUnsubscribeWithTopic:self.strSessoinId];
                    
                    [self StopTimer];
                    [appDelegate createLiveSession:self.dicSessionData completed:^(id responseObject, NSError *error)
                     {
                         if (responseObject && !error)
                         {
                             isSearchingDone = NO;
                             [self StopTimer];
                             isPlaySoundNotification = YES;
                             seconds = 30;
                             timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTick:) userInfo:nil repeats:YES];
                             
                             NSDictionary *dicRes = [responseObject valueForKey:CJsonData];
                             self.strSessoinId = [dicRes stringValueForJSON:@"_id"];
                             self.dicSessionPlayer = dicRes;
                             [self getFindPlayerFromServer:dicRes];
                         }
                         else
                             [self moveUserToBackScreenForcefully];
                     }];
                }
                else
                {
                    // Move on Opponent screen here...................
                    [self searchingFinishedMoveOnNextScreen:dicMQTT];
                }
            }
            else if ([[dicMQTT numberForJson:@"session_status"] isEqual:@2] && [dicMQTT objectForKey:@"session_status"])
            {
                [self checkForSuggestedSession:dicMQTT];
            }
            else
            {
                [self getFindPlayerFromServer:dicMQTT];
            }
        }
    });
}

#pragma mark - Galaxy Player set up

-(BOOL)checkUserKickOutFromLobby:(NSDictionary *)dicUsers
{
    BOOL isKickOut = false;
    
    NSMutableArray *arrTempUser = [NSMutableArray new];
    NSDictionary *dicInvite = [dicUsers valueForKey:@"invitation"];
    
    if ([[dicUsers stringValueForJSON:@"run_type"] isEqualToString:@"solo"])
        [arrTempUser addObjectsFromArray:[dicInvite valueForKey:@"solo"]];
    else
    {
        [arrTempUser addObjectsFromArray:[dicInvite valueForKey:@"team_red"]];
        [arrTempUser addObjectsFromArray:[dicInvite valueForKey:@"team_blue"]];
    }
    
    NSArray *arrLogin = [arrTempUser filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    
    isKickOut = arrLogin.count > 0 ? NO : YES;
    
    return isKickOut;
}

-(void)getFindPlayerFromServer:(NSDictionary *)dicUsers
{
    [arrFindUser removeAllObjects];
    
    NSDictionary *dicInvite = [dicUsers valueForKey:@"invitation"];
    if ([[dicUsers stringValueForJSON:@"run_type"] isEqualToString:@"solo"])
    {
        [arrFindUser addObjectsFromArray:[dicInvite valueForKey:@"solo"]];
        strMyTeamType = @"solo";
    }
    else
    {
        if ([[[NSMutableArray alloc] initWithArray:[dicInvite valueForKey:@"team_red"]] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]].count > 0)
            strMyTeamType = @"team_red";

        if ([[[NSMutableArray alloc] initWithArray:[dicInvite valueForKey:@"team_blue"]] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]].count > 0)
            strMyTeamType = @"team_blue";

        [arrFindUser addObjectsFromArray:[dicInvite valueForKey:@"team_red"]];
        [arrFindUser addObjectsFromArray:[dicInvite valueForKey:@"team_blue"]];
    }
    
    [self calculateDistannceBetweenUser];
}

-(NSArray *)sortRunnerAccordingToTotalDistance:(NSArray *)arrUserPossition Accending:(BOOL)isAcceding
{
    NSArray *sorters = @[[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:isAcceding comparator:^(id obj1, id obj2) {
        NSNumber *n1;
        NSNumber *n2;
        // Either use obj1/2 as numbers or try to convert them to numbers
        if ([obj1 isKindOfClass:[NSNumber class]]) {
            n1 = obj1;
        } else {
            n1 = @([[NSString stringWithFormat:@"%@", obj1] doubleValue]);
        }
        if ([obj2 isKindOfClass:[NSNumber class]]) {
            n2 = obj2;
        } else {
            n2 = @([[NSString stringWithFormat:@"%@", obj2] doubleValue]);
        }
        return [n1 compare:n2];
    }]];
    
    NSArray *sortedArray = [arrUserPossition sortedArrayUsingDescriptors:sorters];
    
    return sortedArray;
}

-(void)calculateDistannceBetweenUser
{
    NSArray *arrLogin = [arrFindUser filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", appDelegate.loginUser.user_id]];
    
    double loginUserLat = 0;
    double loginUserLong = 0;
    
    if (arrLogin.count > 0)
    {
        NSDictionary *dicLoginUser = arrLogin[0];
        loginUserLat = [dicLoginUser stringValueForJSON:@"latitude"].doubleValue;
        loginUserLong = [dicLoginUser stringValueForJSON:@"longitude"].doubleValue;
        
        // Remove login user from array...
        if ([arrFindUser containsObject:arrLogin[0]])
            [arrFindUser removeObject:arrLogin[0]];
    }

    NSMutableArray *arrTempData = [NSMutableArray new];
    
    for (int i = 0; arrFindUser.count>i; i++)
    {
        NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:arrFindUser[i]];
        double otherUserLat = [dicData stringValueForJSON:@"latitude"].doubleValue;
        double otherUserLong = [dicData stringValueForJSON:@"longitude"].doubleValue;
        
        CLLocation *locA1 = [[CLLocation alloc] initWithLatitude:loginUserLat longitude:loginUserLong];
        CLLocation *locB1 = [[CLLocation alloc] initWithLatitude:otherUserLat longitude:otherUserLong];
        CLLocationDistance distance1 = [locA1 distanceFromLocation:locB1];
        
        [dicData setObject:[NSString stringWithFormat:@"%.2f",distance1] forKey:@"distance"];
        [arrTempData addObject:dicData];
    }
    
    NSArray *sortedArray = [self sortRunnerAccordingToTotalDistance:arrTempData Accending:YES];
    [arrFindUser removeAllObjects];
    [arrFindUser addObjectsFromArray:sortedArray];
    
    [arrTempData removeAllObjects];
    
    // To play sound notification...
    if (arrFindUser.count == 0)
        isPlaySoundNotification = YES;
    
    [self getFindUserFromServer:(int)arrFindUser.count];
}



#pragma mark - Searching Finished

-(void)searchingFinishedMoveOnNextScreen:(NSDictionary *)dicFinalData
{
    [self forcefullyRemoveCustomAlertFromSuperView];
    [self removeMQTTNotificationObserver];
    
    [self StopTimer];
    btnQuite.userInteractionEnabled = NO;
    lblTitle.text = self.isSoloFind ? @"Runners Found, Creating Match... Please Wait" : @"Runners Found, Creating Teams... Please Wait";
    
    isPlaySoundNotification = YES;
    [self playInstructionSound:self.isSoloFind ? @"Creating match. Your race will begin in 10 seconds" : @"Creating Teams. Your race will begin in 10 seconds"];
  NSTimeInterval interval = 0.0;
  for (UIImageView *userPick in self.usersIcons) {
    if (!userPick.hidden) {
      [UIView animateWithDuration:1.5
                            delay:interval
                          options:UIViewAnimationOptionCurveLinear
                       animations:^{
        userPick.transform = CGAffineTransformMakeScale(.3, .3);
        userPick.alpha = 0;
      }
                       completion:NULL];
      interval += 0.2;
    }
  }
  
  [UIView animateWithDuration:interval + 2.0
                   animations:^{
    self.pulsationView.alpha = 0;
  }
                   completion:^(BOOL finished) {
    OpponentsViewController *objOpp = [[OpponentsViewController alloc] init];
    objOpp.isSolo = _isSoloFind;
    objOpp.strSessionIDFromSearch = self.strSessoinId;
    objOpp.dicDataCreateSesionFromFind = self.dicSessionData;
    objOpp.dicSesionPlayerFromFind = dicFinalData;
    
    // If Player Quit from Opponent screen and player count is less for Runnig session then create new lobby here......
    objOpp.configureRecreateSession = ^(NSString *strSessionId,NSDictionary *dicRes)
    {
      [self StopTimer];
      
      isPlaySoundNotification = YES;
      seconds = 30;
      timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTick:) userInfo:nil repeats:YES];
      
      self.strSessoinId = strSessionId;
      self.dicSessionPlayer = dicRes;
      [self getFindPlayerFromServer:dicRes];
    };
                       
    [self.navigationController pushViewController:objOpp animated:YES];
  }];
}

#pragma mark - Quit Session
-(void)QuitRunSession
{
    if (!self.strSessoinId)
        return;
    
    [self StopTimer];
    [self removeMQTTNotificationObserver];

    [appDelegate MQTTUnsubscribeWithTopic:self.strSessoinId];
    [appDelegate QuitJoinedLiveRunSession:self.strSessoinId completed:^(id responseObject, NSError *error)
    {
        [mapView clearMGLMapView];       
        [mapView removeFromSuperview] ;
        mapView = nil ;
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
}


#pragma mark - General Functions

- (void)setupPulsationView
{
    if (self.pulsationView)
        return;
    
  CGRect pulsatingFrame = CGRectMake(0,
                                     0,
                                     [UIScreen mainScreen].bounds.size.width,
                                     [UIScreen mainScreen].bounds.size.height);
  self.pulsationView = [[PulsationView alloc] initWithFrame:pulsatingFrame];
  self.pulsationView.backgroundColor = [UIColor clearColor];
  self.pulsationView.alpha = 0;
  [self.view insertSubview:self.pulsationView aboveSubview:imgBackground];
}

-(void)setupForMapView
{
    [mapView clearMGLMapView];
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    mapView.showsUserLocation = NO;
    [mapView setCenterCoordinate:CLLocationCoordinate2DMake([CUserDefaults doubleForKey:CCurrentLatitude], [CUserDefaults doubleForKey:CCurrentLongitude]) zoomLevel:1 animated:NO];
}

-(void)designGalaxyview {
    isPlaySoundNotification = YES;
    seconds = 30;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTick:) userInfo:nil repeats:YES];

    viewUser.hidden = NO;
    viewUser.backgroundColor = [UIColor clearColor];
    viewUser.layer.cornerRadius = CGRectGetHeight(viewUser.frame)/2;
    viewUser.layer.borderWidth = 7;
    imgLoginUser.layer.cornerRadius = CGRectGetHeight(imgLoginUser.frame)/2;
    viewUser.layer.borderColor = CRGB(70, 216, 252).CGColor;

    for (UIView *objView in self.view.subviews)
    {
        if ([objView isKindOfClass:[UIImageView class]])
        {
            UIImageView *img = (UIImageView *)objView;
            
            if (![img isEqual:imgBackground])
            {
                img.layer.cornerRadius = CGRectGetHeight(img.frame)/2;
                img.layer.borderWidth = 2;
                img.layer.borderColor = [UIColor whiteColor].CGColor;
            }
        }
    }
}

-(void)getFindUserFromServer:(int)intUser {
    
    lblTitle.text = intUser > 0 ? @"Please Wait, Finding More Runners" : @"FINDING RUNNERS";

    if (intUser > 0)
        [self playInstructionSound:@"Runners Found. Please Wait, Finding more runners."];
  
  NSString *strImageName = @"picture";
  
  __weak __typeof(self)weakSelf = self;
  if (arrFindUser.count == 0) {
    [self resetUsersImageViews];
  } else
  {
      
    [arrFindUser enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull userDict, NSUInteger idx, BOOL * _Nonnull stop) {
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * idx * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
          [weakSelf imageAnimationView:weakSelf.usersIcons[idx]
                            ImageUrl:[userDict stringValueForJSON:strImageName]];
        if (idx == arrFindUser.count - 1)
        {
          for (int i = (int)arrFindUser.count; weakSelf.usersIcons.count > i; i++)
          {
            ((UIImageView*)weakSelf.usersIcons[i]).image = NULL;
            ((UIImageView*)weakSelf.usersIcons[i]).hidden = YES;
          }
        }
      });
    }];
  }
}

- (void)resetUsersImageViews
{
  for (UIImageView *userImage in self.usersIcons)
  {
    userImage.image = NULL;
    userImage.hidden = YES;
  }
}

- (void)imageAnimationView:(UIImageView *)img ImageUrl:(NSString *)imgUrl
{
  [img setImageWithURL:[appDelegate resizeImage:[NSString stringWithFormat:@"%f",CViewWidth(img)*2] Height:nil imageURL:imgUrl] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
  
  if (img.hidden) {
    img.transform = CGAffineTransformMakeScale(.3, .3);
    img.alpha = 0;
    img.hidden = NO;
    [UIView animateWithDuration:1.5 animations:^{
      img.transform = CGAffineTransformMakeScale(1, 1);
      img.alpha = 1;
    }];
  }
}

#pragma mark - AVSpeechSynthesizer Method
-(void)playInstructionSound:(NSString *)strVoiceSpeech
{
    if (!isPlaySoundNotification)
    {
        //NSLog(@"Not allow to play sound notifications ======== >>> ");
        return;
    }
    AVSpeechUtterance *speechUtterance = [AVSpeechUtterance speechUtteranceWithString:strVoiceSpeech];
    speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate;
    speechUtterance.pitchMultiplier = 1;
    speechUtterance.volume = 1;
    [speechSynthesizer speakUtterance:speechUtterance];
    
    isPlaySoundNotification = NO;

}

#pragma mark - AVSpeechSynthesizer Delegate Method

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    NSLog(@"speechSynthesizer ======== >> ");
}


#pragma mark - Timer Functions
-(void)StopTimer
{
    [timer invalidate];
    timer = nil;
}

-(void)timeTick:(NSTimer *)timer1
{
    // Cheking user availibility...
    if (seconds > -1)
    {
        NSMutableDictionary *dicData = [NSMutableDictionary new];
        [dicData setObject:@"9" forKey:@"user_available_status"];
        [dicData setObject:appDelegate.loginUser.user_id forKey:@"user_id"];
        [dicData setObject:appDelegate.loginUser.user_name forKey:@"User_name"];
        [dicData setObject:strMyTeamType  forKey:@"type"];
        [dicData setObject:self.strSessoinId forKey:@"session_id"];
        [dicData setObject:[NSString stringWithFormat:@"%d",seconds] forKey:@"time_checking_find_screen"];
        [appDelegate MQTTNotifyToServerForUserAvailability:dicData Topic:self.strSessoinId];
    }
    
    seconds--;
}

#pragma mark - Searching related functions

-(void)checkForSuggestedSession:(NSDictionary *)dicSuggested
{
    [self StopTimer];
    [appDelegate MQTTUnsubscribeWithTopic:self.strSessoinId];
    [appDelegate QuitJoinedLiveRunSession:self.strSessoinId completed:^(id responseObject, NSError *error)
     {
         if ([dicSuggested stringValueForJSON:@"distance"].integerValue > 0)
         {
             int oldDistance = [self.dicSessionData stringValueForJSON:@"distance"].intValue / 1000;
             int newDistance = [dicSuggested stringValueForJSON:@"distance"].intValue / 1000;
             
             [self customAlertViewWithThreeButton:@"" Message:[NSString stringWithFormat:@"We can't find anyone who wants to run %dK distance right now, but there are users searching in the %dK. Would you like to join %dK race instead?",oldDistance,newDistance,newDistance] ButtonFirstText:@"Yes" ButtonSecondText:[NSString stringWithFormat:@"Continue searching for %dkm",oldDistance] ButtonThirdText:@"Cancel" Animation:NO completed:^(int index) {
                 
                 switch (index) {
                     case 0:
                     {
                         // New distance...
                         [self.dicSessionData setObject:[dicSuggested stringValueForJSON:@"distance"] forKey:@"distance"];
                         
                         // Create session for suggested distance....
                         [self searchingFromSuggestedSessionFlow:self.dicSessionData SuggestedSession:YES];
                     }
                         break;
                     case 1:
                     {
                         // Go with Old distance...and call api here..
                         [self searchingFromSuggestedSessionFlow:self.dicSessionData SuggestedSession:NO];
                     }
                         break;
                     case 2:
                     {
                         // Move back...
                         [self moveUserToBackScreenForcefully];
                     }
                         break;
                         
                     default:
                         break;
                 }
                 
             }];
         }
         else
         {
             // session not found
             [self customAlertViewWithTwoButton:@"" Message:CMessageSearchingSession ButtonFirstText:@"Continue" ButtonSecondText:@"Find new match" Animation:YES completed:^(int index) {
                 if (index == 0)
                     [self searchingFromSuggestedSessionFlow:self.dicSessionData SuggestedSession:NO];
                 else
                     [self moveUserToBackScreenForcefully];
             }];
         }
     }];
}

-(void)searchingFromSuggestedSessionFlow:(NSDictionary *)dicSession SuggestedSession:(BOOL)isSuggested
{
    [appDelegate createLiveSession:dicSession completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSDictionary *dicRes = [responseObject valueForKey:CJsonData];
             self.strSessoinId = [dicRes stringValueForJSON:@"_id"];
             self.dicSessionPlayer = dicRes;
             
             if (isSuggested)
             {
                 // check player count
                 NSMutableArray *arrPlayers = [NSMutableArray new];
                 NSDictionary *dicInvite = [dicRes valueForKey:@"invitation"];
                 if ([[dicRes stringValueForJSON:@"run_type"] isEqualToString:@"solo"])
                     [arrPlayers addObjectsFromArray:[dicInvite valueForKey:@"solo"]];
                 else
                 {
                     [arrPlayers addObjectsFromArray:[dicInvite valueForKey:@"team_red"]];
                     [arrPlayers addObjectsFromArray:[dicInvite valueForKey:@"team_blue"]];
                 }
                 
                 if (arrPlayers.count < 2)
                     lblTitle.text = @"Session Full, Finding New Session.";
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     // Set same text after getting MQTT Reponse
                     lblTitle.text = lblTitle.text;
                 });
             }
             
             [self StopTimer];
             isPlaySoundNotification = YES;
             seconds = 30;
             timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeTick:) userInfo:nil repeats:YES];
         }
         else
         {
             [self moveUserToBackScreenForcefully];
         }
     }];
    
}

#pragma mark - Screen movement related functions
-(void)moveUserToBackScreenForcefully
{
    [self forcefullyRemoveCustomAlertFromSuperView];
    [self StopTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Action Event

-(IBAction)btnQuitCLK:(id)sender
{
    viewQuitContainer.hidden = NO;
    
    [btnNoWay touchUpInsideClicked:^{
        viewQuitContainer.hidden = YES;
    }];
    
    [btnYes touchUpInsideClicked:^{
        [self QuitRunSession];
    }];
    
}

-(IBAction)btnGotItCLK:(id)sender
{
    if (btnNoticeCheckBox.selected)
    {
        appDelegate.loginUser.session_notice_checkbox = @YES;
        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
    viewRunnerNoticeContainer.hidden = YES;
}

-(IBAction)btnNoticeCheckBoxCLK:(UIButton *)sender
{
    btnNoticeCheckBox.selected = !btnNoticeCheckBox.selected;
}


@end
