//
//  PhotoDetailsViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/14/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "PhotoDetailsViewController.h"
#import "ActivityAddPhotoCell.h"
#import "CommentsViewController.h"



@interface PhotoDetailsViewController ()

@end

@implementation PhotoDetailsViewController
{
    NSArray *arrActivity;
    ActivityAddPhotoCell *cellActivityVideo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tblPhoto registerNib:[UINib nibWithNibName:@"ActivityAddPhotoCell" bundle:nil] forCellReuseIdentifier:@"ActivityAddPhotoCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getActivityDetailFromServer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [tblPhoto reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    // Stop Video when class is disapear...
    [self stopAllreadyPlayedVideo];
}

#pragma mark - Api Functions

-(void)getActivityDetailFromServer
{
    if (!self.strActivityType)
        return;
    
    if (!self.strActivityID)
        return;
    
    // Competed session details only ,..............
    [[APIRequest request] activityDetailWithType:self.strActivityType ActivityId:self.strActivityID completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             // Do your stuff here...
             NSDictionary *dicActivity = [responseObject valueForKey:CJsonData];
             
//             NSLog(@"%@",dicData);
             
             NSDate *date = [self convertDateFromString:[dicActivity stringValueForJSON:@"createdAt"] isGMT:YES formate:CDateFormater];
             double activityTime = [date timeIntervalSince1970];
             
             NSDateFormatter *dtForm = [NSDateFormatter initWithDateFormat:@"MMMM yyyy"];
             NSString *strDate = [dtForm stringFromDate:date];
             
             dtForm.dateFormat = @"MMMM";
             NSString *strShowDate = [dtForm stringFromDate:date];;
             strShowDate = strShowDate.uppercaseString;
             
             // Store Activity date here....
//             TblActivityDates *objActivityDate = (TblActivityDates *)[TblActivityDates findOrCreate:@{@"activity_date" : strDate}];
             
             TblActivityDates *objActivityDate = (TblActivityDates *)[TblActivityDates findOrCreate:@{@"activity_date" : strDate,@"user_name":[dicActivity stringValueForJSON:@"user_name"]}];

             objActivityDate.activity_date = strDate;
             objActivityDate.show_date = strShowDate;
             objActivityDate.timestamp = [NSNumber numberWithDouble:activityTime];
             objActivityDate.user_id = [dicActivity stringValueForJSON:@"user_id"];
             objActivityDate.user_name = [dicActivity stringValueForJSON:@"user_name"];
             objActivityDate.activity_filter_type = @0;
             objActivityDate.activity_type = [dicActivity numberForJson:@"type"];
             objActivityDate.total_month_run = [dicActivity stringValueForJSON:@"total_month_run"];
             objActivityDate.total_month_distance = [dicActivity stringValueForJSON:@"total_month_distance"];

             
             [[Store sharedInstance].mainManagedObjectContext save];

             if (self.strActivityType.integerValue == 4)
                 [self storePhotoActivityToLocal:objActivityDate Data:dicActivity];
             else
                 [self storeVideoActivityToLocal:objActivityDate Data:dicActivity];
         }
     }];
}

-(void)storePhotoActivityToLocal:(TblActivityDates *)objActivityDate Data:(NSDictionary *)dicActivity
{
    // Add Photo Activity....
    
    TblActivityPhoto *objActivityPhoto = (TblActivityPhoto *)[TblActivityPhoto findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
    objActivityPhoto.objActivityDate = objActivityDate;
    objActivityPhoto.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
    objActivityPhoto.activity_id = [dicActivity stringValueForJSON:@"_id"];
    objActivityPhoto.user_id = [dicActivity stringValueForJSON:@"user_id"];
    objActivityPhoto.user_image = [dicActivity stringValueForJSON:@"picture"];
    objActivityPhoto.user_name = [dicActivity stringValueForJSON:@"user_name"];
    objActivityPhoto.activity_main_type = [dicActivity stringValueForJSON:@"type"];
    objActivityPhoto.session_name = [dicActivity stringValueForJSON:@"session_name"];
    objActivityPhoto.session_id = [dicActivity stringValueForJSON:@"session_id"];
    objActivityPhoto.img_width = [dicActivity stringValueForJSON:@"width"];
    objActivityPhoto.img_hieght = [dicActivity stringValueForJSON:@"height"];
    objActivityPhoto.timestamp = objActivityDate.timestamp;
    objActivityPhoto.comments = [dicActivity valueForKey:@"comments"];;
    objActivityPhoto.act_image = [dicActivity stringValueForJSON:@"attachment"];
    objActivityPhoto.caption = [dicActivity stringValueForJSON:@"caption"];
    objActivityPhoto.total_likes = [dicActivity stringValueForJSON:@"like_count"];
    objActivityPhoto.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
    objActivityPhoto.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
    objActivityPhoto.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
    objActivityPhoto.activity_text = [dicActivity stringValueForJSON:@"title_text"];
    
    [[Store sharedInstance].mainManagedObjectContext save];
    
    lblTitle.text = [NSString stringWithFormat:@"%@'S PHOTO",objActivityPhoto.user_name].uppercaseString;
    
    arrActivity = [TblActivityPhoto fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityPhoto.activity_id] sortedBy:nil];
    [tblPhoto reloadData];

}

-(void)storeVideoActivityToLocal:(TblActivityDates *)objActivityDate Data:(NSDictionary *)dicActivity{

    TblActivityVideo *objActivityVideo = (TblActivityVideo *)[TblActivityVideo findOrCreate:@{@"user_id" : [dicActivity stringValueForJSON:@"user_id"],@"activity_id" : [dicActivity stringValueForJSON:@"_id"], @"objActivityDate" : objActivityDate}];
    objActivityVideo.objActivityDate = objActivityDate;
    objActivityVideo.activity_post_time = [dicActivity stringValueForJSON:@"createdAt"];
    objActivityVideo.activity_id = [dicActivity stringValueForJSON:@"_id"];
    objActivityVideo.user_id = [dicActivity stringValueForJSON:@"user_id"];
    objActivityVideo.activity_main_type = [dicActivity stringValueForJSON:@"type"];
    objActivityVideo.timestamp = objActivityDate.timestamp;
    objActivityVideo.comments = [dicActivity valueForKey:@"comments"];
    
    objActivityVideo.user_image = [dicActivity stringValueForJSON:@"picture"];
    objActivityVideo.user_name = [dicActivity stringValueForJSON:@"user_name"];
    objActivityVideo.session_name = [dicActivity stringValueForJSON:@"session_name"];
    objActivityVideo.session_id = [dicActivity stringValueForJSON:@"session_id"];
    
    objActivityVideo.video_width = [dicActivity stringValueForJSON:@"width"];
    objActivityVideo.video_height = [dicActivity stringValueForJSON:@"height"];
    objActivityVideo.video_url = [dicActivity stringValueForJSON:@"attachment"];
    objActivityVideo.thumb_image = [dicActivity stringValueForJSON:@"video_thumb"];
    objActivityVideo.caption = [dicActivity stringValueForJSON:@"caption"];
    objActivityVideo.total_likes = [dicActivity stringValueForJSON:@"like_count"];
    objActivityVideo.total_comments = [dicActivity stringValueForJSON:@"comment_count"];
    objActivityVideo.is_like = [[dicActivity numberForJson:@"is_like"] isEqual:@1] ? @YES : @NO;
    objActivityVideo.is_commented = [[dicActivity numberForJson:@"is_commented"] isEqual:@1] ? @YES : @NO;
    objActivityVideo.activity_text = [dicActivity stringValueForJSON:@"title_text"];
    objActivityVideo.video_seek_time = @0;

    [[Store sharedInstance].mainManagedObjectContext save];
    
    lblTitle.text = [NSString stringWithFormat:@"%@'S VIDEO",objActivityVideo.user_name].uppercaseString;
    
    arrActivity = [TblActivityVideo fetch:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityVideo.activity_id] sortedBy:nil];
    [tblPhoto reloadData];
    
    [self playVideoAutomatically];
}

-(void)playVideoAutomatically
{
    return;
//        NSArray *arrVisibleCell = [tblPhoto visibleCells];
//        for (int c = 0; arrVisibleCell.count > c; c++)
//        {
//            UITableViewCell *cell = arrVisibleCell[c];
//            
//            if ([cell isKindOfClass:[ActivityAddPhotoCell class]])
//            {
//                // Get Video cell here....
//                ActivityAddPhotoCell *cellVideo = (ActivityAddPhotoCell *)cell;
//                if (!cellVideo.viewVideo.hidden)
//                {
//                    cellActivityVideo = cellVideo;
//                    NSLog(@"Play Video here ==========>>>>>  ");
//                    [cellVideo setUpForVideoPlayer];
//                    break;
//                }
//            }
//        }
}

-(void)stopAllreadyPlayedVideo
{
    if (cellActivityVideo)  // Pause Allready played video.....
    {
        if ([cellActivityVideo isKindOfClass:[ActivityAddPhotoCell class]])
        {
            // NSLog(@"View hidden = = %d",cellActivityVideo.viewVideo.hidden);
            if (!cellActivityVideo.viewVideo.hidden && cellActivityVideo && cellActivityVideo.tag == 100)
            {
                NSLog(@"stop Video here ==========>>>>>  ");
                [cellActivityVideo StopVideoPlaying];
                cellActivityVideo = nil;
            }
        }
    }
}




#pragma mark - UITableView Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return arrActivity.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityAddPhotoCell *cell = [tblPhoto dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
    
    NSManagedObject *objCoreData = (NSManagedObject *)arrActivity[indexPath.row];
    
    if ([objCoreData isKindOfClass:[TblActivityPhoto class]])
    {
        // Photo Activity
        TblActivityPhoto *objActPhoto = (TblActivityPhoto *)objCoreData;
        ActivityAddPhotoCell *cell = [tblPhoto dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
        [self configurePhotoActivityCell:cell Activity:objActPhoto];
        return cell;
    }
    else
    {
        // Video Activity
        TblActivityVideo *objActVideo = (TblActivityVideo *)objCoreData;
        ActivityAddPhotoCell *cell = [tblPhoto dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityAddPhotoCell class])];
        [self configureVideoActivityCell:cell Activity:objActVideo];
        return cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - Configure ActivityAddPhotoCell
#pragma mark

- (void)configurePhotoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityPhoto *)objActivityPhoto
{
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = YES;
    cell.imgAddedPhoto.hidden = NO;
    
    [cell setImageHeightWidth:objActivityPhoto.img_width Height:objActivityPhoto.img_hieght PhotoActivity:YES];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityPhoto.activity_post_time];
    
    [cell.lblPhotoCaption hideByHeight:![objActivityPhoto.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityPhoto.caption;
    
    cell.btnDelete.hidden = ![objActivityPhoto.user_id isEqualToString:appDelegate.loginUser.user_id];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityPhoto.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgAddedPhoto setImageWithURL:[NSURL URLWithString:objActivityPhoto.act_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityPhoto.total_comments.integerValue < 3];
    
    cell.btnLike.selected = objActivityPhoto.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    
    
    NSString *strName = objActivityPhoto.user_name;
    NSString *strSession = objActivityPhoto.session_name;
    NSString *strActText = objActivityPhoto.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityPhoto.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:2 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        
    }
    
    cell.imgAddedPhoto.userInteractionEnabled = YES;
    [cell.doubleTapeGesture addTarget:self action:@selector(imgDoubleTapGesture:)];
    cell.doubleTapeGesture.numberOfTapsRequired = 2;
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        
        UIGraphicsBeginImageContext(cell.viewScreenShot.frame.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [appDelegate openSharingOption:screenShot VideoURL:@"" Video:NO TabBar:NO];
        
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityPhoto.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityPhoto.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityPhoto.is_like = @YES;
        }
        
        objActivityPhoto.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityPhoto.total_likes];
        [appDelegate likeActivity:objActivityPhoto.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityPhoto.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
        
        [appDelegate updateActivityListData:RefreshActivityList MainWis:NO];
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityPhoto.activity_id ActType:objActivityPhoto.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityPhoto.activity_id completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 [TblActivityPhoto deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityPhoto.activity_id]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }];
    }];
}

-(void)imgDoubleTapGesture:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:tblPhoto];
    NSIndexPath *tappedIndexPath = [tblPhoto indexPathForRowAtPoint:point];
    
    ActivityAddPhotoCell *cell = [tblPhoto cellForRowAtIndexPath:tappedIndexPath];
    
    cell.imgHeartLikeZoom.hidden = NO;
    cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 8.0, 8.0);} completion:^(BOOL finished){ }];
    
    [UIView animateWithDuration:0.5 animations:^{ cell.imgHeartLikeZoom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);} completion:^(BOOL finished) {cell.imgHeartLikeZoom.hidden = YES;}];
    
    if (!cell.btnLike.selected)
        [cell.btnLike sendActionsForControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Configure ActivityAddVideoCell
#pragma mark -

- (void)configureVideoActivityCell:(ActivityAddPhotoCell *)cell Activity:(TblActivityVideo *)objActivityVideo
{
    cell.objVideoActivity = objActivityVideo;
    [cell setImageHeightWidth:objActivityVideo.video_width Height:objActivityVideo.video_height PhotoActivity:NO];
    cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:objActivityVideo.activity_post_time];
    
    cell.imgHeartLikeZoom.hidden = YES;
    cell.btnVideo.hidden = cell.viewVideo.hidden = NO;
    cell.imgAddedPhoto.hidden = YES;
    
    [cell.lblPhotoCaption hideByHeight:![objActivityVideo.caption isBlankValidationPassed]];
    cell.lblPhotoCaption.text = objActivityVideo.caption;
    
    cell.btnDelete.hidden = ![objActivityVideo.user_id isEqualToString:appDelegate.loginUser.user_id];
    
    [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:objActivityVideo.user_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgVThumb setImageWithURL:[NSURL URLWithString:objActivityVideo.thumb_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.btnLike.selected = objActivityVideo.is_like.boolValue;
    cell.imgHeart.image = cell.btnLike.selected ? [UIImage imageNamed:@"ic_Act_heart_selected"] :[UIImage imageNamed:@"ic_Act_heart_unselected"] ;
    
    cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
    cell.lblTotalComments.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_comments];
    [cell.btnViewAllComments hideByHeight:objActivityVideo.total_comments.integerValue < 3];
    
    NSString *strName = objActivityVideo.user_name;
    NSString *strSession = objActivityVideo.session_name;
    NSString *strActText = objActivityVideo.activity_text;
    
    cell.lblActivityText.text = [NSString stringWithFormat:@"%@ %@ %@",strName, strActText, strSession];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.lblActivityText.attributedText];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(0, strName.length)];
    
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(0,strName.length)];
    [text addAttribute:NSForegroundColorAttributeName value:CRGB(60, 60, 65) range:NSMakeRange(strName.length + strActText.length +2, strSession.length)];
    [text addAttribute: NSFontAttributeName value:CFontGilroyBold(cell.lblActivityText.font.pointSize) range: NSMakeRange(strName.length + strActText.length + 2, strSession.length)];
    cell.lblActivityText.attributedText  = text;
    
    NSArray *arrComments = objActivityVideo.comments.mutableCopy;
    if (arrComments.count > 0)
    {
        cell.viewCommentSeprator.hidden = NO;
        
        NSString *strUserName1 = @"";
        NSString *strUserName2 = @"";
        
        if (arrComments.count > 1)
        {
            // Two Comments
            NSDictionary *dicCom1 = arrComments[0];
            NSDictionary *dicCom2 = arrComments[1];
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            strUserName2 = [dicCom2 stringValueForJSON:@"user_name"];
            NSString *strComment2 = [dicCom2 stringValueForJSON:@"text"];
            cell.lblComment2.text = [NSString stringWithFormat:@"%@ %@",strUserName2, strComment2];
            
            [cell.btnViewAllComments setConstraintConstant:2 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        else
        {
            NSDictionary *dicCom1 = arrComments[0];
            cell.lblComment2.text = nil;
            
            strUserName1 = [dicCom1 stringValueForJSON:@"user_name"];
            NSString *strComment1 = [dicCom1 stringValueForJSON:@"text"];
            cell.lblComment1.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];
            
            [cell.btnViewAllComments setConstraintConstant:3 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
        }
        
        
        [cell.lblComment1 addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
        [cell.lblComment2 addHashTagAndUserHandler:strUserName2 Complete:^(NSString *urlString)
         {
             [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
         }];
        
    }
    else
    {
        cell.lblComment1.text = cell.lblComment2.text = nil;
        cell.viewCommentSeprator.hidden = YES;
        [cell.btnViewAllComments setConstraintConstant:-30 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
    }
    
    
    [cell.lblActivityText addHashTagAndUserHandler:strName Complete:^(NSString *urlString)
     {
         [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
     }];
    
    [cell.btnUser touchUpInsideClicked:^{
        [appDelegate moveOnProfileScreen:strName UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        [appDelegate openSharingOption:nil VideoURL:objActivityVideo.video_url Video:YES TabBar:NO];
    }];
    
    [cell.btnLike touchUpInsideClicked:^{
        
        int likeCount = objActivityVideo.total_likes.intValue;
        
        if (cell.btnLike.selected)
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_unselected"];
            likeCount--;
            objActivityVideo.is_like = @NO;
        }
        else
        {
            cell.imgHeart.image = [UIImage imageNamed:@"ic_Act_heart_selected"];
            likeCount++;
            objActivityVideo.is_like = @YES;
        }
        
        objActivityVideo.total_likes = [NSString stringWithFormat:@"%d",likeCount];
        [[Store sharedInstance].mainManagedObjectContext save];
        
        cell.lblLikes.text = [appDelegate getCommentAndLikeCountWithFormate:objActivityVideo.total_likes];
        [appDelegate likeActivity:objActivityVideo.activity_id Status:!cell.btnLike.selected ? @"true" : @"false" ActivityType:objActivityVideo.activity_main_type];
        
        cell.btnLike.selected = !cell.btnLike.selected;
        
        [appDelegate updateActivityListData:RefreshActivityList MainWis:NO];
    }];
    
    [cell.btnComment touchUpInsideClicked:^{
        [self moveOnCommentScreen:objActivityVideo.activity_id ActType:objActivityVideo.activity_main_type];
    }];
    
    [cell.btnViewAllComments touchUpInsideClicked:^{
        [cell.btnComment sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    [cell.btnVideo touchUpInsideClicked:^{
        if (cell.btnVideo.selected)
        {
            // Already Played
            [cell StopVideoPlaying];
            cellActivityVideo = nil;
        }
        else
        {
            if (cellActivityVideo)
                [self stopAllreadyPlayedVideo];

            cellActivityVideo = cell;
            cellActivityVideo.tag = 100;
            [cell setUpForVideoPlayer];
        }
    }];
    
    cell.btnMuteUnmute.selected = objActivityVideo.video_mute.boolValue;
    [cell.btnMuteUnmute touchUpInsideClicked:^{
        cell.btnMuteUnmute.selected = !cell.btnMuteUnmute.selected;
        cell.playerView.player.muted = cell.btnMuteUnmute.selected;
        objActivityVideo.video_mute = cell.btnMuteUnmute.selected ? @YES : @NO;
        [[Store sharedInstance].mainManagedObjectContext save];
    }];
    
    [cell.btnDelete touchUpInsideClicked:^{
        [appDelegate deletePhotoVideoActivity:objActivityVideo.activity_id completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 [TblActivityVideo deleteObjects:[NSPredicate predicateWithFormat:@"activity_id == %@",objActivityVideo.activity_id]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }];
    }];
}

#pragma mark - Helper Function
-(void)moveOnCommentScreen:(NSString *)strActID ActType:(NSString *)strActType
{
    CommentsViewController *objCo = [[CommentsViewController alloc] init];
    objCo.strActivityId = strActID;
    objCo.strActivityType = strActType;
    [self.navigationController pushViewController:objCo animated:YES];
}

@end
