//
//  TblActivitySyncSession+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivitySyncSession+CoreDataProperties.h"

@implementation TblActivitySyncSession (CoreDataProperties)

+ (NSFetchRequest<TblActivitySyncSession *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblActivitySyncSession"];
}

@dynamic activity_id;
@dynamic activity_main_type;
@dynamic activity_post_time;
@dynamic activity_text;
@dynamic comments;
@dynamic is_like;
@dynamic session_endtime;
@dynamic session_id;
@dynamic session_name;
@dynamic sync_user;
@dynamic timestamp;
@dynamic total_comments;
@dynamic total_likes;
@dynamic user_id;
@dynamic user_image;
@dynamic user_name;
@dynamic is_commented;
@dynamic objActivityDate;

@end
