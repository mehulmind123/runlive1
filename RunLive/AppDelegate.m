
//
//  AppDelegate.m
//  RunLive
//
//  Created by mac-00015 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "AppDelegate.h"
#import "RewardViewController.h"
#import "IntroViewController.h"
#import "ActivityFeedViewController.h"
#import "MyProfileViewController.h"
#import "SessionViewController.h"
#import "SessionListViewController.h"
#import "shareView.h"
#import "OtherUserProfileViewController.h"
#import "MyProfileViewController.h"
#import "SplashViewController.h"
#import "FacebookClass.h"
#import "NotificationViewController.h"
#import "AccountSettingViewController.h"
#import "RunningViewController.h"
#import "InAppPurchaseViewController.h"
#import "NoInternetView.h"
#import "RunSummaryViewController.h"
#import "CommentsViewController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AVFoundation/AVFoundation.h>
#import "SessionCountDown.h"
#import "FindRunnerViewController.h"
#import "OpponentsViewController.h"
#import "SessionDetailsViewController.h"
#import "RunningMusicViewController.h"
#import "SelectMusicViewController.h"
#import "AlbumListViewController.h"
#import "MusicListViewController.h"
#import "PhotoVideoPreviewController.h"
#import "UserRunSummaryViewController.h"
#import "ChatViewController.h"
#import "SessionFeedbackViewController.h"
#import "VideoCropViewController.h"
#import "LocationManager.h"
#define CTABBARHeight Is_iPhone_X ? 89 : 49

@interface AppDelegate ()<PNObjectEventListener>

@property (strong, nonatomic)LocationManager *locationManager;
@property (strong, nonatomic)MotionManager *motionManager;
@end

@implementation AppDelegate
{
    SplashViewController *objSplash;
    NSDictionary *dicLaunchOption;
    NSURLSessionDataTask *taskRuningForCheckingAvailableSession;
    BOOL isOneSignalConfigured;
}

#pragma mark - Application Life Cycle Events
#pragma mark -

-(void)initializeMotionDetector {
    self.motionManager = [MotionManager new];
    [self.motionManager requestPermission];    
    [CUserDefaults setValue:@0 forKey:CMMotionManagerStatus];
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions NS_AVAILABLE_IOS(6_0);
{
    return [super application:application willFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    dicLaunchOption = launchOptions;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Twitter sdk integration ...
    self.objTwitter = [Twitter sharedInstance];
    [self.objTwitter startWithConsumerKey:CTwitterConsumerKey consumerSecret:CTwitterSecretKey];
    
    // Crashalytics......
    [Fabric with:@[[Crashlytics class]]];
    
    // Alloc Health Kit store.
    self.healthStore = [[HKHealthStore alloc] init];
    
    // Spotify Related Code
    SPTAuth *auth = [SPTAuth defaultInstance];
    auth.clientID = @kClientId;
    auth.requestedScopes = @[SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthUserLibraryReadScope];
    auth.redirectURL = [NSURL URLWithString:@kCallbackURL];
    auth.sessionUserDefaultsKey = @kSessionUserDefaultsKey;
    
    NSLog(@"session is: %@", auth.session);
    NSLog(@"isvalid is: %d", auth.session.isValid);
    
    CSetUserLanguage(CLanguageEnglish);
    NSLog(@"French text :%@",CLocalize(@"SIGNUP"));

    NSMutableIndexSet *indexset = [[NSMutableIndexSet alloc] initWithIndexSet:[MIAFNetworking sharedInstance].sessionManager.responseSerializer.acceptableStatusCodes];
    [indexset addIndex:400];
    [MIAFNetworking sharedInstance].sessionManager.responseSerializer.acceptableStatusCodes = indexset;
    [[MIAFNetworking sharedInstance] setDisableTracing:NO];
    
    // Facebook Integrations......
    [[SocialNetworks sharedInstance] setFacebookAppId:CFacebookApiId];
    [[SocialNetworks sharedInstance] setFacebookCallBackURL:CFacebookCallBackUrl];
    
    // STRAVA Integrations......
    [self STRAVAInitialSetUp];

    if ([UIApplication userId] && [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
    {
        isOneSignalConfigured = YES;
        [self onSlashScreenDone];
        [self oneSignalRegistration];
        [self enableRemoteNotification];
    }
    else
    {
        objSplash = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
        self.window.rootViewController = objSplash;
        [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(onSlashScreenDone) userInfo:nil repeats:NO];
    }

    appDelegate.lFollowerCount = 0;
    
    // Check Internet connection..........
    [self internetGoesToOfflineOnline];

    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)onSlashScreenDone
{
    self.isUserOpenSessionListScreen = NO;
    
    [objSplash.view removeFromSuperview];

    NSMutableArray *arrLaoderIamges = [NSMutableArray new];
    
    for (int i = 9; arrLaoderIamges.count < 9; i--)
    {
        NSString *strImageName = [NSString stringWithFormat:@"Loader_%d.png", 9 - (i-1)];
        UIImage *imgLoader = [UIImage imageNamed:strImageName];
        [arrLaoderIamges addObject:imgLoader];
    }
    
    [GiFHUD setGifWithImages:arrLaoderIamges];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:CSongImagePath])
        [[NSFileManager defaultManager] createDirectoryAtPath:CSongImagePath withIntermediateDirectories:YES attributes:nil error:nil];

#pragma mark - Introduction View Controller
    IntroViewController *objIntro = [[IntroViewController alloc] init];
    self.objSuperIntro = [[SuperNavigationController alloc] initWithRootViewController:objIntro];

#pragma mark - Introduction View Controller
    AccountSettingViewController *objAccount = [[AccountSettingViewController alloc] init];
    self.objSuperAccountSeting = [[SuperNavigationController alloc] initWithRootViewController:objAccount];
    
#pragma mark - Running View Controller
    RunningViewController *objRun = [[RunningViewController alloc] init];
    self.objSuperRunning = [[SuperNavigationController alloc] initWithRootViewController:objRun];

    
    if ([UIApplication userId])
    {
        NSArray *arr = [TblUser fetch:[NSPredicate predicateWithFormat:@"user_id == %@", [UIApplication userId]] orderBy:nil ascending:YES];
        
        if (arr.count > 0)
            appDelegate.loginUser = arr[0];

        if (isOneSignalConfigured) // If Coming from notification tap
        {
            [self goInsideTheApp];
        }
        else
        {
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways ||
                [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                [self checkMotionPermissionView];
            else
                [self checkGPSPermissionView];
        }
    }
    else
        self.window.rootViewController = self.objSuperIntro;
    
    if(([[UIApplication sharedApplication] statusBarFrame].size.height > 20))
        [self statusBarFrameNotification:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameNotification:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    
    
}

#pragma mark - Permission Related Methods
#pragma mark -

-(void)checkGPSPermissionView
{
    // Show GPS Permission View here...
    GPSPermissionView *objPermission = [GPSPermissionView initGPSPermissionView];
    [appDelegate.window addSubview:objPermission];
    
    [objPermission.btnGPSGoForIt touchUpInsideClicked:^{
        [appDelegate initialSetUpForLocaitonMananger];
        
        appDelegate.configureGpsPermissionStatus = ^(BOOL isAllow)
        {
            [objPermission.btnGPSNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
        };
    }];
    
    [objPermission.btnGPSNotNow touchUpInsideClicked:^{
        [self checkMotionPermissionView];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [objPermission removeFromSuperview];
        });
    }];
    
}

-(void)checkMotionPermissionView
{
        dispatch_async(GCDMainThread, ^{
            if (![CUserDefaults objectForKey:CMMotionManagerStatus])
            {
                // Show Motion Permission View here...
                MotionPermissionView *objMotion = [MotionPermissionView initMotionPermissionView];
                [appDelegate.window addSubview:objMotion];
                
                [objMotion.btnMotionGoForIt touchUpInsideClicked:^{
                    [appDelegate initializeMotionDetector];
                    dispatch_async(GCDMainThread, ^{
                        [objMotion.btnMotionNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                    });
                    
                }];
                
                [objMotion.btnMotionNotNow touchUpInsideClicked:^{
                    [self checkNotificationPermisssionView];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [objMotion removeFromSuperview];
                    });
                }];
            }
            else
            {
                dispatch_async(GCDMainThread, ^{
                    [self checkNotificationPermisssionView];
                });
            }
        });
}

-(void)checkNotificationPermisssionView
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus == UNAuthorizationStatusNotDetermined)
        {
            dispatch_async(GCDMainThread, ^{
                // Show Motion Permission View here...
                
                NotificationPermissionView *objNotification = [NotificationPermissionView initNotificationPermissionView];
                [appDelegate.window addSubview:objNotification];
                
                [objNotification.btnNotificationDoIt touchUpInsideClicked:^{
                    [appDelegate enableRemoteNotification];
                    [appDelegate oneSignalRegistration];
                    
                    appDelegate.configureNotificationPermissionStatus = ^(BOOL isGranted)
                    {
                        dispatch_async(GCDMainThread, ^{
                            [objNotification.btnNotificationNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                        });
                    };
                }];
                
                [objNotification.btnNotificationNotNow touchUpInsideClicked:^{
                    [self checkMicrophonePermissionView];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [objNotification removeFromSuperview];
                    });
                }];
            });
        }
        else
        {
            dispatch_async(GCDMainThread, ^{
                if (!isOneSignalConfigured)
                {
                    [self oneSignalRegistration];
                    [self enableRemoteNotification];
                }
                
                [self checkMicrophonePermissionView];
            });
        }
    }];
}

-(void)checkMicrophonePermissionView
{
    
    NSLog(@"%@",appDelegate.loginUser.user_name);
    NSLog(@"%d",appDelegate.loginUser.isVoiceChatEnable.boolValue);
    
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionUndetermined && appDelegate.loginUser.isVoiceChatEnable.boolValue)
    {
        dispatch_async(GCDMainThread, ^{
            // Show Motion Permission View here...
            
            VoiceChatPermissionView *objVoiceChat = [VoiceChatPermissionView initVoiceChatPermissionView];
            [appDelegate.window addSubview:objVoiceChat];
            
            [objVoiceChat.btnVoiceChatGotForIt touchUpInsideClicked:^{
                [objVoiceChat askForMicroPhonePermission];
                
                __weak typeof (objVoiceChat) weakObjVoiceChat = objVoiceChat;
                objVoiceChat.configurationVoiceChatPermissionAcceptReject = ^(BOOL isGranted)
                {
                    dispatch_async(GCDMainThread, ^{
                        [weakObjVoiceChat.btnVoiceChatNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                    });
                };
            }];
            
            [objVoiceChat.btnVoiceChatNotNow touchUpInsideClicked:^{
                [self checkHealthKitPermissionView];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [objVoiceChat removeFromSuperview];
                });
            }];
        });
    }
    else
    {
        dispatch_async(GCDMainThread, ^{
            [self checkHealthKitPermissionView];
        });
        
    }
}

-(void)checkHealthKitPermissionView
{
    if ([self.healthStore authorizationStatusForType:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning]] == HKAuthorizationStatusNotDetermined)
    {
        // Show Motion Permission View here...
        HealthKitPermissionView *objHealthKit = [HealthKitPermissionView initHealthKitPermissionView];
        [appDelegate.window addSubview:objHealthKit];
        
        [objHealthKit.btnHealthKitSure touchUpInsideClicked:^{
            if ([HKHealthStore isHealthDataAvailable])
            {
                NSSet *writeDataTypes = [appDelegate.healthStore dataTypesToWrite];
                NSSet *readDataTypes = [appDelegate.healthStore dataTypesToRead];
                
                [appDelegate.healthStore requestAuthorizationToShareTypes:writeDataTypes readTypes:readDataTypes completion:^(BOOL success, NSError *error)
                 {
                     dispatch_async(GCDMainThread, ^{
                         [objHealthKit.btnHealthKitNotNow sendActionsForControlEvents:UIControlEventTouchUpInside];
                     });
                 }];
            }
        }];
        
        [objHealthKit.btnHealthKitNotNow touchUpInsideClicked:^{
            [self goInsideTheApp];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [objHealthKit removeFromSuperview];
            });
        }];
    }
    else
        [self goInsideTheApp];
}

-(void)goInsideTheApp
{
    self.window.rootViewController = [self setTabBarController];
    
    if (appDelegate.loginUser.profileStatus.boolValue)    // If Profile status is true
        [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab3];
    else
        self.window.rootViewController = self.objSuperAccountSeting;
    
    [self toCheckRunningSessionIsAvailable];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //TODO: Confifure Pubnub configuration
        [self configurePubnub];
        [self pubNubAddPushNotifications];
        [self setNotificationBadge];
        
        // To show red dot for message.
        [self showMessageBadgeOnTabBar];
        
        // If apple music selected...
        NSLog(@"Colleciton data ====== >> %@",[CUserDefaults valueForKey:CAppleMusicCollection]);
        if ([CUserDefaults objectForKey:CAppleMusicCollection] && [CUserDefaults valueForKey:CAppleMusicCollection])
        {
            appDelegate.appleMPMusicPlayerController = [MPMusicPlayerController systemMusicPlayer];
            //                appDelegate.appleMPMusicPlayerController = [MPMusicPlayerController applicationMusicPlayer];
            NSData *data = [CUserDefaults objectForKey:CAppleMusicCollection];
            appDelegate.appleMPMusicPlayerController.shuffleMode = MPMusicShuffleModeOff;
            MPMediaItemCollection *objCollection = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [appDelegate.appleMPMusicPlayerController setQueueWithItemCollection:objCollection];
        }
    });
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    NSLog(@"applicationDidEnterBackground ==== > Checking crash");
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    NSLog(@"applicationDidEnterBackground ==== > Checking crash");
    [self SetPubNubClientState:@"offline"];
    [self setNotificationBadge];
    
    NSLog(@"MQTT Alive ============== >>>>>>>>>>%hu",self.objMQTTClient.keepAlive);
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self toCheckRunningSessionIsAvailable];
    
    [self SetPubNubClientState:@"online"];
    [self setNotificationBadge];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSLog(@"applicationDidBecomeActive");
//    [self checkInAppPurchaseStatus:NO];
    
    // To show red dot for message.
    [self showMessageBadgeOnTabBar];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [super application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    
    if([[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) // facebook
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    
    if (([[url absoluteString] rangeOfString:@"frdstravaclient://"].location != NSNotFound))
    {
        // STRAVA call back handler.....
        [[FRDStravaClient sharedInstance] parseStravaAuthCallback:url withSuccess:^(NSString *stateInfo, NSString *code)
        {
            [self STRAVAExchangeToken:code];
        }failure:^(NSString *stateInfo, NSString *error)
        {
            [[self getTopMostViewController] customAlertViewWithOneButton:@"" Message:error ButtonText:@"OK" Animation:NO completed:nil];
        }];
    }
    
    return [[Twitter sharedInstance] application:application openURL:url options:@{}];
    
    return YES;
}

#pragma mark - STRAVA Related Functions

-(void)STRAVAInitialSetUp
{
    [[FRDStravaClient sharedInstance] initializeWithClientId:CSTRAVAClientID clientSecret:CSTRAVAClientSecretKey];
    [[FRDStravaClient sharedInstance] setAccessToken:nil];
}

-(void) STRAVAExchangeToken:(NSString *)code
{
    [[FRDStravaClient sharedInstance] exchangeTokenForCode:code success:^(StravaAccessTokenResponse *response)
    {
     [[FRDStravaClient sharedInstance] setAccessToken:response.accessToken];
        if (self.configureStravaCallBackAfterLogin)
            self.configureStravaCallBackAfterLogin(YES);
        
    }failure:^(NSError *error)
     {
         [[self getTopMostViewController] customAlertViewWithOneButton:@"" Message:error.localizedDescription ButtonText:@"OK" Animation:NO completed:nil];
     }];
}

#pragma mark - Initialize tab bar
- (UITabBarController *)setTabBarController
{
    if (self.objTabBarController == nil)
    {
#pragma mark - Reward View Controller
        RewardViewController *objReward = [[RewardViewController alloc] init];
        self.objSuperReward = [[SuperNavigationController alloc] initWithRootViewController:objReward];
        
#pragma mark - Activity Feed View Controller
        ActivityFeedViewController *objActivity = [[ActivityFeedViewController alloc] init];
        self.objSuperActivityFeed = [[SuperNavigationController alloc] initWithRootViewController:objActivity];
        
#pragma mark - My Profile View Controller
        MyProfileViewController *objMyProfile = [[MyProfileViewController alloc] init];
        self.objSuperMyProfile = [[SuperNavigationController alloc] initWithRootViewController:objMyProfile];
        
#pragma mark - Create Session View Controller
        SessionViewController *objSes = [[SessionViewController alloc] init];
        self.objSuperSession = [[SuperNavigationController alloc] initWithRootViewController:objSes];
        
#pragma mark - Session List View Controller
        SessionListViewController *objSesList = [[SessionListViewController alloc] init];
        self.objSuperSessionList = [[SuperNavigationController alloc] initWithRootViewController:objSesList];

        
        self.objCustomTabBar = [CustomTabBar CustomTabBar];
        self.objTabBarController = [[UITabBarController alloc] init];
        self.objTabBarController.view.backgroundColor = [UIColor clearColor];
        [self.objTabBarController.view addSubview:self.objCustomTabBar];
        self.objTabBarController.tabBar.hidden = YES;
        [self.objTabBarController setViewControllers:@[self.objSuperActivityFeed, self.objSuperReward, self.objSuperSession, self.objSuperSessionList, self.objSuperMyProfile]];
        self.objTabBarController.selectedIndex = 2;
        
        [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab3];
    }
    
    return self.objTabBarController;
}

- (void)statusBarFrameNotification:(NSNotification*)notification
{
    UIViewController *viewController = [self getTopMostViewController];
    
    if ([viewController isKindOfClass:[ActivityFeedViewController class]] || [viewController isKindOfClass:[RewardViewController class]] || [viewController isKindOfClass:[SessionViewController class]] || [viewController isKindOfClass:[SessionListViewController class]] || [viewController isKindOfClass:[MyProfileViewController class]])
    {
        [self showTabBar];
    }
}


- (void)hideTabBar
{
    CViewSetY(self.objCustomTabBar, CScreenHeight);
    CViewSetY(self.objTabBarController.tabBar, CScreenHeight);
    CViewSetHeight(self.objTabBarController.tabBar, 0);
    self.objTabBarController.tabBar.hidden = YES;
    
    self.objCustomTabBar.layer.masksToBounds = YES;
    self.objCustomTabBar.layer.shadowRadius = 0;
    self.objCustomTabBar.layer.shadowColor = [UIColor clearColor].CGColor;
    self.objCustomTabBar.layer.shadowOffset = CGSizeMake(0, 0);
    self.objCustomTabBar.layer.shadowOpacity = 0;
}

- (void)showTabBar
{
    [appDelegate.objTabBarController.view.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop)
     {
         if ([obj isKindOfClass:[CustomTabBar class]])
         {
             NSLog(@"%f",[[UIApplication sharedApplication] statusBarFrame].size.height);
             
             if(([[UIApplication sharedApplication] statusBarFrame].size.height > 20 && !(Is_iPhone_X)) || ([[UIApplication sharedApplication] statusBarFrame].size.height > 44 && Is_iPhone_X))
             {
                 CGFloat cHeight = Is_iPhone_X ? CScreenHeight - ([[UIApplication sharedApplication] statusBarFrame].size.height > 44?44:0) : CScreenHeight - ([[UIApplication sharedApplication] statusBarFrame].size.height > 20?20:0);
                 
                 CViewSetHeight(obj.superview, cHeight);
                 CGRect frame = obj.frame;
                 frame.origin.y =CScreenHeight-49-20;
                 
                 //frame.size.height = 49;
                 frame.size.height = CTABBARHeight;
                 obj.frame = frame;
                 *stop = YES;

             }
             else
             {
                 CGFloat cHeight = Is_iPhone_X ? CScreenHeight - ([[UIApplication sharedApplication] statusBarFrame].size.height > 44?44:0) : CScreenHeight - ([[UIApplication sharedApplication] statusBarFrame].size.height > 20?20:0);
                 
                 CViewSetHeight(obj.superview, cHeight);
                 CGRect frame = obj.frame;
                 //frame.origin.y = CScreenHeight-49;
                 frame.origin.y = Is_iPhone_X ?  CScreenHeight-49 - 20 : CScreenHeight-49;
                 
                 //frame.size.height = 49;
                 frame.size.height = CTABBARHeight;
                 
                 obj.frame = frame;
                 *stop = YES;
                 
                 if (self.objTabBarController.selectedIndex != 2)
                 {
                     self.objCustomTabBar.layer.masksToBounds = NO;
                     self.objCustomTabBar.layer.shadowRadius = 8;
                     self.objCustomTabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
                     self.objCustomTabBar.layer.shadowOffset = CGSizeMake(0, -3);
                     self.objCustomTabBar.layer.shadowOpacity = .5f;
                 }

             }
         }
     }];
}

#pragma mark - Internet Offline/Online

-(void)internetGoesToOfflineOnline
{
    // If Internet Connection appear to offline.......
    __block NoInternetView *objNoInternet = [NoInternetView viewFromXib];
    
    objNoInternet.frame = CGRectMake(0, 0, CScreenWidth, CScreenHeight);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.google.com"]];
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         NSLog(@"TO CHECK NETWORK SWITCHING ====== ");
         
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 NSLog(@"NETWORK REACHABLE");
                 
                 [operationQueue setSuspended:NO];

                 objNoInternet.viewInternet.backgroundColor = CRGB(46, 204, 113);
                 objNoInternet.lblNoInternet.text = @"Connecting…";

                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [objNoInternet removeFromSuperview];
                 });
                 
                 // MQTT connection If not connected...
                 if ([[self getTopMostViewController] isKindOfClass:[RunningViewController class]])
                     [self MQTTInitialSetup];
             }
                 break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
             {
                 NSLog(@"NETWORK UNREACHABLE");
                 [operationQueue setSuspended:YES];
                 [objNoInternet removeFromSuperview];
                 [appDelegate.window addSubview:objNoInternet];
                 objNoInternet.viewInternet.backgroundColor = [UIColor redColor];
                 
                 if ([[self getTopMostViewController] isKindOfClass:[RunningViewController class]])
                     objNoInternet.lblNoInternet.text = @"poor/no internet connection, distance may not update.";
                 else
                     objNoInternet.lblNoInternet.text = @"No Internet connection.";
                 
                 
                 break;
             }
         }
     }];
    [manager.reachabilityManager startMonitoring];
}


#pragma mark - Location Manager Methods

-(void)initialSetUpForLocaitonMananger
{
    self.locationManager = [LocationManager new];
    
    __weak __typeof(self) weakSelf = self;
    
    self.locationManager.didChangeAuthorization = ^(BOOL isGranted) {
        if (weakSelf.configureGpsPermissionStatus)
            weakSelf.configureGpsPermissionStatus(isGranted);
    };
    [self.locationManager requestWhenInUseAuthorization];
}

-(void)getLocationNameFromLatLong:(double)lat andLong:(double)lng completed:(void(^)(NSDictionary *dicAddress))completion
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:lat longitude:lng]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSLog(@"placemark %@",placemark);
        //String to hold address
        NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
        NSLog(@"addressDictionary %@", placemark.addressDictionary);
        
        NSLog(@"placemark %@",placemark.region);
        NSLog(@"placemark %@",placemark.country);  // Give Country Name
        NSLog(@"placemark %@",placemark.locality); // Extract the city name
        NSLog(@"location %@",placemark.name);
        NSLog(@"location %@",placemark.ocean);
        NSLog(@"location %@",placemark.postalCode);
        NSLog(@"location %@",placemark.subLocality);
        
        NSLog(@"location %@",placemark.location);
        //Print the location to console
        NSLog(@"I am currently at %@",locatedAt);
        
        if (completion)
            completion(placemark.addressDictionary);
    }];
}

#pragma mark - share
-(void)openSharingOption:(UIImage *)image VideoURL:(NSString *)strVideoURL Video:(BOOL)isVideo TabBar:(BOOL)isTapBar
{
    UIViewController *viewController = [self getTopMostViewController];
    shareView *objShare = [shareView viewWithNibName:@"shareView"];
    [objShare getFriendListFromServer];
    
    CViewSetWidth(objShare, CScreenWidth);
    CViewSetY(objShare, CScreenHeight-CViewHeight(objShare));
    
    [objShare.btnClose touchUpInsideClicked:^{
        
        if (objShare.viewSearch.isHidden)
        {
            [UIView animateWithDuration:0.4 animations:^{
                objShare.viewController.view.backgroundColor = [objShare.viewController.view.backgroundColor colorWithAlphaComponent:0];
                CViewSetY(objShare,CViewY(objShare) + CViewHeight(objShare));
            } completion:^(BOOL finished) {
                [viewController dismissOverlayViewControllerAnimated:true completion:nil];
                if (isTapBar) {
                    [self showTabBar];
                }
            }];
            return;
        }
        
        if (objShare.txtSearch.text.length == 0)
        {
            [objShare.btnSearch sendActionsForControlEvents:UIControlEventTouchUpInside];
            return;
        }
        
        objShare.txtSearch.text = @"";
        [objShare getFriendListFromServer];
    }];
    
    [objShare.btnFriends touchUpInsideClicked:^{
        objShare.viewlineX.constant = 0;
        objShare.viewSocialMedia.hidden = true;
        objShare.viewFriends.hidden = false;
        objShare.btnSearch.hidden = false;
        [UIView animateWithDuration:.1 animations:^{
            CViewSetY(objShare,objShare.viewY);
        }];
    }];
    
    [objShare.btnSocialMedia touchUpInsideClicked:^{
        objShare.viewlineX.constant = objShare.frame.size.width/2;
        objShare.viewSocialMedia.hidden = false;
        objShare.viewFriends.hidden = true;
        objShare.btnSearch.hidden = true;
        objShare.viewSearch.hidden = true;
        [UIView animateWithDuration:.1 animations:^{
            CViewSetY(objShare,objShare.viewY + 75);
        }];
        
    }];

#pragma mark - Share on Facebook
    [objShare.btnFb touchUpInsideClicked:^{
        //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            if (isVideo)
                [controller setInitialText:strVideoURL];
            else
                [controller addImage:image];
            
            [self.objTabBarController presentViewController:controller animated:YES completion:Nil];
            [objShare.btnClose sendActionsForControlEvents:UIControlEventTouchUpInside];
//        }
//        else
//        {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//        }
    }];

#pragma mark - Share on Twiter
    [objShare.btnTwit touchUpInsideClicked:^{
//        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//        {
            SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            
            if (isVideo)
                [tweetSheet setInitialText:strVideoURL];
            else
                [tweetSheet addImage:image];
            
            [self.objTabBarController presentViewController:tweetSheet animated:YES completion:nil];
            [objShare.btnClose sendActionsForControlEvents:UIControlEventTouchUpInside];
//        }
//        else
//        {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//        }
    }];

#pragma mark - Share on Instagrame
    [objShare.btnInsta touchUpInsideClicked:^{
        
        if(!isVideo)
        {
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
            {
                UIImage *imageToUse = image;
                NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
                NSData *imageData=UIImagePNGRepresentation(imageToUse);
                [imageData writeToFile:saveImagePath atomically:YES];
                NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
                self.documentController=[[UIDocumentInteractionController alloc]init];
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
                self.documentController.delegate = self;
                self.documentController.UTI = @"com.instagram.exclusivegram";
                [self.documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:objShare animated:YES];
            }
            else
            {
                [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:@"Instagram not found" ButtonText:@"OK" Animation:YES completed:nil];
            }

        }
    }];

#pragma mark - Share on Mail
    [objShare.btnMail touchUpInsideClicked:^{
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail])
        {
            mailer.mailComposeDelegate = self;
            mailer.navigationController.navigationBar.translucent = NO;
            
            if (isVideo)
            {
                [mailer setMessageBody:strVideoURL isHTML:NO];
                [mailer setSubject:@"Runlive App!"];
            }
            else
            {
                NSData *myData = UIImageJPEGRepresentation(image, 0.9);
                [mailer addAttachmentData:myData mimeType:@"image/jpg" fileName:@"RunLive.jpg"];
            }
            
            [self.objTabBarController presentViewController:mailer animated:YES completion:^(void)
            {
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            }];
            
            [objShare.btnClose sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }
    }];

    [objShare.btnShareWithFr touchUpInsideClicked:^{
        
        if (objShare.arrSelectedUser.count < 1)
        {
            [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:@"Please select friend." ButtonText:@"OK" Animation:YES completed:nil];
            return ;
        }
        
        if (isVideo)
            [objShare sharePostInChatGroup:nil VideoPost:YES VideoURL:strVideoURL];
        else
            [objShare sharePostInChatGroup:image VideoPost:NO VideoURL:nil];
        
        [UIView animateWithDuration:0.4 animations:^{
            objShare.viewController.view.backgroundColor = [objShare.viewController.view.backgroundColor colorWithAlphaComponent:0];
            CViewSetY(objShare,CViewY(objShare) + CViewHeight(objShare));
        } completion:^(BOOL finished) {
            [viewController dismissOverlayViewControllerAnimated:true completion:nil];
        }];
    }];
    
    [objShare.btnSearch touchUpInsideClicked:^{
        objShare.viewSearch.hidden = !objShare.viewSearch.isHidden;
    }];
    
    CViewSetY(objShare,CViewY(objShare) + CViewHeight(objShare));
    
//    [viewController presentView_DissmissEffect:objShare];
    [viewController presentView:objShare shouldHideOnOutsideClick:NO];
//    [objShare bringSubviewToFront:self.objCustomTabBar];
    if(isTapBar)
    {
        [self hideTabBar];
    }
    
    CViewSetHeight(objShare, Is_iPhone_5 ? 324 : Is_iPhone_6_PLUS ? 374 : 354);
    [UIView animateWithDuration:.4 animations:^{
        CViewSetY(objShare,CViewY(objShare) - CViewHeight(objShare));
        objShare.viewY = CViewY(objShare);
    }];
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
    NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}

#pragma mark - One Signal
// Add this new method
- (void)onOSPermissionChanged:(OSPermissionStateChanges*)stateChanges
{
    NSLog(@"onOSPermissionChanged");
    
    // Example of detecting anwsering the permission prompt
    if (stateChanges.from.status == OSNotificationPermissionNotDetermined) {
        if (stateChanges.to.status == OSNotificationPermissionAuthorized)
            NSLog(@"Thanks for accepting notifications!");
        else if (stateChanges.to.status == OSNotificationPermissionDenied)
            NSLog(@"Notifications not accepted. You can turn them on later under your iOS settings.");
    }
}

// Add this new method
- (void)onOSSubscriptionChanged:(OSSubscriptionStateChanges*)stateChanges
{
    
    NSLog(@"onOSSubscriptionChanged");
    
    // Example of detecting subscribing to OneSignal
    if (!stateChanges.from.subscribed && stateChanges.to.subscribed)
    {
        NSLog(@"Subscribed for OneSignal push notifications!");
        
        NSLog(@"TO PUSH TOKEN ----------%@",stateChanges.to.pushToken);
        NSLog(@"FROM PUSH TOKEN ----------%@",stateChanges.from.pushToken);
        
        if (stateChanges.to.pushToken)
        {
            [CUserDefaults setObject:stateChanges.to.pushToken forKey:CDeviceToken];
            [self registerDeviceTokenForNotification];
        }
    }
}

-(void)oneSignalRegistration
{
    
//    if ([CUserDefaults objectForKey:CDeviceToken]) // If allready register then return from here..
//        return;
//
    
    NSLog(@"Checking Laucnh option data ========== %@",dicLaunchOption);
    
    
    [OneSignal initWithLaunchOptions:dicLaunchOption appId:COneSignalAppId handleNotificationReceived:^(OSNotification *notification)
     {
         if (notification)
         {
             OSNotificationPayload *payload = notification.payload;
             [self actionOnPushNotificationWithDic:payload.additionalData ComingFromQuitMode:NO];
         }
     } handleNotificationAction:^(OSNotificationOpenedResult *result)
     {
         OSNotificationPayload *payload = result.notification.payload;
         NSLog(@"Krishna ======= OSNotificationPayload ======= >>> ");
         [self actionOnPushNotificationWithDic:payload.additionalData ComingFromQuitMode:NO];
     } settings:@{kOSSettingsKeyAutoPrompt : @YES,kOSSettingsKeyInFocusDisplayOption:[NSString stringWithFormat:@"%lu", (unsigned long)OSNotificationDisplayTypeNone]}];
    
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
    }];
    
    OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
    NSLog(@"OSPermissionSubscriptionState == %@",status);
    [OneSignal addPermissionObserver:self];
    [OneSignal addSubscriptionObserver:self];

}

#pragma mark -
#pragma mark - Push Notification Related Stuff
#pragma mark -

-(void)setNotificationBadgeOnAppIcon
{
    if (!appDelegate.loginUser)
        return;
    
    NSArray *arrMessage = [TblChats fetch:[NSPredicate predicateWithFormat:@"loginuser == %@ && isChatDeleted == %@ && isReadMessage == %@", appDelegate.loginUser,@NO,@NO] sortedBy:nil];
    
    if (arrMessage.count > 0 || appDelegate.loginUser.notification_count.integerValue > 0)
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    else
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}

-(void)setNotificationBadge
{
    if (!appDelegate.loginUser)
        return;
    
    [[APIRequest request] checkForNotificationBadge:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             if ([dicData stringValueForJSON:@"count"].intValue > 0)
             {
                 UIViewController *viewController = [self getTopMostViewController];
                 if ([viewController isKindOfClass:[NotificationViewController class]])
                 {
                     self.objCustomTabBar.viewTabNotificationBadge.hidden = YES;
                     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                 }
                 else
                 {
                     self.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                 }
             }
         }
     }];
}

-(void)registerDeviceTokenForNotification
{
    if ([CUserDefaults objectForKey:CDeviceToken] && appDelegate.loginUser)
    {
        [[APIRequest request] registerDeviceTokenOnserver:[CUserDefaults valueForKey:CDeviceToken] completed:^(id responseObject, NSError *error)
        {
            if (!error)
                NSLog(@"token updated on server");
        }];
  }
}

-(void)actionOnPushNotificationWithDic:(NSDictionary *)dic ComingFromQuitMode:(BOOL)isComingFromQuitAppNotificationTap
{
    NSInteger appState = [[UIApplication sharedApplication] applicationState];
    if (appState == UIApplicationStateActive && !isComingFromQuitAppNotificationTap)
    {
        if (appDelegate.loginUser)
        {
            UIViewController *viewController = [self getTopMostViewController];

            // If user on Runner or Opponent screen  screen is open....
            if ([viewController isKindOfClass:[FindRunnerViewController class]] || [viewController isKindOfClass:[OpponentsViewController class]] || [viewController isKindOfClass:[RunningViewController class]] || [viewController isKindOfClass:[InAppPurchaseViewController class]])
                return;
            
            // Show Count down screen for started session ........
            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeRunStart])
            {
                if ([viewController isKindOfClass:[SessionListViewController class]] || [viewController isKindOfClass:[SessionDetailsViewController class]])
                    return;
                
                if (!self.isUserOpenSessionListScreen)
                    [self showCountDownScreenForManualSession:[dic stringValueForJSON:@"sessionId"] completed:nil];
                
                return;
            }
            
            if ([viewController isKindOfClass:[RunSummaryViewController class]] || [viewController isKindOfClass:[SessionFeedbackViewController class]])
            {
                
                if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionResult])
                {
                    // Refresh Screen here...
                    if (self.configureRefreshSessionData)
                        self.configureRefreshSessionData(ResultSession);
                }
                else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionComment])
                {
                    if (self.configureRefreshSessionData)
                        self.configureRefreshSessionData(CommentOnSession);
                }
                return;
            }
            
            if ([viewController isKindOfClass:[CommentsViewController class]])
            {
                if (self.configureRefreshSessionData)
                    self.configureRefreshSessionData(CommentOnActivity);
                
                return;
            }
            
            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeExtendSubscriptionDate])
            {
                // Extend Subscription time period...
                [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:nil];
                return;
            }
            
            // Show push Notification alert with OK Button ........
            if ([viewController isKindOfClass:[RunningMusicViewController class]] || [viewController isKindOfClass:[SelectMusicViewController class]] || [viewController isKindOfClass:[AlbumListViewController class]] || [viewController isKindOfClass:[MusicListViewController class]] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStart] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStartSync] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeRunCancel] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeChatMessage])
            {
                [self vibrationOnNotification];
                [viewController customAlertViewWithOneButton:@"" Message:[dic stringValueForJSON:@"message"] ButtonText:@"OK" Animation:NO completed:nil];
                return;
            }
            
            // Custom session start 1 Mint alert notification...
            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStart1Mint])
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self vibrationOnNotification];
                    [viewController customAlertViewWithOneButton:@"GET READY!" Message:[dic stringValueForJSON:@"message"] ButtonText:@"OK" Animation:NO completed:^{
                        [viewController.navigationController popToRootViewControllerAnimated:NO];
                        [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
                    }];
                });
                return;
            }
            
            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivePremiumPlan])
            {
                    [self vibrationOnNotification];
                    [viewController customAlertViewWithOneButton:@"" Message:[dic stringValueForJSON:@"message"] ButtonText:@"OK" Animation:NO completed:^{
                        [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:nil];
                    }];
                return;
            }

            
            // If notification screen is open....
            if ([viewController isKindOfClass:[NotificationViewController class]])
            {
                // Refresh notification screen...
                if (self.configureRefreshNotificationListScreen)
                    self.configureRefreshNotificationListScreen(YES);
                
                return;
            }

            // If My Profile screen is open screen is open....
            if ([viewController isKindOfClass:[MyProfileViewController class]])
            {
                appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                return;
            }

            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionComment])
            {
                appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                if ([viewController isKindOfClass:[RunSummaryViewController class]] || [viewController isKindOfClass:[SessionFeedbackViewController class]] || [viewController isKindOfClass:[SessionDetailsViewController class]])
                {
                    if (self.configureRefreshSessionData)
                        self.configureRefreshSessionData(CommentOnSession);
                }
                else
                {
                        [self vibrationOnNotification];
                        [viewController customAlertViewWithTwoButton:@"Alert!" Message:[dic stringValueForJSON:@"message"] ButtonFirstText:@"View" ButtonSecondText:@"Dismiss" Animation:YES completed:^(int index) {
                            if (index == 0)
                            {
                                self.isMoveOnNotificationScreen = YES;
                                [viewController.navigationController popToRootViewControllerAnimated:NO];
                                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab5];
                            }
                        }];
                }
            }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionDelete])
            {
                appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                if ([viewController isKindOfClass:[SessionListViewController class]] || [viewController isKindOfClass:[SessionDetailsViewController class]])
                {
                    [self vibrationOnNotification];
                    
                    if (self.configureRefreshSessionData)
                        self.configureRefreshSessionData(DeleteSession);
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:[dic stringValueForJSON:@"message"] ButtonText:@"OK" Animation:YES completed:nil];
                    });
                }
            }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionResult])
             {
                 appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                 [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                 
                 if ([viewController isKindOfClass:[RunSummaryViewController class]] || [viewController isKindOfClass:[SessionFeedbackViewController class]])
                 {
                     // Refresh Screen here...
                     if (self.configureRefreshSessionData)
                         self.configureRefreshSessionData(ResultSession);
                 }
                 else
                 {
                     [self vibrationOnNotification];
                     [viewController customAlertViewWithTwoButton:@"Alert!" Message:[dic stringValueForJSON:@"message"] ButtonFirstText:@"View" ButtonSecondText:@"Dismiss" Animation:YES completed:^(int index) {
                         if (index == 0)
                         {
                             self.isMoveOnNotificationScreen = YES;
                             [viewController.navigationController popToRootViewControllerAnimated:NO];
                             [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab5];
                         }
                     }];
                 }
             }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionJoin] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionSynced] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionDeclined])
            {
                appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                // Refresh Session Listing screen
                if ([viewController isKindOfClass:[SessionListViewController class]] || [viewController isKindOfClass:[SessionDetailsViewController class]])
                {
                    if([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionSynced])
                    {
                        if (self.configureRefreshSessionData)
                            self.configureRefreshSessionData(SyncSession);
                    }
                    else if([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionDeclined])
                    {
                        if (self.configureRefreshSessionData)
                            self.configureRefreshSessionData(UnjoinSession);
                    }
                    else if([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionJoin])
                    {
                        if (self.configureRefreshSessionData)
                            self.configureRefreshSessionData(JoinSession);
                    }
                }
                else
                {
                    [self vibrationOnNotification];
                    [viewController customAlertViewWithTwoButton:@"Alert!" Message:[dic stringValueForJSON:@"message"] ButtonFirstText:@"View" ButtonSecondText:@"Dismiss" Animation:YES completed:^(int index) {
                        if (index == 0)
                        {
                            [viewController.navigationController popToRootViewControllerAnimated:NO];
                            [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
                        }
                    }];
                }
            }
            else
            {
                appDelegate.objCustomTabBar.viewTabNotificationBadge.hidden = NO;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                [self vibrationOnNotification];
                [viewController customAlertViewWithTwoButton:@"Alert!" Message:[dic stringValueForJSON:@"message"] ButtonFirstText:@"View" ButtonSecondText:@"Dismiss" Animation:YES completed:^(int index) {
                    if (index == 0)
                    {
                        if([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeDeleteProduct])
                        {
                            [viewController.navigationController popToRootViewControllerAnimated:NO];
                            [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab2];
                        }
                        else
                        {
                            self.isMoveOnNotificationScreen = YES;
                            [viewController.navigationController popToRootViewControllerAnimated:NO];
                            [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab5];
                        }
                    }
                }];
            }
        }
    }
    else
    {
        UIViewController *viewController = [self getTopMostViewController];
        
        if ([UIApplication userId])
        {
            if ([viewController isKindOfClass:[FindRunnerViewController class]] || [viewController isKindOfClass:[OpponentsViewController class]] || [viewController isKindOfClass:[RunningViewController class]] || [viewController isKindOfClass:[InAppPurchaseViewController class]] || [viewController isKindOfClass:[RunningMusicViewController class]] || [viewController isKindOfClass:[SelectMusicViewController class]] || [viewController isKindOfClass:[AlbumListViewController class]] || [viewController isKindOfClass:[MusicListViewController class]] || [viewController isKindOfClass:[PhotoVideoPreviewController class]])
                return;
            
            
            appDelegate.window.rootViewController = [self setTabBarController];
            [viewController.navigationController popToRootViewControllerAnimated:NO];
            
//            appDelegate.window.rootViewController = appDelegate.objTabBarController;
            
            NSLog(@"Notification background testing with movment ==== ");
            
            if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeChatMessage])
            {
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab1];
                
                if (self.configureRefreshChatListScreen)
                    self.configureRefreshChatListScreen(YES);
            }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeRunStart])
            {
                //[self showCountDownScreenForManualSession:[dic stringValueForJSON:@"sessionId"] completed:nil];
            }
            
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeExtendSubscriptionDate] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeActivePremiumPlan])
            {
                // call profile api...
                [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:nil];
            }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStart] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStartSync] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionSynced] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionJoin] || [[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeSessionStart1Mint])
            {
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab4];
            }
            else if ([[dic stringValueForJSON:@"type"] isEqualToString:CNotificationTypeDeleteProduct])
            {
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab2];
            }
            else
            {
                self.isMoveOnNotificationScreen = YES;
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab5];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [viewController.navigationController popToRootViewControllerAnimated:NO];
            });
        }
    }
}

-(void)actionOnPubNubNotification:(NSDictionary *)dic
{
    /*
     aps =     {
     alert = "krishnasoni messaged you";
     badge = 1;
     "channel_id" = 5a24eb02be2c773855dd4182;
     type = apns;
     };

     */
    
    NSDictionary *dicNotification = [dic valueForKey:@"aps"];
    
    NSInteger appState = [[UIApplication sharedApplication] applicationState];
    
    if (appState == UIApplicationStateActive)
    {
        
        if ([UIApplication userId])
        {
            if ([[dicNotification stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id] || ![[dicNotification stringValueForJSON:@"type"] isEqualToString:CNotificationTypePubNub])
            {
                NSLog(@"This message sent by login user === ");
                return;
            }
            
            self.objCustomTabBar.viewTabMessageBadge.hidden = NO;
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
            
            UIViewController *viewController = [self getTopMostViewController];
            [viewController dismissViewControllerAnimated:YES completion:nil];
            
            // If user on Runner or Opponent screen  screen is open....
            if ([viewController isKindOfClass:[FindRunnerViewController class]] || [viewController isKindOfClass:[OpponentsViewController class]] || [viewController isKindOfClass:[RunningViewController class]] || [viewController isKindOfClass:[InAppPurchaseViewController class]] || [viewController isKindOfClass:[ChatViewController class]] || [viewController isKindOfClass:[ActivityFeedViewController class]])
                return;

            // If channel list not loaded at least once.
            if ([TblChats fetchAllObjects].count < 1)
                [[APIRequest request] ChannelList:YES completed:nil];
        }
    }
    else
    {
        if ([UIApplication userId])
        {
            UIViewController *viewController = [self getTopMostViewController];

            if ([viewController isKindOfClass:[FindRunnerViewController class]] || [viewController isKindOfClass:[OpponentsViewController class]] || [viewController isKindOfClass:[RunningViewController class]] || [viewController isKindOfClass:[InAppPurchaseViewController class]] || [viewController isKindOfClass:[RunningMusicViewController class]] || [viewController isKindOfClass:[SelectMusicViewController class]] || [viewController isKindOfClass:[AlbumListViewController class]] || [viewController isKindOfClass:[MusicListViewController class]])
                    return;

            
            appDelegate.window.rootViewController = [self setTabBarController];
            [viewController.navigationController popToRootViewControllerAnimated:NO];

            
            if ([[dicNotification stringValueForJSON:@"type"] isEqualToString:CNotificationTypePubNub])
            {
                [appDelegate.objCustomTabBar btnTabBarClicked:appDelegate.objCustomTabBar.btnTab1];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (self.configureRefreshChatListScreen)
                        self.configureRefreshChatListScreen(YES);
                });
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [viewController.navigationController popToRootViewControllerAnimated:NO];
            });
        }
    }
        
}

-(void)vibrationOnNotification
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    //AudioServicesPlaySystemSound(0x00001000);
}

#pragma mark - Time Zone related function
-(void)convertDateIntoLocalFromGMT:(double)timeStamp
{
    NSString* LocalDate = [self convertedDateFromTimeStamp:timeStamp isGMT:NO formate:CDateFormater];
    NSLog(@"Local Date ======== >%@",LocalDate);
}

-(void)convertDateIntoGMTFromLocal:(double)timeStamp1
{
    NSDate *Date = [NSDate date];
    double timestamp = [Date timeIntervalSince1970];
    NSString *strGMTDate = [self convertedDateFromTimeStamp:timestamp isGMT:YES formate:CDateFormater];
    NSLog(@"GMT Date ======== >%@",strGMTDate);
    
    [self convertDateIntoLocalFromGMT:timestamp];
}


#pragma mark - Helper Method

-(void)keepAppInIdleMode:(BOOL)isIdle
{
    // For keep Application in idle mode.......
    [[UIApplication sharedApplication] setIdleTimerDisabled:isIdle];
}

-(UIViewController *)getTopMostViewController
{
    UIViewController *selectedViewController;

    UIViewController *viewController = [UIApplication topMostController];
    if ([viewController isKindOfClass:[UIAlertController class]])
        selectedViewController = (UIAlertController *)viewController;
    else if ([viewController isKindOfClass:[UITabBarController class]])
        selectedViewController = ((SuperNavigationController *)((UITabBarController *)viewController).selectedViewController).topViewController;
    else if ([viewController isKindOfClass:[SuperNavigationController class]])
        selectedViewController = ((SuperNavigationController *)viewController).topViewController;
    else
        selectedViewController = viewController;

    return selectedViewController;
    
}

-(void)hideKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

-(void)roundedImageView:(UIImageView *)imgView withSuperView:(UIView *)vwBorder
{
    vwBorder.layer.cornerRadius = vwBorder.frame.size.width/2;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    imgView.clipsToBounds = true;
    vwBorder.clipsToBounds = true;
}


// Convert comment count in 1.0K formate here....
-(NSString *)getCommentAndLikeCountWithFormate:(NSString *)strCount
{
//    strCount = @"86548";
    int comment = strCount.intValue;
    
    if (comment > 999)
    {
        int count = comment/1000; // Main Count
        
        int countModule = comment%1000;
        int countSubModule = countModule/100; // Sub Count
        strCount = [NSString stringWithFormat:@"%d.%@",count,[NSString stringWithFormat:@"%dK",countSubModule]];
    }
    
    NSLog(@"%@",strCount);
    
    return strCount;
}

// Convert Post time in specific formate here.....
- (NSString *)getPostTimeInSpecificFormate:(NSString *)strDate;
{
    NSDate *date = [self convertDateFromString:strDate isGMT:YES formate:CDateFormater];
    double addedTimeStamp = [date timeIntervalSince1970];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];

    int secondsLeft = (currentTimestamp - addedTimeStamp);
    int minutes = secondsLeft/60;
    int hours = minutes/60;
    int days = hours/24;
    int week = days/7;
    int months = days/30;
    int years = months/12;
    
    NSString *strTime;
    
    if (years > 0)
        strTime = (years == 1) ? [NSString stringWithFormat:@"%d year ago", years] : [NSString stringWithFormat:@"%d years ago", years]  ;
    else if (months > 0)
        strTime = (months == 1)  ? [NSString stringWithFormat:@"%d month ago", months] : [NSString stringWithFormat:@"%d months ago", months];
    else if (week > 0)
        strTime = (week == 1) ? [NSString stringWithFormat:@"%d week ago", week] : [NSString stringWithFormat:@"%d weeks ago", week];
    else if (days > 0)
        strTime = (days == 1) ? [NSString stringWithFormat:@"%d day ago", days] : [NSString stringWithFormat:@"%d days ago", days];
    else if (hours > 0)
        strTime = (hours == 1) ? [NSString stringWithFormat:@"%d hour ago", hours] : [NSString stringWithFormat:@"%d hours ago", hours];
    else if (minutes > 0)
        strTime = (minutes == 1) ?  [NSString stringWithFormat:@"%d minute ago", minutes] : [NSString stringWithFormat:@"%d minutes ago", minutes];
    else if (secondsLeft > 0)
        strTime = (secondsLeft == 1) ? [NSString stringWithFormat:@"%d second ago", secondsLeft] : [NSString stringWithFormat:@"%d seconds ago", secondsLeft];
    else
        strTime = @"Just now";
    
    return strTime;
}

-(NSString *)convertKMToMeter:(NSString *)strMeter IsKM:(BOOL)isKm
{
    NSString *strDistance;
    if (isKm)
    {
        // Get Distance in KM...
        strDistance = [NSString stringWithFormat:@"%.2f",strMeter.floatValue / 1000];
    }
    else
    {
        // Get Distance in Meter...
        strDistance = [NSString stringWithFormat:@"%.2f",strMeter.floatValue * 1000];
    }
    
    return strDistance;
}

-(NSString *)getDistanceInWithSelectedFormate:(NSString *)strDistance
{
    /*
     Metric

     Distance - KM
     Weight - KG
     Height - CM
     */
    
    /*
     Imperial
     
     Distance - miles
     Weight - pounds
     Height - feet
     */
    
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric]) // If Unit formate is in Metric.....
        strDistance = [NSString stringWithFormat:@"%.1f KM",strDistance.floatValue / 1000];
    else
    {
        if (strDistance.floatValue * 0.000621371 == 1)
            strDistance = [NSString stringWithFormat:@"%.1f Mile",strDistance.floatValue * 0.000621371];
        else
            strDistance = [NSString stringWithFormat:@"%.1f Miles",strDistance.floatValue * 0.000621371];
            
    }
    
    return strDistance;
}

-(NSString *)convertCaloriesWithSelectedFormate:(NSString *)calories
{
    //Defualt == >> KCAL
    NSString *strFinalCal;
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
    {
        //EST KCALS
        strFinalCal = [NSString stringWithFormat:@"%.2f",calories.floatValue];
    }
    else
    {
        // EST CALS
//        strFinalCal = [NSString stringWithFormat:@"%.2f",calories.floatValue*1000];
        strFinalCal = [NSString stringWithFormat:@"%.2f",calories.floatValue];
    }
    
    return strFinalCal;
}

- (NSString *)convertSpeedInWithSelectedFormate:(float)meters overTime:(NSString *)strTotalTime
{
    int seconds = 0;
    
    NSArray *arrTime = [strTotalTime componentsSeparatedByString:@":"];
    if (arrTime.count > 1)
    {
        NSString *strHourSec = arrTime[0];
        NSString *strMintSec = arrTime[1];
        NSString *strSec = arrTime[2];
        seconds = (strHourSec.intValue * 60 * 60) + (strMintSec.intValue * 60) + strSec.intValue;
    }
    
    if (seconds == 0 || meters == 0)
    {
        return @"00:00";
    }
    
    float avgPaceSecMeters = seconds / meters;
    
    float unitMultiplier;
    NSString *unitName;
    
    // metric
    if ([appDelegate.loginUser.units isEqualToString:CDistanceMetric])
    {
        unitName = @"min/km";
        unitMultiplier = 1000;
        // U.S.
    } else {
        unitName = @"min/mi";
        unitMultiplier = 1609.344;
    }
    
    int paceMin = (int) ((avgPaceSecMeters * unitMultiplier) / 60);
    int paceSec = (int) (avgPaceSecMeters * unitMultiplier - (paceMin*60));
    //    return [NSString stringWithFormat:@"%i:%02i %@", paceMin, paceSec, unitName];
    
    return [NSString stringWithFormat:@"%i:%02i", paceMin, paceSec];
}

-(UIImage *)GetImageWithUserPosition:(int)position
{
    UIImage *image;
    
    switch (position)
    {
        case 1:
            image = [UIImage imageNamed:@"ic_player_rank1"];
            break;
        case 2:
            image = [UIImage imageNamed:@"ic_player_rank2"];
            break;
        case 3:
            image = [UIImage imageNamed:@"ic_player_rank3"];
            break;
        case 4:
            image = [UIImage imageNamed:@"ic_player_rank4"];
            break;
        case 5:
            image = [UIImage imageNamed:@"ic_player_rank5"];
            break;
        case 6:
            image = [UIImage imageNamed:@"ic_player_rank6"];
            break;
        case 7:
            image = [UIImage imageNamed:@"ic_player_rank7"];
            break;
        case 8:
            image = [UIImage imageNamed:@"ic_player_rank8"];
            break;

        default:
            break;
    }
    
    return image;
}

-(UIImage *)GetImageWithUserPerformance:(NSString *)strPosition
{
    UIImage *image;
    
    switch (strPosition.intValue)
    {
        case 1:
            // High Performance
            image = [UIImage imageNamed:@"ic_Activity_Leader_green"];
            break;
        case 2:
            //  Performance not changed
            image = [UIImage imageNamed:@"ic_Activity_Leader_grey"];
            break;
        case 3:
            //  Low Performance
            image = [UIImage imageNamed:@"ic_Activity_Leader_red"];
            break;
        default:
            image = [UIImage imageNamed:@"ic_Activity_Leader_grey"];
            break;
    }
    
    return image;
}

#pragma mark - User Profile
-(void)logoutUser
{
    [[FRDStravaClient sharedInstance] setAccessToken:nil];
    [self MQTTDisconnetFromServer];
    appDelegate.loginUser = nil;
    [CUserDefaults removeObjectForKey:CUserId];
    [CUserDefaults removeObjectForKey:CIAPAppleRecieptID];
    
    [UIApplication removeUserId];
    [TblUser deleteAllObjects];
    appDelegate.loginUser = nil;
    
    [TblActivityPhoto deleteAllObjects];
    [TblActivityVideo deleteAllObjects];
    [TblActivitySyncSession deleteAllObjects];
    [TblActivityRunSession deleteAllObjects];
    [TblActivityCompleteSession deleteAllObjects];
    [TblActivityDates deleteAllObjects];
    [[Store sharedInstance].mainManagedObjectContext save];
    
    appDelegate.window.rootViewController = appDelegate.objSuperIntro;
    appDelegate.objTabBarController = nil;
}

- (void)moveOnProfileScreen:(NSString *)userName UserId:(NSString *)userId andNavigation:(UINavigationController *)navigation isLoginUser:(BOOL)isLogin;
{
    NSString *firstLetter = [userName substringWithRange:NSMakeRange(0, 1)];
    if ([firstLetter isEqualToString:@"@"]) // To remove "@" from string....
        userName = [userName substringFromIndex:1];
    
    if ([appDelegate.loginUser.user_name isEqualToString:userName])
    {
        MyProfileViewController *objOther = [[MyProfileViewController alloc] init];
        objOther.isBackButton = YES;
        [navigation pushViewController:objOther animated:YES];
    }
    else
    {
        OtherUserProfileViewController *objOther = [[OtherUserProfileViewController alloc] init];
        objOther.strUserId = userId;
        objOther.strUserName = userName;
        [navigation pushViewController:objOther animated:YES];
    }
}

#pragma mark - Spotify player
- (void)setSpotifyPlayer
{
    SPTAuth* auth = [SPTAuth defaultInstance];
    
    if (self.player == nil)
    {
        NSError *error = nil;
        self.player = [SPTAudioStreamingController sharedInstance];
        
        if ([self.player startWithClientId:auth.clientID audioController:nil allowCaching:YES error:&error])
        {
            self.player.delegate = self;
            self.player.playbackDelegate = self;
            self.player.diskCache = [[SPTDiskCache alloc] initWithCapacity:1024 * 1024 * 64];
            
            NSLog(@"auth session is: %d", auth.session.isValid);
            
            if ([self isSpotifySessionValid:auth])
            {
                [self.player loginWithAccessToken:auth.session.accessToken];
                NSLog(@"auth session is valid");
            }
            else
            {
                //Todo:Login again
                NSLog(@"Session is not valid");
            }
        }
        else
        {
            self.player = nil;
            
            UIViewController *viewController = [self getTopMostViewController];
            [viewController customAlertViewWithOneButton:@"Error init" Message:@"Something went wrong." ButtonText:@"OK" Animation:NO completed:nil];
            [self closeSession];
        }
    }
}

#pragma mark - Spotify Delegate Methods
#pragma mark -

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceiveMessage:(NSString *)message
{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message from Spotify" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alertView show];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangePlaybackStatus:(BOOL)isPlaying
{
    NSLog(@"is playing = %d", isPlaying);
    if (isPlaying)
        [appDelegate activateAudioSession];
    else
        [appDelegate deactivateAudioSession];
}

-(void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangeMetadata:(SPTPlaybackMetadata *)metadata
{
    //[self updateUI];
    
    if (self.configureTrackChanged)
        self.configureTrackChanged(metadata);
}

-(void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceivePlaybackEvent:(SpPlaybackEvent)event withName:(NSString *)name
{
    NSLog(@"didReceivePlaybackEvent: %zd %@", event, name);
    NSLog(@"isPlaying=%d isRepeating=%d isShuffling=%d isActiveDevice=%d positionMs=%f",
          self.player.playbackState.isPlaying,
          self.player.playbackState.isRepeating,
          self.player.playbackState.isShuffling,
          self.player.playbackState.isActiveDevice,
          self.player.playbackState.position);
}

- (void)audioStreamingDidLogout:(SPTAudioStreamingController *)audioStreaming
{
    [self closeSession];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceiveError:(NSError* )error
{
    NSLog(@"didReceiveError: %zd %@", error.code, error.localizedDescription);
    if (error.code == SPErrorNeedsPremium)
    {
        UIViewController *viewController = [self getTopMostViewController];
        [viewController customAlertViewWithOneButton:@"Premium account required" Message:@"Premium account is required to showcase application functionality. Please login using premium account." ButtonText:@"OK" Animation:NO completed:nil];
    }
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangePosition:(NSTimeInterval)position
{
    if (self.isChangingProgress)
        return;
    
    if(self.configureDidChangePosition)
        self.configureDidChangePosition(position);
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didStartPlayingTrack:(NSString *)trackUri
{
    BOOL isRelinked = [self.player.metadata.currentTrack.playbackSourceUri containsString: @"spotify:track"]
    && ![self.player.metadata.currentTrack.playbackSourceUri isEqualToString:trackUri];
    NSLog(@"Relinked %d", isRelinked);
    
    if (self.configureStartedPlaying)
        self.configureStartedPlaying();
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didStopPlayingTrack:(NSString *)trackUri
{
    NSLog(@"Finishing: %@", trackUri);
    
    if (self.configureTrackStopped)
        self.configureTrackStopped(NO);
}

- (void)audioStreamingDidLogin:(SPTAudioStreamingController *)audioStreaming
{
    //[self updateUI];
    
    if(self.configureSpotifyLogin)
        self.configureSpotifyLogin(nil);
    
    [self.player playSpotifyURI:self.strMainURI startingWithIndex:0 startingWithPosition:0 callback:^(NSError *error)
     {
         if (error != nil)
         {
             NSLog(@"*** failed to play: %@", error);
             
             return;
         }
         
         if(self.configureNewTrackStarted)
             self.configureNewTrackStarted(error);
     }];
}

#pragma mark - Audio Session
#pragma mark -

- (void)activateAudioSession
{
 //   return;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
}

- (void)deactivateAudioSession
{
    //return;
    //  [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
}

- (void)closeSession
{
    NSError *error = nil;
    
    if (![self.player stopWithError:&error])
    {
        UIViewController *viewController = [self getTopMostViewController];
        [viewController customAlertViewWithOneButton:@"Error init" Message:@"" ButtonText:@"OK" Animation:NO completed:nil];
    }
    
    [SPTAuth defaultInstance].session = nil;
}

- (BOOL)isSpotifySessionValid:(SPTAuth*)auth
{
    return auth.session.isValid && auth.session;
}

#pragma mark - Pubnub SDK
#pragma mark -

- (void)configurePubnub
{
    [self pubNubInit];
}

- (void)pubNubInit
{
    
    if (!appDelegate.loginUser)
        return;
    
    // Initialize PubNub client.
    self.myConfig = [PNConfiguration configurationWithPublishKey:CPubnubPublishKey subscribeKey:CPubnubSubscribleKey];
    
    self.myConfig.uuid = [NSString stringWithFormat:@"%@_%@", CApplicationName, appDelegate.loginUser.user_id];
    
    // Set PubNub Configuration
    self.myConfig.TLSEnabled = NO;
    self.myConfig.origin = @"pubsub.pubnub.com";
    self.myConfig.authKey = @"Runlive_ios";
    
    // Presence Settings
//    self.myConfig.presenceHeartbeatValue = 120;
//    self.myConfig.presenceHeartbeatInterval = 30;
    
    // Cipher Key Settings
    // self.client.cipherKey = @"enigma"; # Set this to enable PN AES encryption
    
    // Time Token Handling Settings
    self.myConfig.keepTimeTokenOnListChange = YES;
    self.myConfig.catchUpOnSubscriptionRestore = YES;
    
    // Bind config
    self.client = [PubNub clientWithConfiguration:self.myConfig];
    
    // Configure logger
    self.client.logger.enabled = YES;
    self.client.logger.writeToFile = YES;
    self.client.logger.maximumLogFileSize = (10 * 1024 * 1024);
    self.client.logger.maximumNumberOfLogFiles = 10;
    [self.client.logger setLogLevel:PNVerboseLogLevel];
    
    // Bind didReceiveMessage, didReceiveStatus, and didReceivePresenceEvent 'listeners' to this delegate
    // just be sure the target has implemented the PNObjectEventListener extension
    
    [self.client addListener:self];
    //[self pubNubSetState];
}

-(void)SetPubNubClientTypingState:(BOOL)isTyping Channel:(NSString *)strChannelId
{
    [self.client setState:@{@"typing":isTyping ? @"true" : @"false",@"user_name":appDelegate.loginUser.user_name,@"user_id":appDelegate.loginUser.user_id,@"channel_id":strChannelId} forUUID:self.client.uuid onChannel:strChannelId withCompletion:^(PNClientStateUpdateStatus * _Nonnull status)
     {
         if (!status.isError)
         {
             NSLog(@"Typing state ===============%@",status.data);
         }
         else {
         }
     }];
}


-(void)SetPubNubClientState:(NSString *)state
{
    if (!appDelegate.loginUser)
        return;
    
    NSArray *arrChats = appDelegate.loginUser.chats.allObjects;
    
    for (int i = 0; arrChats.count > i; i++)
    {
        TblChats *objChat = arrChats[i];

        [self.client setState:@{@"state":state,@"user_name":appDelegate.loginUser.user_name,@"user_id":appDelegate.loginUser.user_id,@"channel_id":objChat.channelid} forUUID:self.client.uuid onChannel:objChat.channelid withCompletion:^(PNClientStateUpdateStatus * _Nonnull status)
         {
             if (!status.isError)
             {
                 // Client state successfully modified on specified channel.
                 
//                 NSLog(@"Client state =========%@=======%@",state,objChat.loginuser.user_name);
             }
             // Request processing failed.
             else {
                 
                 // Handle client state modification error. Check 'category' property to find out possible
                 // issue because of which request did fail.
                 //
                 // Request can be resent using: [status retry];
             }
         }];
    }
}

- (void)pubNubAddPushNotifications
{
    if(!appDelegate.loginUser)
        return;
    
    if (!appDelegate.loginUser || ![CUserDefaults dataForKey:CDeviceTokenPubNub])
        return;
    
    [[APIRequest request] ChannelList:NO completed:^(id responseObject, NSError *error) {
        [self SetPubNubClientState:@"online"];
        
        NSArray *arrChats = appDelegate.loginUser.chats.allObjects;
        
        [self.client subscribeToChannels:[arrChats valueForKeyPath:@"channelid"] withPresence:YES];
        
        if (arrChats.count > 0)
        {
            [self addPushNotificationToPubnubChannels:[arrChats valueForKeyPath:@"channelid"]];
        }
    }];
}

-(void)addPushNotificationToPubnubChannels:(NSArray *)arrChannels
{
    
    NSLog(@"Pub nub Device token === >> %@",[CUserDefaults dataForKey:CDeviceTokenPubNub]);
    __weak typeof(self) weakSelf = self;
    [self.client addPushNotificationsOnChannels:arrChannels withDevicePushToken:[CUserDefaults dataForKey:CDeviceTokenPubNub] andCompletion:^(PNAcknowledgmentStatus * _Nonnull status)
     {
         if (!status.isError)
         {
             NSLog(@"Successfully registered device token for the push notification");
             // [self pubNubGetState];
             [weakSelf pubNubGetAllPushNotificationsChannel];
         }
         else
         {
             NSLog(@"Getting some error while registering the device token in the pubnub");
         }
     }];
}

- (void)pubNubRemoveAllPushNotifications
{
    [self.client removeAllPushNotificationsFromDeviceWithPushToken:[CUserDefaults dataForKey:CDeviceTokenPubNub] andCompletion:^(PNAcknowledgmentStatus * _Nonnull status)
    {
    }];
}

- (void)pubNubGetAllPushNotificationsChannel
{
    // To get all Enabled puchNotification Channel from PubNub.........
    [self.client pushNotificationEnabledChannelsForDeviceWithPushToken:[CUserDefaults dataForKey:CDeviceTokenPubNub] andCompletion:^(PNAPNSEnabledChannelsResult *result, PNErrorStatus *status)
    {
        if (!status)
        {
            NSLog(@"Getting All enables pushnotification Channel from Pubnub==== %@ ",result);
        }
        else
        {
            NSLog(@"Getting error enables pushnotification Channel from Pubnub ");
        }
    }];
   }

-(void)showMessageBadgeOnTabBar
{
    if (appDelegate.loginUser)
    {
        NSArray *arrMessage = [TblChats fetch:[NSPredicate predicateWithFormat:@"loginuser == %@ && isChatDeleted == %@ && isReadMessage == %@", appDelegate.loginUser,@NO,@NO] sortedBy:nil];
        
        if (arrMessage.count > 0)
            self.objCustomTabBar.viewTabMessageBadge.hidden = NO;
        else
            self.objCustomTabBar.viewTabMessageBadge.hidden = YES;
        
        [self setNotificationBadgeOnAppIcon];
    }
}



#pragma mark - Streaming Data didReceiveMessage Listener
#pragma mark -

- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message
{
    NSLog(@"Check client didReceiveMessage === %@",message);
    
    if (![message.data.channel isEqualToString:message.data.subscription])
    {
        // Message has been received on channel group stored in message.data.subscription.
    }
    else
    {
        // Message has been received on channel stored in message.data.channel.
    }
    
    if (message)
    {
//        NSLog(@"Received message from '%@': %@ on channel %@ at %@", message.data.publisher, message.data.message, message.data.channel, message.data.timetoken);
        
        NSDictionary *dicMsg = message.data.message;
        
        TblMessages *objMsg = (TblMessages *)[TblMessages findOrCreate:@{@"channelId": [dicMsg stringValueForJSON:@"channelId"], @"timestamp": [dicMsg numberForJson:@"timestamp"]}];
        
        objMsg.timestamp = [dicMsg numberForJson:@"timestamp"];
        objMsg.msgtext = [dicMsg stringValueForJSON:@"msgtext"];
        objMsg.msgdate = [dicMsg stringValueForJSON:@"msgdate"];
        objMsg.userid = [dicMsg stringValueForJSON:@"userid"];
        objMsg.msgtype = [dicMsg stringValueForJSON:@"msgType"];
        objMsg.imgname = [dicMsg stringValueForJSON:@"imgName"];
        objMsg.msgtype = [dicMsg stringValueForJSON:@"msgtype"];
        objMsg.imgurl = [NSString stringWithFormat:@"%@%@",BASEURL,[dicMsg stringValueForJSON:@"imgurl"]];
        objMsg.imgheight = [dicMsg stringValueForJSON:@"imgheight"];
        objMsg.imgwidth = [dicMsg stringValueForJSON:@"imgwidth"];
        objMsg.is_video = [dicMsg stringValueForJSON:@"is_video"].intValue == 0 ? @NO : @YES;
        objMsg.msgdate = [dicMsg stringValueForJSON:@"msgdate"];
        objMsg.channelId = [dicMsg stringValueForJSON:@"channelId"];
        objMsg.userimgurl = [dicMsg stringValueForJSON:@"userimgurl"];
        
       [[[Store sharedInstance] mainManagedObjectContext] save];
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
            if (self.configureGotMessage)
                self.configureGotMessage(dicMsg);
        }
    }
}

#pragma mark - Streaming Data didReceivePresenceEvent Listener
#pragma mark -

- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event
{
    if (![event.data.channel isEqualToString:event.data.subscription])
    {
        // Presence event has been received on channel group stored in event.data.subscription.
    }
    else
    {
        // Presence event has been received on channel stored in event.data.channel.
    }
    
    
    if (![event.data.presenceEvent isEqualToString:@"state-change"])
    {
//        NSLog(@"%@ \"%@'ed\"\nat: %@ on %@ (Occupancy: %@)", event.data.presence.uuid,
//              event.data.presenceEvent, event.data.presence.timetoken, event.data.channel,
//              event.data.presence.occupancy);
    }
    else
    {
//        NSLog(@"%@ changed state at: %@ on %@ to: %@", event.data.presence.uuid,event.data.presence.timetoken, event.data.channel, event.data.presence.state);
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
            NSDictionary *dicUserStats = event.data.presence.state;
            
            if (![[dicUserStats stringValueForJSON:@"user_name"] isEqualToString:appDelegate.loginUser.user_name])
            {
                NSArray *arrChannel = [TblChats fetch:[NSPredicate predicateWithFormat:@"channelid == %@", [dicUserStats stringValueForJSON:@"channel_id"]] sortedBy:nil];
                
                if (arrChannel.count > 0)
                {
                    TblChats *objChat = arrChannel[0];
                    
                    if ([dicUserStats objectForKey:@"typing"])
                    {
                        // Save Typing stats data..
                        
                        NSMutableArray *arrTypingUsers = objChat.typingUser.mutableCopy;
                        NSArray *arrCheckingUser = [arrTypingUsers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_name == %@", [dicUserStats stringValueForJSON:@"user_name"]]];
                        
                        if (arrCheckingUser.count > 0)
                            [arrTypingUsers removeObject:arrCheckingUser[0]];
                        
                        if ([[dicUserStats stringValueForJSON:@"typing"] isEqualToString:@"true"])
                            [arrTypingUsers addObject:dicUserStats];
                        
                        objChat.typingUser = arrTypingUsers;
                        
                        if (arrTypingUsers.count == 1)
                        {
                            objChat.typingMessage = @"1 person is typing...";
                        }
                        else
                        {
                            if (arrTypingUsers.count > 1)
                            {
                                int userTyper = (int)arrTypingUsers.count;
                                
                                int totalTypingUser = userTyper/10;
                                int totalTypingModuleUser = userTyper%10;
                                
                                if (userTyper> 100) // If more then 100 users are typing...
                                {
                                    totalTypingUser = userTyper/100;
                                    totalTypingModuleUser = userTyper%100;
                                }
                                
                                if ((totalTypingModuleUser < 10) && totalTypingUser == 0)
                                {
                                    objChat.typingMessage = [NSString stringWithFormat:@"%d people are typing...",totalTypingModuleUser];
                                }
                                else
                                {
                                    if (totalTypingModuleUser == 0)
                                        objChat.typingMessage = [NSString stringWithFormat:@"%d people are typing...",userTyper];
                                    else
                                        objChat.typingMessage = [NSString stringWithFormat:@"%d+ people are typing...",userTyper-totalTypingModuleUser];
                                }
                            }
                        }
                    }
                    else
                    {
                        // Save onlince stats
                        objChat.isOnline = [[dicUserStats stringValueForJSON:@"state"] isEqualToString:@"online"] ? @YES : @NO;
                    }
                    [[Store sharedInstance].mainManagedObjectContext save];
                    
                    if (self.configureShowTypingIndicator)
                        self.configureShowTypingIndicator(objChat);
                }
            }
        }
    }
    
}

#pragma mark - Streaming Data didReceiveStatus Listener
#pragma mark -

- (void)client:(PubNub *)client didReceiveStatus:(PNStatus *)status
{
    // This is where we'll find ongoing status events from our subscribe loop
    // Results (messages) from our subscribe loop will be found in didReceiveMessage
    // Results (presence events) from our subscribe loop will be found in didReceiveStatus
    
    [self handleStatus:status];
}

#pragma mark - example status handling
#pragma mark -

- (void)handleStatus:(PNStatus *)status
{
    //    Two types of status events are possible. Errors, and non-errors. Errors will prevent normal operation of your app.
    //    If this was a subscribe or presence PAM error, the system will continue to retry automatically.
    //    If this was any other operation, you will need to manually retry the operation.
    //    You can always verify if an operation will auto retry by checking status.willAutomaticallyRetry
    //    If the operation will not auto retry, you can manually retry by calling [status retry]
    //    Retry attempts can be cancelled via [status cancelAutomaticRetry]
    
    if (status.isError)
        [self handleErrorStatus:(PNErrorStatus *)status];
    else
        [self handleNonErrorStatus:status];
}

- (void)handleErrorStatus:(PNErrorStatus *)status
{
    NSLog(@"^^^^ Debug: %@", status.debugDescription);
    if (status.category == PNAccessDeniedCategory)
    {
//        NSLog(@"^^^^ handleErrorStatus: PAM Error: for resource Will Auto Retry?: %@", status.willAutomaticallyRetry ? @"YES" : @"NO");
//        [self handlePAMError:status];
    }
    else if (status.category == PNDecryptionErrorCategory)
    {
        NSLog(@"Decryption error. Be sure the data is encrypted and/or encrypted with the correct cipher key.");
        NSLog(@"You can find the raw data returned from the server in the status.data attribute: %@", status.associatedObject);
        if (status.operation == PNSubscribeOperation)
        {
//            NSLog(@"Decryption failed for message from channel: %@\nmessage: %@",((PNMessageData *)status.associatedObject).channel,((PNMessageData *)status.associatedObject).message);
        }
    }
    else if (status.category == PNMalformedFilterExpressionCategory)
    {
        NSLog(@"Value which has been passed to -setFilterExpression: malformed.");
        NSLog(@"Please verify specified value with declared filtering expression syntax.");
    }
    else if (status.category == PNMalformedResponseCategory)
    {
        NSLog(@"We were expecting JSON from the server, but we got HTML, or otherwise not legal JSON.");
        NSLog(@"This may happen when you connect to a public WiFi Hotspot that requires you to auth via your web browser first,");
        NSLog(@"or if there is a proxy somewhere returning an HTML access denied error, or if there was an intermittent server issue.");
    }
    else if (status.category == PNTimeoutCategory)
    {
        NSLog(@"For whatever reason, the request timed out. Temporary connectivity issues, etc.");
    }
    else if (status.category == PNNetworkIssuesCategory)
    {
        NSLog(@"Request can't be processed because of network issues.");
    }
    else
    {
        // Aside from checking for PAM, this is a generic catch-all if you just want to handle any error, regardless of reason
        // status.debugDescription will shed light on exactly whats going on
        NSLog(@"Request failed... if this is an issue that is consistently interrupting the performance of your app,");
//        NSLog(@"email the output of debugDescription to support along with all available log info: %@", [status debugDescription]);
    }
    if (status.operation == PNHeartbeatOperation)
    {
        NSLog(@"Heartbeat operation failed.");
    }
}

- (void)handlePAMError:(PNErrorStatus *)status
{
// Access Denied via PAM. Access status.data to determine the resource in question that was denied.
// In addition, you can also change auth key dynamically if needed."
    
    NSString *pamResourceName = status.errorData.channels ? status.errorData.channels.firstObject :
    status.errorData.channelGroups.firstObject;
    NSString *pamResourceType = status.errorData.channels ? @"channel" : @"channel-groups";
    
//    NSLog(@"PAM error on %@ %@", pamResourceType, pamResourceName);
    
// If its a PAM error on subscribe, lets grab the channel name in question, and unsubscribe from it, and re-subscribe to a channel that we're authed to
    
    if (status.operation == PNSubscribeOperation)
    {
        if ([pamResourceType isEqualToString:@"channel"])
        {
//            NSLog(@"^^^^ Unsubscribing from %@", pamResourceName);
            [self reconfigOnPAMError:status];
        }
        else
        {
            [self.client unsubscribeFromChannelGroups:@[pamResourceName] withPresence:YES];
        }
    }
    else if (status.operation == PNPublishOperation)
    {
//        NSLog(@"^^^^ Error publishing with authKey: %@ to channel %@.", _authKey, pamResourceName);
        NSLog(@"^^^^ Setting auth to an authKey that will allow for both sub and pub");
        [self reconfigOnPAMError:status];
    }
}

- (void)reconfigOnPAMError:(PNErrorStatus *)status
{
    // If this is a subscribe PAM error
    
    if (status.operation == PNSubscribeOperation)
    {
        PNSubscribeStatus *subscriberStatus = (PNSubscribeStatus *)status;
        
        NSArray *currentChannels = subscriberStatus.subscribedChannels;
        NSArray *currentChannelGroups = subscriberStatus.subscribedChannelGroups;
        
        [self.client copyWithConfiguration:self.myConfig completion:^(PubNub *client){
            self.client = client;
            [self.client subscribeToChannels:currentChannels withPresence:NO];
            [self.client subscribeToChannelGroups:currentChannelGroups withPresence:NO];
        }];
    }
}

- (void)handleNonErrorStatus:(PNStatus *)status
{
    
    // This method demonstrates how to handle status events that are not errors -- that is,
    // status events that can safely be ignored, but if you do choose to handle them, you
    // can get increased functionality from the client
    
    if (status.category == PNAcknowledgmentCategory)
    {
        NSLog(@"^^^^ Non-error status: ACK");
        // For methods like Publish, Channel Group Add|Remove|List, APNS Add|Remove|List
        // when the method is executed, and completes, you can receive the 'ack' for it here.
        // status.data will contain more server-provided information about the ack as well.
    }
    
    if (status.operation == PNSubscribeOperation)
    {
        PNSubscribeStatus *subscribeStatus = (PNSubscribeStatus *)status;
        NSLog(@"PNSubscribeStatus ==== %@",subscribeStatus);
        // Specific to the subscribe loop operation, you can handle connection events
        // These status checks are only available via the subscribe status completion block or
        // on the long-running subscribe loop listener didReceiveStatus
        
        // Connection events are never defined as errors via status.isError
        if (status.category == PNUnexpectedDisconnectCategory)
        {
            // PNUnexpectedDisconnect happens as part of our regular operation
            // This event happens when radio / connectivity is lost
            
//            [self MQTTDisconnetFromServer];
//            NSLog(@"^^^^ Non-error status: Unexpected Disconnect, Channel Info: %@",
//                  subscribeStatus.subscribedChannels);
        }
        
        else if (status.category == PNConnectedCategory)
        {
            // Connect event. You can do stuff like publish, and know you'll get it.
            // Or just use the connected event to confirm you are subscribed for UI / internal notifications, etc
            
            // NSLog(@"Subscribe Connected to %@", status.data[@"channels"]);
            
//            NSLog(@"^^^^ Non-error status: Connected, Channel Info: %@",
//                  subscribeStatus.subscribedChannels);
            //[self pubNubPublish];
            
        }
        else if (status.category == PNReconnectedCategory)
        {
//            NSLog(@"^^^^ Non-error status: Reconnected, Channel Info: %@",
//                  subscribeStatus.subscribedChannels);
        }
    }
    else if (status.operation == PNUnsubscribeOperation)
    {
        if (status.category == PNDisconnectedCategory)
        {
            // PNDisconnect happens as part of our regular operation
            // No need to monitor for this unless requested by support
            NSLog(@"^^^^ Non-error status: Expected Disconnect");
        }
    }
    else if (status.operation == PNHeartbeatOperation)
    {
        NSLog(@"Heartbeat operation successful.");
    }
}

#pragma mark - Configuration
#pragma mark -

- (NSString *)randomString
{
    return [NSString stringWithFormat:@"%d", arc4random_uniform(74)];
}

#pragma mark -
#pragma mark - Push Notification enable/disable

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground ======== " );
    // custom code to handle push while app is in the foreground
   // NSLog(@"%@", notification.request.content.userInfo);
    [self actionOnPubNubNotification:notification.request.content.userInfo];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed ========== " );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    //NSLog(@"%@", response.notification.request.content.userInfo);
    [self actionOnPubNubNotification:response.notification.request.content.userInfo];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo
{
    NSLog(@"Getting Punbun notificaiton here =========== > %@",userInfo);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"Remote notification device token: %@", deviceToken);
    
    const unsigned *devTokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *token = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                       ntohl(devTokenBytes[0]), ntohl(devTokenBytes[1]), ntohl(devTokenBytes[2]),
                       ntohl(devTokenBytes[3]), ntohl(devTokenBytes[4]), ntohl(devTokenBytes[5]),
                       ntohl(devTokenBytes[6]), ntohl(devTokenBytes[7])];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if(deviceToken)
    {
//        NSData* tokenData = [token dataUsingEncoding:NSUTF8StringEncoding];
        [CUserDefaults setObject:deviceToken forKey:CDeviceTokenPubNub];
        [CUserDefaults synchronize];
        [self pubNubAddPushNotifications];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Device is failed to be registered remote notification..... %@", error.localizedDescription);
}

- (void)disableRemoteNotification
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void)enableRemoteNotification
{
    float systemVersion = [IosFullVersion floatValue];
    
    if (systemVersion >= 10)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error )
            {
                if (self.configureNotificationPermissionStatus)
                    self.configureNotificationPermissionStatus(granted);
                    
                dispatch_async(GCDMainThread, ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
}


#pragma mark - Image Related Functions
#pragma mark -

- (NSString*)createSongImagePath
{
    NSError *error;
    
    NSString *strImagePath = CSongImagePath;
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:strImagePath])
        [[NSFileManager defaultManager] createDirectoryAtPath:strImagePath withIntermediateDirectories:NO attributes:nil error:&error]; //
    
    return strImagePath;
}


- (void)deleteSongImagePath
{
    NSError *error;
    
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:CSongImagePath error:&error])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", CSongImagePath, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}


- (NSString*)createAlbumImagePath
{
    NSError *error;
    
    NSString *strImagePath = CAlbumImagePath;
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:strImagePath])
        [[NSFileManager defaultManager] createDirectoryAtPath:strImagePath withIntermediateDirectories:NO attributes:nil error:&error]; //
    
    return strImagePath;
}


- (void)deleteAlbumImagePath
{
    NSError *error;
    
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:CSongImagePath error:&error])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", CAlbumImagePath, file] error:&error];
        if (!success || error) {
            // it failed.
        }
    }
}


-(NSURL *)resizeImage:(NSString *)width Height:(NSString *)height imageURL:(NSString *)url
{
    NSURL *imageUrl;
    
    //?width=200&height=200
    
    if (width)
        imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@?dim=%@",url,width]];
    else
        imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@?dim=%@",url,height]];
     
    
    return imageUrl;
}




#pragma mark - Live Session Related Functions

-(void)QuitJoinedLiveRunSession:(NSString *)strSessionId completed:(void (^)(id responseObject,NSError *error))completion
{
    [[APIRequest request] quitLiveSession:strSessionId completed:^(id responseObject, NSError *error)
     {
         if (completion)
             completion(responseObject,error);
     }];
}

-(void)createLiveSession:(NSDictionary *)dicData completed:(void (^)(id responseObject,NSError *error))completion
{
    [[APIRequest request] createLiveSession:dicData completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSDictionary *dicRes = [responseObject valueForKey:CJsonData];
             NSString *strMQTT = [NSString stringWithFormat:@"%@",[dicRes stringValueForJSON:@"_id"]];
             
             // Subscribe Session On MQTT
             [appDelegate MQTTSubscribeWithTopic:strMQTT];
         }
         
         if (completion)
             completion(responseObject,error);

     }];
}

-(void)toCheckRunningSessionIsAvailable
{
    UIViewController *viewController = [self getTopMostViewController];
    
    if (self.isSharingVideoPhotoOnFacebook) // While sharing video/photo on facebook.,,
        return;
    
    if (!appDelegate.loginUser || [viewController isKindOfClass:[RunningViewController class]] ||  [viewController isKindOfClass:[InAppPurchaseViewController class]] || [viewController isKindOfClass:[RunningMusicViewController class]] || [viewController isKindOfClass:[SelectMusicViewController class]] || [viewController isKindOfClass:[AlbumListViewController class]] || [viewController isKindOfClass:[MusicListViewController class]] || [viewController isKindOfClass:[PhotoVideoPreviewController class]] || [viewController isKindOfClass:[RunSummaryViewController class]] || [viewController isKindOfClass:[SessionFeedbackViewController class]] || [viewController isKindOfClass:[UserRunSummaryViewController class]] || [viewController isKindOfClass:[SLComposeViewController class]] || [viewController isKindOfClass:[UIImagePickerController class]] || [viewController isKindOfClass:[VideoCropViewController class]])
    {
        NSLog(@"May Be user not loged IN OR he will apear some ristricted screen ========== >>SLComposeViewController");
        return;
    }
    
    if (taskRuningForCheckingAvailableSession && taskRuningForCheckingAvailableSession.state == NSURLSessionTaskStateRunning)
    {
        NSLog(@"Allready checking for session wait until previous task has been completed ======== >>>>> ");
        return;
    }
    
    [viewController dismissViewControllerAnimated:NO completion:nil];
    
    taskRuningForCheckingAvailableSession = [[APIRequest request] getAllreadyRunningSeesion:^(id responseObject, NSError *error)
    {
        [taskRuningForCheckingAvailableSession cancel];
        UIViewController *viewController = [self getTopMostViewController];
        if (responseObject && !error)
        {
            // session is available
            if([[[responseObject valueForKey:CJsonData] numberForJson:@"status"] isEqual:@1])
            {
                NSString *strSessionId = [[responseObject valueForKey:CJsonData] stringValueForJSON:@"session_id"];
                [viewController customAlertViewWithTwoButton:@"" Message:@"We noticed that you lost the racing screen. Would you like to Re-Join this race?" ButtonFirstText:@"Ok" ButtonSecondText:@"Cancel" Animation:NO completed:^(int index) {
                    if (index == 0)
                    {
                        // Rejoin session
                        NSDictionary *dicData = [responseObject valueForKey:CJsonData];
                        if ([[dicData numberForJson:@"started"] isEqual:@0] && [[dicData stringValueForJSON:@"type"] isEqualToString:@"created"])
                        {
                            // Call Start api to start manual sessoin...
                            [self startManualSessionIfUserLostConnectivity:strSessionId];
                        }
                        else
                        {
                            [self MQTTInitialSetup];
                            appDelegate.window.rootViewController = appDelegate.objSuperRunning;
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [viewController.navigationController popToRootViewControllerAnimated:NO];
                                
                                if (appDelegate.configureRunningSessionStart)
                                    appDelegate.configureRunningSessionStart(strSessionId);
                            });
                        }
                    }
                    else
                    {
                        // Quit session
                        [[APIRequest request] competeQuitRunning:strSessionId Type:@"quit" RunCompleteType:@"2" completed:^(id responseObject, NSError *error)
                         {
                             RunSummaryViewController *objRunSummary = [[RunSummaryViewController alloc] init];
                             objRunSummary.strResultSessionId = strSessionId;
                             [viewController.navigationController pushViewController:objRunSummary animated:YES];
                         }];
                    }
                }];
            }
        }
        else
        {
            [self toCheckRunningSessionIsAvailable];
        }
    }];
}

-(void)startManualSessionIfUserLostConnectivity:(NSString *)session_id
{
    [[APIRequest request] toStartManualSessionRunning:session_id completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSDictionary *dicData = responseObject;
             if([[dicData stringValueForJSON:CJsonStatus] isEqualToString:CStatusOne])
             {
                 UIViewController *viewController = [self getTopMostViewController];
                 [self MQTTInitialSetup];
                 appDelegate.window.rootViewController = appDelegate.objSuperRunning;
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [viewController.navigationController popToRootViewControllerAnimated:NO];
                     
                     if (appDelegate.configureRunningSessionStart)
                         appDelegate.configureRunningSessionStart(session_id);
                 });

             }
             else
             {
                 [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:[dicData stringValueForJSON:CJsonMessage] ButtonText:@"Ok" Animation:YES completed:nil];
             }
         }
         else
         {
             [self startManualSessionIfUserLostConnectivity:session_id];
         }
     }];

}

-(void)startRunningWithSessionID:(NSString *)session_id
                 currentLocation:(CLLocation *)currentLocation
                       completed:(void (^)(NSString *strSessionId,BOOL isError))completion;
{
    
    [[APIRequest request] userCurrentRunningDetail:session_id completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            NSDictionary *dicRes = [responseObject valueForKey:CJsonData];
            
            if ([[dicRes numberForJson:CJsonStatus] isEqual:@0])
            {
                // Session Detail not found..
                if (completion)
                    completion(nil,NO);
            }
            else
            {
                
                // Subscribe Seesion here
                NSString *strTopic = [NSString stringWithFormat:@"run_%@",session_id];
                [self.objMQTTClient subscribe:strTopic withCompletionHandler:^(NSArray *grantedQos)
                 {
                 }];
                
                // Initial Publish Session here...
                NSMutableDictionary *dicPublishRun = [NSMutableDictionary new];
                [dicPublishRun setDouble:currentLocation.coordinate.latitude forKey:@"latitude"];
                [dicPublishRun setDouble:currentLocation.coordinate.longitude forKey:@"longitude"];
                [dicPublishRun setObject:session_id forKey:@"session_id"];
                [dicPublishRun setObject:appDelegate.loginUser.user_id forKey:@"user_id"];
                [dicPublishRun setObject:@"2" forKey:@"publish_status"];
                [dicPublishRun setObject:@"2" forKey:@"running_status"];
                [dicPublishRun setObject:@"0" forKey:@"sound_alert"];
                
                [dicPublishRun setObject:[dicRes stringValueForJSON:@"total_distance"] ? [dicRes stringValueForJSON:@"total_distance"] : @"0" forKey:@"total_distance"];
                [dicPublishRun setObject:[dicRes stringValueForJSON:@"position"] ? [dicRes stringValueForJSON:@"position"] : @"0" forKey:@"position"];
                
                // Average speed
                [dicPublishRun setObject:[dicRes stringValueForJSON:@"average_speed"] ? [dicRes stringValueForJSON:@"average_speed"] : @"0" forKey:@"average_speed"];
                
                // Average HeartBeat
                [dicPublishRun setObject:@"0" forKey:@"average_bpm"];
                
                // Average Calorie
                [dicPublishRun setObject:[dicRes stringValueForJSON:@"total_calories"] ? [dicRes stringValueForJSON:@"total_calories"] : @"0" forKey:@"total_calories"];
                
                // Total Time
                [dicPublishRun setObject:[dicRes stringValueForJSON:@"total_time"] ? [dicRes stringValueForJSON:@"total_time"] : @"00:00:00" forKey:@"total_time"];
                
                NSError * err;
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dicPublishRun options:0 error:&err];
                [appDelegate MQTTPublishWithTopic:jsonData Topic:strTopic];
                
                
                if (![[dicRes stringValueForJSON:@"locality"] isBlankValidationPassed] && ![[dicRes stringValueForJSON:@"sub_locality"] isBlankValidationPassed])
                {
                    [self getLocationNameFromLatLong:currentLocation.coordinate.latitude
                                             andLong:currentLocation.coordinate.longitude
                                           completed:^(NSDictionary *dicAddress)
                     {
                         [[APIRequest request] addLocalityAndSubLocality:[dicAddress stringValueForJSON:@"City"] SubLocality:[dicAddress stringValueForJSON:@"SubLocality"] SessionId:session_id completed:^(id responseObject, NSError *error)
                          {
                              //
                              if (completion)
                                  completion(session_id,NO);
                          }];
                     }];
                }
                else
                {
                    if (completion)
                        completion(session_id,NO);
                }
            }
        }
        else
        {
            if (completion)
                completion(nil,YES);
        }
    }];
}

// Start Running for Manual session here........
-(void)showCountDownScreenForManualSession:(NSString *)session_id completed:(void (^)(id responseObject,NSError *error))completion
{
    if (!session_id)
        return;
    
    [[APIRequest request] toStartManualSessionRunning:session_id completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             [self.objTabBarController dismissOverlayViewControllerAnimated:YES completion:nil];
             
             NSDictionary *dicData = responseObject;
             if([[dicData stringValueForJSON:CJsonStatus] isEqualToString:CStatusOne])
             {
                 [appDelegate MQTTInitialSetup];
                 SessionCountDown *objSessioCount = [SessionCountDown viewFromXib];
                 objSessioCount.strSessionId = session_id;
                 [objSessioCount removeFromSuperview];
                 [appDelegate.window addSubview:objSessioCount];
             }
             else
             {
                 [[appDelegate getTopMostViewController] customAlertViewWithOneButton:@"" Message:[dicData stringValueForJSON:CJsonMessage] ButtonText:@"Ok" Animation:NO completed:nil];
             }
             
             if (completion)
                 completion(responseObject,error);
         }
         else
         {
             [self showCountDownScreenForManualSession:session_id completed:nil];
         }
     }];
}

#pragma mark - Activity Related Common Functions
#pragma mark -

#pragma mark - Like Activity
-(void)likeActivity:(NSString *)act_id Status:(NSString *)status ActivityType:(NSString *)strActType;
{
    [self updateActivityListData:RefreshActivityList MainWis:YES];
    
    if (!act_id)
        return;
    
    [[APIRequest request] likeActivityFeed:act_id Status:status ActivityType:strActType completed:^(id responseObject, NSError *error)
     {
         //
     }];
}

-(void)updateActivityListData:(ActivityData)actData MainWis:(BOOL)isMain
{
    if (isMain)
    {
        if (self.configureRefreshMainActivityList)
            appDelegate.configureRefreshMainActivityList(actData);

        return;
    }
    
    if (self.configureRefreshMainActivityList)
        appDelegate.configureRefreshMainActivityList(actData);

    if (self.configureRefreshMyProfileActivityList)
        appDelegate.configureRefreshMyProfileActivityList(actData);

    if (self.configureRefreshOtherProfileActivityList)
        appDelegate.configureRefreshOtherProfileActivityList(actData);
}

-(void)deletePhotoVideoActivity:(NSString *)strActivityId completed:(void (^)(id responseObject,NSError *error))completion;
{
    UIViewController *viewController = [self getTopMostViewController];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [viewController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Delete CLK
        NSLog(@"Delete CLK");
        if (strActivityId)
        {
            [[APIRequest request] deleteActivity:strActivityId completed:^(id responseObject, NSError *error)
             {
                 if (responseObject && !error)
                 {
                     if (completion)
                     {
                         completion(responseObject,error);
                     }
                 }
             }];
        }
        [viewController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [viewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - MQTT Funcitons

-(void)MQTTInitialSetup
{
    if (!self.objMQTTClient.connected)
        [self MQTTConnectionSetupWithServer];
}

-(void)MQTTConnectionSetupWithServer
{
    if (!appDelegate.loginUser)
    {
        NSLog(@"Only login user can connect with MQTT ========== >>>>>>>> ");
        return;
    }
    
    NSString *clientID = appDelegate.loginUser.user_id;
    self.objMQTTClient = [[MQTTClient alloc] initWithClientId:clientID];
    self.objMQTTClient.username = CMQTTUSERNAME;
    self.objMQTTClient.password = CMQTTPASSWORD;
    
    // connect the MQTT client
    if (!self.objMQTTClient.connected)
    {
        [self.objMQTTClient connectToHost:CMQTTHOST completionHandler:^(MQTTConnectionReturnCode code)
         {
             if (code == ConnectionAccepted)
             {
                 if (appDelegate.loginUser)
                 {
                     if (self.configureResubscribeOnMQTT)
                         self.configureResubscribeOnMQTT(YES);
                 }
             }
             else
             {
                 NSLog(@"MQTT STATUS =========>%lu",(unsigned long)code);
             }
         }];
    }
    
    
#pragma mark - MQTT Received Message
    // define the handler that will be called when MQTT messages are received by the client
    [self.objMQTTClient setMessageHandler:^(MQTTMessage *message)
     {
         NSString *text = message.payloadString;
         
         // Convert Json into dictionay here.....
         NSError *jsonError;
         NSData *objectData = [text dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
         
         if ([[dicJson numberForJson:@"user_available_status"] isEqual:@9] && [dicJson objectForKey:@"user_available_status"])
         {
        //     NSLog(@"Received MQTT Message Status = 9 ========>%@", dicJson);
         }
         else if([dicJson objectForKey:@"publish_status"] && [[dicJson numberForJson:@"publish_status"] isEqual:@2])
         {
//             NSLog(@"Received MQTT Message Status = 2 ========>%@", dicJson);
         }
         else if([dicJson objectForKey:@"publish_status"] && [[dicJson numberForJson:@"publish_status"] isEqual:@1])
         {
//             NSLog(@"Received MQTT Message Status = 1 ========>%@", dicJson);
         }
         else
         {
//             NSLog(@"Received MQTT Message Status = 100 ========>%@", dicJson);
         }
         
         
         if(dicJson)
         {
             // Cheking user availabililty
             if ([[dicJson numberForJson:@"user_available_status"] isEqual:@9] && [dicJson objectForKey:@"user_available_status"])
                 return;
             
             // For FindRunnner screen....
             [[NSNotificationCenter defaultCenter] postNotificationName:CMQTTSearchingPlayer object:nil userInfo:dicJson];
             
             // For Opponent screen....
             [[NSNotificationCenter defaultCenter] postNotificationName:CMQTTOpponentSearchingPlayer object:nil userInfo:dicJson];

             if ([dicJson objectForKey:@"publish_status"] && [[dicJson numberForJson:@"publish_status"] isEqual:@1])
             {
                 // For Running screen....
                 [[NSNotificationCenter defaultCenter] postNotificationName:CMQTTRunningPlayer object:nil userInfo:dicJson];
                 
             }
         }
     }];
    
    [self.objMQTTClient setDisconnectionHandler:^(NSUInteger code)
     {
         NSLog(@"Check Code === %lu",(unsigned long)code);
     }];
    
}

-(void)MQTTPublishWithTopic:(NSData *)data Topic:(NSString *)strTopic
{
    if (!self.objMQTTClient.connected)
    {
        NSLog(@"User No Longer Availabel =========== >>>>> ");
        [self.objMQTTClient reconnect];
    }
    
    [self.objMQTTClient publishData:data
                            toTopic:strTopic
                            withQos:0
                             retain:YES
                  completionHandler:^(int mid) {}];
}

-(void)MQTTSubscribeWithTopic:(NSString *)strTopic
{
    if (self.objMQTTClient.connected)
    {
        NSLog(@"Subscribe successfully ============= >>>");
        [self.objMQTTClient subscribe:strTopic withCompletionHandler:^(NSArray *grantedQos)
        {
            NSLog(@"%@",grantedQos);
        }];
    }
}

-(void)MQTTUnsubscribeWithTopic:(NSString *)strTopic
{
//    if (self.objMQTTClient.connected)
    [self.objMQTTClient unsubscribe:strTopic withCompletionHandler:nil];
}

-(void)MQTTDisconnetFromServer
{
    [self.objMQTTClient disconnectWithCompletionHandler:^(NSUInteger code) {
        NSLog(@"MQTT Disconnected from server ========== >>>> %lu",(unsigned long)code);
    }];
}

-(void)MQTTNotifyToServerForUserAvailability:(NSDictionary *)dicData Topic:(NSString *)strTopic
{
    
//    NSLog(@"MQTTNotifyToServerForUserAvailability ========= >>> ");
    
    if (!self.objMQTTClient.connected)
    {
        NSLog(@"User No Longer Availabel =========== >>>>> ");
        [self.objMQTTClient reconnect];
    }
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dicData options:0 error:&err];
    [appDelegate MQTTPublishWithTopic:jsonData Topic:strTopic];
}


/*
#pragma mark - In App Purchase

-(void)applicationDidCompleteTransaction:(SKPaymentTransaction *)transaction error:(NSError *)error
{
    NSLog(@"Transaction completed ==========>");
    if (self.configurePlanPurchaseSuccessfully)
        self.configurePlanPurchaseSuccessfully(transaction,error);
}

- (void)paymentQueue:(nonnull SKPaymentQueue *)queue updatedTransactions:(nonnull NSArray<SKPaymentTransaction *> *)transactions
{
    NSLog(@"SKPaymentQueue updatedTransactions ==== %@",transactions);
}

- (void)productsRequest:(nonnull SKProductsRequest *)request didReceiveResponse:(nonnull SKProductsResponse *)response
{
    NSLog(@"SKProductsRequest didReceiveResponse ==== %@",response);
}

- (void)checkInAppPurchaseStatus:(BOOL)shouldShowLoader;
{
    
    if (!appDelegate.loginUser)
    {
        NSLog(@"No one user login yet ====== >>>> ");
        return;
    }
    
    
    // Load the receipt from the app bundle.
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if (receipt)
    {
        if (shouldShowLoader)
            [GiFHUD showWithOverlay];
            
        BOOL sandbox = [[receiptURL lastPathComponent] isEqualToString:@"sandboxReceipt"];
        // Create the JSON object that describes the request
        NSError *error;
        NSDictionary *requestContents = @{
                                          @"receipt-data": [receipt base64EncodedStringWithOptions:0],@"password":@"a0888c94c57942cd87fd6a94a4885b88"
                                          };
        
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents options:0 error:&error];
        
        
        
        if (requestData)
        {
            // Create a POST request with the receipt data.
            NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
            if (sandbox) {
                storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];
            }
            NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
            [storeRequest setHTTPMethod:@"POST"];
            [storeRequest setHTTPBody:requestData];
            
            
            //Can use sendAsynchronousRequest to request to Apple API, here I use sendSynchronousRequest
            NSError *error;
            NSURLResponse *response;
            NSData *resData = [NSURLConnection sendSynchronousRequest:storeRequest returningResponse:&response error:&error];
       
            if (error)
            {
                [GiFHUD dismiss];
                [self checkInAppPurchaseStatus:shouldShowLoader];
            }
            else
            {
                [GiFHUD dismiss];
                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:resData options:0 error:&error];
                if (!jsonResponse) {
                }
                else
                {
                    NSLog(@"jsonResponse:%@", jsonResponse);

                    NSDictionary *dictLatestReceiptsInfo = jsonResponse[@"latest_receipt_info"];

                    long long int expirationDateMs = [[dictLatestReceiptsInfo valueForKeyPath:@"@max.expires_date_ms"] longLongValue];
//                    long long requestDateMs = [jsonResponse[@"receipt"][@"request_date_ms"] longLongValue];

                    // Call api here
                    NSArray *arrReciept = jsonResponse[@"latest_receipt_info"];
                    if (arrReciept.count > 0)
                    {
                        NSDictionary *dicReceipt =  [arrReciept lastObject];
                        NSString *strOriginalSubscriptionId = dicReceipt[@"original_transaction_id"];
                        NSLog(@"%@",strOriginalSubscriptionId);

                        [CUserDefaults setObject:strOriginalSubscriptionId forKey:CIAPAppleRecieptID];
                        [CUserDefaults synchronize];

                        if ([strOriginalSubscriptionId isEqualToString:appDelegate.loginUser.receipt_id])
                            [self informToServerIfIAPAutoRenewable:strOriginalSubscriptionId ExpiryTime:expirationDateMs];
                    }
                }
            }
        }
        else
        {
            [GiFHUD dismiss];
        }
    }
    
}

-(void)informToServerIfIAPAutoRenewable:(NSString *)transaction_id ExpiryTime:(double)expiryTimeStamp
{
    [[APIRequest request] updateDateOnServerForIAP:expiryTimeStamp completed:^(id responseObject, NSError *error) {
        if (responseObject && !error)
        {
            NSLog(@"Date update succefully on server ========== >>> ");
        }
    }];
}

*/

@end
