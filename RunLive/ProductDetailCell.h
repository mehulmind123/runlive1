//
//  ProductDetailCell.h
//  RunLive
//
//  Created by mac-00012 on 10/24/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIImageView *imgProduct;
@end
