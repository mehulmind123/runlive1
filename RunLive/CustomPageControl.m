//
//  CustomPageControl.m
//  Urbana
//
//  Created by mac-0007 on 22/06/15.
//  Copyright (c) 2015 mac-00014. All rights reserved.
//

#import "CustomPageControl.h"
#import "IntroViewController.h"
@implementation CustomPageControl

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView *dot = [self imageViewForSubview: [self.subviews objectAtIndex:i]];
        dot.contentMode = UIViewContentModeScaleAspectFit;
        
        if (i == self.currentPage)
        {
            CGPoint center  = dot.center;
            CViewSetSize(dot, 15, 8);
            dot.center = center;
            
            dot.image = [UIImage imageNamed:@"activePage"];
        }
        else
        {
            CGPoint center  = dot.center;
            CViewSetSize(dot, 6, 6);
            dot.center = center;
            
            dot.image = [self.viewController isKindOfClass:[IntroViewController class]]?[UIImage imageNamed:@"inactivePage_intro"]:[UIImage imageNamed:@"inactivePage"];
        }
    }
}

- (UIImageView *)imageViewForSubview:(UIView *)view
{
    UIImageView * dot = nil;
    if ([view isKindOfClass: [UIView class]])
    {
        for (UIView* subview in view.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                dot = (UIImageView *)subview;
                break;
            }
        }
        
        if (dot == nil)
        {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, view.frame.size.width, view.frame.size.height)];
            [view addSubview:dot];
        }
    }
    else
        dot = (UIImageView *)view;
    
    return dot;
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end
