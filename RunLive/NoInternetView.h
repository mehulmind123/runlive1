//
//  NoInternetView.h
//  Patch
//
//  Created by mac-00015 on 11/17/15.
//  Copyright © 2015 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInternetView : UIView
{
    
}

@property(nonatomic,strong) IBOutlet UILabel *lblNoInternet;
@property(nonatomic,strong) IBOutlet UIView *viewInternet;
@end
