//
//  MIVideoCropperOverlayView.h
//  Runlive
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIVideoCropperOverlayView : UIView

@property (nonatomic, assign) CGRect clearRect;
@property (nonatomic, assign) CGSize maxSize;

- (void)drawRect:(CGRect)rect;

@end
