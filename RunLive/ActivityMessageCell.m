//
//  ActivityMessageCell.m
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityMessageCell.h"
#import "UserCell.h"
#import "ReadMSGCell.h"

@implementation ActivityMessageCell
{
    FetchedResultsTableDataSource *fetchHome;
    UIViewController *viewControler;
    NSURLSessionDataTask *taskRuning;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [tblUser registerNib:[UINib nibWithNibName:@"ReadMSGCell" bundle:nil] forCellReuseIdentifier:@"ReadMSGCell"];
    [tblUser registerNib:[UINib nibWithNibName:@"UserCell" bundle:nil] forCellReuseIdentifier:@"UserCell"];
    tblUser.allowsMultipleSelectionDuringEditing = YES;
    [tblUser setContentInset:UIEdgeInsetsMake(0, 0, 80, 0)];
    
    self.arrUsers = [[NSMutableArray alloc] init];
}

#pragma mark - Fetch Controller

- (void)setFetchController
{
//    fetchHome = nil;
    
    if (fetchHome)
        return;
    
    __weak typeof (self) weakSelf = self;
    fetchHome = [viewControler setFetchResultsController:tblUser entity:@"TblChats" sortDesriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"priority_index" ascending:YES]] predicate:[NSPredicate predicateWithFormat:@"loginuser == %@ && isChatDeleted == %@", appDelegate.loginUser,@NO] section:nil cellIdentifier:@"ReadMSGCell" batchSize:80 cell:^(id cell, TblChats *item, NSIndexPath *indexPath)
                 {
                     NSArray *arrParti = [item.participants.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id != %@", appDelegate.loginUser.user_id]];
                     
                     if(arrParti.count > 0)
                     {
                         TblParticipants *objParticipant = arrParti[0];
                         
                         // Unread message celll.....
                         if (!item.isReadMessage.boolValue)
                         {
                             UserCell *cell1 = (UserCell *)cell;
                             
                             cell1.selectionStyle = UITableViewCellSelectionStyleNone;
                             
                             [cell1.imgUser setImageWithURL:[appDelegate resizeImage:@"120" Height:nil imageURL:objParticipant.picture] placeholderImage:nil options:SDWebImageRetryFailed completed:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                             
                             NSArray *arrParticipants = [TblParticipants fetch:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@", item, appDelegate.loginUser.user_id] orderBy:@"priority_index" ascending:YES];
                             
                             cell1.imgOnline.hidden = YES;
                             
                             // Check status here...
                             cell1.imgVerifiedUser.hidden = !objParticipant.celebrity.boolValue;
                             
                             if(arrParticipants.count > 1)
                             {
                                 //Todo: Operations for the group chat(if new user is going to be added into existing group)
                                 cell1.imgVerifiedUser.hidden = YES;
                                 NSString *strTitle = @"";
                                 TblParticipants *dic1 = arrParticipants[0];
                                 TblParticipants *dic2 = arrParticipants[1];
                                 
                                 if (arrParticipants.count > 2)
                                 {
                                     if(arrParticipants.count - 2 == 1)
                                         strTitle = [NSString stringWithFormat:@"%@, %@ + 1 OTHER", dic1.user_name, dic2.user_name];
                                     else
                                         strTitle = [NSString stringWithFormat:@"%@, %@ + %ud OTHERS", dic1.user_name, dic2.user_name, (int)arrParticipants.count-2];
                                 }
                                 else
                                     strTitle = [NSString stringWithFormat:@"%@, %@", dic1.user_name, dic2.user_name];
                                 
                                 cell1.lbName.text = strTitle;
                             }
                             else
                             {
                                 cell1.imgOnline.hidden = !item.isOnline.boolValue;
                                 cell1.lbName.text = objParticipant.user_name;
                             }
                             
                             cell1.lbMessage.text = item.lastMessage;
                             cell1.lblTime.text = item.msg_time;
                             
                             
                             NSArray *arrTypingUser = item.typingUser.mutableCopy;
                             if (arrTypingUser.count > 0)
                             {
                                 if (arrParticipants.count > 1)
                                 {
                                     // To show typing indicator for group chat
                                     cell1.lbMessage.text = item.typingMessage;
                                 }
                                 else
                                     cell1.lbMessage.text = @"typing...";
                                 
                                 cell1.lbMessage.textColor = CRGB(0, 80, 0);
                                 cell1.lbMessage.font = CFontGilroyBold(12);
                             }
                             else
                             {
                                 cell1.lbMessage.text = item.lastMessage;
                                 cell1.lbMessage.textColor = CRGB(156, 162, 176);
                                 cell1.lbMessage.font = CFontGilroyRegular(12);
                             }
                             
                             if (!cell1.rightUtilityButtons)
                                 [cell setRightUtilityButtons:@[[UIButton buttonWithColor:CRGB(255,35,49) title:@"Delete"]]WithButtonWidth:89];
                             
#pragma mark - Delete Unread chat
                             [cell setLeftRightUtilityButtonsClicked:^(SWCellState state, NSUInteger index)
                              {
                                  // Delete cell Here
                                  [weakSelf unSubscribeFromChannel:item];
                              }];
                             
                           //  return cell;
                         }
                         else
                         {
                          
                             ReadMSGCell *cell1 = (ReadMSGCell *)cell;
                             cell1.selectionStyle = UITableViewCellSelectionStyleNone;
                             
                             [cell1.imgUser setImageWithURL:[appDelegate resizeImage:@"120" Height:nil imageURL:objParticipant.picture] placeholderImage:nil options:SDWebImageRetryFailed completed:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                             
                             NSArray *arrParticipants = [TblParticipants fetch:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@", item, appDelegate.loginUser.user_id] orderBy:@"priority_index" ascending:YES];
                             
                             cell1.imgOnline.hidden = YES;
                             
                             // Check status here...
                             cell1.imgVerifiedUser.hidden = !objParticipant.celebrity.boolValue;
                             
                             if(arrParticipants.count > 1)
                             {
                                 cell1.imgVerifiedUser.hidden = YES;
                                 
                                 //Todo: Operations for the group chat(if new user is going to be added into existing group)
                                 NSString *strTitle = @"";
                                 TblParticipants *dic1 = arrParticipants[0];
                                 TblParticipants *dic2 = arrParticipants[1];
                                 
                                 if (arrParticipants.count > 2)
                                 {
                                     if(arrParticipants.count - 2 == 1)
                                         strTitle = [NSString stringWithFormat:@"%@, %@ + 1 OTHER", dic1.user_name, dic2.user_name];
                                     else
                                         strTitle = [NSString stringWithFormat:@"%@, %@ + %d OTHERS", dic1.user_name, dic2.user_name, (int)arrParticipants.count-2];
                                 }
                                 else
                                     strTitle = [NSString stringWithFormat:@"%@, %@", dic1.user_name, dic2.user_name];
                                 
                                 cell1.lblName.text = strTitle;
                             }
                             else
                             {
                                 cell1.imgOnline.hidden = !item.isOnline.boolValue;
                                 cell1.lblName.text = objParticipant.user_name;
                             }
                             
                             cell1.lblTime.text = item.msg_time;
                             cell1.lbMessage.text = item.lastMessage;
                             
                             NSArray *arrTypingUser = item.typingUser.mutableCopy;
                             if (arrTypingUser.count > 0)
                             {
                                 if (arrParticipants.count > 1)
                                 {
                                     // To show typing indicator for group chat
                                     cell1.lbMessage.text = item.typingMessage;
                                 }
                                 else
                                     cell1.lbMessage.text = @"typing...";
                                 
                                 cell1.lbMessage.textColor = CRGB(0, 80, 0);
                                 cell1.lbMessage.font = CFontGilroyBold(12);
                             }
                             else
                             {
                                 cell1.lbMessage.text = item.lastMessage;
                                 cell1.lbMessage.textColor = CRGB(156, 162, 176);
                                 cell1.lbMessage.font = CFontGilroyRegular(12);
                             }

                             
                             if (!cell1.rightUtilityButtons)
                                 [cell setRightUtilityButtons:@[[UIButton buttonWithColor:CRGB(255,35,49) title:@"Delete"]]WithButtonWidth:89];
                             
#pragma mark - Delete Read chat
                             [cell setLeftRightUtilityButtonsClicked:^(SWCellState state, NSUInteger index)
                              {
                                  // Delete cell Here
                                  [weakSelf unSubscribeFromChannel:item];
                              }];
                             
                        //     return cell;
                         }
                         
                     }
                 }];
    
    [fetchHome setDidSelectRowAtIndexPath:^(id cell, TblChats *item, NSIndexPath *indexPath)
     {
         if (weakSelf.configureActivityMessageDidSelect)
             weakSelf.configureActivityMessageDidSelect(item);
     }];
    
    [fetchHome setIdentifierForRowAtIndexPath:^NSString*(TblChats *item, NSIndexPath *indexPath)
     {
         if (!item.isReadMessage.boolValue)
                 return @"UserCell";
         
             return @"ReadMSGCell";
     }];
    
    fetchHome.heightForItemAtIndexPath = ^CGFloat(TblChats *item, NSIndexPath *indexPath)
    {
        return 80;
    };
    
    [fetchHome loadData];
}


#pragma mark - General Function

-(void)unSubscribeFromChannel:(TblChats *)objChat
{
    UIViewController *viewController = [appDelegate getTopMostViewController];
    [viewController customAlertViewWithTwoButton:@"Alert!" Message:CMessageDeleteChat ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
        if (index == 0)
        {
            [[APIRequest request] deleteUserFromChannel:objChat.channelid completed:^(id responseObject, NSError *error)
             {
                 objChat.isChatDeleted = @YES;
                 [TblMessages deleteObjects:[NSPredicate predicateWithFormat:@"channelId == %@", objChat.channelid]];
                 [TblChats deleteObjects:[NSPredicate predicateWithFormat:@"channelid == %@", objChat.channelid]];
                 [[Store sharedInstance].mainManagedObjectContext save];
                 [self fetchChatListDataFromLocal];
             }];
        }
    }];
}


-(void)fetchChatListDataFromLocal
{
    [self.arrUsers removeAllObjects];
    
    // Subscribe chanels here...
    [appDelegate.client subscribeToChannels:[appDelegate.loginUser.chats.allObjects valueForKeyPath:@"channelid"] withPresence:YES];
    
    [self.arrUsers addObjectsFromArray:[TblChats fetch:[NSPredicate predicateWithFormat:@"loginuser == %@ && isChatDeleted == %@", appDelegate.loginUser,@NO] orderBy:@"priority_index" ascending:YES]];
    
    for (int i=0; i<self.arrUsers.count; i++)
    {
        TblChats *objChat = self.arrUsers[i];
        
        [appDelegate.client historyForChannel:objChat.channelid start:nil end:nil limit:1 withCompletion:^(PNHistoryResult *result, PNErrorStatus *status)
         {
             if (!status)
             {
                 NSArray *arr = result.data.messages;
                 
                 if (arr.count > 0)
                 {
                     NSDictionary *dicMsg = arr[0];
                     
                     double PubNubTimeStamp = [dicMsg doubleForKey:@"timestamp"];
                     objChat.isReadMessage = PubNubTimeStamp > objChat.lastRead.doubleValue ? @NO : @YES;
                     [appDelegate showMessageBadgeOnTabBar];
                     
                     objChat.msg_time = [self messageTime:PubNubTimeStamp];
                     
                     NSArray *arrPartici = [objChat.participants.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@",objChat, appDelegate.loginUser.user_id]];
                     
                     if(arrPartici.count > 0)
                     {
                         NSArray *arrMsgs = result.data.messages;
                         
                         if (arrMsgs.count > 0)
                         {
//                             NSDictionary *dicMsg = arrMsgs[0];
//                             TblParticipants *objParti = arrPartici[0];
//                             objParti.picture = [dicMsg stringValueForJSON:@"userimgurl"];
//                             [[[Store sharedInstance] mainManagedObjectContext] save];
                         }
                     }
                         
                     if ([dicMsg objectForKey:@"msgtype"])
                     {
                         if ([[dicMsg stringValueForJSON:@"msgtype"] isEqualToString:@"T"])
                         {
                             if ([dicMsg objectForKey:@"msgtext"])
                             {
                                 objChat.lastMessage = [dicMsg stringValueForJSON:@"msgtext"];
                                 [[[Store sharedInstance] mainManagedObjectContext] save];
                             }
                         }
                         else
                         {
                             if ([dicMsg objectForKey:@"imgname"])
                             {
                                 objChat.lastMessage = [dicMsg stringValueForJSON:@"imgname"];
                                 [[[Store sharedInstance] mainManagedObjectContext] save];
                             }
                         }
                     }
                 }

                 [[Store sharedInstance].mainManagedObjectContext save];
             }
             else
             {
                 /**
                  Handle message history download error. Check 'category' property
                  to find out possible reason because of which request did fail.
                  Review 'errorData' property (which has PNErrorData data type) of status
                  object to get additional information about issue.
                  
                  Request can be resent using: [status retry];
                  */
             }
             
             if (i == self.arrUsers.count-1)
             {
                 [GiFHUD dismiss];
//                 [tblUser reloadData];
//                 [self setFetchController];
             }
         }];
    }
    
//    [tblUser reloadData];
    [self setFetchController];
}

-(NSString *)messageTime:(double)timeStamp
{
    NSDate *msgDate = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:@"dd-MMM-yyyy, hh:mm a"];
    NSTimeZone *gmt = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *strMsgDate = [dateFormatter stringFromDate:msgDate];
    return strMsgDate;
}

#pragma mark - Api Function

-(void)showActivityIndicator:(BOOL)shouldShow
{
    
    if (shouldShow)
    {
        activityIndicator.hidden = NO;
        [activityIndicator startAnimating];
    }
    else
    {
        activityIndicator.hidden = YES;
        [activityIndicator stopAnimating];
    }
}

- (void)getChatUserList:(UIViewController *)view FirstTime:(BOOL)isFirstTime;
{
    viewControler = view;

    if (taskRuning && taskRuning.state == NSURLSessionTaskStateRunning)
        [taskRuning cancel];
    
    [self showActivityIndicator:NO];
    
    if (_arrUsers.count < 1)
        [self showActivityIndicator:YES];
    
    taskRuning = [[APIRequest request] ChannelList:isFirstTime completed:^(id responseObject, NSError *error) {
        [self showActivityIndicator:NO];
        
        if (isFirstTime)
            [appDelegate SetPubNubClientState:@"online"];
        
        [self fetchChatListDataFromLocal];
    }];
    
}

#pragma mark - Action Event
-(IBAction)btnNewMSGCLK:(id)sender
{
    if (self.configureBtnNewMSGCLK)
        self.configureBtnNewMSGCLK(@"YES");
}

@end
