//
//  TrackOrderViewController.h
//  RunLive
//
//  Created by mac-00012 on 5/31/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackOrderViewController : SuperViewController
{
    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

@property(strong,nonatomic) NSString *sttTrackId;

@end
