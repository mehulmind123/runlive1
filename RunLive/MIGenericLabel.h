//
//  MIGenericLabel.h
//  EdSmart
//
//  Created by mac-0007 on 15/07/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CDeviceHeight [UIScreen mainScreen].bounds.size.height
#define Is_iPhone_4 CDeviceHeight == 480
#define Is_iPhone_5 CDeviceHeight == 568
#define Is_iPhone_6 CDeviceHeight == 667
#define Is_iPhone_6_PLUS CDeviceHeight == 736
@interface MIGenericLabel : UILabel

@end
