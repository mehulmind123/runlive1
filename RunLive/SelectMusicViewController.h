//
//  SelectMusicViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMusicViewController : SuperViewController<SPTAuthViewDelegate>
{
    IBOutlet UILabel *lblSpotifyconnected,*lblPlaylistName,*lblFollowers;
    IBOutlet UIImageView *imgPlaylist,*imgPlaylistType;
    IBOutlet UIView *viewPlaylistContainer;
    
    
}
@end
