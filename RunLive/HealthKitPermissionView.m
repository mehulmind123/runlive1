//
//  HealthKitPermissionView.m
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "HealthKitPermissionView.h"

@implementation HealthKitPermissionView

+ (id)initHealthKitPermissionView
{
    HealthKitPermissionView *objHealthKit = [[[NSBundle mainBundle]loadNibNamed:@"HealthKitPermissionView" owner:nil options:nil] lastObject];
    CViewSetWidth(objHealthKit, CScreenWidth);
    CViewSetHeight(objHealthKit, CScreenHeight);
    return objHealthKit;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self Initialization];
}

#pragma mark - Configuration

-(void)Initialization
{
    self.btnHealthKitSure.layer.cornerRadius = self.btnHealthKitNotNow.layer.cornerRadius = 3;
}

@end
