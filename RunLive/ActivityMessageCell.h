//
//  ActivityMessageCell.h
//  RunLive
//
//  Created by mac-00012 on 10/25/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^activityMessageDidSelect)(TblChats *objChat);

typedef void(^btnNewMSGCLK)(NSString *btnCLK);

@interface ActivityMessageCell : UICollectionViewCell<UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *tblUser;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

@property(weak,nonatomic) IBOutlet UIButton *btnMessageBack;
@property(copy,nonatomic) activityMessageDidSelect configureActivityMessageDidSelect;
@property(copy,nonatomic) btnNewMSGCLK configureBtnNewMSGCLK;

@property(strong,nonatomic) NSMutableArray *arrUsers;

- (void)getChatUserList:(UIViewController *)view FirstTime:(BOOL)isFirstTime;





@end
