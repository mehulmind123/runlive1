//
//  VoiceChatSettingViewController.m
//  RunLive
//
//  Created by mac-0005 on 16/12/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "VoiceChatSettingViewController.h"

@interface VoiceChatSettingViewController ()

@end

@implementation VoiceChatSettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self btnONOFFCLK:appDelegate.loginUser.isVoiceChatEnable.boolValue ? btnON : btnOFF];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action Event
-(IBAction)btnONOFFCLK:(UIButton *)sender
{
    btnON.selected = btnOFF.selected = NO;
    btnON.backgroundColor = btnOFF.backgroundColor = CRGB(203, 210, 211);
    
    switch (sender.tag)
    {
        case 0:
            {
                // ON
                btnON.selected = YES;
                btnON.backgroundColor = CRGB(70, 216, 252);
            }
            break;
        case 1:
        {
            // OFF
            btnOFF.selected = YES;
            btnOFF.backgroundColor = CRGB(70, 216, 252);
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)btnBackCLK:(id)sender
{
    
    if ((appDelegate.loginUser.isVoiceChatEnable.boolValue && btnON.selected) || (!appDelegate.loginUser.isVoiceChatEnable.boolValue && btnOFF.selected))
    {
        // No change
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self customAlertViewWithTwoButton:@"" Message:CMessageSaveChanges ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:nil Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:btnON.selected ? @"on" : @"off" AboutUs:nil Link:nil completed:^(id responseObject, NSError *error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
    
}

@end
