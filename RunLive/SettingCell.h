//
//  SettingCell.h
//  RunLive
//
//  Created by mac-0006 on 02/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
