//
//  MGLMapView+MIMGLMapView.h
//  RunLive
//
//  Created by mac-0005 on 13/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <Mapbox/Mapbox.h>
#import "CustomPhotoVideoMarker.h"
#import "CustomStartEndPointView.h"
#import "MIMapBoxPointAnnotation.h"
#import "MIMGLCustomCurrentLocationAnimtedView.h"

@protocol MIMGLMapViewDelegate
- (void)mapviewDidTapMarker:(NSDictionary *)userInfo;
@end

typedef enum
{
    PhotoVideoMarker,
    StartPossitionMarker,
    EndPossitionMarker,
    CurrentLocationMarker,
} MarkerType;

@interface MGLMapView (MIMGLMapView)<MGLMapViewDelegate>

-(void)clearMGLMapView;

-(void)drawPolylineByUsingCoordinates:(NSArray *)arrCoordinates;

-(void)addCustomMarkerOnMapView:(CLLocationCoordinate2D)coordinate Type:(MarkerType)markerType UserInfo:(NSDictionary *)dicInfo;

- (void)setMapViewDelegate:(id<MIMGLMapViewDelegate>)delegate;

-(void)MGLMapViewCenterCoordinate:(NSArray *)arrCoord;

-(void)setPathCenterInMapView:(NSArray *)arrLocation edgeInset:(UIEdgeInsets)inset;

@end
