//
//  PremiumView.m
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "PremiumView.h"

@implementation PremiumView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.btnUpgradeNow.layer.cornerRadius = 3;
    self.viewContainer.layer.cornerRadius = 3;
    self.viewContainer.layer.masksToBounds = YES;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.center = CScreenCenter;
    
}

@end
