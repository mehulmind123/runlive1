//
//  UIButton+SWTableViewCellButtons.h
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (SWTableViewCellButtons)

+ (UIButton *)buttonWithColor:(UIColor *)color title:(NSString *)title;
+ (UIButton *)buttonWithColor:(UIColor *)color attributedTitle:(NSAttributedString *)title;
+ (UIButton *)buttonWithColor:(UIColor *)color icon:(UIImage *)icon;
+ (UIButton *)buttonWithColor:(UIColor *)color normalIcon:(UIImage *)normalIcon selectedIcon:(UIImage *)selectedIcon;


@end
