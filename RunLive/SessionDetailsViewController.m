//
//  SessionDetailsViewController.m
//  RunLive
//
//  Created by mac-0006 on 18/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionDetailsViewController.h"
#import "SelectMusicViewController.h"
#import "TagpeopleCell.h"
#import "LocationManager.h"

@interface SessionDetailsViewController ()
{
    int hours, minutes, seconds;
    int timeProcess;
    int iOffset;
    
    NSArray *aryPeoples;
    NSArray *filterdPeople; // make ordered predicate
    NSMutableArray *arrCommentList,*arrInvitedUser;
    BOOL isSoloSession,isRefershinData;
    double sessionEndTimeStamp;
    UIRefreshControl *refreshControl;
}

@property (strong, nonatomic) LocationManager *locationManager;

@end

@implementation SessionDetailsViewController

#pragma mark - Life cycle
#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    viewLineComment.hidden = YES;
    viewComment.hidden = YES;
    
    txtComment.placeholder = @"Type your comment here...";
    [txtComment setFont:[UIFont fontWithName:@"Gilroy-Regular" size:17]];
    txtComment.delegate = self;
    
    arrInvitedUser = [NSMutableArray new];
    
    [collInviteFrd registerNib:[UINib nibWithNibName:@"SessionDetailSelectedUserCell" bundle:nil] forCellWithReuseIdentifier:@"SessionDetailSelectedUserCell"];
    [tblComment registerNib:[UINib nibWithNibName:@"RunSummaryCommentCell" bundle:nil] forCellReuseIdentifier:@"RunSummaryCommentCell"];
    [tblPeopleList registerNib:[UINib nibWithNibName:@"TagpeopleCell" bundle:nil] forCellReuseIdentifier:@"TagpeopleCell"];

    tblPeopleList.estimatedRowHeight = 60.0;
    tblPeopleList.rowHeight = 65.00;
    
    arrCommentList = [NSMutableArray new];
    superTable = tblComment;
    
// <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblComment addSubview:refreshControl];
    iOffset = 0;
    [self GetCommentDataFromServer:YES];
    
    
// <----------------------------------- Synce Related code here ----------------------------------->
    timeProcess = 0;
    UILongPressGestureRecognizer *longGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(incrementSpin:)];
    longGR.delegate = self;
    [viewProgress setUserInteractionEnabled:YES];
    [viewProgress addGestureRecognizer:longGR];
    
    lblProgress.startDegree = 0;
    lblProgress.endDegree = 0;
    lblProgress.isStartDegreeUserInteractive = YES;
    lblProgress.isEndDegreeUserInteractive = YES;

    if (self.isCommentView)
        [self btnTabbarClicked:btnComment];
    else
        [self btnTabbarClicked:btnRun];

    
// <----------------------------------- Map View Related code here ----------------------------------->
    CLLocation *cachedlocation = [[CLLocation alloc] initWithLatitude:[CUserDefaults doubleForKey:CCurrentLatitude]
                                                      longitude:[CUserDefaults doubleForKey:CCurrentLongitude]];
    [self setupForMapView:cachedlocation];
    [self setupMapViewWithLatestLocation];
   

    // Refresh screen from push notification...
    appDelegate.configureRefreshSessionData = ^(RefreshSessionWithUpdate refreshSessionWithUpdate)
    {
        if ([[appDelegate getTopMostViewController] isKindOfClass:[SessionDetailsViewController class]])
        {
            if (refreshSessionWithUpdate == DeleteSession)
                [self btnBackClicked:nil];
            else if (refreshSessionWithUpdate == SyncSession || refreshSessionWithUpdate == UnjoinSession)
                [self GetSessionDataFromServer];
            else if (refreshSessionWithUpdate == CommentOnSession)
            {
                iOffset = 0;
                [self GetCommentDataFromServer:YES];
            }
        }
    };
    
    
    [UIApplication applicationWillEnterForeground:^{
        if ([[appDelegate getTopMostViewController] isKindOfClass:[SessionDetailsViewController class]])
            [self setTimerForSession];
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    viewFillCirle.layer.cornerRadius = viewFillCirle.frame.size.width/2;
    viewFillCirle.layer.masksToBounds = YES;
    
    viewScrollIndicator.layer.cornerRadius = viewScrollIndicator.frame.size.width/2;
    viewScrollIndicator.clipsToBounds = true;
    ViewScrollSide.layer.cornerRadius = ViewScrollSide.frame.size.width/2;
    ViewScrollSide.clipsToBounds = true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    [self GetSessionDataFromServer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [timer invalidate];
}

#pragma mark - Session Related data
-(void)GetSessionDataFromServer
{
    if (!self.strSessionId)
        return;
    
    [[APIRequest request] GETSessionDetail:self.strSessionId completed:^(id responseObject, NSError *error)
    {
        NSArray *arrResponse = [responseObject valueForKey:CJsonData];
        
        if (arrResponse.count > 0)
        {
            NSDictionary *dicSessionData = arrResponse[0];
            
            lblSessionTitle.text = [dicSessionData stringValueForJSON:@"name"];
            
            NSArray *arrJoinedUser = [dicSessionData valueForKey:@"joinedUsers"];
            NSArray *arrSyncedUser = [dicSessionData valueForKey:@"syncedUsers"];
            NSArray *arrOtherUser = [dicSessionData valueForKey:@"otherUsers"];
 
            isSoloSession = [[dicSessionData numberForJson:@"isSolo"] isEqual:@1] ? YES : NO;
            
            NSDate *date = [self convertDateFromString:[dicSessionData stringValueForJSON:@"run_time"] isGMT:YES formate:CDateFormater];
            sessionEndTimeStamp = [date timeIntervalSince1970];
            
            NSMutableArray *arrTempCommentUser = [NSMutableArray new];
            
            [arrInvitedUser removeAllObjects];
            
            for (int s = 0; arrSyncedUser.count > s; s++)
            {
                NSMutableDictionary *dicData = [NSMutableDictionary new];
                NSDictionary *dicSynced = arrSyncedUser[s];
                
                [dicData addObject:[dicSynced stringValueForJSON:@"_id"] forKey:@"_id"];
                [dicData addObject:[dicSynced stringValueForJSON:@"country"] forKey:@"country"];
                [dicData addObject:[dicSynced stringValueForJSON:@"fb_id"] forKey:@"fb_id"];
                [dicData addObject:[dicSynced stringValueForJSON:@"first_name"] forKey:@"first_name"];
                [dicData addObject:[dicSynced stringValueForJSON:@"last_name"] forKey:@"last_name"];
                [dicData addObject:[dicSynced stringValueForJSON:@"picture"] forKey:@"picture"];
                [dicData addObject:[dicSynced stringValueForJSON:@"user_name"] forKey:@"user_name"];
                [dicData addObject:@"YES" forKey:@"is_synced"];
                [arrInvitedUser addObject:dicData];

                
                if (![appDelegate.loginUser.user_id isEqualToString:[dicSynced stringValueForJSON:@"_id"]])
                    [arrTempCommentUser addObject:dicData];
                else
                {
                    lblProgress.endDegree = 360;
                    viewProgress.userInteractionEnabled = NO;
                    viewFillCirle.backgroundColor = lblProgress.progressColor = CRGB(30, 215, 96);
                    lblSynced.text = @"SYNCED";
                }
            }
            
            for (int j = 0; arrJoinedUser.count > j; j++)
            {
                NSMutableDictionary *dicData = [NSMutableDictionary new];
                NSDictionary *dicJoined = arrJoinedUser[j];
                
                [dicData addObject:[dicJoined stringValueForJSON:@"_id"] forKey:@"_id"];
                [dicData addObject:[dicJoined stringValueForJSON:@"country"] forKey:@"country"];
                [dicData addObject:[dicJoined stringValueForJSON:@"fb_id"] forKey:@"fb_id"];
                [dicData addObject:[dicJoined stringValueForJSON:@"first_name"] forKey:@"first_name"];
                [dicData addObject:[dicJoined stringValueForJSON:@"last_name"] forKey:@"last_name"];
                [dicData addObject:[dicJoined stringValueForJSON:@"picture"] forKey:@"picture"];
                [dicData addObject:[dicJoined stringValueForJSON:@"user_name"] forKey:@"user_name"];
                [dicData addObject:[dicJoined stringValueForJSON:@"celebrity"] forKey:@"celebrity"];
                [dicData addObject:@"NO" forKey:@"is_synced"];
                
                [arrInvitedUser addObject:dicData];
                
                if (![appDelegate.loginUser.user_id isEqualToString:[dicJoined stringValueForJSON:@"_id"]])
                    [arrTempCommentUser addObject:dicData];
            }
            
            for (int o = 0; arrOtherUser.count > o; o++)
            {
                NSMutableDictionary *dicData = [NSMutableDictionary new];
                NSDictionary *dicOther = arrOtherUser[o];
                
                [dicData addObject:[dicOther stringValueForJSON:@"_id"] forKey:@"_id"];
                [dicData addObject:[dicOther stringValueForJSON:@"country"] forKey:@"country"];
                [dicData addObject:[dicOther stringValueForJSON:@"fb_id"] forKey:@"fb_id"];
                [dicData addObject:[dicOther stringValueForJSON:@"first_name"] forKey:@"first_name"];
                [dicData addObject:[dicOther stringValueForJSON:@"last_name"] forKey:@"last_name"];
                [dicData addObject:[dicOther stringValueForJSON:@"picture"] forKey:@"picture"];
                [dicData addObject:[dicOther stringValueForJSON:@"user_name"] forKey:@"user_name"];
                [dicData addObject:[dicOther stringValueForJSON:@"celebrity"] forKey:@"celebrity"];
                [dicData addObject:@"NO" forKey:@"is_synced"];
                [arrInvitedUser addObject:dicData];
            }
            
            aryPeoples = arrTempCommentUser;
            filterdPeople = @[];

            [collInviteFrd reloadData];
            
            lblSyncedCount.text = [NSString stringWithFormat:@"%d SYNCED",(int)arrSyncedUser.count];
            lblPendingCount.text = [NSString stringWithFormat:@"/ %d PENDING",(int)arrJoinedUser.count+(int)arrOtherUser.count];
            
            [self setTimerForSession];
            
            btnSpotify.selected = [TblMusicList fetchAllObjects].count;
            
        }
        
    }];
}

#pragma mark - Pull Refresh
-(void)handleRefresh
{
    if (iOffset == 0)
    {
        [refreshControl endRefreshing];
        return;
    }
    
    isRefershinData = YES;
    [self GetCommentDataFromServer:NO];
}


#pragma mark - Comment List From Server

-(void)GetCommentDataFromServer:(BOOL)shouldShowLoader
{
    if (!self.strSessionId)
        return;
    
    [[APIRequest request] getCommentListOfSession:self.strSessionId Offset:[NSString stringWithFormat:@"%d",iOffset] completed:^(id responseObject, NSError *error)
    {
        [refreshControl endRefreshing];
        if (responseObject && !error)
        {
            if (iOffset == 0)
                [arrCommentList removeAllObjects];
            
            [arrCommentList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
            [tblComment reloadData];
            
            if (isRefershinData)
                [tblComment setContentOffset:CGPointMake(0, 0) animated:NO];
            else
            {
                if (arrCommentList.count > 0)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
                        [tblComment scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
                    });
                }
            }
            
            isRefershinData = NO;
        }
    }];
}

-(void)addCommentOnSession
{
    if (!self.strSessionId)
        return;
    
    NSString * aString = txtComment.text;
    NSMutableArray *arrMentionUsers = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd])
    {
        NSString *substring = nil;
        [scanner scanString:@"@" intoString:nil]; // Scan the # character
        
        if([scanner scanUpToString:@" " intoString:&substring])
            [arrMentionUsers addObject:substring];
        
        [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before next #
    }
    

    NSString *strCommentText = txtComment.text;
    txtComment.text = @"";

    
    [[APIRequest request] addCommentOnSession:strCommentText SessionId:self.strSessionId UserName:arrMentionUsers completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            [arrCommentList addObject:[responseObject valueForKey:CJsonData]];
            [tblComment reloadData];
 
            if (arrCommentList.count > 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
                    [tblComment scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
                });
            }
        }
    }];
}

#pragma mark - Map View Related Functions
- (void)setupMapViewWithLatestLocation {
    if ([LocationManager sharedInstance].currentLocation) {
        [self setupForMapView:[LocationManager sharedInstance].currentLocation];
    }
}

-(void)setupForMapView:(CLLocation *)location
{
    [mapView clearMGLMapView];
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [mapView setCenterCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) zoomLevel:12 animated:NO];
    [mapView addCustomMarkerOnMapView:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) Type:CurrentLocationMarker UserInfo:@{}];
}

#pragma mark - Synced Related Method
#pragma mark

- (void)incrementSpin:(UILongPressGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        [timerProgress invalidate];
        if (timeProcess < 360)
        {
            timeProcess = 0;
            lblProgress.startDegree = 0;
            lblProgress.endDegree = 0;
            viewFillCirle.backgroundColor = CRGB(63, 65, 87);
            viewProgress.userInteractionEnabled = YES;
        }
        else
        {
            if (!self.strSessionId)
                return;
            
            [[APIRequest request] SyncedSession:self.strSessionId completed:^(id responseObject, NSError *error)
            {
                if (responseObject && !error)
                {
                    // If session succesfully synced.....
                    viewProgress.userInteractionEnabled = NO;
                    viewFillCirle.backgroundColor = lblProgress.progressColor = CRGB(30, 215, 96);
                    lblSynced.text = @"SYNCED";
                    
                    [self GetSessionDataFromServer];
                }
                else
                {
                    timeProcess = 0;
                    lblProgress.startDegree = 0;
                    lblProgress.endDegree = 0;
                    viewFillCirle.backgroundColor = CRGB(63, 65, 87);
                    viewProgress.userInteractionEnabled = YES;
                }
            }];
        }
    }
    else if (sender.state == UIGestureRecognizerStateBegan)
    {
        if(minutes < 15 && hours == 0) // IF 15 MINUTE REMAINING THE ONLY...
            timerProgress = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(updateProgressCounter) userInfo:nil repeats:YES];
    }
}

-(void)updateProgressCounter
{
    timeProcess+=30;
    [lblProgress setEndDegree:timeProcess];
    viewFillCirle.backgroundColor = CRGB(61, 205, 252);
}

#pragma mark - Timer Related Function

-(void)setTimerForSession
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;

    if (difTimeStamp > 0)
    {
        seconds = difTimeStamp;
        
        [timer invalidate];
        timer = nil;
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateCounter) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        minutes = seconds / 60 % 60;
        
        int tempSecond = seconds;
        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        hours = tempSecond / (60 * 60);
        tempSecond = tempSecond - hours * (60 * 60);
        minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;

        [collInviteFrd reloadData];

    }
    else
    {
        lblTimer.text=@"00:00:00";
        
//        [appDelegate showCountDownScreenForManualSession:self.strSessionId completed:^(id responseObject, NSError *error)
//         {
//             [self.navigationController popViewControllerAnimated:YES];
//         }];

    }
    
}

-(void)updateCounter
{
//    NSLog(@"SESSION DETAILS TIMER VALUE ======== >>");
    seconds--;
    if (seconds >= 0)
    {
        int tempSecond = seconds;
        
        int days = tempSecond / (60 * 60 * 24);
        tempSecond = tempSecond - days * (60 * 60 * 24);
        hours = tempSecond / (60 * 60);
        tempSecond = tempSecond - hours * (60 * 60);
        minutes = tempSecond / 60;
        tempSecond = tempSecond - minutes*60;
        
        if (days > 0)
        {
            // Time with day... (DDd:HHh)
            lblTimer.text=[NSString stringWithFormat:@"%02dd:%02dh",days,hours];
        }
        else
        {
            //hh:mm:ss
            lblTimer.text=[NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,tempSecond];
            
            if(minutes < 15 && hours == 0) // Disable Invite friend button here (IF 15 MINUTE REMAINING)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                SessionDetailSelectedUserCell * cell = (SessionDetailSelectedUserCell *)[collInviteFrd cellForItemAtIndexPath:indexPath];
                cell.btnAdd.userInteractionEnabled = NO;
                cell.btnAdd.selected = YES;
            }
        }
    }
    else
    {
        [timer invalidate];
        lblTimer.text = @"00:00:00";
        timer = nil;
     
        [appDelegate showCountDownScreenForManualSession:self.strSessionId completed:^(id responseObject, NSError *error)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    

}


#pragma mark - UICollectionView Delegate and Datasource
#pragma mark

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (isSoloSession)
        return arrInvitedUser.count < 10 ? arrInvitedUser.count+1 : arrInvitedUser.count;

    return arrInvitedUser.count < 20 ? arrInvitedUser.count+1 : arrInvitedUser.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"SessionDetailSelectedUserCell";
    SessionDetailSelectedUserCell *cell = [collInviteFrd dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
    
    cell.btnAdd.hidden = YES;
    cell.viewUser.hidden = NO;
    
    if ((arrInvitedUser.count < 20 && !isSoloSession) || (arrInvitedUser.count < 10 && isSoloSession))  // If user count is less then 19 user then Add button will show.....
    {
        if (indexPath.item == 0)
        {
            cell.btnAdd.hidden = NO;
            cell.viewUser.hidden = YES;
            
            if(minutes < 15 && hours == 0)
            {
                cell.btnAdd.userInteractionEnabled = NO;
                cell.btnAdd.selected = YES;
            }
            else
            {
                cell.btnAdd.userInteractionEnabled = YES;
                cell.btnAdd.selected = NO;
            }
        }
        else
        {
            NSDictionary *dicData = arrInvitedUser[indexPath.item-1];
            cell.viewGray.hidden = [[dicData stringValueForJSON:@"is_synced"] isEqualToString:@"YES"];

            [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
    }
    else
    {
        NSDictionary *dicData = arrInvitedUser[indexPath.item];
        cell.viewGray.hidden = [[dicData stringValueForJSON:@"is_synced"] isEqualToString:@"YES"];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    
    
    [cell.btnAdd touchUpInsideClicked:^{
        
        NSMutableArray *arrTempUser = [NSMutableArray new];
        for (int i = 0; arrInvitedUser.count > i; i++) // To Remove sync status from array.......
        {
            NSDictionary *dicData = arrInvitedUser[i];
            NSMutableDictionary *dicInvite = [NSMutableDictionary new];
            
            [dicInvite addObject:[dicData stringValueForJSON:@"_id"] forKey:@"_id"];
            [dicInvite addObject:[dicData stringValueForJSON:@"country"] forKey:@"country"];
            [dicInvite addObject:[dicData stringValueForJSON:@"fb_id"] forKey:@"fb_id"];
            [dicInvite addObject:[dicData stringValueForJSON:@"first_name"] forKey:@"first_name"];
            [dicInvite addObject:[dicData stringValueForJSON:@"last_name"] forKey:@"last_name"];
            [dicInvite addObject:[dicData stringValueForJSON:@"picture"] forKey:@"picture"];
            [dicInvite addObject:[dicData stringValueForJSON:@"user_name"] forKey:@"user_name"];
            [arrTempUser addObject:dicInvite];
        }
        
        InviteFriendsViewController *objInviteFrd= [[InviteFriendsViewController alloc] init];
        objInviteFrd.arrAllreadyInvitedUser = [[NSMutableArray alloc] initWithArray:arrTempUser];
        objInviteFrd.strSessionId = self.strSessionId;
        objInviteFrd.isSoloSession = isSoloSession;
        [self.navigationController pushViewController:objInviteFrd animated:YES];
    }];
    
    return cell;
}

#pragma mark - UITableView Delegate and Datasource
#pragma mark

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblPeopleList])
        return 65;
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblPeopleList])
        return 65;

    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:tblComment])
        return arrCommentList.count;

    return filterdPeople.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ([tableView isEqual:tblPeopleList])
    {
        static NSString *simpleTableIdentifier = @"TagpeopleCell";
        TagpeopleCell *cell = (TagpeopleCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = filterdPeople[indexPath.row];
        cell.lblContent.text = [dicData stringValueForJSON:@"user_name"];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"84" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        return cell;
    }
    else
    {
        NSString *strIdentifier = @"RunSummaryCommentCell";
        RunSummaryCommentCell *cell = [tblComment dequeueReusableCellWithIdentifier:strIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
        cell.lblComment.text = [dicData stringValueForJSON:@"text"];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        [cell.imgUserPic setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:[dicData stringValueForJSON:@"createdAt"]];
        
        [cell.lblComment addHashTagAndUserHandler:@"" Complete:^(NSString *urlString)
        {
            [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        
        __weak typeof (cell) weakCell = cell;
        [cell.lblUserName addHashTagAndUserHandler:cell.lblUserName.text Complete:^(NSString *urlString)
        {
            [appDelegate moveOnProfileScreen:weakCell.lblUserName.text UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:tblPeopleList])
    {
        NSString *str = [txtComment.text substringToIndex:[txtComment.text rangeOfString:@"@" options:NSBackwardsSearch].location + 1];
        txtComment.text = [NSString stringWithFormat:@"%@%@ ",str,((TagpeopleCell *)[tblPeopleList cellForRowAtIndexPath:indexPath]).lblContent.text];
        filterdPeople = @[];
        [self updateTablePeople];
    }
    else
    {
        // User Detail here...
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:tblComment])
    {
        // Return YES if you want the specified item to be editable.
        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
        
        if ([[dicData stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id] || [[dicData stringValueForJSON:@"owner_id"] isEqualToString:appDelegate.loginUser.user_id])
        {
            return YES;
        }
        
        return NO;
    }
    
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self customAlertViewWithTwoButton:@"" Message:@"Are you sure you want to delete this comment?" ButtonFirstText:@"Yes" ButtonSecondText:@"NO" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
                [[APIRequest request] deleteSessionComment:[dicData stringValueForJSON:@"_id"] completed:^(id responseObject, NSError *error)
                 {
                     if (responseObject && !error)
                     {
                         [arrCommentList removeObjectAtIndex:indexPath.row];
                         [tblComment reloadData];
                     }
                 }];
            }
        }];
    }
}

#pragma mark - ScrollView Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if ([scrollView isEqual:tblPeopleList])
    {
        [self updateScrollIndicator];
    }
}

#pragma mark - HPGrowingView Delegate
#pragma mark

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    txtCommentHeight.constant = height;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText =  [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if (finalText.length > 0)
    {
        btnSend.enabled = [finalText isBlankValidationPassed];
        NSRange rgy = [finalText rangeOfString:@"@" options:NSBackwardsSearch];
        
        if (rgy.location == finalText.length-1)
        {
            filterdPeople = @[];
        }
        else if (rgy.length > 0)
        {
            NSString *AfterAd = [finalText substringFromIndex:[finalText rangeOfString:@"@" options:NSBackwardsSearch].location+1];
            
            if (AfterAd.length > 0)
            {
                if ([[AfterAd substringToIndex:1] isEqualToString:@" "])
                {
                    filterdPeople = @[];
                }
                else
                {
                    filterdPeople = [aryPeoples filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(user_name contains[cd] %@)", AfterAd]];
                }
            }
            else
            {
                
            }
        }
        else
        {
            filterdPeople = @[];
        }
    }
    else
    {
        btnSend.enabled = false;
        filterdPeople = @[];
    }
    
    [self updateTablePeople];
    return true;
}

-(void)updateTablePeople
{
    [tblPeopleList flashScrollIndicators];
    
    [tblPeopleList reloadData];
    tblpeopleHeight.constant = tblPeopleList.contentSize.height;
    [self.view setNeedsUpdateConstraints];
    [self updateScrollIndicator];
    [UIView animateWithDuration:0.25f animations:^{
        [tblPeopleList layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self updateScrollIndicator];
    }];
    
}

-(void)updateScrollIndicator
{
    if (tblPeopleList.contentSize.height != 0)
    {
        scrollIndheight.constant = (tblPeopleList.frame.size.height / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height ;
        scrollIndY.constant = (tblPeopleList.contentOffset.y / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height;
    }
}

#pragma mark - Action
#pragma mark

-(IBAction)btnSpotifyCLK:(UIButton *)sender
{
//    sender.selected = !sender.selected;
    SelectMusicViewController *objSele = [[SelectMusicViewController alloc] init];
    [self.navigationController pushViewController:objSele animated:YES];
}

- (IBAction)btnBackClicked:(id)sender
{
    [mapView clearMGLMapView];
    [mapView removeFromSuperview] ;
    mapView = nil ;

    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnTabbarClicked:(UIButton *)sender
{
    btnRun.selected = btnComment.selected = NO;
    viewLineRun.hidden = viewLineComment.hidden = YES;
    
    if (sender.tag == 0)
    {
        btnRun.selected = YES;
        viewLineRun.hidden = NO;
        viewRun.hidden = imgShadow.hidden = NO;
        viewComment.hidden = YES;
    }
    else
    {
        btnComment.selected = YES;
        viewLineComment.hidden = NO;
        viewRun.hidden = imgShadow.hidden = YES;
        viewComment.hidden = NO;
    }
}

- (IBAction)btnAudioSettingClicked:(id)sender
{
    AudioSettingViewController *objAudio = [[AudioSettingViewController alloc] init];
    [self.navigationController pushViewController:objAudio animated:YES];
}

- (IBAction)btnSendClicked:(id)sender
{
    [appDelegate hideKeyboard];
    
    if ([txtComment.text length] != 0)
        [self addCommentOnSession];
}

@end
