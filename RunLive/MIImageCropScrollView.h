//
//  MIImageCropScrollView.h
//  RunLive
//
//  Created by mac-0005 on 19/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIImageCropScrollView : UIScrollView<UIScrollViewDelegate>

@property(nonatomic,strong) UIImageView *imageView;

-(void)imageToDisplay:(UIImage *)image;

-(void)zoom;

@end
