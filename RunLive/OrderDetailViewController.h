//
//  OrderDetailViewController.h
//  RunLive
//
//  Created by mac-00012 on 5/30/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailViewController : SuperViewController
{
    IBOutlet UICollectionView *clOrder;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UILabel *lblName,*lblPhone,*lblAdderss,*lblCode,*lblOrderStatus,*lblTotalPoints;
    IBOutlet UIButton *btnProblemOrder,*btnTrackMyOrder;
    IBOutlet UIScrollView *scrollViewContainer;
    
    
}

@property(strong, nonatomic) NSString *strOrderId;

@end
