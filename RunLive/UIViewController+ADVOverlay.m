//
//  Copyright © 2012 BeauxApps, LLC
//

#import "UIViewController+ADVOverlay.h"

#import "NSObject+NewProperty.h"
#import "CustomOneButtonAlertView.h"
#import "CustomTwoButtonAlertView.h"
#import "GallexyAlertView.h"




static NSString *const ADVOverlayViewControllerKey = @"ADVOverlayViewControllerKey";

static const NSTimeInterval AnimationDuration = 0.3;

@implementation UIViewController (ADVOverlay)

- (void) presentOverlayViewController:(UIViewController *)controller
                             animated:(BOOL)animated
                           completion:(MIVoidBlock)completion
{
    UIViewController *overlayViewController = [self objectForKey:ADVOverlayViewControllerKey];
    if (overlayViewController) [self dismissOverlayViewControllerAnimated:NO completion:nil];

    
    [self setObject:controller forKey:ADVOverlayViewControllerKey];
    
    [self addChildViewController:controller];
    controller.view.frame = self.view.bounds;
    
    [self viewWillDisappear:animated];
    [controller viewWillAppear:animated];
    
    [UIView transitionWithView:self.view duration:animated ? AnimationDuration : 0 options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [self.view addSubview:controller.view];
    } completion:^(BOOL finished) {
        
        [controller didMoveToParentViewController:self];
        
        [self viewDidDisappear:animated];
        [controller viewDidAppear:animated];
        
        if (completion) completion();
    }];
}

- (void) dismissOverlayViewControllerAnimated:(BOOL)animated
                                   completion:(MIVoidBlock)completion
{
    UIViewController *controller = [self objectForKey:ADVOverlayViewControllerKey];
    if (controller)
    {
        UIViewController *parentViewController = controller.parentViewController;
        
        [controller willMoveToParentViewController:nil];

        [controller viewWillDisappear:animated];
        [parentViewController viewWillAppear:animated];
        
        [UIView transitionWithView:self.view duration:animated ? AnimationDuration : 0 options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [controller.view removeFromSuperview];
        } completion:^(BOOL finished) {
            [controller removeFromParentViewController];
            [self setObject:nil forKey:ADVOverlayViewControllerKey];
            
            [controller viewDidDisappear:animated];
            [parentViewController viewDidAppear:animated];
            
            if (completion) completion();
        }];
    }
}

-(PopUpViewController *)presentViewWithOutsideClickedDisable:(UIView *)view
{
    return [self presentViewOnPopUpViewController:view shouldClickOutside:NO dismissed:nil];
}

-(PopUpViewController *)presentView:(UIView *)view
{
    return [self presentViewOnPopUpViewController:view shouldClickOutside:YES dismissed:nil];
}

-(PopUpViewController *)presentView_DissmissEffect:(UIView *)view
{
    return [self presentViewOnPopUpViewController_DissmissEffect:view shouldClickOutside:YES dismissed:nil];
}
-(PopUpViewController *)presentView:(UIView *)view shouldHideOnOutsideClick:(BOOL)shouldHideOnOutsideClick
{
    return [self presentViewOnPopUpViewController:view shouldClickOutside:shouldHideOnOutsideClick dismissed:nil];
}

-(PopUpViewController *)presentView:(UIView *)view dismissed:(MIVoidBlock)completion
{
   return [self presentViewOnPopUpViewController:view shouldClickOutside:YES dismissed:completion];
}

-(PopUpViewController *)presentViewOnPopUpViewController:(UIView *)view shouldClickOutside:(BOOL)click dismissed:(MIVoidBlock)completion
{
    PopUpViewController *objPopUp = [[PopUpViewController alloc] init];
    objPopUp.shouldHideOnOutsideClick = click;
    [objPopUp.view addSubview:view];
    [self presentOverlayViewController:objPopUp animated:YES completion:nil];
    
    [objPopUp setCloseHandler:^(BOOL animated)
     {
         [self dismissOverlayViewControllerAnimated:YES completion:completion];
     }];
    
    return objPopUp;
}
-(PopUpViewController *)presentViewOnPopUpViewController_DissmissEffect:(UIView *)view shouldClickOutside:(BOOL)click dismissed:(MIVoidBlock)completion
{
    PopUpViewController *objPopUp = [[PopUpViewController alloc] init];
    objPopUp.shouldHideOnOutsideClick = click;
    [objPopUp.view addSubview:view];
    [self presentOverlayViewController:objPopUp animated:YES completion:nil];
    
    __weak typeof(objPopUp) weakObjPopUp = objPopUp;
    [objPopUp setCloseHandler:^(BOOL animated)
     {
         [UIView animateWithDuration:0.4 animations:^{
             weakObjPopUp.view.backgroundColor = [weakObjPopUp.view.backgroundColor colorWithAlphaComponent:0];
             CViewSetY(view,CViewY(view) + CViewHeight(view));
         } completion:^(BOOL finished) {
             [self dismissOverlayViewControllerAnimated:YES completion:completion];
         }];
     }];
    
    return objPopUp;
}

-(void)customAlertViewWithOneButton:(NSString *)title Message:(NSString *)strMessage ButtonText:(NSString *)strButtonText Animation:(BOOL)isAnimate completed:(void (^)())completion;
{
    // First remove custom view if it is there....
    for (UIView *objview in appDelegate.window.subviews)
    {
        if ([objview isKindOfClass:[CustomOneButtonAlertView class]])
        {
            CustomOneButtonAlertView *objCustomView = (CustomOneButtonAlertView *)objview;
            [objCustomView removeFromSuperview];
        }
    }
    
    CustomOneButtonAlertView *objPopUp = [CustomOneButtonAlertView initCustomOneButtonAlertView];
    [objPopUp alertView:title Message:strMessage ButtonTitle:strButtonText];
    
    if (isAnimate)
    {
        objPopUp.viewContainer.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 animations:^{
            objPopUp.viewContainer.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    
    [appDelegate.window addSubview:objPopUp];
    
    [objPopUp.btnMain touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion();
        }
        
    }];
}


-(void)customAlertViewWithTwoButton:(NSString *)title Message:(NSString *)strMessage ButtonFirstText:(NSString *)strButtonFirst ButtonSecondText:(NSString *)strButtonSecond Animation:(BOOL)isAnimate completed:(void (^)(int index))completion;
{
    // First remove custom view if it is there....
    for (UIView *objview in appDelegate.window.subviews)
    {
        if ([objview isKindOfClass:[CustomTwoButtonAlertView class]])
        {
            CustomTwoButtonAlertView *objCustomView = (CustomTwoButtonAlertView *)objview;
            [objCustomView removeFromSuperview];
        }
    }
    
    CustomTwoButtonAlertView *objPopUp = [CustomTwoButtonAlertView initCustomTwoButtonAlertView];
    [objPopUp alertView:title Message:strMessage ButtonFirstText:strButtonFirst ButtonSecondText:strButtonSecond];
    
    if (isAnimate)
    {
        objPopUp.viewContainer.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 animations:^{
            objPopUp.viewContainer.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    
    [appDelegate.window addSubview:objPopUp];
    
    [objPopUp.btnFirst touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion((int)objPopUp.btnFirst.tag);
        }
        
    }];
    
    [objPopUp.btnSecond touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion((int)objPopUp.btnSecond.tag);
        }
        
    }];
}

-(void)customAlertViewWithThreeButton:(NSString *)title Message:(NSString *)strMessage ButtonFirstText:(NSString *)strButtonFirst ButtonSecondText:(NSString *)strButtonSecond ButtonThirdText:(NSString *)strButtonThird Animation:(BOOL)isAnimate completed:(void (^)(int index))completion;
{
    // First remove custom view if it is there....
    for (UIView *objview in appDelegate.window.subviews)
    {
        if ([objview isKindOfClass:[GallexyAlertView class]])
        {
            GallexyAlertView *objCustomView = (GallexyAlertView *)objview;
            [objCustomView removeFromSuperview];
        }
    }
    
    GallexyAlertView *objPopUp = [GallexyAlertView initGallexyAlertView];
    [objPopUp alertView:title Message:strMessage ButtonFirstText:strButtonFirst ButtonSecondText:strButtonSecond ButtonThirdText:strButtonThird];
    
    if (isAnimate)
    {
        objPopUp.viewContainer.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 animations:^{
            objPopUp.viewContainer.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    
    [appDelegate.window addSubview:objPopUp];
    
    [objPopUp.btnFirst touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion((int)objPopUp.btnFirst.tag);
        }
        
    }];
    
    [objPopUp.btnSecond touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion((int)objPopUp.btnSecond.tag);
        }
        
    }];
    
    [objPopUp.btnThird touchUpInsideClicked:^{
        [objPopUp removeFromSuperview];
        
        if (completion) {
            completion((int)objPopUp.btnThird.tag);
        }
        
    }];
}


-(void)forcefullyRemoveCustomAlertFromSuperView
{
    for (UIView *objview in appDelegate.window.subviews)
    {
        if ([objview isKindOfClass:[CustomTwoButtonAlertView class]])
        {
            CustomTwoButtonAlertView *objCustomView = (CustomTwoButtonAlertView *)objview;
            [objCustomView removeFromSuperview];
        }
        
        if ([objview isKindOfClass:[CustomOneButtonAlertView class]])
        {
            CustomOneButtonAlertView *objCustomView = (CustomOneButtonAlertView *)objview;
            [objCustomView removeFromSuperview];
        }
        
        if ([objview isKindOfClass:[GallexyAlertView class]])
        {
            GallexyAlertView *objGallexy = (GallexyAlertView *)objview;
            [objGallexy removeFromSuperview];
        }
    }
}

@end
