//
//  SessionCountDown.h
//  RunLive
//
//  Created by mac-00012 on 6/27/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionCountDown : UIView<AVAudioSessionDelegate>
{
    IBOutlet UIView *viewVideo;
}

@property(nonatomic,strong) NSString *strSessionId;

@end
