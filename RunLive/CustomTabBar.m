//
//  CustomTabBar.m
//  DigitalAddress
//
//  Created by mac-0002 on 08/07/13.
//  Copyright (c) 2013 Mindinventory. All rights reserved.
//

#import "CustomTabBar.h"

#define CTABBARHeight Is_iPhone_X ? 89 : 49

@implementation CustomTabBar

+(id)CustomTabBar
{
    CustomTabBar *tabBar;
    tabBar = [[[NSBundle mainBundle] loadNibNamed:@"CustomTabBar" owner:nil options:nil] lastObject];
    
    //tabBar.frame = CGRectMake(0, CScreenHeight - 49, CScreenWidth, 49);
    tabBar.frame = CGRectMake(0, Is_iPhone_X ?  CScreenHeight-49 - 20 : CScreenHeight-49, CScreenWidth, CTABBARHeight);
    
    tabBar.selectedTab = -1;
    [tabBar btnTabBarClicked:tabBar.btnTab1];
    
    tabBar.layer.masksToBounds = NO;
    tabBar.layer.shadowRadius = 8;
    tabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    tabBar.layer.shadowOffset = CGSizeMake(0, -3);
    tabBar.layer.shadowOpacity = .5f;

    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    if ([tabBar isKindOfClass:[CustomTabBar class]])
        return tabBar;
    else
        return nil;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.viewTabNotificationBadge.layer.cornerRadius = CGRectGetHeight(self.viewTabNotificationBadge.frame)/2;
    self.viewNotificationBadge.layer.cornerRadius = CGRectGetHeight(self.viewNotificationBadge.frame)/2;
    
    self.viewTabMessageBadge.layer.cornerRadius = CGRectGetHeight(self.viewTabNotificationBadge.frame)/2;
    self.viewMessageBadge.layer.cornerRadius = CGRectGetHeight(self.viewNotificationBadge.frame)/2;
 
    if (Is_iPhone_5) {
        cnViewMessageBadgeTralling.constant = -13;
        cnViewNotificationBadgeTralling.constant = -16;
    }
    
}

-(BOOL)showWarningAlert:(UIButton *)btnSelected
{
    if (!appDelegate.objSessionGoingToStart || btnSelected.tag == _btnTab4.tag)
        return NO;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = CDateFormater;
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    double currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    double sessionEndTimeStamp = appDelegate.objSessionGoingToStart.session_end_time.doubleValue;
    double difTimeStamp = sessionEndTimeStamp - currentTimestamp;
    
    if (difTimeStamp > 0 && difTimeStamp < 60)
        return YES;
    else
        return NO;
}

-(IBAction)btnTabBarClicked:(UIButton *)tab
{
    
    if ([self showWarningAlert:tab])
    {
        appDelegate.isStartSessionPopUp = YES;
        
        CustomAlertView *objAlert = [CustomAlertView viewFromXib];
        [appDelegate.objTabBarController presentViewOnPopUpViewController:objAlert shouldClickOutside:NO dismissed:nil];
        [objAlert initWithTitle:@"WARNING!" message:CMessageSessionStartWarning firstButton:@"OK" secondButton:@"LEAVE ANYWAY"];
        
        [objAlert.btnFirst touchUpInsideClicked:^{
            // OK BTN CLK
            [appDelegate.objTabBarController dismissOverlayViewControllerAnimated:YES completion:nil];
        }];
        
        [objAlert.btnSecond touchUpInsideClicked:^{
            // Move ahead
            
            appDelegate.objSessionGoingToStart = nil;
            [self tabBarMovment:tab];
            [appDelegate.objTabBarController dismissOverlayViewControllerAnimated:YES completion:nil];
        }];
    }
    else
    {
        [self tabBarMovment:tab];
    }
}


-(void)tabBarMovment:(UIButton *)tab
{
    if (self.selectedTab == tab.tag)
        return;
    
    self.selectedTab = (int)tab.tag;
    
    appDelegate.isShowActivityScreen = YES;
    
    _btnTab1.selected = _btnTab2.selected = _btnTab3.selected = _btnTab4.selected = _btnTab5.selected = NO;
    
    tab.selected = YES;
    [appDelegate.objTabBarController setSelectedIndex:self.selectedTab];
    
    _imgTab1.image = [UIImage imageNamed:@"tab1_unselected"];
    _imgTab2.image = [UIImage imageNamed:@"tab2_unselected"];
    _imgTab3.image = [UIImage imageNamed:@"tab3_unselected"];
    _imgTab4.image = [UIImage imageNamed:@"tab4_unselected"];
    _imgTab5.image = [UIImage imageNamed:@"tab5_unselected"];
    
    UIImageView *imagView = [[UIImageView alloc] init];
    imagView.frame = tab.frame;
    switch (tab.tag)
    {
        case 0:
        {
            _imgTab1.image = [UIImage imageNamed:@"tab1_selected"];
            imagView = _imgTab1;
        }
            break;
        case 1:
        {
            _imgTab2.image = [UIImage imageNamed:@"tab2_selected"];
            imagView = _imgTab2;
        }
            break;
        case 2:
        {
            _imgTab3.image = [UIImage imageNamed:@"tab3_selected"];
            imagView = _imgTab3;
        }
            break;
        case 3:
        {
            _imgTab4.image = [UIImage imageNamed:@"tab4_selected"];
            imagView = _imgTab4;
        }
            break;
        case 4:
        {
            _imgTab5.image = [UIImage imageNamed:@"tab5_selected"];
            imagView = _imgTab5;
        }
            break;
            
        default:
            break;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView transitionWithView:imagView duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            imagView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        }completion:^(BOOL finished) {
            imagView.transform = CGAffineTransformIdentity;
        }];
    });
    
    if (self.selectedTab == 2)
    {
        self.viewTabNotificationBadge.backgroundColor = self.viewTabMessageBadge.backgroundColor = CRGB(29, 30, 46);
        self.backgroundColor = CRGB(29, 30, 46);
        
        self.layer.masksToBounds = YES;
        self.layer.shadowRadius = 0;
        self.layer.shadowColor = [UIColor clearColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowOpacity = 0;
    }
    else
    {
        self.viewTabNotificationBadge.backgroundColor = self.viewTabMessageBadge.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.masksToBounds = NO;
        self.layer.shadowRadius = 8;
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0, -3);
        self.layer.shadowOpacity = .5f;
    }
    
    [appDelegate toCheckRunningSessionIsAvailable];

}

@end
