/*
    Copyright (C) 2014 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    
                Contains shared helper methods on HKHealthStore that are specific to Fit's use cases.
            
*/

@import HealthKit;

@interface HKHealthStore (AAPLExtensions)

// Fetches the single most recent quantity of the specified type.
//- (void)aapl_mostRecentQuantitySampleOfType:(HKQuantityType *)quantityType predicate:(NSPredicate *)predicate completion:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;


// Read/Write Functions
- (NSSet *)dataTypesToWrite;
- (NSSet *)dataTypesToRead;

// BPM related functions
-(void)fetchBPMValueFromHealthKit:(HKWorkout *)dicWork completion:(void (^)(HKQuantity *quantity, NSError *error))completion;



// Weight Related Functions
//-(void)storeWeightInHealthKit:(float)weight completion:(void (^)(BOOL success, NSError *error))completion;


// Height Related Functions
//-(void)storeHeightInHealthKit:(float)height completion:(void (^)(BOOL success, NSError *error))completion;

//- (void)fetchUserHeightFromHealthKit:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;


// Energy Related Functions
//-(void)storeEnergyInHealthKit:(float)energy completion:(void (^)(BOOL success, NSError *error))completion;

//- (void)fetchUserEnergyFromHealthKit:(void (^)(HKQuantity *mostRecentQuantity, NSError *error))completion;


// Workout related functions
//-(void)storeWorkoutInHealthKit:(NSDictionary *)dicWork completion:(void (^)(BOOL success, NSError *error))completion;

//-(void)fetchWorkoutFromHealthKit:(void (^)(HKWorkout *workout, NSError *error))completion;




@end






