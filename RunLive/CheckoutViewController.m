//
//  CheckoutViewController.m
//  RunLive
//
//  Created by mac-0008 on 24/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "CheckoutViewController.h"

@interface CheckoutViewController ()

@end

@implementation CheckoutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setInitInTextFiled:txtFirstName andPlaceholder:@"First Name"];
    [self setInitInTextFiled:txtLastName andPlaceholder:@"Last Name"];
    [self setInitInTextFiled:txtAddress1 andPlaceholder:@"Address Line 1"];
    [self setInitInTextFiled:txtAddress2 andPlaceholder:@"Address Line 2"];
    [self setInitInTextFiled:txtCity andPlaceholder:@"City"];
    [self setInitInTextFiled:txtStateProvinceRegion andPlaceholder:@"State/Province/Region"];
    [self setInitInTextFiled:txtPostcode andPlaceholder:@"Postcode"];
    [self setInitInTextFiled:txtCountry andPlaceholder:@"Country"];
    [self setInitInTextFiled:txtphoneNumber andPlaceholder:@"Phone number"];
    
    [self getLastAddressFromServer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - API Functions

-(void)getLastAddressFromServer
{

    [[APIRequest request] getLastAddress:^(id responseObject, NSError *error)
    {
        NSDictionary *dicAdderss = [responseObject valueForKey:CJsonData];

        if ([dicAdderss objectForKey:@"name"] && [dicAdderss stringValueForJSON:@"name"].length > 0)
        {
            txtFirstName.text = [dicAdderss stringValueForJSON:@"name"];
            [txtFirstName floatTheLabel:@"blank" andText:txtFirstName.text];
        }
        
        if ([dicAdderss objectForKey:@"fore_name"] && [dicAdderss stringValueForJSON:@"fore_name"].length > 0)
        {
            txtLastName.text = [dicAdderss stringValueForJSON:@"fore_name"];
            [txtLastName floatTheLabel:@"blank" andText:txtLastName.text];
        }
        
        
        if ([dicAdderss objectForKey:@"street"] && [dicAdderss stringValueForJSON:@"street"].length > 0)
        {
            txtAddress1.text = [dicAdderss stringValueForJSON:@"street"];
            [txtAddress1 floatTheLabel:@"blank" andText:txtAddress1.text];
        }
        
        
        if ([dicAdderss objectForKey:@"number"] && [dicAdderss stringValueForJSON:@"number"].length > 0)
        {
            txtAddress2.text = [dicAdderss stringValueForJSON:@"number"];
            [txtAddress2 floatTheLabel:@"blank" andText:txtAddress2.text];
        }
        
        
        if ([dicAdderss objectForKey:@"city"] && [dicAdderss stringValueForJSON:@"city"].length > 0)
        {
            txtCity.text = [dicAdderss stringValueForJSON:@"city"];
            [txtCity floatTheLabel:@"blank" andText:txtCity.text];
        }
        
        
        if ([dicAdderss objectForKey:@"state"] && [dicAdderss stringValueForJSON:@"state"].length > 0)
        {
            txtStateProvinceRegion.text = [dicAdderss stringValueForJSON:@"state"];
            [txtStateProvinceRegion floatTheLabel:@"blank" andText:txtStateProvinceRegion.text];
        }
        
        
        if ([dicAdderss objectForKey:@"post_code"] && [dicAdderss stringValueForJSON:@"post_code"].length > 0)
        {
            txtPostcode.text = [dicAdderss stringValueForJSON:@"post_code"];
            [txtPostcode floatTheLabel:@"blank" andText:txtPostcode.text];
        }
        
        
        if ([dicAdderss objectForKey:@"country"] && [dicAdderss stringValueForJSON:@"country"].length > 0)
        {
            txtCountry.text = [dicAdderss stringValueForJSON:@"country"];
            [txtCountry floatTheLabel:@"blank" andText:txtCountry.text];
        }
        
        
        if ([dicAdderss objectForKey:@"phone_number"] && [dicAdderss stringValueForJSON:@"phone_number"].length > 0)
        {
            txtphoneNumber.text = [dicAdderss stringValueForJSON:@"phone_number"];
            [txtphoneNumber floatTheLabel:@"blank" andText:txtphoneNumber.text];
        }
        
    }];
}

#pragma mark - Helper Method
#pragma mark

- (void)setInitInTextFiled:(ACFloatingTextField *)textfield andPlaceholder:(NSString *)placeholder
{
    [textfield setTextFieldPlaceholderText:placeholder];
    textfield.imageWrong = @"wrong";
    textfield.selectedLineColor = CLineSelected;
    textfield.placeHolderColor = CRGB(255, 255, 255);
    textfield.selectedPlaceHolderColor = CRGB(143,160,182);
    textfield.lineColor = CRGB(117, 121, 144);
}

#pragma mark  UITextfield Delegates
#pragma mark

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtFirstName)
        [txtFirstName floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtLastName )
        [txtLastName floatTheLabel:@"blank" andText:textField.text];
    else if (textField == txtAddress1)
        [txtAddress1 floatTheLabel:@"blank" andText:textField.text];
    else if (textField == txtAddress2)
        [txtAddress2 floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtCity)
        [txtCity floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtStateProvinceRegion)
        [txtStateProvinceRegion floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtPostcode)
        [txtPostcode floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtCountry)
        [txtCountry floatTheLabel:@"blank" andText:textField.text];
    else if(textField == txtphoneNumber)
        [txtphoneNumber floatTheLabel:@"blank" andText:textField.text];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txtFirstName)
        [txtFirstName floatTheLabel:@"" andText:textField.text];
    else if(textField == txtLastName )
        [txtLastName floatTheLabel:@"" andText:textField.text];
    else if (textField == txtAddress1)
        [txtAddress1 floatTheLabel:@"" andText:textField.text];
    else if (textField == txtAddress2)
        [txtAddress2 floatTheLabel:@"" andText:textField.text];
    else if(textField == txtCity)
        [txtCity floatTheLabel:@"" andText:textField.text];
    else if(textField == txtStateProvinceRegion)
        [txtStateProvinceRegion floatTheLabel:@"" andText:textField.text];
    else if(textField == txtPostcode)
        [txtPostcode floatTheLabel:@"" andText:textField.text];
    else if(textField == txtCountry)
        [txtCountry floatTheLabel:@"" andText:textField.text];
    else if(textField == txtphoneNumber)
        [txtphoneNumber floatTheLabel:@"" andText:textField.text];
    
}

-(BOOL)checkAlphabetValidation:(NSString *)string
{
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (range.location == 0 && [string isEqualToString:@" "])
        return NO;
    
    if (textField == txtFirstName)
    {
        if ([self checkAlphabetValidation:string])
            [txtFirstName floatTheLabel:@"blank" andText:strNew];
        
        return [self checkAlphabetValidation:string];
    }
    else if(textField == txtLastName )
    {
        if ([self checkAlphabetValidation:string])
            [txtLastName floatTheLabel:@"blank" andText:strNew];
        
        return [self checkAlphabetValidation:string];
    }
    else if (textField == txtAddress1)
        [txtAddress1 floatTheLabel:@"blank" andText:strNew];
    else if (textField == txtAddress2)
        [txtAddress2 floatTheLabel:@"blank" andText:strNew];
    else if(textField == txtCity)
    {
        [txtCity floatTheLabel:@"blank" andText:strNew];

//        if ([self checkAlphabetValidation:string])
//            [txtCity floatTheLabel:@"blank" andText:strNew];
//        
//        return [self checkAlphabetValidation:string];
    }
    else if (textField == txtStateProvinceRegion)
        [txtStateProvinceRegion floatTheLabel:@"blank" andText:strNew];
    else if(textField == txtPostcode)
        [txtPostcode floatTheLabel:@"blank" andText:strNew];
    else if(textField == txtCountry)
    {
        if ([self checkAlphabetValidation:string])
            [txtCountry floatTheLabel:@"blank" andText:strNew];
        
        return [self checkAlphabetValidation:string];
    }
    else if(textField == txtphoneNumber)
        [txtphoneNumber floatTheLabel:@"blank" andText:strNew];
    
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Action Event

-(IBAction)btnCancelCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnSubmitCLK:(id)sender
{
    [appDelegate hideKeyboard];
    
    if (![txtFirstName.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter first name." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtLastName.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter last name." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtAddress1.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter address line 1." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtCity.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter city." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtStateProvinceRegion.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter State/Province/Region." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtPostcode.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter postcode." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtCountry.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter country." ButtonText:@"OK" Animation:YES completed:nil];
    else if (![txtphoneNumber.text isBlankValidationPassed])
        [self customAlertViewWithOneButton:@"" Message:@"Please enter phone number." ButtonText:@"OK" Animation:YES completed:nil];
    else
    {
        /*
        User Name                - JOHN "GULLIBLE" DOE
        Street                       - CENTER FOR FINANCIAL ASSISTANCE TO DEPOSED NIGERIAN ROYALTY
        Street No and city    - 421 E DRACHMAN
        City/Phone No           - TUCSON AZ 85705-7598
        Country                     - USA
         
         USERNAME
         ADDRESS1/ ADDRESS2/ PROINCE/ REGION/ CITY/ STATE/ POSTALCODE
         PHONE NUMBER
         COUNTRY
         
         */

        NSString *strAddress = [NSString stringWithFormat:@"%@ %@, %@ %@, %@, %@, %@, %@, %@.",txtFirstName.text,txtLastName.text,txtAddress1.text,txtAddress2.text,txtCity.text,txtStateProvinceRegion.text,txtPostcode.text,txtphoneNumber.text,txtCountry.text];

        
        CustomAlertView *objAlert = [CustomAlertView viewFromXib];
        [self presentViewOnPopUpViewController:objAlert shouldClickOutside:NO dismissed:nil];
        [objAlert initWithTitle:@"Is this right?" message:strAddress firstButton:@"NO!" secondButton:@"YES!"];

        [objAlert.btnFirst touchUpInsideClicked:^{
            // NO BTN CLK
            [self dismissOverlayViewControllerAnimated:YES completion:nil];
        }];
        
        [objAlert.btnSecond touchUpInsideClicked:^{
            // YES BTN CLK
            [[APIRequest request] GenerateOrder:txtFirstName.text ForName:txtLastName.text Street:txtAddress1.text Number:txtAddress2.text City:txtCity.text PostCode:txtPostcode.text Country:txtCountry.text PhoneNumber:txtphoneNumber.text State:txtStateProvinceRegion.text completed:^(id responseObject, NSError *error)
             {
                 [self dismissOverlayViewControllerAnimated:YES completion:nil];
                 viewOrderSuccess.hidden = NO;
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.navigationController popToRootViewControllerAnimated:YES];
                 });
             }];
        }];
    }
    
}

@end
