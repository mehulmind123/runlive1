//
//  PushNotificationManager.m
//  MI API Example
//
//  Created by mac-0001 on 25/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "PushNotificationManager.h"
#import "Master.h"
#import "NSObject+NewProperty.h"


//#import <Parse/Parse.h>

@interface NSObject ()


// ParseCrashReporting

+ (void)enable;



// Parse

+ (void)setApplicationId:(NSString *)applicationId clientKey:(NSString *)clientKey;



// PFAnalytics

+ (void)trackAppOpenedWithLaunchOptionsInBackground:(NSDictionary *)launchOptions block:(id)block;



// PFInstallation

+ (instancetype)currentInstallation;

@property (nonatomic, strong) NSString *deviceToken;

@property (nonatomic, assign) NSInteger badge;

@property (nonatomic, strong) NSArray *channels;

- (void)setDeviceTokenFromData:(NSData *)deviceTokenData;




// PFObject

- (void)saveEventually;

- (void)removeObjectForKey:(NSString *)key;



// PFQuery

-(id)queryWithClassName:(NSString *)className;

- (void)whereKey:(NSString *)key equalTo:(id)object;

- (void)findObjectsInBackgroundWithBlock:(id)block;

@end




@interface NSObject ()

- (void)applicationDidReceiveRemoteNotification:(NSDictionary *)userInfo fromState:(UIApplicationState)state;

@end





@interface PushNotificationManager ()
{
    Class PFInstallation;
    Class PFQuery;
    
    NSMutableArray *arrNotificationOnservers;
}
@end


@implementation PushNotificationManager

+ (instancetype)sharedInstance
{
    static PushNotificationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PushNotificationManager alloc] init];
    });
    
    return _sharedInstance;
}

-(NSMutableArray *)arrNotificationOnservers
{
    if (!arrNotificationOnservers)
        arrNotificationOnservers = [[NSMutableArray alloc] init];

    return arrNotificationOnservers;
}


#pragma mark - Parse Push Methods

-(void)registerForPushNotification
{
    if (IS_IPHONE_SIMULATOR)
        return;
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else if([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotificationTypes:)])
    {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:1];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

#pragma mark - # MI-ParsePush

- (void)addNotificationObserver:(CanNotificaitonHandledWithBlock)observer
{
    if (![[self arrNotificationOnservers] containsObject:observer])
        [[self arrNotificationOnservers] addObject:observer];
        
}

- (void)removeNotificationObserver:(CanNotificaitonHandledWithBlock)observer
{
    if ([[self arrNotificationOnservers] containsObject:observer])
        [[self arrNotificationOnservers] removeObject:observer];
}



@end
