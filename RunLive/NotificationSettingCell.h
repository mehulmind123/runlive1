//
//  NotificationSettingCell.h
//  RunLive
//
//  Created by mac-00012 on 11/8/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSettingCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lblTitle,*lblSubTitle;

@property(weak,nonatomic) IBOutlet UISwitch *btnSwitch;
@end

