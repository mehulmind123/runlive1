//
//  MyBasketViewController.m
//  RunLive
//
//  Created by mac-00012 on 10/22/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MyBasketViewController.h"
#import "BasketProductCell.h"
#import "ProductDetailsViewController.h"
#import "CheckoutViewController.h"



#define CELLWIDTH CScreenWidth * 80/100

@interface MyBasketViewController ()

@end

@implementation MyBasketViewController
{
    NSMutableArray *arrProduct;
    NSIndexPath *SelectedIndexPathTapBar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:YES];


    [clProduct registerNib:[UINib nibWithNibName:@"BasketProductCell" bundle:nil] forCellWithReuseIdentifier:@"BasketProductCell"];
    
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:0 inSection:0];
    arrProduct = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    
    [self getDataFromServer];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)getDataFromServer
{
    lblUserName.text = [NSString stringWithFormat:@"%@, YOU HAVE",appDelegate.loginUser.user_name.uppercaseString];
    lblEmptyUserName.text = [NSString stringWithFormat:@"%@, YOUR BASKET IS",appDelegate.loginUser.user_name.uppercaseString];
    
    [[APIRequest request] basketList:^(id responseObject, NSError *error)
    {
        [arrProduct removeAllObjects];
        [arrProduct addObjectsFromArray:[responseObject valueForKey:CJsonData]];
        [clProduct reloadData];
        
        pageControl.numberOfPages = arrProduct.count;
        pageControl.currentPage = 0;

        if (arrProduct.count > 0)
        {
            viewEmptyBasket.hidden = YES;
            lblProductCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrProduct.count];
        }
        else
            viewEmptyBasket.hidden = NO;
        
    }];
}

#pragma mark - Collection View Delegate
#pragma mark
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrProduct.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"BasketProductCell";
    BasketProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.transform = [SelectedIndexPathTapBar isEqual:indexPath] ? CGAffineTransformIdentity : TRANSFORM_CELL_VALUE;
    
    NSDictionary *dicData = [arrProduct objectAtIndex:indexPath.item];
    cell.lblProductName.text = [dicData stringValueForJSON:@"name"];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *strPoint = [numberFormatter stringFromNumber:[NSNumber numberWithInt:[dicData stringValueForJSON:@"points"].intValue]];
    
    cell.lblProductPoints.text = [NSString stringWithFormat:@"-%@ RC",strPoint];
   
    [cell.imgProduct setImageWithURL:[appDelegate resizeImage:nil Height:[NSString stringWithFormat:@"%f",CViewHeight(cell.imgProduct)*2] imageURL:[dicData stringValueForJSON:@"image"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];


    
#pragma mark - Remove CLK
    [cell.btnRemove touchUpInsideClicked:^{
        [[APIRequest request] removeProductFromBasket:[dicData stringValueForJSON:@"_id"] completed:^(id responseObject, NSError *error)
         {
             [arrProduct removeObjectAtIndex:indexPath.item];
             [clProduct reloadData];
             
             if (SelectedIndexPathTapBar.item < arrProduct.count)
             {
                 [clProduct scrollToItemAtIndexPath:SelectedIndexPathTapBar atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
             }
             else if(arrProduct.count > 0)
             {
                 SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:SelectedIndexPathTapBar.item-1 inSection:0];
                 [clProduct scrollToItemAtIndexPath:SelectedIndexPathTapBar atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
             }
             
             pageControl.numberOfPages = arrProduct.count;
             [pageControl setCurrentPage:SelectedIndexPathTapBar.item];
             
             if (arrProduct.count == 0)
                 viewEmptyBasket.hidden = NO;
             
             lblProductCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrProduct.count];
         }];
    }];
    
#pragma mark - More Details
    [cell.btnMoreDetails touchUpInsideClicked:^{
        
        if ([[self navigationController] viewControllers].count>=2)
        {
            if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[ProductDetailsViewController class]])
            {
                [self.navigationController popViewControllerAnimated:NO];
            }
            else
            {
                ProductDetailsViewController *obj = [[ProductDetailsViewController alloc] init];
                obj.strProductId = [dicData stringValueForJSON:@"product_id"];
                [self.navigationController pushViewController:obj animated:NO];
            }
        }
        else
        {
            ProductDetailsViewController *obj = [[ProductDetailsViewController alloc] init];
            obj.strProductId = [dicData stringValueForJSON:@"product_id"];
            [self.navigationController pushViewController:obj animated:NO];
        }
    }];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width * CELLWIDTH)/CScreenWidth , CGRectGetHeight(clProduct.frame) - 10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2, 0, (self.view.frame.size.width - ((self.view.frame.size.width * CELLWIDTH)/CScreenWidth))/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (SelectedIndexPathTapBar)
    {
        BasketProductCell *cell = (BasketProductCell *)[clProduct cellForItemAtIndexPath:SelectedIndexPathTapBar];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];

    }
    
    BasketProductCell *cell = (BasketProductCell *)[clProduct cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        cell.transform = CGAffineTransformIdentity;
    }];
    
    SelectedIndexPathTapBar = indexPath;
    [clProduct scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [pageControl setCurrentPage:(int)SelectedIndexPathTapBar.item];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    //    float pageWidth = (((CGRectGetWidth(clPlayers.frame))/CScreenWidth) + CELLWIDTH); // width + space
    float pageWidth = CELLWIDTH;
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = currentOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
    int index = newTargetOffset / pageWidth;
    
    //    int page =round(scrollView.contentOffset.x/scrollView.bounds.size.width);
    [pageControl setCurrentPage:index];
    
    SelectedIndexPathTapBar = [NSIndexPath indexPathForItem:index inSection:0];
    
    if (index == 0) // If first index
    {
        UICollectionViewCell *cell1 = [clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
        BasketProductCell *cell = (BasketProductCell *)cell1;
        
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        cell = (BasketProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
    }
    else
    {
        UICollectionViewCell *cell1 = [clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        BasketProductCell *cell = (BasketProductCell *)cell1;
        ;
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        
        index --; // left
        cell = (BasketProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
        index ++;
        index ++; // right
        cell = (BasketProductCell *)[clProduct cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }
    
    if(arrProduct.count == 0)
        return;
}


#pragma mark - Action Event
#pragma mark

-(IBAction)btnCloseCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)btnCheckOutCLK:(id)sender
{
    CheckoutViewController *objCheck = [[CheckoutViewController alloc] init];
    [self.navigationController pushViewController:objCheck animated:NO];
}


@end
