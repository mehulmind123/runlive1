//
//  CommentsViewController.m
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "CommentsViewController.h"
#import "commentDPCell.h"
#import "TagpeopleCell.h"

@interface CommentsViewController ()
{
    NSArray *aryPeoples;
    NSArray *filterdPeople; // make ordered predicate
    
    UIRefreshControl *refreshControl;
    int iOffset;
    BOOL isRefershinData;
    
    NSMutableArray *arrCommentList;
}
@end

@implementation CommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    arrCommentList = [NSMutableArray new];
    
    [tblComments registerNib:[UINib nibWithNibName:@"commentDPCell" bundle:nil] forCellReuseIdentifier:@"commentDPCell"];
    [tblPeopleList registerNib:[UINib nibWithNibName:@"TagpeopleCell" bundle:nil] forCellReuseIdentifier:@"TagpeopleCell"];
    
//    tblComments.transform = CGAffineTransformMakeRotation(-M_PI);
//    tblComments.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, CScreenWidth -10);

    tblComments.estimatedRowHeight = 30.0;
    tblComments.rowHeight = UITableViewAutomaticDimension;
    
    tblPeopleList.estimatedRowHeight = 60.0;
    tblPeopleList.rowHeight = 65.00;
    
    txtVComent.placeholder = @"Type your comment here...";
    [txtVComent setFont:[UIFont fontWithName:@"Gilroy-Regular" size:17]];
    txtVComent.delegate = self;
    filterdPeople = @[];
    [self getFriendList];
    
    // <----------------------------------- Add Pull To Refresh ----------------------------------->
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = CNavigationBarColor;
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [tblComments addSubview:refreshControl];
    
    
    iOffset = 0;
    [self getCommentListFromServer];
    
    appDelegate.configureRefreshSessionData = ^(RefreshSessionWithUpdate refreshSessionWithUpdate)
    {
        if ([[appDelegate getTopMostViewController] isKindOfClass:[CommentsViewController class]])
        {
            if (refreshSessionWithUpdate == CommentOnActivity)
            {
                iOffset = 0;
                [self getCommentListFromServer];
            }
        }
    };

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [appDelegate hideTabBar];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tblComments reloadData];
    });

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    ViewScrollSide.hidden = false;
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
     viewScrollIndicator.layer.cornerRadius = viewScrollIndicator.frame.size.width/2;
     viewScrollIndicator.clipsToBounds = true;
     ViewScrollSide.layer.cornerRadius = ViewScrollSide.frame.size.width/2;
     ViewScrollSide.clipsToBounds = true;

}

#pragma mark - Api Function
#pragma mark

-(void)getFriendList
{
    [[APIRequest request] friendList:@"" loader:NO completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             
             NSMutableArray *arrTempUser = [NSMutableArray new];
             
             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"] isKindOfClass:[NSArray class]])
                 [arrTempUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"]];
             
             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"other"] isKindOfClass:[NSArray class]])
                 [arrTempUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"other"]];
             
             aryPeoples = [arrTempUser mutableCopy];
             [tblPeopleList reloadData];
         }
     }];

}

-(void)getCommentListFromServer
{
    if (!self.strActivityId)
        return;
    
    [[APIRequest request] GetActivityCommentList:[NSNumber numberWithInt:iOffset] ActivityId:self.strActivityId completed:^(id responseObject, NSError *error)
    {
        //
        
        [refreshControl endRefreshing];
        if (responseObject && !error)
        {
            if (iOffset == 0)
                [arrCommentList removeAllObjects];
            
            NSArray *arrPreviousComment = arrCommentList.mutableCopy;

            [arrCommentList removeAllObjects];
            [arrCommentList addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            [arrCommentList addObjectsFromArray:arrPreviousComment];
            
            iOffset = [[responseObject valueForKey:@"extra"] stringValueForJSON:@"offset"].intValue;
            [tblComments reloadData];
            
            if (isRefershinData)
                [tblComments setContentOffset:CGPointMake(0, 0) animated:NO];
            else
            {
                if (arrCommentList.count > 0)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
                        [tblComments scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
                    });
                }
            }
            
            isRefershinData = NO;
        }

    }];
    
}


#pragma mark - Pull Refresh
-(void)handleRefresh
{
    if (iOffset == 0)
    {
        [refreshControl endRefreshing];
        return;
    }
    
    isRefershinData = YES;
    [self getCommentListFromServer];
}

#pragma mark - Add comment

-(void)addCommentOnSession
{
    if (!self.strActivityId)
        return;
    
    NSString *strCommentText = txtVComent.text;
    txtVComent.text = @"";
    
    [[APIRequest request] addCommentOnActivity:strCommentText ActivityId:self.strActivityId completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
//            [arrCommentList addObject:[responseObject valueForKey:CJsonData]];
//            [tblComments reloadData];
            
            [[APIRequest request] updateActivityDataInLocal:self.strActivityType ActivityId:self.strActivityId UpdateData:[responseObject valueForKey:@"extra"]];
            
            iOffset = 0;
            [self getCommentListFromServer];
            
//            if (arrCommentList.count > 0)
//            {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrCommentList count]-1 inSection: 0];
//                    [tblComments scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: NO];
//                });
//            }
        }

    }];
}


#pragma mark - Table View Delegate
#pragma mark

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if ([tableView isEqual:tblComments])
        return arrCommentList.count;
    
    return filterdPeople.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:tblComments])
    {
        NSString *strIdentifier = @"commentDPCell";
        commentDPCell *cell = [tblComments dequeueReusableCellWithIdentifier:strIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
         cell.lblTime.text = [appDelegate getPostTimeInSpecificFormate:[dicData stringValueForJSON:@"createdAt"]];
        
        [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"80" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *strUserName1 = [dicData stringValueForJSON:@"user_name"];
        NSString *strComment1 = [dicData stringValueForJSON:@"text"];
        cell.lblContent.text = [NSString stringWithFormat:@"%@ %@",strUserName1, strComment1];

        [cell.lblContent addHashTagAndUserHandler:strUserName1 Complete:^(NSString *urlString) {
            [appDelegate moveOnProfileScreen:urlString UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        
        [cell.btnUser touchUpInsideClicked:^{
            [appDelegate moveOnProfileScreen:strUserName1 UserId:@"" andNavigation:self.navigationController isLoginUser:NO];
        }];
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"TagpeopleCell";
        TagpeopleCell *cell = (TagpeopleCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [filterdPeople objectAtIndex:indexPath.item];
        [cell.imgDP setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.lblContent.text = [dicData stringValueForJSON:@"user_name"];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:tblPeopleList])
    {
        NSString *str = [txtVComent.text substringToIndex:[txtVComent.text rangeOfString:@"@" options:NSBackwardsSearch].location + 1];
        txtVComent.text = [NSString stringWithFormat:@"%@%@ ",str,((TagpeopleCell *)[tblPeopleList cellForRowAtIndexPath:indexPath]).lblContent.text];
        filterdPeople = @[];
        [self updateTablePeople];
    }
    else
    {
        // User Detail here....
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:tblComments])
    {
        NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
        
        if ([[dicData stringValueForJSON:@"user_id"] isEqualToString:appDelegate.loginUser.user_id] || [[dicData stringValueForJSON:@"owner_id"] isEqualToString:appDelegate.loginUser.user_id])
        {
            return YES;
        }
        return NO;
    }
    
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self customAlertViewWithTwoButton:@"" Message:@"Are you sure you want to delete this comment?" ButtonFirstText:@"Yes" ButtonSecondText:@"NO" Animation:YES completed:^(int index) {
            if (index == 0)
            {
                NSDictionary *dicData = [arrCommentList objectAtIndex:indexPath.row];
                
                [[APIRequest request] deleteActivityComment:[dicData stringValueForJSON:@"_id"] completed:^(id responseObject, NSError *error) {
                    if (responseObject && !error)
                    {
                        [arrCommentList removeObjectAtIndex:indexPath.row];
                        [tblComments reloadData];
                        
                        [[APIRequest request] updateActivityDataInLocal:self.strActivityType ActivityId:self.strActivityId UpdateData:[responseObject valueForKey:@"extra"]];
                    }
                }];
            }
        }];
    }
}

#pragma mark - ScrollView Method
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if ([scrollView isEqual:tblPeopleList])
    {
        [self updateScrollIndicator];
    }
}


#pragma mark - HPGrowingView Delegate
#pragma mark

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    txtCommentHeight.constant = height;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText =  [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    
    if (finalText.length > 0)
    {
        btnSend.enabled = [finalText isBlankValidationPassed];
        NSRange rgy = [finalText rangeOfString:@"@" options:NSBackwardsSearch];
        
        if (rgy.location == finalText.length-1) {
            
            filterdPeople = @[];//[aryPeoples copy];
            
        }
        else if (rgy.length > 0) {
            
            NSString *AfterAd = [finalText substringFromIndex:[finalText rangeOfString:@"@" options:NSBackwardsSearch].location+1];
            if (AfterAd.length > 0) {
                if ([[AfterAd substringToIndex:1] isEqualToString:@" "]) {
                     filterdPeople = @[];
                }
                else
                {
//                filterdPeople = [aryPeoples filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", AfterAd]];
                    filterdPeople = [aryPeoples filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(user_name contains[cd] %@)", AfterAd]];
                }
                /*
                 __block NSString *lastWord = nil;
                 
                 [AfterAd enumerateSubstringsInRange:NSMakeRange(0, [AfterAd length]) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange subrange, NSRange enclosingRange, BOOL *stop) {
                 lastWord = substring;
                 *stop = YES;
                 }];
                 
                 if (lastWord.length > 0 && lastWord.length < finalText.length) {
                 
                 NSString *find1 = [finalText substringWithRange:NSMakeRange([finalText rangeOfString:lastWord options:NSBackwardsSearch].location-1, 1)];
                 if([find1 isEqualToString:@"@"])
                 {
                 filterdPeople = [aryPeoples filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", lastWord]];
                 }
                 }
                 */
            }
            else
            {
                
            }
        }
        else
        {
            filterdPeople = @[];
        }
    }
    else
    {
        btnSend.enabled = false;
        filterdPeople = @[];
    }
    
    [self updateTablePeople];
    return true;
}

#pragma mark - Helper function
#pragma mark

-(void)updateTablePeople
{
    [tblPeopleList flashScrollIndicators];
    
    [tblPeopleList reloadData];
    tblpeopleHeight.constant = tblPeopleList.contentSize.height;
    [self.view setNeedsUpdateConstraints];
    [self updateScrollIndicator];
    [UIView animateWithDuration:0.25f animations:^{
       [tblPeopleList layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self updateScrollIndicator];
    }];
   
}

-(void)updateScrollIndicator
{
    if (tblPeopleList.contentSize.height != 0)
    {
        scrollIndheight.constant = (tblPeopleList.frame.size.height / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height ;
        scrollIndY.constant = (tblPeopleList.contentOffset.y / tblPeopleList.contentSize.height) * tblPeopleList.frame.size.height;
    }
}

#pragma mark - Action Event
#pragma mark
-(IBAction)btnSendClk:(id)sender
{
//    [txtVComent setText:@""];
    filterdPeople = @[];
    [self updateTablePeople];
    
    [appDelegate hideKeyboard];
    
    if ([txtVComent.text length] != 0)
        [self addCommentOnSession];

}

@end
