//
//  ChatViewController.m
//  TextToU
//
//  Created by mac-0009 on 7/25/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatTableViewCell.h"
#import "ChatUserTableViewCell.h"
#import "ChatImageCell.h"
#import "ChatSentImageCell.h"
#import "MessageDate.h"
#import "MSGSelectFriendsViewController.h"
#import "ActivityFeedViewController.h"

@interface ChatViewController ()
{
    FetchedResultsTableDataSource *fetchHome;
    NSNumber *msg_id,*offset;
    BOOL iskeepToindex,isViewVisible,isImageZoom;
    NSString* strChannel, *str;
    BOOL isShowingTypingIndicator;
    
}
@end

@implementation ChatViewController
@synthesize user_id;

#pragma mark - View Life Cycle
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [tblChat registerNib:[UINib nibWithNibName:@"ChatTableViewCell" bundle:nil] forCellReuseIdentifier:@"ChatTableViewCell"];
    [tblChat registerNib:[UINib nibWithNibName:@"ChatUserTableViewCell" bundle:nil] forCellReuseIdentifier:@"ChatUserTableViewCell"];
    [tblChat registerNib:[UINib nibWithNibName:@"ChatImageCell" bundle:nil] forCellReuseIdentifier:@"ChatImageCell"];
    [tblChat registerNib:[UINib nibWithNibName:@"ChatSentImageCell" bundle:nil] forCellReuseIdentifier:@"ChatSentImageCell"];
    [tblChat registerNib:[UINib nibWithNibName:@"MessageDate" bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:@"MessageDate"];

    tblChat.estimatedRowHeight = 100.0;
    tblChat.rowHeight = UITableViewAutomaticDimension;
    tblChat.transform = CGAffineTransformMakeRotation(-M_PI);

    
    isImageZoom = NO;
    isViewVisible = YES;
    
    [self initialize];
    
    appDelegate.configureGotMessage = ^(NSDictionary *dic)
    {
        if (isViewVisible)
        {
            NSLog(@"channel id is : %@", self.strChannelId);
            NSLog(@"dictionary is : %@", dic);
            [[APIRequest request] ReadMessage:self.strChannelId completed:nil];
        }
    };
    
    
    // To show typing indication if any one hase typing....
    appDelegate.configureShowTypingIndicator = ^(TblChats *objChat)
    {
        NSArray *arrParticipants = [TblParticipants fetch:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@", objChat, appDelegate.loginUser.user_id] orderBy:@"priority_index" ascending:YES];

        if (arrParticipants.count > 0)
        {
            NSArray *arrTypingUser = objChat.typingUser.mutableCopy;
            if (arrTypingUser.count > 0)
            {
                viewTypingIndicator.hidden = NO;
                if (arrParticipants.count > 1) // For group chatting...
                    lbTypingMessage.text = objChat.typingMessage;
                else
                    lbTypingMessage.text = @"typing...";
            }
            else
            {
                viewTypingIndicator.hidden = YES;
            }
        }
        
        
    };
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideShowStatusBar:NO];
    self.navigationController.navigationBarHidden = YES;
    
    [appDelegate hideTabBar];
    
    isShowingTypingIndicator = NO;
    
    if (isImageZoom)
    {
        isImageZoom = NO;
        return;
    }
    
    appDelegate.myConfig.uuid = [NSString stringWithFormat:@"%@_%@", CApplicationName, appDelegate.loginUser.user_id];
    
    // Load Channel history
    [self loadChannelHistory];

    
    // If Add new user in existing chat....
    __weak typeof(self) weakSelf = self;
    self.configureAddUserToGroup = ^(NSMutableArray *arrSelectedUsers)
    {
        [weakSelf addNewMemberInGroup:arrSelectedUsers];
    };
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    isViewVisible = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate SetPubNubClientTypingState:NO Channel:self.strChannelId];
    isShowingTypingIndicator = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Functions
#pragma mark -

-(void)initialize
{
    if (self.isFromMSGScreen)
    {
        NSArray *arr = [TblParticipants fetch:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@", self.chatObject, appDelegate.loginUser.user_id] orderBy:@"priority_index" ascending:YES];
        
        if(arr.count > 1)
        {
            
//Todo: Operations for the group chat(if new user is going to be added into existing group)
            
            NSString *strTitle = @"";

            TblParticipants *dic1 = arr[0];
            TblParticipants *dic2 = arr[1];

            if (arr.count > 2)
            {
                if(arr.count - 2 == 1)
                    strTitle = [NSString stringWithFormat:@"%@, %@ + 1 OTHER", dic1.user_name, dic2.user_name];
                else
                    strTitle = [NSString stringWithFormat:@"%@, %@ + %d OTHERS", dic1.user_name, dic2.user_name, (int)arr.count-2];
            }
            else
                strTitle = [NSString stringWithFormat:@"%@, %@", dic1.user_name, dic2.user_name];
            
            lbTopTitle.text = strTitle;
        }
        else
        {
            
//Todo: Operations for the one to one chat(if new user is going to be added into existing group)
            
            TblParticipants *objParticipant = arr[0];
            lbTopTitle.text = objParticipant.user_name;
        }
    }
    else
    {
        if(self.arrSelectedChat.count > 1)
        {
//Todo: Operations for the group chat(if a brand new group is going to be created)
            
            NSString *strTitle = @"";
            
            NSDictionary *dic1 = self.arrSelectedChat[0];
            NSDictionary *dic2 = self.arrSelectedChat[1];

            if (self.arrSelectedChat.count > 2)
            {
                if(self.arrSelectedChat.count - 2 == 1)
                    strTitle = [NSString stringWithFormat:@"%@, %@ + 1 OTHER", [dic1 stringValueForJSON:@"user_name"], [dic2 stringValueForJSON:@"user_name"]];
                else
                    strTitle = [NSString stringWithFormat:@"%@, %@ + %lu OTHERS", [dic1 stringValueForJSON:@"user_name"], [dic2 stringValueForJSON:@"user_name"], self.arrSelectedChat.count-2];
            }
            else
                strTitle = [NSString stringWithFormat:@"%@, %@", [dic1 stringValueForJSON:@"user_name"], [dic2 stringValueForJSON:@"user_name"]];
            
            lbTopTitle.text = strTitle;
        }
        else
        {
            
//Todo: Operations for the one to one chat(if brand new user is picked from the friend list for one to one chat)
            
            //            "_id" = 586cd6ac0387e7fc36c69e69;
            //            country = India;
            //            "fb_id" = "";
            //            "first_name" = Chintan;
            //            "last_name" = "";
            //            picture = "http://192.168.1.57:1337/user/avatar/586cd6ac0387e7fc36c69e69";
            //            "user_name" = Chintan;
            
            NSDictionary *dicUser = self.arrSelectedChat[0];
            lbTopTitle.text = [dicUser stringValueForJSON:@"user_name"];
        }
    }
    
    tblChat.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, CScreenWidth -10);
    txtVChat.placeholder = @"Type your message here...";
    txtVChat.font = CFontGilroyMedium(15.5);
    txtVChat.textColor = CRGB(156, 159, 179);
    txtVChat.maxNumberOfLines = 4;
    txtVChat.minNumberOfLines = 1;
    txtVChat.animateHeightChange = YES;
    txtVChat.delegate = self;
    
    if (self.strChannelId)
        [self setFetchController];
    else
    {
        [self registerChannelIfNotExists:^(NSString *strChannelId) {
        }];
    }
}

#pragma mark - Fetch Controller

- (void)setFetchController
{
    fetchHome = nil;
    __weak typeof(self) weakSelf = self;
    __weak typeof(tblChat) weakTblChat = tblChat;
    fetchHome = [self setFetchResultsController:tblChat entity:@"TblMessages" sortDesriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timestamp.doubleValue" ascending:false]] predicate:[NSPredicate predicateWithFormat:@"channelId == %@", self.strChannelId] section:@"getHeaderText" cellIdentifier:@"ChatUserTableViewCell" batchSize:20 cell:^(id cell, TblMessages *item, NSIndexPath *indexPath)
                 {
                     if ([indexPath isEqual:[weakTblChat lastIndexPath]])
                     {
                         [weakSelf loadHistoryWithLimit:weakSelf.numStartTimestamp endTimeStamp:nil];
                     }
                     
                     if ([item.msgtype isEqualToString:@"I"])
                     {
                         if ([item.userid isEqualToString:appDelegate.loginUser.user_id])
                         {
                             ChatSentImageCell *cell1 = (ChatSentImageCell *)cell;
                             cell1.lblTime.text = [weakSelf getTimeMessageDisplayTime:item.timestamp.intValue];
                             [cell1 setImageHeightWidth:item.imgwidth Height:item.imgheight];
                             [cell1.imgChat setImageWithURL:[NSURL URLWithString:item.imgurl] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                         }
                         else
                         {
                             ChatImageCell *cell1 = (ChatImageCell *)cell;
                             
                             [cell1.imgUser setImageWithURL:[appDelegate resizeImage:@"120" Height:nil imageURL:item.userimgurl] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                             
                             cell1.lblTime.text = [weakSelf getTimeMessageDisplayTime:item.timestamp.intValue];
                             [cell1 setImageHeightWidth:item.imgwidth Height:item.imgheight];
                             
                             [cell1.imgChat setImageWithURL:[NSURL URLWithString:item.imgurl] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                         }
                     }
                     else
                     {
                         if ([item.userid isEqualToString:appDelegate.loginUser.user_id])
                         {
                             ChatTableViewCell *cell1 = (ChatTableViewCell *)cell;
                             
                             NSString *strMessage = [item.msgtext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                             cell1.lblText.text = strMessage;
                             cell1.lblShowText.text = strMessage;
                             cell1.lblTime.text = [weakSelf getTimeMessageDisplayTime:item.timestamp.intValue];
                             
                             cell1.lblShowText.textColor = item.is_video.boolValue ? CRGB(71, 216, 253) : [UIColor blackColor];
                         }
                         else
                         {
                             ChatUserTableViewCell *cell1 = (ChatUserTableViewCell *)cell;
                             cell1.lblShowText.text = item.msgtext;
                             
                             [cell1.imgUser setImageWithURL:[appDelegate resizeImage:@"120" Height:nil imageURL:item.userimgurl] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

                             NSString *strMessage = [item.msgtext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                             cell1.lblShowText.text = cell1.lblText.text = strMessage;
                             cell1.lblTime.text = [weakSelf getTimeMessageDisplayTime:item.timestamp.intValue];
                             
                             cell1.lblShowText.textColor = item.is_video.boolValue ? CRGB(71, 216, 253) : [UIColor blackColor];
                         }
                     }
                 }];

#pragma mark - TableView DidSelectRowAtIndexPath
    [fetchHome setDidSelectRowAtIndexPath:^(ChatUserTableViewCell *cell, TblMessages *item, NSIndexPath *indexPath)
     {
         //      ProfileHeaderCell *cell = [tblChat cellForRowAtIndexPath:indexPath];
         if ([item.msgtype isEqualToString:@"I"])
         {
             isImageZoom = YES;
             ChatImageCell *cell1 = (ChatImageCell *)cell;
             JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
             imageInfo.placeholderImage = cell1.imgChat.image;
             imageInfo.image = cell1.imgChat.image;
             imageInfo.referenceRect = cell1.imgChat.frame;
             imageInfo.referenceView = cell1.imgChat.superview;
             imageInfo.referenceContentMode = cell1.imgChat.contentMode;
             imageInfo.referenceCornerRadius = cell1.imgChat.layer.cornerRadius;
             
             JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo mode:JTSImageViewControllerMode_Image backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
             [imageViewer showFromViewController:weakSelf transition:JTSImageViewControllerTransition_FromOriginalPosition];
         }
         else
         {
             if (item.is_video.boolValue)
             {
                 if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:item.msgtext]])
                 {
                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.msgtext] options:@{} completionHandler:nil];
                 }
             }
         }
     }];

    [fetchHome setIdentifierForRowAtIndexPath:^NSString*(TblMessages *item, NSIndexPath *indexPath)
    {
        if ([item.msgtype isEqualToString:@"I"])
        {
            if ([item.userid isEqualToString:appDelegate.loginUser.user_id])
                return @"ChatSentImageCell";
            return @"ChatImageCell";
        }
        else
        {
            if ([item.userid isEqualToString:appDelegate.loginUser.user_id])
                return @"ChatTableViewCell";
            return @"ChatUserTableViewCell";
        }
    }];
    
    [fetchHome setIdentifierForFooterInSection:^NSString*(id <NSFetchedResultsSectionInfo> info,NSInteger sectionIndex)
     {
         return @"MessageDate";
     }];
    
    [fetchHome setViewForFooterInSection:^(MessageDate *header, id <NSFetchedResultsSectionInfo> info, NSInteger sectionIndex)
     {
         NSLog(@"%@",info.name);
         
         header.lblDate.text = info.name;
     }];
    
    [fetchHome setHeightForFooterInSection:^CGFloat(id <NSFetchedResultsSectionInfo> info,NSInteger sectionIndex)
     {
         return 30;
     }];
    
    [fetchHome loadData];
}


#pragma mark - Save Message To Local
- (void)saveMessagesToLocal:(NSArray*)arr
{
    for (int i=0; i<arr.count; i++)
    {
        NSDictionary *dicMsg = arr[i];
        
        if (![dicMsg objectForKey:@"timestamp"])
            continue;
        
        double msgTimeStamp = [dicMsg stringValueForJSON:@"timestamp"].doubleValue;
        double msgDeleteTimeStamp = self.chatObject.msg_deleted_timestamp.doubleValue;
        
        if (msgTimeStamp <= msgDeleteTimeStamp)
            continue;
        
            
        TblMessages *objMsg = (TblMessages *)[TblMessages findOrCreate:@{@"timestamp": [dicMsg numberForJson:@"timestamp"]}];
        
        objMsg.timestamp = [dicMsg numberForJson:@"timestamp"];
        
        if ([dicMsg objectForKey:@"msgtext"])
            objMsg.msgtext = [dicMsg stringValueForJSON:@"msgtext"];
        
        if ([dicMsg objectForKey:@"msgdate"])
            objMsg.msgdate = [dicMsg stringValueForJSON:@"msgdate"];
        
        if ([dicMsg objectForKey:@"userid"])
            objMsg.userid =  [dicMsg stringValueForJSON:@"userid"];
        
        if ([dicMsg objectForKey:@"msgtype"])
            objMsg.msgtype = [dicMsg stringValueForJSON:@"msgtype"];
        
        if ([dicMsg objectForKey:@"imgname"])
            objMsg.imgname = [dicMsg stringValueForJSON:@"imgname"];
        
        if ([dicMsg objectForKey:@"imgurl"])
            objMsg.imgurl = [NSString stringWithFormat:@"%@%@",BASEURL,[dicMsg stringValueForJSON:@"imgurl"]];
        
        if ([dicMsg objectForKey:@"imgheight"])
            objMsg.imgheight = [dicMsg stringValueForJSON:@"imgheight"];

        if ([dicMsg objectForKey:@"imgwidth"])
            objMsg.imgwidth = [dicMsg stringValueForJSON:@"imgwidth"];

        if ([dicMsg objectForKey:@"msgdate"])
            objMsg.msgdate = [dicMsg stringValueForJSON:@"msgdate"];
        
        if ([dicMsg objectForKey:@"channelId"])
            objMsg.channelId = [dicMsg stringValueForJSON:@"channelId"];
        
        if ([dicMsg objectForKey:@"userimgurl"])
            objMsg.userimgurl = [dicMsg stringValueForJSON:@"userimgurl"];
        
        if ([dicMsg objectForKey:@"is_video"])
            objMsg.is_video = [dicMsg stringValueForJSON:@"is_video"].intValue == 0 ? @NO : @YES;
    }
    
    [[[Store sharedInstance] mainManagedObjectContext] save];
}

#pragma mark - APi Functions

-(void)loadChannelHistory
{
    if (self.strChannelId)
    {
        NSArray *arrReadChat = [TblChats fetch:[NSPredicate predicateWithFormat:@"channelid == %@",self.strChannelId] sortedBy:nil];
        if (arrReadChat.count > 0) {
            TblChats *objReadChat = arrReadChat[0];
            objReadChat.isReadMessage = @YES;
            [[Store sharedInstance].mainManagedObjectContext save];
            [appDelegate showMessageBadgeOnTabBar];
        }
        
        [[APIRequest request] ReadMessage:self.strChannelId completed:nil];
        
        [appDelegate.client subscribeToChannels: @[self.strChannelId] withPresence:YES];
        [self AddChannelForPubnubPushNotificaiton];
        
        [self loadHistoryWithLimit:nil endTimeStamp:nil];
    }
}

-(void)addNewMemberInGroup:(NSArray *)arrMem
{
    NSMutableArray *arrIds = [NSMutableArray new];
    
    for (int i=0; i<arrMem.count; i++)
    {
        NSDictionary *dicUser = arrMem[i];
        [arrIds addObject:[dicUser stringValueForJSON:@"_id"]];
    }
    
    [[APIRequest request] ChatMember:self.strChannelId UserId:arrIds completed:^(id responseObject, NSError *error)
     {
         NSArray *arrData = [responseObject valueForKey:CJsonData];
         
         if (arrData.count>0)
         {
             NSDictionary *dicData = arrData[0];
             self.strChannelId = [dicData stringValueForJSON:@"_id"];
             
             NSArray *arr = [TblChats fetch:[NSPredicate predicateWithFormat:@"channelid == %@", self.strChannelId] orderBy:nil ascending:YES];
             
             if (arr.count > 0)
                 self.chatObject = arr[0];
             
             [self loadChannelHistory];
             [self initialize];
         }
     }];

}


- (void)registerChannelIfNotExists:(void(^)(NSString *strChannelId))completion
{
    NSMutableArray *arrIds = [[NSMutableArray alloc] init];
    
    if(self.arrSelectedChat.count > 1)
    {
//Todo: Create the group chat on PubNub
        for (int i=0; i<self.arrSelectedChat.count; i++)
        {
            NSDictionary *dicUser = self.arrSelectedChat[i];
            [arrIds addObject:[dicUser stringValueForJSON:@"_id"]];
        }
    }
    else
    {
//Todo: Create the one-to-one chat on PubNub
        if (self.arrSelectedChat.count > 0)
        {
            NSDictionary *dicUser = self.arrSelectedChat[0];
            [arrIds addObject:[dicUser stringValueForJSON:@"_id"]];
        }
    }
    
    [[APIRequest request] RegisterChannel:@{@"user_ids":arrIds} completed:^(id responseObject, NSError *error)
    {
        
//Todo: Get the channed id from the response, subscribe and publish message to PubNub
        
        if([responseObject objectForKey:CJsonData] && [[responseObject objectForKey:CJsonData] isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dicData = [responseObject objectForKey:CJsonData];
            if([dicData objectForKey:@"channel_id"])
            {
                self.strChannelId = [dicData stringValueForJSON:@"channel_id"];
                [[APIRequest request] ChannelDetail:self.strChannelId completed:^(id responseObject, NSError *error)
                {
                    NSArray *arr = [TblChats fetch:[NSPredicate predicateWithFormat:@"channelid == %@", self.strChannelId] orderBy:nil ascending:YES];
                    
                    if (arr.count > 0)
                        self.chatObject = arr[0];
                    
                    [self setFetchController];
                    
                    if (completion)
                        completion(self.strChannelId);
                }];
                
                [appDelegate.client subscribeToChannels: @[self.strChannelId] withPresence:YES];
                [self AddChannelForPubnubPushNotificaiton];
                
            }
        }
    }];
}

- (void)uploadChatImage:(UIImage*)image completed:(void(^)(id responseObject, NSError *error))completion
{
    NSLog(@"CALL IMAGE UPLOAD API HERE ..........");
    [[APIRequest request] UploadChatImage:image completed:^(id response1, NSError *error1)
     {
         if (completion)
             completion(response1, error1);
     }];
}

#pragma mark - PUBNUB Functions

- (void)publishMessage:(NSString *)strMessage strChannelId:(NSString*)strChannelId imgUrl:(NSString*)imgUrl imgName:(NSString*)imgName msgType:(NSString *)msgType imgHeight:(NSString *)imgheight imgWidth:(NSString *)imgwidth
{
    NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:@"dd MMMM, yyyy"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *strToday = [dateFormatter stringFromDate:[NSDate date]];
    
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *strTodayTime = [dateFormatter stringFromDate:[NSDate date]];
    
    NSNumber *iCurrentTimestamp = [NSNumber numberWithInteger:[NSDate currentTimestampInteger]];
    
    NSDictionary *dicMessage = @{
                                 @"msgtext": strMessage,
                                 @"channelId": strChannelId,
                                 @"userid":appDelegate.loginUser.user_id,
                                 @"userimgurl":appDelegate.loginUser.picture,
                                 @"msgdate":strToday,
                                 @"timestamp": iCurrentTimestamp,
                                 @"imgurl":imgUrl,
                                 @"imgheight":imgheight,
                                 @"imgwidth":imgwidth,
                                 @"imgname":imgName,
                                 @"msgtype":msgType,
                                 @"is_video":@"0",
                                 @"uuid":appDelegate.myConfig.uuid,
                                 @"user_name":appDelegate.loginUser.user_name,
                                 @"msgdisplaytime":strTodayTime
                                };
    
    NSDictionary *payloads = @{@"pn_apns":
                                   @{
                                       @"aps" :
                                           @{
                                               @"alert": [NSString stringWithFormat:@"%@ messaged you", appDelegate.loginUser.user_name],
                                               @"badge": @1,
                                               @"channel_id":strChannelId,
                                               @"user_id":appDelegate.loginUser.user_id,
                                               @"type":CNotificationTypePubNub
                                               }
                                       },
                               @"pn_debug": @"true"};
    
    [appDelegate.client publish:dicMessage toChannel:strChannelId mobilePushPayload:payloads storeInHistory:YES withCompletion:^(PNPublishStatus * _Nonnull status)
    {
        if (!status.isError)
        {
            NSLog(@"Message sent at TT: %@", status.data.timetoken);
            
//TODO: Use the timetoken(timestamp) for the further information - Message sent successfully
            
            TblMessages *objMsg = (TblMessages *)[TblMessages findOrCreate:@{@"timestamp": iCurrentTimestamp}];
            objMsg.timestamp = iCurrentTimestamp;
            objMsg.msgtext = strMessage;
            objMsg.msgdate = strToday;
            objMsg.userid = appDelegate.loginUser.user_id;
            objMsg.msgtype = msgType;
            objMsg.imgname = imgName;
            objMsg.msgtype = msgType;
            objMsg.imgheight = imgheight;
            objMsg.imgwidth = imgwidth;
            objMsg.is_video = @NO;
            objMsg.imgurl = [NSString stringWithFormat:@"%@%@",BASEURL,imgUrl];
            objMsg.msgdate = strToday;
            objMsg.channelId = strChannelId;
            objMsg.userimgurl = appDelegate.loginUser.picture;

            [[[Store sharedInstance] mainManagedObjectContext] save];
            
      //      NSLog(@"Fetched objects are:: %@", [TblMessages fetch:[NSPredicate predicateWithFormat:@"channelId == %@", strChannelId] orderBy:nil ascending:YES]);
            
            [[APIRequest request] MessageSend:self.strChannelId completed:^(id responseObject, NSError *error)
            {
                [[APIRequest request] ReadMessage:strChannelId completed:^(id responseObject, NSError *error)
                {
                    NSLog(@"READ MESAAGE SUCCEFULLY ============= >>> ");
                }];
            }];
        }
        else
        {
            [appDelegate handleStatus:status];
        }
    }];
}

- (void)loadHistoryWithLimit:(NSNumber *)startTimeStamp endTimeStamp:(NSNumber *)endTimeStamp
{
//    endTimeStamp = [NSNumber numberWithInteger:[NSDate currentTimestampInteger]];
    [appDelegate.client historyForChannel:self.strChannelId start:startTimeStamp end:nil limit:50 withCompletion:^(PNHistoryResult *result, PNErrorStatus *status)
     {
         if (!status)
         {
             /**
              Handle downloaded history using:
              result.data.start - oldest message time stamp in response
              result.data.end - newest message time stamp in response
              result.data.messages - list of messages
              */
             
             self.numStartTimestamp = result.data.end;
             [self saveMessagesToLocal:result.data.messages];
         }
         else
         {
             /**
              Handle message history download error. Check 'category' property
              to find out possible reason because of which request did fail.
              Review 'errorData' property (which has PNErrorData data type) of status
              object to get additional information about issue.
              
              Request can be resent using: [status retry];
              */
         }
     }];
}


-(void)AddChannelForPubnubPushNotificaiton
{
    if(![CUserDefaults objectForKey:CDeviceTokenPubNub])
        return;
    
    [appDelegate addPushNotificationToPubnubChannels:@[self.strChannelId]];
}

#pragma mark - General Functions
- (NSString *)getTimeMessageDisplayTime:(int)timestamp
{
    NSTimeInterval interval = timestamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"hh:mm a"];
    return [formatter stringFromDate:date];
}

#pragma mark - Growing textview delegate methods
#pragma mark -

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    txtChatHeight.constant = height > 37 ? height : 37;
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *finalText =  [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
    btnSend.enabled = (finalText.length > 0);
    
    if (finalText.length > 0 )
    {
        // Show Typing Indicator
        if (!isShowingTypingIndicator)
        {
            isShowingTypingIndicator = YES;
            [appDelegate SetPubNubClientTypingState:YES Channel:self.strChannelId];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [appDelegate SetPubNubClientTypingState:NO Channel:self.strChannelId];
                isShowingTypingIndicator = NO;
            });
        }
    }
    else
    {
        // Hide Typing Indicator
        [appDelegate SetPubNubClientTypingState:NO Channel:self.strChannelId];
        isShowingTypingIndicator = NO;
    }
    
    return true;
}


#pragma mark - Action Event
#pragma mark -


- (IBAction)btnSendClicked:(id)sender
{
//    [appDelegate hideKeyboard];
    
    if ([txtVChat.text isBlankValidationPassed])
    {
        [appDelegate SetPubNubClientTypingState:NO Channel:self.strChannelId];
        isShowingTypingIndicator = NO;
        
        NSString *strMessage = txtVChat.text;
        strMessage = [strMessage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        txtVChat.text = @"";
        btnSend.enabled = NO;
        
        //Todo: Call api to get the channel id to create the channel
        
        if (self.strChannelId)
        {
            [self publishMessage:strMessage strChannelId:self.strChannelId imgUrl:@"" imgName:@"" msgType:@"T" imgHeight:@"" imgWidth:@""];
        }
        else
        {
            [self registerChannelIfNotExists:^(NSString *strChannelId) {
                
                [self publishMessage:strMessage strChannelId:strChannelId imgUrl:@"" imgName:@"" msgType:@"T" imgHeight:@"" imgWidth:@""];
                
            }];
        }
    }
}

- (IBAction)btnImageClicked:(id)sender
{
    if(IS_IPHONE_SIMULATOR)
    {
        if(self.strChannelId)
        {
            [self uploadChatImage:[UIImage imageNamed:@"5.jpg"] completed:^(id responseObject, NSError *error) {
                
                if ([responseObject objectForKey:CJsonData])
                {
                    NSDictionary *dicData = [responseObject objectForKey:CJsonData];
                    
                    if ([dicData objectForKey:@"image"])
                    {
                        NSString *strImageUrl = [dicData stringValueForJSON:@"image"];
                        NSString *strImageHeight = [dicData stringValueForJSON:@"height"];
                        NSString *strImageWidth = [dicData stringValueForJSON:@"width"];
                        
                        [self publishMessage:@"" strChannelId:self.strChannelId imgUrl:strImageUrl imgName:@"image" msgType:@"I" imgHeight:strImageHeight imgWidth:strImageWidth];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self setFetchController];
                        });
                        
                    }
                }
            }];
        }
        else
        {
            [self registerChannelIfNotExists:^(NSString *strChannelId) {
                
                [self uploadChatImage:[UIImage imageNamed:@"5.jpg"] completed:^(id responseObject, NSError *error) {
                    
                    if ([responseObject objectForKey:CJsonData])
                    {
                        NSDictionary *dicData = [responseObject objectForKey:CJsonData];
                        
                        if ([dicData objectForKey:@"image"])
                        {
                            NSString *strImageUrl = [dicData stringValueForJSON:@"image"];
                            NSString *strImageHeight = [dicData stringValueForJSON:@"height"];
                            NSString *strImageWidth = [dicData stringValueForJSON:@"width"];

                            [self publishMessage:@"" strChannelId:self.strChannelId imgUrl:strImageUrl imgName:@"image" msgType:@"I" imgHeight:strImageHeight imgWidth:strImageWidth];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [self setFetchController];
                            });
                            
                        }
                    }
                }];
            }];
        }
        
        return;
    }
    
    [self presentImagePickerSource:^(UIImage *image) {
        if (image)
        {
            if (image)
            {
                //Todo: Call upload image on the server api and getting the response call publish method
                
                if(self.strChannelId)
                {
                    [self uploadChatImage:image completed:^(id responseObject, NSError *error) {
                        
                        if ([responseObject objectForKey:CJsonData])
                        {
                            NSDictionary *dicData = [responseObject objectForKey:CJsonData];
                            
                            if ([dicData objectForKey:@"image"])
                            {
                                NSString *strImageUrl = [dicData stringValueForJSON:@"image"];
                                NSString *strImageHeight = [dicData stringValueForJSON:@"height"];
                                NSString *strImageWidth = [dicData stringValueForJSON:@"width"];
                                
                                [self publishMessage:@"" strChannelId:self.strChannelId imgUrl:strImageUrl imgName:@"image" msgType:@"I" imgHeight:strImageHeight imgWidth:strImageWidth];
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    [self setFetchController];
                                });
                                
                            }
                        }
                    }];
                }
                else
                {
                    [self registerChannelIfNotExists:^(NSString *strChannelId)
                     {
                         
                         [self uploadChatImage:image completed:^(id responseObject, NSError *error) {
                             
                             if ([responseObject objectForKey:CJsonData])
                             {
                                 NSDictionary *dicData = [responseObject objectForKey:CJsonData];
                                 
                                 if ([dicData objectForKey:@"image"])
                                 {
                                     NSString *strImageUrl = [dicData stringValueForJSON:@"image"];
                                     NSString *strImageHeight = [dicData stringValueForJSON:@"height"];
                                     NSString *strImageWidth = [dicData stringValueForJSON:@"width"];
                                     
                                     [self publishMessage:@"" strChannelId:self.strChannelId imgUrl:strImageUrl imgName:@"image" msgType:@"I" imgHeight:strImageHeight imgWidth:strImageWidth];
                                     
                                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                         [self setFetchController];
                                     });
                                     
                                 }
                             }
                         }];
                     }];
                }
            }
        }
    } AllowEditing:YES];
  
}

- (IBAction)btnAddFreindCLK:(id)sender
{
    MSGSelectFriendsViewController *objSel = [[MSGSelectFriendsViewController alloc] init];
    objSel.arrAlreadyAdded = [TblParticipants fetch:[NSPredicate predicateWithFormat:@"chatobject == %@ && user_id != %@", self.chatObject, appDelegate.loginUser.user_id] orderBy:@"priority_index" ascending:YES];
    [self.navigationController pushViewController:objSel animated:YES];
}

-(IBAction)btnBackCLK:(id)sender
{
    if (self.isFromMSGScreen)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        for (int i =0; [self.navigationController viewControllers].count > i; i++)
        {
            UIViewController *viewController = [self.navigationController viewControllers][i];
            if ([viewController isKindOfClass:[ActivityFeedViewController class]])
            {
                ActivityFeedViewController *obj = (ActivityFeedViewController *)viewController;
                [self.navigationController popToViewController:obj animated:YES];
                break;
            }
        }
    }
}

@end
