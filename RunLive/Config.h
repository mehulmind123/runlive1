
// # MI-r6
// All the configuration setting will be saved over here to have the stuff related to app mode(production/development)

#define CProductionMode 1

#if CProductionMode

//Live
#define BASEURL @"http://api.runlive.us:1337/v1/"


#else
    
//Mind Live
#define BASEURL @"http://192.168.1.23:1337/v1/"

#endif

