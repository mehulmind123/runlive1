//
//  MonthCell.h
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lblMonth;
@property(weak,nonatomic) IBOutlet UILabel *lblMonthName;

@end
