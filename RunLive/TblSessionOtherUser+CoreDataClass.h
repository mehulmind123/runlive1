//
//  TblSessionOtherUser+CoreDataClass.h
//  RunLive
//
//  Created by mac-00012 on 1/2/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TblSessionList;

NS_ASSUME_NONNULL_BEGIN

@interface TblSessionOtherUser : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblSessionOtherUser+CoreDataProperties.h"
