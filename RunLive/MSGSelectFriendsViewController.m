//
//  MSGSelectFriendsViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/4/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "MSGSelectFriendsViewController.h"
#import "InviteSearchCell.h"
#import "ChatViewController.h"


@interface MSGSelectFriendsViewController ()

@end

@implementation MSGSelectFriendsViewController
{
    NSMutableArray *arrSelectedUser,*arrUser;
    NSURLSessionDataTask *taskRuning;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [tblUser registerNib:[UINib nibWithNibName:@"InviteSearchCell" bundle:nil] forCellReuseIdentifier:@"InviteSearchCell"];
    
    [txtSearch setValue:CRGB(167, 170, 175) forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtSearch addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    arrSelectedUser = [NSMutableArray new];
    arrUser = [NSMutableArray new];
    btnDone.hidden = YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];
    [arrSelectedUser removeAllObjects];
    [tblUser reloadData];
    [self getFriendListFromServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Textfield Methods
-(void)textfieldDidChange:(UITextField *)txt
{
    if (txtSearch.text.length == 0)
        [txtSearch resignFirstResponder];
    
    [self getFriendListFromServer];
}


#pragma mark - API Function
-(void)getFriendListFromServer
{
    
    if (taskRuning && taskRuning.state == NSURLSessionTaskStateRunning)
        [taskRuning cancel];
    
    taskRuning = [[APIRequest request] friendList:txtSearch.text loader:NO completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             [arrUser removeAllObjects];
             [tblUser reloadData];

             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"] isKindOfClass:[NSArray class]])
                 [arrUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"]];

             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"other"] isKindOfClass:[NSArray class]])
                 [arrUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"other"]];

//Todo: Remove users from the array which have been already added to the group
             
             NSArray *arrCopy = [arrUser mutableCopy];
             
             for (int i=0; i<arrCopy.count; i++)
             {
                 NSDictionary *dicObject = arrCopy[i];
                 
                 NSArray *arrFilter = [self.arrAlreadyAdded filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"user_id == %@", [dicObject stringValueForJSON:@"_id"]]];
                 
                 if(arrFilter.count <= 0)
                     continue;
                 
                 [arrUser removeObject:[arrCopy objectAtIndex:i]];
             }
             
             [tblUser reloadData];
         }
     }];
}


#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrUser.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"InviteSearchCell";
    InviteSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
        cell = [[InviteSearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSDictionary *dicData = [arrUser objectAtIndex:indexPath.item];
    [cell.imgFreind setImageWithURL:[appDelegate resizeImage:@"100" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    
    NSString *strFirstname = [[dicData stringValueForJSON:@"first_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (!strFirstname || [strFirstname isEqualToString:@""])
        cell.lblUserName.text = [NSString stringWithFormat:@"%@", [dicData stringValueForJSON:@"user_name"]];
    else
        cell.lblUserName.text = [NSString stringWithFormat:@"%@ %@", strFirstname,[dicData stringValueForJSON:@"last_name"]];
    
    cell.viewSelect.hidden = ![arrSelectedUser containsObject:dicData];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    InviteSearchCell *cell = [tblUser cellForRowAtIndexPath:indexPath];
    
    NSDictionary *dicData = [arrUser objectAtIndex:indexPath.item];
    
    if ([arrSelectedUser containsObject:dicData])
    {
        [arrSelectedUser removeObject:dicData];
        cell.viewSelect.hidden = YES;
    }
    else
    {
        [arrSelectedUser addObject:dicData];
        cell.viewSelect.hidden = NO;
    }
    
    btnDone.hidden = arrSelectedUser.count == 0;
}

#pragma mark - Action Event

-(IBAction)btnDoneCLK:(id)sender
{
    if ([[self navigationController] viewControllers].count>=2)
    {
        if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] isKindOfClass:[ChatViewController class]])
        {
            ChatViewController *objChat = (ChatViewController *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
            
            if (objChat.configureAddUserToGroup)
                objChat.configureAddUserToGroup(arrSelectedUser);
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            ChatViewController *obj = [[ChatViewController alloc] init];
            obj.isFromMSGScreen = NO;
            obj.arrSelectedChat = [NSArray arrayWithArray:arrSelectedUser];
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
    else
    {
        ChatViewController *obj = [[ChatViewController alloc] init];
        obj.isFromMSGScreen = NO;
        obj.strChannelId = @"";
        [self.navigationController pushViewController:obj animated:YES];
    }
}


@end
