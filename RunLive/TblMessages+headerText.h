//
//  TblMessages+headerText.h
//  RunLive
//
//  Created by mac-0004 on 03/11/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "TblMessages+CoreDataClass.h"

@interface TblMessages (headerText)
-(NSString*)getHeaderText;
@end
