//
//  UILabel+TTTAttributedLabel.h
//  Journey
//
//  Created by mac-0001 on 12/06/15.
//  Copyright (c) 2015 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (TTTAttributedLabel)<TTTAttributedLabelDelegate>

//-(void)addHashTagAndUserHandler:(void(^)(NSString *urlString))block;

-(void)addHashTagAndUserHandler:(NSString *)strUserName Complete:(void(^)(NSString *urlString))block;



@end
