//
//  GPSPermissionView.m
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "GPSPermissionView.h"

@implementation GPSPermissionView

+ (id)initGPSPermissionView
{
    GPSPermissionView *objGPSView = [[[NSBundle mainBundle]loadNibNamed:@"GPSPermissionView" owner:nil options:nil] lastObject];
    CViewSetWidth(objGPSView, CScreenWidth);
    CViewSetHeight(objGPSView, CScreenHeight);
    return objGPSView;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self Initialization];
}

#pragma mark - Configuration

-(void)Initialization
{
    _btnGPSNotNow.layer.cornerRadius = _btnGPSGoForIt.layer.cornerRadius = 3;
}

@end
