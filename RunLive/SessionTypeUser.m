//
//  SessionTypeUser.m
//  RunLive
//
//  Created by mac-00012 on 11/19/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionTypeUser.h"

@implementation SessionTypeUser

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.btnGoBack.layer.cornerRadius = self.btnCancel.layer.cornerRadius = 3;
    self.center = CScreenCenter;
}


@end
