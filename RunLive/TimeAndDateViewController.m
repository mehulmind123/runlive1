//
//  TimeAndDateViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "TimeAndDateViewController.h"
#import "DayCell.h"
#import "MonthCell.h"

@interface TimeAndDateViewController ()

@end

@implementation TimeAndDateViewController
{
    NSIndexPath *selectedDayIndexPath,*selectedMonthIndexPath;
    
    NSMutableArray *arrDay,*arrMonth;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [tblMonth registerNib:[UINib nibWithNibName:@"DayCell" bundle:nil] forCellReuseIdentifier:@"DayCell"];
    [tblDay registerNib:[UINib nibWithNibName:@"MonthCell" bundle:nil] forCellReuseIdentifier:@"MonthCell"];
    
    arrDay = [NSMutableArray new];
    arrMonth = [NSMutableArray new];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isTime)
    {
        
        NSLog(@"%@",self.strSelectedTime);
        
        NSArray *arrSelectTime = [self.strSelectedTime componentsSeparatedByString:@":"];
        NSString *strHour = nil;
        NSString *strMint = nil;
        
        if (arrSelectTime.count > 1)
        {
            strHour = arrSelectTime[0];
            strMint = arrSelectTime[1];
        }
        
        
        lblTitleDay.text = @"M";
        lblTitleMonth.text = @"H";
        btnAM.hidden = btnPM.hidden = lblAM.hidden = lblPM.hidden = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self btnAMPMCLK:btnAM];
        });
     
        [arrMonth addObjectsFromArray:@[@{@"name":@"",@"day":@"01"},@{@"name":@"",@"day":@"02"},@{@"name":@"",@"day":@"03"},@{@"name":@"",@"day":@"04"},@{@"name":@"",@"day":@"05"},@{@"name":@"",@"day":@"06"},@{@"":@"",@"day":@"07"},@{@"name":@"",@"day":@"08"},@{@"name":@"",@"day":@"09"},@{@"name":@"",@"day":@"10"},@{@"name":@"",@"day":@"11"},@{@"name":@"",@"day":@"12"},]];

        for (int i = 0; i<60; i++)
        {
            NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
            
            [dicData addObject:@"" forKey:@"name"];
            [dicData addObject:i < 10 ? [NSString stringWithFormat:@"0%d",i] : [NSString stringWithFormat:@"%d",i] forKey:@"day"];
            [arrDay addObject:dicData];
        }
        
        if (strHour)
        {
            int index = strHour.intValue;
            selectedMonthIndexPath = [NSIndexPath indexPathForRow:index-1 inSection:0];
            [tblMonth scrollToRowAtIndexPath:selectedMonthIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
        }

        if (strMint)
        {
            int index = strMint.intValue;
            selectedDayIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [tblDay scrollToRowAtIndexPath:selectedDayIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
        }

        [tblMonth reloadData];
        [tblDay reloadData];
    }
    else
    {
        lblTitleDay.text = @"DAY";
        lblTitleMonth.text = @"MONTH";
        btnAM.hidden = btnPM.hidden = lblAM.hidden = lblPM.hidden = YES;
        
        [self getAllMonthOfYear];
    }
}

#pragma mark - Get All Month

-(void)GetAllDayOfMonth:(NSDictionary *)dicData
{
    [arrDay removeAllObjects];
    selectedDayIndexPath = nil;
    
    selectedDayIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components=[calendar components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay fromDate:[NSDate date]];
    
    [components setYear: [[NSString stringWithFormat:@"%@", [dicData objectForKey:@"year"]] integerValue]];
    [components setMonth:[[NSString stringWithFormat:@"%@", [dicData objectForKey:@"day"]] integerValue]];
    
    NSDate *date = [calendar dateFromComponents:components];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    
    NSLog(@"%d", (int)range.length);
    
    for (int day = 1; day <= (int)range.length; day++)
    {
        NSString *strDate = [NSString stringWithFormat:@"%@%02ld%02d",[dicData valueForKey:@"year"] ,[[dicData valueForKey:@"day"] integerValue],day];
        
        NSDateFormatter *df=[[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyyMMdd"];
        NSDate *targetDate=[df dateFromString:strDate];
        [df setDateFormat:@"EEEE"];
        NSString *sDAY=[df stringFromDate:targetDate];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
        
        NSDate *date1 = [NSDate date];
        NSDate *date2 = targetDate;
        
        NSDateComponents *date1Components = [calendar components:comps fromDate: date1];
        NSDateComponents *date2Components = [calendar components:comps fromDate: date2];
        
        date1 = [calendar dateFromComponents:date1Components];
        date2 = [calendar dateFromComponents:date2Components];
        NSComparisonResult result = [date1 compare:date2];
        
        if (result != NSOrderedDescending)
        {
            NSLog(@"Add dates in array");
            NSMutableDictionary *dicDay = [NSMutableDictionary new];
            [dicDay setObject:[NSString stringWithFormat:@"%d",day] forKey:@"day"];
            [dicDay setObject:sDAY forKey:@"name"];
            [arrDay addObject:dicDay];
        }
    }
    
    [tblDay reloadData];
}

-(void)getAllMonthOfYear
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    
    // To set Month and year to Components
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    
    selectedMonthIndexPath = [NSIndexPath indexPathForRow:month-1 inSection:0];
    
    // To get next year's month
    for (int i = 1 ; i < month; i++)
    {
        NSInteger nextYear = [components year]+1;
        NSMutableDictionary *dicMonth = [[NSMutableDictionary alloc] init];
        [dicMonth setObject:[[[dateFormatter monthSymbols]objectAtIndex: i-1] capitalizedString] forKey:@"name"];
        [dicMonth setObject:[NSString stringWithFormat:@"%d",i] forKey:@"day"];
        [dicMonth setObject:[NSString stringWithFormat:@"%d",(int)nextYear] forKey:@"year"];
        [arrMonth addObject:dicMonth];
    }
    
    // To get current year's month
    for(int months = (int)month; months <= 12; months++)
    {
        NSMutableDictionary *dicMonth = [[NSMutableDictionary alloc] init];
        [dicMonth setObject:[[[dateFormatter monthSymbols]objectAtIndex: months-1] capitalizedString] forKey:@"name"];
        [dicMonth setObject:[NSString stringWithFormat:@"%d",months] forKey:@"day"];
        [dicMonth setObject:[NSString stringWithFormat:@"%d",(int)year] forKey:@"year"];
        
        if (![arrMonth containsObject:dicMonth])
            [arrMonth addObject:dicMonth];
    }
    
    // Get All day's from the Selected Month
    [self GetAllDayOfMonth:arrMonth[selectedMonthIndexPath.row]];
    
    [tblMonth reloadData];
}


#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:tblDay])
        return arrDay.count;

    return arrMonth.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblMonth])
    {
        NSString *identifier = @"DayCell";
        DayCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil)
            cell = [[DayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrMonth objectAtIndex:indexPath.row];
        cell.lblName.text = [dicData stringValueForJSON:@"name"];
        cell.lblDay.text = [dicData stringValueForJSON:@"day"];
        
        cell.lblName.hidden = self.isTime;
        
        if ([indexPath isEqual:selectedMonthIndexPath])
        {
            cell.lblDay.backgroundColor = CRGB(70, 216, 252);
            cell.lblDay.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.lblDay.backgroundColor = [UIColor whiteColor];
            cell.lblDay.textColor = [UIColor blackColor];
        }
        
        return cell;
    }
    else
    {
        NSString *identifier = @"MonthCell";
        MonthCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil)
            cell = [[MonthCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dicData = [arrDay objectAtIndex:indexPath.row];
        cell.lblMonthName.text = [dicData stringValueForJSON:@"name"];
        cell.lblMonth.text = [dicData stringValueForJSON:@"day"];

        
        cell.lblMonthName.hidden = self.isTime;
        
        if ([indexPath isEqual:selectedDayIndexPath])
        {
            cell.lblMonth.backgroundColor = CRGB(70, 216, 252);
            cell.lblMonth.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.lblMonth.backgroundColor = [UIColor whiteColor];
            cell.lblMonth.textColor = [UIColor blackColor];
        }
        
        return cell;
   }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tblDay])
    {
        if (selectedDayIndexPath)
        {
            MonthCell *cell = [tblDay cellForRowAtIndexPath:selectedDayIndexPath];
            cell.lblMonth.backgroundColor = [UIColor whiteColor];
            cell.lblMonth.textColor = [UIColor blackColor];
        }
        
        MonthCell *cell = [tblDay cellForRowAtIndexPath:indexPath];
        cell.lblMonth.backgroundColor = CRGB(70, 216, 252);
        cell.lblMonth.textColor = [UIColor whiteColor];
        selectedDayIndexPath = indexPath;
    }
    else
    {
        if (selectedMonthIndexPath)
        {
            DayCell *cell = [tblMonth cellForRowAtIndexPath:selectedMonthIndexPath];
            cell.lblDay.backgroundColor = [UIColor whiteColor];
            cell.lblDay.textColor = [UIColor blackColor];
            
        }
        
        DayCell *cell = [tblMonth cellForRowAtIndexPath:indexPath];
        cell.lblDay.backgroundColor = CRGB(70, 216, 252);
        cell.lblDay.textColor = [UIColor whiteColor];
        
        selectedMonthIndexPath = indexPath;
        
        if (self.isTime)
        {
            //
        }
        else
        {
            [self GetAllDayOfMonth:arrMonth[indexPath.row]];
        }

    }
}

#pragma mark - Aciton Event

-(IBAction)btnAMPMCLK:(UIButton *)sender
{
    btnAM.selected = btnPM.selected = NO;
    
    btnAM.layer.cornerRadius = btnPM.layer.cornerRadius = CGRectGetHeight(btnAM.frame)/2;
    btnAM.layer.borderWidth = btnPM.layer.borderWidth = 0;
    btnAM.layer.borderColor = btnAM.layer.borderColor = [UIColor clearColor].CGColor;
    lblAM.textColor = lblPM.textColor = [UIColor whiteColor];
    
    switch (sender.tag)
    {
        case 0:
        {
            btnAM.selected = YES;
            btnAM.layer.borderColor = [UIColor whiteColor].CGColor;
            btnAM.layer.borderWidth = 1;
            lblAM.textColor = [UIColor blackColor];
        }
            break;
        case 1:
        {
            btnPM.selected = YES;
            btnPM.layer.borderColor = [UIColor whiteColor].CGColor;
            btnPM.layer.borderWidth = 1;
            lblPM.textColor = [UIColor blackColor];
        }
            break;
        default:
            break;
    }
}

-(IBAction)btnOKCancelCLK:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
        case 1:
        {
            // Select Data here.....
            
            if (!selectedMonthIndexPath)
            {
                [self customAlertViewWithOneButton:@"" Message: self.isTime ? @"Please select hour." : @"Please select day." ButtonText:@"OK" Animation:YES completed:nil];
                return;
            }
            
            if (!selectedDayIndexPath)
            {
                [self customAlertViewWithOneButton:@"" Message: self.isTime ? @"Please select minute." : @"Please select month." ButtonText:@"OK" Animation:YES completed:nil];
                return;
            }
            
            NSString *strMonthMin;
            NSString *strDayHour;
            NSString *strYear;
            
            if (selectedMonthIndexPath)
            {
                NSDictionary *dicYear = arrMonth[selectedMonthIndexPath.item];
                strMonthMin = self.isTime ? [dicYear stringValueForJSON:@"day"] : [dicYear stringValueForJSON:@"name"];
                strYear = [dicYear stringValueForJSON:@"year"];
            }

            if (selectedDayIndexPath)
            {
                NSDictionary *dicDay = arrDay[selectedDayIndexPath.item];
                strDayHour = [dicDay stringValueForJSON:@"day"];
            }
            
            if (!strMonthMin)
            {
                [self customAlertViewWithOneButton:@"" Message: self.isTime ? @"Please select hour." : @"Please select day." ButtonText:@"OK" Animation:YES completed:nil];
                return;
            }
            
            if (!strDayHour)
            {
                [self customAlertViewWithOneButton:@"" Message: self.isTime ? @"Please select minute." : @"Please select month." ButtonText:@"OK" Animation:YES completed:nil];
                return;
            }

            NSLog(@"%@ %@",strDayHour, strMonthMin);
            
            NSString *strFinalText;
            NSString *strDateForApi;
            
            if (self.isTime)
                strFinalText = btnAM.selected ? [NSString stringWithFormat:@"%@ : %@ am",strMonthMin, strDayHour] : [NSString stringWithFormat:@"%@ : %@ pm",strMonthMin, strDayHour];
            else
            {
                strFinalText = [NSString stringWithFormat:@"%@ %@",strDayHour, strMonthMin];
                strDateForApi = [NSString stringWithFormat:@"%@ %@",strFinalText,strYear];
            }
            
            if (self.configureGetTimeDate)
                self.configureGetTimeDate(strFinalText,self.isTime,strDateForApi);

            [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}


@end
