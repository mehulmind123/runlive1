//
//  TblActivityVideo+CoreDataProperties.h
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivityVideo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblActivityVideo (CoreDataProperties)

+ (NSFetchRequest<TblActivityVideo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *activity_id;
@property (nullable, nonatomic, copy) NSString *activity_main_type;
@property (nullable, nonatomic, copy) NSString *activity_post_time;
@property (nullable, nonatomic, copy) NSString *activity_text;
@property (nullable, nonatomic, copy) NSString *caption;
@property (nullable, nonatomic, retain) NSObject *comments;
@property (nullable, nonatomic, copy) NSNumber *is_like;
@property (nullable, nonatomic, copy) NSNumber *isAdminActivity;
@property (nullable, nonatomic, copy) NSString *session_id;
@property (nullable, nonatomic, copy) NSString *session_name;
@property (nullable, nonatomic, copy) NSString *thumb_image;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, copy) NSString *total_comments;
@property (nullable, nonatomic, copy) NSString *total_likes;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_image;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, copy) NSString *video_height;
@property (nullable, nonatomic, copy) NSNumber *video_mute;
@property (nullable, nonatomic, copy) NSNumber *video_seek_time;
@property (nullable, nonatomic, copy) NSString *video_url;
@property (nullable, nonatomic, copy) NSString *video_width;
@property (nullable, nonatomic, copy) NSNumber *is_commented;
@property (nullable, nonatomic, retain) TblActivityDates *objActivityDate;

@end

NS_ASSUME_NONNULL_END
