//
//  NotificationPermissionView.m
//  RunLive
//
//  Created by mac-0005 on 01/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "NotificationPermissionView.h"

@implementation NotificationPermissionView

+ (id)initNotificationPermissionView
{
    NotificationPermissionView *objNotificaitonView = [[[NSBundle mainBundle]loadNibNamed:@"NotificationPermissionView" owner:nil options:nil] lastObject];
    CViewSetWidth(objNotificaitonView, CScreenWidth);
    CViewSetHeight(objNotificaitonView, CScreenHeight);
    return objNotificaitonView;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self Initialization];
}

#pragma mark - Configuration

-(void)Initialization
{
    self.btnNotificationDoIt.layer.cornerRadius = self.btnNotificationNotNow.layer.cornerRadius = 3;
}
@end
