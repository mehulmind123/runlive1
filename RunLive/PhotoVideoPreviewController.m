//
//  PhotoVideoPreviewController.m
//  RunLive Demo
//
//  Created by mac-00014 on 11/21/16.
//  Copyright © 2016 MI. All rights reserved.
//

#import "PhotoVideoPreviewController.h"
#import "RunningViewController.h"
#import "TwitterVideoUpload.h"
#import "videoWaterMarkView.h"
#import <AVKit/AVKit.h>


@interface PhotoVideoPreviewController ()

@end

@implementation PhotoVideoPreviewController
{
    AVPlayer *player;
    NSData *shareData;
    UIDocumentInteractionController *documentController;
    PHAsset *assetWithWaterMark;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialization];
    
    
    [TwitterVideoUpload instance].statusContent = @"#TwitterVideo https://github.com/mtrung/TwitterVideoUpload";
    
    if (self.asset)
    {
         [self getImageFromAsset];
        btnPlayVideo.hidden = viewVideoContainer.hidden = self.asset.mediaType == PHAssetMediaTypeImage;
        btnZoomImage.hidden = self.asset.mediaType != PHAssetMediaTypeImage;
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
    appDelegate.isSharingVideoPhotoOnFacebook = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

    if (self.asset)
    {
        if (self.asset.mediaType == PHAssetMediaTypeImage)
        {
            //
            viewVideoContainer.hidden = YES;
            viewImageContainer.hidden = NO;
        }
        else
        {
            viewVideoContainer.hidden = NO;
            viewImageContainer.hidden = YES;
            [self getVideoFromAsset:self.asset];

        }
    }
    
}

#pragma mark - Configuration
#pragma mark -

-(void)initialization
{
      btnUpload.layer.cornerRadius = btnShareFB.layer.cornerRadius = btnShareTwitter.layer.cornerRadius = btnShareEmail.layer.cornerRadius = btnShareInstagram.layer.cornerRadius = 2;
     btnUpload.layer.masksToBounds = btnShareFB.layer.masksToBounds = btnShareTwitter.layer.masksToBounds = btnShareEmail.layer.masksToBounds = btnShareInstagram.layer.masksToBounds = YES;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, txtAddCaption.frame.size.height)];
    txtAddCaption.leftView = paddingView;
    txtAddCaption.leftViewMode = UITextFieldViewModeAlways;
    txtAddCaption.rightView = paddingView;
    txtAddCaption.rightViewMode = UITextFieldViewModeAlways;
        
    [txtAddCaption setValue: CRGB(122, 126, 143) forKeyPath:@"_placeholderLabel.textColor"];
    
    txtAddCaption.layer.borderColor = CRGB(199, 202, 220).CGColor;
    txtAddCaption.layer.borderWidth = 1;
    txtAddCaption.layer.cornerRadius = 3;
    txtAddCaption.layer.masksToBounds = true;
    [self setWatermarkData];
}

#pragma mark - Photo Related Functionality
-(void)setPreviewItemHeight:(CGFloat)imgWidth Height:(CGFloat)imgHeight
{
    
    //......Available space for Video
    CGFloat wAvailable = CScreenWidth;
    //    CGFloat hAvailable = CScreenWidth;
    
    //......Original width and height of video.
    CGFloat wOriginalVideo = imgWidth;
    CGFloat hOriginalVideo = imgHeight;
    
    //......Scale to fit in (<= screen square rect)
    CGFloat hAspect = wOriginalVideo == 0 ? 0 : (hOriginalVideo) * (wAvailable / wOriginalVideo);
    cnPreviewItemHeight.constant = hAspect;//MIN(hAspect, hAvailable);
}

-(void)setWatermarkData
{
    lblPhotoTotalDistance.text = [self.dicPhotoVideoWaterMark stringValueForJSON:@"distance"];
    lblPhotoMovingTime.text = [self.dicPhotoVideoWaterMark stringValueForJSON:@"time"];
    lblPhotoPace.text = [self.dicPhotoVideoWaterMark stringValueForJSON:@"pace"];
    lblPhotoRank.text = [self.dicPhotoVideoWaterMark stringValueForJSON:@"rank"];
}


-(void)getImageFromAsset
{
    imgVCapture.image = self.imgCapture;
    [self setPreviewItemHeight:self.imgCapture.size.width Height:self.imgCapture.size.height];
}

-(void)getVideoFromAsset:(PHAsset *)PlayAsset
{
    [[PHImageManager defaultManager] requestAVAssetForVideo:PlayAsset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info)
     {
         // Set Preview item view frame....
         AVMutableComposition *mutableComposition;
         mutableComposition = (AVMutableComposition *)avAsset;
         AVAssetTrack *videoAssetTrack = [[mutableComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
         CGSize naturalSize = videoAssetTrack.naturalSize;
         
         //......Orientation of video.
         switch ([self orientationForVideoTrack:videoAssetTrack])
         {
             case UIInterfaceOrientationLandscapeLeft:
             case UIInterfaceOrientationLandscapeRight:
             {
                 // naturalSize keep as it is
                 break;
             }
             case UIInterfaceOrientationPortraitUpsideDown:
             case UIInterfaceOrientationPortrait:
             {
                 naturalSize = CGSizeMake(naturalSize.height, naturalSize.width);
                 break;
             }
             default:
                 break;
         }
         
         dispatch_async(GCDMainThread, ^{
             [self setPreviewItemHeight:naturalSize.width Height:naturalSize.height];
             [self.view layoutIfNeeded];
         });
         
         
         // Use the AVAsset avAsset
         AVURLAsset *assetURL = [AVURLAsset assetWithURL:((AVURLAsset *)avAsset).URL];
         NSString *str= [NSString stringWithFormat:@"%@",assetURL.URL];
         NSURL *url = [NSURL URLWithString:str];
         
         shareData = [NSData dataWithContentsOfURL:url];
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             player = [AVPlayer playerWithURL:url]; //
             AVPlayerLayer *avLayer = [AVPlayerLayer layer];
             [avLayer setPlayer:player];
             [avLayer setFrame:CGRectMake(0, 0, viewVideo.frame.size.width, viewVideo.frame.size.height)];
             [avLayer setBackgroundColor:[UIColor whiteColor].CGColor];
             //             [avLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
             [viewVideo.layer addSublayer:avLayer];
             
             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
         });
     }];
}


- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    // not equivalent to image.size (which depends on the imageOrientation)!
    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x = (refWidth - size.width) / 2.0;
    double y = (refHeight - size.height) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    
    return cropped;
}

#pragma mark - AVPlayer Delegate Method
-(void)itemDidFinishPlaying:(NSNotification *) notification
{
    btnPlayVideo.hidden = NO;
    [player seekToTime:kCMTimeZero];
}

#pragma mark - Api Functions
-(void)uploadMedia:(NSString *)type Data:(NSData *)mediaData
{
    if (!self.strPreviewMediaSessionID)
        return;
    
    [[APIRequest request] uploadPhotoAndVideo:type
                                    SessionID:self.strPreviewMediaSessionID
                                   Attachment:mediaData
                                      Caption:txtAddCaption.text
                                     Latitude:self.mediaLocation.coordinate.latitude
                                    Longitude:self.mediaLocation.coordinate.longitude
                                    completed:^(id responseObject, NSError *error)
     {
     }];
    
    [self moveOnRunningView];
}


#pragma mark - Social share fuctions

#pragma mark - Facebook Sharing
-(void)shareOnFacebook
{
    if (self.asset.mediaType == PHAssetMediaTypeImage)
    {
        viewImageWaterMark.hidden = NO;
        UIImage *imageWaterMark = [viewImageContainer screenshot];
        viewImageWaterMark.hidden = YES;
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:@"RunLive app!"];
        [controller addImage:imageWaterMark];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else
    {
        
        [self exportVideoWithFinalOutput:NO completed:^(NSURL *videoFinalUrl, BOOL isError, PHAsset *watermarkAsset)
        {
            if (!isError)
            {
                assetWithWaterMark = watermarkAsset;
                NSString *strLocalIdentifier=[watermarkAsset.localIdentifier substringToIndex:36];

                NSArray *Array = [watermarkAsset.localIdentifier componentsSeparatedByString:@"/"];
                if (Array.count > 0)
                {
                    strLocalIdentifier = Array[0];
                }

                NSString *strUrl = [NSString stringWithFormat:@"assets-library://asset/asset.MOV?id=%@&ext=MOV",strLocalIdentifier];
                NSURL *strFinalURL = [NSURL URLWithString:strUrl];
                
                appDelegate.isSharingVideoPhotoOnFacebook = YES;
                if(![FBSDKAccessToken currentAccessToken])
                {
                    FBSDKLoginManager *login1 = [[FBSDKLoginManager alloc]init];
                    [login1 logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                        FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
                        video.videoURL = strFinalURL;
                        FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                        content.video = video;
                        
                        [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
                        
                    }];
                }
                else {
                    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
                    video.videoURL = strFinalURL;
                    
                    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                    content.video = video;
                    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
                }
            }
        }];
    }
        
        
}

#pragma mark - FBSDKSharingDelegate
#pragma mark -

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"completed share:%@", results);
    [self deleteVideoFromAssetLibrary];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"sharing error:%@", error);
    NSString *message = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?:
    @"There was a problem sharing, please try again later.";
    NSString *title = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops!";
    [self deleteVideoFromAssetLibrary];
    
    [self customAlertViewWithOneButton:title Message:message ButtonText:@"OK" Animation:NO completed:nil];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"share cancelled");
    [self deleteVideoFromAssetLibrary];
}

#pragma mark - Twitter Sharing
#pragma mark -

-(void)sahreOnTwitter
{
    if(self.asset.mediaType == PHAssetMediaTypeImage)
    {
        viewImageWaterMark.hidden = NO;
        UIImage *imageWaterMark = [viewImageContainer screenshot];
        viewImageWaterMark.hidden = YES;
        
        // Attach an image to the Share
        //      if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        //        {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:txtAddCaption.text];
        [tweetSheet addImage:imageWaterMark];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        //        }
        //        else
        //        {
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        //        }
    }
    else
    {
        if (appDelegate.objTwitter.sessionStore.hasLoggedInUsers)
        {
            [self loginWithSession:appDelegate.objTwitter.sessionStore.session];
        }
        else
        {
            [appDelegate.objTwitter logInWithCompletion:^(TWTRSession * _Nullable session, NSError * _Nullable error)
             {
                 [self loginWithSession:session];
             }];
        }
        
    }
    
}

- (void)loginWithSession:(TWTRSession *)session
{
    [self exportVideoWithFinalOutput:NO completed:^(NSURL *videoFinalUrl, BOOL isError, PHAsset *watermarkAsset)
    {
        if (!isError)
        {
            if (session)
            {
                [GiFHUD showWithOverlay];
                __block NSString *mediaID = @"";
                __block NSDictionary *postParams = @{};
                NSString *url = @"https://upload.twitter.com/1.1/media/upload.json";
                
                TWTRAPIClient *client = [[TWTRAPIClient alloc] initWithUserID:session.userID];
                postParams = @{@"command": @"INIT",
                               @"media_category" : @"amplify_video",
                               @"total_bytes" : [NSNumber numberWithInteger: shareData.length].stringValue,
                               @"media_type" : @"video/mp4"
                               };
                NSError *error;
                NSURLRequest *preparedRequest = [client URLRequestWithMethod:@"POST" URL:url parameters:postParams error:&error];
                
                [client sendTwitterRequest:preparedRequest completion:^(NSURLResponse *urlResponse, NSData *responseData, NSError *error){
                    
                    if(!error){
                        NSError *jsonError;
                        NSDictionary *json = [NSJSONSerialization
                                              JSONObjectWithData:responseData
                                              options:0
                                              error:&jsonError];
                        
                        mediaID = [json objectForKey:@"media_id_string"];
                        
                        NSError *error;
                        NSString *videoString = [shareData base64EncodedStringWithOptions:0];
                        // Second call with command APPEND
                        postParams = @{@"command" : @"APPEND",
                                       @"media_id" : mediaID,
                                       @"segment_index" : @"0",
                                       @"media" : videoString};
                        
                        NSURLRequest *preparedRequest = [client URLRequestWithMethod:@"POST" URL:url parameters:postParams error:&error];
                        
                        [client sendTwitterRequest:preparedRequest completion:^(NSURLResponse *urlResponse, NSData *responseData, NSError *error){
                            
                            if(!error){
                                NSError *error;
                                // Third call with command FINALIZE
                                postParams = @{@"command" : @"FINALIZE",
                                               @"media_id" : mediaID};
                                
                                NSURLRequest *preparedRequest = [client URLRequestWithMethod:@"POST" URL:url parameters:postParams error:&error];
                                
                                [client sendTwitterRequest:preparedRequest completion:^(NSURLResponse *urlResponse, NSData *responseData, NSError *error){
                                    
                                    if(!error){
                                        
                                        NSError *error;
                                        // publish video with status
                                        NSString *url = @"https://api.twitter.com/1.1/statuses/update.json";
                                        NSMutableDictionary *message = [[NSMutableDictionary alloc] initWithObjectsAndKeys:txtAddCaption.text,@"status",@"true",@"wrap_links",mediaID, @"media_ids", nil];
                                        NSURLRequest *preparedRequest = [client URLRequestWithMethod:@"POST" URL:url parameters:message error:&error];
                                        
                                        [client sendTwitterRequest:preparedRequest completion:^(NSURLResponse *urlResponse, NSData *responseData, NSError *error){
                                            if(!error)
                                            {
                                                [GiFHUD dismiss];
                                                NSError *jsonError;
                                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
                                                NSLog(@"%@", json);
                                                [self moveOnRunningView];
                                            }
                                            else
                                            {
                                                NSLog(@"Error: %@", error);
                                                [self customAlertViewWithOneButton:@"" Message:error.localizedDescription ButtonText:@"OK" Animation:NO completed:nil];
                                                [GiFHUD dismiss];
                                            }
                                        }];
                                    }else{
                                        NSLog(@"Error command FINALIZE: %@", error);
                                        [self customAlertViewWithOneButton:@"" Message:error.localizedDescription ButtonText:@"OK" Animation:NO completed:nil];
                                        [GiFHUD dismiss];
                                    }
                                }];
                                
                            }else{
                                NSLog(@"Error command APPEND: %@", error);
                                [self customAlertViewWithOneButton:@"" Message:error.localizedDescription ButtonText:@"OK" Animation:NO completed:nil];
                                [GiFHUD dismiss];
                            }
                        }];
                    }
                    else
                    {
                        [GiFHUD dismiss];
                    }
                }];
            }
        }
    }];
}

#pragma mark - Email Sharing
#pragma mark -

-(void)shareOnEmail
{
    if (self.asset.mediaType == PHAssetMediaTypeImage)
    {
        viewImageWaterMark.hidden = NO;
        UIImage *imageWaterMark = [viewImageContainer screenshot];
        viewImageWaterMark.hidden = YES;
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail])
        {
            mailer.mailComposeDelegate = self;
            mailer.navigationController.navigationBar.translucent = NO;
            [mailer setSubject:txtAddCaption.text];

            NSData *myData = UIImageJPEGRepresentation(imageWaterMark, 0.9);
            [mailer addAttachmentData:myData mimeType:@"image/jpg" fileName:@"RunLive.jpg"];
            
            [self presentViewController:mailer animated:YES completion:^(void)
             {
                 [self hideShowStatusBar:YES];
             }];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }
    }
    else
    {
        [self exportVideoWithFinalOutput:NO completed:^(NSURL *videoFinalUrl, BOOL isError, PHAsset *watermarkAsset)
        {
            if (!isError)
            {
                assetWithWaterMark = watermarkAsset;
                
                AVAsset *avAsset = player.currentItem.asset;
                AVURLAsset *assetURL = [AVURLAsset assetWithURL:((AVURLAsset *)avAsset).URL];
                NSString* videoPath = assetURL.URL.absoluteString;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                                         [NSString stringWithFormat:@"/Saved Video/%@",[videoPath lastPathComponent]]];
                
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                NSData *fileData = [self getShareDataInBytes];
                
                [mail addAttachmentData:fileData mimeType:@"video/quicktime" fileName:[myPathDocs lastPathComponent]];
                [self presentViewController:mail animated:YES completion:nil];
            }
        }];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Instagram Sharing
#pragma mark -

-(void)shareOnInstagram
{
    if(self.asset.mediaType == PHAssetMediaTypeImage)
    {
        viewImageWaterMark.hidden = NO;
        UIImage *imageWaterMark = [viewImageContainer screenshot];
        viewImageWaterMark.hidden = YES;
        
        NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
        if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
        {
            NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
//            NSData *contentData = [self getShareDataInBytes];
            NSData *contentData = UIImagePNGRepresentation(imageWaterMark);
            
            [contentData writeToFile:saveImagePath atomically:YES];
            
            NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
            documentController=[[UIDocumentInteractionController alloc]init];
            
            documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
            documentController.delegate = self;
            documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", txtAddCaption.text], @"InstagramCaption", nil];
            documentController.UTI = @"com.instagram.exclusivegram";
            [documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:self.view animated:YES];
        }
        else
        {
            [self customAlertViewWithOneButton:@"" Message:@"Instagram not found." ButtonText:@"OK" Animation:YES completed:nil];
        }
    }
    else
    {
        // Video Sharing on Instagrame...
//        AVAsset *avAsset = player.currentItem.asset;
//        AVURLAsset *assetURL = [AVURLAsset assetWithURL:((AVURLAsset *)avAsset).URL];
//        [self loadCameraRollAssetToInstagram:assetURL.URL andMessage:txtAddCaption.text];
        
        [self exportVideoWithFinalOutput:NO completed:^(NSURL *videoFinalUrl, BOOL isError, PHAsset *watermarkAsset)
         {
             if (!isError) {
                 assetWithWaterMark = watermarkAsset;
                 [self loadCameraRollAssetToInstagram:assetWithWaterMark andMessage:txtAddCaption.text];
             }
         }];
    }
}

- (void)loadCameraRollAssetToInstagram:(PHAsset*)finalAsset andMessage:(NSString*)message
{
//    NSString *mediaId = self.asset.localIdentifier;
    NSString *mediaId = finalAsset.localIdentifier;
    NSString *escapedCaption  = [message stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=%@&InstagramCaption=%@",mediaId,escapedCaption]];
    
    //    @"instagram://library?LocalIdentifier=\(localID)";
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:^(BOOL success) {
            //
        }];
//        [[UIApplication sharedApplication] openURL:instagramURL];
    }
}


-(NSData *)getShareDataInBytes
{
    NSData *data;

    
    if(self.asset.mediaType == PHAssetMediaTypeImage)
    {
        // Attach an image to the email
        data = UIImagePNGRepresentation(imgVCapture.image);
    }
    else
    {
        data = shareData;
    }

    return data;
    
}


#pragma mark - Extract Output Video
#pragma mark -

-(void)deleteVideoFromAssetLibrary
{
    
    if (assetWithWaterMark)
    {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            BOOL req = [assetWithWaterMark canPerformEditOperation:PHAssetEditOperationDelete];
            if (req) {
                NSLog(@"true");
                [PHAssetChangeRequest deleteAssets:@[assetWithWaterMark]];
            }
        } completionHandler:^(BOOL success, NSError *error) {
            NSLog(@"Finished Delete asset. %@", (success ? @"Success." : error));
            if (success) {
                NSLog(@"delete successfully");
            }
        }];
    }
    appDelegate.isSharingVideoPhotoOnFacebook = NO;
}

// A method to identify current video orientation
- (UIInterfaceOrientation)orientationForVideoTrack:(AVAssetTrack *)videoTrack
{
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}


- (void)exportVideoWithFinalOutput:(BOOL)isNormalUpload completed:(void (^)(NSURL *videoFinalUrl ,BOOL isError,PHAsset *watermarkAsset))completion;
{
    
    AVMutableComposition *mutableComposition;
    
    AVAsset *videoAsset = player.currentItem.asset;
    
    mutableComposition = (AVMutableComposition *)videoAsset;
    
    // 4 - Get path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"FinalVideo.mov"]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:myPathDocs])
    {
        NSError *error;
        
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:myPathDocs]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:&error];
            if (!success) {
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
        
    }
    
    NSURL *finalVideoOutputPath = [NSURL fileURLWithPath:myPathDocs];
    
    //.....Create an AVAssetTrack with our final asset
    AVAssetTrack *videoAssetTrack = [[mutableComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize naturalSize = videoAssetTrack.naturalSize;
    
    //......Orientation of video.
    switch ([self orientationForVideoTrack:videoAssetTrack])
    {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            // naturalSize keep as it is
            break;
        }
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationPortrait:
        {
            naturalSize = CGSizeMake(naturalSize.height, naturalSize.width);
            break;
        }
        default:
            break;
    }
    
    //......Crop Rect Video
    CGFloat cropOffX = 0;
    CGFloat cropOffY = 0;
    CGFloat cropWidth = naturalSize.width;
    CGFloat cropHeight = naturalSize.height;
    
    //...If selected video is scrollable and not fit to square
//    CGRect aspectRect = CGRectMake(0, 0, viewVideo.bounds.size.width, viewVideo.bounds.size.height);
//    CGRect visibleRect = CGRectMake(0, 0, naturalSize.width, naturalSize.height);
//
//    //......(screen square rect if not)
//    if (naturalSize.width != naturalSize.height && aspectRect.size.width != aspectRect.size.height)
//    {
//        cropOffX = naturalSize.width * visibleRect.origin.x / aspectRect.size.width;
//        cropOffY = naturalSize.height * visibleRect.origin.y / aspectRect.size.height;
//
//        if (naturalSize.width < naturalSize.height)
//        {
//            cropWidth = naturalSize.width;
//            cropHeight = (visibleRect.size.height / visibleRect.size.width) * naturalSize.width;
//        }
//        else
//        {
//            cropWidth = (visibleRect.size.width / visibleRect.size.height) * naturalSize.height;
//            cropHeight = naturalSize.height;
//        }
//    }
    
    
    
    //.....Create a video composition and set some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.renderSize = CGSizeMake(cropWidth, cropHeight);
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    
    //.....Create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoAssetTrack];
    
    UIInterfaceOrientation videoOrientation = [self orientationForVideoTrack:videoAssetTrack];
    
    CGAffineTransform t1 = CGAffineTransformIdentity;
    CGAffineTransform t2 = CGAffineTransformIdentity;
    
    switch (videoOrientation) {
        case UIInterfaceOrientationPortrait:
            t1 = CGAffineTransformMakeTranslation(videoAssetTrack.naturalSize.height - cropOffX, 0 - cropOffY);
            t2 = CGAffineTransformRotate(t1, M_PI_2);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, videoAssetTrack.naturalSize.width - cropOffY);
            t2 = CGAffineTransformRotate(t1, - M_PI_2);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, 0 - cropOffY);
            t2 = CGAffineTransformRotate(t1, 0);
            break;
        case UIInterfaceOrientationLandscapeRight:
            t1 = CGAffineTransformMakeTranslation(videoAssetTrack.naturalSize.width - cropOffX, videoAssetTrack.naturalSize.height - cropOffY);
            t2 = CGAffineTransformRotate(t1, M_PI);
            break;
        default:
            NSLog(@"no supported orientation has been found in this video");
            break;
    }
    
    CGAffineTransform finalTransform = t2;
    [layerInstruction setTransform:finalTransform atTime:kCMTimeZero];
    
    // Add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    // For socail sharing only...
//    isNormalUpload = YES;
    if (!isNormalUpload)
    {
        [self applyVideoEffectsToComposition:videoComposition size:naturalSize];
    }
    
    
    // Now export final video on finalVideoOutputPath
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mutableComposition presetName:AVAssetExportPreset1280x720]; // AVAssetExportPresetHighestQuality
    exporter.outputURL = finalVideoOutputPath;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = videoComposition;
    exporter.shouldOptimizeForNetworkUse = YES;
    
        [GiFHUD showWithOverlay];
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [GiFHUD dismiss];
                switch ([exporter status])
                {
                    case AVAssetExportSessionStatusFailed:
                    {
                        if (completion) {
                            completion(nil,YES,nil);
                        }
                    }
                        break;
                    case AVAssetExportSessionStatusCancelled:
                        if (completion) {
                            completion(nil,YES,nil);
                        }
                        break;
                    case AVAssetExportSessionStatusCompleted:
                    {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            NSData *mediaData = [NSData dataWithContentsOfURL:exporter.outputURL];
                            
                            shareData = mediaData;
                            
                            if (isNormalUpload)
                                [self uploadMedia:@"2" Data:mediaData];
                            else
                            {
                                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
                                 {
                                     [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:finalVideoOutputPath];
                                 }completionHandler:^(BOOL success, NSError *error)
                                 {
                                     if (success)
                                     {
                                         NSLog(@"Movie saved to camera roll ======= ");
                                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                             
                                                 PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
//                                                         double duration = 20.5;// value in seconds
                                                 double duration = 10.1;// value in seconds
                                                 fetchOptions.predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"mediaType = %ld AND duration < %f",(long)PHAssetMediaTypeVideo,duration]];
                                                 fetchOptions.sortDescriptors =@[[NSSortDescriptor sortDescriptorWithKey:@"modificationDate" ascending:NO]];
                                               PHFetchResult *resultAsset = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:fetchOptions];
                                                 
                                                 if (resultAsset.count > 0)
                                                 {
                                                     if (completion)
                                                         completion(finalVideoOutputPath,NO,resultAsset.firstObject);
                                                 }
                                             });
                                         });
                                     }
                                     else
                                     {
                                         NSLog(@"Could not save movie to camera roll. Error: %@", error);
                                     }
                                 }];
                            }
                        });
                    }
                        break;
                        
                    default:
                        break;
                }
            });
        }];
}


- (void)applyVideoEffectsToComposition:(AVMutableVideoComposition *)composition size:(CGSize)size
{
    CALayer *overlayLayer = [CALayer layer];
    UIImage *overlayImage = nil;
    
    UIView *viewTempMark = [UIView new];
    viewTempMark.frame = CGRectMake(0, 0, size.width, size.height);
    
    videoWaterMarkView *objVideoWaterMark = [videoWaterMarkView initVideoWaterMarkView:size];
    [objVideoWaterMark setDataForWaterMark:self.dicPhotoVideoWaterMark];
    [objVideoWaterMark layoutIfNeeded];
    [viewTempMark addSubview:objVideoWaterMark];
    
    UIImageView *imgTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    imgTemp.contentMode = UIViewContentModeScaleAspectFit;
    imgTemp.backgroundColor = [UIColor clearColor];
    
    UIGraphicsBeginImageContextWithOptions(viewTempMark.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [viewTempMark.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *imgWaterMark = UIGraphicsGetImageFromCurrentImageContext();
    imgTemp.image = imgWaterMark;
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(imgTemp.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [imgTemp.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * imgFinal = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    overlayLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer setMasksToBounds:YES];
    
    overlayImage = imgFinal;
    [overlayLayer setContents:(id)[overlayImage CGImage]];
    
    // 2 - set up the parent layer
    CALayer *parentLayer = [CALayer layer];
    [parentLayer setMasksToBounds:YES];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    
    CALayer *videoLayer = [CALayer layer];
    [videoLayer setMasksToBounds:YES];
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    // 3 - apply magic
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

#pragma mark - IBAction Event
#pragma mark-

-(void)moveOnRunningView
{
    [self.navigationController popViewControllerAnimated:YES];
    
//    for (int i = 0; [[self navigationController] viewControllers].count >i; i++)
//    {
//        UIViewController *viewController = [[self navigationController] viewControllers][i];
//        if ([viewController isKindOfClass:[RunningViewController class]])
//        {
//            RunningViewController *objRun = (RunningViewController *)viewController;
//            [self.navigationController popToViewController:objRun animated:YES];
//            break;
//        }
//    }
}

-(IBAction)btnUploadCLK:(id)sender
{
    
    if (self.asset.mediaType == PHAssetMediaTypeImage)
    {
        // Uplaod Photo Api
        NSData *mediaData = [NSData dataWithData:UIImageJPEGRepresentation(imgVCapture.image, 0.9)];
        [self uploadMedia:@"1" Data:mediaData];
    }
    else
    {
        // Uplaod Video Api
        [self exportVideoWithFinalOutput:YES completed:nil];
    }
    
}

-(IBAction)btnSocialCLK:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            //Facebook
            [self shareOnFacebook];
//            [self moveOnRunningView];
        }
            break;
        case 1:
        {
            //Twitter
            [self sahreOnTwitter];
//            [self moveOnRunningView];
        }
            break;
        case 2:
        {
            //Email
            [self shareOnEmail];
//            [self moveOnRunningView];
        }
            break;
        case 3:
        {
            //Instagram
            [self shareOnInstagram];
//            [self moveOnRunningView];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)btnBackAction:(UIButton *)sender
{
    [player pause];
    btnPlayVideo.hidden = NO;
    
    if (self.configureBtnBackCLK)
        self.configureBtnBackCLK(YES);
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnPlayVideoAction:(UIButton *)sender
{
    [player play];
    btnPlayVideo.hidden = YES;
}

-(IBAction)btnZoomImage:(id)sender
{
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.placeholderImage = imgVCapture.image;
    imageInfo.image = imgVCapture.image;
    imageInfo.referenceRect = imgVCapture.frame;
    imageInfo.referenceView = imgVCapture.superview;
    imageInfo.referenceContentMode = imgVCapture.contentMode;
    imageInfo.referenceCornerRadius = imgVCapture.layer.cornerRadius;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo mode:JTSImageViewControllerMode_Image backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];

}


@end
