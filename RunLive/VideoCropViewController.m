//
//  VideoCropViewController.m
//  Runlive
//
//  Created by mac-0005 on 08/03/18.
//  Copyright © 2018 mac-0005. All rights reserved.
//

#import "VideoCropViewController.h"
#import "MIVideoCropperView.h"


@interface VideoCropViewController ()

@end

@implementation VideoCropViewController
{
    MIVideoCropperView *vCropVideo;
    NSURL *videoUrl;
    AVPlayer *player;
    CLMBProgress *clProgress;
}

- (id)initWithVideo:(NSURL *)strVideoUrl
{
    self = [super initWithNibName:@"VideoCropViewController" bundle:nil];
    
    if (self)
    {
//        self.delegate = delegate;
//        self.originalImage = [self fixOrientation:originalImage];
//
//        isImageSizeValidation = isSizeValidation;
        videoUrl = strVideoUrl;
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark -
#pragma mark - General Methods

- (void)initialize
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSURL *movieURL = videoUrl;
        player = [AVPlayer playerWithURL:movieURL]; //
        player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        AVPlayerLayer *avLayer = [AVPlayerLayer layer];
        [avLayer setPlayer:player];
        [avLayer setFrame:CGRectMake(0, 0, viewVideoPreview.frame.size.width,viewVideoPreview.frame.size.height)];
        [avLayer setBackgroundColor:[UIColor clearColor].CGColor];
        [avLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        player.muted = NO;
        [viewVideoPreview.layer addSublayer:avLayer];
        [player play];
        
        [self addCropView];
        
        
        [self getVideofromLibrarayIsFromSave:NO];
        clProgress = [[CLMBProgress alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight)];
        clProgress.hidden = YES;
        [self.view addSubview:clProgress];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[player currentItem]];
    });
}


-(void)addCropView
{
    CGRect frame = viewVideoPreview.frame;
    frame.origin = CGPointMake(0, 0);
    vCropVideo  = [[MIVideoCropperView alloc] initWithFrame:frame];
    [viewVideoPreview addSubview:vCropVideo];
}


#pragma mark -
#pragma mark - Merge/Export video
// A method to identify current video orientation
- (NSString*) applicationDocumentsDirectory
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (void)removeFileAtPath:(NSString *)outputPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:outputPath])
    {
        if ([fileManager isDeletableFileAtPath:outputPath])
        {
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:outputPath error:&error];
            if (!success) {
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }
}

- (UIInterfaceOrientation)orientationForVideoTrack:(AVAssetTrack *)videoTrack
{
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

- (void)exportVideoWithFinalOutput:(NSURL *)recordedURL
{
    AVMutableComposition *mutableComposition;
    mutableComposition = (AVMutableComposition *)[AVAsset assetWithURL:recordedURL];
    
    //.....Final video output path
    NSString *documentsDirectory = [self applicationDocumentsDirectory];
    NSString *videoPath = [documentsDirectory stringByAppendingPathComponent:@"final_video.mov"];
    [self removeFileAtPath:videoPath];
    NSURL *finalVideoOutputPath = [[NSURL alloc] initFileURLWithPath:videoPath];
    
    //.....Create an AVAssetTrack with our final asset
    AVAssetTrack *videoAssetTrack = [[mutableComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize naturalSize = videoAssetTrack.naturalSize;
    
    
    //......Orientation of video.
    switch ([self orientationForVideoTrack:videoAssetTrack])
    {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            // naturalSize keep as it is
            break;
        }
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationPortrait:
        {
            naturalSize = CGSizeMake(naturalSize.height, naturalSize.width);
            break;
        }
        default:
            break;
    }
    
    
    //......Crop Rect Video
    CGFloat cropOffX = 0;
    CGFloat cropOffY = 0;
    CGFloat cropWidth = naturalSize.width;
    CGFloat cropHeight = naturalSize.height;
    
    
    //...If selected video is scrollable and not fit to square
    CGRect aspectRect = CGRectMake(0, 0, CViewWidth(vCropVideo.overlayView), CViewHeight(vCropVideo.overlayView));
    CGRect visibleRect = vCropVideo.overlayView.clearRect;
    
    //......(screen square rect if not)
    if (naturalSize.width != naturalSize.height && aspectRect.size.width != aspectRect.size.height)
    {
        cropOffX = naturalSize.width * visibleRect.origin.x / aspectRect.size.width;
        cropOffY = naturalSize.height * visibleRect.origin.y / aspectRect.size.height;
        
        if (naturalSize.width < naturalSize.height)
        {
            cropWidth = naturalSize.width;
            cropHeight = (visibleRect.size.height / visibleRect.size.width) * naturalSize.width;
        }
        else
        {
            cropWidth = (visibleRect.size.width / visibleRect.size.height) * naturalSize.height;
            cropHeight = naturalSize.height;
        }
        
        cropHeight = cropHeight+1; // Buffer height....
        
    }
    
    
    //.....Create a video composition and set some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    //    videoComposition.renderSize = CGSizeMake(naturalSize.height, naturalSize.height);   //....render size to its height x height (Square)
    videoComposition.renderSize = CGSizeMake(cropWidth, cropHeight);
    
    videoComposition.frameDuration = CMTimeMake(1, 25);
    
    
    //.....Create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, mutableComposition.duration);
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoAssetTrack];
    UIInterfaceOrientation videoOrientation = [self orientationForVideoTrack:videoAssetTrack];
    
    CGAffineTransform t1 = CGAffineTransformIdentity;
    CGAffineTransform t2 = CGAffineTransformIdentity;
    
    switch (videoOrientation) {
        case UIInterfaceOrientationPortrait:
            t1 = CGAffineTransformMakeTranslation(videoAssetTrack.naturalSize.height - cropOffX, 0 - cropOffY);
            t2 = CGAffineTransformRotate(t1, M_PI_2);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, videoAssetTrack.naturalSize.width - cropOffY);
            t2 = CGAffineTransformRotate(t1, - M_PI_2);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, 0 - cropOffY);
            t2 = CGAffineTransformRotate(t1, 0);
            break;
        case UIInterfaceOrientationLandscapeRight:
            t1 = CGAffineTransformMakeTranslation(videoAssetTrack.naturalSize.width - cropOffX, videoAssetTrack.naturalSize.height - cropOffY);
            t2 = CGAffineTransformRotate(t1, M_PI);
            break;
        default:
            NSLog(@"no supported orientation has been found in this video");
            break;
    }
    CGAffineTransform finalTransform = t2;
    [layerInstruction setTransform:finalTransform atTime:kCMTimeZero];
    
    // Add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    // Now export final video on finalVideoOutputPath
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mutableComposition presetName:AVAssetExportPreset1280x720];
    exporter.outputURL = finalVideoOutputPath;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = videoComposition;
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([exporter status])
        {
            case AVAssetExportSessionStatusFailed:
                break;
            case AVAssetExportSessionStatusCancelled:
                break;
            case AVAssetExportSessionStatusCompleted:
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self removeFileAtPath:recordedURL.absoluteString];
                    [self saveVideoInCameraRoll:finalVideoOutputPath];
                });
            }
                break;
                
            default:
                break;
        }
    }];
}

- (void)saveVideoInCameraRoll:(NSURL *)videoUrl
{
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
     {
         [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:videoUrl];
     }completionHandler:^(BOOL success, NSError *error)
     {
         if (success)
         {
             NSLog(@"Movie saved to camera roll.");
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [self getVideofromLibrarayIsFromSave:YES];
             });
         }
         else
         {
             clProgress.hidden = YES;
             NSLog(@"Could not save movie to camera roll. Error: %@", error);
         }
     }];
    
}

- (void)getVideofromLibrarayIsFromSave:(BOOL)isFromSave
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.sortDescriptors =@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        PHFetchResult *resultAsset = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:fetchOptions];
        clProgress.hidden = YES;
        
        if (self.delegate && isFromSave)
        {
            [self.delegate finishCroping:[resultAsset objectAtIndex:0] didCancel:NO];
            [self dismissViewControllerAnimated:NO completion:nil];
        }
    });
}



#pragma mark - AVPlayerLayer Delegate
- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    [player seekToTime:kCMTimeZero];
    [player play];
}


#pragma mark - Action Event
-(IBAction)btnCancelCLK:(id)sender
{
    if (self.delegate)
        [self.delegate finishCroping:nil didCancel:YES];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btnCropVideoCLK:(id)sender
{
    if (videoUrl)
    {
        [self.view bringSubviewToFront:clProgress];
        clProgress.hidden = NO;
        clProgress.progressHUD.labelText = @"Preparing Video...";
        [self exportVideoWithFinalOutput:videoUrl];
    }
}
@end
