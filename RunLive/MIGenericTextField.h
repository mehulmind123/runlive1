//
//  MIGenericTextField.h
//  Aiminaabee
//
//  Created by mac-00014 on 10/21/16.
//  Copyright © 2016 mac-0009. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CDeviceHeight [UIScreen mainScreen].bounds.size.height
#define Is_iPhone_4 CDeviceHeight == 480
#define Is_iPhone_5 CDeviceHeight == 568
#define Is_iPhone_6 CDeviceHeight == 667
#define Is_iPhone_6_PLUS CDeviceHeight == 736
@interface MIGenericTextField : UITextField

@end
