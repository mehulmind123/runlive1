//
//  AboutViewController.m
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "AboutViewController.h"
#import "TermsAndPrivacyPoilicyViewController.h"


@interface AboutViewController ()

@end

@implementation AboutViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [webAbout setScalesPageToFit:NO];
    webAbout.delegate = self;
    
    [[APIRequest request] aboutUs:^(id responseObject, NSError *error)
    {
        NSString *strDescription = [NSString stringWithFormat:@"<html>" "<style type=\"text/css\">" "body { background-color:transparent; font-family:Gilroy; font-size:14;color: #7A7E93;}" "</style>" "<body>" "<p>%@</p>" "</body></html>", [[responseObject valueForKey:CJsonData] stringValueForJSON:@"content"]];
        
//        [webAbout loadHTMLString:strDescription baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        [webAbout loadHTMLString:strDescription baseURL:nil];
    }];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (([[request.URL absoluteString] rangeOfString:@"privacypolicy"].location != NSNotFound))
    {
            //    https://runlive.fit/privacypolicy
        TermsAndPrivacyPoilicyViewController *objPrivacy = [TermsAndPrivacyPoilicyViewController new];
        objPrivacy.isTOS = NO;
        [self.navigationController pushViewController:objPrivacy animated:YES];
        return NO;
    }
    else if (([[request.URL absoluteString] rangeOfString:@"tos"].location != NSNotFound))
    {
        //    https://runlive.fit/tos
        TermsAndPrivacyPoilicyViewController *objTOS = [TermsAndPrivacyPoilicyViewController new];
        objTOS.isTOS = YES;
        [self.navigationController pushViewController:objTOS animated:YES];
            return NO;
    }
    

//    [[UIApplication sharedApplication] openURL:request.URL];
    return YES;
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
