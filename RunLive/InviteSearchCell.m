//
//  InviteSearchCell.m
//  RunLive
//
//  Created by mac-00012 on 11/2/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "InviteSearchCell.h"

@implementation InviteSearchCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];

    self.imgFreind.layer.cornerRadius = CGRectGetHeight(self.imgFreind.frame)/2;
    self.viewSelect.layer.borderColor = CRGB(71, 216, 253).CGColor;
    self.viewSelect.layer.borderWidth = 2;
    self.viewSelect.layer.cornerRadius = CGRectGetHeight(self.viewSelect.frame)/2;
    self.viewSelectInner.layer.cornerRadius = CGRectGetHeight(self.viewSelectInner.frame)/2;
}

@end
