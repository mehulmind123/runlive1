//
//  InviteFriendHeaderView.h
//  RunLive
//
//  Created by mac-0006 on 16/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendHeaderView : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *lblHeaderTitle;

@end
