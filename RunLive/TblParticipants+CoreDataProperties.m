//
//  TblParticipants+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 05/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblParticipants+CoreDataProperties.h"

@implementation TblParticipants (CoreDataProperties)

+ (NSFetchRequest<TblParticipants *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblParticipants"];
}

@dynamic country;
@dynamic fb_id;
@dynamic first_name;
@dynamic last_name;
@dynamic picture;
@dynamic priority_index;
@dynamic user_id;
@dynamic user_name;
@dynamic celebrity;
@dynamic chatobject;

@end
