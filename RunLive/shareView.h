//
//  shareView.h
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface shareView : UIView

@property IBOutlet UICollectionView *collectionPeople;
@property IBOutlet UIButton *btnSearch,*btnClose,*btnFriends,*btnSocialMedia,*btnShareWithFr;
@property IBOutlet UIButton *btnFb,*btnInsta,*btnTwit,*btnMail;
@property IBOutlet UIPageControl *pageConrol;
@property IBOutlet NSLayoutConstraint *viewlineX;
@property IBOutlet UITextField *txtSearch;
@property IBOutlet UIView *viewFriends,*viewSocialMedia,*viewSearch;
@property CGFloat viewY;
@property NSMutableArray *arrSelectedUser;
@property NSArray *arrayData;

+(id)GetShareView;

-(void)getFriendListFromServer;

- (void)sharePostInChatGroup:(UIImage *)image VideoPost:(BOOL)isVideoPost VideoURL:(NSString *)strVideoURL;

@end
