//
//  SelectMusicViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SelectMusicViewController.h"
#import "AlbumListViewController.h"

@interface SelectMusicViewController ()

@end

@implementation SelectMusicViewController
{
    SPTAuthViewController *authViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [viewPlaylistContainer hideByHeight:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [appDelegate hideTabBar];

    SPTAuth* auth = [SPTAuth defaultInstance];
    if([appDelegate isSpotifySessionValid:auth])
        lblSpotifyconnected.text = @"Connected";
    else
        lblSpotifyconnected.text = @"Not Connected";
    
    [self setMusicRelatedData];
}

-(void)setMusicRelatedData
{
    NSArray *arrMusic = [TblMusicList fetchAllObjects];
    
    if (arrMusic.count > 0)
    {
        [viewPlaylistContainer hideByHeight:NO];
        TblMusicList *objMusic = (TblMusicList *)[arrMusic objectAtIndex:0];
        lblPlaylistName.text = objMusic.playlist_name;
        if (objMusic.isApple.boolValue)
        {
            //Apple playlist
            [lblFollowers hideByHeight:YES];
            NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:[[appDelegate createAlbumImagePath] stringByAppendingPathComponent:objMusic.album_image]], 0.9)];
            imgPlaylist.image = [UIImage imageWithData:imageData];
            imgPlaylistType.image = [UIImage imageNamed:@"ic_apple"];
        }
        else
        {
            // Spotify playlist
            lblFollowers.text =  objMusic.followers.integerValue == 1 ? [NSString stringWithFormat:@"%@ follower", objMusic.followers] : [NSString stringWithFormat:@"%@ followers", objMusic.followers];
            imgPlaylistType.image = [UIImage imageNamed:@"ic_spotify_green_small"];
            [imgPlaylist setImageWithURL:[NSURL URLWithString:objMusic.album_image] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [lblFollowers hideByHeight:NO];
        }
    }
    else
        [viewPlaylistContainer hideByHeight:YES];
}

#pragma mark - Spotify Delegates

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didFailToLogin:(NSError *)error
{
    NSLog(@"*** Failed to log in: %@", error);
}

- (void)authenticationViewControllerDidCancelLogin:(SPTAuthViewController *)authenticationViewController
{
    NSLog(@"%@",@"Login cancelled.");
}

- (void)authenticationViewController:(SPTAuthViewController *)viewcontroller didLoginWithSession:(SPTSession *)session
{
    AlbumListViewController *objAlbum = [[AlbumListViewController alloc] init];
    objAlbum.isApple = NO;
    [self.navigationController pushViewController:objAlbum animated:YES];
}

#pragma mark - Action Event

-(IBAction)btnAppleMusicCLK:(id)sender
{
    AlbumListViewController *objAlbum = [[AlbumListViewController alloc] init];
    objAlbum.isApple = YES;
    [self.navigationController pushViewController:objAlbum animated:YES];
}

-(IBAction)btnSpotifyCLK:(id)sender
{
    SPTAuth* auth = [SPTAuth defaultInstance];
    
    if([appDelegate isSpotifySessionValid:auth])
    {
        [auth renewSession:auth.session callback:^(NSError *error, SPTSession *session)
         {
             if (!error)
             {
                 AlbumListViewController *objAlbum = [[AlbumListViewController alloc] init];
                 objAlbum.isApple = NO;
                 [self.navigationController pushViewController:objAlbum animated:YES];
             }
         }];
    }
    else
    {
        authViewController = [SPTAuthViewController authenticationViewController];
        authViewController.delegate = self;
        authViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        authViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        self.definesPresentationContext = YES;
        
        [self presentViewController:authViewController animated:NO completion:nil];
    }
}

- (IBAction)btnNoMusicCLK:(id)sender
{
    appDelegate.isMusicSelected = YES;
    [TblMusicList deleteAllObjects];
    [appDelegate deleteSongImagePath];
    [appDelegate deleteAlbumImagePath];
    [self.navigationController popViewControllerAnimated:NO];
    
    [CUserDefaults removeObjectForKey:CAppleMusicCollection];
    [CUserDefaults synchronize];
    
    [appDelegate.appleMPMusicPlayerController pause];
    appDelegate.appleMPMusicPlayerController = nil;
    
    SPTAuthViewController *authvc = [SPTAuthViewController authenticationViewController];
    // Clear the cookies before showing the login view
    [authvc clearCookies:^() {
        SPTAuth* auth = [SPTAuth defaultInstance];
        auth.session = nil;
        auth = nil;
    }];

}


- (IBAction)btnBackCLK:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
