//
//  TimeAndDateViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^getTimeDate)(NSString *strTimeDate,BOOL isTime,NSString *strDateWithTime);

@interface TimeAndDateViewController : SuperViewController
{
    IBOutlet UITableView *tblDay,*tblMonth;
    IBOutlet UILabel *lblTitleDay,*lblTitleMonth,*lblAM,*lblPM;
    IBOutlet UIButton *btnAM,*btnPM;
}

@property(copy,nonatomic) getTimeDate configureGetTimeDate;
@property(atomic,assign) BOOL isTime;
@property(strong,nonatomic) NSString *strSelectedTime;

@end
