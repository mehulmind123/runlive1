//
//  SessionTypeViewController.m
//  RunLive
//
//  Created by mac-00012 on 11/19/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionTypeViewController.h"
#import "InviteFriendCell.h"
#import "SessionTypeUser.h"
#import "SessionTypeEvenUser.h"
#import "SessionDrag.h"
#import "CreateSessionViewController.h"



@interface SessionTypeViewController ()

@end

@implementation SessionTypeViewController
{
    SessionDrag *_draggedCard;
    InviteFriendCell *SelectedCell;
    BOOL isDragPressTeamRed;
    
    NSMutableArray *arrTeamRedCell,*arrTeamBlueCell;
    NSIndexPath *selectedDragIndexPath;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [clFriends registerNib:[UINib nibWithNibName:@"InviteFriendCell" bundle:nil] forCellWithReuseIdentifier:@"InviteFriendCell"];
    [clTeamRed registerNib:[UINib nibWithNibName:@"InviteFriendCell" bundle:nil] forCellWithReuseIdentifier:@"InviteFriendCell"];
    [clTeamBlue registerNib:[UINib nibWithNibName:@"InviteFriendCell" bundle:nil] forCellWithReuseIdentifier:@"InviteFriendCell"];

    [self btnTeamTypeCLK:btnSolo];
    
    viewBottom.layer.masksToBounds = NO;
    viewBottom.layer.shadowRadius = 8;
    viewBottom.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewBottom.layer.shadowOffset = CGSizeMake(0, -3);
    viewBottom.layer.shadowOpacity = .5f;
    
    
// Add login user in team
    /*
     "_id" = 585bb84d7fea3e444eb7d854;
     country = US;
     "fb_id" = 1795594987375350;
     "first_name" = "";
     "last_name" = "";
     picture = "http://67.227.155.244:8088/user/avatar/585bb84d7fea3e444eb7d854";
     "user_name" = bhumigoklani;
     */

    NSMutableDictionary *dicUser = [NSMutableDictionary new];
    [dicUser addObject:appDelegate.loginUser.user_id forKey:@"_id"];
    [dicUser addObject:appDelegate.loginUser.country forKey:@"country"];
    [dicUser addObject:appDelegate.loginUser.fb_id forKey:@"fb_id"];
    [dicUser addObject:appDelegate.loginUser.first_name forKey:@"first_name"];
    [dicUser addObject:appDelegate.loginUser.last_name forKey:@"last_name"];
    [dicUser addObject:appDelegate.loginUser.picture forKey:@"picture"];
    [dicUser addObject:appDelegate.loginUser.user_name forKey:@"user_name"];
    [_arrFriends addObject:dicUser];
    
    
// Add Gesture in Team Blue collection View
    arrTeamBlueCell = [NSMutableArray new];
    [self setUpGesturesForTeamBlue];
    
// Add Gesture in Team Red collection View
    arrTeamRedCell = [NSMutableArray new];
    [self setUpGesturesForTeamRed];
    
    [self addFreindForTeam];
    
    [self initDraggedCardView];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanInMainView:)];
    panGesture.delegate = self;
    [viewTeam addGestureRecognizer:panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    btnTeam.layer.cornerRadius = btnSolo.layer.cornerRadius = btnOk.layer.cornerRadius = 3;
}

#pragma mark - Common Fuction
#pragma mark

-(void)addFreindForTeam
{
    for (int i = 0; _arrFriends.count > i; i++)
    {
        
        if (_arrFriends.count / 2 <= i)
            [arrTeamRedCell addObject:_arrFriends[i]];
        else
            [arrTeamBlueCell addObject:_arrFriends[i]];
    }
    
    [clTeamRed reloadData];
    [clTeamBlue reloadData];
}


#pragma mark - Drag Common Function
- (void)initDraggedCardView
{
    _draggedCard = [SessionDrag viewFromXib];
    _draggedCard.hidden = YES;
    [viewTeam addSubview:_draggedCard];
}

- (void)setSelectedModel:(InviteFriendCell *)cell atPoint:(CGPoint)point
{
    SelectedCell = cell;
    
    if (cell != nil)
    {
        _draggedCard.imgUser.image = cell.imgUser.image;
        _draggedCard.lblName.text = cell.lblUserName.text;
        _draggedCard.lblCity.text = cell.lblCity.text;
        _draggedCard.imgVerifyUser.hidden = cell.imgVerifiedUser.hidden;
        [_draggedCard addBorderColor:isDragPressTeamRed];
        
        _draggedCard.center = point;
        _draggedCard.hidden = NO;
        [self updateCardViewDragState:[self isValidDragPoint:point]];
    }
    else
    {
        _draggedCard.hidden = YES;
    }
}

- (BOOL)isValidDragPoint:(CGPoint)point
{
    if (isDragPressTeamRed)
        return point.x > CScreenWidth/2;
    else
            return point.x < CScreenWidth/2;

    return YES;
}

- (void)updateCardViewDragState:(BOOL)validDropPoint
{
        if (validDropPoint)
            _draggedCard.alpha = 1.0f;
        else
            _draggedCard.alpha = 0.8f;
}



#pragma mark - Team Blue Drag and Drop

- (void)setUpGesturesForTeamBlue
{
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlePressInTeamBlue:)];
    longPressGesture.numberOfTouchesRequired = 1;
    longPressGesture.minimumPressDuration    = 0.1f;
    [clTeamBlue addGestureRecognizer:longPressGesture];
}

- (void)handlePressInTeamBlue:(UILongPressGestureRecognizer *)gesture
{
    isDragPressTeamRed = NO;
    CGPoint point = [gesture locationInView:clTeamBlue];

    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        NSIndexPath *indexPath = [clTeamBlue indexPathForItemAtPoint:point];
        if (indexPath != nil)
        {
            InviteFriendCell *cell = (InviteFriendCell *)[clTeamBlue cellForItemAtIndexPath:indexPath];
            point = [gesture locationInView:clTeamBlue];
            [self setSelectedModel:cell atPoint:point];
        }
        
        CGPoint touchPoint = [gesture locationInView:viewTeam];
        _draggedCard.center = touchPoint;
        selectedDragIndexPath = indexPath;
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        _draggedCard.hidden = YES;
        
        NSIndexPath *indexPath = [clTeamBlue indexPathForItemAtPoint:point];
        
        NSLog(@"get cell at indexpath%@",indexPath);
    }

}

#pragma mark - Team Red Drag and Drop

- (void)setUpGesturesForTeamRed
{
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlePressInTeamRed:)];
    longPressGesture.numberOfTouchesRequired = 1;
    longPressGesture.minimumPressDuration    = 0.1f;
    [clTeamRed addGestureRecognizer:longPressGesture];
}

- (void)handlePressInTeamRed:(UILongPressGestureRecognizer *)gesture
{
    isDragPressTeamRed = YES;
    CGPoint point = [gesture locationInView:clTeamRed];
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        _draggedCard.center = point;
        
        NSIndexPath *indexPath = [clTeamRed indexPathForItemAtPoint:point];
        if (indexPath != nil)
        {
            InviteFriendCell *cell = (InviteFriendCell *)[clTeamRed cellForItemAtIndexPath:indexPath];
            point = [gesture locationInView:clTeamRed];
            [self setSelectedModel:cell atPoint:point];
        }
        
        CGPoint touchPoint = [gesture locationInView:viewTeam];
        _draggedCard.center = touchPoint;
        selectedDragIndexPath = indexPath;
    }

    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        _draggedCard.hidden = YES;
    }
}

#pragma mark - Main View Gesture
- (void)handlePanInMainView:(UIPanGestureRecognizer *)gesture
{
    CGPoint touchPoint = [gesture locationInView:viewTeam];
    
    if (gesture.state == UIGestureRecognizerStateChanged && !_draggedCard.hidden)
    {
        _draggedCard.hidden = NO;
        _draggedCard.center = touchPoint;
        [self updateCardViewDragState:[self isValidDragPoint:touchPoint]];
    }
    else if (gesture.state == UIGestureRecognizerStateRecognized && SelectedCell != nil)
    {
        _draggedCard.hidden = YES;
        
        BOOL validDropPoint = [self isValidDragPoint:touchPoint];
        
        if (isDragPressTeamRed)
        {
            // Code for red Team.....
            if (validDropPoint)
            {
//                NSIndexPath *indexPath = [clTeamRed indexPathForItemAtPoint:touchPoint];
                NSDictionary *dicData = [arrTeamRedCell objectAtIndex:selectedDragIndexPath.item];
                [arrTeamRedCell removeObjectAtIndex:selectedDragIndexPath.item];
                [arrTeamBlueCell addObject:dicData];
                
                [clTeamRed reloadData];
                [clTeamBlue reloadData];
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:arrTeamBlueCell.count-1 inSection:0];
                [clTeamBlue scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
            }
            SelectedCell = nil;
        }
        else
        {
            // Code for Blue Team......
            if (validDropPoint)
            {
                NSDictionary *dicData = [arrTeamBlueCell objectAtIndex:selectedDragIndexPath.item];
                [arrTeamBlueCell removeObjectAtIndex:selectedDragIndexPath.item];
                [arrTeamRedCell addObject:dicData];
                
                [clTeamRed reloadData];
                [clTeamBlue reloadData];
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:arrTeamRedCell.count-1 inSection:0];
                [clTeamRed scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
            }
            SelectedCell = nil;
        }
    }
    
    [gesture setTranslation:CGPointMake(0, 0) inView:viewTeam];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - Collection View Delegate
#pragma mark -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:clFriends])
        return self.arrFriends.count;
    else if ([collectionView isEqual:clTeamRed])
            return arrTeamRedCell.count;
    else
        return arrTeamBlueCell.count;
            
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"InviteFriendCell";
    InviteFriendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.viewSelect.hidden = YES;

    cell.backgroundColor = cell.contentView.backgroundColor = [UIColor clearColor];
    
    if ([collectionView isEqual:clTeamRed])
    {
        NSDictionary *dicData = [arrTeamRedCell objectAtIndex:indexPath.item];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.viewCircle.layer.borderColor = [UIColor redColor].CGColor;
        
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    }
    else if ([collectionView isEqual:clTeamBlue])
    {
        NSDictionary *dicData = [arrTeamBlueCell objectAtIndex:indexPath.item];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.viewCircle.layer.borderColor = CRGB(62, 205, 252).CGColor;
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
    }
    else
    {
        NSDictionary *dicData = [_arrFriends objectAtIndex:indexPath.item];
        cell.lblUserName.text = [dicData stringValueForJSON:@"user_name"];
        cell.lblCity.text = [dicData stringValueForJSON:@"country"];
        [cell.imgUser setImageWithURL:[appDelegate resizeImage:@"122" Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.imgVerifiedUser.hidden = [[dicData numberForJson:@"celebrity"] isEqual:@0];
        cell.viewCircle.layer.borderColor = CRGB(235, 241, 245).CGColor;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:clFriends])
        return CGSizeMake(CScreenWidth/4 , 126);

    return CGSizeMake(CGRectGetWidth(clTeamBlue.frame)/2 , 126);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
}

#pragma mark - Alert Functions

-(void)showAlertForUser:(BOOL)isSolo
{
    SessionTypeUser *objSessionType = [SessionTypeUser viewFromXib];
    objSessionType.lblText.text = isSolo ? @"You can have solo session with only 9 other users." : @"You can have team session with only 19 other users.";
    
    [self presentViewOnPopUpViewController:objSessionType shouldClickOutside:NO dismissed:nil];
    
    [objSessionType.btnCancel touchUpInsideClicked:^{
        [self dismissOverlayViewControllerAnimated:YES completion:nil];
    }];
    
    [objSessionType.btnGoBack touchUpInsideClicked:^{
        [self.navigationController popViewControllerAnimated:YES];
        [objSessionType.btnCancel sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];

}

#pragma mark - Api Function
-(void)InviteFriendForSession
{
    for (int i = 0; [self.navigationController viewControllers].count > i; i++)
    {
        UIViewController *viewController = [self.navigationController viewControllers][i];
        
        if ([viewController isKindOfClass:[CreateSessionViewController class]])
        {
            [self dismissOverlayViewControllerAnimated:YES completion:nil];
            
            NSMutableDictionary *dicServer = [NSMutableDictionary new];
            
            if (btnSolo.selected)
            {
                [dicServer addObject:_arrFriends forKey:@"solo"];
            }
            else
            {
                [dicServer addObject:arrTeamRedCell forKey:@"team_red"];
                [dicServer addObject:arrTeamBlueCell forKey:@"team_blue"];
            }
            
            CreateSessionViewController *objCre = (CreateSessionViewController *)viewController;
            objCre.dicSelectedFriend = dicServer;
            [self.navigationController popToViewController:objCre animated:YES];
            break;
        }
    }
}

#pragma mark - Action Event
-(IBAction)btnTeamTypeCLK:(UIButton *)sender
{
    btnSolo.selected = btnTeam.selected = NO;
    btnSolo.backgroundColor = btnTeam.backgroundColor = CRGB(203, 211, 212);
    viewTeam.hidden = clFriends.hidden = YES;
    
    switch (sender.tag)
    {
        case 0:
        {
            btnSolo.selected = YES;
            clFriends.hidden = NO;
            btnSolo.backgroundColor = CRGB(61, 205, 252);
        }
            break;
        case 1:
        {
            btnTeam.selected = YES;
            viewTeam.hidden = NO;
            btnTeam.backgroundColor = CRGB(61, 205, 252);
        }
            break;
            
        default:
            break;
    }
}

-(IBAction)btnOK:(id)sender
{
    if (btnSolo.selected)
    {
        // Check Max 09 user...
        if (_arrFriends.count > 10)
            [self showAlertForUser:btnSolo.selected];
        else if (_arrFriends.count < 1) // Check Min 1 user...
            [self customAlertViewWithOneButton:@"" Message:@"Please select at least one friend to invite." ButtonText:@"OK" Animation:YES completed:nil];
        else
            [self InviteFriendForSession];
    }
    else
    {
        // Check Max 19 user...
        if (arrTeamRedCell.count < 2)
            [self customAlertViewWithOneButton:@"" Message:@"For team session, there needs to be at least 2 user in each team." ButtonText:@"OK" Animation:YES completed:nil];
        else if (arrTeamBlueCell.count < 2)
            [self customAlertViewWithOneButton:@"" Message:@"For team session, there needs to be at least 2 user in each team." ButtonText:@"OK" Animation:YES completed:nil];
        else if (_arrFriends.count < 4) // Check Min 3 user...
            [self customAlertViewWithOneButton:@"" Message:@"Please select at least 4 friends to invite." ButtonText:@"OK" Animation:YES completed:nil];
        else if (_arrFriends.count > 20)
            [self showAlertForUser:btnSolo.selected];
        else
        {
            if (arrTeamBlueCell.count != arrTeamRedCell.count)
            {
                SessionTypeEvenUser *objEven = [SessionTypeEvenUser viewFromXib];
                [self presentViewOnPopUpViewController:objEven shouldClickOutside:NO dismissed:nil];
                
                [objEven.btnProceed touchUpInsideClicked:^{
                    [self InviteFriendForSession];
                }];
                
                [objEven.btnGoBack touchUpInsideClicked:^{
                    [self.navigationController popViewControllerAnimated:YES];
                    [self dismissOverlayViewControllerAnimated:YES completion:nil];
                }];
            }
            else
                [self InviteFriendForSession];
        }
    }
}

@end
