//
//  CustomOneButtonAlertView.h
//  RunLive
//
//  Created by mac-0005 on 13/02/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomOneButtonAlertView : UIView
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblMessage;
    
}

@property(strong,nonatomic) IBOutlet UIButton *btnMain;
@property(strong,nonatomic) IBOutlet UIView *viewContainer;

+(id)initCustomOneButtonAlertView;

-(void)alertView:(NSString *)strTitle Message:(NSString *)strMessage ButtonTitle:(NSString *)strBtnTitle;


@end
