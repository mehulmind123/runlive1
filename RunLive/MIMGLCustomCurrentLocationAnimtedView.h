//
//  MIMGLCustomCurrentLocationAnimtedView.h
//  RunLive
//
//  Created by mac-0005 on 16/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <Mapbox/Mapbox.h>

@interface MIMGLCustomCurrentLocationAnimtedView : MGLAnnotationView
@property(weak,nonatomic) IBOutlet UIImageView *imgUserPosition;
@property(weak,nonatomic) IBOutlet UIView *viewAnimation;

@end
