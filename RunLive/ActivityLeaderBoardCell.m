//
//  ActivityLeaderBoardCell.m
//  RunLive
//
//  Created by mac-00012 on 10/28/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "ActivityLeaderBoardCell.h"

@implementation ActivityLeaderBoardCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];

}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = 1;
    _viewUser.layer.borderColor = CRGB(235, 241, 245).CGColor;
}

@end
