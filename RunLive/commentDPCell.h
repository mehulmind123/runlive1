//
//  commentDPCell.h
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface commentDPCell : UITableViewCell

@property IBOutlet TTTAttributedLabel *lblContent;

@property IBOutlet UILabel *lblTime;
@property IBOutlet UIImageView *imgDP;
@property IBOutlet UIView *vwBorder;

@property(weak,nonatomic) IBOutlet UIButton *btnUser;


@end
