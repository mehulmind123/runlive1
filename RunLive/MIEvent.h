//
//  MIEvent.h
//  Master
//
//  Created by mac-0001 on 04/05/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MIEvent : NSObject

@property(nonatomic, copy) NSString *eventId;

@property(nonatomic, copy) NSDate *startDate;       // GMT Timezone

@property(nonatomic, copy) NSDate *endDate;         // GMT Timezone


@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *location;
@property(nonatomic, copy) NSString *notes;
@property(nonatomic, copy) NSURL *URL;


@end
