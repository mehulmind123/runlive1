//
//  TblActivityDates+CoreDataProperties.h
//  RunLive
//
//  Created by mac-00012 on 7/26/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblActivityDates+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TblActivityDates (CoreDataProperties)

+ (NSFetchRequest<TblActivityDates *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *activity_date;
@property (nullable, nonatomic, copy) NSNumber *activity_filter_type;
@property (nullable, nonatomic, copy) NSNumber *activity_type;
@property (nullable, nonatomic, copy) NSString *show_date;
@property (nullable, nonatomic, copy) NSNumber *timestamp;
@property (nullable, nonatomic, copy) NSString *total_month_distance;
@property (nullable, nonatomic, copy) NSString *total_month_run;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nullable, nonatomic, copy) NSString *user_name;
@property (nullable, nonatomic, retain) NSSet<TblActivityCompleteSession *> *objActivityCompleteSession;
@property (nullable, nonatomic, retain) NSSet<TblActivityPhoto *> *objActivityPhoto;
@property (nullable, nonatomic, retain) NSSet<TblActivityRunSession *> *objActivityRunSession;
@property (nullable, nonatomic, retain) NSSet<TblActivitySyncSession *> *objActivitySyncSession;
@property (nullable, nonatomic, retain) NSSet<TblActivityVideo *> *objActivityVideo;

@end

@interface TblActivityDates (CoreDataGeneratedAccessors)

- (void)addObjActivityCompleteSessionObject:(TblActivityCompleteSession *)value;
- (void)removeObjActivityCompleteSessionObject:(TblActivityCompleteSession *)value;
- (void)addObjActivityCompleteSession:(NSSet<TblActivityCompleteSession *> *)values;
- (void)removeObjActivityCompleteSession:(NSSet<TblActivityCompleteSession *> *)values;

- (void)addObjActivityPhotoObject:(TblActivityPhoto *)value;
- (void)removeObjActivityPhotoObject:(TblActivityPhoto *)value;
- (void)addObjActivityPhoto:(NSSet<TblActivityPhoto *> *)values;
- (void)removeObjActivityPhoto:(NSSet<TblActivityPhoto *> *)values;

- (void)addObjActivityRunSessionObject:(TblActivityRunSession *)value;
- (void)removeObjActivityRunSessionObject:(TblActivityRunSession *)value;
- (void)addObjActivityRunSession:(NSSet<TblActivityRunSession *> *)values;
- (void)removeObjActivityRunSession:(NSSet<TblActivityRunSession *> *)values;

- (void)addObjActivitySyncSessionObject:(TblActivitySyncSession *)value;
- (void)removeObjActivitySyncSessionObject:(TblActivitySyncSession *)value;
- (void)addObjActivitySyncSession:(NSSet<TblActivitySyncSession *> *)values;
- (void)removeObjActivitySyncSession:(NSSet<TblActivitySyncSession *> *)values;

- (void)addObjActivityVideoObject:(TblActivityVideo *)value;
- (void)removeObjActivityVideoObject:(TblActivityVideo *)value;
- (void)addObjActivityVideo:(NSSet<TblActivityVideo *> *)values;
- (void)removeObjActivityVideo:(NSSet<TblActivityVideo *> *)values;

@end

NS_ASSUME_NONNULL_END
