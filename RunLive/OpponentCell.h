//
//  OpponentCell.h
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpponentCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lblUserName,*lblRuns;
@property(weak,nonatomic) IBOutlet UIView *viewUser;
@property(weak,nonatomic) IBOutlet UIImageView *imgUser,*imgRank,*imgVeriefiedUser;
@end
