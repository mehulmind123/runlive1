//
//  TblActivityPhoto+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 07/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblActivityPhoto+CoreDataProperties.h"

@implementation TblActivityPhoto (CoreDataProperties)

+ (NSFetchRequest<TblActivityPhoto *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblActivityPhoto"];
}

@dynamic act_image;
@dynamic activity_id;
@dynamic activity_main_type;
@dynamic activity_post_time;
@dynamic activity_text;
@dynamic caption;
@dynamic comments;
@dynamic img_hieght;
@dynamic img_width;
@dynamic is_like;
@dynamic isAdminActivity;
@dynamic session_id;
@dynamic session_name;
@dynamic timestamp;
@dynamic total_comments;
@dynamic total_likes;
@dynamic user_id;
@dynamic user_image;
@dynamic user_name;
@dynamic is_commented;
@dynamic objActivityDate;

@end
