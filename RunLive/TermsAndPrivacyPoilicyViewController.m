//
//  TermsAndPrivacyPoilicyViewController.m
//  RunLive
//
//  Created by mac-0005 on 25/01/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import "TermsAndPrivacyPoilicyViewController.h"

@interface TermsAndPrivacyPoilicyViewController ()

@end

@implementation TermsAndPrivacyPoilicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_isTOS)
    {
        lblTitle.text = @"Terms of Service";
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://runlive.fit/tos/"]]];
    }
    else
    {
        lblTitle.text = @"Privacy Policy";
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://runlive.fit/privacypolicy/"]]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
