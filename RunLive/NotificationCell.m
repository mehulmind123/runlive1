//
//  NotificationCell.m
//  RunLive
//
//  Created by mac-0006 on 21/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell
@synthesize vwBorder,imgDP;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [appDelegate roundedImageView:imgDP withSuperView:vwBorder];
    vwBorder.layer.borderColor = [CImageOuterBorderColor CGColor];
    vwBorder.layer.borderWidth = 1;

    self.lblContent.preferredMaxLayoutWidth = CGRectGetWidth(self.lblContent.frame);
}
@end
