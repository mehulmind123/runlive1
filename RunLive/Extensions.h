//
//  Exetensions.h
//  Project
//
//  Created by mac-00015 on 8/4/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#ifndef Extensions_h
    #define Extensions_h

#endif /* Extensions_h */

#import "UIView+AutoLayoutConstraints.h"
#import "NSObject+Extension.h"
#import "UIApplication+Extension.h"
#import "NSObject+NewProperty.h"
#import "DelegateObserver.h"
#import "NSDate+MIExtension.h"
#import "NSDateFormatter+DateExtension.h"
#import "NSString+MIExtension.h"
#import "UIView+MIExtension.h"
#import "UITextField+MIExtension.h"
#import "UITextView+MIExtension.h"
#import "NSDictionary+MIExtension.h"
#import "UITableView+Extension.h"




#import "UIViewController+Helper.h"
#import "UIViewController+InAppPurchase.h"
#import "UIViewController+MessageMailComposerViewControllers.h"
#import "UIButton+EventHandler.h"
#import "UIAlertView+EventHandler.h"
#import "UIView+ScreenShot.h"
#import "UIViewController+ScreenShot.h"

#import "UIViewController+LoaderAndAlerts.h"
#import "UINavigationController+Extension.h"
#import "UIActionSheet+EventHandler.h"
#import "UIAlertController+Extension.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"


#import "UIImage+Extension.h"
#import "UIBarButtonItem+Extension.h"




#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    #import "UIAlertController+Extension.h"
#endif
