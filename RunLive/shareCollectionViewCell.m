//
//  shareCollectionViewCell.m
//  RunLive
//
//  Created by mac-0004 on 27/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "shareCollectionViewCell.h"

@implementation shareCollectionViewCell
@synthesize imgProfile,viewOuter,lblName,lblSubName,viewBlue;
- (void)awakeFromNib {
    [super awakeFromNib];
    

    // Initialization code
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self layoutIfNeeded];
    
    viewBlue.layer.cornerRadius = viewBlue.frame.size.width/2;
    viewBlue.clipsToBounds = true;
    
    viewOuter.layer.cornerRadius = viewOuter.frame.size.width/2;
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = true;
    viewOuter.clipsToBounds = true;
}
@end
