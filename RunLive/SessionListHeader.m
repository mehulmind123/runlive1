//
//  SessionListHeader.m
//  RunLive
//
//  Created by mac-00012 on 11/17/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionListHeader.h"

@implementation SessionListHeader

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgTag.layer.cornerRadius = CGRectGetHeight(_imgTag.frame)/2;
}

@end
