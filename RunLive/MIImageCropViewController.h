//
//  MIImageCropViewController.h
//  RunLive
//
//  Created by mac-0005 on 19/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIImageCropScrollView.h"
@protocol MIImageCustomCropDelegate
- (void)finishImageCroping:(UIImage *)image didCancel:(BOOL)cancel;
@end

@interface MIImageCropViewController : UIViewController
{
    IBOutlet MIImageCropScrollView *scrollView;
    IBOutlet UIButton *btnZoom,*btnCancel,*btnCrop;
}

@property(nonatomic, strong) id<MIImageCustomCropDelegate> delegate;

- (id)initWithImage:(UIImage *)originalImage withDelegate:(id<MIImageCustomCropDelegate>)delegate;
@end
