//
//  TblSessionList+CoreDataClass.m
//  RunLive
//
//  Created by mac-0005 on 29/11/17.
//  Copyright © 2017 mindinventory. All rights reserved.
//

#import "TblSessionList+CoreDataClass.h"
#import "TblJoinedUser+CoreDataClass.h"

#import "TblSessionListDate+CoreDataClass.h"

#import "TblSessionOtherUser+CoreDataClass.h"

#import "TblSessionSyncedUser+CoreDataClass.h"

@implementation TblSessionList

@end
