//
//  SelectedSessionUserCell.h
//  RunLive
//
//  Created by mac-00012 on 11/16/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedSessionUserCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIView *viewUser,*viewContainer;
@property(weak,nonatomic) IBOutlet UIImageView *imgUser;
@property(weak,nonatomic) IBOutlet UIButton *btnAdd;

@end
