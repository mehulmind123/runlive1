//
//  shareView.m
//  RunLive
//
//  Created by mac-0004 on 26/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "shareView.h"
#import "shareCollectionViewCell.h"

@implementation shareView
{
    NSURLSessionDataTask *taskRuning;
}
@synthesize collectionPeople,btnFriends,btnSearch,btnClose,btnSocialMedia,btnShareWithFr,arrayData,pageConrol,btnFb,btnMail,btnTwit,btnInsta,arrSelectedUser;

-(void)awakeFromNib
{
    [super awakeFromNib];
    [collectionPeople registerNib:[UINib nibWithNibName:@"shareCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"shareCollectionViewCell"];
    
    btnShareWithFr.layer.cornerRadius = 3;
    btnInsta.layer.cornerRadius = btnTwit.layer.cornerRadius = btnMail.layer.cornerRadius = btnFb.layer.cornerRadius = 2;
    
    [_txtSearch addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if(Is_iPhone_5 || Is_iPhone_4 || IS_IPAD)
    {
        [self setButtonInset:btnFb];
        [self setButtonInset:btnInsta];
        [self setButtonInset:btnMail];
        [self setButtonInset:btnTwit];
    }
}

+(id)GetShareView
{
    shareView *customView = [[[NSBundle mainBundle] loadNibNamed:@"shareView" owner:nil options:nil] lastObject];
    
    if ([customView isKindOfClass:[shareView class]])
    {
        customView.layer.cornerRadius = 2.0f;
        
//        CViewSetWidth(customView, (CScreenWidth * 324)/375);
        
        return customView;
    }
    else
        return nil;
}

#pragma mark - Textfield Methods
-(void)textfieldDidChange:(UITextField *)txt
{
    if (_txtSearch.text.length == 0)
        [_txtSearch resignFirstResponder];
    
    [self getFriendListFromServer];
}


#pragma mark - Api Functions


-(void)getFriendListFromServer
{
    if (taskRuning && taskRuning.state == NSURLSessionTaskStateRunning)
        [taskRuning cancel];

    taskRuning = [[APIRequest request] friendList:_txtSearch.text loader:NO completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             NSMutableArray *arrUser = [NSMutableArray new];
             
             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"] isKindOfClass:[NSArray class]])
                 [arrUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"frequently"]];
             
             if ([[[responseObject valueForKey:CJsonData] valueForKey:@"other"] isKindOfClass:[NSArray class]])
                 [arrUser addObjectsFromArray:[[responseObject valueForKey:CJsonData] valueForKey:@"other"]];
             
             arrayData = [arrUser mutableCopy];
             [collectionPeople reloadData];
             
             [pageConrol setNumberOfPages:arrayData.count/5];
             pageConrol.currentPage = 0;
             pageConrol.hidden = arrayData.count/5 > 1 ? NO : YES;
         }
     }];

}


#pragma mark - Collection View Datasource and Delegate methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"shareCollectionViewCell";
    
    shareCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    NSDictionary *dicData = [arrayData objectAtIndex:indexPath.item];
    
    [cell.imgProfile setImageWithURL:[appDelegate resizeImage:[NSString stringWithFormat:@"%f",CViewWidth(cell.imgProfile)] Height:nil imageURL:[dicData stringValueForJSON:@"picture"]] placeholderImage:nil options:SDWebImageRetryFailed usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.lblSubName.text = [dicData stringValueForJSON:@"country"];
    
    NSString *strFirstname = [[dicData stringValueForJSON:@"first_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (!strFirstname || [strFirstname isEqualToString:@""])
        cell.lblName.text = [NSString stringWithFormat:@"%@", [dicData stringValueForJSON:@"user_name"]];
    else
        cell.lblName.text = [NSString stringWithFormat:@"%@ %@", strFirstname,[dicData stringValueForJSON:@"last_name"]];
    
    // to invisible cell
//     cell.alpha = (indexPath.row == [collectionView numberOfItemsInSection:indexPath.section]-1) ? 0 : 1;
    
    if ([arrSelectedUser containsObject:dicData])
    {
        cell.viewOuter.layer.borderColor = [CRGB(117, 218, 246) CGColor];
        cell.viewOuter.layer.borderWidth = 2;
        cell.viewBlue.hidden = false;
    }
    else
    {
        cell.viewOuter.layer.borderColor = [CImageOuterBorderColor CGColor];
        cell.viewOuter.layer.borderWidth = 1;
        cell.viewBlue.hidden = true;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  return CGSizeMake(CScreenWidth/5 , CGRectGetHeight(collectionView.frame));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    shareCollectionViewCell *cell = (shareCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    NSDictionary *dicData = [arrayData objectAtIndex:indexPath.item];
    
    if ([arrSelectedUser containsObject:dicData])
    {
        [arrSelectedUser removeObject:dicData];
        
        cell.viewOuter.layer.borderColor = [CImageOuterBorderColor CGColor];
        cell.viewOuter.layer.borderWidth = 1;
        cell.viewBlue.hidden = true;
    }
    else
    {
        if (!arrSelectedUser) {
            arrSelectedUser = @[].mutableCopy;
        }
        
        [arrSelectedUser addObject:dicData];
        cell.viewOuter.layer.borderColor = [CRGB(117, 218, 246) CGColor];
        cell.viewOuter.layer.borderWidth = 2;
        cell.viewBlue.hidden = false;

    }
}

#pragma mark - Scroll View methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = round(scrollView.contentOffset.x/scrollView.bounds.size.width);
    pageConrol.currentPage = page;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
{
    @try {
        [collectionPeople scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:pageConrol.currentPage * 5 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:true];
    } @catch (NSException *exception) {
        NSLog(@"Opps missed");
    }
    
}

-(void)setButtonInset:(UIButton *)btn
{
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(btn.titleEdgeInsets.top, btn.titleEdgeInsets.left - 10, btn.titleEdgeInsets.bottom, btn.titleEdgeInsets.right)];
    [btn setContentEdgeInsets:UIEdgeInsetsMake(btn.contentEdgeInsets.top, btn.contentEdgeInsets.left - 8, btn.contentEdgeInsets.bottom, btn.contentEdgeInsets.right)];
}


#pragma mark - Chat Related Functions

- (void)sharePostInChatGroup:(UIImage *)image VideoPost:(BOOL)isVideoPost VideoURL:(NSString *)strVideoURL
{
    NSMutableArray *arrIds = [[NSMutableArray alloc] init];
    
    if(arrSelectedUser.count > 1)
    {
        //Todo: Create the group chat on PubNub
        for (int i=0; i<self.arrSelectedUser.count; i++)
        {
            NSDictionary *dicUser = self.arrSelectedUser[i];
            [arrIds addObject:[dicUser stringValueForJSON:@"_id"]];
        }
    }
    else
    {
        //Todo: Create the one-to-one chat on PubNub
        if (self.arrSelectedUser.count > 0)
        {
            NSDictionary *dicUser = self.arrSelectedUser[0];
            [arrIds addObject:[dicUser stringValueForJSON:@"_id"]];
        }
    }
    
    [[APIRequest request] RegisterChannel:@{@"user_ids":arrIds} completed:^(id responseObject, NSError *error)
     {
         //Todo: Get the channed id from the response, subscribe and publish message to PubNub
         
         if([responseObject objectForKey:CJsonData] && [[responseObject objectForKey:CJsonData] isKindOfClass:[NSDictionary class]])
         {
             NSDictionary *dicData = [responseObject objectForKey:CJsonData];
             if([dicData objectForKey:@"channel_id"])
             {
                 NSString *strChannelId = [dicData stringValueForJSON:@"channel_id"];
              
                 [appDelegate.client subscribeToChannels: @[strChannelId] withPresence:YES];
                 [self AddChannelForPubnubPushNotificaiton:strChannelId];
                 
                 if (isVideoPost)
                 {
                     // Share Video Here
                     [self publishMessage:strVideoURL strChannelId:strChannelId imgUrl:@"" imgName:@"" msgType:@"T" imgHeight:@"" imgWidth:@"" VideoPost:YES];
                 }
                 else
                 {
                     [self uploadChatImage:image completed:^(id responseObject, NSError *error)
                      {
                          if ([responseObject objectForKey:CJsonData])
                          {
                              NSDictionary *dicData = [responseObject objectForKey:CJsonData];
                              
                              if ([dicData objectForKey:@"image"])
                              {
                                  NSString *strImageUrl = [dicData stringValueForJSON:@"image"];
                                  NSString *strImageHeight = [dicData stringValueForJSON:@"height"];
                                  NSString *strImageWidth = [dicData stringValueForJSON:@"width"];
                                  [self publishMessage:@"" strChannelId:strChannelId imgUrl:strImageUrl imgName:@"image" msgType:@"I" imgHeight:strImageHeight imgWidth:strImageWidth VideoPost:NO];
                              }
                          }
                      }];
                 }
             }
         }
     }];
}
- (void)uploadChatImage:(UIImage*)image completed:(void(^)(id responseObject, NSError *error))completion
{
    NSLog(@"CALL IMAGE UPLOAD API HERE ..........");
    [[APIRequest request] UploadChatImage:image completed:^(id response1, NSError *error1)
     {
         if (completion)
             completion(response1, error1);
     }];
}


-(void)AddChannelForPubnubPushNotificaiton:(NSString *)strChannelID
{
    if(![CUserDefaults objectForKey:CDeviceTokenPubNub])
        return;
    
    [appDelegate addPushNotificationToPubnubChannels:@[strChannelID]];
}

- (void)publishMessage:(NSString *)strMessage strChannelId:(NSString*)strChannelId imgUrl:(NSString*)imgUrl imgName:(NSString*)imgName msgType:(NSString *)msgType imgHeight:(NSString *)imgheight imgWidth:(NSString *)imgwidth VideoPost:(BOOL)isVideoPost
{
    NSDateFormatter *dateFormatter = [NSDateFormatter initWithDateFormat:@"dd MMMM, yyyy"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *strToday = [dateFormatter stringFromDate:[NSDate date]];
    
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *strTodayTime = [dateFormatter stringFromDate:[NSDate date]];
    
    NSNumber *iCurrentTimestamp = [NSNumber numberWithInteger:[NSDate currentTimestampInteger]];
    
    NSDictionary *dicMessage = @{
                                 @"msgtext": strMessage,
                                 @"channelId": strChannelId,
                                 @"userid":appDelegate.loginUser.user_id,
                                 @"userimgurl":appDelegate.loginUser.picture,
                                 @"msgdate":strToday,
                                 @"timestamp": iCurrentTimestamp,
                                 @"imgurl":imgUrl,
                                 @"imgname":imgName,
                                 @"imgheight":imgheight,
                                 @"imgwidth":imgwidth,
                                 @"is_video":isVideoPost ? @"1" : @"0",
                                 @"msgtype":msgType,
                                 @"uuid":appDelegate.myConfig.uuid,
                                 @"user_name":appDelegate.loginUser.user_name,
                                 @"msgdisplaytime":strTodayTime
                                 };
    
    NSDictionary *payloads = @{@"pn_apns":
                                   @{
                                       @"aps" :
                                           @{
                                               @"alert": [NSString stringWithFormat:@"%@ messaged you", appDelegate.loginUser.user_name],
                                               @"badge": @1,
                                               @"channel_id":strChannelId,
                                               @"user_id":appDelegate.loginUser.user_id,
                                               @"type":CNotificationTypePubNub,
                                               }
                                       }};
    
    [appDelegate.client publish:dicMessage toChannel:strChannelId mobilePushPayload:payloads storeInHistory:YES withCompletion:^(PNPublishStatus * _Nonnull status)
     {
         
         if (!status.isError)
         {
             NSLog(@"Message sent at TT: %@", status.data.timetoken);
             
             //TODO: Use the timetoken(timestamp) for the further information - Message sent successfully
             
             NSLog(@"Fetched objects are:: %@", [TblMessages fetch:[NSPredicate predicateWithFormat:@"channelId == %@", strChannelId] orderBy:nil ascending:YES]);
             
             [[APIRequest request] MessageSend:strChannelId completed:^(id responseObject, NSError *error) {
                 
                 //                [[APIRequest request] ReadMessage:strChannelId completed:nil];
                 
                 [[APIRequest request] ReadMessage:strChannelId completed:^(id responseObject, NSError *error)
                  {
                      NSLog(@"READ MESAAGE SUCCEFULLY ============= >>> ");
                  }];
                 
             }];
         }
         else
         {
             [appDelegate handleStatus:status];
         }
     }];
}

@end
