//
//  TagpeopleCell.h
//  RunLive
//
//  Created by mac-0004 on 27/10/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagpeopleCell : UITableViewCell
@property IBOutlet UILabel *lblContent;
@property IBOutlet UIImageView *imgDP;
@property IBOutlet UIImageView *imgVerifiedUser;
@property IBOutlet UIView *vwBorder;

@end
