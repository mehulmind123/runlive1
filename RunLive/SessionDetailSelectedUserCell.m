//
//  SessionDetailSelectedUserCell.m
//  RunLive
//
//  Created by mac-0006 on 18/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "SessionDetailSelectedUserCell.h"

@implementation SessionDetailSelectedUserCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    _btnAdd.layer.cornerRadius = CGRectGetHeight(_btnAdd.frame)/2;
    
    _imgUser.layer.cornerRadius = CGRectGetHeight(_imgUser.frame)/2;
    _imgUser.layer.masksToBounds = YES;
    
    _viewUser.layer.cornerRadius = CGRectGetHeight(_viewUser.frame)/2;
    _viewUser.layer.borderWidth = .7;
    _viewUser.layer.borderColor = CRGB(53, 54, 76).CGColor;
    _viewUser.layer.masksToBounds = YES;
    
    _viewGray.layer.masksToBounds = YES;
    _viewGray.layer.cornerRadius = CGRectGetHeight(_viewGray.frame)/2;
    
    _btnAdd.layer.masksToBounds = YES;
    
}

@end
