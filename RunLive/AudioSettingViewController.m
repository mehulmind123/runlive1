//
//  AudioSettingViewController.m
//  RunLive
//
//  Created by mac-0006 on 03/11/2016.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "AudioSettingViewController.h"

@interface AudioSettingViewController ()

@end

@implementation AudioSettingViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sldAudio.value = appDelegate.loginUser.audio.integerValue;
        [self SetCountlblPosition];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action

- (IBAction)btnBackClicked:(id)sender
{
    
    if (appDelegate.loginUser.audio.integerValue != sldAudio.value)
    {
        [self customAlertViewWithTwoButton:@"" Message:CMessageSaveChanges ButtonFirstText:@"Yes" ButtonSecondText:@"No" Animation:YES completed:^(int index)
         {
             if (index == 0) {
                 [[APIRequest request] updateProfile:nil LastName:nil DOB:nil Weight:nil Height:nil Gender:nil Notification:nil Sound:nil Vibrate:nil Audio:[NSString stringWithFormat:@"%d",(int)sldAudio.value] Units:nil ProfileVisibility:nil Subscription:nil MuteVoiceChat:nil VoiceChatEnable:nil AboutUs:nil Link:nil completed:^(id responseObject, NSError *error) {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
             }
             else
             {
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMinusClicked:(id)sender
{
    sldAudio.value = sldAudio.value - 1;
    [self SetCountlblPosition];
}

- (IBAction)btnPlusClicked:(id)sender
{
    sldAudio.value = sldAudio.value + 1;
    [self SetCountlblPosition];
}

- (IBAction)lblAudioSliderValueChanged:(UISlider *)sender
{
    [self SetCountlblPosition];
}

-(void)SetCountlblPosition
{
    lblCount.text = [NSString stringWithFormat:@"%d%@",(int)sldAudio.value,@"%"];
    CGRect trackRect = [sldAudio trackRectForBounds:sldAudio.bounds];
    CGRect thumbRect = [sldAudio thumbRectForBounds:sldAudio.bounds trackRect:trackRect value:sldAudio.value];
    float size = thumbRect.origin.x + sldAudio.frame.origin.x;
    cnLblXPoint.constant = size;
    
    
    //[UIView animateWithDuration:1 animations:^{
  //      [self.view layoutIfNeeded];
    //}];
    
    
}

@end
