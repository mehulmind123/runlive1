//
//  MyProfileViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/3/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : SuperViewController
{
    IBOutlet UITableView *tblUser;
    IBOutlet NSLayoutConstraint *cnTblBottomSpace,*cnTblTopSpace;
}

@property(assign,atomic) BOOL isBackButton;
@end
