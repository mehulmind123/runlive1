//
//  FindRunnerViewController.h
//  RunLive
//
//  Created by mac-00012 on 11/15/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MotionManager.h"

@interface FindRunnerViewController : SuperViewController<AVSpeechSynthesizerDelegate>
{
    IBOutlet UIView *viewUser;
    IBOutlet UIImageView *imgLoginUser,*imgBackground;
    
    IBOutlet MGLMapView *mapView;
    IBOutlet UILabel *lblTitle,*lblNotice;
    
    IBOutlet UIButton *btnQuite;
    IBOutlet UIView *viewQuitContainer,*viewQuit,*viewRunnerNoticeContainer,*viewRunnerNotice;
    IBOutlet UIButton *btnNoWay,*btnYes,*btnGotIt,*btnNoticeCheckBox;
    
}

@property(atomic,assign) BOOL isSoloFind;

@property(nonatomic,strong) NSString *strSessoinId;

// This dictionary contain Session create related data...
@property(nonatomic,strong) NSDictionary *dicSessionData;

// This dictionary contain Find player data...
@property(nonatomic,strong) NSDictionary *dicSessionPlayer;

@end
