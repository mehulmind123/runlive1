//
//  TblUser+CoreDataProperties.m
//  RunLive
//
//  Created by mac-0005 on 20/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import "TblUser+CoreDataProperties.h"

@implementation TblUser (CoreDataProperties)

+ (NSFetchRequest<TblUser *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TblUser"];
}

@dynamic about_us;
@dynamic age;
@dynamic audio;
@dynamic country;
@dynamic createdAt;
@dynamic date_of_birth;
@dynamic email;
@dynamic fb_id;
@dynamic first_name;
@dynamic gender;
@dynamic height;
@dynamic is_default_image;
@dynamic is_premium;
@dynamic is_verified;
@dynamic isVoiceChatEnable;
@dynamic last_name;
@dynamic muteVoiceChat;
@dynamic notification;
@dynamic notification_count;
@dynamic notification_sound;
@dynamic notification_vibrate;
@dynamic picture;
@dynamic picture_path;
@dynamic profile_visibility;
@dynamic profileStatus;
@dynamic receipt_id;
@dynamic reward_point;
@dynamic run_performance;
@dynamic run_rank;
@dynamic run_total_BPM;
@dynamic run_total_calories;
@dynamic run_total_miles;
@dynamic run_total_runs;
@dynamic session_notice_checkbox;
@dynamic status;
@dynamic strava_id;
@dynamic stravaConnection;
@dynamic subscription;
@dynamic subscription_exp_date;
@dynamic subscription_plan;
@dynamic subscription_trial;
@dynamic token;
@dynamic units;
@dynamic updatedAt;
@dynamic user_id;
@dynamic user_name;
@dynamic user_token;
@dynamic verify;
@dynamic weight;
@dynamic link;
@dynamic chats;

@end
