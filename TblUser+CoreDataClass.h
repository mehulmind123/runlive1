//
//  TblUser+CoreDataClass.h
//  RunLive
//
//  Created by mac-0005 on 20/03/18.
//  Copyright © 2018 mindinventory. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TblChats;

NS_ASSUME_NONNULL_BEGIN

@interface TblUser : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TblUser+CoreDataProperties.h"
